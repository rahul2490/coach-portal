## HealthSlate Food Coach Portal

### Server requirements:

* Web server apache2 / nginx
* PHP [With all default extensions] >= 5.5.9
* mysql
* curl, json, mbstring, mcrypt, pdo_mysql, mysqli, OpenSSL, Tokenizer PHP Extension, If not included by default
* expires_module, deflate_module, rewrite_module apache2 Modules, If not included by default


### Jenkins Requirements:
* Jenkins Plugins: Publish Over SSH, Git, xUnit, JUnit.
* [Composer](https://getcomposer.org/)
* node
* [bower](http://bower.io/)
* gulp
* fpm,rpm (Utility for creating deb and rpm, [fpm link](https://github.com/jordansissel/fpm))


### Jenkins Build Configuration:
**Build, Execute Shell:** `make clean && make`

**Post-build Actions:** Send build artifacts over SSH

```
Transfer Set
Source files:	    ./dist/hs-meal-processing-portal*.rpm
Remove prefix:	    dist
Remote directory:	rpms
Exec command:
                    sudo /bin/rpm -iU  /home/jenkins/rpms/hs-meal-processing-portal*.rpm
                    /bin/rm -rf /home/jenkins/rpms/hs-meal-processing-portal*.rpm
```

**Note**: SSH user must be sudoer with NOPASSWD enabled.

### Database and Application Configuration
Once admin package is install on server, there will be need of configuring external config file `.env`. This file can be created by copying `.env.example`. 

## Extras
#### Building minified and combined assets
It utilizes `gulp` for executing tasks of assets management. Assets (js,css,font,image) can be merged, minified and moved to public folder by simply executing command: `gulp`.

#### Development on CSS / LESS / JS
While working on such assets please execute `gulp watch` from terminal. So that each time you will modify these files **gulp** will rebuild corresponding public assets files.

#### External config file
This Application can load environment specific configuration from external file named **.env**. Please refer to `.env.example` for more info.
Each variable within env file will overwrite it's corresponding config variable. Please use pipe sign (`|`) to specify array of values.

#### Checking logs
Application's dated logs are stored within storage folder of app. You may use `tail -0f /var/www/hs-meal-processing-portal/storage/logs/laravel-$(date +"%Y-%m-%d").log` command from application folder to view logs in terminal.

#### Checking deployed version
Every time admin is deployed it creates VERSION file with details of deployment. 
So current deployed version of application can be found at `http://localhost/VERSION`, where localhost would be base URL of application. 
<?php

namespace HealthSlatePortal\Console;

use Illuminate\Console\Scheduling\Schedule;
use Symfony\Component\Console\Input\ArgvInput;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {
	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		//
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule $schedule
	 *
	 * @return void
	 */
	protected function schedule( Schedule $schedule ) {

		$current_command = ( new ArgvInput() )->getFirstArgument();
		// preventing for DB connection requirement on jenkins
		if ( $current_command == "schedule:run" ) {
		}
	}
}

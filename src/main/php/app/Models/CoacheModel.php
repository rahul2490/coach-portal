<?php

/**
 * Created by PhpStorm.
 * User: rpatidar
 * Date: 31/08/17
 */

namespace HealthSlatePortal\Models;

use Carbon\Carbon;
use DB;

class CoacheModel {

    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;

    /**
     *
     */
    function __construct() {
        $this->connection = DB::connection();
    }


    /**
     * @param $facility_id
     * @param Co-brand ID
     * @return
     * get facility coach list function
     * it's show the list of facility coach
     */
    public function get_facility_coach_list($facility_id = 0, $coach_id = 0, $active_user_role = '') {

       $query =  $this->connection
            ->table('users AS u')
            ->select([
                'u.*',
                'pr.provider_id',
                'pr.is_sms_enabled',
                'pr.is_email_enabled',
                'pr.acuity_link',
                'pr.type',
                'f.name',
                $this->connection->raw( 'group_concat(f.name SEPARATOR ", ") as facility_names' ),
                $this->connection->raw( 'group_concat(f.facility_id SEPARATOR ",") as facility_id' ),
            ])
            ->join('provider AS pr', 'pr.user_id', '=', 'u.user_id')
            ->join('facility_provider AS fp', 'pr.provider_id', '=', 'fp.providers_provider_id')
            ->join('facility AS f', 'f.facility_id', '=', 'fp.facility_facility_id')
            ->whereRaw($this->connection->raw('(ORD(pr.is_deleted) = 0 OR pr.is_deleted IS NULL)'));

             if($active_user_role != 'admin')
                $query->whereIn('u.user_type', ['Food Coach' , 'Coach' , 'Tech Support']);
             else
                $query->whereIn('u.user_type', ['Food Coach' , 'Coach' , 'Tech Support']);

             $query->where( 'u.email', 'not like', '\\_%' )
             ->where('u.cobrand_id', '=', session('user')->cobrandId);


       if($active_user_role != 'admin')
            $query->where('f.facility_id', '=', $facility_id);

        if ($coach_id) {
            $query->where('pr.provider_id', '=', $coach_id);
        }

         $query->orderBy('u.first_name','asc');
       return
            $query->groupBy('u.user_id')->get();


    }


    public function get_facility_coach_list_for_admin($facility_id = 0, $coach_id = 0, $active_user_role = '') {

        $query =  $this->connection
            ->table('users AS u')
            ->select([
                'u.*',
                'pr.provider_id',
                'pr.is_sms_enabled',
                'pr.is_email_enabled',
                'pr.acuity_link',
                'pr.type',
                'f.name',
                $this->connection->raw( 'group_concat(f.name SEPARATOR ", ") as facility_names' ),
                $this->connection->raw( 'group_concat(f.facility_id SEPARATOR ",") as facility_id' ),
            ])
            ->leftJoin('provider AS pr', 'pr.user_id', '=', 'u.user_id')
            ->leftJoin('facility_provider AS fp', 'pr.provider_id', '=', 'fp.providers_provider_id')
            ->leftJoin('facility AS f', 'f.facility_id', '=', 'fp.facility_facility_id')
            ->whereRaw($this->connection->raw('(ORD(pr.is_deleted) = 0 OR pr.is_deleted IS NULL)'))
            ->whereIn('u.user_type', ['Food Coach' , 'Coach' , 'Tech Support','ROLE_FACILITY_ADMIN'])
            ->where( 'u.email', 'not like', '\\_%' )
            ->where('u.cobrand_id', '=', session('user')->cobrandId)
            ->orderBy('u.first_name','asc');
        return
            $query->groupBy('u.user_id')->get();



    }


    public function get_all_facility_list_for_admin($cobrand_id) {
        $query =  $this->connection
            ->table('facility AS f')
            ->select(['*'])
            ->where('f.cobrand_id', '=', $cobrand_id)
            ->orderBy('f.name','asc');
        return
            $query->get();
    }


    /**
     * get  cobrand all coach list function
     * it's show  all coach brand list
     */

    public function get_cobrand_all_coach_list($facility_id = 0) {
        return $this->connection
            ->table('users AS u')
            ->select([
                'u.user_id','u.first_name','u.last_name','u.user_type', 'u.email',
                'pr.provider_id',
                'fp.providers_provider_id',
                'fp.facility_facility_id'
            ])
            ->join('provider AS pr', 'pr.user_id', '=', 'u.user_id')
            ->leftJoin('facility_provider AS fp', 'pr.provider_id', '=', 'fp.providers_provider_id')
            ->whereRaw($this->connection->raw('(ORD(pr.is_deleted) = 0 OR pr.is_deleted IS NULL)'))
            ->whereIn('u.user_type', ['Food Coach' , 'Coach'])
            ->where( 'u.email', 'not like', '\\_%' )
            ->where('u.cobrand_id', '=', session('user')->cobrandId)
            ->orderBy('u.first_name', 'asc')
            ->groupBy('u.user_id')
            ->get();
    }



    public function get_admin_facility_list($facility_id = 0, $coach_id = 0, $active_user_role = '') {

        $query =  $this->connection
            ->table('users AS u')
            ->select([
                'u.*',
                'pr.provider_id',
                'pr.is_sms_enabled',
                'pr.is_email_enabled',
                'pr.acuity_link',
                'pr.type',
                'f.name',
                $this->connection->raw( 'group_concat(f.name SEPARATOR ", ") as facility_names' ),
                $this->connection->raw( 'group_concat(f.facility_id SEPARATOR ",") as facility_id' ),
            ])
            ->leftJoin('provider AS pr', 'pr.user_id', '=', 'u.user_id')
            ->leftJoin('facility_provider AS fp', 'pr.provider_id', '=', 'fp.providers_provider_id')
            ->leftJoin('facility AS f', 'f.facility_id', '=', 'fp.facility_facility_id')
            ->whereRaw($this->connection->raw('(ORD(pr.is_deleted) = 0 OR pr.is_deleted IS NULL)'));

        if($active_user_role != 'admin')
            $query->whereIn('u.user_type', ['Food Coach' , 'Coach' , 'Tech Support']);
        else
            $query->whereIn('u.user_type', ['Food Coach' , 'Coach' , 'Tech Support','ROLE_FACILITY_ADMIN']);

        $query->where( 'u.email', 'not like', '\\_%' )
            ->where('u.cobrand_id', '=', session('user')->cobrandId);
        //->orderBy('u.first_name','asc');

        if($active_user_role != 'admin')
            $query->where('f.facility_id', '=', $facility_id);

        if ($coach_id) {
            $query->where('pr.user_id', '=', $coach_id);
        }

        return
            $query->groupBy('u.user_id')->get();



    }


}

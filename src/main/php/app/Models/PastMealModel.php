<?php

/**
 * Created by PhpStorm.
 * User: rpatidar
 * Date: 31/08/17
 */

namespace HealthSlatePortal\Models;

use Carbon\Carbon;
use DB;

class PastMealModel
{

    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;
    protected $group_connection;

    /**
     *
     */
    function __construct()
    {
        $this->connection = DB::connection();
        $this->group_connection = DB::connection('groups');
        $this->patient_db = env('DB_DATABASE');
    }

    public function get_past_meal($patientId,$current_date,$compare_time,$meal_type){
        $query = $this->connection
            ->table('log AS l')
            ->select([ 'l.log_id','l.is_removed', 'fi.image_name', 'l.is_processed', 'l.log_time_offset', 'l.offset_key', 'fls.type', 'fls.meal_name',
               'fls.fats', 'fls.carbs', 'fls.has_missing_food', 'fld.no_of_servings', 'fld.serving_size_unit', 'u.first_name', 'u.last_name',
                'fls.calories', 'fm.serving_size_unit as unit_from_food_master',
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", IF(l.offset_key != "", l.offset_key, u.offset_key) ), "%h:%i %p") as log_time'),
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", IF(l.offset_key != "", l.offset_key, u.offset_key) ), "%m/%d/%Y") as date'),'l.created_on']);
        //->setBindings([ $offset_key, $offset_key], 'select');
        $query->join('patient as p', function($join) use ($patientId, $current_date, $compare_time){
            $join->on('p.patient_id','=','l.patient_id');
            $join->where('l.created_on','>=',$current_date );
            $join->where('l.created_on','<=',$compare_time );
            $join ->where('l.log_type','=','Meal');
            $join ->where('l.patient_id','=',$patientId);
        })
            // ->join('patient as p','p.patient_id', '=' ,'l.patient_id')

            ->leftJoin('users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin('misc_log as m','m.log_id', '=' ,'l.log_id')
            ->leftJoin('food_log_summary as fls','fls.log_id' ,'=' ,'l.log_id')
            ->leftJoin('food_log_detail as fld','fld.food_log_summary_id' ,'=' ,'fls.food_log_summary_id')
            ->leftJoin('food_master as fm', 'fm.food_master_id','=','fld.food_master_id')
            ->leftJoin('food_image as fi', 'fi.food_log_summary_id','=','fls.food_log_summary_id')
            ->leftJoin('exercise_log as el','el.log_id' ,'=' ,'l.log_id')
            ->leftJoin('glucose_log as gl','gl.log_id' ,'=' ,'l.log_id')
            ->leftJoin('activity_log as al','al.log_id' ,'=' ,'l.log_id');
        if($meal_type && $meal_type!="all") {
            $query->where('fls.type','=',$meal_type);
        }
        $query->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL)'));
        $query->whereRaw($this->connection->raw(' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL)'));
        $query->whereRaw($this->connection->raw(' (ORD(l.is_processed) = 1)'));
        $query->groupBy('l.log_id');
        $query->orderBY('l.created_on','desc');
        return $query->get();


    }

    public function get_past_meal_by_log_id($log_id){
        $query = $this->connection
            ->table('log AS l')
            ->select([
                'l.log_id', 'fi.image_name','l.log_time','fls.type', 'fls.meal_name'])
            ->leftJoin('food_log_summary as fls','fls.log_id', '=' ,'l.log_id')
            ->leftJoin('food_log_detail as fld','fld.food_log_summary_id' ,'=' ,'fls.food_log_summary_id')
            ->leftJoin('food_master as fm', 'fm.food_master_id','=','fld.food_master_id')
            ->leftJoin('food_image as fi', 'fi.food_log_summary_id','=','fls.food_log_summary_id')
            ->leftJoin('users as u' ,'u.user_id','= ','l.user_id')
            ->where('l.log_id','=',$log_id);
        $query->groupBy('l.log_id');
        return $query->get();

    }
    public function get_past_meal_by_item($log_id){
        $query = $this->connection
            ->table('log AS l')
            ->select([
                'fld.food_log_detail_id as detail_id','fld.food_name','fld.carbs','fld.fats','fld.protein','fld.calories','fld.no_of_servings as quantity','fld.serving_size_unit as unit'])
            ->leftJoin('food_log_summary as fls','fls.log_id', '=' ,'l.log_id')
            ->leftJoin('food_log_detail as fld','fld.food_log_summary_id' ,'=' ,'fls.food_log_summary_id')
            ->leftJoin('food_master as fm', 'fm.food_master_id','=','fld.food_master_id')
            ->leftJoin('food_image as fi', 'fi.food_log_summary_id','=','fls.food_log_summary_id')
            ->leftJoin('users as u' ,'u.user_id','= ','l.user_id')
            ->where('l.log_id','=',$log_id);
        $query->groupBy('fld.food_log_detail_id');
        return $query->get();

    }

}
<?php

/**
 * Created by PhpStorm.
 * User: rpatidar
 * Date: 21/11/17
 */

namespace HealthSlatePortal\Models;

use Carbon\Carbon;
use DB;

class DashboardSettingModel
{

    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;

    /**
     *
     */
    function __construct()
    {
        $this->connection = DB::connection();
    }


    public function getProfileDetail($patient_id)
    {


        return $this->connection
            ->table('users AS u')
            ->select([
                'u.email',
                'u.first_name',
                'u.last_name',
                'u.address', 'u.city', 'u.state', 'u.zip_code','u.phone','u.temp_password','u.group_visibility','u.group_display_name','p.*','t.starting_weight','t.activity_steps','tm.daily_calorie_budget','pd.dietary_restrictions','pd.meal_preparer','pdiabetes.diagnosis','pdiabetes.diagnosis_date','pdiabetes.diabetes_support','pdiabetes.treated_depression','pdiabetes.time_since_last_diabetes_edu','r.is_snack_time',
                'r.is_breakfast_time','r.is_lunch_time','r.is_dinner_time','r.is_dinner_time','t.weight_time'
            ])
            ->join('patient AS p', 'p.user_id', '=', 'u.user_id')
            ->leftJoin('targets AS t', 't.patient_id', '=', 'p.patient_id')
            ->leftJoin('target_meal AS tm', 't.target_id', '=', 'tm.target_id')
            ->leftJoin('patient_dietary_info AS pd', 'pd.patient_id', '=', 'p.patient_id')
            ->leftJoin('patient_diabetes_info AS pdiabetes', 'pdiabetes.patient_id', '=', 'p.patient_id')
            ->leftJoin('reminder AS r', 'r.target_id', '=', 't.target_id')
            ->where('p.patient_id', '=', $patient_id)
            ->where('tm.meal_type', '=', 'Daily')
            ->first();



    }

    public function getReminderDetail($patient_id)
    {

        return $this->connection
            ->table('users AS u')
            ->select([
                'r.is_snack_time',
                'r.is_breakfast_time','r.is_lunch_time','r.is_dinner_time','r.is_meal_time','r.is_weighing_myself','t.weight_time','t.target_id','tm.meal_target_id','tm.meal_type','tm.meal_time','t.weight_time','ump.time_of_day','ump.day_of_week','ump.type_of_reminder','ump.date_of_month',
                'ump.sms','ump.email','ump.is_in_app_message_notify','ump.is_coach_communication_reminder','ump.is_coach_communication_reminder',
            ])
            ->join('patient AS p', 'p.user_id', '=', 'u.user_id')
            ->leftJoin('targets AS t', 't.patient_id', '=', 'p.patient_id')
            ->leftJoin('target_meal AS tm', 't.target_id', '=', 'tm.target_id')
            ->leftJoin('reminder AS r', 'r.target_id', '=', 't.target_id')
            ->leftJoin('user_messaging_preference AS ump', 'ump.user_id', '=', 'u.user_id')

            ->where('p.patient_id', '=', $patient_id)
            ->get();



    }




    public function get_admin_facility_list($email, $cobrand_id)
    {
        return $this->connection
            ->table('users AS u')
            ->select([
                'pr.provider_id',
                'f.name',
                'f.facility_id'
            ])
            ->join('provider AS pr', 'pr.user_id', '=', 'u.user_id')
            ->join('facility_provider AS fp', 'pr.provider_id', '=', 'fp.providers_provider_id')
            ->join('facility AS f', 'f.facility_id', '=', 'fp.facility_facility_id')
            ->where('u.email', '=', $email)
            ->where('u.cobrand_id', '=', $cobrand_id)
            ->orderBy('f.name', 'asc')
            ->get();
    }

    public function get_all_facility_list($cobrand_id)
    {
        return $this->connection
            ->table('facility AS f')
            ->select([
                'f.facility_id',
                'f.name',
                'f.state',
                'f.city',
                'f.zip',
                'f.address',
                'f.timezone_name',
                'f.timezone_offset',
                'f.contact_info',
                'f.contact_person_name',
                'f.is_notification_enabled',
                'f.is_skip_consent',
                'f.partner_id',
                $this->connection->raw('group_concat(u.first_name SEPARATOR ", ") as user_name'),
                // $this->connection->raw('fp.providers_provider_id') as user_id,
                $this->connection->raw('group_concat(u.email SEPARATOR ", ") as email'),
                //$this->connection->raw('u.email'),
            ])
            ->leftjoin('facility_provider AS fp', 'fp.facility_facility_id', '=', 'f.facility_id')
            ->leftjoin('provider AS pr', 'pr.provider_id', '=', 'fp.providers_provider_id')
            ->leftjoin('users AS u', 'u.user_id', '=', 'pr.user_id')
            ->where('f.cobrand_id', '=', $cobrand_id)
            ->groupBy('f.facility_id')
            ->get();
    }



    public  function get_device_information($patient_id)
    {
        return $this->connection
            ->table('patient AS p')
            ->select([
                'd.registration_id','d.device_type','d.device_id','p.patient_id'
            ])
            ->join('device AS d', 'd.device_mac_address', '=', 'p.device_mac_address' )
            ->where('p.patient_id', '=', $patient_id)
            ->first();
    }


    public function get_taget_reminder_information($patient_id)
    {

        return $this->connection
            ->table('targets AS t')
            ->select([
                't.*','r.*'
            ])
            ->join('reminder AS r', 'r.target_id', '=', 't.target_id')
            ->where('t.patient_id', '=', $patient_id)
            ->first();

    }

    public function get_taget_meal_information($target_id)
    {

        return $this->connection
            ->table('target_meal AS tm')
            ->select([
                'tm.*'
            ])

            ->where('tm.target_id', '=', $target_id)
            ->get();

    }


}



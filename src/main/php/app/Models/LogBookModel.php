<?php

/**
 * Created by PhpStorm.
 * User: rpatidar
 * Date: 31/08/17
 */

namespace HealthSlatePortal\Models;

use Carbon\Carbon;
use DB;

class LogBookModel
{

    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;
    protected $group_connection;

    /**
     *
     */
    function __construct()
    {
        $this->connection = DB::connection();
        $this->group_connection = DB::connection('groups');
        $this->patient_db = env('DB_DATABASE');
        $this->patient_device_connection = DB::connection('device_support');
    }

    public function get_logbook_for_patient($patient_id, $current_date, $compare_time,$meal_type=0, $offset_key = null){
        if (empty($offset_key)) {
            $offset_key = 'UTC';
        }

        $query = $this->connection
            ->table('log AS l')
            ->select([
                'l.log_id','l.is_removed','l.is_processed', 'l.log_time', 'l.log_time_offset', 'l.offset_key', 'fs.type as log_type', 'fs.meal_name',
                'fs.fats', 'fs.carbs','fs.calories', 'fs.has_missing_food',
                   $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", IF(l.offset_key != "", l.offset_key, u.offset_key) ), "%h:%i %p") as date_time'),
                   $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", IF(l.offset_key != "", l.offset_key, u.offset_key) ), "%Y-%m-%d") as date'),'l.created_on']);
                 //->setBindings([ $offset_key, $offset_key], 'select');
              $query->join('patient as p', function($join) use ($patient_id, $current_date, $compare_time){
                 $join->on('p.patient_id','=','l.patient_id');
                 $join->where('l.created_on','>=',$current_date );
                 $join->where('l.created_on','<=',$compare_time );
                 $join ->where('l.log_type','=','Meal');
                 $join ->where('l.patient_id','=',$patient_id);
             })

            ->leftJoin('users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin('misc_log as m','m.log_id', '=' ,'l.log_id')
            ->leftJoin('food_log_summary as fs','fs.log_id' ,'=' ,'l.log_id')
            ->leftJoin('exercise_log as el','el.log_id' ,'=' ,'l.log_id')
            ->leftJoin('glucose_log as gl','gl.log_id' ,'=' ,'l.log_id')
            ->leftJoin('activity_log as al','al.log_id' ,'=' ,'l.log_id');
        if($meal_type && $meal_type!="All") {
            $query->where('fs.type','=',$meal_type);
        }
        $query->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL)'));
        $query->whereRaw($this->connection->raw(' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL)'));
        $query->whereRaw($this->connection->raw(' (ORD(l.is_processed) = 1)'));
        //$query->groupBy('date')
        $query->orderBY('l.created_on','desc');
        return $query->get();

    }

    public function get_logbook_for_patient_activity($patient_id, $current_date, $compare_time, $offset_key = null){
        if (empty($offset_key)) {
            $offset_key = 'UTC';
        }
        $query = $this->connection
            ->table('activity_log_summary AS mfa')
            ->select([
                'mfa.activity_log_summary_id as logId', 'mfa.steps AS steps','mfa.timestamp AS DAY','mfa.activity_type AS log_type', 'mfa.log_time_offset as logOffset',
   'mfa.attribute_type as attributeType', 'mfa.service_name as serviceName',
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(mfa.timestamp/1000) ,"UTC", IF(u.offset_key != "", u.offset_key, u.offset_key) ), "%h:%i %p") as date_time'),
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(mfa.timestamp/1000) ,"UTC", IF(u.offset_key != "", u.offset_key, u.offset_key) ), "%Y-%m-%d") as date'),'mfa.timestamp as created_on'])
            ->join('patient as p','p.patient_id', '=' ,'mfa.patient_id')
            ->join('users as u','u.user_id', '=' ,'p.user_id')
            ->where('mfa.patient_id','=',$patient_id)
            ->where('mfa.timestamp' ,'>=',$current_date)
            ->where('mfa.timestamp','<=',$compare_time)
            ->where('mfa.attribute_type','=','steps')
            ->whereNotNull('u.offset_key');
            //$query->groupBy('date');
            $query->orderBY('mfa.timestamp','ASC');
        return $query->get();

    }


    public function get_logbook_for_patient_activity_minutes($patient_id,$current_date, $compare_time, $offset_key = null){
        if (empty($offset_key)) {
            $offset_key = 'UTC';
        }

        $query = $this->connection
            ->table('log AS l')
            ->select(['l.log_id','l.is_removed','l.is_processed', 'l.log_time', 'l.log_time_offset','l.log_type','l.offset_key','al.minutes_performed','al.type as activity_log_type',
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", IF(l.offset_key != "", l.offset_key, u.offset_key) ), "%h:%i %p") as date_time'),
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", IF(l.offset_key != "", l.offset_key, u.offset_key) ), "%Y-%m-%d") as date'),'l.created_on'

            ])
            //->setBindings([ $offset_key, $offset_key], 'select')
            ->leftJoin('patient AS p', 'l.patient_id', '=', 'p.patient_id' )
            ->leftJoin('users AS u', 'u.user_id', '=', 'p.user_id' )
            ->join('activity_log as al','al.log_id', '=' ,'l.log_id')
            ->where('l.patient_id','=',$patient_id)
            ->where('l.log_type','=','ActivityMinute')
            ->where('l.created_on' ,'>=',$current_date)
            ->where('l.created_on','<=', $compare_time);
        $query->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL)'));
        $query->whereRaw($this->connection->raw(' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL)'));

        //$query->groupBy('date')
        $query->orderBY('l.created_on','ASC');
        return $query->get();

    }


    public function get_logbook_for_patient_activity_min_for_log_sumarry($patient_id, $current_date, $compare_time, $offset_key = null){
        if (empty($offset_key)) {
            $offset_key = 'UTC';
        }
        $query = $this->connection
            ->table('activity_log_summary AS mfa')
            ->select([
                'mfa.activity_log_summary_id as logId', 'mfa.steps AS steps','mfa.timestamp AS DAY','mfa.activity_type', 'mfa.log_time_offset as logOffset',
                'mfa.attribute_type AS log_type', 'mfa.service_name as serviceName',
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(mfa.timestamp/1000) ,"UTC", IF(u.offset_key != "", u.offset_key, u.offset_key) ), "%h:%i %p") as date_time'),
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(mfa.timestamp/1000) ,"UTC", IF(u.offset_key != "", u.offset_key, u.offset_key) ), "%Y-%m-%d") as date'),'mfa.timestamp as created_on'])
            ->join('patient as p','p.patient_id', '=' ,'mfa.patient_id')
            ->join('users as u','u.user_id', '=' ,'p.user_id')
            ->where('mfa.patient_id','=',$patient_id)
            ->where('mfa.timestamp' ,'>=',$current_date)
            ->where('mfa.timestamp','<=',$compare_time)
            ->whereIn('mfa.attribute_type', ['minutesFairlyActive', 'minutesVeryActive'])
            ->whereNotNull('u.offset_key');
        //$query->groupBy('date');
        $query->orderBY('mfa.timestamp','ASC');
        return $query->get();

    }


    public function get_logbook_for_glucose($patient_id,$current_date, $compare_time, $offset_key = null){
        if (empty($offset_key)) {
            $offset_key = 'UTC';
        }

        $query = $this->connection
            ->table('log AS l')
            ->select(['l.log_id','l.is_removed','l.is_processed', 'l.log_time', 'l.log_time_offset','l.log_type','l.offset_key','gl.glucose_level','gl.glucose_log_id',
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", ? ), "%h:%i %p") as date_time'),
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", ? ), "%Y-%m-%d") as date'),'l.created_on'
            ])
            ->setBindings([ $offset_key, $offset_key], 'select')
            ->join('glucose_log as gl','gl.log_id', '=' ,'l.log_id')
            ->where('l.patient_id','=',$patient_id)
            ->where('l.log_type','=','Glucose')
            ->where('l.created_on' ,'>=',$current_date)
            ->where('l.created_on','<=', $compare_time);
        $query->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL)'));
        $query->whereRaw($this->connection->raw(' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL)'));

        //$query->groupBy('date')
        $query->orderBY('l.created_on','ASC');
        return $query->get();

    }

    public function get_logbook_for_medication($patient_id,$current_date, $compare_time, $offset_key = null){
        if (empty($offset_key)) {
            $offset_key = 'UTC';
        }

        $query = $this->connection
            ->table('log AS l')
            ->select(['l.log_id','l.is_removed','l.is_processed', 'l.log_time', 'l.log_time_offset','l.log_type','l.offset_key','ml.quantityTaken','ml.medication_log_id',
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", ? ), "%h:%i %p") as date_time'),
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", ? ), "%Y-%m-%d") as date'),'l.created_on'
            ])
            ->setBindings([ $offset_key, $offset_key], 'select')
            ->join('medication_log as ml','ml.log_id', '=' ,'l.log_id')
            ->where('l.patient_id','=',$patient_id)
            ->where('l.log_type','=','Medication')
            ->where('l.created_on' ,'>=',$current_date)
            ->where('l.created_on','<=', $compare_time);
        $query->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL)'));
        $query->whereRaw($this->connection->raw(' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL)'));

        //$query->groupBy('date')
        $query->orderBY('l.created_on','ASC');
        return $query->get();

    }




    public function get_patient_weight_by_log($patient_id, $current_date, $compare_time, $offset_key = null){
        if (empty($offset_key)) {
            $offset_key = 'UTC';
        }
        $query = $this->connection
            ->table('log AS l')
            ->select([
                'l.log_id','l.log_type','ml.weight as weight','l.log_source as serviceName',
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", IF(l.offset_key != "", l.offset_key, u.offset_key) ), "%h:%i %p") as date_time'),
                $this->connection->raw('DATE_FORMAT(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", IF(l.offset_key != "", l.offset_key, u.offset_key) ), "%Y-%m-%d") as date'),'l.created_on'])
            //->setBindings([ $offset_key, $offset_key], 'select')

            ->leftJoin('patient AS p', 'l.patient_id', '=', 'p.patient_id' )
            ->leftJoin('users AS u', 'u.user_id', '=', 'p.user_id' )
            ->join('misc_log as ml','ml.log_id', '=' ,'l.log_id')
            ->where('l.patient_id','=',$patient_id)
            ->where('l.log_type','=','Weight')
            ->where('l.created_on','>=',$current_date )
            ->where('l.created_on','<=',$compare_time );
             $query->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL)'));
             $query->whereRaw($this->connection->raw(' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL)'));

                 $query->orderBY('l.created_on','desc');
            return $query->get();
    }

    public function  patient_device_data($patient_uuid)
    {
        $query =$this->patient_device_connection
            ->table('patient_device AS pd')
            ->select([
                'pd.*' ])
            ->join('patient as p','p.patient_uuid', '=' ,'pd.patient_uuid')
            ->join('device as d','d.device_name', '=' ,'pd.device_name')
            ->where('p.patient_uuid','=',$patient_uuid)
            ->where('d.device_name','=','fitbit')
            ->first();

         return $query;
    }

    public function update_patient_service_log($patient_uuid,$activity_start_date)
    {

        $query =$this->patient_device_connection
            ->table('patient_service_log')
            ->where('patient_uuid','=',$patient_uuid)
            ->update(['activity_start_date' => $activity_start_date]);
        return $query;


    }


    public function get_fitbit_notification_data($third_party_user_id)
    {
        $query =$this->patient_device_connection
            ->table('fitbit_notification AS fbn')
            ->select([
                'fbn.*' ])
            ->where('is_processed','=',0)
            ->where('is_error','=',0)
            ->where('owner_id','=',$third_party_user_id)
            ->where('collection_type','=','activities')
            ->first();

        return $query;

    }

   public function insert_fitbit_notification_data($third_party_user_id,$date)
   {

       $query =$this->patient_device_connection
           ->table('fitbit_notification')->insert(
               array(
                   'collection_type'     =>   'activities',
                   'owner_id'   =>   $third_party_user_id,
                   'date'   =>   $date,
                   'subscription_id'   =>  $third_party_user_id,
                   'user_type'   =>   'user',
                   'count'   =>   0,
                   'is_processed'   =>  0,
                   'is_error'   =>   0
               )
           );

       return $query;


   }

   public function update_fitbit_notification_data($fitbit_notification_id,$count)
   {

       $query =$this->patient_device_connection
           ->table('fitbit_notification')
           ->where('fitbit_notification_id','=',$fitbit_notification_id)
           ->update(['count' => $count]);
       return $query;



   }




}
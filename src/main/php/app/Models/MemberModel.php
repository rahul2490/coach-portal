<?php

/**
 * Created by PhpStorm.
 * User: rpatidar
 * Date: 31/08/17
 */

namespace HealthSlatePortal\Models;

use Carbon\Carbon;
use DB;
use phpDocumentor\Reflection\Types\Null_;

class MemberModel {

    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;
    protected $group_connection;

    /**
     *
     */
    function __construct() {
        $this->connection           = DB::connection();
        $this->group_connection     = DB::connection('groups');
        $this->group_db             = env('GROUP_DB_DATABASE');
        $this->patient_db           = env('DB_DATABASE');
        $this->active_user_role     = session('userRole');
    }


    /* @param $patiet_id $loggedInUser
     * @desc TO get last messages send date of the logged in user
     */
    public function get_last_message_sent_to_patient($patiet_id = 0, $loggedInUserId) {

        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id','m.message',
                $this->connection->raw( 'MAX(IF(m.owner = "PROVIDER", m.timestamp, NULL)) as last_message_sent_to_patient' ),
                $this->connection->raw( 'MAX(IF(m.owner = "Tech Support", m.timestamp, NULL)) as last_message_sent_to_patient_tech_support' ),
            ] );

            $query->leftJoin('message AS m', function($join) use ($loggedInUserId, $query, $patiet_id) {
                $join->on('p.patient_id', '=', 'm.patient_id');
                if($loggedInUserId !='' && $this->active_user_role != 'admin' && $this->active_user_role != 'facilityadmin'){
                    $join->where('m.user_id', '=', $loggedInUserId);
                    if(!empty($patiet_id)) {
                        $query->whereRaw($this->connection->raw(' m.message_id in (select max(m.message_id) from message as m where (m.owner = "PROVIDER" OR m.owner = "Tech Support" ) and m.patient_id in (' . implode(',', $patiet_id) . ') group by m.patient_id) '));
                    }
                }
            });

        return $query
            ->whereIn( 'p.patient_id', $patiet_id )
            ->groupBy('p.patient_id')
            ->get();
    }

    /**
     * get member list function
     * it's give list of members
     */



    public function get_member_list($facility_id = 0, $member_filter = '', $group_filter = '' ,$another_filter='', $loggedInUserId = 0, $request, $birthday_date = '') {

        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'u.user_id', 'u.email', 'p.patient_id','u.first_name','u.last_name','u.email','p.image_path as patient_profile_image','u.registration_date',
                'p.app_version','gm.user_id as group_user_id', 'hupu.hide_unhide','hupu.pin_unpin',
                'pm.date','pm.engagement_score','pm.is_at_milestone_risk','pm.is_missing_starting_weight','pm.is_weight_increasing','pm.is_not_losing__wt_low_engagement',
                'pm.is_decrease_in_weight_logging','pm.is_engagement_declining','pm.is_no_step_reported_3_days','pm.is_not_responded_to_2_messages','pm.is_avg_step_declining',
                'pm.is_on_track','pm.patient_metrics_id',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
                $this->connection->raw('FLOOR(DATEDIFF(CURDATE(), IF(DAYNAME(u.registration_date) = "Sunday" , u.registration_date , date(u.registration_date + INTERVAL 6 - weekday(u.registration_date) DAY))) / 7) as member_completed_week'),


            ] )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( "group_user AS gm", 'u.user_id', '=', 'gm.user_id' );
            //->leftJoin( 'message AS m', 'p.patient_id', '=', 'm.patient_id' )

         if($birthday_date!="")
         {
             //$query->where("p.dob","=",$birthday_date);
             $query->where('p.dob', 'like', '%'.$birthday_date->format("m-d").'%');
         }

        if ($member_filter == 'hidden_members') {

            $query->join('patient_hide_unhide_pin_unpin_status AS hupu', function($join) use ($loggedInUserId) {
                $join->on('p.patient_id', '=', 'hupu.patient_id');
                $join->where('hupu.hide_unhide', '=', 1);
                $join->where('hupu.user_id', '=', $loggedInUserId);
            });
        }
        else
        {
           $query->leftJoin('patient_hide_unhide_pin_unpin_status AS hupu', function($join) use ($loggedInUserId) {
            $join->on('p.patient_id', '=', 'hupu.patient_id');
            $join->where('hupu.user_id', '=', $loggedInUserId);
           });

        }

        if ($member_filter == 'recommended_message') {
            $query->join('patient_metrics AS pm', function($join) use ($loggedInUserId) {
                $join->on('pm.patient_id', '=', 'p.patient_id');
                $join->where('pm.date', '=', date('Y-m-d'));
            });
        }
        else
        {
            $query->leftJoin('patient_metrics AS pm', function($join) use ($loggedInUserId) {
                $join->on('pm.patient_id', '=', 'p.patient_id');
                $join->where('pm.date', '=', date('Y-m-d'));
            });
        }

//        if ($member_filter == 'member_without_group') {
//            $query->whereNull('gm.user_id');
//            $query->whereRaw($this->connection->raw('(ORD(u.is_registration_completed) = 1)'));
//        }
        if ($member_filter == 'in_person_member') {
            $query->leftJoin( "$this->group_db.groups AS g", 'gm.group_id', '=', 'g.group_id' );
            $query->where('g.is_person', '=', 1);
        }
        if ($member_filter == 'online_member') {
            $query->leftJoin( "$this->group_db.groups AS g", 'gm.group_id', '=', 'g.group_id' );
            $query->where('g.is_person', '!=', 1);
        }

        if ($member_filter == 'my_members') {
            $query->leftJoin( "$this->group_db.groups AS g", 'gm.group_id', '=', 'g.group_id' );
            if($this->active_user_role == 'leadcoach')
                $query->where('g.lead_user_id', '=', $loggedInUserId);

            if($this->active_user_role == 'foodcoach')
                $query->where('g.primary_food_coach_id', '=', $loggedInUserId);
            /*else
            {
                $query->leftJoin( "group_user AS gum", 'g.group_id', '=', 'gum.group_id' );
                $query->whereIn('gum.user_id', [$loggedInUserId]);
            }*/
        }

        if ($member_filter == 'member_group') {
            if ($group_filter != '') {
                $query->where('gm.group_id', $group_filter);
            }
        }

        if ($member_filter == 'member_without_group') {
            if ($another_filter != '') {
                if ($another_filter == 'all') {
                    $query->whereNull('gm.user_id');
                    //$query->whereRaw($this->connection->raw('(ORD(u.is_registration_completed) = 1)'));
                }
                if ($another_filter == 'offline_members') {
                    $query->whereNull('gm.user_id');
                    $query->whereNull('u.registration_date');
                    $query->whereRaw($this->connection->raw(' (ORD(u.is_registration_completed) = 0 )'));

                }
                if ($another_filter == 'ennrolled_members') {
                    $query->whereNull('gm.user_id');
                    $query->where('u.password', '!=', '');
                }
            }
        }


        if ($keyword = request()->get( 'search' )['value']) {

            $query->havingRaw( $this->connection->raw( ' (
                            p.patient_id like ?
                            OR u.email like ?
                            OR u.first_name like ?
                            OR u.last_name like ?
                        )' ), [ "%$keyword%", "%$keyword%", "%$keyword%", "%$keyword%"] );
        }

        if ($keyword = request()->get( 'order' )[0]['column']) {

            if($keyword == 1 && request()->get( 'order' )[0]['dir'] == 'asc')
                $query->orderBy( 'p.patient_id' , 'asc');
            elseif(($keyword == 7) && request()->get( 'order' )[0]['dir'] == 'asc')
               $query->orderBy( 'u.registration_date' , 'desc');
            elseif(($keyword == 7) && request()->get( 'order' )[0]['dir'] == 'desc')
               $query->orderBy( 'u.registration_date' , 'asc');

            elseif(($keyword == 5) && request()->get( 'order' )[0]['dir'] == 'asc')
            {
                $query->leftJoin('message AS m', function($join) use ($loggedInUserId) {
                    $join->on('p.patient_id', '=', 'm.patient_id');
                    $join->where('m.user_id', '=', $loggedInUserId);
                });

                $query->orderBy('m.timestamp' , 'asc');
            }
            elseif(($keyword == 5) && request()->get( 'order' )[0]['dir'] == 'desc')
            {
                $query->leftJoin('message AS m', function($join) use ($loggedInUserId) {
                    $join->on('p.patient_id', '=', 'm.patient_id');
                    $join->where('m.user_id', '=', $loggedInUserId);
                });
//                $query->orderBy( 'm.timestamp' , 'desc');
                $query->orderBy( 'm.timestamp', 'asc');
            }

            elseif(($keyword == 4) && request()->get( 'order' )[0]['dir'] == 'asc')
            {
                $query->leftJoin('message AS m', function($join) use ($loggedInUserId) {
                    $join->on('p.patient_id', '=', 'm.patient_id');
                });
                $query->leftJoin('message_tag AS mt', function($join) use ($loggedInUserId, $query) {
                    $join->on('m.message_id', '=', 'mt.message_id');

                    if ($this->active_user_role == 'techsupport')
                        $join->where('role', '=', 'Support');
                    elseif ($loggedInUserId != '')
                        $join->where('mt.user_id', '=', $loggedInUserId);
                });
                $query->orderBy( 'mt.created_on' , 'asc');
                $query->whereRaw($this->connection->raw('(ORD(mt.is_read) = 0 OR mt.is_read IS NULL)'));
            }
            elseif(($keyword == 4) && request()->get( 'order' )[0]['dir'] == 'desc')
            {
                $query->leftJoin('message AS m', function($join) use ($loggedInUserId) {
                    $join->on('p.patient_id', '=', 'm.patient_id');
                });
                $query->leftJoin('message_tag AS mt', function($join) use ($loggedInUserId, $query) {
                    $join->on('m.message_id', '=', 'mt.message_id');

                    if ($this->active_user_role == 'techsupport')
                        $join->where('role', '=', 'Support');
                    elseif ($loggedInUserId != '')
                        $join->where('mt.user_id', '=', $loggedInUserId);
                });
                $query->orderBy( 'mt.created_on' , 'desc');
//                $query->orderBy( 'mt.created_on' , 'desc');
                $query->whereRaw($this->connection->raw('(ORD(mt.is_read) = 0 OR mt.is_read IS NULL)'));
            }

            elseif(($keyword == 2) && request()->get( 'order' )[0]['dir'] == 'asc')
            {
                $query->orderBy( 'pm.is_at_milestone_risk' , 'asc');
            }
            elseif(($keyword == 2) && request()->get( 'order' )[0]['dir'] == 'desc')
            {
                $query->orderBy( 'pm.is_at_milestone_risk' , 'desc');
            }
            elseif(($keyword == 3) && request()->get( 'order' )[0]['dir'] == 'asc')
            {
                $query->leftJoin('log AS l', function($join) {
                    $join->on('l.patient_id', '=', 'p.patient_id');
                    $join->where('l.is_removed', '!=', 1);
                    $join->where('l.log_type', '=', 'Weight');
                });
                $query->orderBy( 'l.created_on' , 'asc');
            }
            elseif(($keyword == 3) && request()->get( 'order' )[0]['dir'] == 'desc')
            {
                $query->leftJoin('log AS l', function($join) {
                    $join->on('l.patient_id', '=', 'p.patient_id');
                    $join->where('l.is_removed', '!=', 1);
                    $join->where('l.log_type', '=', 'Weight');
                });
                $query->orderBy( 'l.created_on' , 'desc');
            }
            else
            {
                $query->orderBy( 'p.patient_id' , 'desc');
            }
        }
        else
        {
            $query->orderBy( 'p.patient_id' , 'desc');
        }

        if($this->active_user_role == 'admin')
        {
            //$query->where( 'p.facility_id', $facility_id );
            $query->select( [
                'u.user_id', 'u.email', 'p.patient_id','u.first_name','u.last_name','u.email','p.image_path as patient_profile_image','u.registration_date',
                'p.app_version','gm.user_id as group_user_id', 'hupu.hide_unhide','hupu.pin_unpin',
                'pm.date','pm.engagement_score','pm.is_at_milestone_risk','pm.is_missing_starting_weight','pm.is_weight_increasing','pm.is_not_losing__wt_low_engagement',
                'pm.is_decrease_in_weight_logging','pm.is_engagement_declining','pm.is_no_step_reported_3_days','pm.is_not_responded_to_2_messages','pm.is_avg_step_declining',
                'pm.is_on_track','pm.patient_metrics_id',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
                $this->connection->raw('FLOOR(DATEDIFF(CURDATE(), IF(DAYNAME(u.registration_date) = "Sunday" , u.registration_date , date(u.registration_date + INTERVAL 6 - weekday(u.registration_date) DAY))) / 7) as member_completed_week'),
            ] );
            $query->leftJoin( 'facility AS f', 'f.facility_id', '=', 'p.facility_id' );
        }
        else
            $query->where( 'p.facility_id', $facility_id );

        return $query
            ->whereRaw($this->connection->raw('(ORD(p.is_deleted) = 0 OR p.is_deleted IS NULL)'))
            ->where( 'u.email', 'not like', '\\_%' )
            //->where('p.patient_id',7639)
            ->where('u.cobrand_id', session('user')->cobrandId)
            //->whereRaw($this->connection->raw('(ORD(u.is_registration_completed) = 1)'))
            ->groupBy('p.patient_id')
            //->limit($request['length'], $request['start'])
            ->get();
    }




    public function get_member_message_detail($patient_ids,$user_id) {
        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id',
                $this->connection->raw( 'count(DISTINCT mt.message_tag_id) as total_unread_message' ),
                //$this->connection->raw( 'count(DISTINCT IF( (m.owner = "PATIENT" AND m.read_status = 0),m.message_id,NULL) ) as total_unread_message' ),
                //$this->connection->raw( 'from_unixtime( MAX(IF(m.owner = "PROVIDER", m.timestamp, NULL)) /1000 ) as last_message_sent_to_patient' ),
                //$this->connection->raw( 'from_unixtime( MIN(IF(m.owner = "PATIENT" AND m.read_status = 0, m.timestamp, NULL)) /1000 ) as last_unread_message_of_patient' ),
                //$this->connection->raw( 'MAX(IF(m.owner = "PROVIDER", m.timestamp, NULL)) as last_message_sent_to_patient' ),
                $this->connection->raw( 'MAX(IF(m.owner = "PATIENT" , mt.created_on, NULL)) as last_unread_message_of_patient' ),
            ] );
             $query->leftJoin('message AS m', function($join) use ($user_id) {
                 $join->on('p.patient_id', '=', 'm.patient_id');
                 //if($user_id !=''){
                   //  $join->where('m.user_id', '=', $user_id);
                 //}
             });
            $query->leftJoin('message_tag AS mt', function($join) use ($user_id, $query) {
                $join->on('m.message_id', '=', 'mt.message_id');

                if($this->active_user_role == 'techsupport')
                    $join->where('role', '=', 'Support');
                elseif ($user_id !='')
                    $join->where('mt.user_id', '=', $user_id);

                $query->whereRaw($this->connection->raw('(ORD(mt.is_read) = 0 OR mt.is_read IS NULL)'));
            });

        return $query
            ->whereIn( 'p.patient_id', $patient_ids )
            ->groupBy('p.patient_id')
            ->get();
    }


    public function get_in_app_message_list($patient_ids) {

        $query = $this->connection
            ->table( 'message AS m' )
            ->select( [
                'm.message','m.patient_id',
            ] );

        return $query
            ->whereIn( 'm.patient_id', $patient_ids )
            ->where('m.timestamp', '>', (strtotime('-7 days') * 1000))
            ->where('m.owner', '=', 'PROVIDER')
            ->get();
    }


    public function get_my_saved_message_list($patient_ids) {

        $query = $this->connection
            ->table( 'message AS m' )
            ->select( [
                'm.message','m.patient_id',
            ] );

        return $query
            ->where( 'm.patient_id', $patient_ids )
            ->where('m.owner', '=', 'PROVIDER')
            ->where('m.saved_message', 1)
            ->groupBy('m.message')
            ->orderBy('m.message_id','desc')
            ->limit(20)
            ->get();
    }


    public function get_member_hide_unhide_detail($patient_ids, $loggedInUserId) {

        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id','hupu.hide_unhide','hupu.pin_unpin',
            ] )
            ->leftJoin('patient_hide_unhide_pin_unpin_status AS hupu', function($join) use ($loggedInUserId) {
                $join->on('p.patient_id', '=', 'hupu.patient_id');
                $join->where('hupu.user_id', '=', $loggedInUserId);
            });

        return $query
            ->whereIn( 'p.patient_id', $patient_ids )
            ->groupBy('p.patient_id')
            ->get();
    }


    public function get_member_patient_metrics_detail($patient_ids, $loggedInUserId) {

        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id','pm.date','pm.engagement_score','pm.is_at_milestone_risk','pm.is_missing_starting_weight','pm.is_weight_increasing','pm.is_not_losing__wt_low_engagement',
                'pm.is_decrease_in_weight_logging','pm.is_engagement_declining','pm.is_no_step_reported_3_days','pm.is_not_responded_to_2_messages','pm.is_avg_step_declining',
                'pm.is_on_track','pm.patient_metrics_id',
            ] )
            ->leftJoin('patient_metrics AS pm', function($join) use ($loggedInUserId) {
                $join->on('pm.patient_id', '=', 'p.patient_id');
                $join->where('pm.date', '=', date('Y-m-d'));
            });

        return $query
            ->whereIn( 'p.patient_id', $patient_ids )
            ->groupBy('p.patient_id')
            ->get();
    }



    public function get_member_detail_group_detail($facility_id = 0, $user_id) {

        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'u.user_id','p.uuid','d.device_type','cpt.name as curriculum_program_type_name', 'p.patient_id as pid','p.lead_coach_id', 'p.image_path as patient_profile_image', 'u.offset_key','u.email','u.registration_date','gm.user_id as group_user_id', 'g.name as group_name','g.group_id','g.is_person','g.capacity','g.lead_user_id','g.primary_food_coach_id','is_full','g.diabetes_type_id',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),'pm.*','p.patient_id'
            ] )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( "$this->group_db.group_member AS gm", 'u.user_id', '=', 'gm.user_id' )
            ->leftJoin( "$this->group_db.groups AS g", 'gm.group_id', '=', 'g.group_id' )
            ->leftJoin('patient_metrics AS pm', 'pm.patient_id', '=', 'p.patient_id')
            ->leftJoin( 'device AS d', 'p.device_mac_address', '=', 'd.device_mac_address' )
            ->leftJoin( 'diabetes_type AS dt', 'p.diabetes_type_id',  '=', 'dt.diabetes_type_id' )
            ->leftJoin( 'curriculum_program_type AS cpt', 'dt.curriculum_program_type_id',  '=', 'cpt.curriculum_program_type_id' );

        if($this->active_user_role != 'admin')
            $query->where( 'p.facility_id', $facility_id );

        return $query
            ->whereRaw($this->connection->raw('(ORD(p.is_deleted) = 0 OR p.is_deleted IS NULL)'))
            ->where( 'u.email', 'not like', '\\_%' )
            ->where('u.cobrand_id' , session('user')->cobrandId)
            ->where( 'u.user_id', $user_id )
            ->orderBy( 'p.patient_id')
            ->first();
    }



    public function get_member_provider_chat_detail($facility_id, $user_id, $filter_user_id, $last_message_id = 0) {

        $query =  $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id','p.image_path as patient_profile_image',
                'm.message_id','m.message','m.owner','m.read_status','m.image_path','m.timestamp',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as patient_full_name' ),
                $this->connection->raw( 'concat(provider.first_name, " " ,provider.last_name) as provider_full_name' ),
            ] )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( 'message AS m', 'p.patient_id', '=', 'm.patient_id' )
            ->leftJoin( 'users AS provider', 'm.user_id', '=', 'provider.user_id' )
            ->where('u.cobrand_id' , session('user')->cobrandId);
            //->where( 'm.timestamp' ,'>', (strtotime('-1 months')));
            //->where( 'u.user_id', $user_id );

        if($this->active_user_role != 'admin')
            $query->where( 'p.facility_id', $facility_id );

        if ($last_message_id) {
            $query->where( 'm.message_id', '>' , $last_message_id );
        }

        if ($filter_user_id && $filter_user_id != 'all') {
            $query->where( 'm.user_id', $filter_user_id );
            $query->where( 'u.user_id', $user_id );
        }
        else
        {
            $query->where( 'u.user_id', $user_id );
        }

        return $query
            ->orderBy('m.message_id', 'desc')
            ->limit(200)
            ->get();
    }






    public function get_facility_tech_support_list($facility_id) {

        return $this->connection
            ->table('users AS u')
            ->select([
                'u.user_id','u.user_type','u.first_name','u.email',
                'pr.provider_id','f.lead_coach',
                'fp.providers_provider_id',
                'fp.facility_facility_id',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as provider_full_name' )
            ])
            ->join('provider AS pr', 'pr.user_id', '=', 'u.user_id')
            ->leftJoin('facility_provider AS fp', 'pr.provider_id', '=', 'fp.providers_provider_id')
            ->leftJoin('facility AS f', 'f.facility_id', '=', 'fp.facility_facility_id')
            ->whereRaw($this->connection->raw('(ORD(pr.is_deleted) = 0 OR pr.is_deleted IS NULL)'))
            ->whereIn('u.user_type', ['Tech Support'])
            ->where( 'u.email', 'not like', '\\_%' )
            ->where( 'fp.facility_facility_id', $facility_id )
            ->where('u.cobrand_id', '=', session('user')->cobrandId)
            ->groupBy('u.user_id')
            ->get();
    }



    public function get_member_detail_provider_detail($facility_id = 0, $user_id) {

        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'u.group_display_name','u.user_id','u.first_name','u.email','ump.is_in_app_message_notify', 'ump.email as patient_send_email_notify', 'p.patient_id','lcu.user_id as lead_coach_id', 'p.image_path as patient_profile_image', 'u.offset_key','d.registration_id','d.device_type',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as patient_full_name' ),'gm.group_id as user_group_id',
                $this->connection->raw( 'concat(lcu.first_name, " " ,lcu.last_name) as leadcoach_full_name' ),'f.lead_coach as facility_default_lead_coach_id'
            ] )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin('provider AS pr', 'pr.user_id', '=', 'p.lead_coach_id')
            ->leftJoin( 'users AS lcu', 'pr.user_id', '=', 'lcu.user_id' )
            ->leftJoin('device AS d', 'd.device_mac_address', '=', 'p.device_mac_address' )
            ->leftJoin('facility AS f', 'f.facility_id', '=', 'p.facility_id')
            ->leftJoin( "group_user AS gm", 'u.user_id', '=', 'gm.user_id' )
            ->leftJoin( 'user_messaging_preference AS ump', 'u.user_id', '=', 'ump.user_id' );

        return $query
            ->whereRaw($this->connection->raw('(ORD(p.is_deleted) = 0 OR p.is_deleted IS NULL)'))
            ->where( 'u.email', 'not like', '\\_%' )
            ->where('u.cobrand_id' , session('user')->cobrandId)
            ->where( 'p.facility_id', $facility_id )
            ->where( 'u.user_id', $user_id )
            ->orderBy( 'p.patient_id')
            ->first();
    }


    public function get_message_users_list($facility_id = 0, $patient_id) {

        $query =  $this->connection
            ->table( 'message AS m' )
            ->select( [
                'u.user_id','u.first_name',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as provider_full_name' ),
            ] )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'm.user_id' )
            ->where('u.cobrand_id' , session('user')->cobrandId)
            ->whereNotIn('u.user_type', ['Tech Support'])
            ->where( 'm.patient_id', $patient_id );

        return $query
            ->groupBy('m.user_id')
            ->get();
    }



    public function get_group_users_list_by_group_id($facility_id = 0, $group_id) {

        $query =  $this->connection
            ->table("$this->group_db.group_member AS gm")
            ->select( [
                'u.user_id','u.first_name','u.user_type','u.email',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as provider_full_name' ),
            ] )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'gm.user_id' )
            ->where('u.cobrand_id' , session('user')->cobrandId)
            ->whereNotIn('u.user_type', ['PATIENT'])
            ->where('gm.group_id', $group_id);

        return $query
            ->orderBy('u.first_name')
            ->groupBy('u.user_id')
            ->get();
    }


    public function get_recommended_message_list(){
        $query =  $this->connection
            ->table( 'predefined_message AS pm' )
            ->select( [
                'pm.*'
            ] )
            ->where('pm.message_type' , 'Recommended')
            ->where('pm.is_published' , 1)
            ->where('pm.status' , 'is_at_milestone_risk');

        return $query
            ->get();
    }


    /* @param patient_id
     * @desc To get unread message count of the Member.
     */
    public function get_member_unread_message_count_detail($patient, $user_id) {

        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id','m.message_id'
            ] );
        $query->join('message AS m', function($join) use ($user_id) {
            $join->on('p.patient_id', '=', 'm.patient_id');
            //$join->where('m.user_id', '=', $user_id);
        });
        $query->join('message_tag AS mt', function($join) use ($user_id, $query) {
            $join->on('m.message_id', '=', 'mt.message_id');

            if($this->active_user_role == 'techsupport')
            {
                $join->where('role', '=', 'Support');
            }
            else
                $join->where('mt.user_id', '=', $user_id);

            $query->whereRaw($this->connection->raw('(ORD(mt.is_read) = 0 OR mt.is_read IS NULL)'));
        });

        return $query
            ->where( 'p.patient_id', $patient )
            ->get();
    }



    /**
     * @param $patient_id
     *
     * @return array|static[]
     */
    public function get_patient_current_weight_date($patient_ids,$pastweek_date,$today_date){
        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( ['p.patient_id',
                $this->connection->raw('max(l.created_on) as last_weight_date')
            ] )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( 'log AS l', 'l.patient_id', '=', 'p.patient_id' );
            //->leftJoin( 'misc_log AS ml', 'ml.log_id', '=', 'l.log_id' );

        return $query->whereIn('p.patient_id', $patient_ids )
            ->where('l.is_removed', '!=', 1)
            ->where('l.log_type', '=', 'Weight')
            ->groupBy('p.patient_id')
            ->get();
    }



    /**
     * @param $patient_id
     * @Desc Get Patient Engajment Schore
     * @return array|static[]
     */
    public function get_patient_engagement_score($patient_ids){

        $now = Carbon::now();
        if($now->isSunday()){
            $last_sunday = $now->copy()->startOfDay();
        }else{
            $last_sunday = new Carbon('last sunday');
        }
        $current_start   = $last_sunday;
        $current_end     = $now;
        $last_week_start = $current_start->copy()->subWeeks(1);
        $last_week_end   = $current_start;

        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( ['p.patient_id',
                $this->connection->raw( 'SUM(
					IF(
						(
						(pm.engagement_score > 0) AND
						pm.date >= "'.$last_week_start->format( 'Y-m-d' ).'"
				        AND pm.date < "'.$last_week_end->format( 'Y-m-d' ).'"),
				        pm.engagement_score, 0
					)
				) as prior_week_engagement_score' ),

                $this->connection->raw( 'COUNT(
					IF(
						(
						(pm.engagement_score > 0) AND
						pm.date >= "'.$last_week_start->format( 'Y-m-d' ).'"
				        AND pm.date < "'.$last_week_end->format( 'Y-m-d' ).'"),
				        pm.engagement_score, null
					)
				) as prior_week_engagement_score_total_days' ),

                $this->connection->raw( 'SUM(
					IF(					
						(
						(pm.engagement_score > 0) AND
						pm.date >= "'.$current_start->format( 'Y-m-d' ).'"
				        AND pm.date <= "'.$current_end->format( 'Y-m-d' ).'"),
						pm.engagement_score, 0
					)
				) as current_week_engagement_score' ),

                $this->connection->raw( 'COUNT(
					IF(					
						(
						(pm.engagement_score > 0) AND
						pm.date >= "'.$current_start->format( 'Y-m-d' ).'"
				        AND pm.date <= "'.$current_end->format( 'Y-m-d' ).'"),
						pm.engagement_score, null
					)
				) as current_week_engagement_score_total_days' ),
            ] )
            ->leftJoin( 'patient_metrics AS pm', 'pm.patient_id', '=', 'p.patient_id' );

        return
            $query->whereIn('p.patient_id', $patient_ids )
            ->groupBy('p.patient_id')
            ->get();
    }




}//End of File

<?php

/**
 * Created by PhpStorm.
 * User: rpatidar
 * Date: 31/08/17
 */

namespace HealthSlatePortal\Models;

use Carbon\Carbon;
use DB;

class GroupDetailModel {

    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;
    protected $group_connection;

    /**
     *
     */
    function __construct() {
        $this->connection = DB::connection();
        $this->group_connection = DB::connection('groups');
        $this->patient_db  = env('DB_DATABASE');
        $this->active_user_role     = session('userRole');
        $this->group_db  = env('GROUP_DB_DATABASE');
    }


    /**
     * group_report_for_duration
     *
     * @param $facility_id
     * @param null|Carbon $start
     * @param null|Carbon $end
     * @param null $lead_coach
     *
     * @return array|static[]
     */
    public function get_group_report_for_duration(array $patients, Carbon $start, Carbon $end, Carbon $prior_week_start, Carbon $prior_week_end,$group_session_id=0){
        $query =   $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id',
                'dt.name AS diabetes_type_name',
                'p.diabetes_type_id',
                'p.uuid','p.image_path as patient_profile_image',
                'cpt.name as curriculum_program_type_name',
                'sa.session_attendance_id','sa.notes','sa.attendance_type','sa.weight_log_id','sa.unlock_content_date','sa.group_session_id',

                $this->connection->raw('IF(DAYNAME(registration_date) = "Sunday" , registration_date , date(registration_date + INTERVAL 6 - weekday(registration_date) DAY)) as member_start_date'),
                $this->connection->raw('FLOOR(DATEDIFF(CURDATE(), IF(DAYNAME(registration_date) = "Sunday" , registration_date , date(registration_date + INTERVAL 6 - weekday(registration_date) DAY))) / 7) as member_completed_week'),
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
                // total logs

                $this->connection->raw('count(
					DISTINCT IF(l.log_date_time >= "'.$start->format( 'Y-m-d H:i:s' ).'", l.log_id, null)
				  ) as total_logs'),
                $this->connection->raw( 'count(
					DISTINCT IF(l.log_date_time >= "'.$start->format( 'Y-m-d H:i:s' ).'", DATE(l.log_date_time), null)
				) as days_logged' ),

                't.starting_weight',
                't.target_weight',
                // This week manual Activity Minutes
                $this->connection->raw( 'SUM(
					IF(
						(l.log_type = "ActivityMinute" AND l.log_date_time >= "'.$start->format( 'Y-m-d H:i:s' ).'"),
						al.minutes_performed, null
					)
				) as manual_activity_minutes' ),

                // Prior week manual Activity Minutes
                $this->connection->raw( 'SUM(
					IF(
						(l.log_type = "ActivityMinute"
						AND l.log_date_time >= "'.$prior_week_start->format( 'Y-m-d H:i:s' ).'"
				        AND l.log_date_time < "'.$start->format( 'Y-m-d H:i:s' ).'"),
						al.minutes_performed, null
					)
				) as prior_week_manual_activity_minutes' ),
            ])
            ->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
            ->leftJoin( 'targets AS t', 't.patient_id', '=', 'p.patient_id' )
            ->leftJoin( 'log AS l', function ( $q ) use ( $prior_week_start, $end ){
                $q->on( 'l.patient_id', '=', 'p.patient_id' );
                $q->where( 'l.log_date_time', '>=', $prior_week_start->format( 'Y-m-d H:i:s' ) );
                $q->where( 'l.log_date_time', '<', $end->format( 'Y-m-d H:i:s' ) );
            } )
            ->leftJoin( 'activity_log AS al', 'al.log_id', '=', 'l.log_id' )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( 'curriculum_program_type AS cpt', 'cpt.curriculum_program_type_id', '=', 'dt.curriculum_program_type_id' )
            ->leftJoin( "$this->group_db.group_member AS gm", 'gm.user_id', '=', 'u.user_id' )
            //->leftJoin( "$this->group_db.session_attendance AS sa", 'sa.group_member_id', '=', 'gm.group_member_id' )
            ->leftJoin( "$this->group_db.session_attendance AS sa", function ( $q ) use ( $group_session_id ){
                $q->on( 'sa.group_member_id', '=', 'gm.group_member_id' );
              if($group_session_id)
                  $q->where( 'sa.group_session_id', '=', $group_session_id );
            } )
            ->whereIn( 'p.patient_id', $patients )
            ->groupBy( 'p.patient_id' )
            ->get();

          return $query;
    }


    /**
     * group_report_for_duration for inperson
     *
     * @param $facility_id
     * @param null|Carbon $start
     * @param null|Carbon $end
     * @param null $lead_coach
     *
     * @return array|static[]
     */
    public function get_group_report_for_duration_for_inperson(array $patients, Carbon $start, Carbon $end, Carbon $prior_week_start, Carbon $prior_week_end,$group_session_id=0,  $session_start_date,  $session_end_date){

        $query =   $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id',
                'dt.name AS diabetes_type_name',
                'p.diabetes_type_id',
                'p.uuid','p.image_path as patient_profile_image',
                'cpt.name as curriculum_program_type_name',
                'sa.session_attendance_id','sa.notes','sa.attendance_type','sa.weight_log_id','sa.unlock_content_date','sa.group_session_id',

                $this->connection->raw('IF(DAYNAME(registration_date) = "Sunday" , registration_date , date(registration_date + INTERVAL 6 - weekday(registration_date) DAY)) as member_start_date'),
                $this->connection->raw('FLOOR(DATEDIFF(CURDATE(), IF(DAYNAME(registration_date) = "Sunday" , registration_date , date(registration_date + INTERVAL 6 - weekday(registration_date) DAY))) / 7) as member_completed_week'),
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
                // total logs

                $this->connection->raw('count(
					DISTINCT IF(l.log_date_time!="", l.log_id, null)
				  ) as total_logs'),
                $this->connection->raw( 'count(
					DISTINCT IF(l.log_date_time!="", DATE(l.log_date_time), null)
				) as days_logged' ),

                't.starting_weight',
                't.target_weight',
                // This week manual Activity Minutes
                $this->connection->raw( 'SUM(
					IF(
						(l.log_type = "ActivityMinute" AND l.log_date_time >= "'.$start->format( 'Y-m-d H:i:s' ).'"),
						al.minutes_performed, null
					)
				) as manual_activity_minutes' ),

                // Prior week manual Activity Minutes
                $this->connection->raw( 'SUM(
					IF(
						(l.log_type = "ActivityMinute"
						AND l.log_date_time >= "'.$prior_week_start->format( 'Y-m-d H:i:s' ).'"
				        AND l.log_date_time < "'.$start->format( 'Y-m-d H:i:s' ).'"),
						al.minutes_performed, null
					)
				) as prior_week_manual_activity_minutes' ),
            ])
            ->leftJoin( 'diabetes_type AS dt', 'dt.diabetes_type_id', '=', 'p.diabetes_type_id' )
            ->leftJoin( 'targets AS t', 't.patient_id', '=', 'p.patient_id' )
            ->leftJoin( 'log AS l', function ( $q ) use ( $session_start_date, $session_end_date ){
                $q->on( 'l.patient_id', '=', 'p.patient_id' );
                $q->where( 'l.log_date_time', '>', $session_start_date );
                $q->where( 'l.log_date_time', '<=', $session_end_date );

            } )
            ->leftJoin( 'activity_log AS al', 'al.log_id', '=', 'l.log_id' )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( 'curriculum_program_type AS cpt', 'cpt.curriculum_program_type_id', '=', 'dt.curriculum_program_type_id' )
            ->leftJoin( "$this->group_db.group_member AS gm", 'gm.user_id', '=', 'u.user_id' )
            //->leftJoin( "$this->group_db.session_attendance AS sa", 'sa.group_member_id', '=', 'gm.group_member_id' )
            ->leftJoin( "$this->group_db.session_attendance AS sa", function ( $q ) use ( $group_session_id ){
                $q->on( 'sa.group_member_id', '=', 'gm.group_member_id' );
                if($group_session_id)
                    $q->where( 'sa.group_session_id', '=', $group_session_id );
            } )
            ->whereIn( 'p.patient_id', $patients )
            ->whereRaw($this->connection->raw(' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL)'))
            ->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL)'))
            ->groupBy( 'p.patient_id' )
            ->get();

        return $query;
    }






    /**
     * get_group_report_for_duration_weight_details
     *
     * @param $facility_id
     * @param null|Carbon $start
     * @param null|Carbon $end
     * @param null $lead_coach
     *
     * @return array|static[]
     */
    public function get_group_report_for_duration_weight_details(array $patients, Carbon $start, Carbon $end, Carbon $prior_week_start, Carbon $prior_week_end){
         $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id',

                // this_week_weight
                $this->connection->raw('SUBSTRING_INDEX(
				    GROUP_CONCAT(
				        IF(
					        (l.log_type = "Weight" AND l.log_date_time >= "'.$start->format( 'Y-m-d H:i:s' ).'"),
					        CAST(ml.weight AS CHAR), NULL
				        ) ORDER BY l.log_time DESC ), ",", 1
				  ) as current_weight'),
                // prior_week_weight
                $this->connection->raw('SUBSTRING_INDEX(
				    GROUP_CONCAT(
				        IF(
					        (l.log_type = "Weight"
					        AND l.log_date_time >= "'.$prior_week_start->format( 'Y-m-d H:i:s' ).'"
					        AND l.log_date_time < "'.$start->format( 'Y-m-d H:i:s' ).'"),
					        CAST(ml.weight AS CHAR), NULL
				        ) ORDER BY l.log_time DESC ), ",", 1
				  ) as prior_week_weight'),

            ])
            ->join( 'log AS l', function ( $q ) use ( $prior_week_start, $end ){
                $q->on( 'l.patient_id', '=', 'p.patient_id' );
                $q->where( 'l.log_date_time', '>=', $prior_week_start->format( 'Y-m-d H:i:s' ) );
                $q->where( 'l.log_date_time', '<', $end->format( 'Y-m-d H:i:s' ) );

            } )

            ->join( 'misc_log AS ml', 'ml.log_id', '=', 'l.log_id' )
            ->whereIn( 'p.patient_id', $patients );

         $query->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL)'));

        return $query->groupBy( 'p.patient_id' )->get();
    }


    /**
     * get_group_report_for_duration_weight_details for inperson
     *
     * @param $facility_id
     * @param null|Carbon $start
     * @param null|Carbon $end
     * @param null $lead_coach
     *
     * @return array|static[]
     */
    public function get_group_report_for_duration_weight_details_for_inperson(array $patients, $group_session_id,$array_group_session_id){
        $query = $this->group_connection
            ->table('session_attendance AS sa')
            ->select( [
                'sa.session_attendance_id','ml.weight','p.patient_id','sa.group_member_id','sa.activity_minutes','sa.group_session_id','gm.user_id',
            ])
            ->join( 'group_session AS gs', 'gs.group_session_id', '=', 'sa.group_session_id')
            ->join( 'group_member AS gm', 'gm.group_member_id', '=', 'sa.group_member_id')
            ->join( "$this->patient_db.patient AS p", 'gm.user_id', '=', 'p.user_id')
            ->leftJoin( "$this->patient_db.misc_log AS ml", 'ml.log_id', '=', 'sa.weight_log_id')

            ->whereIn( 'sa.group_session_id', $array_group_session_id);

       // return $query->groupBy( 'p.patient_id' )->get();
        return $query->get();
    }

    /**
     * @param array $patients
     * @param Carbon $start
     * @param Carbon $end
     *
     * @return array|static[]
     */
    public function get_patients_synced_minutes( array $patients, Carbon $start, Carbon $end ) {

        $start_date = $start->timestamp * 1000;

        $statement = $this->connection
            ->table( 'activity_log_summary AS m' )
            ->select( [
                'm.patient_id',
                $this->connection->raw( 'FLOOR(SUM(m.steps)) AS total_activity_minutes_synced' ),
            ] )
            ->leftJoin( 'patient AS p', 'p.patient_id', '=', 'm.patient_id' )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->whereIn( 'm.patient_id', $patients )
            ->whereIn( 'm.attribute_type', [ 'minutesFairlyActive', 'minutesVeryActive' ] );

        if ( ! empty( $start ) && ! empty( $end ) ) {
            $statement->whereRaw( $this->connection->raw( 'm.timestamp >= IF( '.$start_date.' < (UNIX_TIMESTAMP(password_created_date) * 1000) , (UNIX_TIMESTAMP(password_created_date) * 1000), '.$start_date.' )' ) );
            $statement->where( 'm.timestamp', '<', $end->timestamp * 1000 );
        }

        return $statement->groupBy( 'm.patient_id' )->get();
    }




}//End of Class


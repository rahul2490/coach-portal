<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model {

	protected $table = 'patient';

	protected $primaryKey = 'patient_id';

	public $timestamps = false;

	/**
	 * Filter Deleted Patients
	 *
	 * @param $query
	 * @param bool $is_deleted
	 *
	 * @return mixed
	 */
	public function scopeIsDeleted( $query, $is_deleted = true ) {
		if ( $is_deleted ) {
			return $query->whereRaw( $this->getConnection()->raw( 'ORD(is_deleted) = 1' ) );
		}

		return $query->whereRaw( $this->getConnection()->raw( '(ORD(is_deleted) = 0 OR is_deleted IS NULL)' ) );
	}

	public function user() {
		return $this->belongsTo( 'HealthSlatePortal\Models\Eloquent\User', 'user_id' );
	}
}

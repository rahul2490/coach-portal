<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class FacilityProvider extends Model {

    protected $table = 'facility_provider';

    protected $primaryKey = 'facility_provider_id';

    public $timestamps = false;
}

<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class MessageTag extends Model {

    protected $table = 'message_tag';

	protected $primaryKey = 'message_tag_id';

	public $timestamps = false;

}

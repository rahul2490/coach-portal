<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class PatientMotivationImage extends Model {

    protected $table = 'patient_motivation_image';

    protected $primaryKey = 'patient_motivation_id';

    protected $fillable = ['image_name', 'patient_id', 'description', 'is_public' ];

    public $timestamps = false;
}

<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model {

    protected $table = 'user_roles';

    protected $primaryKey = 'user_role_id';

    public $timestamps = false;
}

<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model {

    protected $table = 'group_user';

	protected $primaryKey = 'group_user_id';

	public $timestamps = false;

}

<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class PatientHideUnhide extends Model {

    protected $table = 'patient_hide_unhide_pin_unpin_status';

	protected $primaryKey = 'patient_action_status_id';

    protected $fillable = ['user_id', 'patient_id', 'hide_unhide', 'pin_unpin' , 'date'];

	public $timestamps = false;

}

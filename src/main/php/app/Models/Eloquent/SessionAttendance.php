<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class SessionAttendance extends Model {

    protected $connection = 'groups';

    protected $table = 'session_attendance';

	protected $primaryKey = 'session_attendance_id';

	public $timestamps = false;

}

<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class MisfitActivityLog extends Model {

    protected $table = 'misfit_activity_log';

    protected $primaryKey = 'activity_log_id';

    public $timestamps = false;

}

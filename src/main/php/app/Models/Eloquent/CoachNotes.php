<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class CoachNotes extends Model {

    protected $table = 'coach_notes';

    protected $primaryKey = 'coach_notes_id';

    public $timestamps = false;
}

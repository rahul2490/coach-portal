<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model {

    protected $connection = 'groups';

    protected $table = 'groups';

	protected $primaryKey = 'group_id';

	public $timestamps = false;

}

<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class PatientDiabetesInfo extends Model {

    protected $table = 'patient_diabetes_info';

    protected $primaryKey = 'diabetes_info_id';

    public $timestamps = false;

    /**
     * Filter Deleted Patients
     *
     * @param $query
     * @param bool $is_deleted
     *
     * @return mixed
     */

}

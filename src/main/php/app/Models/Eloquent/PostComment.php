<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Model {

    protected $connection = 'groups';

    protected $table = 'post_comment';

    protected $primaryKey = 'post_comment_id';

    public $timestamps = false;
}

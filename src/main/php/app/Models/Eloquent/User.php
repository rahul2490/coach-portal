<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

	protected $table = 'users';

	protected $primaryKey = 'user_id';

	public $timestamps = false;

	protected $fillable = [ 'first_name', 'last_name' ];

	public function getFullNameAttribute() {
		return $this->first_name . ' ' . $this->last_name;
	}

	public function getAddressInfoAttribute() {
		return $this->address . ' ' . $this->city. ' ' . $this->state;
	}

	public function scopeIsRegistrationCompleted( $query, $is_registration_completed = true ){
		if($is_registration_completed){
			// registered 1
			return $query->whereRaw($this->getConnection()->raw('ORD(is_registration_completed) = 1'));
		}else{
			// not yet registered (null,0)
			return $query->whereRaw($this->getConnection()->raw('( (is_registration_completed IS NULL) OR ORD(is_registration_completed) = 0 )'));
		}
	}

	// a user will have either patient or provider
	public function provider() {
		//return $this->hasOne( 'HealthSlatePortal\Models\Eloquent\Provider', 'user_id', 'user_id' );
	}

}
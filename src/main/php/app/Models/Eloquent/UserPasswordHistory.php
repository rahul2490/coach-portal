<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class UserPasswordHistory extends Model {

    protected $table = 'user_password_history';

    protected $primaryKey = 'user_password_history_id';

    public $timestamps = false;
}

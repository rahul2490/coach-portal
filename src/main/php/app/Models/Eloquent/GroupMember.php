<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class GroupMember extends Model {

    protected $connection = 'groups';

    protected $table = 'group_member';

	protected $primaryKey = 'group_member_id';

	public $timestamps = false;

}

<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class UserMessagePreference extends Model {

    protected $table = 'user_messaging_preference';

    protected $primaryKey = 'user_messaging_preference_id';

    public $timestamps = false;

    /**
     * Filter Deleted Patients
     *
     * @param $query
     * @param bool $is_deleted
     *
     * @return mixed
     */

}

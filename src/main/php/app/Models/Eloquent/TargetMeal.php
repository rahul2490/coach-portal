<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class TargetMeal extends Model {

    protected $table = 'target_meal';

    protected $primaryKey = 'meal_target_id';

    public $timestamps = false;

    /**
     * Filter Deleted Patients
     *
     * @param $query
     * @param bool $is_deleted
     *
     * @return mixed
     */

}

<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Target extends Model {

    protected $table = 'targets';

    protected $primaryKey = 'target_id';

    public $timestamps = false;

    /**
     * Filter Deleted Patients
     *
     * @param $query
     * @param bool $is_deleted
     *
     * @return mixed
     */

}

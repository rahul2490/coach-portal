<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class PredefinedMessageModel extends Model {

    protected $table = 'predefined_message';

    protected $primaryKey = 'predefined_message_id';

    public $timestamps = false;

}

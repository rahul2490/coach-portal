<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Facility extends Model {

    protected $table = 'facility';

    protected $primaryKey = 'facility_id';

    public $timestamps = false;
}

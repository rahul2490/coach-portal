<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

    protected $connection = 'groups';

    protected $table = 'post';

    protected $primaryKey = 'post_id';

    public $timestamps = false;
}

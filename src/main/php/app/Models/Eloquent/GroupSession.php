<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class GroupSession extends Model {

    protected $connection = 'groups';

    protected $table = 'group_session';

	protected $primaryKey = 'group_session_id';

	public $timestamps = false;

}

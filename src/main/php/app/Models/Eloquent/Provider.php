<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Provider extends Model {

	protected $table = 'provider';

	protected $primaryKey = 'provider_id';

	public $timestamps = false;

}

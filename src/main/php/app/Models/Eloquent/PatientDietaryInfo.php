<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class PatientDietaryInfo extends Model {

    protected $table = 'patient_dietary_info';

    protected $primaryKey = 'dietary_info_id';

    public $timestamps = false;

    /**
     * Filter Deleted Patients
     *
     * @param $query
     * @param bool $is_deleted
     *
     * @return mixed
     */

}

<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class ActivityLogSummary extends Model {

    protected $table = 'activity_log_summary';

    protected $primaryKey = 'activity_log_summary_id';

    public $timestamps = false;

}

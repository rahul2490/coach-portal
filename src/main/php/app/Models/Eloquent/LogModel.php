<?php

namespace HealthSlatePortal\Models\Eloquent;

use Illuminate\Database\Eloquent\Model;

class LogModel extends Model {

    protected $table = 'log';

   // protected $primaryKey = 'misc_log_id';

    public $timestamps = false;

}

<?php
/**
 * Created by PhpStorm.
 * User: rpatidar
 * Date: 31/08/17
 */

namespace HealthSlatePortal\Models;

use Carbon\Carbon;
use DB;

class HealthSlateModel {

	/**
	 * @var \Illuminate\Database\Connection
	 */
	protected $connection;

	/**
	 *
	 */
	function __construct() {
		$this->connection = DB::connection();
	}

	/**
	 * @param $query
	 * @param array $bindings
	 *
	 * @return array
	 */
	public function get_query_result( $query, $bindings = [ ] ) {
		return $this->connection->select( $query, $bindings );
	}

	/**
	 * get_atoz_preferences
	 *
	 * @param null $preference_name
	 *
	 * @return array|mixed|static|static[]
	 */
	public function get_atoz_preferences($preference_name = null){
		if($preference_name){
			return $this->connection->table('atoz_preferences')->where('preference_name',$preference_name)->first();
		}
		return $this->connection->table('atoz_preferences')->get();
	}

	public function get_food_master( $list = false ) {
		$query = $this->connection->table( 'food_master' )->orderBy( 'foodName' );
		if ( $list ) {
			return $query->lists( 'foodName', 'food_master_id' );
		}

		return $query->get();
	}

	public function get_next_meal_log_id( $meal_log, $status = null ) {
		$query = $this->connection
			->table( 'food_log_summary AS fls' )
			->leftJoin( 'log AS l', function ( $q ) {
				$q->on( 'l.log_id', '=', 'fls.log_id' );
				$q->where( 'l.log_type', '=', 'Meal' );
			} )
			->where( 'fls.food_log_summary_id', '<', $meal_log->food_log_summary_id )
			->whereRaw( $this->connection->raw( ' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) ' ) );
                        
                        return $query->orderBy( 'fls.food_log_summary_id', 'DESC' )->first();
	}

	public function get_prev_meal_log_id( $meal_log, $status = null ) {
		$query = $this->connection
			->table( 'food_log_summary AS fls' )
			->leftJoin( 'log AS l', function ( $q ) {
				$q->on( 'l.log_id', '=', 'fls.log_id' );
				$q->where( 'l.log_type', '=', 'Meal' );
			} )
			->where( 'fls.food_log_summary_id', '>', $meal_log->food_log_summary_id )
			->whereRaw( $this->connection->raw( ' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) ' ) );

		return $query->orderBy( 'fls.food_log_summary_id', 'ASC' )->first();
	}

	public function get_meal_log( $food_log_id = null, $status = null ) {
		$query = $this->connection
			->table( 'food_log_summary AS fls' )
			->leftJoin( 'log AS l', function ( $q ) {
				$q->on( 'l.log_id', '=', 'fls.log_id' );
				$q->where( 'l.log_type', '=', 'Meal' );
			} )
			->leftJoin( 'users AS coach_u', 'l.user_id', '=', 'coach_u.user_id' )
			->select( [
				'fls.*',
				'l.patient_id',
				 $this->connection->raw( 'ORD(l.is_suggested) AS is_suggested'),
				'coach_u.user_id AS provider_user_id',
				$this->connection->raw( 'CONCAT(coach_u.first_name) AS provider_display_name' )
			] )
			->whereRaw( $this->connection->raw( ' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) ' ) );

		if ( ! empty( $food_log_id ) ) {
			$query->where( 'fls.food_log_summary_id', '=', $food_log_id );
		}

		$meal_log = $query->orderBy( 'fls.food_log_summary_id', 'DESC' )->first();

		if ( ! empty( $meal_log ) ) {
			$meal_log->foodLogDetail = collect( $this->connection->table( 'food_log_detail' )
			                                                     ->where( 'food_log_summary_id', '=', $meal_log->food_log_summary_id )
			                                                     ->get() );
		}
		if ( ! empty( $meal_log ) && intval( $meal_log->has_image ) ) {
			$meal_log->foodImage = $this->connection->table( 'food_image AS fi' )
			                                        ->where( 'fi.food_log_summary_id', '=', $meal_log->food_log_summary_id )
			                                        ->first();
		}

		return $meal_log;
	}

	public function get_patient_detail( $patient_ids, $single = true ) {
		$result = $this->connection
			->table( 'patient AS p' )
			->leftJoin( 'users AS u', 'p.user_id', '=', 'u.user_id' )
			->leftJoin( 'diabetes_type AS dt', 'p.diabetes_type_id', '=', 'dt.diabetes_type_id' )
			->whereIn( 'p.patient_id', $patient_ids )
			->select( [
				'u.*',
				'p.patient_id',
				'p.mrn',
				'p.diabetes_type_id',
				'dt.name AS diabetes_type_name',
				'p.lead_coach_id',
				'p.is_deleted',
                                $this->connection->raw('CEIL( DATEDIFF( NOW() , u.password_created_date ) /7 ) as week_in'),
			] )
			->get();
		if ( $single ) {
			return ! empty( $result ) ? $result[0] : null;
		}

		return $result;
	}

	public function get_suggested_meal_for_food_ids( $food_master_ids, $food_log_summary_id, $offset = 0, $limit = 100 ) {
		$ids = explode( ',', $food_master_ids );

		$query = $this->connection->table( 'food_log_detail AS fld' )
		                          ->leftJoin( 'food_log_summary AS fls', 'fls.food_log_summary_id', '=', 'fld.food_log_summary_id' )
		                          ->leftJoin( 'food_image AS fi', 'fi.food_log_summary_id', '=', 'fls.food_log_summary_id' )
		                          ->whereIn( 'fld.food_master_id', $ids )
		                          ->groupBy( 'fld.food_log_summary_id' )
		                          ->having( $this->connection->raw( 'count(*)' ), '>=', count( $ids ) )
		                          ->select( [ 'fls.*', 'fld.food_master_id', 'fi.image_name' ] );
		if ( ! empty( $food_log_summary_id ) ) {
			$query->whereNotIn( 'fls.food_log_summary_id', explode( ',', $food_log_summary_id ) );
		}

		return $query->orderBy( 'fls.timestamp', 'DESC' )
		             ->skip( $offset )->limit( $limit )->get();
	}

	/**
	 * @param $food_log_summary_id
	 * @param $patient_id
	 * @param null $status
	 * @param int $offset
	 * @param int $limit
	 *
	 * @return array|static[]
	 */
	public function get_previous_meal_of_patient($food_log_summary_id, $patient_id, $status = null, $offset = 0, $limit = 100 ) {
		$query = $this->connection
			->table( 'food_log_summary AS fls' )
			->select( [
				'fls.*',
				'l.patient_id',
				'fi.image_name'
			] )
			->leftJoin( 'log AS l', function ( $q ) {
				$q->on( 'l.log_id', '=', 'fls.log_id' );
				$q->where( 'l.log_type', '=', 'Meal' );
			} )
			->leftJoin( 'food_image AS fi', 'fi.food_log_summary_id', '=', 'fls.food_log_summary_id' )
			->where( 'fls.food_log_summary_id', '<', $food_log_summary_id )
			->whereRaw( $this->connection->raw( ' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) ' ) )
			->where( 'l.patient_id', '=', $patient_id );

		return $query->orderBy( 'fls.food_log_summary_id', 'DESC' )->skip( $offset )->limit( $limit )->get();
	}
}
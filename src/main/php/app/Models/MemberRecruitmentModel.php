<?php

/**
 * Created by PhpStorm.
 * User: rpatidar
 * Date: 31/08/17
 */

namespace HealthSlatePortal\Models;

use Carbon\Carbon;
use DB;
use Session;
class MemberRecruitmentModel {

    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;

    /**
     * it's constructor
     */
    function __construct() {
        $this->connection = DB::connection();
    }


    /**
     * get prospective memberlist function
     * (it's give list of prospective member)
     */


    public function get_prospective_member_list($facility_id = 0) {
        /**
         * get_facility_patients
         *
         * @param $facility_id
         * @param null $statusFilter
         * @param int $daysFilter
         *
         * @return array|static[]
         */
          //it's give data from user and patient table

        //echo  Session::get('userRole'); die;
        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( [
                'p.patient_id','p.mrn','p.is_invited','u.user_id as u_id','p.is_consent_accepted','u.phone','u.first_name','u.last_name','u.email','u.registration_date','p.patient_status','f.name as facility_name',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name'),
                $this->connection->raw( 'from_unixtime(p.invitation_sent_date/1000) AS invitation_sent_date'),
            ] )
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( 'facility AS f', 'f.facility_id', '=', 'p.facility_id' );

        if(Session::get('userRole')!="admin")
        {
            $query->where( 'p.facility_id', '=', $facility_id );
        }

        return $query
            ->where( 'u.email', 'not like', '\\_%' )
            ->where('u.cobrand_id', '=', session('user')->cobrandId)
            ->whereNotNull( 'p.invitation_sent_date')
            ->whereRaw($this->connection->raw('(ORD(u.is_registration_completed) = 0)'))
            ->whereRaw($this->connection->raw('(ORD(p.is_deleted) = 0 OR p.is_deleted IS NULL)'))
            ->whereRaw($this->connection->raw('(ORD(p.is_archived) = 0 OR p.is_archived IS NULL)'))
            ->orderBy( 'p.patient_id' )
            ->get();
    }



}

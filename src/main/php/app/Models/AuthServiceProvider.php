<?php
/**
 * User: rpatidar
 * Date: 31/08/2017
 */

namespace HealthSlatePortal\Models;

use HealthSlatePortal\Helpers\RequestHelper;
use Cookie;
use Log;


class AuthServiceProvider {

	/**
	 * @var RequestHelper
	 */
	protected $requestHelper;

	/**
	 * @param RequestHelper $requestHelper
	 */
	public function __construct( RequestHelper $requestHelper ) {
		$this->requestHelper = $requestHelper;
	}

	/**
	 * @param $username
	 * @param $password
	 *
	 * @return object
	 */
	public function getAccessToken( $username, $password ) {
        $cobrandId = get_cobrand_id();
		$url = config( 'rest.POST_get_auth_token' );

		$response = $this->requestHelper->makeRequest( 'POST', $url, [
			'username' => $username,
			'password' => $password
		],['COBRAND_ID'=>$cobrandId] );

		if ( $response->statusCode === 401 ) {
			// Bad credentials
			return $response->body;
		} else if ( $response->statusCode === 200 && empty( $response->body->DATA ) ) {
			// Invalid username/password
			$response->body->errorDetail = ! empty( $response->body->REASON ) ? $response->body->REASON : trans( 'auth.failed' );

			return $response->body;
		} else if ( $response->statusCode === 200 && ! empty( $response->body->DATA ) && ! empty( $response->body->DATA->TOKEN_INFO ) && ! empty( $response->body->DATA->USER_ROLES ) ) {
			//  No need to add this because we are allowing tech support also.
			// ! $this->hasCoachRole(@$response->body->DATA->TOKEN_INFO->userRole)

			if ( ! $this->hasAllowedUserRole( $response->body->DATA->USER_ROLES ) ) {
				logger('User Role or Role list does not have valid role info');
				return (object) [ 'errorDetail' => trans( 'auth.invalid_user_role' ) ];
			}

			$response->body->DATA->TOKEN_INFO->now = time();
			//'{"accessToken":"88222238-7364-4271-8f86-89c97c91c70b","refreshToken":"963fd6b-d2aa-4e3f-a6d2-75da52deb72e","firstName":"Jared","lastName":"Saul","userName":"jaredsaul+DPP@healthslate.com","userRole":"ROLE_FACILITY_ADMIN","expiresIn":2544400,"now":1460461295}';
			return $response->body->DATA->TOKEN_INFO;
		} else {
			//Log::error( $response->getBody() );
		}

		return (object) [ 'errorDetail' => trans( 'common.internal_server_error' ) ];

	}

	/**
	 * @param $user
	 *
	 * @return bool
	 */
	public function isInvalidToken( $user ) {
		if ( empty( $user->accessToken ) ) {
			return true;
		}
		$url      = config( 'rest.GET_validate_token' );
		$response = $this->requestHelper->makeRequest( 'GET', $url, [ ], [
			'Authorization' => 'Bearer ' . $user->accessToken
		] );
		if ( $response->statusCode === 200 ) {
			return false;
		}

		return true;
	}

	/**
	 * @param $user
	 *
	 * @return bool
	 */
	public function refreshToken( $user ) {
		if ( empty( $user->refreshToken ) ) {
			return false;
		}
		$url      = config( 'rest.GET_get_refresh_token' );
		$response = $this->requestHelper->makeRequest( 'GET', $url, [
			'refreshToken' => $user->refreshToken
		] );

		if ( $response->statusCode === 200 && ! empty( $response->body->DATA ) && ! empty( $response->body->DATA->TOKEN_INFO ) ) {

			$user->accessToken  = $response->body->DATA->TOKEN_INFO->accessToken;
			$user->refreshToken = $response->body->DATA->TOKEN_INFO->refreshToken;
			$user->expiresIn    = $response->body->DATA->TOKEN_INFO->expiresIn;
			$user->now          = time();
			session( [ 'user' => $user ] );
                        Cookie::queue(Cookie::make('access_token', $user->accessToken, $user->expiresIn));
                        Cookie::queue(Cookie::make('expires_in', $user->expiresIn, $user->expiresIn));
			Log::info( "User token refreshed successfully" );

			return true;
		}

		// Token Not refreshed
		return false;
	}

	/**
	 * @param $accessToken
	 *
	 * @return bool
	 */
	public function getUserInfoByToken( $accessToken ) {
		$url = config( 'rest.GET_user_info_by_token' );

		$response = $this->requestHelper->makeRequest( 'GET', $url, [ ], [
			'Authorization' => 'Bearer ' . $accessToken
		] );

		if ( $response->statusCode === 200 && ! empty( $response->body ) ) {
			return ( $response->body );
		}

		return false;
	}

	/**
	 * get Admin user TokenInfo object by user token
	 *
	 * @param $accessToken
	 * @param null $refreshToken
	 * @param int $expiresIn
	 *
	 * @return bool|object
	 */
	public function getAdminUserByToken( $accessToken, $refreshToken = null, $expiresIn = - 1 ) {

		$userInfo = $this->getUserInfoByToken( $accessToken );
		if ( $userInfo && ! empty( $userInfo->userRole ) && ! empty( $userInfo->user ) ) {
			if ( $allowed_role = $this->hasAllowedUserRole( $userInfo->userRole ) ) {
				info( 'Allowed Roles are: ' . json_encode( $allowed_role ) );
				$token = [
					"accessToken"  => $accessToken,
					"refreshToken" => $refreshToken,
					"firstName"    => @$userInfo->user->firstName,
					"lastName"     => @$userInfo->user->lastName,
					"userName"     => $userInfo->user->email,
                    "cobrandId"    => 1,
					"userRole"     => !empty($allowed_role->authority) ? $allowed_role->authority : null,
					"expiresIn"    => intval( $expiresIn ), //    -1 will force Refresh token call just after this login.
					"now"          => time()
				];
                info( 'User Data: ' . json_encode( $token ) );
				return (object) $token;
			} else {
				return (object) [ 'errorDetail' => trans( 'auth.invalid_user' ) ];
			}
		}

		return false;
	}

	/**
	 * hasAllowedUserRole
	 *
	 * @param $userRole
	 * @param array $role_name
	 *
	 * @return bool|string
	 */
	private function hasAllowedUserRole( $userRole, $role_name = [ 'ROLE_FACILITY_ADMIN', 'ROLE_PROVIDER', 'ROLE_ADMIN'] ) {
		$filtered = collect( $userRole )->filter( function ( $item ) use ( $role_name ) {
			return in_array( $item->authority, $role_name );
		} );
		if ( $filtered->count() ) {
			return $filtered->last();
		}

		return false;
	}

	/**
	 * @param string $userRole
	 * @param array $possible_coach
	 *
	 * @return bool
	 */
    /*private function hasCoachRole( $userRole, $possible_coach = [ 'ROLE_FACILITY_ADMIN', 'ROLE_PROVIDER' ] ) {
        return in_array( $userRole, $possible_coach );
    }*/
}
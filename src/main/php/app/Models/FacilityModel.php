<?php

/**
 * Created by PhpStorm.
 * User: rpatidar
 * Date: 31/08/17
 */

namespace HealthSlatePortal\Models;

use Carbon\Carbon;
use DB;

class FacilityModel {

    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;

    /**
     *
     */
    function __construct() {
        $this->connection = DB::connection();
    }


    public function get_admin_facility_list($email, $cobrand_id) {
        return $this->connection
            ->table('users AS u')
            ->select([
                'pr.provider_id',
                'f.name',
                'f.facility_id'
            ])
            ->join('provider AS pr', 'pr.user_id', '=', 'u.user_id')
            ->join('facility_provider AS fp', 'pr.provider_id', '=', 'fp.providers_provider_id')
            ->join('facility AS f', 'f.facility_id', '=', 'fp.facility_facility_id')
            ->where('u.email', '=', $email)
            ->where('u.cobrand_id', '=', $cobrand_id)
            ->orderBy('f.name','asc')
            ->get();
    }

    public function get_all_facility_list($cobrand_id) {
        return $this->connection
            ->table('facility AS f')
            ->select([
                'f.facility_id',
                'f.name',
                'f.state',
                'f.city',
                'f.zip',
                'f.address',
                'f.timezone_name',
                'f.timezone_offset',
                'f.contact_info',
                'f.contact_person_name',
                'f.is_notification_enabled',
                'f.is_skip_consent',
                'f.partner_id',
                $this->connection->raw( 'group_concat(u.first_name SEPARATOR ", ") as user_name'),
               // $this->connection->raw('fp.providers_provider_id') as user_id,
                $this->connection->raw( 'group_concat(u.email SEPARATOR ", ") as email'),
               //$this->connection->raw('u.email'),
            ])
            ->leftjoin('facility_provider AS fp','fp.facility_facility_id','=','f.facility_id')
            ->leftjoin('provider AS pr', 'pr.provider_id', '=', 'fp.providers_provider_id')
            ->leftjoin('users AS u', 'u.user_id', '=', 'pr.user_id')
            ->where('f.cobrand_id', '=', $cobrand_id)
            ->groupBy('f.facility_id')
            ->get();
    }



    public function get_facility_detail($facility_id) {
        return $this->connection
            ->table('facility AS f')
            ->select([
                'f.facility_id',
                'f.name',
                'f.state',
                'f.city',
                'f.zip',
                'f.address',
                'f.timezone_name',
                'f.timezone_offset',
                'f.contact_info',
                'f.contact_person_name',
                'f.is_notification_enabled',
                'f.is_skip_consent',
                'f.partner_id',
                'f.cdc_organization_code',
                'f.is_chatbot_enabled'
            ])
            ->where('f.facility_id', '=', $facility_id)
            ->first();
    }


    public function get_user_password_history($user_id, $date) {
        return $this->connection
            ->table('user_password_history AS u')
            ->select([
                'u.*',
            ])
            ->where('u.user_id', '=', $user_id)
            ->where('u.timestamp', '>', $date)
            //->where('u.timestamp', '<>', '0000-00-00 00:00:00')
            //->whereNotNull('u.timestamp')
            ->get();
    }

    public function get_user_info($userEmail) {
        return $this->connection
            ->table('users AS u')
            ->select([
                'u.*',
                'pr.provider_id',
                'pr.type',
                'pr.image_path',
                'pr.facility_id',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
            ])
            ->leftJoin('provider AS pr', 'pr.user_id', '=', 'u.user_id')
            ->where('u.email', '=', $userEmail)
            ->where('u.user_type', '<>', 'PATIENT')
            ->first();
    }



    /**
     * get  cobrand all coach list function
     * it's show  all coach brand list
     */

    public function get_cobrand_all_facility_admin_list($facility_id = 0) {
        return $this->connection
            ->table('users AS u')
            ->select([
                'u.user_id','u.first_name','u.last_name','u.user_type',
                'pr.provider_id',
                'fp.providers_provider_id',
                'fp.facility_facility_id',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' )
            ])
            ->leftJoin('provider AS pr', 'pr.user_id', '=', 'u.user_id')
            ->leftJoin('facility_provider AS fp', 'pr.provider_id', '=', 'fp.providers_provider_id')
            ->whereRaw($this->connection->raw('(ORD(pr.is_deleted) = 0 OR pr.is_deleted IS NULL)'))
            ->whereIn('u.user_type', ['ROLE_FACILITY_ADMIN'])
            ->where( 'u.email', 'not like', '\\_%' )
            ->where('u.cobrand_id', '=', session('user')->cobrandId)
            ->orderBy('u.first_name')
            ->groupBy('u.user_id')
            ->get();
    }



    /**
     * get  cobrand all coach list function
     * it's show  all coach brand list
     */

    public function get_facility_admin_list($facility_id = 0) {
        return $this->connection
            ->table('users AS u')
            ->select([
                'u.user_id','u.first_name','u.last_name','u.user_type','u.email',
                'pr.provider_id',
                'fp.providers_provider_id',
                'fp.facility_facility_id',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' )
            ])
            ->join('provider AS pr', 'pr.user_id', '=', 'u.user_id')
            ->leftJoin('facility_provider AS fp', 'pr.provider_id', '=', 'fp.providers_provider_id')
            ->whereRaw($this->connection->raw('(ORD(pr.is_deleted) = 0 OR pr.is_deleted IS NULL)'))
            ->whereIn('u.user_type', ['ROLE_FACILITY_ADMIN'])
            ->where( 'u.email', 'not like', '\\_%' )
            ->where('u.cobrand_id', '=', session('user')->cobrandId)
            ->where('fp.facility_facility_id', '=', $facility_id)
            ->groupBy('u.user_id')
            ->get();
    }

    /**
     * @param $userEmail
     * @param $logged_meal_start_date
     * @param $logged_meal_end_date
     * @param $report_sent_start_date
     * @param $report_sent_end_date
     *
     * @return array|static[]
     */
    public function get_patients_of_coach($userEmail, $logged_meal_start_date = null, $logged_meal_end_date = null, $report_sent_start_date = null, $report_sent_end_date = null) {
        $user = $this->get_user_info($userEmail);

        // user email not found or user is not a provider type
        if (empty($user) || empty($user->provider_id)) {
            return [];
        }
        $user_provider_id = $user->provider_id;
        $user_facility_id = $user->facility_id;
        info('Looking up for patients of provider_id: ' . $user_provider_id . ' type: ' . $user->type);

        $query = $this->connection
                ->table('patient AS p')
                ->select([
                    'p.*',
                    'u.*',
                    'd.device_id', 'd.device_type', 'd.device_app_name', 'd.device_build_type',
                    $this->connection->raw('CEIL( DATEDIFF( NOW() , u.password_created_date ) /7 ) as week_in'),
                    'wmr.weekly_meal_review_id',
                    $this->connection->raw('IF(wmr.is_sent = 1, wmr.last_updated, NULL ) as sent_time'),
                    $this->connection->raw('IF(wmr.is_sent = 1, DATEDIFF(NOW(),wmr.last_updated), NULL )as days_since_sent')
                ])
                ->leftJoin('users AS u', 'u.user_id', '=', 'p.user_id')
                ->leftJoin('device AS d', 'd.device_mac_address', '=', 'p.device_mac_address')
                //->leftJoin('patient_facility AS pf', 'p.patient_id', '=', 'pf.patients_patient_id')
                ->leftJoin(
                $this->connection->raw('( select patient_id,
				SUBSTRING_INDEX(group_concat(weekly_meal_review_id order by weekly_meal_review_id desc), ",", 1 ) AS weekly_meal_review_id,
				SUBSTRING_INDEX(group_concat(is_sent order by weekly_meal_review_id desc), ",", 1 ) AS is_sent,
				SUBSTRING_INDEX(group_concat(last_updated order by weekly_meal_review_id desc), ",", 1 ) AS last_updated
				from weekly_meal_review
				where is_sent = 1
				group by patient_id ) AS wmr '), 'p.patient_id', '=', 'wmr.patient_id');

        if (!empty($logged_meal_start_date)) {
            // for meal log info we need to join log table too
            // with group by patient id
            $query->leftJoin('log AS l', function($q) {
                $q->on('l.patient_id', '=', 'p.patient_id');
                $q->where('l.log_type', '=', 'Meal');
            });

            // case of never logged patients
            if (empty($logged_meal_end_date)) {
                $query->whereNull('l.log_id');
            } else if (!empty($logged_meal_end_date)) {
                $query->where('l.created_on', '>=', $logged_meal_start_date->timestamp * 1000);
                $query->where('l.created_on', '<', $logged_meal_end_date->timestamp * 1000);
                $query->whereRaw($this->connection->raw(' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL)'));
            }
        }

        if (!empty($report_sent_start_date) && !empty($report_sent_end_date)) {
            $query->where('wmr.is_sent', '=', 1);
            $query->where('wmr.last_updated', '>=', $report_sent_start_date->format('Y-m-d H:i:s'));
            $query->where('wmr.last_updated', '<', $report_sent_end_date->format('Y-m-d H:i:s'));
        }

        if ($user->type == 'Coach') {
            $query->where('p.lead_coach_id', '=', $user_provider_id);
        } else if ($user->type == 'Food Coach') {
            $query->where('p.primary_food_coach_id', '=', $user_provider_id);
        }
        $query->where('u.email', 'not like', '\\_%')
                ->where('p.mrn', 'not like', '%test%')
                ->where('p.mrn', 'not like', 'dis%')
                ->where('p.mrn', 'not like', 'deactive%')
                ->whereRaw($this->connection->raw('ORD(u.is_registration_completed) = 1'));

        return $query->groupBy('p.patient_id')
                        //->orderBy( 'enm.sent_time' )
                        ->get();
        // if not a lead_coach_id ?
    }

    /**
     * @param $patient_ids
     * @param $start
     * @param $end
     * @param bool $single
     *
     * @return static|array|null|static[]
     */
    public function get_total_meals($patient_ids, $start, $end, $single = true) {
        $query = $this->connection
                ->table('food_log_summary AS fls')
                ->select([
                    $this->connection->raw('count(fls.food_log_summary_id) AS total_food_logs'),
                    $this->connection->raw('SUM(fls.carbs) AS total_carbs'),
                    $this->connection->raw('SUM(fls.fats) AS total_fats'),
                    $this->connection->raw('SUM(fls.protein) AS total_protein'),
                    $this->connection->raw('SUM(fls.calories) AS total_calories'),
                    'l.patient_id',
                ])
                ->leftJoin('log AS l', function ( $q ) {
                    $q->on('l.log_id', '=', 'fls.log_id');
                    $q->where('l.log_type', '=', 'Meal');
                })
                ->whereIn('l.patient_id', $patient_ids)
                ->whereRaw($this->connection->raw(' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) '))
                ->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) '))
                ->where('l.created_on', '>=', $start->timestamp * 1000)
                ->where('l.created_on', '<', $end->timestamp * 1000);


        $result = $query->groupBy('l.patient_id')->get();

        if ($single) {
            return !empty($result) ? $result[0] : null;
        }
        return $result;
    }

    public function get_patient_daily_meal_logs($patient_id, $start, $end, $offset_key = null) {
        if (empty($offset_key)) {
            $offset_key = 'UTC';
        }
        return $this->connection
                        ->table('food_log_summary AS fls')
                        ->select([
                            $this->connection->raw('DATE(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", ? )) as day'),
                            $this->connection->raw('DAYNAME(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", ? )) as day_name'),
                            $this->connection->raw('SUM(fls.calories) as total_cal'),
                            $this->connection->raw('SUM(fls.carbs) as total_carb'),
                            $this->connection->raw('SUM(fls.fats) as total_fat'),
                            $this->connection->raw('SUM(fls.protein) as total_protein'),
                            'l.patient_id',
                        ])
                        ->setBindings([ $offset_key, $offset_key], 'select')
                        ->leftJoin('log AS l', function ( $q ) {
                            $q->on('l.log_id', '=', 'fls.log_id');
                            $q->where('l.log_type', '=', 'Meal');
                        })
                        ->where('l.patient_id', '=', $patient_id)
                        ->whereRaw($this->connection->raw(' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) '))
                        ->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) '))
                        ->where('l.created_on', '>=', $start->timestamp * 1000)
                        ->where('l.created_on', '<', $end->timestamp * 1000)
                        ->groupBy('day')
                        ->get();
    }

    public function get_patient_logged_meals($patient_id, $limit = -1, $offset = 0) {
        $query = $this->connection
                ->table('food_log_summary AS fls')
                ->select([
                    'fls.*',
                    'l.is_suggested',
                    'l.patient_id',
                    'fi.image_name'
                ])
                ->leftJoin('log AS l', function ( $q ) {
                    $q->on('l.log_id', '=', 'fls.log_id');
                })
                ->leftJoin('food_image AS fi', 'fi.food_log_summary_id', '=', 'fls.food_log_summary_id')
                ->where('l.patient_id', '=', $patient_id)
                ->where('l.log_type', '=', 'Meal')
                ->whereRaw($this->connection->raw(' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) '))
                ->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) '))
                //->orderBy('fi.image_name', 'DESC')
                //->orderBy('fls.calories', 'DESC')
                ->orderBy('l.log_date_time', 'DESC');
        if ($limit > 0) {
            $query->skip($offset)->limit($limit);
        }

        return $query->get();
    }

    public function get_patients_targets($patient_ids, $single = true) {
        $result = $this->connection
                ->table('targets AS t')
                ->leftJoin('target_meal AS tm', 't.target_id', '=', 'tm.target_id')
                ->whereIn('t.patient_id', $patient_ids)
                ->get();
        $result = collect($result);
        if ($single) {
            return $result;
        }

        return $result->groupBy('patient_id');
    }

    public function get_patients_allergies($patient_id) {
        $result = $this->connection
                ->table('patient_dietary_info')
                ->select('dietary_restrictions')
                ->where('patient_id', '=', $patient_id)
                ->first();

        return $result;
    }

    public function get_patient_suggested_meals($patient_id, $start, $end, $limit = -1, $offset = 0) {
        $query = $this->connection
                ->table('food_log_summary AS fls')
                ->select([
                    'fls.*',
                    'l.is_suggested',
                    'l.patient_id',
                    'fi.image_name'
                ])
                ->leftJoin('log AS l', function ( $q ) {
                    $q->on('l.log_id', '=', 'fls.log_id');
                    $q->where('l.log_type', '=', 'Meal');
                })
                ->leftJoin('food_image AS fi', 'fi.food_log_summary_id', '=', 'fls.food_log_summary_id')
                ->leftJoin('suggested_log AS sl', 'l.log_id', '=', 'sl.log_id')
                ->where('l.patient_id', '=', $patient_id)
                ->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) '))
                ->whereRaw($this->connection->raw('ORD(l.is_suggested) = 1'))
                ->whereRaw($this->connection->raw('ORD(sl.is_drafted) = 1'))
                ->where('l.last_modified', '>=', $start->timestamp * 1000)
                ->where('l.last_modified', '<', $end->timestamp * 1000)
                ->orderBy('fi.image_name', 'DESC')
                ->orderBy('fls.calories', 'DESC')
                ->orderBy('l.last_modified', 'DESC');
        if ($limit > 0) {
            $query->skip($offset)->limit($limit);
        }

        return $query->get();
    }

    /**
     * @param $q
     * @param $is_coach_login
     * @param $coach_info
     *
     * @return array|static[]
     */
    public function search_patient($q, $is_coach_login, $coach_info) {
        $statement = $this->connection
                ->table('patient AS p')
                ->leftJoin('users AS u', 'p.user_id', '=', 'u.user_id')
                ->leftJoin('diabetes_type AS dt', 'p.diabetes_type_id', '=', 'dt.diabetes_type_id')
                ->leftJoin('patient_facility AS pf', 'p.patient_id', '=', 'pf.patients_patient_id')
                ->leftJoin('facility AS f', 'pf.facilities_facility_id', '=', 'f.facility_id')
                ->where(function ( $query ) use ( $q ) {
            $query->where('p.patient_id', '=', $q);
            $query->orWhere('u.first_name', 'LIKE', '%' . $q . '%');
            $query->orWhere('u.last_name', 'LIKE', '%' . $q . '%');
        });
        // Coach Login
        if ($is_coach_login) {
            $statement->where('p.lead_coach_id', '=', $coach_info->provider_id);
        }
        // Food Coach Login
        else if (!empty($coach_info) && !empty($coach_info->type) && $coach_info->type == 'Food Coach') {
            $statement->where('p.primary_food_coach_id', '=', $coach_info->provider_id);
        }
        // Any Provider Role
        else if (!empty($coach_info) && !empty($coach_info->facility_id)) {
            $statement->where('f.facility_id', '=', $coach_info->facility_id);
        }

        return $statement
                        ->select([
                            'u.*',
                            'p.patient_id',
                            'p.mrn',
                            'p.uuid',
                            'p.diabetes_type_id',
                            'dt.name AS diabetes_type_name',
                            'p.lead_coach_id',
                            'p.is_deleted',
                            'f.*',
                            'f.name AS facility_name'
                        ])
                        ->where('u.email', 'not like', '\\_%')
                        ->whereRaw($this->connection->raw('ORD(u.is_registration_completed) = 1'))
                        ->groupBy('p.patient_id')
                        ->orderBy('f.facility_id')
                        ->orderBy('p.patient_id')
                        ->get();
    }

    /**
     * get patient's weight logs
     * @param $patient_id
     * @param $start
     * @param $end
     * @param null $offset_key
     *
     * @return null
     */
    public function get_patient_weight_logs($patient_id, $start, $end, $offset_key = null) {
        if (empty($offset_key)) {
            $offset_key = 'UTC';
        }
        return $this->connection
                        ->table('log AS l')
                        ->select([
                            $this->connection->raw('DATE(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", ? )) as day'),
                            $this->connection->raw('DAYNAME(CONVERT_TZ(FROM_UNIXTIME(l.created_on/1000) ,"UTC", ? )) as day_name'),
                            $this->connection->raw('SUBSTRING_INDEX(group_concat(weight order by l.created_on desc), ",", 1 ) AS current_weight'),
                        ])
                        ->setBindings([ $offset_key, $offset_key], 'select')
                        ->leftJoin('misc_log AS ml', 'ml.log_id', '=', 'l.log_id')
                        ->where('l.log_type', '=', 'Weight')
                        ->where('l.patient_id', '=', $patient_id)
                        ->where('l.created_on', '>=', $start->timestamp * 1000)
                        ->where('l.created_on', '<', $end->timestamp * 1000)
                        ->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL )'))
                        ->groupBy('day')
                        ->orderBy('day', 'DESC')
                        ->get();
    }

    public function get_patient_wmr_log($patient_id, $weekly_meal_review_id = null) {
        $query = $this->connection
                ->table('weekly_meal_review AS wmr')
                ->where('wmr.patient_id', '=', $patient_id);
        if (!empty($weekly_meal_review_id)) {
            return $query
                            ->where('wmr.weekly_meal_review_id', '=', $weekly_meal_review_id)
                            ->first();
        }

        return $query
                        ->orderBy('wmr.weekly_meal_review_id', 'desc')
                        ->get();
    }

    /**
     * @param string $time_HHMM
     * @param int $day
     *
     * @return array|static[]
     */
    public function get_patient_for_wmr($time_HHMM = '1700', $day = 6) {
        return $this->connection
                        ->table('users AS u')
                        ->select([
                            'u.user_id',
                            'p.patient_id',
                            $this->connection->raw('CONVERT_TZ( UTC_TIMESTAMP(), "UTC", u.offset_key) as user_now'),
                            'wmr.*'
                        ])
                        ->leftJoin('patient AS p', 'p.user_id', '=', 'u.user_id')
                        ->rightJoin('weekly_meal_review AS wmr', 'p.patient_id', '=', 'wmr.patient_id')
                        // left join WMR table to select content of this week
                        ->whereRaw($this->connection->raw('EXTRACT(HOUR_MINUTE FROM CONVERT_TZ( UTC_TIMESTAMP(), "UTC", u.offset_key) ) = ?'), [ $time_HHMM])
                        ->whereRaw($this->connection->raw('DAYOFWEEK( CONVERT_TZ( UTC_TIMESTAMP(), "UTC", u.offset_key) ) = ?'), [ $day])
                        ->where('u.email', 'not like', '\\_%')
                        ->where('u.user_type', '=', 'PATIENT')
                        ->whereRaw($this->connection->raw('ORD(u.is_registration_completed) = 1'))
                        ->whereRaw($this->connection->raw(' ( wmr.is_sent IS NULL OR wmr.is_sent = 0 ) '))
                        ->whereRaw($this->connection->raw('DATEDIFF(CONVERT_TZ( UTC_TIMESTAMP(), "UTC", u.offset_key), wmr.week_start_date) <=7 '))
                        ->get();
    }

    /**
     * 
     * @param type $food_log_summary_ids
     * @return type
     */
    public function get_meal_info_by_id($food_log_summary_ids) {
        return $this->connection
                        ->table('food_log_summary AS fls')
                        ->select([
                            'fls.*',
                            $this->connection->raw('ORD(l.is_suggested) as is_suggested'),
                            $this->connection->raw('ORD(l.is_removed) as is_removed'),
                            $this->connection->raw('ORD(sl.is_drafted) as is_drafted'),
                            'l.patient_id',
                            'fi.image_name'
                        ])
                        ->leftJoin('log AS l', function ( $q ) {
                            $q->on('l.log_id', '=', 'fls.log_id');
                            $q->where('l.log_type', '=', 'Meal');
                        })
                        ->leftJoin('suggested_log AS sl', 'l.log_id', '=', 'sl.log_id')
                        ->leftJoin('food_image AS fi', 'fi.food_log_summary_id', '=', 'fls.food_log_summary_id')
                        ->whereIn('fls.food_log_summary_id', $food_log_summary_ids)
                        ->get();
    }

    /**
     * get_no_meals_logged_patients
     * 
     * @param type $food_coach_id
     * @return type
     */
    public function get_no_meals_logged_patients($food_coach_id = null) {
        $statement = $this->connection
                ->table('patient AS p')
                ->select([
                    'u.*',
                    'p.patient_id',
                    'p.mrn',
                    'p.uuid',
                    'p.diabetes_type_id',
                    'p.lead_coach_id',
                    'p.primary_food_coach_id',
                    'p.is_deleted',
                    $this->connection->raw('UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 168 HOUR)) as week_start_date'),
                ])
                ->leftJoin('log AS l', function ( $q ) {
                    $q->on('l.patient_id', '=', 'p.patient_id');
                    $q->where('l.log_type', '=', 'Meal');
                })
                ->leftJoin('users AS u', 'p.user_id', '=', 'u.user_id')
                ->where('u.email', 'not like', '\\_%')
                ->where('p.mrn', 'not like', '%test%')
                ->where('p.mrn', 'not like', 'dis%')
                ->where('p.mrn', 'not like', 'deactive%')
                ->whereRaw($this->connection->raw('ORD(u.is_registration_completed) = 1'))
                ->whereRaw($this->connection->raw('l.log_id IS NOT NULL'))
                ->whereNotIn('p.patient_id', function ( $q ) {
            $q->select('patient_id')
            ->from('log')
            ->where('log_type', 'Meal')
            ->whereRaw($this->connection->raw(' (ORD(is_suggested) = 0 OR is_suggested IS NULL) '))
            ->whereRaw($this->connection->raw(' (ORD(is_removed) = 0 OR is_removed IS NULL) '))
            ->whereRaw($this->connection->raw(' TIMESTAMPDIFF(HOUR, FROM_UNIXTIME(created_on/1000), NOW()) <= 168 '))
            ->groupBy('patient_id');
        });

        if (!empty($food_coach_id)) {
            $statement->where('p.primary_food_coach_id', '=', $food_coach_id);
        }
        return $statement->groupBy('patient_id')->get();
    }

    /**
     * get_never_meal_logged_patient
     * 
     * @param type $food_coach_id
     * @return type
     */
    public function get_never_meal_logged_patient($food_coach_id = null) {
        $statement = $this->connection
                ->table('patient AS p')
                ->select([
                    'u.*',
                    'p.patient_id',
                    'p.mrn',
                    'p.uuid',
                    'p.diabetes_type_id',
                    'p.lead_coach_id',
                    'p.primary_food_coach_id',
                    'p.is_deleted',
	                $this->connection->raw('UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 168 HOUR)) as week_start_date'),
                ])
                ->leftJoin('users AS u', 'p.user_id', '=', 'u.user_id')
                ->leftJoin('log AS l', function($q) {
                    $q->on('l.patient_id', '=', 'p.patient_id');
                    $q->where('l.log_type', '=', 'Meal');
                })
                ->where('u.email', 'not like', '\\_%')
                ->where('p.mrn', 'not like', '%test%')
                ->where('p.mrn', 'not like', 'dis%')
                ->where('p.mrn', 'not like', 'deactive%')
                ->whereNull('l.log_id');
        if (!empty($food_coach_id)) {
            $statement->where('p.primary_food_coach_id', '=', $food_coach_id);
        }
        return $statement->get();
    }
    
    /**
     * get_no_meals_logged_patients_first_two_week
     * 
     * @param type $food_coach_id
     * @return type
     */
    public function get_no_meals_logged_patients_first_two_week($food_coach_id = null) {
        $statement = $this->connection
                ->table('patient AS p')
                ->select([
                    'ut.*',
                    'p.patient_id',
                    'p.mrn',
                    'p.uuid',
                    'p.diabetes_type_id',
                    'p.lead_coach_id',
                    'p.primary_food_coach_id',
                    'p.is_deleted',
                    'lt.first_logged_on', 
                    $this->connection->raw('UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 168 HOUR)) as week_start_date'),
                ])
                ->leftJoin('users AS ut', 'p.user_id', '=', 'ut.user_id')
                ->leftJoin( $this->connection->raw( ' (select patient_id, min(created_on) as first_logged_on
                        FROM log 
                        WHERE log_type = "Meal"
                        and (ORD(is_suggested) = 0 OR is_suggested IS NULL) 
                        and (ORD(is_removed) = 0 OR is_removed IS NULL) 
                        GROUP BY patient_id) AS lt
			'), 'lt.patient_id', '=', 'p.patient_id' )
                ->where('ut.email', 'not like', '\\_%')
                ->where('p.mrn', 'not like', '%test%')
                ->where('p.mrn', 'not like', 'dis%')
                ->where('p.mrn', 'not like', 'deactive%')
                ->whereRaw($this->connection->raw('ut.password_created_date IS NOT NULL'))
                ->whereRaw($this->connection->raw('first_logged_on IS NOT NULL'))
                ->whereRaw($this->connection->raw('lt.first_logged_on > UNIX_TIMESTAMP(ut.password_created_date + INTERVAL 2 WEEK)*1000'));

        if (!empty($food_coach_id)) {
            $statement->where('p.primary_food_coach_id', '=', $food_coach_id);
        }
        return $statement->groupBy('patient_id')->get();
    }

    /**
     * get_no_meals_logged_patients_first_four_week
     * 
     * @param type $food_coach_id
     * @return type
     */
    public function get_no_meals_logged_patients_first_four_week($food_coach_id = null) {
        $statement = $this->connection
                ->table('patient AS p')
                ->select([
                    'u.*',
                    'p.patient_id',
                    'p.mrn',
                    'p.uuid',
                    'p.diabetes_type_id',
                    'p.lead_coach_id',
                    'p.primary_food_coach_id',
                    'p.is_deleted',
                    $this->connection->raw('UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 168 HOUR)) as week_start_date'),
                    $this->connection->raw('CEIL( DATEDIFF( NOW() , u.password_created_date ) /7 ) as week_in'),
                ])
                ->leftJoin('users AS u', 'p.user_id', '=', 'u.user_id')
                ->leftJoin('log AS l', function($q) {
                    $q->on('l.patient_id', '=', 'p.patient_id');
                    $q->where('l.log_type', '=', 'Meal');
                })
                ->where('u.email', 'not like', '\\_%')
                ->where('p.mrn', 'not like', '%test%')
                ->where('p.mrn', 'not like', 'dis%')
                ->where('p.mrn', 'not like', 'deactive%')
                ->whereRaw($this->connection->raw('CEIL( DATEDIFF( NOW() , u.password_created_date ) /7 ) = 4'))
                ->whereNull('l.log_id');
        if (!empty($food_coach_id)) {
            $statement->where('p.primary_food_coach_id', '=', $food_coach_id);
        }
        return $statement->groupBy('patient_id')->get();
    }

}

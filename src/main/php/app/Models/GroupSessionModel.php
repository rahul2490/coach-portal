<?php

/**
 * Created by PhpStorm.
 * User: rpatidar
 * Date: 31/08/17
 */

namespace HealthSlatePortal\Models;

use Carbon\Carbon;
use DB;

class GroupSessionModel
{

    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;
    protected $group_connection;

    /**
     *
     */
    function __construct()
    {
        $this->connection = DB::connection();
        $this->group_connection = DB::connection('groups');
        $this->patient_db = env('DB_DATABASE');
        $this->active_user_role = session('userRole');
    }


    public function getActivityData($patient_id, $start_date, $end_date)
    {

        return $this->connection
            ->table( 'log AS l')
            ->select( [
                $this->connection->raw( 'SUM(al.minutes_performed) AS total_activity_minutes')
            ])
            ->join('activity_log AS al','al.log_id', '=', 'l.log_id')
            //->join('patient AS P','P.patient_id', '=', 'l.patient_id')
            ->where( 'l.patient_id',$patient_id)
            ->where( 'al.timestamp','>=',$start_date)
            ->where( 'al.timestamp','<=',$end_date)
            ->first();


    }

}


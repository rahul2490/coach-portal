<?php

/**
 * Created by PhpStorm.
 * User: rpatidar
 * Date: 31/08/17
 */

namespace HealthSlatePortal\Models;

use Carbon\Carbon;
use DB;

class GroupModel {

    /**
     * @var \Illuminate\Database\Connection
     */
    protected $connection;
    protected $group_connection;

    /**
     *
     */
    function __construct() {
        $this->connection = DB::connection();
        $this->group_connection = DB::connection('groups');
        $this->patient_db  = env('DB_DATABASE');
        $this->active_user_role     = session('userRole');
    }


    public function get_enrolled_member_list($facility_id = 0) {
        /**
         * get_facility_patients
         *
         * @param $facility_id
         * @param null $statusFilter
         * @param int $daysFilter
         *
         * @return array|static[]
         */

        $query = $this->connection
                ->table( 'users AS u' )
                ->select( [
                    'p.patient_id','u.registration_date','u.group_visibility','u.user_id',
                    $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
                ] )
                ->leftJoin( 'patient AS p', 'u.user_id', '=', 'p.user_id' )
                ->leftJoin( 'group_user AS gu', 'u.user_id', '=', 'gu.user_id' )
                ;

            return $query
                ->whereRaw($this->connection->raw('(ORD(p.is_deleted) = 0 OR p.is_deleted IS NULL)'))
                ->where('p.mrn', 'not like', 'dis%')
                ->where('p.mrn', 'not like', 'deactiv%')
                ->where('p.facility_id', '=', $facility_id )
                ->whereNull('gu.user_id')
                ->orderBy( 'u.registration_date' )
                ->limit(5)
                ->get();
    }



    public function get_group_member_color_coding_detail($patient_ids) {

        return $this->connection
            ->table( 'patient_metrics AS pm' )
            ->select( [
                'pm.patient_id','pm.date','pm.engagement_score','pm.is_at_milestone_risk','pm.is_missing_starting_weight','pm.is_weight_increasing','pm.is_not_losing__wt_low_engagement',
                'pm.is_decrease_in_weight_logging','pm.is_engagement_declining','pm.is_no_step_reported_3_days','pm.is_not_responded_to_2_messages','pm.is_avg_step_declining',
                'pm.is_on_track','pm.patient_metrics_id',
            ] )
            ->whereIn('pm.patient_id', $patient_ids )
            ->get();
    }


    public function get_facility_group_list($facility_id = 0, $group, $capacity, $user_id = 0) {

        $query =  $this->group_connection
            ->table( 'groups AS g' )
            ->select( [
                'g.*',
                $this->group_connection->raw('FLOOR(DATEDIFF(CURDATE(), IF(DAYNAME(g.timestamp) = "Sunday" , g.timestamp , g.timestamp)) / 7) as completed_week'),
                $this->group_connection->raw('COUNT(gm.group_member_id) as total_member'),
                $this->group_connection->raw('COUNT(p.patient_id) as total_users'),
                $this->group_connection->raw('group_concat(p.patient_id) as group_patient_ids'),
            ] )
            ->leftJoin( 'group_member AS gm', 'g.group_id', '=', 'gm.group_id' )
            //->leftJoin( 'group_session AS gs', 'g.group_id', '=', 'gs.group_id' )
            ->leftJoin( "$this->patient_db.users AS u", 'gm.user_id', '=', 'u.user_id' )
            ->leftJoin( "$this->patient_db.patient AS p", 'u.user_id', '=', 'p.user_id' )
            ->where('g.facility_id', $facility_id );

        if ($group == 'online') {
            $query->whereRaw($this->group_connection->raw('(g.is_person = 0 OR g.is_person IS NULL)'));
        } else if ($group == 'inperson') {
            $query->where('g.is_person', 1);
        }

        if ($capacity == 'full') {
            $query->where('g.is_full', 1);
        } else if ($capacity == 'notfull') {
            $query->whereRaw($this->group_connection->raw('(g.is_full = 0 OR g.is_full IS NULL)'));
        }

        if($user_id)
        {
           // if($this->active_user_role == 'leadcoach')
              //  $query->where('g.lead_user_id', $user_id);
           // else
           // {
                $query->leftJoin( "group_member AS gum", 'g.group_id', '=', 'gum.group_id' );
                $query->whereIn('gum.user_id', [$user_id]);
           // }
        }

        return $query
            ->orderBy('g.group_id', 'desc')
            ->groupBy( 'g.group_id' )
            ->get();
    }







    public function get_group_detail($group_id) {

        $query =  $this->group_connection
            ->table( 'groups AS g' )
            ->select( [
                'g.*','p.patient_id','u.user_id','d.registration_id','d.device_type','dt.name as diabetes_type_name',
                $this->group_connection->raw('COUNT(gm.group_member_id) as total_member'),
                $this->group_connection->raw('COUNT(p.patient_id) as total_users'),
                'cpt.name as curriculum_program_type_name'
            ] )
            ->leftJoin( 'group_member AS gm', 'g.group_id', '=', 'gm.group_id' )
            ->leftJoin( "$this->patient_db.users AS u", 'gm.user_id', '=', 'u.user_id' )
            ->leftJoin( "$this->patient_db.patient AS p", 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( "$this->patient_db.diabetes_type AS dt", 'g.diabetes_type_id',  '=', 'dt.diabetes_type_id' )
            ->leftJoin( "$this->patient_db.curriculum_program_type AS cpt", 'dt.curriculum_program_type_id',  '=', 'cpt.curriculum_program_type_id' )
            ->leftJoin( "$this->patient_db.device AS d", 'd.device_mac_address', '=', 'p.device_mac_address' )
            ->where('g.group_id', $group_id );

        return $query
            ->groupBy( 'g.group_id' )
            ->first();
    }


    public function get_group_coaches_detail($group_id) {

        $query =  $this->group_connection
            ->table( 'groups AS g' )
            ->select( [
                'g.group_id','g.lead_user_id','g.primary_food_coach_id','u.user_id','u.user_type',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
            ] )
            ->leftJoin( 'group_member AS gm', 'g.group_id', '=', 'gm.group_id' )
            ->leftJoin( "$this->patient_db.users AS u", 'gm.user_id', '=', 'u.user_id' )
            ->leftJoin( "$this->patient_db.provider AS pr", 'u.user_id', '=', 'pr.user_id' )
            ->whereRaw($this->connection->raw('(ORD(pr.is_deleted) = 0 OR pr.is_deleted IS NULL)'))
            ->whereIn('u.user_type', ['Coach','Food Coach'])
            ->where('g.group_id', $group_id );

        return $query
            ->get();
    }

    public function get_post_read_count($group_id,$time,$user_id){
        $query =  $this->group_connection
            ->table( 'post AS p' )
            ->select( [
                $this->group_connection->raw('COUNT(*) as total_post')
            ] )
        ->where('p.group_id','=', $group_id )
        ->where('p.user_id','<>',$user_id)
        ->where('p.timestamp','>=',$time);
        return $query
            ->get();
    }



    public function get_all_group_list($facility_id,$user_id=0) {

        $query =  $this->group_connection
            ->table( 'groups AS g' )
            ->select( [
                'g.group_id','g.name','g.lead_user_id','g.facility_id','g.is_full','g.is_person','g.capacity'
                //$this->group_connection->raw('COUNT(gm.group_member_id) as total_member'),
                //$this->group_connection->raw('COUNT(p.patient_id) as total_users')
            ] );
            //->leftJoin( 'group_member AS gm', 'g.group_id', '=', 'gm.group_id' );
            //->leftJoin( "$this->patient_db.users AS u", 'gm.user_id', '=', 'u.user_id' )
            //->leftJoin( "$this->patient_db.patient AS p", 'u.user_id', '=', 'p.user_id' )
            //->where('g.group_id', $group_id );
         if($user_id) {
             $query->join('group_member AS gm', 'g.group_id', '=', 'gm.group_id');
             $query->where( 'gm.user_id', $user_id );
         }
        if($this->active_user_role != 'admin')
            $query->where( 'g.facility_id', $facility_id );


        return $query
            ->orderBy('g.name', 'asc')
            ->get();
    }



    public function get_member_coach_detail($user_id) {

        $query = $this->connection
            ->table( 'users AS u' )
            ->select( [
                'p.patient_id','p.uuid','p.diabetes_type_id', 'p.device_mac_address', 'd.device_type', 'cpt.name as curriculum_program_type_name', 'u.user_id', 'lcu.user_id as leadcoach_id', 'lcu.first_name as leadcoach_name','lcu.city as leadcoach_city','lcu.state as leadcoach_state','lcu.state as leadcoach_state','pr.acuity_link'
            ] )
            ->leftJoin( 'patient AS p', 'u.user_id',        '=', 'p.user_id' )
            ->leftJoin( 'users AS lcu', 'p.lead_coach_id',  '=', 'lcu.user_id' )
            ->leftJoin( 'diabetes_type AS dt', 'p.diabetes_type_id',  '=', 'dt.diabetes_type_id' )
            ->leftJoin( 'curriculum_program_type AS cpt', 'dt.curriculum_program_type_id',  '=', 'cpt.curriculum_program_type_id' )
            ->leftJoin( 'provider AS pr', 'lcu.user_id',  '=', 'pr.user_id' )
            ->leftJoin( 'device AS d', 'p.device_mac_address', '=', 'd.device_mac_address' );

        return $query
            ->where('u.user_id', '=', $user_id )
            ->first();
    }


    public function get_coach_detail($user_id) {

        $query = $this->connection
            ->table( 'users AS u' )
            ->select( [
               'u.*','pr.acuity_link'
            ] )
            ->leftJoin( 'provider AS pr', 'u.user_id',  '=', 'pr.user_id' );

        return $query
            ->where('u.user_id', '=', $user_id )
            ->first();
    }


    public function get_auto_messages_day_one() {

        return $this->connection
            ->table( 'auto_messages AS am' )
            ->select( [
                'am.*'
            ] )
            ->where('am.day', '=', 1 )
            ->get();
    }


    public function get_group_session_detail($group_id) {

        return $this->group_connection
            ->table( 'group_session AS gs' )
            ->select( [
                'gs.group_session_id','gs.group_id'
            ] )
            ->where('gs.group_id', '=', $group_id )
            ->get();
    }



    public function get_group_member_list($group_id, $facility_id) {

        $query =  $this->group_connection
            ->table( 'group_member AS gu' )
            ->select( ['g.lead_user_id as leadcoach_id','g.primary_food_coach_id as foodcoach_id','p.image_path as patient_image',
                'p.patient_id','u.registration_date','u.email','u.group_visibility','u.user_id','u.user_type','g.name as group_name','gu.group_member_id',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
            ] )
            ->join( 'groups AS g', 'g.group_id', '=', 'gu.group_id' )
            ->join( "$this->patient_db.users AS u", 'u.user_id', '=', 'gu.user_id' )
            ->leftJoin( "$this->patient_db.patient AS p", 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( "$this->patient_db.provider AS pr", 'u.user_id',  '=', 'pr.user_id' )
            ->where( 'g.facility_id', $facility_id )
            ->where( 'u.email', 'not like', '\\_%' )
            ->where('gu.group_id', $group_id );

        return $query
            ->groupBy( 'gu.user_id' )
            ->get();
    }



    public function get_group_post_by_group($group_id, $facility_id,$post_ids) {
        //print_r($getresult);

        $query =  $this->group_connection
            ->table( 'post as pst' )
            ->select( ['p.patient_id','u.user_id','u.user_type','u.user_type as post_user_type',
                $this->connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
                'pst.post_id','pst.post_type','pst.group_id','pst.user_id','pst.title','pst.description','pst.media_thumbnail','pst.media_url as post_media_url',
                'pst.media_type as post_media_type','pst.media_embed_url','pst.timestamp as post_timestamp','pst.last_updated','pst.is_auto_post',
                'pst_cmnt.post_comment_id', 'pst_cmnt.post_id as post_comment_post_id', 'pst_cmnt.post_id as comment_post_id','pst_cmnt.comment','pst_cmnt.is_like','pst_cmnt.timestamp as comment_timestamp',
                'pst_cmnt.media_url as comment_media_url','pst_cmnt.media_type as comment_media_type','p.image_path as comment_user_image','p.image_path as post_user_image','food.server_image_path as food_image','g.lead_user_id as lead_user','pst_cmnt.user_id as comment_user_id',
                'fls.type as meal_type','fls.calories as log_calories',
                $this->connection->raw( 'concat(cmnt_user.first_name, " " ,cmnt_user.last_name) as comment_user_name')

            ] )
            ->join( 'groups AS g', 'g.group_id', '=', 'pst.group_id' )
            ->leftJoin( "post_comment AS pst_cmnt", 'pst_cmnt.post_id',  '=', 'pst.post_id' )
            ->leftJoin( "$this->patient_db.users AS cmnt_user", 'pst_cmnt.user_id',  '=', 'cmnt_user.user_id' )
            ->leftJoin( "$this->patient_db.users AS u", 'u.user_id', '=', 'pst.user_id' )
            ->leftJoin( "$this->patient_db.patient AS p", 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( "$this->patient_db.provider AS pr", 'u.user_id',  '=', 'pr.user_id' )
            ->leftJoin( "$this->patient_db.food_image AS food", 'food.food_log_summary_id',  '=', 'pst.object_id' )
            ->leftJoin( "$this->patient_db.food_log_summary AS fls", 'fls.food_log_summary_id',  '=', 'food.food_log_summary_id')
            ->where( 'g.facility_id', $facility_id )
            ->whereIn( 'pst.post_id', $post_ids )
             ->orderBy('pst.post_id', 'desc' );
            //->whereIn( 'pst.post_type', array('Foodlog','Text','Html') )
//            ->where('pst.group_id', $group_id );
//            ->orderBy('pst.post_id', 'desc' );


        return $query
            ->get();
    }


    public function get_group_post_ids($group_id, $facility_id,$getresult) {
        //print_r($getresult);

        $query =  $this->group_connection
            ->table( 'post as pst' )
            ->select('pst.post_id')
            //->select( $this->connection->raw( 'group_concat(pst.post_id SEPARATOR ", ") as post_id' ) )
            ->join( 'groups AS g', 'g.group_id', '=', 'pst.group_id' )
            ->whereIn( 'pst.post_type', array('Foodlog','Text','Html') )
            ->where('pst.group_id', $group_id )
            ->where( 'g.facility_id', $facility_id )
            ->orderBy('pst.post_id', 'desc' )
            ->offset($getresult)
            ->limit(25);

        return $query
            ->get();
    }

    public function get_patient_step_count($patient_ids,$utc_timestamp,$compare_timestamp){
        $query = $this->connection
            ->table( 'patient AS p' )
            ->select( ['p.is_step_count_challenge_accepted','log.timestamp as timestamp',$this->group_connection->raw('COUNT(p.patient_id) as total_patient'),$this->connection->raw( 'concat(u.first_name," ",u.last_name) as patient_name' ),$this->connection->raw('SUM(log.steps) as total_steps'),'p.image_path as patient_image'] );
           // ->leftJoin( 'activity_log_summary AS log','p.patient_id','=','log.patient_id')
            $query->leftJoin('activity_log_summary AS log', function($join) use ($utc_timestamp,$compare_timestamp) {
                $join->on('p.patient_id','=','log.patient_id');
                $join->where('log.timestamp','>=',$compare_timestamp );
                $join->where('log.timestamp','<=',$utc_timestamp );
                $join ->where('log.attribute_type','=','steps');
            })
             ->leftJoin( 'users AS u', 'p.user_id','=', 'u.user_id' )
             ->whereIn('p.patient_id',$patient_ids)//array('7236','7228','7241')
             ->where('p.is_consent_accepted',1)
             ->orWhere('p.is_consent_accepted',0);
        return $query
            ->groupBy( 'p.patient_id' )
            ->orderBy('log.steps', 'desc')
            ->get();
    }

    public function get_count_is_like($post_id){
       return $query =  $this->group_connection
            ->table( 'post_comment as ps_cm' )
            ->select( [ $this->group_connection->raw('COUNT(ps_cm.is_like) as total_like') ] )
            ->where('ps_cm.post_id', $post_id )
            ->groupBy('ps_cm.post_id')
               ->get();
    }

    public function get_count_comment($post_id){
        return $query =  $this->group_connection
            ->table( 'post_comment as ps_cm' )
            ->select( [ $this->group_connection->raw('COUNT(ps_cm.comment) as total_comment') ] )
            ->where('ps_cm.post_id', $post_id )
            ->groupBy('ps_cm.post_id')
            ->get();
    }

    public function get_post_detail($post_id) {

        $query =  $this->group_connection
            ->table( 'post AS p' )
            ->select( [
                'p.post_id','p.description','p.post_type','p.media_url','p.media_type','p.group_id','p.user_id'
            ] )
            ->where('p.post_id', $post_id );
        return $query
            ->first();
    }




    public function get_group_session_list($group_id)
    {

        $query = $this->group_connection
            ->table('groups AS g')
            ->select([
                'g.group_id', 'g.diabetes_type_id', 'gs.group_session_id', 'gs.topic_id','gs.start_date','gs.location','gs.unlock_content_date','cpt.name as curriculum_program_type_name',
                //$this->group_connection->raw('count( IF(sa.attendance_type = "InPerson")) as sa.inperson_count'),
                $this->group_connection->raw('count(
					DISTINCT IF(sa.attendance_type = "InPerson", sa.session_attendance_id, null)
				  ) as inperson_count'),
                $this->group_connection->raw('count(
					DISTINCT IF(sa.attendance_type = "MakeUp" || sa.attendance_type = "MarkAsComplete", sa.session_attendance_id, null)
				  ) as makeup_count'),
                $this->group_connection->raw('count(
					DISTINCT IF(sa.attendance_type = "InPerson" || sa.attendance_type = "MakeUp" || sa.activity_minutes != "" || sa.weight_log_id != "", sa.session_attendance_id, null)
				  ) as status'),


            ])
            ->join('group_session AS gs', 'g.group_id', '=', 'gs.group_id')
            ->leftjoin('session_attendance AS sa', 'sa.group_session_id', '=', 'gs.group_session_id')
            ->leftJoin( "$this->patient_db.diabetes_type AS dt", 'g.diabetes_type_id',  '=', 'dt.diabetes_type_id' )
            ->leftJoin( "$this->patient_db.curriculum_program_type AS cpt", 'dt.curriculum_program_type_id',  '=', 'cpt.curriculum_program_type_id' );


        return $query
            ->where('g.group_id', $group_id)
            ->groupBy('gs.group_session_id')
            ->get();
    }


    public function get_group_current_session_name($group_id) {

        $query =  $this->group_connection
            ->table( 'groups AS g' )
            ->select( [
                'g.group_id','g.diabetes_type_id', 'gsa.attendance_type','gs.group_session_id','gs.topic_id', 'cpt.name as curriculum_program_type_name',
            ] )
            ->join( 'group_session AS gs', 'g.group_id', '=', 'gs.group_id' )
            ->leftJoin( "$this->patient_db.diabetes_type AS dt", 'g.diabetes_type_id',  '=', 'dt.diabetes_type_id' )
            ->leftJoin( "$this->patient_db.curriculum_program_type AS cpt", 'dt.curriculum_program_type_id',  '=', 'cpt.curriculum_program_type_id' )
            ->leftJoin('session_attendance AS gsa', function($join) use ($group_id)
            {
                $join->on('gs.group_session_id', '=', 'gsa.group_session_id');
                $join->where('gsa.attendance_type','!=', '');
            });

        return $query
            ->where('gsa.attendance_type','!=', '')
            ->whereIn('g.group_id', $group_id)
            //->orderBy('gs.group_session_id', 'desc')
            ->groupBy( 'gs.group_session_id' )
            ->get();
    }


    public function get_group_session_detail_by_id($group_session_id)
    {
        $query = $this->group_connection
            ->table('groups AS g')
            ->select([
                'g.group_id', 'g.diabetes_type_id', 'gs.*','cpt.name as curriculum_program_type_name',
            ])
            ->join('group_session AS gs', 'g.group_id', '=', 'gs.group_id')
            ->leftJoin( "$this->patient_db.diabetes_type AS dt", 'g.diabetes_type_id',  '=', 'dt.diabetes_type_id' )
            ->leftJoin( "$this->patient_db.curriculum_program_type AS cpt", 'dt.curriculum_program_type_id',  '=', 'cpt.curriculum_program_type_id' );


        return $query
            ->where('gs.group_session_id', $group_session_id)
            ->first();
    }


    public function get_group_report_for_duration_weight_details($patient_ids){
        return $this->connection
            ->table( 'patient AS p' )
            ->select( [
               't.starting_weight as starting_weight','t.target_weight as target_weight',

                // this_week_weight
                $this->connection->raw('SUBSTRING_INDEX(
				    GROUP_CONCAT(
				        IF(
					        (l.log_type = "Weight"),
					        CAST(ml.weight AS CHAR), NULL
				        ) ORDER BY l.log_time DESC ), ",", 1
				  ) as current_weight'),

            ])
            ->join( 'log AS l','l.patient_id', '=', 'p.patient_id'  )
            ->join( 'misc_log AS ml', 'ml.log_id', '=', 'l.log_id' )
            ->join('targets AS t', 't.patient_id', '=', 'p.patient_id')
            ->whereIn( 'p.patient_id',$patient_ids)
            ->whereRaw($this->connection->raw(' (ORD(l.is_suggested) = 0 OR l.is_suggested IS NULL) '))
            ->whereRaw($this->connection->raw(' (ORD(l.is_removed) = 0 OR l.is_removed IS NULL) '))
            ->groupBy( 'p.patient_id' )
            ->get();
    }




    public function  get_group_new_detail($group_session_id)
    {

        $query = $this->group_connection
            ->table('group_session AS gs')
            ->select([
                'p.patient_id','g.facility_id','g.is_person','g.lead_user_id','g.group_id','g.name as group_name','cpt.name as curriculum_program_type_name','gs.topic_id','sa.session_attendance_id','gs.location', 'gs.start_date', 'gs.unlock_content_date',
                 'u.user_id',$this->group_connection->raw( 'concat(u.first_name, " " ,u.last_name) as full_name' ),
                $this->group_connection->raw( 'concat(lcu.first_name, " " ,lcu.last_name) as lead_coach_full_name' ),
            ])
            ->leftJoin('session_attendance AS sa ', 'gs.group_session_id', '=', 'sa.group_session_id')
            ->leftJoin('group_member AS gm', 'gm.group_member_id', '=', 'sa.group_member_id')
            ->join('groups AS g', 'gs.group_id', '=', 'g.group_id')
            ->leftJoin( "$this->patient_db.patient AS p", 'gm.user_id', '=', 'p.user_id' )
            ->leftJoin( "$this->patient_db.users AS u", 'u.user_id', '=', 'p.user_id' )
            ->leftJoin( "$this->patient_db.users AS lcu", 'lcu.user_id', '=', 'g.lead_user_id' )
            ->leftJoin( "$this->patient_db.diabetes_type AS dt", 'g.diabetes_type_id',  '=', 'dt.diabetes_type_id' )
            ->leftJoin( "$this->patient_db.curriculum_program_type AS cpt", 'dt.curriculum_program_type_id',  '=', 'cpt.curriculum_program_type_id' );




       return $query
            ->where('gs.group_session_id', $group_session_id)
            ->groupBy( 'p.patient_id' )
            ->get();



    }

    public function get_today_birthday_data($active_facility_id, $today_date)
    {
        $query = $this->connection
            ->table( 'patient AS p' )
            ->select([ 'p.patient_id', $this->connection->raw('COUNT(p.patient_id) as birthday_count')])
            ->leftJoin( 'users AS u', 'u.user_id', '=', 'p.user_id' );

        if($this->active_user_role != 'admin')
            $query->where('p.facility_id', $active_facility_id);

           return  $query->where('p.dob', 'like', '%'.$today_date->format("m-d").'%')
                ->where('u.cobrand_id', session('user')->cobrandId)
                 ->first();

    }


}//End of Class


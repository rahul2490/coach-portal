<?php

namespace HealthSlatePortal\Helpers;

use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;
use Yajra\Datatables\Services\DataTable;

/**
 * Class DataTableServiceProvider
 * @package HealthSlateAdmin\Helpers
 */
class DataTableServiceProvider extends DataTable {

	/**
	 * @var
	 */
	protected $data;

	/**
	 * @param JsonResponse $data
	 *
	 * @return $this
	 */
	public function makeFrom( JsonResponse $data ) {
		$this->data = $data;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function ajax() {
		return $this->data;
	}

	/**
	 *
	 */
	public function query() {

	}

	public function getDataTables(){
		return $this->datatables;
	}

	/**
	 * @param Collection $builder
	 * @param bool $isExportRequest
	 * @param string $export
	 *
	 * @return JsonResponse|mixed
	 */
	public function dataTableResponse( Collection $builder, $isExportRequest = false, $export = 'csv' ) {

		if ( $isExportRequest ) {
			$this->datatables->getRequest()->merge( [ 'length' => - 1 ] );
		}

		$jsonResponse = $this->datatables->usingCollection( $builder )->make( true );

		// is a internal call?
		if ( request('_store') == 'yes' ) {

			$this->makeFrom( $jsonResponse );
			return $this->store( $export );
		}
		if ( $isExportRequest ) {
			$this->makeFrom( $jsonResponse );

			info(request()->getMethod() . "[" . request()->path() . "] Processing download request, Type: ".$export);
			if ( $export == 'csv' ) {

				return $this->csv();
			} elseif ( $export == 'excel' ) {

				return $this->excelExport($builder);
			} elseif ( $export == 'pdf' ) {

				return $this->pdf();
			}
		}

		return $jsonResponse;
	}

	/**
	 * @param string $ext
	 *
	 * @return mixed
	 */
	public function store( $ext = 'csv' ) {
		return $this->buildExcelFile()->store( $ext, false, true );
	}

	/**
	 * Custom function to export excel from view file
	 *
	 * @param $data
	 * @param string $view
	 *
	 * @return mixed
	 */
	public function excelExport( $data, $view = 'report.excel-export' ) {

		return app( 'excel' )->create( $this->filename(), function ( $excel ) use ( $data, $view ) {
			$excel->sheet( 'exported-data', function ( $sheet ) use ( $data, $view ) {
				if ( view()->exists( $view ) ) {
					$sheet->loadView( $view );
				} else {
					$sheet->fromArray( $this->getDataForExport() );
				}
			} );
		} )->download( 'xls' );
	}

	/**
	 * Send DataTable Response with additional key with array of first column values
	 * @param Collection $records
	 * @param string $keyName
	 *
	 * @return JsonResponse
	 */
	public function dataTableWithIds( Collection $records , $keyName = 'id') {

		$mDataSupport     = true;
		$orderFirst       = true;
		$total            = $records->count();
		$filteredTotal    = $total;
		//$id               = [ ];
		$collectionEngine = $this->datatables->usingCollection( $records );

		if ( $total ) {
			$collectionEngine->orderRecords( ! $orderFirst );
			$collectionEngine->filterRecords();
			$filteredTotal = $collectionEngine->collection->count();
			$collectionEngine->orderRecords( $orderFirst );
			//$id = $collectionEngine->collection->pluck( 0 )->toArray();
			$collectionEngine->paginate();
		}
		$collectionEngine
			->with( 'recordsTotal', $total )
			->with( 'recordsFiltered', $filteredTotal );
			//->with( $keyName, $id );

		return $collectionEngine->render( $mDataSupport );

	}
}
<?php
/**
 * User: Rpatidar
 * Date: 7/9/17
 */

namespace HealthSlatePortal\Helpers;

use HealthSlatePortal\Models\Healthslate;
use Twilio\Rest\Client;
use Exception;
use Mail;
//use Sms;

class SmsServiceHelper {

    /**
     * @param Healthslate $healthslate
     */
    public function __construct( Healthslate $healthslate ) {
        $atoz_preferences = $healthslate->get_atoz_preferences();

        if ( empty( $atoz_preferences ) ) {
            logger( 'Sms ServiceHelper: atoz_preferences Table is empty' );
        }

        $atoz_preferences = collect( $atoz_preferences );
        /*$host             = $atoz_preferences->where( 'preference_name', 'smtpHost' )->first();
        $emailId          = $atoz_preferences->where( 'preference_name', 'emailId' )->first();
        $username         = $atoz_preferences->where( 'preference_name', 'username' )->first();
        $password         = $atoz_preferences->where( 'preference_name', 'password' )->first();*/
        $twilioNumber     = $atoz_preferences->where( 'preference_name', 'twilioNumber' )->first();
        $twilioAccounSSid = $atoz_preferences->where( 'preference_name', 'twilioAccounSSid' )->first();
        $twilioAuthToken  = $atoz_preferences->where( 'preference_name', 'twilioAuthToken' )->first();


        if ( ! empty( $twilioNumber ) && ! empty( $twilioAccounSSid ) && ! empty( $twilioAuthToken )) {
            config( [ 'sms.twilioNumber' => $twilioNumber->preference_value ] );
            config( [ 'sms.twilioAccounSSid' => $twilioAccounSSid->preference_value ] );
            config( [ 'sms.twilioAuthToken' => $twilioAuthToken->preference_value ] );

        } else {
            logger( 'SmsServiceHelper: missing sms configuration in atoz_preferences Table' );
        }
    }

    /**
     * @param $subject
     * @param array $body
     * @param $to
     * @param array $file
     *
     * @return int
     */
    public function sendSms($sms_body="",$to_number) {

        $twilionumber = config('sms.twilioNumber');
        $twilioaccounssid = config('sms.twilioAccounSSid');
        $twilioauthtoken = config('sms.twilioAuthToken');
       // info( 'Processing send email request' );
        $client = new Client($twilioaccounssid, $twilioauthtoken);
        try {

            $message = $client->messages->create(
                $to_number, // Text this number
                array(
                    'from' => $twilionumber, // From a valid Twilio number
                    'body' => $sms_body
                )
            );

            return  $message->sid;

        }catch (Exception $e) {
             return  $e->getMessage();

        }
    }
}
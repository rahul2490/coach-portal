<?php
/**
 * User: rpatidar
 */

if (! function_exists('bit_to_int')) {

	function bit_to_int($bit){
		if(is_null($bit)){
			return 0;
		}
		if(is_int($bit)){
			return $bit;
		}
		return ord($bit);
	}
}

if (! function_exists('format_number')) {
	/**
	 * format_number
	 *
	 * @param $num
	 * @param int $decimal
	 * @param bool $ignore_zero
	 *
	 * @return int|string
	 */
	function format_number( $num, $decimal = 2, $ignore_zero = true ) {
		$num = is_null($num) ? 0 : $num;
		if($ignore_zero && $num == 0){
			return $num;
		}
		return number_format( (float) $num, $decimal, '.', '' );
	}
}

if ( ! function_exists( 'add_anchor_to_message' ) ) {

	/**
	 * @param $message
	 *
	 * @return mixed
	 */
	function add_anchor_to_message( $message ) {
		if ( ! empty( $message ) && str_contains( $message, [ 'http', 'https', 'www' ] ) ) {
			$reg = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i';

			return preg_replace( $reg, '<a href="$0">$0</a>', $message );
		}

		return $message;
	}
}


if (! function_exists('format_weight')) {
    /**
     * format_weight
     *
     * @param $weight
     * @param int $decimal
     *
     * @return string
     */
    function format_weight( $weight, $decimal = 2 ) {
        return number_format( (float) $weight, $decimal, '.', '' );
    }
}


if ( ! function_exists( 'get_weight_loss_data' ) ) {
	/**
	 * add weight_loss & weight_loss_percent after processing starting_weight & current_weight
	 * @param $row
	 *
	 * @return mixed
	 */
	function get_weight_loss_data( $row ) {
		$row->weight_loss         = null;
		$row->weight_loss_percent = null;
		if ( ! empty( $row->starting_weight ) && ! is_null( $row->current_weight ) ) {
			$row->weight_loss         = format_number( ( $row->starting_weight - $row->current_weight ) );
			$row->weight_loss_percent = format_number( ( $row->weight_loss ? ( $row->weight_loss / $row->starting_weight ) * 100 : '' ), 1 );
		}
		if ( ! empty( $row->starting_weight ) ) {
			$row->starting_weight = format_number( $row->starting_weight );
		}
		if ( ! empty( $row->current_weight ) ) {
			$row->current_weight = format_number( $row->current_weight );
		}

		return $row;
	}

    function getWeightLossByGroup($patients, $no_data_message = 'Group target not set'){
        /*$patients = [
            (object) [ 'starting_weight' => 120, 'current_weight' => 110, 'target_weight' => 90],
            (object) [ 'starting_weight' => 130, 'current_weight' => 140, 'target_weight' => 100],
            (object) [ 'starting_weight' => 130, 'current_weight' => 130, 'target_weight' => 100],
            (object) [ 'starting_weight' => 150, 'current_weight' => 100, 'target_weight' => 120],
            (object) [ 'starting_weight' => 200, 'current_weight' => 150, 'target_weight' => 150],
            (object) [ 'starting_weight' => 150, 'current_weight' => 141, 'target_weight' => 100],
        ];*/



        $group_weight_loss          = (object) [ ];
        $weight_loss                = 0;
        $target_weight_loss         = 0;
        $goal_completion_percentage = 0;
        foreach ( $patients as $key => $patient ) {

            $starting_weight = floatval( $patient->starting_weight );
            $current_weight  = floatval( $patient->current_weight );
            $target_weight   = floatval( $patient->target_weight );

            if ( ! empty( $current_weight ) && $current_weight > 0 ) {
                // only +ve loss is counted towards group goal
                // ony till 100% is counted if done more than 100% loss.
                if($starting_weight >= $current_weight) {
                    if ( $current_weight >= $target_weight ) {
                        $weight_loss_temp = $starting_weight - $current_weight;

                    } else {
                        $weight_loss_temp = $starting_weight - $target_weight;

                    }
                    $weight_loss        = $weight_loss + $weight_loss_temp;
                    $target_weight_loss_temp = $starting_weight - $target_weight;
                    $target_weight_loss = $target_weight_loss + $target_weight_loss_temp;
                    info( 'Processing : ' . $key );
                    info( 'patient_id: ' . @$patient->patient_id . ' weight_loss: ' . $weight_loss_temp . ', target_weight_loss: ' . $target_weight_loss_temp . '  weight loss %: ' . ( $target_weight_loss_temp ? ( ( $weight_loss_temp / $target_weight_loss_temp ) * 100 ) : 0 ) );
                }
            }
        }
        info('Total weight_loss: '.$weight_loss. ' Total target_weight_loss: '.$target_weight_loss);
        if ( $weight_loss && $target_weight_loss ) {
            $goal_completion_percentage = ( $weight_loss / $target_weight_loss ) * 100;
        }
        else if($weight_loss == 0 && $target_weight_loss > 0 ){
            $goal_completion_percentage = 0;
        }
        else {
            $goal_completion_percentage = $no_data_message;
        }

        if ( is_numeric($goal_completion_percentage) && $goal_completion_percentage > 100 ) {
            $goal_completion_percentage = 100;
        }
        $group_weight_loss->total_weight_loss          = format_weight($weight_loss);
        $group_weight_loss->goal_completion_percentage = is_numeric($goal_completion_percentage) ? format_weight($goal_completion_percentage) : $goal_completion_percentage;

        info(json_encode($group_weight_loss));
        return $group_weight_loss;
    }

    function get_cobrand_id(){
        $domain =  $_SERVER['SERVER_NAME'];
        $cobrandId='';
        info('Domain: ' . $domain);
        if($domain =='stage.soleraone.com' || $domain =='soleraone.com' || $domain =='www.soleraone.com' || $domain == 'soleraonestage.healthslate.com'){
            $cobrandId = 2;
        }else{
            $cobrandId = 1;
        }
        return $cobrandId;
    }


    function siteURL() {
        if(get_cobrand_id() == 1)
            return env('APP_BASE_URL');
        else
            return env('APP_BASE_URL_SOLERA');
    }



    if (! function_exists('timezone')) {

        function timezone(){
            return array(
                "-12"   => "(GMT -12:00) Eniwetok, Kwajalein",
                "-11"   => "(GMT -11:00) Midway Island, Samoa",
                "-10"   => "(GMT -10:00) Hawaii",
                "-9"    => "(GMT -9:00) Alaska",
                "-8"    => "(GMT -8:00) Pacific Time (US & Canada)",
                "-7"    => "(GMT -7:00) Mountain Time (US & Canada)",
                "-6"    => "(GMT -6:00) Central Time (US & Canada), Mexico City",
                "-5"    => "(GMT -5:00) Eastern Time (US & Canada), Bogota, Lima",
                "-4"    => "(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz",
                "-3.5"  => "(GMT -3:30) Newfoundland",
                "-3"    => "(GMT -3:00) Brazil, Buenos Aires, Georgetown",
                "-2"    => "(GMT -2:00) Mid-Atlantic",
                "-1"    => "(GMT -1:00 hour) Azores, Cape Verde Islands",
                "-0"    => "(GMT) Western Europe Time, London, Lisbon, Casablanca",
                "+1"    => "(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris",
                "+2"    => "(GMT +2:00) Kaliningrad, South Africa",
                "+3"    => "(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg",
                "+3.5"  => "(GMT +3:30) Tehran",
                "+4"    => "(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi",
                "+4.5"  => "(GMT +4:30) Kabul",
                "+5"    => "(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent",
                "+5.5"  => "(GMT +5:30) Bombay, Calcutta, Madras, New Delhi",
                "+5.75" => "(GMT +5:45) Kathmandu",
                "+6"    => "(GMT +6:00) Almaty, Dhaka, Colombo",
                "+7"    => "(GMT +7:00) Bangkok, Hanoi, Jakarta",
                "+8"    => "(GMT +8:00) Beijing, Perth, Singapore, Hong Kong",
                "+9"    => "(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk",
                "+9.5"  => "(GMT +9:30) Adelaide, Darwin",
                "+10"   => "(GMT +10:00) Eastern Australia, Guam, Vladivostok",
                "+11"   => "(GMT +11:00) Magadan, Solomon Islands, New Caledonia",
                "+12"   => "(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka",
            );
        }
    }


    if (! function_exists('state')) {

        function state()
        {
            return array(""=>"Select State","AL" => "AL" , "AK" => "AK", "AS" => "AS", "AZ" => "AZ",
                "AR" => "AR", "CA" => "CA", "CO" => "CO", "CT" => "CT", "DE" => "DE", "DC" => "DC",
                "FM" => "FM", "FL" => "FL", "GA" => "GA", "GU" => "GU", "HI" => "HI", "ID" => "ID",
                "IL" => "IL", "IN" => "IN", "IA" => "IA", "KS" => "KS", "KY" => "KY", "LA" => "LA",
                "ME" => "ME", "MH" => "MH", "MD" => "MD", "MA" => "MA", "MI" => "MI", "MN" => "MN",
                "MS" => "MS", "MO" => "MO", "MT" => "MT", "NE" => "NE", "NV" => "NV", "NH" => "NH",
                "NJ" => "NJ", "NM" => "NM", "NY" => "NY", "NC" => "NC", "ND" => "ND", "MP" => "MP",
                "OH" => "OH", "OK" => "OK", "OR" => "OR", "PW" => "PW", "PA" => "PA", "PR" => "PR",
                "RI" => "RI", "SC" => "SC", "SD" => "SD", "TN" => "TN", "TX" => "TX", "UT" => "UT",
                "VT" => "VT", "VI" => "VI", "VA" => "VA", "WA" => "WA", "WV" => "WV", "WI" => "WI",
                "WY" => "WY");
        }
    }

    if (! function_exists('coach_type')) {

        function coach_type()
        {
            return array("Coach" => "Coach" , "Food Coach" => "Food Coach", "Tech Support" => "Tech Support");
        }
    }

    if (! function_exists('diabetes_type')) {

        function diabetes_type()
        {
            return array("" => "Select Diabetes Type" ,"1" => "TYPE 2", "2" => "2012 Curriculum", "3" => "Prevent T2 Curriculum");
        }
    }

    if (! function_exists('add_member_diabetes_type')) {

        function add_member_diabetes_type()
        {
            return array("" => "Select Diabetes Type" , "1" => "Diabetes" , "2" => "DPP-2012", "3" => "DPP-Prevent T2");
        }
    }

    if(! function_exists('UUID'))
    {
        function UUID() {
            $data = openssl_random_pseudo_bytes(16, $secure);
            if (false === $data) { return false; }
            $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
            $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
            return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
        }
    }


    function get_engagement_score($member)
    {
        $color_class    = 'white';
        $status         = '';
        $status_type    = '';
        if($member->is_at_milestone_risk)
        {
            $color_class = 'danger';
            $status      = 'Milestone Risk';
            $status_type = 'is_at_milestone_risk';
        }
        else if($member->is_missing_starting_weight)
        {
            $color_class = 'danger';
            $status      = 'No starting weight';
            $status_type = 'is_missing_starting_weight';
        }
        else if($member->is_weight_increasing)
        {
            $color_class = 'danger';
            $status      = 'Weight increasing';
            $status_type = 'is_at_milestone_risk';
        }
        else if($member->is_not_losing__wt_low_engagement)
        {
            $color_class = 'danger';
            $status      = 'Not losing and low engagement';
            $status_type = 'is_weight_increasing';
        }
        else if($member->is_decrease_in_weight_logging)
        {
            $color_class = 'danger';
            $status      = 'Decrease in weight logging';
            $status_type = 'is_decrease_in_weight_logging';
        }
        else if($member->is_engagement_declining)
        {
            $color_class = 'warning';
            $status      = 'Engagement decline';
            $status_type = 'is_engagement_declining';
        }
        else if($member->is_no_step_reported_3_days)
        {
            $color_class = 'warning';
            $status      = 'No step data';
            $status_type = 'is_no_step_reported_3_days';
        }
        else if($member->is_not_responded_to_2_messages)
        {
            $color_class = 'warning';
            $status      = 'Not responding to LC messages';
            $status_type = 'is_not_responded_to_2_messages';
        }
        else if($member->is_avg_step_declining)
        {
            $color_class = 'warning';
            $status      = 'Steps/day decline';
            $status_type = 'is_avg_step_declining';
        }
        else if($member->is_on_track)
        {
            $color_class = 'success';
            $status      = '';
            $status_type = 'is_on_track';
        }

        return array('color_class' => $color_class, 'status' => $status, 'status_type' => $status_type);
    }

    function array_strip_tags($array)
    {
        $result = array();
        foreach ($array as $key => $value) {
            $key = strip_tags($key);
            if (is_array($value)) {
                $result[$key] = array_strip_tags($value);
            }
            else {
                $result[$key] = strip_tags($value);
            }
        }
        return $result;
    }


}
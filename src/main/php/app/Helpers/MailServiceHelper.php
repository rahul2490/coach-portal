<?php
/**
 * User: Rpatidar
 * Date: 7/9/17
 */

namespace HealthSlatePortal\Helpers;

use HealthSlatePortal\Models\Healthslate;
use Mail;

class MailServiceHelper {

	/**
	 * @param Healthslate $healthslate
	 */
	public function __construct( Healthslate $healthslate ) {
		$atoz_preferences = $healthslate->get_atoz_preferences();

		if ( empty( $atoz_preferences ) ) {
			logger( 'MailServiceHelper: atoz_preferences Table is empty' );
		}

		$atoz_preferences = collect( $atoz_preferences );
		$host             = $atoz_preferences->where( 'preference_name', 'smtpHost' )->first();
		$emailId          = $atoz_preferences->where( 'preference_name', 'emailId' )->first();
		$username         = $atoz_preferences->where( 'preference_name', 'username' )->first();
		$password         = $atoz_preferences->where( 'preference_name', 'password' )->first();

        $name = 'Healthslate';
		if(get_cobrand_id() == 2)
        {
            $emailId->preference_value = 'soleraone@soleraone.com';
            $name = 'soleraone';
        }

		if ( ! empty( $host ) && ! empty( $emailId ) && ! empty( $username ) && ! empty( $password ) ) {
			config( [ 'mail.host' => $host->preference_value ] );
			config( [ 'mail.username' => $username->preference_value ] );
			config( [ 'mail.password' => $password->preference_value ] );
			config( [ 'mail.from' => [ 'address' => $emailId->preference_value, 'name' => $name ] ] );
		} else {
			logger( 'MailServiceHelper: missing email configuration in atoz_preferences Table' );
		}
	}

	/**
	 * @param $subject
	 * @param array $body
	 * @param $to
	 * @param array $file
	 *
	 * @return int
	 */
	public function sendEmail(array $body, $to, $file = [] ) {
	   // dd($body);
		info( 'Processing send email request' );
		Mail::later(5, 'email.common' , $body, function ( $message ) use ($body, $to, $file , $body) {
			$message->to( $to );
			$message->subject( $body['subject'] );
            ///$message->setBody(nl2br($body['content']), 'text/html');
			/*if ( isset( $file['full'] ) && isset( $file['title'] ) ) {
				$message->attach( $file['full'], [ 'as' => $file['title'] ] );
			}*/
		} );
		$failed = count(Mail::failures());
		info( 'Send email request completed, sent_status: '.($failed == 0 ? 'Y' : 'N') );
		return ($failed == 0 ? 1 : 0);
	}
}
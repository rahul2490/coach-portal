<?php

namespace HealthSlatePortal\Helpers;


use Carbon\Carbon;

class DateUtil {

	/**
	 * @var Carbon
	 */
	protected $carbon;

	/**
	 * @param Carbon $carbon
	 */
	public function __construct( Carbon $carbon ) {
		$this->carbon       = $carbon->now();
	}

	public function getCarbon() {
		return $this->carbon;
	}

	public function getLast7DaysStartDay( ){
		return $this->carbon->copy()->subWeek();
	}
	/**
	 *
	 */
	public function changeWeekStartDay() {
		if ( Carbon::MONDAY === $this->carbon->getWeekStartsAt() ) {
			$this->carbon->setWeekStartsAt( Carbon::SUNDAY );
			$this->carbon->setWeekEndsAt( Carbon::SATURDAY );
		} else {
			$this->carbon->setWeekStartsAt( Carbon::MONDAY );
			$this->carbon->setWeekEndsAt( Carbon::SUNDAY );
		}
	}

	/**
	 * @return Carbon
	 */
	public function getThisWeekStartDate() {
		$this->changeWeekStartDay();
		$date = $this->carbon->copy()->startOfWeek();
		$this->changeWeekStartDay();
		return $date;
	}

	/**
	 * @param int $week
	 *
	 * @return Carbon
	 */
	public function getLastWeekStartDate($week = 1) {
		$this->changeWeekStartDay();
		$date = $this->carbon->copy()->subWeeks($week)->startOfWeek();
		$this->changeWeekStartDay();
		return $date;
	}
}
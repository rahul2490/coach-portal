<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group( array( 'middleware' => [ 'guest' ] ), function () {
	Route::get( '/', array( 'as' => 'login', 'uses' => 'AuthController@index' ) );
	//Route::get( 'login/byToken', array( 'as' => 'login-by-token', 'uses' => 'AuthController@loginByTokenHandler' ) );
	Route::post( '/login', array( 'as' => 'login-handler', 'uses' => 'AuthController@loginHandler' ) );

    Route::post( 'forgot-password', array( 'as' => 'forgot-password', 'uses' => 'LoginController@fogotPassword' ) );

    Route::get('token/{token}', 'LoginController@resetPassword');
    Route::post( 'resetpasswordstore', ['as' => 'resetpasswordstore', 'uses'   => 'LoginController@resetPasswordStore' ]);

} );

Route::get( 'logout', array( 'as' => 'logout', 'uses' => 'AuthController@logout' ) );




/*
|--------------------------------------------------------------------------
| Role Common Routes: All Types Role Routes
|--------------------------------------------------------------------------
*/


//globalXssClean();

Route::filter('strip_tags', function()
{
    Input::merge(array_strip_tags(Input::all()));
});


$prefixes = ['facilityadmin' => 'role-facility-admin', 'leadcoach' => 'role-lead-coach', 'foodcoach' => 'role-food-coach', 'admin' => 'role-admin', 'techsupport' => 'role-tech-support'];
foreach($prefixes as $prefix => $role_key) {
    Route::group(['before' => 'strip_tags', 'prefix' => $prefix, 'middleware' => [ 'auth', 'auth.token', $role_key]], function () {

        Route::get( 'dashboard/{patientId}',    ['as'   => 'dashboard','uses' => 'DashboardController@index'] );
        Route::get( 'setting/{patientId}',      ['as'   => 'setting','uses' => 'DashboardSetting@index'] );
        Route::get( 'logbook/{patientId?}',      ['as'   => 'logbook','uses' => 'LogBookController@index'] );
        Route::get( 'pastmeal/{patientId?}',     ['as'   => 'pastmeal','uses' => 'PastMealController@index'] );




        Route::get( 'direct-message/{user_id}', ['as' => 'directMessageFullPage', 'uses'   => 'MessageController@direct_message_to_patient_full_page' ]);
        Route::get( 'addmember', ['as' => 'addmember', 'uses' => 'facilityadmin\MemberRecruitmentController@index' ]);

        Route::get( 'prospectivemember', ['as' => 'prospectivemember', 'uses' => 'facilityadmin\MemberRecruitmentController@prospectiveMember' ]);

        //Help Section Start
        Route::get( 'portaltrainingwebinar',    ['as'   => 'portaltrainingwebinar','uses' => 'HelpController@portalTrainingWebinar'] );
        Route::get( 'apphelpvideoios',          ['as'   => 'apphelpvideoios','uses' => 'HelpController@appHelpVideoIos'] );
        Route::get( 'apphelpvideoandroid',      ['as'   => 'apphelpvideoandroid','uses' => 'HelpController@appHelpVideoAndroid'] );




        Route::get( 'videodetailios',  ['as' => 'videodetailios','uses' => 'facilityadmin\HomeController@videodetailios'] );
        Route::get( 'videodetailandriod', ['as' => 'videodetailandriod','uses' => 'facilityadmin\HomeController@videodetailandriod'] );

        /*Route::get('videodetailandriod',function() {
            return view('help.app_help_video_android_detail');
        });

        Route::get('videodetailios',function() {
            return view('help.app_help_video_ios_detail');
        });*/

        //Help Section End
    });
}

Route::group( [ 'before' => 'strip_tags', 'middleware' => [ 'auth', 'auth.token'] ], function () {


    Route::post( 'hide-unhide', ['as' => 'hide-unhide', 'uses'   => 'facilityadmin\MemberController@patient_hide_unhide' ]);
    Route::post( 'pin-unpin', ['as' => 'pin-unpin', 'uses'   => 'facilityadmin\MemberController@patient_pin_unpin' ]);
    Route::get( 'facilityadmin/member-detail', ['as' => 'facilityadmin.member-detail', 'uses'   => 'facilityadmin\MemberController@memberDetail' ]);

    Route::get( 'directMessage', ['as' => 'directMessage', 'uses'   => 'MessageController@direct_message_to_patient' ]);

    Route::post( 'saveDirectMessage', ['as' => 'saveDirectMessage', 'uses'   => 'MessageController@save_direct_message' ]);
    Route::get( 'reloadDirectMessage', ['as' => 'reloadDirectMessage', 'uses'   => 'MessageController@reload_direct_message' ]);
    Route::post( 'markreadmessage', ['as' => 'markreadmessage', 'uses'   => 'MessageController@markReadMessage' ]);

    Route::post( 'change-password', ['as' => 'change-password', 'uses' => 'LoginController@changePassword' ]);



    /*member recruitment section start  */
    Route::post( 'facilityadmin/mrnsave', ['as' => 'facilityadmin.mrnsave', 'uses'   => 'facilityadmin\MemberRecruitmentController@mrnSave' ]);
    Route::post( 'addmember-store', ['as' => 'addmember-store', 'uses' => 'facilityadmin\MemberRecruitmentController@addMemberStore' ]);
    Route::post( 'prospectivememberlist', ['as' => 'prospectivememberlist', 'uses'   => 'facilityadmin\MemberRecruitmentController@prospectiveMembersList' ]);
    Route::post( 'delete-prospectivemember', ['as'   => 'delete-prospectivemember','uses' => 'facilityadmin\MemberRecruitmentController@delete'] );
    Route::post( 'invitationsend', ['as' => 'invitationsend', 'uses'   => 'facilityadmin\MemberRecruitmentController@invitationSend' ]);
    Route::post( 'mrnsave', ['as' => 'mrnsave', 'uses'   => 'facilityadmin\MemberRecruitmentController@mrnSave' ]);
    Route::get( 'pastmeal/past_meal_by_month',     ['as'   => 'pastmeal/past_meal_by_month','uses' => 'PastMealController@past_meal_by_month'] );
    Route::get( 'pastmeal/past_meal_by_week',     ['as'   => 'pastmeal/past_meal_by_week','uses' => 'PastMealController@past_meal_by_week'] );
    Route::get( 'pastmeal/past_meal_by_filter',     ['as'   => 'past_meal_by_filter','uses' => 'PastMealController@past_meal_by_filter'] );
    Route::get( 'pastmeal/remove_past_meal',     ['as'   => 'pastmeal/remove_past_meal','uses' => 'PastMealController@remove_past_meal'] );
    Route::get( 'pastmeal/edit_past_meal/{logId}',     ['as'   => 'pastmeal/edit_past_meal','uses' => 'PastMealController@edit_past_meal'] );
    Route::get( 'pastmeal/get_past_meal_by_date_range_weekly',     ['as'   => 'pastmeal/get_past_meal_by_date_range_weekly','uses' => 'PastMealController@get_past_meal_by_date_range_weekly'] );

    Route::get( 'logbook/log_book_by_filter',     ['as'   => 'log_book_by_filter','uses' => 'LogBookController@log_book_by_filter'] );
    Route::get( 'logbook/log_book_by_week',     ['as'   => 'logbook/log_book_by_week','uses' => 'LogBookController@log_book_by_week'] );
    Route::get( 'logbook/log_book_by_month',     ['as'   => 'logbook/log_book_by_month','uses' => 'LogBookController@log_book_by_month'] );
    Route::get( 'logbook/get_log_book_by_date_range_weekly',     ['as'   => 'logbook/get_log_book_by_date_range_weekly','uses' => 'LogBookController@get_log_book_by_date_range_weekly'] );
    Route::get( 'logbook/get_log_book_by_date_range_monthly',     ['as'   => 'logbook/get_log_book_by_date_range_monthly','uses' => 'LogBookController@get_log_book_by_date_range_monthly'] );
    Route::get( 'logbook/get_log_fitbit_data',     ['as'   => 'logbook/get_log_fitbit_data','uses' => 'LogBookController@get_log_fitbit_data'] );

    /*member recruitment section end  */

    Route::get('laravel-error-logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::post( 'add-coach-notes', ['as' => 'add-coach-notes', 'uses'   => 'DashboardController@add_new_coach_notes' ]);
    Route::get( 'get-coach-notes', ['as' => 'get-coach-notes', 'uses'   => 'DashboardController@get_coach_notes' ]);
    Route::get( 'get-all-goals-of-patient', ['as' => 'get-all-goals-of-patient', 'uses'   => 'DashboardController@get_all_goals_of_patient' ]);
    Route::post( 'motivation-image-update', ['as' => 'motivation-image-update', 'uses'   => 'DashboardController@motivation_image_update' ]);
    Route::post( 'user-profile-image-update', ['as' => 'user-profile-image-update', 'uses'   => 'DashboardController@user_profile_image_update' ]);


    Route::get( 'patient-weight-summary-chart-data', ['as' => 'patient-weight-summary-chart-data','uses' => 'DashboardController@get_patient_weight_summary_chart_data'] );
    Route::get( 'weekly-weight-summary-chart-data', ['as'  => 'weekly-weight-summary-chart-data','uses' => 'DashboardController@get_weekly_weigh_summary_chart_data'] );

    Route::get( 'meals-logged-chart-data', ['as' => 'meals-logged-chart-data','uses' => 'DashboardController@get_meals_logged_chart_data'] );
    Route::get( 'weekly-meals-logged-chart-data', ['as'  => 'weekly-meals-logged-chart-data','uses' => 'DashboardController@get_weekly_meals_logged_chart_data'] );

    Route::get( 'daily-engagement-data', ['as' => 'daily-engagement-data','uses' => 'DashboardController@get_daily_engagement_data'] );
    Route::get( 'weekly-engagement-data', ['as'  => 'weekly-engagement-data','uses' => 'DashboardController@get_daily_engagement_data'] );

    Route::get( 'physical-activity-steps-daily', ['as' => 'physical-activity-steps-daily','uses' => 'DashboardController@get_physical_activity_steps_daily'] );
    Route::get( 'physical-activity-weekly', ['as'  => 'physical-activity-weekly','uses' => 'DashboardController@get_physical_activity_weekly']);

    Route::get( 'meal-source-chart-data', ['as' => 'meal-source-chart-data','uses' => 'DashboardController@get_meal_source_chart_data'] );
    Route::get( 'weekly-meal-source-chart-data', ['as'  => 'weekly-meal-source-chart-data','uses' => 'DashboardController@get_weekly_meal_source_chart_data'] );


    /* dashboard setting */
    Route::post( 'update_member_profile_setting', ['as' => 'update_member_profile_setting', 'uses'   => 'DashboardSetting@updateMemberProfile' ]);
    Route::post( 'update_member_diabetes_info', ['as' => 'update_member_diabetes_info', 'uses'   => 'DashboardSetting@updateMemberDiabetesInfo' ]);
    Route::post( 'update_member_reminder', ['as' => 'update_member_reminder', 'uses'   => 'DashboardSetting@updateMemberReminder' ]);

} );



/*
|--------------------------------------------------------------------------
| Role Admin Routes: admin
|--------------------------------------------------------------------------
*/
Route::group( [ 'before' => 'strip_tags', 'middleware' => [ 'auth', 'auth.token', 'role-admin' ] ], function () {

    Route::get( 'admin/home', ['as'   => 'admin.home','uses' => 'admin\HomeController@index'] );
    Route::any( 'admin/members/{group_id?}', ['as' => 'admin.members', 'uses'   => 'admin\MemberController@index' ]);
    Route::get( 'admin/membersList', ['as' => 'admin.membersList', 'uses'   => 'admin\MemberController@membersList' ]);
    Route::any( 'admin/coaches', ['as' => 'admin.coaches', 'uses'   => 'admin\CoachController@index' ]);
    Route::get( 'admin/coachesList', ['as' => 'admin.coachesList', 'uses' => 'admin\CoachController@coachesList' ]);
    Route::get(  'admin/facility', ['as' => 'admin.facility', 'uses' => 'admin\FacilityController@index' ]);
    Route::get(  'admin/edit_facility/{id}', ['as' => 'admin.edit_facility', 'uses' => 'admin\FacilityController@edit_facility' ]);
    Route::get(  'admin/add_facility', ['as' => 'admin.add_facility', 'uses' => 'admin\FacilityController@add_facility' ]);
    Route::post( 'admin/assign-facility-to-facility-admin', ['as' => 'admin.assign-facility-to-facility-admin', 'uses' => 'admin\FacilityController@assignFacility' ] );
    Route::post( 'admin/facility-store', ['as' => 'admin.facility-store', 'uses' => 'admin\FacilityController@facilityStore' ]);
    Route::post( 'admin/add-facility-store', ['as' => 'admin.add-facility-store', 'uses' => 'admin\FacilityController@addfacilityStore' ]);
    Route::post(  'admin/facility-delete', ['as' => 'admin/facility-delete', 'uses' => 'admin\FacilityController@delete_facility_admin' ]);
    Route::post( 'admin/allCoaches', ['as' => 'admin/allCoaches', 'uses' => 'facilityadmin\MemberRecruitmentController@get_all_coaches' ]);
    Route::post( 'admin/create-new-facility-admin', ['as' => 'admin.create-new-facility-admin', 'uses' => 'admin\FacilityController@create_new_facility_admin' ] );

    //for coach add edit delete
    Route::get( 'admin/coach/{coach_id}', ['as' => 'admin.coach-edit', 'uses'   => 'admin\CoachController@coachEdit' ]);
    Route::post( 'admin/coach-add', ['as' => 'admin.coach-add-store1', 'uses'    => 'admin\CoachController@coachAddStore' ]);
    Route::post( 'admin/coach-edit', ['as' => 'admin.coach-edit-store1', 'uses'  => 'admin\CoachController@coachEditStore' ]);
    Route::post( 'admin/coach/delete', [ 'as'   => 'admin.delete-coach', 'uses' => 'admin\CoachController@delete' ] );
    Route::post( 'admin/coach-reset-password', ['as' => 'admin/coach-reset-password', 'uses' => 'admin\CoachController@coachResetPassword' ]);
    Route::get( 'admin/edit-facility-admin', ['as' => 'admin.edit-facility-admin', 'uses' => 'admin\CoachController@editFacilityAdmin' ]);
    Route::post( 'admin/update-facility-admin', ['as' => 'admin.update-facility-admin', 'uses' => 'admin\CoachController@updateFacilityAdmin' ]);
} );


/*
|--------------------------------------------------------------------------
| Role Facility Admin Routes: ROLE_FACILITY_ADMIN
|--------------------------------------------------------------------------
*/
Route::group( [ 'before' => 'strip_tags', 'middleware' => [ 'auth', 'auth.token', 'role-facility-admin' ] ], function () {

    Route::get( 'facilityadmin/home', ['as'   => 'facilityadmin.home','uses' => 'facilityadmin\HomeController@index'] );
    Route::post(  'facilityadmin/facility-delete', ['as' => 'facilityadmin/facility-delete', 'uses' => 'facilityadmin\FacilityController@delete_facility_admin' ]);
    Route::post( 'facilityadmin/facilityChange', ['as' => 'facilityadmin.facilityChange', 'uses' => 'facilityadmin\HomeController@facilityChange' ]);
    Route::get(  'facilityadmin/facility', ['as' => 'facilityadmin.facility', 'uses' => 'facilityadmin\FacilityController@index' ]);
    Route::post( 'facilityadmin/facility-store', ['as' => 'facilityadmin.facility-store', 'uses' => 'facilityadmin\FacilityController@facilityStore' ]);

    Route::post( 'facilityadmin/assign-facility-to-facility-admin', ['as' => 'facilityadmin.assign-facility-to-facility-admin', 'uses' => 'facilityadmin\FacilityController@assignFacility' ] );

    Route::any( 'facilityadmin/coaches', ['as' => 'facilityadmin.coaches', 'uses'   => 'facilityadmin\CoachController@index' ]);
    Route::get( 'facilityadmin/coachesList', ['as' => 'facilityadmin.coachesList', 'uses' => 'facilityadmin\CoachController@coachesList' ]);
    Route::get( 'facilityadmin/coach/{coach_id}', ['as' => 'facilityadmin.coach-edit', 'uses' => 'facilityadmin\CoachController@coachEdit' ]);

    Route::post( 'facilityadmin/coach/get-assign-coach-data', ['as' => 'facilityadmin.get-assign-coach-data', 'uses' => 'facilityadmin\CoachController@getFoodLifestyleCoachData' ] );
    Route::post( 'facilityadmin/coach/assign-coach', ['as' => 'facilityadmin.assign-coach', 'uses' => 'facilityadmin\CoachController@assignCoach' ] );
    Route::post( 'facilityadmin/coach/delete', [ 'as'   => 'facilityadmin.delete-coach', 'uses' => 'facilityadmin\CoachController@delete' ] );
    Route::post( 'facilityadmin/coach-edit', ['as' => 'facilityadmin.coach-edit-store', 'uses' => 'facilityadmin\CoachController@coachEditStore' ]);
    Route::post( 'facilityadmin/coach-add', ['as' => 'facilityadmin.coach-add-store', 'uses' => 'facilityadmin\CoachController@coachAddStore' ]);
    Route::post( 'facilityadmin/coach-reset-password', ['as' => 'facilityadmin/coach-reset-password', 'uses' => 'facilityadmin\CoachController@coachResetPassword' ]);

    Route::any( 'facilityadmin/members/{group_id?}', ['as' => 'facilityadmin.members', 'uses'   => 'facilityadmin\MemberController@index' ]);
    Route::get( 'facilityadmin/membersList', ['as' => 'facilityadmin.membersList', 'uses'   => 'facilityadmin\MemberController@membersList' ]);

    Route::get( 'facilityadmin/member-group-detail', ['as' => 'facilityadmin.member-group-detail', 'uses'   => 'facilityadmin\MemberController@memberGroupDetail' ]);
    Route::post( 'facilityadmin/add-member-to-group', ['as' => 'facilityadmin.add-member-to-group', 'uses'   => 'facilityadmin\MemberController@addMemberToGroup' ]);
    Route::post( 'facilityadmin/remove-member-from-group', ['as' => 'facilityadmin.remove-member-from-group', 'uses'   => 'facilityadmin\MemberController@removeMemberFromToGroup' ]);

    /* My Group Section  Start */
    Route::any( 'facilityadmin/group', ['as' => 'facilityadmin.group', 'uses'   => 'facilityadmin\MyGroupController@index' ]);
    //Route::any( 'facilityadmin/group', ['as' => 'facilityadmin.group', 'uses'   => 'facilityadmin\MyGroupController@comingsoon' ]);
    Route::post( 'facilityadmin/group-member-list', ['as' => 'facilityadmin.group-member-list', 'uses'   => 'facilityadmin\MyGroupController@membersList' ]);
    Route::get( 'facilityadmin/group-list', ['as' => 'facilityadmin.group-list', 'uses'   => 'facilityadmin\MyGroupController@groupList' ]);
    Route::post( 'facilityadmin/group-create', ['as' => 'facilityadmin.group-create', 'uses'   => 'facilityadmin\MyGroupController@createGroup' ]);
    Route::post( 'facilityadmin/group-update', ['as' => 'facilityadmin.group-update', 'uses'   => 'facilityadmin\MyGroupController@updateGroup' ]);
    Route::get( 'facilityadmin/group-detail', ['as' => 'facilityadmin.group-detail', 'uses'   => 'facilityadmin\MyGroupController@groupDetail' ]);
    Route::post( 'facilityadmin/group-delete', [ 'as'   => 'facilityadmin.group-delete', 'uses' => 'facilityadmin\MyGroupController@deleteGroup' ] );
    Route::post( 'facilityadmin/remove-group-coach', [ 'as'   => 'facilityadmin.remove-group-coach', 'uses' => 'facilityadmin\MyGroupController@removeCoachFromGroup' ] );
    Route::post( 'facilityadmin/assign-coach-to-group', ['as' => 'facilityadmin.assign-coach-to-group', 'uses' => 'facilityadmin\MyGroupController@assignCoachToGroup' ] );
    Route::post( 'facilityadmin/coach/get-group-assign-coach-data', ['as' => 'facilityadmin.get-group-assign-coach-data', 'uses' => 'facilityadmin\MyGroupController@getFoodLifestyleCoachData' ] );
    /* My Group Section  END */
    Route::post( 'facilityadmin/group-wall_scroll', ['as' => 'facilityadmin.group-wall', 'uses'   => 'facilityadmin\GroupWallController@scroll_data' ]);
    Route::get( 'facilityadmin/group-wall/{group_id}', ['as' => 'facilityadmin.group-wall', 'uses'   => 'facilityadmin\GroupWallController@index' ]);
    Route::post(  'facilityadmin/post-delete', ['as' => 'facilityadmin/post-delete', 'uses' => 'facilityadmin\GroupWallController@delete_post' ]);
    Route::post(  'facilityadmin/remove_post_comment', ['as' => 'facilityadmin/remove_post_comment', 'uses' => 'facilityadmin\GroupWallController@remove_post_comment' ]);

} );



/*
|--------------------------------------------------------------------------
| Role Lead Coach Routes: Coach
|--------------------------------------------------------------------------
*/
Route::group( [ 'middleware' => [ 'auth', 'auth.token', 'role-lead-coach' ] ], function () {

    Route::get( 'leadcoach/home', ['as'   => 'leadcoach.home','uses' => 'leadcoach\HomeController@index'] );


    Route::any( 'leadcoach/members/{group_id?}', ['as' => 'leadcoach.members', 'uses'   => 'leadcoach\MemberController@index' ]);
    Route::get( 'leadcoach/membersList', ['as' => 'leadcoach.membersList', 'uses'   => 'leadcoach\MemberController@membersList' ]);
    Route::post( 'leadcoach/facilityChange', ['as' => 'leadcoach.facilityChange', 'uses' => 'leadcoach\HomeController@facilityChange' ]);

    Route::any( 'leadcoach/group', ['as' => 'leadcoach.group', 'uses'   => 'leadcoach\MyGroupController@index' ]);
    Route::get( 'leadcoach/group-list', ['as' => 'leadcoach.group-list', 'uses'   => 'leadcoach\MyGroupController@groupList' ]);
    Route::get( 'leadcoach/message-group-member', ['as' => 'leadcoach.message-group-member', 'uses'   => 'leadcoach\MyGroupController@messageGroupMember' ]);
    Route::post( 'leadcoach/saveGroupMessage', ['as' => 'leadcoach.saveGroupMessage', 'uses'   => 'leadcoach\MyGroupController@saveGroupMessage' ]);
    Route::get( 'leadcoach/group-wall/{group_id}', ['as' => 'leadcoach.group-wall', 'uses'   => 'leadcoach\GroupWallController@index' ]);

    Route::get( 'leadcoach/group-session/{group_id}', ['as' => 'leadcoach.group-session', 'uses'   => 'leadcoach\GroupSessionController@index' ]);

    Route::get( 'leadcoach/group-session-detail/{group_id}', ['as' => 'leadcoach.group-session-detail', 'uses'   => 'leadcoach\GroupSessionController@group_session_detail' ]);
    Route::get( 'leadcoach/get-group-session-detail', ['as' => 'leadcoach.get-group-session-detail', 'uses'   => 'leadcoach\GroupSessionController@getGroupSessionDetail' ]);
    Route::post( 'leadcoach/update-group-session-detail', ['as' => 'leadcoach.update-group-session-detail', 'uses'   => 'leadcoach\GroupSessionController@UpdateGroupSessionDetail' ]);
    Route::get( 'leadcoach/group-detail/{group_id}', ['as' => 'leadcoach.group-detail', 'uses'   => 'leadcoach\GroupDetailController@index' ]);
    Route::get( 'leadcoach/group-session-list', ['as' => 'leadcoach.group-session-list', 'uses'   => 'leadcoach\GroupSessionController@groupSessionList' ]);
    Route::post( 'leadcoach/group-session-store', ['as' => 'leadcoach.group-session-store', 'uses'   => 'leadcoach\GroupSessionController@groupSessionstore' ]);
   // group_session_detail
    Route::get( 'leadcoach/group-wall/{group_id}', ['as' => 'leadcoach.group-wall', 'uses'   => 'leadcoach\GroupWallController@index' ]);
    Route::post( 'leadcoach/group-wall_scroll', ['as' => 'leadcoach.group-wall_scroll', 'uses'   => 'leadcoach\GroupWallController@scroll_data' ]);
    Route::post(  'leadcoach/post-delete', ['as' => 'leadcoach/post-delete', 'uses' => 'leadcoach\GroupWallController@delete_post' ]);
    Route::post(  'leadcoach/like_post_by_coach', ['as' => 'leadcoach/like_post_by_coach', 'uses' => 'leadcoach\GroupWallController@like_post_by_coach' ]);
    Route::post(  'leadcoach/remove_like_post_by_coach', ['as' => 'leadcoach/remove_like_post_by_coach', 'uses' => 'leadcoach\GroupWallController@remove_like_post_by_coach' ]);
    Route::post(  'leadcoach/add_comment', ['as' => 'leadcoach/add_comment', 'uses' => 'leadcoach\GroupWallController@add_comment' ]);
    Route::post(  'leadcoach/remove_post_comment', ['as' => 'leadcoach/remove_post_comment', 'uses' => 'leadcoach\GroupWallController@remove_post_comment' ]);
    Route::post( 'leadcoach/post-detail', ['as' => 'leadcoach/post-detail', 'uses'   => 'leadcoach\GroupWallController@postDetail' ]);
    Route::post( 'leadcoach/update-post', ['as' => 'leadcoach/update-post', 'uses'   => 'leadcoach\GroupWallController@update_post' ]);
    Route::post( 'leadcoach/add-new-post', ['as' => 'leadcoach/add-new-post', 'uses'   => 'leadcoach\GroupWallController@add_new_post' ]);
    Route::post( 'leadcoach/delete-post-image', ['as' => 'leadcoach/delete-post-image', 'uses'   => 'leadcoach\GroupWallController@delete_post_image' ]);
    //group sessin attendance
    Route::get( 'leadcoach/get-session-attendance-data', ['as' => 'leadcoach.get-session-attendance-data', 'uses'   => 'leadcoach\GroupSessionController@getSessionAttendanceData' ]);
    Route::post( 'leadcoach/update-session-attendance-data', ['as' => 'leadcoach.update-session-attendance-data', 'uses'   => 'leadcoach\GroupSessionController@UpdateSessionAttendanceData' ]);
    Route::post( 'leadcoach/update-session-attendance-activity-data', ['as' => 'leadcoach.update-session-attendance-activity-data', 'uses'   => 'leadcoach\GroupSessionController@UpdateSessionAttendanceActivityData' ]);
    Route::get( 'leadcoach/group-session-attendance-list', ['as' => 'leadcoach.group-session-attendance-list', 'uses'   => 'leadcoach\GroupSessionController@groupSessionAttendanceList' ]);
    Route::get( 'leadcoach/get-session-attendance-weight-data', ['as' => 'leadcoach.get-session-attendance-weight-data', 'uses'   => 'leadcoach\GroupSessionController@getSessionWeightData' ]);
    Route::post( 'leadcoach/update-session-attendance-weight-data', ['as' => 'leadcoach.update-session-attendance-weight-data', 'uses'   => 'leadcoach\GroupSessionController@updateSessionWeightData' ]);

    //unlock content for all
    Route::get( 'leadcoach/unlock_content_get_data', ['as' => 'leadcoach.unlock_content_get_data', 'uses'   => 'leadcoach\GroupSessionController@unlockContentGetData' ]);
    Route::post( 'leadcoach/update_unlock_content_data', ['as' => 'leadcoach.update_unlock_content_data', 'uses'   => 'leadcoach\GroupSessionController@updateUnlockContentData' ]);
    Route::post( 'leadcoach/update-mark-as-complete', ['as' => 'leadcoach.update-mark-as-complete', 'uses'   => 'leadcoach\GroupSessionController@updateMarkAsCompleted' ]);


    Route::get( 'leadcoach/group-session-list', ['as' => 'leadcoach.group-session-list', 'uses'   => 'leadcoach\GroupSessionController@groupSessionList' ]);
    Route::post( 'leadcoach/group-session-store', ['as' => 'leadcoach.group-session-store', 'uses'   => 'leadcoach\GroupSessionController@groupSessionstore' ]);
    Route::post( 'leadcoach/group-post', ['as' => 'leadcoach.group-post', 'uses'   => 'leadcoach\MyGroupController@save_group_post' ]);
} );


/*
|--------------------------------------------------------------------------
| Role Food Coach Routes: Food Coach
|--------------------------------------------------------------------------
*/
Route::group( [ 'middleware' => [ 'auth', 'auth.token', 'role-food-coach' ] ], function () {

    Route::get( 'foodcoach/home', ['as'   => 'foodcoach.home','uses' => 'foodcoach\HomeController@index'] );
    Route::any( 'foodcoach/members/{group_id?}', ['as' => 'foodcoach.members', 'uses'   => 'foodcoach\MemberController@index' ]);
    Route::get( 'foodcoach/membersList', ['as' => 'foodcoach.membersList', 'uses'   => 'foodcoach\MemberController@membersList' ]);
    Route::post( 'foodcoach/facilityChange', ['as' => 'foodcoach.facilityChange', 'uses' => 'foodcoach\HomeController@facilityChange' ]);

    Route::any( 'foodcoach/group', ['as' => 'foodcoach.group', 'uses'   => 'foodcoach\MyGroupController@index' ]);
    Route::get( 'foodcoach/group-list', ['as' => 'foodcoach.group-list', 'uses'   => 'foodcoach\MyGroupController@groupList' ]);

    Route::get( 'foodcoach/message-group-member', ['as' => 'foodcoach.message-group-member', 'uses'   => 'foodcoach\MyGroupController@messageGroupMember' ]);
    Route::post( 'foodcoach/saveGroupMessage', ['as' => 'foodcoach.saveGroupMessage', 'uses'   => 'foodcoach\MyGroupController@saveGroupMessage' ]);

    Route::get( 'foodcoach/group-wall/{group_id}', ['as' => 'foodcoach.group-wall', 'uses'   => 'foodcoach\GroupWallController@index' ]);
    Route::post( 'foodcoach/group-wall_scroll', ['as' => 'foodcoach.group-wall_scroll', 'uses'   => 'foodcoach\GroupWallController@scroll_data' ]);
    Route::post(  'foodcoach/post-delete', ['as' => 'foodcoach/post-delete', 'uses' => 'foodcoach\GroupWallController@delete_post' ]);
    Route::post(  'foodcoach/like_post_by_coach', ['as' => 'foodcoach/like_post_by_coach', 'uses' => 'foodcoach\GroupWallController@like_post_by_coach' ]);
    Route::post(  'foodcoach/remove_like_post_by_coach', ['as' => 'foodcoach/remove_like_post_by_coach', 'uses' => 'foodcoach\GroupWallController@remove_like_post_by_coach' ]);
    Route::post(  'foodcoach/add_comment', ['as' => 'foodcoach/add_comment', 'uses' => 'foodcoach\GroupWallController@add_comment' ]);
    Route::post(  'foodcoach/remove_post_comment', ['as' => 'foodcoach/remove_post_comment', 'uses' => 'foodcoach\GroupWallController@remove_post_comment' ]);
    Route::post( 'foodcoach/post-detail', ['as' => 'foodcoach/post-detail', 'uses'   => 'foodcoach\GroupWallController@postDetail' ]);
    Route::post( 'foodcoach/update-post', ['as' => 'foodcoach/update-post', 'uses'   => 'foodcoach\GroupWallController@update_post' ]);
    Route::post( 'foodcoach/add-new-post', ['as' => 'foodcoach/add-new-post', 'uses'   => 'foodcoach\GroupWallController@add_new_post' ]);
    Route::post( 'foodcoach/delete-post-image', ['as' => 'foodcoach/delete-post-image', 'uses'   => 'foodcoach\GroupWallController@delete_post_image' ]);

    Route::get( 'foodcoach/group-session-list', ['as' => 'foodcoach.group-session-list', 'uses'   => 'foodcoach\GroupSessionController@groupSessionList' ]);
    Route::post( 'foodcoach/group-session-store', ['as' => 'foodcoach.group-session-store', 'uses'   => 'foodcoach\GroupSessionController@groupSessionstore' ]);

} );



/*
|--------------------------------------------------------------------------
| Role Tech Support Routes: Tech Support
|--------------------------------------------------------------------------
*/
Route::group( [ 'middleware' => [ 'auth', 'auth.token', 'role-tech-support' ] ], function () {

    Route::get( 'techsupport/home', ['as'   => 'techsupport.home','uses' => 'techsupport\HomeController@index'] );
    Route::any( 'techsupport/members/{group_id?}', ['as' => 'techsupport.members', 'uses'   => 'techsupport\MemberController@index' ]);
    Route::get( 'techsupport/membersList', ['as' => 'techsupport.membersList', 'uses'   => 'techsupport\MemberController@membersList' ]);
    Route::post( 'techsupport/facilityChange', ['as' => 'techsupport.facilityChange', 'uses' => 'techsupport\HomeController@facilityChange' ]);

} );


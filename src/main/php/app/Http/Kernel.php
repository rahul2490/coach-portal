<?php

namespace HealthSlatePortal\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \HealthSlatePortal\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \HealthSlatePortal\Http\Middleware\VerifyCsrfToken::class,
        \HealthSlatePortal\Http\Middleware\AfterMiddleware::class,
        \HealthSlatePortal\Http\Middleware\LogAfterRequest::class
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'                  => \HealthSlatePortal\Http\Middleware\Authenticate::class,
        'auth.token'            => \HealthSlatePortal\Http\Middleware\AuthenticateToken::class,
        'ajax-filter'           => \HealthSlatePortal\Http\Middleware\AjaxFilterMiddleware::class,
        'guest'                 => \HealthSlatePortal\Http\Middleware\RedirectIfAuthenticated::class,
        'role-facility-admin'   => \HealthSlatePortal\Http\Middleware\RoleFacilityAdminMiddleware::class,
        'role-lead-coach'       => \HealthSlatePortal\Http\Middleware\RoleLeadCoachMiddleware::class,
        'role-food-coach'       => \HealthSlatePortal\Http\Middleware\RoleFoodCoachMiddleware::class,
        'role-tech-support'     => \HealthSlatePortal\Http\Middleware\RoleTechSupportMiddleware::class,
        'role-admin'            => \HealthSlatePortal\Http\Middleware\RoleAdminMiddleware::class,
    ];
}

<?php

namespace HealthSlatePortal\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'facilityadmin/facilityChange', 'facilityadmin/coachesList', 'facilityadmin/membersList',
        'facilityadmin/group-member-list', 'facilityadmin/group-list','prospectivememberlist',
        'delete-prospectivemember','facilityadmin/hideunhideevent',
        'hide-unhide','pin-unpin','facilityadmin/coach-reset-password','leadcoach/facilityChange','foodcoach/facilityChange','techsupport/facilityChange','saveDirectMessage','markreadmessage','facilityadmin/facility','facilityadmin/facility-delete','admin/facility-delete',
        'facilityadmin/post-delete','facilityadmin/group-wall_scroll','leadcoach/group-wall_scroll','leadcoach/post-delete','leadcoach/like_post_by_coach','leadcoach/remove_like_post_by_coach','leadcoach/add_comment','leadcoach/remove_post_comment','leadcoach/post-detail','leadcoach/update-post',
        'leadcoach/add-new-post','leadcoach/delete-post-image','foodcoach/group-wall_scroll','foodcoach/post-delete','foodcoach/like_post_by_coach','foodcoach/remove_like_post_by_coach','foodcoach/add_comment','foodcoach/remove_post_comment','foodcoach/post-detail','foodcoach/update-post',
        'foodcoach/add-new-post','foodcoach/delete-post-image',
        'facilityadmin/post-delete','facilityadmin/group-wall_scroll','leadcoach/group-wall_scroll','leadcoach/post-delete','leadcoach/like_post_by_coach','leadcoach/remove_like_post_by_coach','leadcoach/add_comment','admin/allCoaches','admin/coach-reset-password',
        'facilityadmin/post-delete','facilityadmin/group-wall_scroll','leadcoach/group-wall_scroll','leadcoach/post-delete','leadcoach/like_post_by_coach','leadcoach/remove_like_post_by_coach','leadcoach/add_comment','admin/allCoaches','facilityadmin/remove_post_comment','pastmeal/past_meal_by_month','pastmeal/past_meal_by_week',
        'past_meal_by_filter','pastmeal/remove_past_meal','leadcoach/group-post',
    ];
}

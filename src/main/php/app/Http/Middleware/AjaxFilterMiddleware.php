<?php

namespace HealthSlatePortal\Http\Middleware;

use Closure;

class AjaxFilterMiddleware {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {
		$action = $request->get('action',null);
		// allow non-ajax request for CSV download only
		if (!$request->ajax() && !in_array($action, ['csv'])) {
			logger('Invalid request for ajax-filter url: ' . $request->getRequestUri() . ' Method: ' . $request->method() . ' Ip: ' . $request->getClientIp());
			return response('Unauthorized', 401);
		}
		return $next( $request );
	}
}

<?php

namespace HealthSlatePortal\Http\Middleware;

use Closure;

class AfterMiddleware {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {
		$response = $next($request);

		$response->headers->set('Cache-Control', 'no-cache, no-store, max-age=0, must-revalidate');
		$response->headers->set('Expires', 'Fri, 01 Jan 1990 00:00:00 GMT');


		// for dev only
		if ( count( \DB::getQueryLog() ) ) {
			info( ( \DB::getQueryLog() ) );
			info( 'Total Queries : ' . count( \DB::getQueryLog() ) );
		}

		return $response;
	}

	/**
	 * perform action after HTTP response has already been sent to browser.
	 *
	 * @param \Illuminate\Http\Request $request
	 * @param \Illuminate\Http\Response $response
	 */
	public function terminate($request, $response) {
		if(defined('LARAVEL_START')){
			info($request->getMethod() . "[" . $request->path() . "] Execution time : ".round((microtime(true)-LARAVEL_START)*1000,4)." ms");
		}
	}
}

<?php

namespace HealthSlatePortal\Http\Middleware;

use Closure;

class RoleTechSupportMiddleware {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {
        $user = session('user', false);
        if($user->userRole == 'Tech Support')
            return $next( $request );
        else if($user->userRole == 'Coach')
            return redirect()->intended('/leadcoach/home');
        else if($user->userRole == 'ROLE_FACILITY_ADMIN')
            return redirect()->intended('facilityadmin/home');
        else if($user->userRole == 'Food Coach')
            return redirect()->intended('/foodcoach/home');
        else if($user->userRole == 'ADMIN' || $user->userRole == 'ROLE_ADMIN')
            return redirect()->intended('/admin/home');
        else
        {
           logger('Unauthorized User');
           // return response('Unauthorized', 401);
           return response()->view( "errors.503", [ 'page_title' => 'Unauthorized User', 'code' => 503,'css'=> array('error.min') ], 200 );
        }
	}
}

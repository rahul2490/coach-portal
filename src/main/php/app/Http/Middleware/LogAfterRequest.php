<?php

namespace HealthSlatePortal\Http\Middleware;
use Illuminate\Support\Facades\Log;
use Closure;
use Redirect;

use Illuminate\Contracts\Routing\TerminableMiddleware;


class LogAfterRequest implements TerminableMiddleware {

    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        if(date('Y')=='2017' && date('m')=='11' && date('d')<='19')
        Log::info('app.requests', ['request' => $_SERVER]);
    }

}

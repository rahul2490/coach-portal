<?php

namespace HealthSlatePortal\Http\Middleware;

use Closure;
use HealthSlatePortal\Models\AuthServiceProvider;
use HealthSlatePortal\Models\FacilityModel;
use Request;
use Response;
use Session;
use Log;

class AuthenticateToken {

	/**
	 * @var AuthServiceProvider
	 */
	protected $authServiceProvider;
    protected $facility_model;
	/**
	 * @param AuthServiceProvider $authServiceProvider
	 */
	public function __construct( AuthServiceProvider $authServiceProvider , FacilityModel $facility_model  ) {
		$this->authServiceProvider  = $authServiceProvider;
        $this->facility_model       = $facility_model;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure $next
	 *
	 * @return mixed
	 */
	public function handle( $request, Closure $next ) {
		$user = session('user', false);

		if ( empty( $user ) ) {
			// User is from Portal
			$user = $this->authServiceProvider->getAdminUserByToken( $request->cookie( 'access_token' ) );
			if ( empty( $user ) || isset( $user->errorDetail ) ) {
				// Not able to fetch user by access token
				return $this->commonErrorResponseHandler( 'Unauthorized access, Invalid Token', '/logout' );
			}
			session( [ 'user' => $user ] );

            Log::info('Getting All Facility Of The Logged In User');
            $response = $this->facility_model->get_admin_facility_list($user->userName, Session('user')->cobrandId);
            $user_role = $this->facility_model->get_user_info($user->userName);
            if(!empty($user_role))
            {
                Session::put( 'userId', $user_role->user_id);
                Session::put( 'full_name', $user_role->full_name);
            }
            if($user->userRole == "ROLE_PROVIDER")
            {
                $user->userRole = $user_role->user_type;
            }
            Log::info('Total Facility Of The Logged In User: ' . count($response));
            if(count($response) > 0)
            {
                Session::put( 'facility', $response );
                Session::put( 'active_facility', $response[0]->facility_id );
            }
            else
            {
                Session::put( 'facility', $response );
                Session::put( 'active_facility', '' );
            }

            if($user->userRole == 'Coach')
            {
                Session::put( 'userRole', 'leadcoach');
                return redirect()->intended('/leadcoach/home');
            }
            else if($user->userRole == 'Food Coach')
            {
                Session::put( 'userRole', 'foodcoach');
                return redirect()->intended('/foodcoach/home');
            }
            else if($user->userRole == 'Tech Support')
            {
                Session::put( 'userRole', 'techsupport');
                return redirect()->intended('/techsupport/home');
            }
            else if($user->userRole == 'ROLE_FACILITY_ADMIN')
            {
                Session::put( 'userRole', 'facilityadmin');
                return redirect()->intended('/facilityadmin/home');
            }
            else if($user->userRole == 'ADMIN' || $user->userRole == 'ROLE_ADMIN')
            {
                Session::put( 'userRole', 'admin');
                return redirect()->intended('/admin/home');
            }

		} else {
			// Check if user token is invalid ?
			//(time() > $user->now + $user->expiresIn) /*commented for SSO implementation */
			/*if ( $this->authServiceProvider->isInvalidToken($user)) {
				// not valid, refresh token
				if (!$this->authServiceProvider->refreshToken($user)) {
					// refresh token failed
					return $this->commonErrorResponseHandler('Unauthorized access, Invalid Token', '/logout');
				}
			}*/
		}
		return $next( $request );
	}

	/**
	 * commonErrorResponseHandler
	 *  a common function for error response generation and redirect from Middleware
	 *
	 * @param $message
	 * @param string $redirect_to
	 *
	 * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	function commonErrorResponseHandler( $message, $redirect_to = "/" ) {
		if ( Request::ajax() ) {
			if ( Request::server( 'HTTP_REFERER', false ) ) {
				session( [ 'url.intended' => Request::server( 'HTTP_REFERER' ) ] );
			}
			if ( request( 'draw' ) ) {
				//  is datatable request
				return Response::json( [ 'draw'            => intval( request( 'draw' ) ),
				                         'recordsTotal'    => 0,
				                         'recordsFiltered' => 0,
				                         'data'            => [ ],
				                         'error'           => $message,
				                         'redirect_to'     => url( $redirect_to )
				] );
			}

			//  is normal ajax request
			return Response::json( [ 'error' => $message, 'redirect_to' => url( $redirect_to ) ] );
		} else {
			//  is browser request
			return redirect( $redirect_to );
		}
	}
}

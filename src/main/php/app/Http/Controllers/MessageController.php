<?php

namespace HealthSlatePortal\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CurriculumServiceProvider;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\Patient;
use Illuminate\Support\Facades\Log;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\PatientHideUnhide;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\Message;
use HealthSlatePortal\Models\Eloquent\MessageTag;
use HealthSlatePortal\Models\Eloquent\SessionAttendance;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use Edujugon\PushNotification\PushNotification;
use HealthSlatePortal\Helpers\MailServiceHelper;
use HealthSlatePortal\Models\Eloquent\Provider;
use DB;
use Input;
use Validator;
use ChristofferOK\LaravelEmojiOne\LaravelEmojiOne;


class MessageController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $user;
    protected $dataTableService;
    protected $group_model;
    protected $group_member;
    protected $curriculumServiceProvider;
    protected $mailServiceHelper;
    protected $provider;
    protected $data = array();

    function __construct( FacilityModel $facility_model, MemberModel $member_model, Facility $facility, CoacheModel $coach_model,
                          DataTableServiceProvider $dataTableService, User $user, Provider $provider, GroupModel $group_model, GroupMember $group_member, CurriculumServiceProvider $curriculumServiceProvider,MailServiceHelper $mailServiceHelper) {
        $this->facility_model       = $facility_model;
        $this->coach_model          = $coach_model;
        $this->member_model         = $member_model;
        $this->group_member         = $group_member;
        $this->group_model          = $group_model;
        $this->facility             = $facility;
        $this->user                 = $user;
        $this->provider             = $provider;
        $this->dataTableService     = $dataTableService;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->mailServiceHelper = $mailServiceHelper;
        $this->group_db             = env('GROUP_DB_DATABASE');
        $this->active_facility_id   = session('active_facility');
        $this->active_user_id       = session('userId');
        $this->active_user_role     = session('userRole');
        $this->data['common_js']    = array('tagging/jquery-migrate-1.2.1', 'tagging/jquery.caretposition', 'tagging/jquery.sew');

    }


    public function direct_message_to_patient()
    {
        if (request()->ajax()) {
            $user_id            = request('id' , 0);
            $filter_user_id     = request('filter-messages' , 0);
            info('Direct Message Modal Open For USER: '. $user_id);
            if($user_id)
            {
                $response           = $this->member_model->get_member_detail_group_detail(session('active_facility'), $user_id);
                $message_users      = $this->member_model->get_group_users_list_by_group_id(session('active_facility'), $response->group_id);
                $tech_support       = $this->member_model->get_facility_tech_support_list(session('active_facility'));
                $message            = $this->member_model->get_member_provider_chat_detail(session('active_facility'), $user_id, $filter_user_id);

                $group_users_filter = array();
                $tagging_list = array();

                if(!empty($message_users))
                {
                    $food_coach = false;
                    foreach ($message_users as $val)
                    {
                        $group_users_filter[$val->user_id]  = $val->provider_full_name;
                        if($val->user_type != 'Food Coach')
                        $tagging_list[]  = array('name' => $val->first_name, 'type' => $val->user_type);

                        //if($val->user_type == 'Food Coach')
                          //  $food_coach = true;
                    }
                    //if($food_coach)
                      //  $tagging_list[]        = array('name' => 'Food_Coach', 'type' => 'Food Coach');
                }
                if(!empty($tech_support))
                {
                    foreach ($tech_support as $val)
                    {
                        $group_users_filter[$val->user_id]  = $val->provider_full_name;
                        $tagging_list[]        = array('name' => 'Support', 'type' => 'Tech Support');
                        break;
                    }
                }

                $message_data = array();

                $default_profile_image = env('APP_BASE_URL').'/img/default-user.png';

                $read_status = 1;
                if(!empty($message))
                {
                    $timezone = '';
                    if(isset($_COOKIE['user_timezone'])) {
                        $timezone =  $_COOKIE['user_timezone'];
                    }
                    $emoji = new LaravelEmojiOne();
                    foreach($message as $value)
                    {
                        $patient_profile_image = env('APP_BASE_URL').'/img/default-user.png';
                        if($value->patient_profile_image != '')
                        {
                            $patient_profile_image = env('PROFILE_IMAGE_BASE_URL') . $value->patient_profile_image;
                        }


                        $send_on    = Carbon::createFromTimestamp($value->timestamp/1000)->format('M, d Y h:i A');
                        $sent_on    = Carbon::createFromTimestamp($value->timestamp/1000)->format('Y-m-d H:i:s');
                        $sent_on    =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $sent_on)->timezone($timezone)->format('M, d Y h:i A') : $send_on;

                        $chat_image = '';
                        if($value->image_path != '')
                        {    if(strstr($value->image_path, ".pdf"))
                            {

                                $chat_image = '<span class="body"><img class="img-attachment" src="'.env('APP_BASE_URL').'/img/pdf.png'.'"></span>';
                            }
                            else
                            {
                                $chat_image = '<span class="body"><img class="img-attachment" src="'.env('PROFILE_IMAGE_BASE_URL') . $value->image_path.'"></span>';
                            }
                        }
                        if($value->owner == 'PATIENT')
                        {
                            $message_data[] = '<li class="in"><img class="avatar" src="'.$patient_profile_image.'" /><div class="message"><span class="arrow"></span><a class="name"> '.$value->patient_full_name.' </a><span class="datetime"> at '.$sent_on.' </span><span class="body text-left"> '.$emoji->shortnameToImage(nl2br($value->message)).' </span>'.$chat_image.'</div></li>';
                        }
                        else if($value->owner == 'PROVIDER'){
                            $message_data[] = '<li class="out"><img class="avatar" src="'.$default_profile_image.'" /><div class="message"><span class="arrow"></span><a class="name"> '.$value->provider_full_name.' </a><span class="datetime"> at '.$sent_on.' </span><span class="body text-left"> '.$emoji->shortnameToImage(nl2br($value->message)).' </span>'.$chat_image.'</div></li>';
                        }
                    }
                    $message_data = array_reverse($message_data);
                }

                if(count($message_data) == 0)
                {
                    $message_data[] = '<li class="out"><div class="message"><span class="arrow"></span><a class="name">  </a><span class="datetime"> </span><span class="body"> '. trans( 'common.no_message_history' ).' </span></div></li>';
                }

                    //$message_count = Message::where('patient_id',$response->pid)->where('user_id',$this->active_user_id)->where('read_status', DB::raw(0))->where('owner','PATIENT')->count();
                    $message_count = $this->member_model->get_member_unread_message_count_detail($response->pid, $this->active_user_id);
                    if(count($message_count) > 0)
                    {
                       $read_status = 0;
                    }

                $check_engagement   = get_engagement_score($response);
                $color_class        = $check_engagement['color_class'];
                $response->color_class    = $color_class;
                $response->color_status   = $check_engagement['status'];
                $response->status_type    = $check_engagement['status_type'];

                $recommended_message_response   = collect($this->member_model->get_recommended_message_list());
                $in_app_message_list  = collect($this->member_model->get_in_app_message_list([$response->pid]));
                $saved_message_list   = collect($this->member_model->get_my_saved_message_list($response->pid));
                $message_type         = $recommended_message_response->where('status', $response->status_type);
                $message_text = array();
                if(count($in_app_message_list) > 0 && count($message_type) > 0)
                {
                    $count_sent_message = 0;
                    $predefined_message_id = [];
                    foreach ($in_app_message_list as $sent_message)
                    {
                        $check_sent_message = collect($message_type)->where('message_text', $sent_message->message)->first();

                        if(count($check_sent_message) > 0)
                        {
                            $count_sent_message += 1;
                            $predefined_message_id[] = $check_sent_message->predefined_message_id;
                        }
                    }

                    if($count_sent_message != count($message_type))
                    {
                        foreach ($message_type->all() as $check_message)
                        {
                            if(in_array($check_message->predefined_message_id, $predefined_message_id))
                            {
                                continue;
                            }
                            else
                            {
                                $message_text[$check_message->message_text] = str_limit($check_message->message_text,30);
                            }
                        }
                    }

                }
                elseif(count($message_type) > 0)
                {
                    foreach ($message_type->all() as $check_message)
                    {
                        $message_text[$check_message->message_text] = str_limit($check_message->message_text,30);
                    }
                }

                $saved_message_data = array();

                if(count($saved_message_list) > 0)
                {
                    foreach ($saved_message_list as $saved_message)
                    {
                        $saved_message->message = preg_replace('/@(\w+)/', '', $saved_message->message);
                        $saved_message->message = str_replace('<span></span>', '', $saved_message->message);
                        $saved_message->message = trim($saved_message->message);
                        $saved_message_data[$saved_message->message] = str_limit($saved_message->message,30);
                    }
                }

                $member_detail = array();
                if(!empty($response))
                {
                    $profile_image = env('APP_BASE_URL').'/img/default-user.png';
                    if($response->patient_profile_image != '')
                    {
                        $profile_image = env('PROFILE_IMAGE_BASE_URL') . $response->patient_profile_image;
                    }
                    $member_detail['name']   = $response->full_name .'-'. $response->patient_id;
                    $member_detail['image']  = $profile_image;
                    $member_detail['patient_id']= $response->patient_id;
                    $member_detail['last_message_id'] = @$message[0]->message_id;
                }



                return response()->json([
                    'success' => 'true',
                    'member_detail' => $member_detail,
                    'message' => $message_data,
                    'provider_list' =>$group_users_filter,
                    'tagging_list' => $tagging_list,
                    'markread_status' => $read_status,
                    'recommended_message' => $message_text,
                    'saved_message' => $saved_message_data
                ]);
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'get detail'])
                ]);
            }
        }
    }


    public function save_direct_message(Request $request)
    {
        // file attachment validation

        if($this->active_user_role == 'facilityadmin' || $this->active_user_role == 'admin' )
        {
            return response()->json([
                'error' => 'Unable to send message'
            ]);
        }

        if($request->hasFile('uploadfile')) {

            $validator = Validator::make($request->all(), [
                'uploadfile' => 'mimes:jpg,jpeg,png,pdf'
            ]);

            if ($validator->fails()) {
                return response()->json(
                    [
                        'error' => 'Please upload only image and pdf file'
                    ]
                );
            }
        }

        // message required  validation
        $validator = Validator::make($request->all(), [
        'send_message'   => 'required'
       ]);

        if ($validator->fails()) {
            return response()->json(
                [
                    'error' => 'Please write somthing'
                ]
            );
        }

        if (request()->ajax()) {
            $user_id        = request('id' , 0);
            $send_message   = request('send_message' , '');
            $save_message   = request('submit', '');
            info('Sending Direct Message To USER: '. $user_id);

            if($user_id != '')
            {
                $response                     = $this->member_model->get_member_detail_provider_detail(session('active_facility'), $user_id);
                $member_detail_group_detail   = $this->member_model->get_member_detail_group_detail(session('active_facility'), $user_id);

                if(empty($response))
                {
                    return response()->json([
                        'error' => 'Member detail not found'
                    ]);
                }
                if(!empty($member_detail_group_detail->lead_coach_id) || !empty($response->facility_default_lead_coach_id))
                {

                    $url            = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
                    $send_message   = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $send_message);
                    // this regular expresion add span tag in @
                    $send_message   = preg_replace('/@(\w+)/', '<span>@${1}</span> ', $send_message);
                    $sent_message_attachment ="";
                    $file_extension ="";
                    info('Sending Direct Message To Patient: '. $response->patient_id . 'By UserID: ' . $this->active_user_id);
                    // if($response->lead_coach_id == $this->active_user_id)
                    $sent_message = $send_message;
                    /// else
                       // $sent_message = 'By ' . session('user')->firstName .' '. session('user')->lastName .' : '. $send_message;

                    if($request->hasFile('uploadfile'))
                    {

                        $file = $request->file('uploadfile');
                        if($file->getClientOriginalExtension() == 'pdf')
                            $destinationPath =  config('healthslate.log_image_save_url').'pdf/';
                        else
                            $destinationPath =  config('healthslate.log_image_save_url').'logImages/';
                        $file_attachment_name = md5(time() . uniqid()).'.'.$file->getClientOriginalExtension();
                        $file->move($destinationPath, $file_attachment_name);
                        info('Saving Attached File to Folder: '. $file_attachment_name);
                    }
                    $tagged_member_ids = array();
                    if(isset($response->user_group_id))
                    {
                        $message_users      = $this->member_model->get_group_users_list_by_group_id(session('active_facility'), $response->user_group_id);
                        $tech_support       = $this->member_model->get_facility_tech_support_list(session('active_facility'));

                        $group_users_filter = array();
                        if(!empty($message_users))
                        {
                            foreach ($message_users as $val)
                            {
                                if (strpos($sent_message, '@' . $val->first_name) !== false) {
                                    $group_users_filter[]  = array('name' => $val->first_name, 'type' => $val->user_type, 'user_id' => $val->user_id, 'email' => $val->email);
                                    $sent_message = str_replace('@' . $val->first_name, '<span>@' . $val->first_name . '</span>', $sent_message);
                                    $tagged_member_ids[] = $val->user_id;
                                }
                            }
                            if (strpos($sent_message, '@Food_Coach') !== false) {
                                $sent_message = str_replace('@Food_Coach', '<span>@Food_Coach</span>', $sent_message);
                            }
                        }
                        if(!empty($tech_support))
                        {
                            foreach ($tech_support as $val)
                            {
                                if (strpos($sent_message, '@Support') !== false) {
                                    $group_users_filter[]  = array('name' => $val->first_name, 'type' => $val->user_type, 'user_id' => $val->user_id, 'email' => $val->email);
                                    $sent_message = str_replace('@Support', '<span>@Support</span>', $sent_message);
                                    $tagged_member_ids[] = 'Support';
                                }
                                break;
                            }
                        }
                    }

                    $emoji = new LaravelEmojiOne();
                    $android_payload_image_path = '';
                    $sent_message_for_push_notification = $sent_message;
                    $message_id = new Message;
                    $message_id->message        = $emoji->toShort($send_message);
                    if($this->active_user_role == 'techsupport')
                        $message_id->owner      = 'Tech Support';
                    else
                        $message_id->owner      = 'PROVIDER';
                    $message_id->read_status    = DB::raw(0);
                    $message_id->timestamp      = (time() * 1000);
                    $message_id->patient_id     = $response->patient_id;
                    $message_id->user_id        = $this->active_user_id;

                    if($save_message == 'save')
                    {
                        $message_id->saved_message    = 1;
                    }
                    if($request->hasFile('uploadfile')) {

                        if($file->getClientOriginalExtension() == 'pdf') {
                            $message_id->image_path = '/pdf/' . $file_attachment_name;
                            $sent_message_attachment = '<span class="body"><img class="img-attachment" src="'.env('APP_BASE_URL').'/img/pdf.png'.'"></span>';
                            $android_payload_image_path = env('APP_BASE_URL') .$message_id->image_path;
                        }
                        else {
                            $message_id->image_path = '/logImages/' . $file_attachment_name;
                            $sent_message_attachment = '<span class="body"><img class="img-attachment" src="'.env('PROFILE_IMAGE_BASE_URL') .$message_id->image_path.'"></span>';
                            $android_payload_image_path = env('PROFILE_IMAGE_BASE_URL') .$message_id->image_path;
                        }
                        $file_extension = $file->getClientOriginalExtension();
                    }
                    $message_id->save();
                    $owner=$message_id->owner;

                    // Insert recors into Message Tagged Table
                    if(!empty($tagged_member_ids) && $message_id->message_id != '')
                    {
                        foreach ($tagged_member_ids as $id)
                        {
                            if($id == 'Support')
                            {
                                $message_tag = new MessageTag;
                                $message_tag->message_id     = $message_id->message_id;
                                $message_tag->user_id        = null;
                                $message_tag->role           = 'Support';
                                $message_tag->created_on     = (time() * 1000);
                                $message_tag->save();
                            }
                            else
                            {
                                $message_tag = new MessageTag;
                                $message_tag->message_id     = $message_id->message_id;
                                $message_tag->user_id        = $id;
                                $message_tag->role           = null;
                                if($id == $this->active_user_id)
                                    $message_tag->is_read    = 1;
                                $message_tag->created_on     = (time() * 1000);
                                $message_tag->save();
                            }
                        }
                    }

                    if($file_extension!="")
                    {
                      if($file_extension == "pdf")
                          $sent_message_for_push_notification = session('user')->firstName.": \xf0\x9f\x93\x84 ".$sent_message;
                      elseif($file_extension == "jpg" || $file_extension == "jpeg" || $file_extension == "png")
                          $sent_message_for_push_notification = session('user')->firstName.": \xf0\x9f\x93\xb7 ".$sent_message;

                    }

                    // Push notification Start
                    if($response->device_type != "" && $response->is_in_app_message_notify == 1) {
                        info('Sending Push notification to User: ' . $response->patient_id . ' Device Type: ' . $response->device_type);
                        if ($response->registration_id != '' && $response->device_type == 'Android') {

                            if(!empty($this->active_user_id))
                            {
                                $provider_id  = $this->provider->where('user_id', $this->active_user_id)->first();
                                $provider_id  = $provider_id->provider_id;
                            }

                            if($android_payload_image_path != '')
                                $android_payload = array('providerId' => $provider_id, 'messageText' => $sent_message, 'isInAppMessageNotify' => true, 'senderFirstName' => session('user')->firstName, 'imagePath' => $android_payload_image_path);
                            else
                                $android_payload = array('providerId' => $provider_id, 'messageText' => $sent_message, 'isInAppMessageNotify' => true, 'senderFirstName' => session('user')->firstName);

                            $push = new PushNotification('gcm');
                            $push->setMessage(['data' => ['data' => ['actions' => 'message', 'payload' => json_encode($android_payload) ],],])->setDevicesToken($response->registration_id)->send();
                            Log::info('Notification Response: ' . print_r($push->getFeedback(), true));
                        } else
                         if ($response->registration_id != '' && $response->device_type == 'IOS') {

                             if(!empty($this->active_user_id))
                             {
                                 $provider_id  = $this->provider->where('user_id', $this->active_user_id)->first();
                                 $provider_id  = $provider_id->provider_id;
                             }
                             $data_array = json_encode(array('providerId' => $provider_id));
                             $push = new PushNotification('apn');
                             $push->setMessage(['aps' => ['alert' => $sent_message_for_push_notification,  'category' => 'IN_APP_MESSAGE','sound' => 'default'], 'event' => 'message', 'data' => $data_array ])->setDevicesToken($response->registration_id)->send();
                             Log::info('Notification Response: ' . print_r($push->getFeedback(), true));
                        }
                    }
                    // Push notification End

                    if($response->patient_send_email_notify == 1)
                    {
                        $message_get_data = Message::where('patient_id', $response->patient_id)->orderBy( 'timestamp', 'desc')->limit(1)->first();
                        if(count($message_get_data) > 0)
                        {
                            $time = ($message_get_data->timestamp/1000);
                            if(date("Y-m-d ", $time) < date("Y-m-d ",time()))
                            {
                                info('Sending Mail to Patient: '. $response->patient_id );
                                $name= $response->first_name;
                                if(Session::get('user')->cobrandId==2){
                                    $message = "You have received a new message from your health coach. You can see it on the web here or by opening your app and looking for the red message indicator.If you need assistance please use the Help page in the app or call us at 888-291-7245.You can stop receiving email notification of coach messages by going in your SoleraOne app to Menu/Settings/Coach Communication. ";

                                }else{
                                    $message = "You have received a new message from your health coach. You can see it on the web here or by opening your app and looking for the red message indicator.If you need assistance please use the Help page in the app or call us at 888-291-7245.You can stop receiving email notification of coach messages by going in your HealthSlate app to Menu/Settings/Coach Communication. ";

                                }
                                $body = ['name'=>$name, 'content' => $message,'subject' =>'Message from coach' ];
                                $to = $response->email;
                                $this->mailServiceHelper->sendEmail($body, trim($to));
                            }
                        }
                    }

                    $timezone = '';
                    if(isset($_COOKIE['user_timezone'])) {
                        $timezone =  $_COOKIE['user_timezone'];
                    }

                    $send_on    = Carbon::createFromTimestamp(time())->format('M, d Y h:i A');
                    $sent_on    =  ( ! empty( $timezone ) ) ? Carbon::createFromTimestamp(time())->timezone($timezone)->format('M, d Y h:i A') : $send_on;

                    $patient_profile_image = env('APP_BASE_URL').'/img/default-user.png';
                    if($response->patient_profile_image != '')
                    {
                        $patient_profile_image = env('PROFILE_IMAGE_BASE_URL') . $response->patient_profile_image;
                    }
                    if($owner='PROVIDER'){
                        $patient_profile_image = env('APP_BASE_URL').'/img/default-user.png';
                    }

                    $emoji = new LaravelEmojiOne();
                    $sent_message = $emoji->unicodeToImage($send_message);

                    $message_data  = '<li class="out"><img class="avatar" alt="" src="'.$patient_profile_image.'" /><div class="message"><span class="arrow"></span><a class="name"> '.session('full_name').' </a><span class="datetime"> at '.$sent_on.' </span><span class="body"> '.nl2br($sent_message).' </span>'.$sent_message_attachment.'</div></li>';
                    info('Direct Message Sent To Patient: '. $response->patient_id . 'By UserID: ' . $this->active_user_id);
                    // for saave direct message
                    $saved_message_data = array();
                    if($response->patient_id)
                    {
                        $saved_message_list   = collect($this->member_model->get_my_saved_message_list($response->patient_id));
                        if(count($saved_message_list) > 0)
                        {
                            foreach ($saved_message_list as $saved_message)
                            {
                                $saved_message->message = preg_replace('/@(\w+)/', '', $saved_message->message);
                                $saved_message->message = str_replace('<span></span>', '', $saved_message->message);
                                $saved_message->message = trim($saved_message->message);
                                $saved_message_data[$saved_message->message] = str_limit($saved_message->message,30);
                            }
                        }

                    }



                    return $response = [
                        'success' => $message_data,
                        'last_message_id' => $message_id->message_id,
                        'saved_message' => $saved_message_data
                    ];
                }
                else
                {
                    return response()->json([
                        'error' => 'Member Lifestyle Coach not found!'
                    ]);
                }
            }
        }
    }


    public function reload_direct_message()
    {
        if (request()->ajax()) {
            $user_id             = request('id' , 0);
            $filter_user_id      = request('filter-messages' , 0);
            $last_message_id     = request('last_message_id' , 0);
            info('Reload Message For USER: '. $user_id);
            if($user_id)
            {
                $response           = $this->member_model->get_member_detail_group_detail(session('active_facility'), $user_id);
                $message            = $this->member_model->get_member_provider_chat_detail(session('active_facility'), $user_id, $filter_user_id, $last_message_id);

                $message_data = array();

                $default_profile_image = env('APP_BASE_URL').'/img/default-user.png';

                if(!empty($message))
                {
                    $timezone = '';
                    if(isset($_COOKIE['user_timezone'])) {
                        $timezone =  $_COOKIE['user_timezone'];
                    }
                    $emoji = new LaravelEmojiOne();
                    foreach($message as $value)
                    {
                        $patient_profile_image = env('APP_BASE_URL').'/img/default-user.png';
                        if($value->patient_profile_image != '')
                        {
                            $patient_profile_image = env('PROFILE_IMAGE_BASE_URL') . $value->patient_profile_image;
                        }

                        $send_on    = Carbon::createFromTimestamp($value->timestamp/1000)->format('M, d Y h:i A');
                        $sent_on    = Carbon::createFromTimestamp($value->timestamp/1000)->format('Y-m-d H:i:s');
                        $sent_on    =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $sent_on)->timezone($timezone)->format('M, d Y h:i A') : $send_on;

                        $chat_image = '';
                        if($value->image_path != '')
                        {
                            if(strstr($value->image_path, ".pdf"))
                            {
                                $chat_image = '<span class="body"><img class="img-attachment" src="'.env('APP_BASE_URL').'/img/pdf.png'.'"></span>';
                            }
                            else
                            {
                                $chat_image = '<span class="body"><img class="img-attachment" src="'.env('PROFILE_IMAGE_BASE_URL') . $value->image_path.'"></span>';
                            }
                        }
                        if($value->owner == 'PATIENT')
                        {
                            $message_data[] = '<li class="in"><img class="avatar" src="'.$patient_profile_image.'" /><div class="message"><span class="arrow"></span><a class="name"> '.$value->patient_full_name.' </a><span class="datetime"> at '.$sent_on.' </span><span class="body"> '.$emoji->shortnameToImage($value->message).' </span>'.$chat_image.'</div></li>';
                        }
                        else if($value->owner == 'PROVIDER'){
                            $message_data[] = '<li class="out"><img class="avatar" src="'.$default_profile_image.'" /><div class="message"><span class="arrow"></span><a class="name"> '.$value->provider_full_name.' </a><span class="datetime"> at '.$sent_on.' </span><span class="body"> '.$emoji->shortnameToImage($value->message).' </span>'.$chat_image.'</div></li>';
                        }
                    }
                    $message_data = array_reverse($message_data);
                }

                /* code for on load pop up read message*/
                   $meaaage_id = @$message[0]->message_id;
                   if($meaaage_id !=''){
                    if($this->active_user_role == 'techsupport' && $meaaage_id > 0)
                    {
                        MessageTag::where('message_id',$meaaage_id )->where('role', 'Support')->update(['is_read' => DB::raw(1)]);
                    }
                    elseif($meaaage_id > 0)
                    {
                        MessageTag::where('message_id', $meaaage_id)->where('user_id', $this->active_user_id)->update(['is_read' => DB::raw(1)]);
                    }
                       Message::where('patient_id', $response->patient_id)->where('user_id', $this->active_user_id)->update(['read_status' => DB::raw(1)]);
                }


                /* code for on load pop up read message*/
                $member_detail = array();
                if(!empty($response))
                {
                    $profile_image = env('APP_BASE_URL').'/img/default-user.png';
                    if($response->patient_profile_image != '')
                    {
                        $profile_image = env('PROFILE_IMAGE_BASE_URL') . $response->patient_profile_image;
                    }
                    $member_detail['name']   = $response->full_name .'-'. $response->patient_id;
                    $member_detail['image']  = $profile_image;
                    $member_detail['patient_id']= $response->patient_id;
                    $member_detail['last_message_id'] = @$message[0]->message_id;
                }

                return response()->json([
                    'success' => 'true',
                    'member_detail' => $member_detail,
                    'message' => $message_data,
                ]);
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'get detail'])
                ]);
            }
        }
    }


    public function direct_message_to_patient_full_page($user_id)
    {

        $this->data['user_id'] = base64_decode($user_id);
        $this->data['js'] = array('message');
        return view( 'common.direct_message_full_page', $this->data);
    }

    /* @param $request
     * @desc Mark as read all messages of the loggedin Member
     *
     */
    public function markReadMessage(Request $request)
    {

        $total_messages = $this->member_model->get_member_unread_message_count_detail($request->patient_id, $this->active_user_id);
        if(!empty($total_messages))
        {
            $ids = array();
            foreach ($total_messages as $message_id)
                $ids[] = $message_id->message_id;

            if($this->active_user_role == 'techsupport' && count($ids) > 0)
            {
                MessageTag::whereIn('message_id', $ids)->where('role', 'Support')->update(['is_read' => DB::raw(1)]);
            }
            elseif(count($ids) > 0)
            {
                MessageTag::whereIn('message_id', $ids)->where('user_id', $this->active_user_id)->update(['is_read' => DB::raw(1)]);
            }
        }
        Message::where('patient_id', $request->patient_id)->where('user_id', $this->active_user_id)->update(['read_status' => DB::raw(1)]);

        return response()->json([
            'success' => 'Mark as read successfully'
        ]);
    }


}//End of Class

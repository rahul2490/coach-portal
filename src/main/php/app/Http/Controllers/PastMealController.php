<?php

namespace HealthSlatePortal\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CurriculumServiceProvider;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\PatientModel;
use HealthSlatePortal\Models\Eloquent\Patient;
use Illuminate\Support\Facades\Log;
use HealthSlatePortal\Models\Eloquent\Post;
use HealthSlatePortal\Models\Eloquent\PostComment;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\PatientHideUnhide;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\Message;
use HealthSlatePortal\Models\Eloquent\LogModel;
use HealthSlatePortal\Models\Eloquent\SessionAttendance;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\PastMealModel;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use Edujugon\PushNotification\PushNotification;
use HealthSlatePortal\Helpers\MailServiceHelper;
use HealthSlatePortal\Models\DashboardSettingModel;

use DB;
use Input;
use Validator;


class PastMealController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $user;
    protected $dataTableService;
    protected $group_model;
    protected $past_meal_model;
    protected $group_member;
    protected $curriculumServiceProvider;
    protected $mailServiceHelper;
    protected $patient_model;
    protected  $dashboard_setting_model;
    protected $data = array();

    function __construct( PatientModel $patient_model,FacilityModel $facility_model, MemberModel $member_model, Facility $facility, CoacheModel $coach_model,
                          DataTableServiceProvider $dataTableService, User $user, GroupModel $group_model,PastMealModel $past_meal_model, GroupMember $group_member, CurriculumServiceProvider $curriculumServiceProvider,MailServiceHelper $mailServiceHelper,DashboardSettingModel $dashboard_setting_model) {
        $this->dashboard_setting_model = $dashboard_setting_model;
        $this->facility_model       = $facility_model;
        $this->patient_model       = $patient_model;
        $this->coach_model          = $coach_model;
        $this->member_model         = $member_model;
        $this->group_member         = $group_member;
        $this->group_model          = $group_model;
        $this->past_meal_model      = $past_meal_model;
        $this->facility             = $facility;
        $this->user                 = $user;
        $this->dataTableService     = $dataTableService;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->mailServiceHelper = $mailServiceHelper;
        $this->group_db             = env('GROUP_DB_DATABASE');
        $this->active_facility_id   = session('active_facility');
        $this->active_user_id       = session('userId');
        $this->active_user_role     = session('userRole');
       // $this->data['highchart_js'] = array('highcharts','highcharts-3d','highcharts-more');
        $this->data['common_js']    = array('tagging/jquery-migrate-1.2.1', 'tagging/jquery.caretposition', 'tagging/jquery.sew','meal','dashboard');



    }


    function index($patientId ='')
    {
        $timezone = '';
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }
        $this->data['patient_id'] = $patientId;
       // $this->data['patient_info'] = $this->patient_model->get_patient_info($patientId, $this->active_facility_id);
        $patient_info = $this->patient_model->get_patient_info($patientId, $this->active_facility_id);
        $this->data['patient_info'] = $patient_info;

        if(empty($patient_info->patient_id))
        {
            return redirect()->intended('/'.$this->active_user_role.'/members');
        }
        $this->data['group_link'] = "";
        $this->data['session'] = "" ;
        $this->data['total_post']='';

        if($patient_info->group_id !="" )
        {
            $group_id   = array($patient_info->group_id);
            $is_person  = $patient_info->is_person;

            $completed_week         = $patient_info->completed_week;
            $group_session_detail   = collect($this->group_model->get_group_current_session_name($group_id));

            $curriculum_for_dpp     = '';
            $curriculum_for_dpp2    = '';

            if ($is_person == 1) {
                // Calling Curriculam server for Section list for both type DPP and DPP2
                $curriculum_program_type_name = $group_session_detail->unique('curriculum_program_type_name')->pluck('curriculum_program_type_name' )->toArray();
                //print_r(($curriculum_program_type_name)); exit;
                if(count($curriculum_program_type_name) > 0)
                {
                    foreach ($curriculum_program_type_name as $value)
                    {
                        if($value == 'DPP2' || $value == 'DPP')
                        {
                            $curriculum = [
                                (object) [ 'uuid' => 'inperson', 'curriculum_program_type_name' => $value],
                            ];

                            if($value == 'DPP')
                                $curriculum_for_dpp  = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                            if($value == 'DPP2')
                                $curriculum_for_dpp2 = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                        }
                    }
                }
            }

            if($completed_week == 0)
                $completed_week = 1;
            $total_sessions  = array();
            if($is_person != 1)
                $group_link = route('leadcoach.group-detail', ['group_id' => $patient_info->group_id]);
            else
            {
                $group_link = route('leadcoach.group-session', ['group_id' => $patient_info->group_id]);
                $week_group = 0;

                $current_session_name = '';
                if(count($group_session_detail) > 0)
                {
                    $group_current_session = $group_session_detail->where('group_id', $patient_info->group_id)->last();
                    //print_r($group_current_session);
                    if(!empty($group_current_session->topic_id))
                    {
                        if($curriculum_for_dpp != '')
                        {
                            $total_sessions = $curriculum_for_dpp;
                            $current_session_name = $curriculum_for_dpp->where('section_uuid', $group_current_session->topic_id)->toArray();
                        }

                        if($curriculum_for_dpp2 != '')
                        {
                            $total_sessions = $curriculum_for_dpp2;
                            $current_session_name = $curriculum_for_dpp2->where('section_uuid', $group_current_session->topic_id)->toArray();
                        }


                        if(!empty($current_session_name))
                        {
                            foreach ($current_session_name as $val)
                                $week_group =  $val->display_week;
                        }
                    }

                }
            }

            $this->data['group_link']  = $group_link;
            if($is_person == 1)
                $this->data['session'] = $week_group."/".count($total_sessions) ;
            else
                $this->data['session'] = "NA" ;

            // Batch Count Start
            $read_count = GroupMember::where('group_id', $group_id)->where('user_id',$this->active_user_id )->first();
            if(!empty($read_count))
            {
                $post_read_time =  $read_count->post_read_time;
                $user_id= $this->active_user_id;
                $group_post = $this->group_model->get_post_read_count($group_id,$post_read_time,$user_id);
            }
            if(!empty($group_post)){
                $count_post=$group_post[0]->total_post;
                if($count_post==0){
                    $count_post='';
                }
            }
            else{
                $count_post='';
            }
            $this->data['total_post'] = $count_post;
            // Batch Count End
        }

        $this->data['profile_detail'] = $this->dashboard_setting_model->getProfileDetail($patientId);
        $this->data['state'] = state();

        $now = Carbon::now();
        if(!empty($timezone))
            $now = Carbon::createFromFormat('Y-m-d H:i:s', $now)->timezone($timezone);
        else
            $now = Carbon::createFromFormat('Y-m-d H:i:s', $now);
        $week_end      = $now->copy()->endOfDay();
        $week_start    = $now->copy()->subDays(6)->startOfDay();

        $month_end   = $now->copy()->startOfDay();
        $month_start   = $now->copy()->subMonth();

        $date_array = [
            'week_start'  =>  $week_start->format('m/d/Y'),
            'week_end'    =>  $week_end->format('m/d/Y'),
            'month_start'  => $month_start->format('m/d/Y'),
            'month_end'    => $month_end->format('m/d/Y'),

        ];
        $this->data['date_array'] = $date_array;

        $patient_data = Patient::where('patient_id',$patientId)->first();
        $patient_type = $patient_data->diabetes_type_id;
        $user_id =  $patient_data->user_id;
        $user_data = User::where('user_id',$user_id)->first();
        $offset_key  = $user_data->offset_key;
        $this->data['user_timezone']  = $user_data->offset_key;
        $this->data['patient_type']= $patient_type;

        if(!empty($offset_key))
        {
            $start = Carbon::createFromFormat('Y-m-d H:i:s', $week_start,$offset_key)->setTimezone('UTC');
            $end   = Carbon::createFromFormat('Y-m-d H:i:s', $week_end,$offset_key)->setTimezone('UTC');

        }
        else
        {
        $start = Carbon::createFromFormat('Y-m-d H:i:s', $week_start);
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $week_end);

       }
        $current_date = strtotime($start)*1000;
        $compare_time = strtotime($end)*1000;

        $this->data['past_meal_data']=$this->past_meal_model->get_past_meal($patientId,$current_date,$compare_time,$meal_type='all');
        $this->data['patient_id']=$patientId;
        $this->data['past_meal']=array();
        //print_r(  $this->data['past_meal_data']);die;
        foreach($this->data['past_meal_data'] as $meals){
              if($meals->image_name !=''){
                  $meals->image_name =env('PROFILE_IMAGE_BASE_URL').'logImages/'.$meals->image_name;
              }else if($meals->meal_name=='SKIPPED_MEAL'){
                  $meals->meal_name='SKIPPED MEAL';
                  $meals->image_name=env('APP_BASE_URL').'/img/skipped_meal.svg';
              }
              else{
                  $meals->image_name=env('APP_BASE_URL').'/img/no_image_available.svg';
              }
//              if($meals->log_time!=''){
//                  $co_date= ($meals->log_time/1000);
//                  $meals->log_time =date("d/m/Y h:i A ",$co_date);
//              }
            $past_meal_data=   array(
             'log_id'=> $meals->log_id,
            'image_name' => $meals->image_name,
            'is_processed' =>$meals->is_processed,
            'log_time' => $meals->date.' ' .$meals->log_time,
            'log_time_offset' =>$meals->log_time_offset,
            'offset_key'=>$meals->offset_key,
            'type' => $meals->type,
            'meal_name'=> $meals->meal_name,
            'fats' => $meals->fats,
            'carbs' => $meals->carbs,
            'has_missing_food' => $meals->has_missing_food,
            'no_of_servings'=> $meals->no_of_servings,
            'serving_size_unit' => $meals->serving_size_unit,
            'first_name' => $meals->first_name,
            'last_name' => $meals->last_name,
            'calories' => $meals->calories,
            'unit_from_food_master' => $meals->unit_from_food_master,
             'is_removed'=>$meals->is_removed,
        );

            array_push($this->data['past_meal'],$past_meal_data);

        }
        return view( 'common.past_meal', $this->data );
    }

    public function past_meal_by_month()
    {
        $patient_id = request('patient_id');
        $timezone = '';
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }
        $current_date = strtotime(date('Y-m-d h:i:s')) * 1000;
        $compare_time = strtotime("-1 month") * 1000;
        $this->data['past_meal_data'] = $this->past_meal_model->get_past_meal($patient_id, $current_date, $compare_time, $meal_type = 0);
        $this->data['past_meal'] = array();
        foreach ($this->data['past_meal_data'] as $meals) {
            if ($meals->image_name != '') {
                $meals->image_name = env('PROFILE_IMAGE_BASE_URL') . 'logImages/' . $meals->image_name;
            }else if($meals->meal_name=='SKIPPED_MEAL'){
                $meals->image_name=env('APP_BASE_URL').'/img/skipped_meal.svg';
            } else {
                $meals->image_name = env('APP_BASE_URL') . '/img/no_image_available.svg';
            }
            if ($meals->log_time != '') {
                $co_date = ($meals->log_time / 1000);
                $meals->log_time = date("d/m/Y h:i A", $co_date);
            }
            $past_meal_data = array(
                'log_id' => $meals->log_id,
                'image_name' => $meals->image_name,
                'is_processed' => $meals->is_processed,
                'log_time' => $meals->log_time,
                'log_time_offset' => $meals->log_time_offset,
                'offset_key' => $meals->offset_key,
                'type' => $meals->type,
                'meal_name' => $meals->meal_name,
                'fats' => $meals->fats,
                'carbs' => $meals->carbs,
                'has_missing_food' => $meals->has_missing_food,
                'no_of_servings' => $meals->no_of_servings,
                'serving_size_unit' => $meals->serving_size_unit,
                'first_name' => $meals->first_name,
                'last_name' => $meals->last_name,
                'calories' => $meals->calories,
                'unit_from_food_master' => $meals->unit_from_food_master,
            );

            array_push($this->data['past_meal'], $past_meal_data);

        }


        $return_html = '';
        foreach ($this->data['past_meal'] as $meal) {
            $route="'".route('pastmeal/edit_past_meal',['logId' => $meal['log_id']])."'";
            $return_html .= '
              <div class="col-xs-6 col-sm-6 col-lg-3" id="meal_' . $meal['log_id'] . '">

               <div class="btn btn-block btn-default btn-bg" >
               <img height="300px" src="' . $meal['image_name'] . '" class="responsive" width="auto" />
               </div>
               <div class="overlay" onclick="location.href='.$route.'">
                <a class="badge badge-danger delete_meal" data-log-id="' . $meal['log_id'] . '"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
                <div class="log-info">
                <h5> <strong>' . $meal['calories'] . '</strong> <small> ' . $meal['unit_from_food_master'] . ' </small></h5>
                 <h6> ' . $meal['meal_name'] . ' </h6>
                  </div>
                   </div>
                   <p class="text-center font-dark"><span class=" sbold font-md"> ' . $meal['type'] . '</span>  <br/> <span class="text-center font-dark ">Processed By: ' . $meal['first_name'] . ' ' . $meal['last_name'] . '  <br/><span class=" font-sm font-grey-salsa"><i class="fa fa-clock-o"></i>  ' . $meal['log_time'] . ' </span></span></p>


           </div>';
        }


        return $return_html;


    }

    public function past_meal_by_week(){
        if (request()->ajax()) {
            $patient_id = request('patient_id');
            $timezone = '';
            if (isset($_COOKIE['user_timezone'])) {
                $timezone = $_COOKIE['user_timezone'];
            }
            $current_date = strtotime(date('Y-m-d h:i:s')) * 1000;
            $compare_time = strtotime("-1 week") * 1000;
            $this->data['past_meal_data'] = $this->past_meal_model->get_past_meal($patient_id, $current_date, $compare_time, $meal_type = 0);
            $this->data['past_meal'] = array();


            foreach ($this->data['past_meal_data'] as $meals) {
                if ($meals->image_name != '') {
                    $meals->image_name = env('PROFILE_IMAGE_BASE_URL') . 'logImages/' . $meals->image_name;
                }else if($meals->meal_name=='SKIPPED_MEAL'){
                    $meals->image_name=env('APP_BASE_URL').'/img/skipped_meal.svg';
                } else {
                    $meals->image_name = env('APP_BASE_URL') . '/img/no_image_available.svg';
                }
                if ($meals->log_time != '') {
                    $co_date = ($meals->log_time / 1000);
                    $meals->log_time = date("d/m/Y h:i A", $co_date);
                }
                $past_meal_data = array(
                    'log_id' => $meals->log_id,
                    'image_name' => $meals->image_name,
                    'is_processed' => $meals->is_processed,
                    'log_time' => $meals->log_time,
                    'log_time_offset' => $meals->log_time_offset,
                    'offset_key' => $meals->offset_key,
                    'type' => $meals->type,
                    'meal_name' => $meals->meal_name,
                    'fats' => $meals->fats,
                    'carbs' => $meals->carbs,
                    'has_missing_food' => $meals->has_missing_food,
                    'no_of_servings' => $meals->no_of_servings,
                    'serving_size_unit' => $meals->serving_size_unit,
                    'first_name' => $meals->first_name,
                    'last_name' => $meals->last_name,
                    'calories' => $meals->calories,
                    'unit_from_food_master' => $meals->unit_from_food_master,
                );

                array_push($this->data['past_meal'], $past_meal_data);

            }


            $return_html = '';
            foreach ($this->data['past_meal'] as $meal) {
                $route="'".route('pastmeal/edit_past_meal',['logId' => $meal['log_id']])."'";
                $return_html .= '
              <div class="col-xs-6 col-sm-6 col-lg-3" id="meal_' . $meal['log_id'] . '">

               <div class="btn btn-block btn-default btn-bg" >
               <img height="300px" src="' . $meal['image_name'] . '" class="responsive" width="auto" />
               </div>
               <div class="overlay" onclick="location.href='.$route.'">
               <a class="badge badge-danger delete_meal" data-log-id="' . $meal['log_id'] . '"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
                <div class="log-info">
                <h5> <strong>' . $meal['calories'] . '</strong> <small> ' . $meal['unit_from_food_master'] . '</small></h5>
                 <h6> ' . $meal['meal_name'] . ' </h6>
                  </div>
                   </div>
                   <p class="text-center font-dark"><span class=" sbold font-md"> ' . $meal['type'] . '</span>  <br/> <span class="text-center font-dark ">Processed By: ' . $meal['first_name'] . ' ' . $meal['last_name'] . ' <br/><span class=" font-sm font-grey-salsa"><i class="fa fa-clock-o"></i>  ' . $meal['log_time'] . ' </span></span></p>


           </div>';
            }


            return $return_html;
        }

    }

    public function past_meal_by_filter(Request $request)
    {
        if (request()->ajax()) {
            $timezone = '';
            if (isset($_COOKIE['user_timezone'])) {
                $timezone = $_COOKIE['user_timezone'];
            }
            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $meal_type        =  $request->input('meal_type');
            $clicked        =  $request->input('clicked');
            $date = explode(' - ', $date);

            $start = Carbon::createFromFormat('m/d/Y', $date[0])->setTime(0, 0, 0);
            $end = Carbon::createFromFormat('m/d/Y', $date[1])->setTime(23, 59, 59);


//            if($clicked == 'right')
//            {
//                $daily_start  = $start->copy()->addDays(7);
//                $daily_end    = $end->copy()->addDays(7);
//            }
//            else
//            {
//                $daily_start  = $start->copy()->subDays(7);
//                $daily_end    = $end->copy()->subDays(7);
//            }
            $patient_data = Patient::where('patient_id',$patient_id)->first();
            $patient_type = $patient_data->diabetes_type_id;
            $user_id =  $patient_data->user_id;
            $user_data = User::where('user_id',$user_id)->first();
            $offset_key  = $user_data->offset_key;
            $this->data['user_timezone']  = $user_data->offset_key;
            $this->data['patient_type']= $patient_type;

//            $patient_data = Patient::where('patient_id',$patient_id)->first();
//            $user_id = $patient_data->user_id; $user_data = User::where('user_id',$user_id)->first();
//            $user_timezone = $user_data->offset_key;
//            $offset_key = $user_data->offset_key;
            if(!empty($offset_key)) {
                $start = Carbon::createFromFormat('Y-m-d H:i:s', $start, $offset_key)->setTimezone('UTC');
                $end = Carbon::createFromFormat('Y-m-d H:i:s', $end, $offset_key)->setTimezone('UTC');
            }
            $current_date = strtotime($start)*1000;
            $compare_time = strtotime($end)*1000;

            $this->data['past_meal_data'] = $this->past_meal_model->get_past_meal($patient_id,$current_date, $compare_time, $meal_type);
            $this->data['past_meal'] = array();


            foreach ($this->data['past_meal_data'] as $meals) {
                if ($meals->image_name != '') {
                    $meals->image_name = env('PROFILE_IMAGE_BASE_URL') . 'logImages/' . $meals->image_name;
                }else if($meals->meal_name=='SKIPPED_MEAL'){
                    $meals->meal_name='SKIPPED MEAL';
                    $meals->image_name=env('APP_BASE_URL').'/img/skipped_meal.svg';
                } else {
                    $meals->image_name = env('APP_BASE_URL') . '/img/no_image_available.svg';
                }
//                if ($meals->log_time != '') {
//                    $co_date = ($meals->log_time / 1000);
//                    $meals->log_time =date("d/m/Y h:i A",$co_date);
//                }
                $past_meal_data = array(
                    'log_id' => $meals->log_id,
                    'image_name' => $meals->image_name,
                    'is_processed' => $meals->is_processed,
                    'log_time' => $meals->date.' '.$meals->log_time,
                    'log_time_offset' => $meals->log_time_offset,
                    'offset_key' => $meals->offset_key,
                    'type' => $meals->type,
                    'meal_name' => $meals->meal_name,
                    'fats' => $meals->fats,
                    'carbs' => $meals->carbs,
                    'has_missing_food' => $meals->has_missing_food,
                    'no_of_servings' => $meals->no_of_servings,
                    'serving_size_unit' => $meals->serving_size_unit,
                    'first_name' => $meals->first_name,
                    'last_name' => $meals->last_name,
                    'calories' => $meals->calories,
                    'unit_from_food_master' => $meals->unit_from_food_master,
                );

                array_push($this->data['past_meal'], $past_meal_data);

            }


            $return_html = '';
            foreach ($this->data['past_meal'] as $meal) {
                $route="'".route('pastmeal/edit_past_meal',['logId' => $meal['log_id']])."'";
                $return_html .= '
              <div class="col-xs-6 col-sm-6 col-lg-3" id="meal_'.$meal['log_id'].'">

               <div class="btn btn-block btn-default btn-bg" >
               <img src="' . $meal['image_name'] . '" class="responsive" width="auto" />
               </div>
               <a class="badge badge-danger delete_meal" data-log-id="'.$meal['log_id'].'"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
               <div class="overlay" onclick="location.href='.$route.'">
                <div class="log-info">
                <h5> <strong>' . $meal['calories'] . '</strong> <small> '.$meal['unit_from_food_master'].' </small></h5>
                 <h6> ' . $meal['meal_name'] . ' </h6>
                  </div>
                   </div>
                   <p class="text-center font-dark"><span class=" sbold font-md"> ' . $meal['type'] . '</span>  <br/><span class=" font-sm font-grey-salsa"><i class="fa fa-clock-o"></i>  ' . $meal['log_time'] . ' </span></p>


           </div>';
            }


            return $return_html;

        }
    }

    public function remove_past_meal(){
        if (request()->ajax()) {
            $log_id = request('log_id');
            info('Deleting Meal:' . $log_id );

            $post_data = Post::where('log_id',$log_id)->first();
            if(!empty($post_data)) {
                $post_id = $post_data->post_id;

                PostComment::where('post_id', $post_id)->delete();
                $deleted = Post::where('post_id', $post_id)->delete();
            }
            $deleted = LogModel::where('log_id', $log_id)->update(array('is_removed' => 1));

         if($deleted){
             return response()->json(
                 [
                     'success' => "Meal deleted successfully",
                 ]
             );

         }else{
             return response()->json(
                 [
                     'error' => "Error in meal deletion",
                 ]
             );

         }
        }
    }


    public function edit_past_meal($log_id){
        $data = collect(LogModel::where('log_id', $log_id)->get());
        if(count($data)>0){
            $patientId=$data->pluck( 'patient_id' )->toArray();
            $patientId=$patientId[0];
        }else{
            $patientId='';
        }

        $this->data['patient_id'] = $patientId;
        $patient_info = $this->patient_model->get_patient_info($patientId, $this->active_facility_id);
        $this->data['patient_info'] = $patient_info;
        if(empty($this->data['patient_info'])) {
            abort(403, 'Unauthorized action.');
        }
        $this->data['group_link'] = "";
        $this->data['session'] = "" ;
        if($patient_info->group_id!="")
        {
            $group_id = array($patient_info->group_id);
            $is_person = $patient_info->is_person;
            $capacity = $patient_info->capacity;
            $completed_week = $patient_info->completed_week;
            $group_session_detail = collect($this->group_model->get_group_current_session_name($group_id));

            $curriculum_for_dpp     = '';
            $curriculum_for_dpp2    = '';
            if ($is_person != 0) {
                // Calling Curriculam server for Section list for both type DPP and DPP2
                $group_session_detail = collect($this->group_model->get_group_current_session_name($group_id));

                $curriculum_program_type_name = $group_session_detail->unique('curriculum_program_type_name')->pluck('curriculum_program_type_name' )->toArray();
                //print_r(count($curriculum_program_type_name)); exit;


                if(count($curriculum_program_type_name) > 0)
                {
                    foreach ($curriculum_program_type_name as $value)
                    {
                        if($value == 'DPP2' || $value == 'DPP')
                        {
                            $curriculum = [
                                (object) [ 'uuid' => 'inperson', 'curriculum_program_type_name' => $value],
                            ];

                            if($value == 'DPP')
                                $curriculum_for_dpp  = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                            if($value == 'DPP2')
                                $curriculum_for_dpp2 = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));

                        }
                    }
                }
            }


            if($completed_week == 0)
                $completed_week = 1;
            $week_group = $is_person == 0 ? 'Week ' . $completed_week : 'Session ' .$completed_week;


            if($is_person != 1)
                $group_link = route('leadcoach.group-detail', ['group_id' => $patient_info->group_id]);
            else
            {


                $group_link = route('leadcoach.group-session', ['group_id' => $patient_info->group_id]);
                $week_group =  0;

                $current_session_name = '';
                if(count($group_session_detail) > 0)
                {
                    $group_current_session = $group_session_detail->where('group_id', $patient_info->group_id)->last();

                    if(!empty($group_current_session->topic_id))
                    {
                        if($curriculum_for_dpp != '')
                            $current_session_name = $curriculum_for_dpp->where('section_uuid', $group_current_session->topic_id)->toArray();
                        if(count($current_session_name) == 0 && $curriculum_for_dpp2 != '')
                            $current_session_name = $curriculum_for_dpp2->where('section_uuid', $group_current_session->topic_id)->toArray();

                        if(!empty($current_session_name))
                        {

                            foreach ($current_session_name as $session_name)
                            {
                                $week_group =  $session_name->display_week ;
                            }
                        }
                    }

                }
            }

            $this->data['group_link'] = $group_link;
            if($is_person!=0)
                $this->data['session'] = $week_group."/".count($curriculum_for_dpp) ;
            else
                $this->data['session'] = "NA" ;

        }
        $this->data['profile_detail'] = $this->dashboard_setting_model->getProfileDetail($patientId);

        $this->data['state'] = state();
        $this->data['log_detail']=$this->past_meal_model->get_past_meal_by_log_id($log_id);
        $this->data['item_detail']=$this->past_meal_model->get_past_meal_by_item($log_id);
        $total_calories='';
        $total_fats='';
        $total_carbs='';
        $total_protein='';
        foreach($this->data['item_detail'] as $idetails){
            $total_calories = $total_calories + $idetails->quantity * $idetails->calories;
            $total_fats = $total_fats + $idetails->fats;
            $total_carbs = $total_carbs + $idetails->carbs;
            $total_protein = $total_protein + $idetails->protein;

        }

        $this->data['total_calories']=round($total_calories,1);
        $this->data['total_fats']=round($total_fats,1);
        $this->data['total_carbs']=round($total_carbs,1);
        $this->data['total_protein']=round($total_protein,1);

        $this->data['past_meal_detail']=array();
        foreach ($this->data['log_detail'] as $meals) {
            if ($meals->image_name != '') {
                $meals->image_name = env('PROFILE_IMAGE_BASE_URL') . 'logImages/' . $meals->image_name;
            } else {
                $meals->image_name = env('APP_BASE_URL') . '/img/no_image_available.svg';
            }
            if ($meals->log_time != '') {
                $co_date = ($meals->log_time / 1000);
                $meals->log_time =date("d/m/Y h:i A",$co_date);
            }
            $past_meal_data = array(
                'log_id' => $meals->log_id,
                'image_name' => $meals->image_name,
                'log_time' => $meals->log_time,
                'type' => $meals->type,
                'meal_name' => $meals->meal_name,
            );

            array_push($this->data['past_meal_detail'], $past_meal_data);

        }
        //print_r($past_meal_data);die;
        return view('common.edit_past_meal',$this->data);

    }


    public function get_past_meal_by_date_range_weekly(Request $request){
        if (request()->ajax()) {
            $timezone = '';
            if (isset($_COOKIE['user_timezone'])) {
                $timezone = $_COOKIE['user_timezone'];
            }
            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $clicked        =  $request->input('clicked');
            $meal_type        =  $request->input('meal_type');
            $date = explode(' - ', $date);
            $start = Carbon::createFromFormat('m/d/Y', $date[0])->setTime(0, 0, 0);
            $end = Carbon::createFromFormat('m/d/Y', $date[1])->setTime(23, 59, 59);

            if($clicked == 'right')
            {
                $daily_start  = $start->copy()->addDays(7);
                $daily_end    = $end->copy()->addDays(7);
            }
            else
            {
                $daily_start  = $start->copy()->subDays(7);
                $daily_end    = $end->copy()->subDays(7);
            }

//            $patient_data = Patient::where('patient_id',$patient_id)->first();
//            $user_id = $patient_data->user_id; $user_data = User::where('user_id',$user_id)->first();
//            $user_timezone = $user_data->offset_key;
//            $offset_key = $user_data->offset_key;

            $patient_data = Patient::where('patient_id',$patient_id)->first();
            $patient_type = $patient_data->diabetes_type_id;
            $user_id =  $patient_data->user_id;
            $user_data = User::where('user_id',$user_id)->first();
            $offset_key  = $user_data->offset_key;
            $this->data['user_timezone']  = $user_data->offset_key;
            $this->data['patient_type']= $patient_type;

            if(!empty($offset_key)) {
                $start = Carbon::createFromFormat('Y-m-d H:i:s', $daily_start, $offset_key)->setTimezone('UTC');
                $end = Carbon::createFromFormat('Y-m-d H:i:s', $daily_end, $offset_key)->setTimezone('UTC');
            }
            $current_date = strtotime($start)*1000;
            $compare_time = strtotime($end)*1000;


//            if(!empty($timezone)) {
//                $start = Carbon::createFromFormat('Y-m-d H:i:s', $daily_start, $timezone)->setTimezone('UTC');
//                $end = Carbon::createFromFormat('Y-m-d H:i:s', $daily_end, $timezone)->setTimezone('UTC');
//            }


//            $current_date = strtotime($start)*1000;
//            $compare_time = strtotime($end)*1000;

            $this->data['past_meal_data'] = $this->past_meal_model->get_past_meal($patient_id,$current_date, $compare_time, $meal_type);
            $this->data['past_meal'] = array();


            foreach ($this->data['past_meal_data'] as $meals) {
                if ($meals->image_name != '') {
                    $meals->image_name = env('PROFILE_IMAGE_BASE_URL') . 'logImages/' . $meals->image_name;
                }else if($meals->meal_name=='SKIPPED_MEAL'){
                    $meals->meal_name='SKIPPED MEAL';
                    $meals->image_name=env('APP_BASE_URL').'/img/skipped_meal.svg';
                } else {
                    $meals->image_name = env('APP_BASE_URL') . '/img/no_image_available.svg';
                }
//                if ($meals->log_time != '') {
//                    $co_date = ($meals->log_time / 1000);
//                    $meals->log_time =date("d/m/Y h:i A",$co_date);
//                }
                $past_meal_data = array(
                    'log_id' => $meals->log_id,
                    'image_name' => $meals->image_name,
                    'is_processed' => $meals->is_processed,
                    'log_time' => $meals->date.' '.$meals->log_time,
                    'log_time_offset' => $meals->log_time_offset,
                    'offset_key' => $meals->offset_key,
                    'type' => $meals->type,
                    'meal_name' => $meals->meal_name,
                    'fats' => $meals->fats,
                    'carbs' => $meals->carbs,
                    'has_missing_food' => $meals->has_missing_food,
                    'no_of_servings' => $meals->no_of_servings,
                    'serving_size_unit' => $meals->serving_size_unit,
                    'first_name' => $meals->first_name,
                    'last_name' => $meals->last_name,
                    'calories' => $meals->calories,
                    'unit_from_food_master' => $meals->unit_from_food_master,
                );

                array_push($this->data['past_meal'], $past_meal_data);

            }


            $return_html = '';
            foreach ($this->data['past_meal'] as $meal) {
                $route="'".route('pastmeal/edit_past_meal',['logId' => $meal['log_id']])."'";
                $return_html .= '
              <div class="col-xs-6 col-sm-6 col-lg-3" id="meal_'.$meal['log_id'].'">

               <div class="btn btn-block btn-default btn-bg" >
               <img src="' . $meal['image_name'] . '" class="responsive" width="auto" />
               </div>
               <a class="badge badge-danger delete_meal" data-log-id="'.$meal['log_id'].'"> <i class="fa fa-trash" aria-hidden="true"></i> </a>
               <div class="overlay" onclick="location.href='.$route.'">
                <div class="log-info">
                <h5> <strong>' . $meal['calories'] . '</strong> <small> '.$meal['unit_from_food_master'].' </small></h5>
                 <h6> ' . $meal['meal_name'] . ' </h6>
                  </div>
                   </div>
                   <p class="text-center font-dark"><span class=" sbold font-md"> ' . $meal['type'] . '</span>  <br/> <span class=" font-sm font-grey-salsa"><i class="fa fa-clock-o"></i>  ' . $meal['log_time'] . ' </span></p>


           </div>';
            }



            return response()->json(
                [
                    'success'   => $return_html,
                    'start'     => $daily_start->format('m/d/Y'),
                    'end'       => $daily_end->format('m/d/Y'),
                    'current' => $current_date,
                    'compare' => $compare_time,

                ]
            );
        }
    }




}//End of Class

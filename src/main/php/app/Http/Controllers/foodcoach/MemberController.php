<?php

namespace HealthSlatePortal\Http\Controllers\foodcoach;
use Carbon\Carbon;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CurriculumServiceProvider;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\Patient;
use Illuminate\Support\Facades\Log;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\PatientHideUnhide;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\Message;
use HealthSlatePortal\Models\Eloquent\SessionAttendance;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use Edujugon\PushNotification\PushNotification;
use DB;

class MemberController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $user;
    protected $dataTableService;
    protected $group_model;
    protected $group_member;
    protected $curriculumServiceProvider;
    protected $data = array();

    function __construct( FacilityModel $facility_model, MemberModel $member_model, Facility $facility, CoacheModel $coach_model,
                          DataTableServiceProvider $dataTableService, User $user, GroupModel $group_model, GroupMember $group_member, CurriculumServiceProvider $curriculumServiceProvider) {
        $this->facility_model       = $facility_model;
        $this->coach_model          = $coach_model;
        $this->member_model         = $member_model;
        $this->group_member         = $group_member;
        $this->group_model          = $group_model;
        $this->facility             = $facility;
        $this->user                 = $user;
        $this->dataTableService     = $dataTableService;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->group_db             = env('GROUP_DB_DATABASE');
        $this->active_facility_id   = session('active_facility');
        $this->active_user_id       = session('userId');
        $this->data['js']           = array('message', 'member','member-recruitment');
        $this->data['common_js']    = array('tagging/jquery-migrate-1.2.1', 'tagging/jquery.caretposition', 'tagging/jquery.sew');
    }


    /**
     * To render member page
     * (it's show member page)
     */
	public function index($group_id = '') {

        $group_list = $this->group_model->get_all_group_list($this->active_facility_id, $this->active_user_id);

        $facility_group_list = array();
        if(!empty($group_list))
        {
            $facility_group_list[''] = 'Select Group';
            foreach ($group_list as $key => $value)
            {
                $facility_group_list[$value->group_id] = $value->name;
            }
        }
        $this->data['facility_group_list']  = $facility_group_list;
        $this->data['member_group_id']      = base64_decode($group_id);

        info('Total Group Found: ' . count($facility_group_list));
        return view( 'foodcoach.member.member', $this->data);
	}


    /**
     * member list function
     * (it's load data on member page )
     */
    public function membersList(Request $request)
    {
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 0);

        $start                  = request('start', 0);
        $length                 = request('length', 0);
        $datatable_request      = array('start' => $start, 'length' => $length);

        $member_filter  = $request->member_filter;
        $group_filter   = $request->group_filter;
        $another_filter   = $request->another_filter;

        $response       = $this->member_model->get_member_list($this->active_facility_id, $member_filter, $group_filter,$another_filter, $this->active_user_id, $datatable_request);

        $response_total = count($response);
        $response       = collect($response);

        info('Total Members Found: ' . count($response));

        $sorting_status = true;

        $data    = collect();
        $danger  = collect();
        $warning = collect();
        $success = collect();
        $white   = collect();
        $active  = collect();
        $final_data = collect();

        $color_class = '';
        $status = '';

        foreach ($response as $member) {
            $check_engagement       = get_engagement_score($member);
            $color_class            = $check_engagement['color_class'];
            $status                 = $check_engagement['status'];
            $member->status_type    = $check_engagement['status_type'];

            $member->color_class    = $color_class;
            $member->color_status   = $status;

            if($sorting_status)
            {
                if($member->hide_unhide == 1)
                    $active->push($member);
                else if($color_class == 'danger')
                    $danger->push($member);
                else if($color_class == 'warning')
                    $warning->push($member);
                else if($color_class == 'success')
                    $success->push($member);
                else if($color_class == 'white')
                    $white->push($member);
                else if($color_class == 'active')
                    $active->push($member);
            }
            else
            {
                $active->push($member);
            }
        }
        $danger   = $danger->sortByDesc('pin_unpin');
        $warning  = $warning->sortByDesc('pin_unpin');
        $success  = $success->sortByDesc('pin_unpin');

        $data = $data->merge($danger);
        $data = $data->merge($warning);
        $data = $data->merge($success);

        $white_sorting_for_pin      = collect();
        $white_sorting_for_unpin    = collect();
        foreach ($white as $white_sorting) {
            if($white_sorting->pin_unpin == 1)
                $white_sorting_for_pin->push($white_sorting);
            else
                $white_sorting_for_unpin->push($white_sorting);
        }

        $active_sorting_for_pin       = collect();
        $active_sorting_for_unpin     = collect();
        foreach ($active as $active_sorting) {
            if($active_sorting->pin_unpin == 1)
                $active_sorting_for_pin->push($active_sorting);
            else
                $active_sorting_for_unpin->push($active_sorting);
        }

        $data = $data->merge($white_sorting_for_pin);
        $data = $data->merge($white_sorting_for_unpin);

        $data = $data->merge($active_sorting_for_pin);
        $data = $data->merge($active_sorting_for_unpin);

        $data = $data->slice($request['start'], $request['length']);

        $patient_ids    = $data->pluck( 'patient_id' )->toArray();
        $member_message_detail      = $this->member_model->get_member_message_detail($patient_ids,$this->active_user_id);
        if(!empty($member_message_detail))
        {
            foreach ($response as $row) {
                foreach ($member_message_detail as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->total_unread_message            = $value->total_unread_message;
                        //$row->last_message_sent_to_patient    = $value->last_message_sent_to_patient;
                        $row->last_unread_message_of_patient  = $value->last_unread_message_of_patient;
                    }
                }
            }
        }

        $last_sent_member_detail  = $this->member_model->get_last_message_sent_to_patient($patient_ids, $this->active_user_id);
        if(!empty($last_sent_member_detail))
        {
            foreach ($response as $row) {
                foreach ($last_sent_member_detail as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->last_message_sent_to_patient    = $value->last_message_sent_to_patient;
                    }
                }
            }
        }

        $recommended_message_response   = collect($this->member_model->get_recommended_message_list());
        $count = 1;
        foreach ($data as $member) {

            $color_class        = $member->color_class;
            $status             = $member->color_status;
            $status_type        = $member->status_type;

            $recommended_message = '<div class="col-sm-8"><p class="recm-msg"><i></i></p></div><div class="col-sm-4 text-right send-btn"><div class="btn-group"><a data-id="'.$member->user_id.'" data-toggle="modal" href="#direct-msg-coach" class="btn grey-gallery btn-sm btn-outline sbold ">Create New</a></div></div>';
            if($color_class == 'danger' || $color_class == 'warning')
            {
                if($count == 1){
                    $in_app_message_list  = collect($this->member_model->get_in_app_message_list($patient_ids));
                    $count += 1;
                }
                $message_text = '';
                $message_type         = $recommended_message_response->where('status', $status_type);
                $in_app_message_type  = $in_app_message_list->where('patient_id', $member->patient_id);

                if(count($in_app_message_type) > 0 && count($message_type) > 0)
                {
                    $count_sent_message = 0;
                    $predefined_message_id = [];
                    foreach ($in_app_message_type as $sent_message)
                    {
                        $check_sent_message = collect($message_type)->where('message_text', $sent_message->message)->first();

                        if(count($check_sent_message) > 0)
                        {
                            $count_sent_message += 1;
                            $predefined_message_id[] = $check_sent_message->predefined_message_id;
                            //echo $sent_message->message_text;
                        }
                    }
                    if($count_sent_message == count($message_type))
                    {
                        $message_text = 'Already Sent';
                        //Already Sent All Message for week
                    }
                    else
                    {
                        foreach ($message_type->all() as $check_message)
                        {
                            if(in_array($check_message->predefined_message_id, $predefined_message_id))
                            {
                                continue;
                            }
                            else
                            {
                                $message_text = $check_message->message_text;
                                break;
                            }
                        }
                    }

                }
                elseif(count($message_type) > 0)
                {
                    $message_type = $message_type->first();
                    $message_text = $message_type->message_text;
                }

                if($message_text != 'Already Sent')
                    $recommended_message = '<div class="col-sm-8"><p class="recm-msg"><i>'.str_limit($message_text,50).'</i></p></div><div class="col-sm-4 text-right send-btn"><div class="btn-group"><a class="btn grey-gallery btn-sm btn-outline sbold ">Send</a><a class="btn grey-gallery btn-sm btn-outline sbold" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-angle-down"></i></a><ul class="dropdown-menu"><li><a data-id="'.$member->user_id.'" data-recomm_message="'.$message_text.'" data-toggle="modal" href="#direct-msg-coach"><i class="fa fa-pencil"></i>  Edit Message </a></li><li><a data-id="'.$member->user_id.'" data-toggle="modal" href="#direct-msg-coach"><i class="fa fa-list-alt"></i> Create New </a></li></ul></div></div>';
                else
                    $recommended_message = '<div class="col-sm-8"><p class="recm-msg"><i>'.str_limit($message_text,50).'</i></p></div><div class="col-sm-4 text-right send-btn"><div class="btn-group"><a data-id="'.$member->user_id.'" data-toggle="modal" href="#direct-msg-coach" class="btn grey-gallery btn-sm btn-outline sbold ">Create New</a></div></div>';
            }


            $timezone = '';
            if(isset($_COOKIE['user_timezone'])) {
                $timezone =  $_COOKIE['user_timezone'];
            }

            $last_message_sent_to_patient   = '';
            $last_unread_message_of_patient = '';
            $here = Carbon::now($timezone);
            if(!empty($member->last_message_sent_to_patient))
            {
                $last_message_sent_to_patient    = Carbon::createFromTimestamp($member->last_message_sent_to_patient/1000)->format('Y-m-d H:i:s');
                $last_message_sent_to_patient    =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $last_message_sent_to_patient)->timezone($timezone) : $last_message_sent_to_patient;
                $here->addSeconds($last_message_sent_to_patient->offset - $here->offset);
                $last_message_sent_to_patient     = '<small>'.$last_message_sent_to_patient->diffForHumans($here).'</small>';
                $last_message_sent_to_patient     =  str_replace("before","ago",$last_message_sent_to_patient);
            }
            if(!empty($member->last_unread_message_of_patient))
            {
                $last_unread_message_of_patient    = Carbon::createFromTimestamp($member->last_unread_message_of_patient/1000)->format('Y-m-d H:i:s');
                $last_unread_message_of_patient    =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $last_unread_message_of_patient)->timezone($timezone) : $last_unread_message_of_patient;
                $here->addSeconds($last_unread_message_of_patient->offset - $here->offset);
                $last_unread_message_of_patient = '(' . $last_unread_message_of_patient->diffForHumans($here). ')';
                $last_unread_message_of_patient =  str_replace("before","ago",$last_unread_message_of_patient);
            }

            //Check user pinned or not
            $pin_unpin = '<a data-toggle="tooltip" title="Pin" data-value="1" data-id="'.$member->user_id.'" class="btn font-grey-salsa btn-sm btn-pin sbold pin_unpin member-list" data-original-title="Pin"><i class="iconhs-pin"></i></a>';
            if($member->pin_unpin == 1)
            {
                $pin_unpin = '<a data-toggle="tooltip" title="Unpin" data-value="0" data-id="'.$member->user_id.'" class="btn font-dark btn-sm btn-pin sbold pin_unpin member-list" data-original-title="Unpin"><i class="iconhs-pin"></i></a>';
            }

            //Check user hide or not
            $hide_unhide = '<a data-toggle="tooltip" title="Hide" data-value="1" data-id="'.$member->user_id.'" class="btn font-dark btn-sm btn-hide sbold hide_unhide member-list" data-original-title="Hide" aria-describedby="tooltip124891"><i class="fa fa-eye"></i></a>';
            if($member->hide_unhide == 1)
            {
                $color_class    = 'active';
                $status         = '';
                $recommended_message = '';
                $hide_unhide = '<a data-toggle="tooltip" title="Unhide" data-value="0" data-id="'.$member->user_id.'" class="btn font-dark btn-sm btn-hide sbold hide_unhide member-list" data-original-title="Unhide" aria-describedby="tooltip124891"><i class="fa fa-eye-slash"></i></a>';
            }

            $asign_group    = '';
            $remove_member  = '';
            if(session('userRole') == 'facilityadmin')
            {
                $asign_group = '<a  href="#modal_assignGroup" data-toggle="modal" data-uid="'.$member->user_id.'" title="Group" class="btn font-dark btn-sm btn-hide sbold member-list"><i class="fa fa-users"></i>  </a>';
                if($member->group_user_id != '')
                    $asign_group = '<a  href="#modal_assignGroup" data-toggle="modal" data-uid="'.$member->user_id.'" title="Group" class="btn font-grey-salsa btn-sm btn-hide sbold member-list"><i class="fa fa-users"></i>  </a>';

                $remove_member = '<a  title="Delete" data-toggle="modal" data-id="'.$member->user_id.'" data-target="#modal_prospective_member" class="btn font-dark btn-sm btn-hide sbold member-list"><i class="fa fa-remove"></i></a>';
            }

            $member_detail = '<a  title="Member Details" data-toggle="modal" data-id="'.$member->user_id.'" data-target="#member_detail" class="btn font-dark btn-sm btn-hide sbold member-list"><i class="fa fa-info-circle"></i></a>';
            $patient_profile_image = env('APP_BASE_URL').'/img/default-user.png';
            if($member->patient_profile_image != '')
            {
                $patient_profile_image = env('PROFILE_IMAGE_BASE_URL') . $member->patient_profile_image;
            }

            $row = [
                'hidden_id'                 =>  '',
                'patient_id'                => '<div class="col-sm-6 '.$color_class.'"><span class="memberid">'.$member->patient_id.' </span></br> Week '.abs($member->member_completed_week).' </div><div class="col-sm-6"> <img class="user-pic rounded"  src="'.$patient_profile_image.'"></br><a href="'.url('foodcoach/dashboard') .'/'. $member->patient_id.'" class="member-name sbold">'.str_limit($member->first_name).' '.str_limit($member->last_name).'</a></div>',

                'status(engagement score)'  => '<p class="pri-msg"><a>  '.$status.' </a></p>',
                'recommended message'       => "$recommended_message",

                'last sent'                 => $last_message_sent_to_patient,
                'unread'                    => '<span class="messages" data-id="'.$member->user_id.'" data-toggle="modal" href="#direct-msg-coach">'.$member->total_unread_message.'</br><small>'.$last_unread_message_of_patient.'</small></span>',
                'signed_on'                 => ( ! empty( $timezone ) && !empty($member->registration_date) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $member->registration_date)->timezone($timezone)->format('m/d/Y h:i A') : '',
                'action'                    => $asign_group.' '.$pin_unpin.' '.$hide_unhide .' '. $remove_member .' '. $member_detail,

            ];

            $final_data->push(array_values($row));
        }

        info('Sending Data To DataTableService');

        $final_response['data']              = $final_data;
        $final_response['recordsTotal']      = $response_total;
        $final_response['recordsFiltered']   = $response_total;
        return response()->json( $final_response );
    }




    public function patient_hide_unhide(Request $request)
    {
        if (request()->ajax()) {
            $user_id    = request('user_id', 0);
            $value      = request('value', 0);

            $patient = Patient::where('user_id', $user_id)->first();

            if(empty($patient))
            {
                info('Member not found into Database USERID:' . $user_id);
                return response()->json([
                    'error' => 'Member not found into Database'
                ]);
            }

            info('Updating status for USERID:' . $user_id);
            $user = PatientHideUnhide::firstOrNew(array('patient_id' => $patient->patient_id, 'user_id' => $this->active_user_id));
            $user->hide_unhide  = $value;
            $user->date         = date('Y-m-d H:i:s');
            $user->save();

            info('Member Status Updated Successfully: PatientId' . $patient->patient_id);

            return $response = [
                'success' => 'Member Status Updated Successfully!',
            ];
        }
        else
        {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'update status'])
            ]);
        }
    }


    public function patient_pin_unpin(Request $request)
    {
        if (request()->ajax()) {
            $user_id    = request('user_id', 0);
            $value      = request('value', 0);

            $patient = Patient::where('user_id', $user_id)->first();

            if(empty($patient))
            {
                info('Member not found into Database USERID:' . $user_id);
                return response()->json([
                    'error' => 'Member not found into Database'
                ]);
            }

            info('Updating status for USERID:' . $user_id);
            $user = PatientHideUnhide::firstOrNew(array('patient_id' => $patient->patient_id, 'user_id' => $this->active_user_id));
            $user->pin_unpin    = $value;
            $user->date         = date('Y-m-d H:i:s');
            $user->save();

            info('Member Status Updated Successfully: PatientId' . $patient->patient_id);

            return $response = [
                'success' => 'Member Status Updated Successfully!',
            ];
        }
        else
        {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'update status'])
            ]);
        }
    }



    public function memberDetail()
    {
        if (request()->ajax()) {
            $user_id  = request('id' , 0);
            if($user_id)
            {
                $response = $this->member_model->get_member_detail_group_detail(session('active_facility'), $user_id);
                $data = array();
                if(!empty($response))
                {

                    $data['name']               = 'Name:<b> ' .$response->full_name . '</b>';
                    $data['email']              = 'Email:<b> ' . $response->email . '</b>';;
                    $data['group_name']         = 'Group Name:<b> ' .$response->group_name . '</b>';;
                    $data['registration_date']  = !empty($response->registration_date) ? 'Registration Date:<b> ' . date("Y-m-d", strtotime($response->registration_date)) . '</b>' : 'Registration Date: ';
                    $data['offset_key']         = 'Timezone:<b> ' .$response->offset_key . '</b>';
                }

                return $response = [
                    'success' => $data,
                ];
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'get detail'])
                ]);
            }
        }
    }



    public function memberGroupDetail()
    {
        if (request()->ajax()) {
            $user_id  = request('uid' , 0);
            if($user_id)
            {
                $group_list = $this->group_model->get_all_group_list($this->active_facility_id);
                $data = $this->member_model->get_member_detail_group_detail($this->active_facility_id, $user_id);
                return $response = [
                    'success' => $data,
                    'group_list' => $group_list,
                ];
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'get detail'])
                ]);
            }
        }
    }



    public function addMemberToGroup()
    {
        if (request()->ajax()) {

            $group_id  = request('group_id' , 0);
            $user_id   = request('uid' , 0);


           // Log::info('Sending Notification To device_token: ' . $user->device_token);

            $push = new PushNotification('gcm');
            /*$response = $push->setMessage(['message'=>'Hello World'])
                ->setApiKey('APA91bHxQUeh1eIOw9Vrwixiz9k9-JnMpkBQvWKe94Y65AOc5MMPmlri4G8yQi4BTZWMUsZAoDV7zNgh9i0VaDgfGv3Zb5gdAw3MYZ7xuX5d1t067Xnfq2o1A0MKwnHSgq-H8KdWJpON6StSm6R2Rvb_QwPCC4Zw6A')
                ->setConfig(['dry_run' => false])
                ->sendByTopic("'dogs' in topics || 'cats' in topics",true);*/
            /*
                        $push->setMessage([
                            'notification' => [
                                'title'=>'This is the title',
                                'body'=>'This is the message',
                                'sound' => 'default'
                            ],
                            'data' => [
                                'extraPayLoad1' => 'value1',
                                'extraPayLoad2' => 'value2'
                            ]
                        ])
                            ->setApiKey('APA91bHxQUeh1eIOw9Vrwixiz9k9-JnMpkBQvWKe94Y65AOc5MMPmlri4G8yQi4BTZWMUsZAoDV7zNgh9i0VaDgfGv3Zb5gdAw3MYZ7xuX5d1t067Xnfq2o1A0MKwnHSgq-H8KdWJpON6StSm6R2Rvb_QwPCC4Zw6A');

                        Log::info('Notification Response: ' . print_r($push->getFeedback(), true) );


                        dd('test');*/

            info('Checking into Group Member table for alredy exist Member into Group');
            $group_response = $this->group_member->where('group_id', $group_id)->where('user_id', $user_id)->get();
            if(count($group_response) > 0)
            {
                info('Selected Member already exist into that Group UserId: ' . $user_id);
                return response()->json([
                    'error' => 'Selected Member already exist into that Group'
                ]);
            }

            if($group_id != '' && $user_id != '')
            {
                $user_old_group_detail = $this->member_model->get_member_detail_group_detail($this->active_facility_id, $user_id);

                if(!empty($user_old_group_detail->group_id) && !empty($user_old_group_detail->group_user_id))
                {
                    if($user_old_group_detail->is_person == 1)
                    {
                        info('Deleting a in_person session_attendance,group_member Group Member: ' . $user_old_group_detail->patient_id);
                        DB::table("$this->group_db.session_attendance")->where('group_member_id', $user_id)->delete();
                        DB::table("$this->group_db.post_comment")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.post")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.group_member")->where('user_id', $user_id)->where('group_id', $user_old_group_detail->group_id)->delete();
                        DB::table("group_user")->where('user_id', $user_id)->where('group_id', $user_old_group_detail->group_id)->delete();
                    }
                    else
                    {
                        info('Deleting a Online Group post_comment,post,group_member Member: ' . $user_old_group_detail->patient_id);
                        DB::table("$this->group_db.post_comment")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.post")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.group_member")->where('user_id', $user_id)->where('group_id', $user_old_group_detail->group_id)->delete();
                        DB::table("group_user")->where('user_id', $user_id)->where('group_id', $user_old_group_detail->group_id)->delete();
                    }
                }


                $loggedInUser = session('user');
                info('Assign New Member To GroupID: ' . $group_id . ' Request by: ' . $loggedInUser->userName . ' Facility: ' . session('active_facility'));

                $group_member_id = new GroupMember;
                $group_member_id->group_id      = $group_id;
                $group_member_id->user_id       = $user_id;
                $group_member_id->timestamp     = date('Y-m-d H:i:s');
                $group_member_id->save();

                $group_user_id = new GroupUser;
                $group_user_id->group_id      = $group_id;
                $group_user_id->user_id       = $user_id;
                $group_user_id->save();

                $new_group_detail = $this->group_model->get_group_detail($group_id);
                if($group_member_id)
                {
                    if($user_old_group_detail->patient_id)
                    {
                        info('Updating Patient Lead and Food Coach Ids of the Patient: ' . $user_old_group_detail->patient_id);
                        $lead_user_id = '';
                        if(!empty($new_group_detail->lead_user_id))
                        {
                            $lead_user_id  = $this->provider->where('user_id', $new_group_detail->lead_user_id)->first();
                            $lead_user_id  = $lead_user_id->provider_id;
                        }

                        $primary_food_coach_id = '';
                        if(!empty($new_group_detail->primary_food_coach_id))
                        {
                            $primary_food_coach_id  = $this->provider->where('user_id', $new_group_detail->primary_food_coach_id)->first();
                            $primary_food_coach_id  = $primary_food_coach_id->provider_id;
                        }

                        $patient_detail =  Patient::find($user_old_group_detail->patient_id);
                        $patient_detail->lead_coach_id          = $lead_user_id;
                        $patient_detail->primary_food_coach_id  = $primary_food_coach_id;
                        $patient_detail->update();


                        info('Checking into sharing_preference Table to add Meals and Activity preference');
                        $sharing_preference = DB::table("sharing_preference")->where('patient_id', $user_old_group_detail->patient_id)->get();
                        if(!$sharing_preference)
                        {
                            info('Member not exist into sharing_preference MemberId: ' . $user_old_group_detail->patient_id);
                            DB::table('sharing_preference')->insert([
                                ['patient_id' => $user_old_group_detail->patient_id, 'meals' => 0, 'activity' => 0],
                            ]);
                            info('Member added to sharing_preference MemberId: ' . $user_old_group_detail->patient_id);
                        }
                    }
                }

                // To update capacity of the group
                info('Total Group Member into New Group: '. $new_group_detail->total_member);
                if($new_group_detail->capacity <= $new_group_detail->total_member)
                {
                    $group = Groups::find($new_group_detail->group_id);
                    $group->is_full  = 1;
                    $group->update();
                    info("Group Capacity was: $new_group_detail->capacity So it's Full Now GorupId: $new_group_detail->group_id" );
                }


                if($new_group_detail->is_person != 1)
                {
                    info('Group is Online so sending in-app message to Member');

                    $member_coach_detail = $this->group_model->get_member_coach_detail($user_id);
                    $auto_mesage_detail = $this->group_model->get_auto_messages_day_one();
                    if(!empty($auto_mesage_detail))
                    {
                        info('Sending auto message to Member');
                        foreach ($auto_mesage_detail as $value)
                        {
                            $message = str_replace('##coachFname##', $member_coach_detail->leadcoach_name , $value->template);
                            $message = str_replace('##coachCity##',  $member_coach_detail->leadcoach_city , $message);
                            $message = str_replace('##coachState##', $member_coach_detail->leadcoach_state , $message);
                            $message = str_replace('##acuityLink##', $member_coach_detail->acuity_link , $message);

                            if($member_coach_detail->device_type == 'IOS')
                                $message = str_replace('##appGuideVideoURL##', 'https://healthslate-1.wistia.com/medias/l9whlf8srw' , $message);
                            else
                                $message = str_replace('##appGuideVideoURL##', 'https://healthslate-1.wistia.com/medias/3ohz3tx2ge' , $message);

                            $message_id = new Message;
                            $message_id->message        = $message;
                            $message_id->owner          = 'PROVIDER';
                            $message_id->read_status    = DB::raw(0);
                            $message_id->timestamp      = (time() * 1000);
                            $message_id->patient_id     = $member_coach_detail->patient_id;
                            $message_id->user_id        = $member_coach_detail->leadcoach_id;
                            $message_id->save();
                        }
                    }
                    info('Calling Curriculum Server for Section Title');
                    $curriculumn = $this->curriculumServiceProvider->getCurriculumV2SectionList($member_coach_detail);
                    if(!empty($curriculumn))
                    {
                        foreach ($curriculumn as $value)
                        {
                            if($value->type == strtolower($member_coach_detail->curriculum_program_type_name))
                            {
                                $message_id = new Message;
                                $message_id->message        = "Session $value->display_week \"$value->sectionTitle\" is now available. You can find it under Menu/Videos (Sessions).";
                                $message_id->owner          = 'PROVIDER';
                                $message_id->read_status    = DB::raw(0);
                                $message_id->timestamp      = (time() * 1000);
                                $message_id->patient_id     = $member_coach_detail->patient_id;
                                $message_id->user_id        = $member_coach_detail->leadcoach_id;
                                $message_id->save();
                            }
                            break;
                        }
                    }

                }
                else
                {
                    info('New group is In Person');
                    $group_session = $this->group_model->get_group_session_detail($new_group_detail->group_id);
                    if(count($group_session) > 0)
                    {
                        foreach ($group_session as $session_value)
                        {
                            $session_id = new SessionAttendance;
                            $session_id->group_member_id    = $group_member_id->group_member_id;
                            $session_id->group_session_id   = $session_value->group_session_id;
                            $session_id->timestamp          = date('Y-m-d H:i:s');
                            $session_id->save();
                        }
                    }
                }

                return $response = [
                    'success' => "Member assign to new group successfully!",
                ];
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'Assign Member'])
                ]);
            }
        }
    }



    public function removeMemberFromToGroup()
    {
        if (request()->ajax()) {

            $group_id  = request('group_id' , 0);
            $user_id   = request('uid' , 0);
            info('Checking into Group Member table for alredy exist Member into Group');
            $group_response = $this->group_member->where('group_id', $group_id)->where('user_id', $user_id)->first();
            if(count($group_response) > 0)
            {
                info('Removing Member from Group: ' . $group_id);
                $new_group_detail = $this->group_model->get_group_detail($group_id);
                if(!empty($new_group_detail->group_id))
                {
                    if($new_group_detail->is_person == 1)
                    {
                        info('Deleting a in_person session_attendance,group_member Group Member: ' . $new_group_detail->patient_id);
                        DB::table("$this->group_db.session_attendance")->where('group_member_id', $group_response->group_member_id)->delete();
                        DB::table("$this->group_db.post_comment")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.post")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.group_member")->where('user_id', $user_id)->where('group_id', $new_group_detail->group_id)->delete();
                        DB::table("group_user")->where('user_id', $user_id)->where('group_id', $new_group_detail->group_id)->delete();
                    }
                    else
                    {
                        info('Deleting a Online Group post_comment,post,group_member Member: ' . $new_group_detail->patient_id);
                        DB::table("$this->group_db.group_member")->where('user_id', $user_id)->where('group_id', $new_group_detail->group_id)->delete();
                        DB::table("$this->group_db.post_comment")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.post")->where('user_id', $user_id)->delete();
                        DB::table("group_user")->where('user_id', $user_id)->where('group_id', $new_group_detail->group_id)->delete();
                    }

                    // To update capacity of the group
                    info('Total Group Member into Group: '. $new_group_detail->total_member);
                    if($new_group_detail->capacity <= $new_group_detail->total_member)
                    {
                        $group = Groups::find($new_group_detail->group_id);
                        $group->is_full  = 1;
                        $group->update();
                        info("Group Capacity was: $new_group_detail->capacity So it's Full Now GorupId: $new_group_detail->group_id" );
                    }

                    return $response = [
                        'success' => 'Member removed from Group successfully!',
                    ];
                }
            }
            else
            {
                info('Selected Member not exist into that Group UserId: ' . $user_id);
                return response()->json([
                    'error' => 'Selected Member not exist into that Group'
                ]);
            }
        }
    }




}//End of Class

<?php

namespace HealthSlatePortal\Http\Controllers\foodcoach;
use Carbon\Carbon;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\Message;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use HealthSlatePortal\Helpers\MailServiceHelper;
use HealthSlatePortal\Models\CurriculumServiceProvider;
use Illuminate\Support\Facades\Log;
use Edujugon\PushNotification\PushNotification;
use Validator;
use Session;
use DB;
class MyGroupController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $group_model;
    protected $group_member;
    protected $dataTableService;
    protected $curriculumServiceProvider;
    protected $mailServiceHelper;
    protected $user;
    protected $data = array();

    function __construct( FacilityModel $facility_model, MemberModel $member_model,
                          Facility $facility, CoacheModel $coach_model, GroupModel $group_model,
                          DataTableServiceProvider $dataTableService, User $user, GroupMember $group_member ,MailServiceHelper $mailServiceHelper,CurriculumServiceProvider $curriculumServiceProvider) {
        $this->facility_model   = $facility_model;
        $this->coach_model      = $coach_model;
        $this->member_model     = $member_model;
        $this->group_model      = $group_model;
        $this->group_member     = $group_member;
        $this->facility         = $facility;
        $this->user             = $user;
        $this->dataTableService = $dataTableService;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->active_user_id   = session('userId');
        $this->active_user_role     = session('userRole');
        $this->mailServiceHelper = $mailServiceHelper;
        $this->group_db         = env('GROUP_DB_DATABASE');
        $this->data['js']       = array('group','coach');
    }


    /**
     * @param
     * @return Load view for Member
     */
	public function index() {
        return view( 'foodcoach.mygroup.group', $this->data);
	}



    /**
     * @param $active_facility
     * @return Json response of Group Data
     */
    public function groupList()
    {
        $group    = request('group' , 0);
        $capacity = request('capacity' , 0);
        $response = collect($this->group_model->get_facility_group_list(session('active_facility'), $group, $capacity, session('userId')));

        info('Total Group Found: ' . count($response));
        $data = collect();

        if(count($response) > 0)
        {
            $group_ids = $response->pluck( 'group_id' )->toArray();
            $curriculum_for_dpp     = '';
            $curriculum_for_dpp2    = '';
            if ($group != 'online') {
                // Calling Curriculam server for Section list for both type DPP and DPP2
                $group_session_detail = collect($this->group_model->get_group_current_session_name($group_ids));
                $curriculum_program_type_name = $group_session_detail->unique('curriculum_program_type_name')->pluck( 'curriculum_program_type_name' )->toArray();
                if(count($curriculum_program_type_name) > 0)
                {
                    foreach ($curriculum_program_type_name as $value)
                    {
                        if($value == 'DPP2' || $value == 'DPP')
                        {
                            $curriculum = [
                                (object) [ 'uuid' => $group, 'curriculum_program_type_name' => $value],
                            ];
                            if($value == 'DPP')
                                $curriculum_for_dpp  = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                            if($value == 'DPP2')
                                $curriculum_for_dpp2 = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                        }
                    }
                }
            }

            foreach ($response as $group) {

                $group_members = $this->group_model->get_group_coaches_detail($group->group_id);
                $coach_list = '';$food_coach = ''; $life_coach = '';
                if(!empty($group_members))
                {
                    foreach ($group_members as $key => $value)
                    {
                        if($value->lead_user_id == $value->user_id)
                            $life_coach  = "<span>$value->full_name <strong>(Lifestyle Coach)</strong></span><br>";
                        else if ($value->primary_food_coach_id == $value->user_id)
                            $food_coach  = "<span>$value->full_name <strong>(Food Coach)</strong></span><br>";
                        else
                            $coach_list .= "<span>$value->full_name <strong>(Coach)</strong></span><br>";
                    }
                    $life_coach .= $food_coach .' '. $coach_list;
                }

                if($group->completed_week == 0)
                    $group->completed_week = 1;

                $icon_group = $group->is_person == 0 ? 'iconhs-onlinegroup' : 'iconhs-inperson';
                $week_group = $group->is_person == 0 ? 'Week ' . $group->completed_week : 'Session ' .$group->completed_week;

                $message_group ='<a data-target="#msg-group-member" data-toggle="modal" data-id="'.$group->group_id.'" title="Message Group" class="btn font-dark btn-sm btn-hide sbold pull-left"> <i class="fa fa-envelope"></i></a>
                <a title="Group Wall" href="'.route('foodcoach.group-wall', ['group_id' => base64_encode($group->group_id)]).'" class="btn font-dark btn-sm btn-hide sbold pull-left"><i class="iconhs-groupwall"></i></a>';
                if($group->total_users == 0)
                {
                    $message_group ='';
                }
                $link = '';
                if($group->group_id)
                    $link = 'href="'.route('foodcoach.members', ['group_id' => base64_encode($group->group_id)]).'"';

                if($group->is_person == 1)
                {
                    $week_group = 'Session has not started yet';

                    $current_session_name = '';
                    if(count($group_session_detail) > 0)
                    {
                        $group_current_session = $group_session_detail->where('group_id', $group->group_id)->last();
                        if(!empty($group_current_session->topic_id))
                        {
                            if($curriculum_for_dpp != '')
                                $current_session_name = $curriculum_for_dpp->where('section_uuid', $group_current_session->topic_id)->toArray();
                            if(count($current_session_name) == 0 && $curriculum_for_dpp2 != '')
                                $current_session_name = $curriculum_for_dpp2->where('section_uuid', $group_current_session->topic_id)->toArray();

                            if(!empty($current_session_name))
                            {
                                foreach ($current_session_name as $session_name)
                                {
                                    $week_group = 'Session ' . $session_name->display_week . ' : '. str_limit($session_name->sectionTitle, 40);
                                }
                            }
                        }
                    }
                }

                $row = [
                    'hidden_id'       => '',
                    'group_name'      => '<div class="col-sm-1"><span class="group-type"><i class="'.$icon_group.'"></i></span></div>
                                            <div class="col-sm-11">
                                                <p><a class="memberid">'.$group->name.'</a><a class="pull-right font-dark" '.$link.' > '.$group->total_users.' Members</a> </p>
                                                <p><strong>'.$week_group.'</strong><span class="pull-right">Started-'.date('M d',strtotime($group->timestamp)).'</span> </p>
                                          </div>',

                    'status'          => $life_coach,
                    'action'          => $message_group,

                ];
                $data->push(array_values($row));
            }
        }

        info('Sending Data To DataTableService');
        return $this->dataTableService->dataTableWithIds($data);
    }



   public function comingsoon()
   {
    return view("facilityadmin.mygroup.comingsoon");

   }







    public function groupDetail()
    {
        if (request()->ajax()) {
            $group_id  = request('id' , 0);
            if($group_id)
            {
                $data = $this->group_model->get_group_detail($group_id);
                return $response = [
                    'success' => $data,
                ];
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'get detail'])
                ]);
            }
        }
    }












     public function messageGroupMember()
     {


         if (request()->ajax())
         {
             $group_id  = request('group_id' , 0);
             if($group_id)
             {
                 $data = collect();
                 $group_name="";
                 $group_list  = $this->group_model->get_group_member_list($group_id,session('active_facility'));
                 $group_members = collect($group_list);
                 //$group_members_filtered = $group_members->where('user_type', 'PATIENT');
                 $group_members_filtered = $group_members;
                 if(!empty($group_members_filtered))
                 {
                     foreach ($group_members_filtered as $grouplist_value)
                     {
                         $group_name = $grouplist_value->group_name;
                         if($grouplist_value->user_type=="PATIENT")
                         {

                             $row = [
                                 'user_id' => $grouplist_value->user_id,
                                 'full_name' => $grouplist_value->full_name,
                                 'email' => $grouplist_value->email,

                             ];

                             $data->push($row);
                         }

                     }
                 }

                 return $response = [

                     'group_list' => $data,
                     'group_name'=>$group_name,
                 ];
             }
             else
             {
                 return response()->json([
                     'error' => trans('common.unable_to_do_action', [ 'action' => 'get detail'])
                 ]);
             }
         }

     }

 /* save group message start here*/
     public function saveGroupMessage(Request $request)
     {
         if($this->active_user_role == 'facilityadmin' || $this->active_user_role == 'admin' )
         {
             return response()->json([
                 'error' => 'Unable to send message'
             ]);
         }

         if($request->hasFile('uploadfile'))
         {
             $validator = Validator::make($request->all(), [
                 'uploadfile' => 'mimes:jpg,jpeg,png,pdf'
             ]);

             if ($validator->fails())
             {
                 return response()->json(
                     [
                         'error' => 'Please upload only image and pdf file'
                     ]
                 );
             }
          }

         // message required  validation
          $validator = Validator::make($request->all(), [
             'message'   => 'required'
          ]);

         if ($validator->fails())
         {
             return response()->json(
                 [
                     'error' => 'Please write somthing'
                 ]
             );
         }


         if (request()->ajax())
         {
             $group_members   = request('group_members' , '');
              $send_message   = request('message' , '');
              $user_id="";
              $count=1;
              $one_time_file_upload ="";
              foreach ($group_members as $group_members_value )
              {
                  $user_id = $group_members_value;
                  info('Sending Direct Message To USER: '. $user_id);

                 if($user_id != '' && ! empty($user_id))
                 {
                     $response   = $this->member_model->get_member_detail_provider_detail(session('active_facility'), $user_id);
                     if(empty($response))
                     {
                         return response()->json([
                             'error' => 'Member detail not found'
                         ]);
                     }

                     if(!empty($response->lead_coach_id) || !empty($response->facility_default_lead_coach_id))
                     {
                         $sent_message_attachment ="";
                         $file_extension ="";
                         info('Sending Direct Message To Patient: '. $response->patient_id . 'By UserID: ' . $this->active_user_id);
                         if($response->lead_coach_id == $this->active_user_id)
                             $sent_message = $send_message;
                         else
                             $sent_message = 'By ' . session('user')->firstName .' '. session('user')->lastName .' : '. $send_message;

                         if($request->hasFile('uploadfile'))
                         {
                             if($count==1)
                             {
                                 $file = $request->file('uploadfile');
                                 if ($file->getClientOriginalExtension() == 'pdf')
                                     $destinationPath = config('healthslate.log_image_save_url') . 'pdf/';
                                 else
                                     $destinationPath = config('healthslate.log_image_save_url') . 'logImages/';
                                 $file_attachment_name = md5(time() . uniqid()) . '.' . $file->getClientOriginalExtension();
                                 $file->move($destinationPath, $file_attachment_name);
                                 info('Saving Attached File to Folder: ' . $file_attachment_name);
                             }
                         }

                         $android_payload_image_path = '';
                         $sent_message_for_push_notification = $sent_message;
                         $message_id = new Message;
                         $message_id->message        = $sent_message;
                         $message_id->owner          = 'PROVIDER';
                         $message_id->read_status    = DB::raw(0);
                         $message_id->timestamp      = (time() * 1000);
                         $message_id->patient_id     = $response->patient_id;
                         $message_id->user_id        = $this->active_user_id;
                         if($request->hasFile('uploadfile'))
                         {

                             if($file->getClientOriginalExtension() == 'pdf')
                             {
                                 $message_id->image_path = '/pdf/' . $file_attachment_name;
                                 $sent_message_attachment = '<span class="body"><img class="img-attachment" src="'.env('APP_BASE_URL').'/img/pdf.png'.'"></span>';
                                 $android_payload_image_path = env('APP_BASE_URL') .$message_id->image_path;
                             }
                             else
                             {
                                 $message_id->image_path = '/logImages/' . $file_attachment_name;
                                 $sent_message_attachment = '<span class="body"><img class="img-attachment" src="'.env('PROFILE_IMAGE_BASE_URL') .$message_id->image_path.'"></span>';
                                 $android_payload_image_path = env('PROFILE_IMAGE_BASE_URL') .$message_id->image_path;
                             }
                             $file_extension = $file->getClientOriginalExtension();
                         }
                         $message_id->save();

                       if($file_extension!="")
                         {
                             if($file_extension == "pdf")
                                 $sent_message_for_push_notification = session('user')->firstName.": \xf0\x9f\x93\x84 ".$sent_message;
                             elseif($file_extension == "jpg" || $file_extension == "jpeg" || $file_extension == "png")
                                 $sent_message_for_push_notification = session('user')->firstName.": \xf0\x9f\x93\xb7 ".$sent_message;

                         }

                         // Push notification Start
                       if($response->device_type != "" && $response->is_in_app_message_notify == 1)
                         {
                             info('Sending Push notification to User: ' . $response->patient_id . ' Device Type: ' . $response->device_type);
                             if ($response->registration_id != '' && $response->device_type == 'Android')
                             {
                                 if($android_payload_image_path != '')
                                    $android_payload = array('providerId' => $this->active_user_id, 'messageText' => $sent_message, 'isInAppMessageNotify' => true, 'senderFirstName' => session('user')->firstName, 'imagePath' => $android_payload_image_path);
                                 else
                                     $android_payload = array('providerId' => $this->active_user_id, 'messageText' => $sent_message, 'isInAppMessageNotify' => true, 'senderFirstName' => session('user')->firstName);

                                 $push = new PushNotification('gcm');
                                 $push->setMessage(['data' => ['data' => ['actions' => 'message', 'payload' => json_encode($android_payload) ],],])->setDevicesToken($response->registration_id)->send();
                                 Log::info('Notification Response: ' . print_r($push->getFeedback(), true));
                              } else if ($response->registration_id != '' && $response->device_type == 'IOS')
                              {
                                     $push = new PushNotification('apn');
                                     $push->setMessage(['aps' => ['alert' => $sent_message_for_push_notification,  'category' => 'IN_APP_MESSAGE','sound' => 'default'], 'event' => 'message',])->setDevicesToken($response->registration_id)->send();
                                     Log::info('Notification Response: ' . print_r($push->getFeedback(), true));
                               }
                           }
                         // Push notification End

                         if($response->patient_send_email_notify == 1)
                         {
                             $message_get_data = Message::where('patient_id', $response->patient_id)->orderBy( 'timestamp', 'desc')->limit(1)->first();
                             if(count($message_get_data) > 0)
                             {
                                 $time = ($message_get_data->timestamp/1000);
                                 if(date("Y-m-d ", $time) < date("Y-m-d ",time()))
                                 {
                                     info('Sending Mail to Patient: '. $response->patient_id );
                                     $name= $response->first_name;
                                     if(Session::get('user')->cobrandId==2){
                                         $message = "You have received a new message from your health coach. You can see it on the web here or by opening your app and looking for the red message indicator.If you need assistance please use the Help page in the app or call us at 888-291-7245.You can stop receiving email notification of coach messages by going in your SoleraOne app to Menu/Settings/Coach Communication. ";

                                     }else {
                                         $message = "You have received a new message from your health coach. You can see it on the web here or by opening your app and looking for the red message indicator.If you need assistance please use the Help page in the app or call us at 888-291-7245.You can stop receiving email notification of coach messages by going in your HealthSlate app to Menu/Settings/Coach Communication. ";
                                     }
                                     $body = ['name'=>$name, 'content' => $message,'subject' =>'Message from coach' ];
                                     $to = $response->email;
                                     $this->mailServiceHelper->sendEmail($body, trim($to));
                                 }
                             }
                         }



                     }
                     else
                     {
                         return response()->json([
                             'error' => 'Member Lifestyle Coach not found!'
                         ]);
                     }
                 }

                 $count++;

              }

             return $response = [
                 'success' => 'Message Send Successfully',

             ];

         }



     }

    /* save grop message end here*/


}

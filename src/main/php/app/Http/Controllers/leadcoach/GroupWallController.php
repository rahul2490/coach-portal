<?php

namespace HealthSlatePortal\Http\Controllers\leadcoach;
use Carbon\Carbon;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use HealthSlatePortal\Models\Eloquent\Post;
use HealthSlatePortal\Models\Eloquent\PostComment;
use Illuminate\Support\Facades\Log;
use Validator;
use Session;
use DB;
use ChristofferOK\LaravelEmojiOne\LaravelEmojiOne;
class GroupWallController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $group_model;
    protected $group_member;
    protected $dataTableService;
    protected $user;
    protected $data = array();

    function __construct( FacilityModel $facility_model, MemberModel $member_model,
                          Facility $facility, CoacheModel $coach_model, GroupModel $group_model,
                          DataTableServiceProvider $dataTableService, User $user, GroupMember $group_member) {
        $this->facility_model   = $facility_model;
        $this->coach_model      = $coach_model;
        $this->member_model     = $member_model;
        $this->group_model      = $group_model;
        $this->group_member     = $group_member;
        $this->facility         = $facility;
        $this->user             = $user;
        $this->dataTableService = $dataTableService;
        $this->group_db         = env('GROUP_DB_DATABASE');
        $this->data['js']       = array('group');
        $this->active_facility_id   = session('active_facility');
        $this->active_user_id       = session('userId');
        $this->active_user_role     = session('userRole');
        $this->active_user_name  = session('user')->firstName.''. session('user')->lastName;
    }



    /**
     * @param
     * @return Load view for Member
     */
	public function index($group_id) {
        $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
        $emoji = new LaravelEmojiOne();
        $group_id=base64_decode($group_id);
        $current_time=date('Y-m-d H:i:s');
        GroupMember::where('group_id',$group_id)->where('user_id',$this->active_user_id)->update(['post_read_time'=>$current_time]);
        $timezone = '';
        if(isset($_COOKIE['user_timezone'])) {
            $timezone =  $_COOKIE['user_timezone'];
        }

        $carbon = new Carbon();
        $carbon->setTimezone($timezone);
        $here = $carbon->now();
        $utc_timestamp = (time() * 1000);
        //$compare_timestamp=strtotime(date('Y-m-d 08:00:00'))*1000;
        $this->data['day_name'] = date('D',strtotime(date('Y-m-d H:i:s')));
        if(!empty($timezone))
        {
            $utc_timestamp = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $here)->timezone('UTC')->timestamp * 1000 : (time() * 1000);
        }

        $compare_timestamp= date("Y-m-d 08:00:00", strtotime('monday this week'));
        if(!empty($timezone)) {
            $compare_timestamp = (!empty($timezone)) ? Carbon::createFromFormat('Y-m-d H:i:s', $compare_timestamp)->timezone('UTC')->timestamp * 1000 : (time() * 1000);
        }

        $this->data['group_member'] = collect($this->group_model->get_group_member_list($group_id, session('active_facility')));
        if(count($this->data['group_member']) <= 0)
        {
            return redirect()->intended('/'.$this->active_user_role.'/group');
        }

        $patient_ids        =  $this->data['group_member']->where('user_type', 'PATIENT')->pluck( 'patient_id' )->toArray();
        if(count($patient_ids)>0) {
            $this->data['weight_details'] = $this->group_model->get_group_report_for_duration_weight_details($patient_ids);
        }else{
            $this->data['weight_details']=array();
        }
        $this->data['weight_lose']= getWeightLossByGroup($this->data['weight_details']);
        if(count($patient_ids)>0){
            $this->data['step_patient']=collect($this->group_model->get_patient_step_count($patient_ids,$utc_timestamp,$compare_timestamp));
        }else{
            $this->data['step_patient']=array();
        }
        $group_total_steps='';
        $total_member=0;
        foreach($this->data['step_patient'] as $step){
            if($step->total_steps != 0){
                $group_total_steps=$group_total_steps+$step->total_steps;
                $total_member= $total_member+1;
            }else{
                $group_total_steps=$group_total_steps+$step->total_steps;
                $total_member=$total_member;
            }

        }

        if($total_member > 0){
            $this->data['group_steps_average'] = round($group_total_steps/$total_member);
        }else{
            $this->data['group_steps_average']=0;
        }

        $group_post_ids = collect($this->group_model->get_group_post_ids($group_id, session('active_facility'),$getresult=0));
        $post_ids = $group_post_ids->pluck( 'post_id' )->toArray();
        $this->data['group_post']   = $this->group_model->get_group_post_by_group($group_id, session('active_facility'),$post_ids);
        $data = collect();

        foreach($this->data['group_post'] as $post)
        {
            if($data->contains('post_id', $post->post_id))
                continue;
            $user_like = 0;
            $comments = collect();
            $total_like = 0;
            foreach ($this->data['group_post'] as $comment)
            {
                if($post->post_id == $comment->post_comment_post_id && $comment->post_comment_id != '')
                {
                    if($comment->is_like)
                    {
                        $total_like += 1;
                        if($comment->comment_user_id ==  $this->active_user_id ){
                            $user_like = 1;
                        }

                        continue;
                    }
                   $timezone = '';
                    if(isset($_COOKIE['user_timezone'])) {
                        $timezone =  $_COOKIE['user_timezone'];
                    }
                    $comment_timestamp   = '';
                    //$last_unread_message_of_patient = '';
                    if(!empty($comment->comment_timestamp))
                    {
                        $comment_timestamp     = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s',$comment->comment_timestamp)->timezone($timezone) : Carbon::createFromFormat( 'Y-m-d H:i:s', $comment->comment_timestamp);
                        $comment_timestamp     = $comment_timestamp->diffForHumans();
                    }

                    if((!empty($comment->comment_user_image))){
                        //$comment_user_image = env('PROFILE_IMAGE_BASE_URL').$comment->comment_user_image;
                        $comment_user_image =env('APP_BASE_URL').'/img/default-user.png' ;
                    }else{
                        $comment_user_image =env('APP_BASE_URL').'/img/default-user.png' ;

                    }

                    //$comment1 = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $comment->comment);
                    $comment_row = [
                        'post_comment_id'   => $comment->post_comment_id,
                        'post_id'           => $comment->post_id,
                        'user_id'           => $comment->comment_user_id,
                        'comment'           => $emoji->shortnameToImage($comment->comment),
                        'is_like'           => $comment->is_like,
                        'media_url'         => $comment->comment_media_url,
                        'media_type'        => $comment->comment_media_type,
                        'timestamp'         => $comment_timestamp,
                        'commented_user_name'=>$comment->comment_user_name,
                        'comment_user_image'=> $comment_user_image,
                    ];
                    $comments = $comments->push($comment_row);
                    $comments->toArray();
                }
            }

            $post_description ='';
            $post_media_url='';
            $food_image='';
            $timezone = '';
            if(isset($_COOKIE['user_timezone'])) {
                $timezone =  $_COOKIE['user_timezone'];
            }
            $post_timestamp    = '';
            //$last_unread_message_of_patient = '';
            if(!empty($post->post_timestamp))
            {
                $post_timestamp     = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s',$post->post_timestamp)->timezone($timezone) : Carbon::createFromFormat( 'Y-m-d H:i:s',  $post->post_timestamp);
                $post_timestamp     =  $post_timestamp->diffForHumans();
            }



            if((!empty($post->post_user_image))&&($post->post_user_type=='PATIENT')){
                $post_image_url = env('PROFILE_IMAGE_BASE_URL').$post->post_user_image;
            }
            else if((empty($post->post_user_image))&&($post->post_user_type=='COACH')){
                $post_image_url = env('APP_BASE_URL').'/img/default-user.png';
            }else{
                $post_image_url = env('APP_BASE_URL').'/img/default-user.png';
            }


            if(($post->post_type=='Text')&&($post->post_media_type=='image') ||($post->post_media_type=='Image')){
                $post_description =$post->description;
                $post_media_url= env('LOG_IMAGE_BASE_URL').'patientapp-groups'.$post->post_media_url;
            }else{
                $post_description =$post->description;
                $post_media_url=$post->post_media_url;
            }
            if($post->post_type=='Foodlog'){
                $post_description =$post->description;
                $food_image=env('LOG_IMAGE_BASE_URL').$post->food_image;
            }
            if($post->post_type=='Html'){
                $post_description =$post->description;
                $post_media_url=$post->post_media_url;
            }

            $count_comments=count($comments);
            if($total_like==0){
                $total_like='';
            }
            if($count_comments==0){
                $count_comments = '';
            }
            // echo $post_image_url;
            //$post_description = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $post_description);
            $post_row = [
                'post_id'           => $post->post_id,
                'user_id'           => $post->user_id,
                'group_id'          => $post->group_id,
                'full_name'         => $post->full_name,
                'post_type'         => $post->post_type,
                'post_user_type'    => $post->post_user_type,
                'title'             => $post->title,
                'description'       => $emoji->shortnameToImage(nl2br($post_description)),
                'total_like'        => $total_like,
                'total_comment'     => $count_comments,
                'timestamp'         => $post_timestamp,
                'last_updated'      => $post->last_updated,
                'media_url'         => $post_media_url,
                'media_type'        => $post->post_media_type,
                'media_thumbnail'   => $post->media_thumbnail,
                'media_embed_url'   => $post->media_embed_url,
                'is_auto_post'      => $post->is_auto_post,
                'post_user_image'   => $post_image_url,
                'food_image'        => $food_image,
                'meal_type'         => $post->meal_type,
                'log_calories'      => $post->log_calories,
                'lead_user'         => $post->lead_user,
                'user_like'         =>  $user_like
            ];

            if(count($comments) > 0)
                $post_row['post_comments'] = $comments;

            $data->push($post_row);
        }
        $this->data['post_data']=$data;
        $this->data['group_id']=$group_id;
        $this->data['user_id']=$this->active_user_id;
       // print_r($this->data['post_data']);die;

        return view( 'leadcoach.mygroup.group-wall', $this->data);
	}



    /**
     * @param $active_facility
     * @return Json response of member data
     */
    public function membersList()
    {
            $response = $this->group_model->get_enrolled_member_list(session('active_facility'));
        info('Total Enrolled Members Found: ' . count($response));
        $data = collect();

        foreach ($response as $member) {
            $row = [
                'hidden_id'             => '',
                'patient_id'            => $member->patient_id,
                'name'                  => $member->full_name,
                'registration_date'     => ( ! empty( $member->registration_date ) ) ? Carbon::createFromFormat( 'Y-m-d H:i:s', $member->registration_date )->format(  config( 'healthslate.default_date_format' ) ) : '',
                'device_mac_address'    => '<input class="form-control" type="checkbox" value="'.$member->user_id.'"></input>',
                'app_version'           => '',
            ];
            $data->push(array_values($row));
        }
        info('Sending Data To DataTableService');
        return $this->dataTableService->dataTableWithIds($data);
    }



    /**
     * @param $active_facility
     * @return Json response of Group Data
     */
    public function groupList()
    {
        $group    = request('group' , 0);
        $capacity = request('capacity' , 0);
        $response = $this->group_model->get_facility_group_list(session('active_facility'), $group, $capacity);

        info('Total Group Found: ' . count($response));
        $data = collect();

        if(count($response) > 0)
        {
            foreach ($response as $group) {

                $group_members = $this->group_model->get_group_coaches_detail($group->group_id);
                $coach_list = '';$food_coach = ''; $life_coach = '';
                if(!empty($group_members))
                {
                    foreach ($group_members as $key => $value)
                    {
                        if($value->lead_user_id == $value->user_id)
                            $life_coach  = "<span>$value->full_name <strong>(Lifestyle Coach)</strong></span><br>";
                        else if ($value->primary_food_coach_id == $value->user_id)
                            $food_coach  = "<span>$value->full_name <strong>(Food Coach)</strong></span><br>";
                        else
                            $coach_list .= "<span>$value->full_name <strong>(Coach)</strong></span><br>";
                    }
                    $life_coach .= $food_coach .' '. $coach_list;
                }

                $icon_group = $group->is_person == 0 ? 'iconhs-onlinegroup' : 'iconhs-inperson';

                $delete_group = '';
                if($group->total_users == 0)
                    $delete_group = '<a  title="Delete Group" data-toggle="modal" data-id="'.$group->group_id.'" data-target="#remove_group" class="btn font-dark btn-sm btn-hide sbold pull-left"> <i class="fa fa-remove"></i>  </a>';
                $link = '';
                if($group->group_id)
                    $link = 'href="'.route('facilityadmin.members', ['group_id' => base64_encode($group->group_id)]).'"';

                $row = [
                    'hidden_id'       => '',
                    'group_name'      => '<div class="col-sm-1 success"><span class="group-type"><i class="'.$icon_group.'"></i></span></div>
                                            <div class="col-sm-11">
                                                <p><a class="memberid">'.$group->name.'</a><a class="pull-right font-dark" '.$link.' > '.$group->total_member.' Members</a> </p>
                                                <p><strong>Week '.$group->completed_week.'</strong><span class="pull-right">Started-'.date('M d',strtotime($group->timestamp)).'</span> </p>
                                          </div>',

                    'status'          => $life_coach,

                    'action'          => '',

                ];
                $data->push(array_values($row));
            }
        }

        info('Sending Data To DataTableService');
        return $this->dataTableService->dataTableWithIds($data);
    }



   public function comingsoon()
   {
    return view("facilityadmin.mygroup.comingsoon");

   }



   public function createGroup(Request $request)
   {
       if (request()->ajax()) {

           $messages = [
               'name.unique'    => 'Group Name already taken.',
           ];

           $validator = Validator::make(request()->input(), [
               'name'               => 'required|unique:groups.groups,name',
               'diabetesTypeId'     => 'required',
               'capacity'           => 'required|numeric',
               'group_type'         => 'required',
               'preferredLanguage'  => 'required',
               'selectLifeCoach'    => 'required',
           ], $messages);

           if ($validator->fails()) {
               info('Create Group Validation Failed');
               foreach ($validator->errors()->all() as $key => $value)
               {
                   return $response = [
                       'error' => $value,
                   ];
                   break;
               }
           }
           info('Create Group Validation Success');
           $loggedInUser = session('user');
           $user = $this->user->where('email', $loggedInUser->userName)->first();

           $total_member = 0;
           if(!empty($request->selectLifeCoach))
               $total_member = 1;
           if(!empty($request->selectFoodCoach))
               $total_member += 1;

           $group = new Groups;
           $group->name                     = $request->name;
           $group->lead_user_id             = $request->selectLifeCoach;
           $group->facility_admin_id        = $user->user_id;
           $group->primary_food_coach_id    = isset($request->selectFoodCoach) ? $request->selectFoodCoach : '';
           $group->timestamp                = date('Y-m-d H:i:s');
           $group->is_person                = $request->group_type;
           $group->capacity                 = $request->capacity;
           $group->is_full                  = $request->capacity <= $total_member ? 1 : 0;
           $group->diabetes_type_id         = $request->diabetesTypeId;
           $group->is_auto_post_enabled	    = $request->autoPostEnable == 1 ? 1 : 0 ;
           $group->auto_post_time	        = $request->autoPostTime;
           $group->preferred_language	    = $request->preferredLanguage;
           $group->facility_id              = session('active_facility');
           $group->save();

            if($group && !empty($request->selectLifeCoach))
            {
                $group_member_ld = new GroupMember;
                $group_member_ld->group_id      = $group->group_id;
                $group_member_ld->user_id       = $request->selectLifeCoach;
                $group_member_ld->timestamp     = date('Y-m-d H:i:s');
                $group_member_ld->save();
            }
           if($group && !empty($request->selectFoodCoach))
           {
               $group_member_fd = new GroupMember;
               $group_member_fd->group_id      = $group->group_id;
               $group_member_fd->user_id       = $request->selectFoodCoach;
               $group_member_fd->timestamp     = date('Y-m-d H:i:s');
               $group_member_fd->save();
           }

           if (!$group || !$group_member_ld) {
               return response()->json([
                   'error' => trans('common.unable_to_do_action', [ 'action' => 'create group'])
               ]);
           }

           return $response = [
               'success' => 'New Group Created Successfully!',
           ];
       }
   }



    public function updateGroup(Request $request)
    {
        if (request()->ajax()) {

            $messages = [
                'name.unique'    => 'Group Name already taken.',
            ];

            $validator = Validator::make(request()->input(), [
                'id'                 => 'required',
                'name'               => 'required|unique:groups.groups,name,'. $request->id.',group_id',
                //'diabetesTypeId'     => 'required',
                'capacity'           => 'required|numeric',
                'preferredLanguage'  => 'required',
            ], $messages);

            if ($validator->fails()) {
                info('Edit Group Validation Failed');
                foreach ($validator->errors()->all() as $key => $value)
                {
                    return $response = [
                        'error' => $value,
                    ];
                    break;
                }
            }
            info('Edit Group Validation Success');

            $group_detail = $this->group_model->get_group_detail($request->id);
            info('Total Group Member: '. $group_detail->total_member);
            $group = Groups::find($request->id);
            $group->name                     = $request->name;
            $group->capacity                 = $request->capacity;
            $group->is_full                  = $request->capacity <= $group_detail->total_member ? 1 : 0;

            if(!empty($request->diabetesTypeId))
                $group->diabetes_type_id         = $request->diabetesTypeId;

            $group->is_auto_post_enabled	 = $request->autoPostEnable == 1 ? 1 : 0 ;
            $group->auto_post_time	         = $request->autoPostTime;
            $group->preferred_language	     = $request->preferredLanguage;
            $group->update();

            //$this->group_member->where('group_id', $request->id)->where('user_id', $group_detail->lead_user_id)->update(['user_id' => $request->selectLifeCoach]);
            //if(!empty($request->selectFoodCoach))
                //$this->group_member->where('group_id', $request->id)->where('user_id', $group_detail->primary_food_coach_id)->update(['user_id' => $request->selectFoodCoach]);

            if (!$group) {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'Edit Group'])
                ]);
            }

            return $response = [
                'success' => 'Edit Group Successfully!',
            ];
        }
    }




    public function groupDetail()
    {
        if (request()->ajax()) {
            $group_id  = request('id' , 0);
            if($group_id)
            {
                $data = $this->group_model->get_group_detail($group_id);
                return $response = [
                    'success' => $data,
                ];
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'get detail'])
                ]);
            }
        }
    }



    /**
     * Process delete a Group From Facility
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteGroup() {

        $group_id = request('id');
        info('Deleting Group GroupId: ' . $group_id);
        $group_detail = $this->group_model->get_group_detail($group_id);
        if(!empty($group_detail))
        {
            if($group_detail->is_person)
            {
                info('Deleting a in_person Group');
                DB::table("$this->group_db.group_session")->where('group_id', $group_id)->delete();
                DB::table("$this->group_db.post_comment")->where('group_id', $group_id)->delete();
                DB::table("$this->group_db.post")->where('group_id', $group_id)->delete();
                $this->group_member->where('group_id', $group_id)->delete();
                DB::table("group_user")->where('group_id', $group_id)->delete();
                $is_person = Groups::find($group_id);
                $is_person->delete();

                if (!$is_person) {
                    return response()->json([
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Delete Group'])
                    ]);
                }
            }
            else
            {
                info('Deleting a Online Group');
                DB::table("$this->group_db.post_comment")->where('group_id', $group_id)->delete();
                DB::table("$this->group_db.post")->where('group_id', $group_id)->delete();
                $this->group_member->where('group_id', $group_id)->delete();
                DB::table("group_user")->where('group_id', $group_id)->delete();
                $online_group = Groups::find($group_id);
                $online_group->delete();

                if (!$online_group) {
                    return response()->json([
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Delete Group'])
                    ]);
                }
            }
        }

        return response()->json(
            [
                'success' => 'Group deleted Successfully!'
            ]
        );
    }



    public function removeCoachFromGroup()
    {
        if (request()->ajax()) {
            $group_id   = request('gid' , 0);
            $user_id    = request('uid' , 0);
            info('Deleting a Coach from Group: ' . $group_id);
            if($group_id)
            {
                $data = $this->group_member->where('group_id', $group_id)->where('user_id', $user_id)->delete();
                DB::table("group_user")->where('user_id', $user_id)->where('group_id', $group_id)->delete();
                if(!$data)
                {
                    return response()->json([
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Remove Coach'])
                    ]);
                }

                return $response = [
                    'success' => 'Coach Removed Successfully!',
                ];
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'Remove Coach'])
                ]);
            }
        }
    }



    /**
     * Process assign coach to Group
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignCoachToGroup()
    {

        if (request()->ajax()) {
            $coach_type = request('membership');
            $group_id   = request('group_id');

            if ($coach_type == 1)
                $coach_id = request('select_multi_food');
            else
                $coach_id = request('select_multi_life');

            $data = $this->group_member->where('group_id', $group_id)->where('user_id', $coach_id)->get();
            if(count($data) > 0)
            {
                return response()->json([
                    'error' => 'Selected Coach already exist into that Group'
                ]);
            }

            $loggedInUser = session('user');
            info('Assign New Coach To GroupID: ' . $group_id . ' Request by: ' . $loggedInUser->userName . ' Facility: ' . session('active_facility'));

            $group_member_ld = new GroupMember;
            $group_member_ld->group_id      = $group_id;
            $group_member_ld->user_id       = $coach_id;
            $group_member_ld->timestamp     = date('Y-m-d H:i:s');
            $group_member_ld->save();

            $group_user_id = new GroupUser;
            $group_user_id->group_id      = $group_id;
            $group_user_id->user_id       = $coach_id;
            $group_user_id->save();

            if (!$group_member_ld) {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', ['action' => 'Assign Coach'])
                ]);
            }

            return response()->json(
                [
                    'success' => 'Coach Assign To Group Successfully!'
                ]
            );
        }
    }

    /* For post Scroll by lead coach*/
    public function scroll_data() {
        $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
        $emoji = new LaravelEmojiOne();
        //$group_id=base64_decode($group_id = $group_id);
        $group_id=request('group_id');
        $getresult=request('getresult');

        $this->data['group_member'] = $this->group_model->get_group_member_list($group_id, session('active_facility'));
        $group_post_ids = collect($this->group_model->get_group_post_ids($group_id, session('active_facility'),$getresult));
        $post_ids = $group_post_ids->pluck( 'post_id' )->toArray();

        $this->data['group_post']   = $this->group_model->get_group_post_by_group($group_id, session('active_facility'),$post_ids);
        $data = collect();
        foreach($this->data['group_post'] as $post)
        {
            if($data->contains('post_id', $post->post_id))
                continue;
            $user_like=0;
            $comments = collect();
            $total_like = 0;
            foreach ($this->data['group_post'] as $comment)
            {
                if($post->post_id == $comment->post_comment_post_id && $comment->post_comment_id != '')
                {
                    if($comment->is_like)
                    {
                        $total_like += 1;
                        if($comment->comment_user_id ==  $this->active_user_id ){
                            $user_like = 1;
                        }
                        continue;
                    }



                    $timezone = '';
                    if(isset($_COOKIE['user_timezone'])) {
                        $timezone =  $_COOKIE['user_timezone'];
                    }
                    $comment_timestamp   = '';
                    //$last_unread_message_of_patient = '';
                    if(!empty($comment->comment_timestamp))
                    {
                        $comment_timestamp     = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s',$comment->comment_timestamp)->timezone($timezone) : Carbon::createFromFormat( 'Y-m-d H:i:s', $comment->comment_timestamp);
                        $comment_timestamp     = $comment_timestamp->diffForHumans();
                    }

                    if((!empty($comment->comment_user_image))){
                        //$comment_user_image = env('PROFILE_IMAGE_BASE_URL').$comment->comment_user_image;
                        $comment_user_image =env('APP_BASE_URL').'/img/default-user.png' ;
                    }else{
                        $comment_user_image =env('APP_BASE_URL').'/img/default-user.png' ;
                    }


                    $comment_row = [
                        'post_comment_id'   => $comment->post_comment_id,
                        'post_id'           => $comment->post_id,
                        'user_id'           => $comment->user_id,
                        'comment'           => $emoji->shortnameToImage($comment->comment),
                        'is_like'           => $comment->is_like,
                        'media_url'         => $comment->comment_media_url,
                        'media_type'        => $comment->comment_media_type,
                        'timestamp'         => $comment_timestamp,
                        'commented_user_name'=>$comment->comment_user_name,
                        'comment_user_image'=> $comment_user_image,
                    ];
                    $comments = $comments->push($comment_row);
                    $comments->toArray();
                }
            }

            $post_description ='';
            $post_media_url='';
            $food_image='';
            $timezone = '';
            if(isset($_COOKIE['user_timezone'])) {
                $timezone =  $_COOKIE['user_timezone'];
            }
            $post_timestamp    = '';
            //$last_unread_message_of_patient = '';
            if(!empty($post->post_timestamp))
            {
                $post_timestamp     = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s',$post->post_timestamp)->timezone($timezone) : Carbon::createFromFormat( 'Y-m-d H:i:s',  $post->post_timestamp);
                $post_timestamp     =  $post_timestamp->diffForHumans();
            }



            if((!empty($post->post_user_image))&&($post->post_user_type=='PATIENT')){
                $post_image_url = env('PROFILE_IMAGE_BASE_URL').$post->post_user_image;
            }
            else if((empty($post->post_user_image))&&($post->post_user_type=='COACH')){
                $post_image_url = env('APP_BASE_URL').'/img/default-user.png';
            }else{
                $post_image_url = env('APP_BASE_URL').'/img/default-user.png';
            }


            if($post->post_type=="Text" && $post->post_media_type=="image" || $post->post_media_type=='Image')
            {
                $post_description = $post->description;
                $post_media_url=env('LOG_IMAGE_BASE_URL').'patientapp-groups'.$post->post_media_url;
            }
            else
            {
                $post_description =$post->description;
                $post_media_url=$post->post_media_url;
            }
            if($post->post_type=='Foodlog'){
                $post_description =$post->description;
                $food_image=env('LOG_IMAGE_BASE_URL').$post->food_image;
            }
            if($post->post_type=='Html'){
                $post_description =$post->description;
                $post_media_url=$post->post_media_url;
            }



            $count_comments=count($comments);
            if($total_like==0){
                $total_like='';
            }
            if($count_comments==0){
                $count_comments = '';
            }

            // echo $post_image_url;

            $post_row = [
                'post_id'           => $post->post_id,
                'user_id'           => $post->user_id,
                'group_id'          => $post->group_id,
                'full_name'         => $post->full_name,
                'post_type'         => $post->post_type,
                'post_user_type'    => $post->post_user_type,
                'title'             => $post->title,
                'description'       => $emoji->shortnameToImage(nl2br($post_description)),
                'total_like'        => $total_like,
                'total_comment'     => $count_comments,
                'timestamp'         => $post_timestamp,
                'last_updated'      => $post->last_updated,
                'media_url'         => $post_media_url,
                'media_type'        => $post->post_media_type,
                'media_thumbnail'   => $post->media_thumbnail,
                'media_embed_url'   => $post->media_embed_url,
                'is_auto_post'      => $post->is_auto_post,
                'post_user_image'   => $post_image_url,
                'food_image'        => $food_image,
                'meal_type'         => $post->meal_type,
                'log_calories'      => $post->log_calories,
                 'user_like'        => $user_like
            ];

            if(count($comments) > 0)
                $post_row['post_comments'] = $comments;

            $data->push($post_row);
        }


        $this->data['post_data']=$data;
//      echo "<pre>";
//       print_r( $post_row);

        $return_html='';
        foreach( $this->data['post_data'] as $post){


            $return_html.='<li class="media" id="post_'.$post['post_id'].'">';
            if($post['is_auto_post']=='1') {
                $return_html .= '<a class="pull-left" href = "javascript:;" >
                <img class="todo-userpic" src = "'.env('APP_BASE_URL').'/img/HS_auto-post-icon.png'.'" width = "27px" height = "27px" > </a >';
            }
            else{
                $return_html.='<a class="pull-left" href = "javascript:;" >
            <img class="todo-userpic" src = "'.$post['post_user_image'].'" width = "27px" height = "27px" > </a >';
            }
            $return_html.='<div class="media-body todo-comment">';

            $return_html.='<div class="actions pull-right">';
            $return_html.=' <a post_id="'.$post['post_id'].'" class="btn font-grey-salsa btn-sm delete-group-post" href="javascript:"><i class="fa fa-trash"></i> Delete</a>';
            if($post['post_type']=='Text' && $post['is_auto_post']!='1' && $post['user_id']== $this->active_user_id){
                $return_html.='<a class="btn font-grey-salsa btn-sm " href="#edit_post" data-id="'.$post['post_id'].'" data-toggle="modal"><i class="fa fa-pencil"></i> Edit</a>';
            }

            $return_html.='</div> <p class="todo-comment-head">';

            if($post['is_auto_post']=='1') {
                $return_html .= '<span class="todo-comment-username">HealthSlate</span> &nbsp';
            }
            else {
                $return_html .= '<span class="todo-comment-username">' . $post['full_name'] . '</span> &nbsp';
            }

            $return_html.='<span class="todo-comment-date">'. $post['timestamp'].'</span>';
            $return_html.='</p>';

            if($post['post_type']=="Html"){
                $return_html.='<div id="desc_'.$post['post_id'].'"><p class="todo-text-color">'.$post['description'].'</p></div>';
            }
            if($post['post_type']=="Text"){

                $return_html.='<div id="desc_'.$post['post_id'].'"><p class="todo-text-color">'.$post['description'].'</p></div>';

                if($post['media_type']=="image" && !empty($post['media_type'])){
                    $return_html .= '<img id="img_'.$post['post_id'].'" src="' . $post['media_url'] . '">';
                }
                else{
                    $return_html .= '';
                }
            }

            if($post['post_type']=="Foodlog"){
                $return_html.='<div id="desc_'.$post['post_id'].'"><p class="todo-text-color">'.$post['description'].'</p></div>';
                $return_html.='<p class="todo-text-color"><span>'.$post['meal_type'].'</span> | <span>'.$post['log_calories'].' Cal</span></p>';
                $return_html.='<img  src="'.$post['food_image'].'">';
            }

           $return_html.= '<div class="media-comment_'.$post['post_id'].'" id="media-comment_'.$post['post_id'].'">';
            if(!empty($post['post_comments'] ) ){
                foreach($post['post_comments'] as $comments){
                    $return_html.='<div class="media" id="post_comment_'.$comments['post_comment_id'].'">';
                    $return_html.='<a class="pull-left" href="javascript:;">';

                    $return_html.='<img class="todo-userpic" src="'.$comments['comment_user_image'].'" width="27px" height="27px"> </a>';
                    $return_html.=' <a post_comment_id="'.$comments['post_comment_id'].'"  post_id="'.$post['post_id'].'" class="btn font-grey-salsa btn-sm delete-post-comment pull-right" href="javascript:;">
                                    <i class="fa fa-trash"></i> Delete </a>';

                    $return_html.='<div class="media-body">';


                    $return_html.='<p class="todo-comment-head"><span class="todo-comment-username">'.$comments['commented_user_name'].'</span> &nbsp';
                    $return_html.='<span class="todo-comment-date">'.$comments['timestamp'].'</span></p>';

                    $return_html.='<p class="todo-text-color">'.$comments['comment'].'</p>';

                    $return_html.='</div>';
                    $return_html.='</div>';
                }
            }else{
                $return_html.='';
            }
            $return_html.= '</div>';
            $return_html.='<div class="actions pull-right margin-btm-10 margin-t-m-10">';
            $return_html.='<small id="like_'.$post['post_id'].'"> '.$post['total_like'].' </small>';
            if($post['user_like']==1){
            $return_html.='<a  post_id="'.$post['post_id'].'" class="btn btn-circle btn-icon-only btn-default _like post-like green-jungle"><i class="fa fa-thumbs-up"></i></a>';
            }else{
             $return_html.='<a  post_id="'.$post['post_id'].'" class="btn btn-circle btn-icon-only btn-default _like post-like"><i class="fa fa-thumbs-up"></i></a>';
            }

            $return_html.='<small id="total_comment_'.$post['post_id'].'"> '.$post['total_comment'].' </small> <a post_id="'.$post['post_id'].'" class="btn btn-circle btn-icon-only btn-default _comment post-comment"><i class="fa fa-commenting"></i></a></div><div class="clearfix"></div>
             <div class="_comment-box " id="comment_'.$post['post_id'].'" style="display:none">
                                                                    <ul class="media-list">
                                                                        <li class="media">
                                                                            <a class="pull-left" href="javascript:;">
                                                                                <img class="todo-userpic" src="'.$post['post_user_image'].'" width="27px" height="27px"> </a>
                                                                            <div class="media-body">
                                                                                <textarea class="form-control todo-taskbody-taskdesc" id="text_'.$post['post_id'].'" rows="2" placeholder="Comment"></textarea>
                                                                             <p class="text-error" style="display:none;color:red;"></p>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                    <button post_id="'.$post['post_id'].'" type="button" class="pull-right btn btn-sm green-jungle margin-t-b-10 add-comment" > &nbsp; Submit &nbsp; </button>
                                                                </div></div></li>';

        }


        return $return_html;
    }

    /* For post Delete by lead coach*/
    public function delete_post(){
        $post_id = request('post_id');

        $deleted = false;
        info('Deleting Post From group: ' . $post_id );
        PostComment::where('post_id', $post_id)->delete();
        $deleted = Post::where('post_id', $post_id)->delete();


//        if (!$deleted) {
//            return response()->json([
//                'error' => trans('common.unable_to_do_action',
//                    [ 'action' => 'remove post'])
//            ]);
//        }

        return response()->json(
            [
                'success' => 'Post deleted successfully.'
            ]
        );
    }

    /* For post like by lead coach*/
    public function like_post_by_coach()
    {

        $timezone = '';
        if(isset($_COOKIE['user_timezone'])) {
            $timezone =  $_COOKIE['user_timezone'];
        }

        $post_id = request('post_id');
        $group_id = request('group_id');
        $loggedInUser =  $this->active_user_id ;

        $PostComment = new PostComment;
        $PostComment->post_id  = $post_id;
        $PostComment->comment  = Null;
        $PostComment->is_like  = 1;
        $PostComment->timestamp=date('Y-m-d H:i:s');
        $PostComment->user_id  = $loggedInUser;
        $PostComment->group_id  = $group_id;
        $PostComment->media_url  = Null;
        $PostComment->media_type  = Null;
        $PostComment->save();
        $response_data =  $this->group_model->get_count_is_like($post_id);
        $response=$response_data[0]->total_like;
        info('post like inserted successfully');
        return $response;
//        return response()->json(
//            [
//                'success' => 'Post Like successfully.'
//            ]
//        );

    }


    /* Function for like post*/
    public function remove_like_post_by_coach()
    {
        $post_id = request('post_id');
        $group_id = request('group_id');
        $loggedInUser =  $this->active_user_id ;
        PostComment::where('post_id', $post_id)->where('group_id', $group_id)->where('user_id', $loggedInUser)->delete();
        $response_data =  $this->group_model->get_count_is_like($post_id);
        if(!empty($response_data)){
            $response=$response_data[0]->total_like;
        }else{
            $response=0;
        }

        info('post like deleted successfully');
        return $response;

//        return response()->json(
//            [
//                'success' => 'Post Like deleted successfully.'
//            ]
//        );

    }


    /* Function for add post comment */
    public function add_comment()
    {

        $timezone = '';
        if(isset($_COOKIE['user_timezone'])) {
            $timezone =  $_COOKIE['user_timezone'];
        }
        $current_timestamp = date('Y-m-d H:i:s');
        $comment_date_time   = '';


        if($current_timestamp)
        {
            $comment_date_time     = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s',$current_timestamp)->timezone($timezone) : Carbon::createFromFormat( 'Y-m-d H:i:s', $current_timestamp);
            $comment_date_time     = $comment_date_time->diffForHumans();
        }
        $emoji = new LaravelEmojiOne();
        $post_id = request('post_id');
        $group_id = request('group_id');
        $comment=request('comment');
        $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
        $comment = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $comment);
        $loggedInUser =  $this->active_user_id ;
        $PostComment = new PostComment;
        $PostComment->post_id  = $post_id;
        $PostComment->comment  = $emoji->shortnameToImage($comment);
        $PostComment->is_like  = Null;
        $PostComment->timestamp = date('Y-m-d H:i:s');
        $PostComment->user_id  = $loggedInUser;
        $PostComment->group_id  = $group_id;
        $PostComment->media_url  = Null;
        $PostComment->media_type  = Null;
        $PostComment->save();
        $post_comment_id=$PostComment->post_comment_id;
        $return_html='';
        $return_html='<div class="media" id="post_comment_'.$post_comment_id.'">
        <a class="pull-left" href="javascript:;">
        <img class="todo-userpic" src="'.env('APP_BASE_URL').'/img/default-user.png" width="27px" height="27px"> </a>
        <a post_comment_id="'.$post_comment_id.'"  post_id="'.$post_id.'" class="btn font-grey-salsa btn-sm delete-post-comment pull-right" href="javascript:;">
         <i class="fa fa-trash"></i> Delete
         </a>
        <div class="media-body">
        <p class="todo-comment-head"><span class="todo-comment-username">'.$this->active_user_name.'</span> &nbsp
        <span class="todo-comment-date"> '.$comment_date_time.' </span></p>
        <p class="todo-text-color">'.$comment.'</p>
        </div>
        </div>';

        info('post comment added successfully');
        return $return_html;

//        return response()->json(
//            [
//                'success' => 'Post Like deleted successfully.'
//            ]
//        );

    }

    /* Function for like post*/
    public function remove_post_comment()
    {
        $comment_id = request('comment_id');
        $post_id = request('post_id');
        PostComment::where('post_comment_id', $comment_id)->delete();
        $response_data =  $this->group_model->get_count_comment($post_id);
        if(!empty($response_data)){
            $response=$response_data[0]->total_comment;
        }else{
            $response=0;
        }

        info('Comment deleted successfully');
         return response()->json(
            [
                'success' => "comment deleted successfully",
                'data'    =>$response,
                'error'=>'Error in post comment deletion',
            ]
        );

    }

    public function postDetail()
    {
        if (request()->ajax()) {
            $post_id  = request('id' , 0);
            if($post_id)
            {
                $data = $this->group_model->get_post_detail($post_id);
                $post_id     =       $data->post_id;
                $description = $data->description;
                $post_type  = $data->post_type;
                $media_type = $data->media_type;
                if($media_type=='image'){
                    $media_url  = env('LOG_IMAGE_BASE_URL').'patientapp-groups'.$data->media_url;
                }else{
                    $media_url=  $data->media_url;
                }
                $emoji = new LaravelEmojiOne();

                $group_id = $data->group_id;
                $user_id = $data->user_id;
                $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
              // $description = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $description);

                $new_data = (object)array(
                'post_id'     =>       $post_id,
                'description' => $emoji->shortnameToImage(nl2br($description)),
                'post_type'  => $post_type,
                'media_url'  =>$media_url,
                'media_type' => $media_type,
                'group_id'   => $group_id,
                'user_id'    => $user_id,
                'img_path'   => $data->media_url,

                );

                return $response = [
                    'success' => $new_data,
                ];
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'get detail'])
                ]);
            }
        }
    }

    public function update_post(Request $request)
    {
        $user_id =  $this->active_user_id ;
        if($request->hasFile('uploadpostfile'))
        {
            $validator = Validator::make($request->all(), [
                'uploadpostfile' => 'mimes:jpg,jpeg,png'
            ]);

            if ($validator->fails())
            {
                return response()->json(
                    [
                        'error' => 'Please upload only image file'
                    ]
                );
            }
        }
        if (request()->ajax()) {
            $emoji = new LaravelEmojiOne();
            $post_id  =  $request->input('post_id');
            $group_id= $request->input('group_id');
            $description = $request->input('post_description');
            $attach=$request->input('file_name');
            //$url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
            $url = "/(?i)\b((?:https?:\/\/|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))/";
            $description = preg_replace($url, '<a href="$1" target="_blank" title="$1">$1</a>', $description);
            $update='';
            if($request->hasFile('uploadpostfile'))
            {
                    $file = $request->file('uploadpostfile');
                    if($file) {
                        $destinationPath = config('healthslate.log_post_image_save_url') . 'images/postImages/';
                        $file_attachment_name = md5(time() . uniqid()) . '.' . $file->getClientOriginalExtension();
                        $file_name_upload = '/images/postImages/' . $file_attachment_name;
                        $file_name= env('LOG_IMAGE_BASE_URL').'patientapp-groups'.$file_name_upload;
                        $file->move($destinationPath, $file_attachment_name);
                        info('Saving Attached File to Folder: ' . $file_name_upload);
                        $user_id = $this->active_user_id;
                        Post::where('post_id',$post_id)->update(['description' =>$emoji->toShort($description),'media_url'=>$file_name_upload,'media_type'=>'image']);
                        $update = 1;
                    }
            }else{
                $user_id= $this->active_user_id ;
                Post::where('post_id',$post_id)->update(['description' =>$emoji->toShort($description)]);
                info('post updated ');
                $update = 1;
                if($attach == ""){
                    $file_name=null;
                }else{
                    $file_name=$attach;
                }

            }
            $data=array(
                'description'=>$emoji->shortnameToImage(nl2br($description)),
                'post_id'=>$post_id,
                'post_update'=>$update,
                'post_media_url'=>$file_name,
            );
            return response()->json(
                [
                    'success' => 'Post updated successfully.',
                    'data'=> $data,
                    'error'=>'Error in post update',

                ]
            );
        }
    }

    public function add_new_post(Request $request){
        $emoji = new LaravelEmojiOne();
        $user_id =  $this->active_user_id ;
        $group_id = $request->input('post_group_id');
        $description = $request->input('new_post');
        $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
        $description = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $description);

        $timezone = '';
        if(isset($_COOKIE['user_timezone'])) {
            $timezone =  $_COOKIE['user_timezone'];
        }

        if($request->hasFile('newpostfile'))
        {
            $validator = Validator::make($request->all(), [
                'newpostfile' => 'mimes:jpg,jpeg,png'
            ]);

            if ($validator->fails())
            {
                return response()->json(
                    [
                        'error' => 'Please upload only image file'
                    ]
                );
            }
        }

        if($request->hasFile('newpostfile'))
        {
            $file = $request->file('newpostfile');
            if($file) {
                $destinationPath = config('healthslate.log_post_image_save_url') . 'images/postImages/';
                $file_attachment_name = md5(time() . uniqid()) . '.' . $file->getClientOriginalExtension();
                $file_name = '/images/postImages/' . $file_attachment_name;
                $file->move($destinationPath, $file_attachment_name);
                info('Saving Attached File for new post to Folder: ' . $file_name);
                $Post = new Post;
                $Post->post_type  = 'Text';
                $Post->description  = $emoji->toShort($description);
                $Post->timestamp = date('Y-m-d H:i:s');
                $Post->user_id  = $user_id;
                $Post->group_id  = $group_id;
                $Post->media_url  = $file_name;
                $Post->media_type  = 'image';
                $Post->save();
                $post_id = $Post->post_id;
                $post_user_image = env('APP_BASE_URL').'/img/default-user.png';
                $post_image_path=env('LOG_IMAGE_BASE_URL').'patientapp-groups'.$file_name;

            }


        }else{
            $user_id= $this->active_user_id ;
            $Post = new Post;
            $Post->post_type  = 'Text';
            $Post->description  = $emoji->toShort($description);
            $Post->timestamp = date('Y-m-d H:i:s');
            $Post->user_id  = $user_id;
            $Post->group_id  = $group_id;
            $Post->media_url  = null;
            $Post->media_type  = null;
            $Post->save();
            $post_id = $Post->post_id;
            $post_user_image = env('APP_BASE_URL').'/img/default-user.png';
            $post_image_path='';

        }


        $current_timestamp = date('Y-m-d H:i:s');
        $comment_date_time   = '';


        if($current_timestamp)
        {
            $comment_date_time     = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s',$current_timestamp)->timezone($timezone) : Carbon::createFromFormat( 'Y-m-d H:i:s', $current_timestamp);
            $comment_date_time     = $comment_date_time->diffForHumans();
        }
        $return_html='';
        $return_html.='<li class="media" id="post_'.$post_id.'">';
        $return_html.='<a class="pull-left" href = "javascript:;" >
        <img class="todo-userpic" src = "'.$post_user_image.'" width = "27px" height = "27px" > </a >';
        $return_html.='<div class="media-body todo-comment">';
        $return_html.='<div class="actions pull-right">';
        $return_html.=' <a post_id="'.$post_id.'" class="btn font-grey-salsa btn-sm delete-group-post" href="javascript:"><i class="fa fa-trash"></i> Delete</a>';
        if($Post->post_type=='Text' && $Post->is_auto_post != '1' && $Post->user_id == $this->active_user_id){
            $return_html.='<a class="btn font-grey-salsa btn-sm " href="#edit_post" data-id="'.$post_id.'" data-toggle="modal"><i class="fa fa-pencil"></i> Edit </a>';
        }
        $return_html.='</div> <p class="todo-comment-head">';
        $return_html .= '<span class="todo-comment-username">' .  $this->active_user_name  . '</span> &nbsp';
        $return_html.='<span class="todo-comment-date">'. $comment_date_time.'</span>';
        $return_html.='</p>';
        $return_html.='<div id="desc_'.$post_id.'"><p class="todo-text-color">'. $emoji->shortnameToImage(nl2br($Post->description)) .'</p></div>';
        if( $Post->media_type =="image" || $Post->media_type == "Image" && !empty($Post->media_type)){
                $return_html .= '<img  id="img_'.$post_id.'" src="' . $post_image_path. '">';
            }
            else{
                $return_html .= '';
            }
        $return_html.= '<div class="media-comment_'.$post_id.'" id="media-comment_'.$post_id.'"></div><div class="actions pull-right margin-btm-10 margin-t-m-10">';
        $return_html.='<small id="like_'.$post_id.'">  </small>';

        $return_html.='<a  post_id="'.$post_id.'" class="btn btn-circle btn-icon-only btn-default _like post-like"><i class="fa fa-thumbs-up"></i></a>';
        $return_html.='<small id="total_comment_'.$post_id.'">  </small>
         <a post_id="'.$post_id.'" class="btn btn-circle btn-icon-only btn-default _comment post-comment"><i class="fa fa-commenting"></i></a></div><div class="clearfix"></div>
             <div class="_comment-box " id="comment_'.$post_id.'" style="display:none">
                                                                    <ul class="media-list">
                                                                        <li class="media">
                                                                            <a class="pull-left" href="javascript:;">
                                                                                <img class="todo-userpic" src="'.$post_user_image.'" width="27px" height="27px"> </a>
                                                                            <div class="media-body">
                                                                                <textarea class="form-control todo-taskbody-taskdesc" id="text_'.$post_id.'" rows="2" placeholder="Comment"></textarea>
                                                                             <p class="text-error" style="display:none;color:red;"></p>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                    <button post_id="'.$post_id.'" type="button" class="pull-right btn btn-sm green-jungle margin-t-b-10 add-comment" > &nbsp; Submit &nbsp; </button>
                                                                </div></div></li>';

        return response()->json(
            [
                'success' => 'Post creates successfully.',
                'return_html'=>$return_html,

            ]
        );
    }


    public function delete_post_image(){
        $post_id = request('post_id');
        if($post_id !=''){
            Post::where('post_id',$post_id)->update(['media_url'=>null,'media_type'=>null]);
        }

        return response()->json(
            [
                'success' => 'Post image deleted successfully.',
                'error'=>'Error in post image deletion',

            ]
        );

    }


}

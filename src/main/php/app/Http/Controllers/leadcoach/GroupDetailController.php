<?php

namespace HealthSlatePortal\Http\Controllers\leadcoach;
use Carbon\Carbon;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Models\CurriculumServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\GroupDetailModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use Illuminate\Support\Facades\Log;
use Validator;
use Session;
use DB;
class GroupDetailController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $group_model;
    protected $group_detail;
    protected $group_member;
    protected $dataTableService;
    protected $user;
    protected $curriculumServiceProvider;
    protected $data = array();

    function __construct( FacilityModel $facility_model, MemberModel $member_model, GroupDetailModel $group_detail,
                          Facility $facility, CoacheModel $coach_model, GroupModel $group_model,
                          DataTableServiceProvider $dataTableService, User $user, GroupMember $group_member, CurriculumServiceProvider $curriculumServiceProvider) {
        $this->facility_model   = $facility_model;
        $this->coach_model      = $coach_model;
        $this->member_model     = $member_model;
        $this->group_model      = $group_model;
        $this->group_detail     = $group_detail;
        $this->group_member     = $group_member;
        $this->facility         = $facility;
        $this->user             = $user;
        $this->dataTableService = $dataTableService;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->group_db         = env('GROUP_DB_DATABASE');
        $this->active_facility_id   = session('active_facility');
        $this->active_user_role     = session('userRole');
        $this->data['common_js']= array('group-detail');
    }


    /**
     * @param
     * @return Load view for Member
     */
	public function index($group_id) {

        $group_detail = $this->group_model->get_group_detail($group_id);

        if(empty($group_detail->group_id))
            return redirect()->intended('/'.$this->active_user_role.'/group');
        if($group_detail->facility_id != $this->active_facility_id)
            return redirect()->intended('/'.$this->active_user_role.'/group');
        if($group_detail->is_person == 1)
            abort(403, 'Unauthorized action.');

        $group_member_list  = collect($this->group_model->get_group_member_list($group_id, $this->active_facility_id));
        $patient_ids        = $group_member_list->where('user_type', 'PATIENT')->pluck( 'patient_id' )->toArray();
        $lead_coach         = $group_member_list->where('user_id', $group_detail->lead_user_id)->first();

        if(count($lead_coach) == 0)
            abort(403, 'Unauthorized action.');

        $member_details = '';
        if(!empty($patient_ids))
        {
            $member_details = $this->apiGroupParticipationReport($patient_ids);
        }

        $this->data['group_detail'] = $group_detail;
        $this->data['coach_detail'] = $lead_coach;
        $this->data['member_group_detail'] = $member_details;

        return view( 'leadcoach.mygroup.group-detail', $this->data);
	}





    /**
     * @param Validator $validator
     *
     * @return array
     */
    public function apiGroupParticipationReport($patient_ids){

        info( 'Patient Ids are:', $patient_ids );
        $now = Carbon::now();
        info( 'DateTime Today is: ' . $now );
        if($now->isSunday()){
            $last_sunday = $now->copy()->startOfDay();
        }else{
            $last_sunday = new Carbon('last sunday');
        }

        $start = $last_sunday;
        $end   = $now;
        info( 'Processing Group report for: ' . $start . ' - ' . $end );

        $last_week_start = $start->copy()->subWeeks(1);
        $last_week_end = $start;

        info( 'Prior week will be: ' . $last_week_start . ' - ' . $last_week_end );
        $result = [];

        $patients = $this->group_detail->get_group_report_for_duration( $patient_ids, $start, $end, $last_week_start, $last_week_end );

        $patient_weight = $this->group_detail->get_group_report_for_duration_weight_details( $patient_ids, $start, $end, $last_week_start, $last_week_end );


        if(!empty($patients)) {
            foreach ($patients as $p_key => $p_row) {
                $p_current_weight    		= $this->in_multiarray($p_row->patient_id, $patient_weight,"current_weight");
                $p_prior_week_weight 		= $this->in_multiarray($p_row->patient_id, $patient_weight,"prior_week_weight");
                $p_row->current_weight 		=  isset($p_current_weight) ? $p_current_weight : null;
                $p_row->prior_week_weight 	=  isset($p_prior_week_weight) ? $p_prior_week_weight : null;
            }
        }

        $this_week_activity_minutes  = collect( $this->group_detail->get_patients_synced_minutes( $patient_ids, $start, $end ) );

        $prior_week_activity_minutes = collect( $this->group_detail->get_patients_synced_minutes( $patient_ids, $last_week_start, $last_week_end ) );

        // week number has starting index 0 on curriculum server
        $patients = $this->curriculumServiceProvider->getUnitsCompletedInSection($patients);

        $patients = $this->curriculumServiceProvider->getSectionListByPatientType($patients);

        $group_weight_loss = $this->getWeightLossByGroup($patients, null);

        foreach ( $patients as $record ) {

            $is_week_one = ( $record->member_completed_week == 1 ) ? true : false;

            $goal = $this->getWeightLossByGroup([$record], null);

            $minutes = floatval( $record->manual_activity_minutes );
            $prior_week_minutes = intval( $record->prior_week_manual_activity_minutes );

            $this_week = $this_week_activity_minutes->where( 'patient_id', $record->patient_id )->first();
            if ( ! empty( $this_week ) ) {
                $minutes = $minutes + floatval( $this_week->total_activity_minutes_synced );
            }
            $last_week = $prior_week_activity_minutes->where( 'patient_id', $record->patient_id )->first();
            if ( ! empty( $last_week ) ) {
                $prior_week_minutes = $prior_week_minutes + floatval( $last_week->total_activity_minutes_synced );
            }
            // for week 1, starting_weight will be prior weight
            $prior_weight = $is_week_one ? $record->starting_weight : ( is_null( $record->prior_week_weight ) ? $record->prior_week_weight : floatval( $record->prior_week_weight ) );

            $goal_achieved = null;
            $change_since_start_weight = null;
            $change_since_start_weight_percent = null;
            // consider This week weight
            // if not present then fall back to prior weight
            if(!is_null($record->current_weight) ){
                $goal_achieved = round($goal->goal_completion_percentage);
                $change_since_start_weight = floatval($record->current_weight) - floatval($record->starting_weight);
                $change_since_start_weight_percent = ( ! empty( $record->current_weight ) ) ? ( ( $change_since_start_weight / $record->current_weight ) * 100 ) : $change_since_start_weight_percent;
            }else if(!empty($prior_weight)){
                $data_set = (object) [ 'starting_weight' => $record->starting_weight, 'current_weight' => $prior_weight, 'target_weight' => $record->target_weight];
                $prior_goal = $this->getWeightLossByGroup([$data_set], null);
                $goal_achieved = round($prior_goal->goal_completion_percentage);
                // for week 1, change since starting should be null, if current weight is not present.
                if(!$is_week_one){
                    $change_since_start_weight         = ( floatval( $prior_weight ) - floatval( $record->starting_weight ) );
                    $change_since_start_weight_percent = ( ! empty( $prior_weight ) ) ? ( ( $change_since_start_weight / floatval( $prior_weight ) ) * 100 ) : $change_since_start_weight_percent;
                }
            }

            $change_since_prior_weight   = null;
            $change_since_prior_activity = null;
            if ( ! empty( $record->current_weight ) && ! empty( $prior_weight ) ) {
                $change_since_prior_weight = $record->current_weight - $prior_weight;
            }
            if ( ! empty( $minutes ) && ! empty( $prior_week_minutes ) ) {
                $change_since_prior_activity = $minutes - $prior_week_minutes;
            }

            $patient_profile_image = env('APP_BASE_URL').'img/default-user.png';
            if($record->patient_profile_image != '')
            {
                $patient_profile_image = env('PROFILE_IMAGE_BASE_URL') . $record->patient_profile_image;
            }

            $result[] = [
                'userTypeId'                    => $record->patient_id,
                'full_name'                     => $record->full_name,
                'patient_profile_image'         => $patient_profile_image,
                'thisWeekWeight'                => is_null( $record->current_weight ) ? $record->current_weight : floatval( $record->current_weight ),
                'priorWeight'                   => $prior_weight,
                'startingWeight'                => $record->starting_weight,
                'targetWeight'                  => $record->target_weight,
                'goalAcheived'                  => $goal_achieved,
                'thisWeekActivityMinutes'       => $minutes,
                'priorWeekActivityMinutes'      => $is_week_one ? null : $prior_week_minutes,
                'changeSinceStartWeight'        => $change_since_start_weight,
                'changeSinceStartWeightPercent' => $change_since_start_weight_percent,
                'changeSincePriorWeight'        => $change_since_prior_weight,
                'changeSincePriorActivity'      => $change_since_prior_activity,
                'logDaysCount'                  => $record->days_logged,
                'logsCount'                     => $record->total_logs,
                'viewedContentCount'            => isset( $record->viewed_units_in_week ) ? ( $record->viewed_units_in_week ) : 0,
                'totalContentCount'             => isset( $record->units_in_week ) ? ( $record->units_in_week ) : 0,
                'programWeek'             		=> isset( $record->member_completed_week ) ? ( $record->member_completed_week + 1) : 1,
                'topicName'						=> isset( $record->sectionTitle ) ? ( $record->sectionTitle ) : null,
                'total_weight_loss'             => $group_weight_loss->total_weight_loss,
                'goal_completion_percentage'    => round($group_weight_loss->goal_completion_percentage),
            ];
        }
        return $result;
    }


    /**
     * @Desc: multidimensional arrays check key exist
     * @param
     *
     * @return array
     */
    function in_multiarray($elem,$patient_weight,$field)
    {
        foreach ($patient_weight as $p_key => $p_row) {
            if($p_row->patient_id == $elem)
                return $p_row->$field;
        }
        return null;
    }



    private function getWeightLossByGroup($patients, $no_data_message = 'Group target not set'){
        /*$patients = [
            (object) [ 'starting_weight' => 120, 'current_weight' => 110, 'target_weight' => 90],
            (object) [ 'starting_weight' => 130, 'current_weight' => 140, 'target_weight' => 100],
            (object) [ 'starting_weight' => 130, 'current_weight' => 130, 'target_weight' => 100],
            (object) [ 'starting_weight' => 150, 'current_weight' => 100, 'target_weight' => 120],
            (object) [ 'starting_weight' => 200, 'current_weight' => 150, 'target_weight' => 150],
            (object) [ 'starting_weight' => 150, 'current_weight' => 141, 'target_weight' => 100],
        ];*/



        $group_weight_loss          = (object) [ ];
        $weight_loss                = 0;
        $target_weight_loss         = 0;
        $goal_completion_percentage = 0;
        foreach ( $patients as $key => $patient ) {

            $starting_weight = floatval( $patient->starting_weight );
            $current_weight  = floatval( $patient->current_weight );
            $target_weight   = floatval( $patient->target_weight );

            if ( ! empty( $current_weight ) && $current_weight > 0 ) {
                // only +ve loss is counted towards group goal
                // ony till 100% is counted if done more than 100% loss.
                if($starting_weight >= $current_weight) {
                    if ( $current_weight >= $target_weight ) {
                        $weight_loss_temp = $starting_weight - $current_weight;

                    } else {
                        $weight_loss_temp = $starting_weight - $target_weight;

                    }
                    $weight_loss        = $weight_loss + $weight_loss_temp;
                    $target_weight_loss_temp = $starting_weight - $target_weight;
                    $target_weight_loss = $target_weight_loss + $target_weight_loss_temp;
                    info( 'Processing : ' . $key );
                    info( 'patient_id: ' . @$patient->patient_id . ' weight_loss: ' . $weight_loss_temp . ', target_weight_loss: ' . $target_weight_loss_temp . '  weight loss %: ' . ( $target_weight_loss_temp ? ( ( $weight_loss_temp / $target_weight_loss_temp ) * 100 ) : 0 ) );
                }
            }
        }
        info('Total weight_loss: '.$weight_loss. ' Total target_weight_loss: '.$target_weight_loss);
        if ( $weight_loss && $target_weight_loss ) {
            $goal_completion_percentage = ( $weight_loss / $target_weight_loss ) * 100;
        }
        else if($weight_loss == 0 && $target_weight_loss > 0 ){
            $goal_completion_percentage = 0;
        }
        else {
            $goal_completion_percentage = $no_data_message;
        }

        if ( is_numeric($goal_completion_percentage) && $goal_completion_percentage > 100 ) {
            $goal_completion_percentage = 100;
        }
        $group_weight_loss->total_weight_loss          = format_weight($weight_loss);
        $group_weight_loss->goal_completion_percentage = is_numeric($goal_completion_percentage) ? format_weight($goal_completion_percentage) : $goal_completion_percentage;

        info(json_encode($group_weight_loss));
        return $group_weight_loss;
    }



}

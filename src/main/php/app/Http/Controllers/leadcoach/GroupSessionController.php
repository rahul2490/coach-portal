<?php

namespace HealthSlatePortal\Http\Controllers\leadcoach;
use Carbon\Carbon;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use HealthSlatePortal\Models\GroupDetailModel;
use HealthSlatePortal\Models\GroupSessionModel;
use HealthSlatePortal\Models\Eloquent\GroupSession;
use HealthSlatePortal\Models\Eloquent\Patient;
use HealthSlatePortal\Models\Eloquent\SessionAttendance;
use HealthSlatePortal\Models\Eloquent\MiscLog;
use HealthSlatePortal\Models\Eloquent\LogModel;
use HealthSlatePortal\Models\CurriculumServiceProvider;
use Illuminate\Support\Facades\Log;
use Validator;
use Session;
use DB;
class GroupSessionController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $group_model;
    protected $group_member;
    protected $dataTableService;
    protected $group_detail;
    protected $curriculumServiceProvider;
    protected $user;
    protected $data = array();

    function __construct(GroupSessionModel $group_session_model,FacilityModel $facility_model, MemberModel $member_model,
                          Facility $facility, CoacheModel $coach_model, GroupModel $group_model,
                          DataTableServiceProvider $dataTableService, User $user, GroupMember $group_member,CurriculumServiceProvider $curriculumServiceProvider, GroupDetailModel $group_detail) {
        $this->facility_model   = $facility_model;
        $this->group_session_model   = $group_session_model;
        $this->coach_model      = $coach_model;
        $this->member_model     = $member_model;
        $this->group_model      = $group_model;
        $this->group_detail     = $group_detail;
        $this->group_member     = $group_member;
        $this->facility         = $facility;
        $this->user             = $user;
        $this->dataTableService = $dataTableService;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->group_db         = env('GROUP_DB_DATABASE');
        $this->active_user_role     = session('userRole');
        $this->active_facility_id   = session('active_facility');
        $this->data['common_js']= array('group-session');
    }


    /**
     * @param
     * @return Load view for Group Session Page
     */
	public function index($group_id) {
        $group_detail = $this->group_model->get_group_detail($group_id);

        if(empty($group_detail->group_id))
            return redirect()->intended('/'.$this->active_user_role.'/group');
        if($group_detail->facility_id != $this->active_facility_id)
            return redirect()->intended('/'.$this->active_user_role.'/group');
        if($group_detail->is_person == 0)
            abort(403, 'Unauthorized action.');

        $group_member_list  = collect($this->group_model->get_group_member_list($group_id, $this->active_facility_id));
        $patient_ids        = $group_member_list->where('user_type', 'PATIENT')->pluck( 'patient_id' )->toArray();
        $lead_coach         = $group_member_list->where('user_id', $group_detail->lead_user_id)->first();
        if(count($lead_coach) == 0)
            abort(403, 'Unauthorized action.');

        info('Calling Curriculum Server for Section Title');
        $weight_lose="";
        if(count($patient_ids)>0) {
            $weight_details = $this->group_model->get_group_report_for_duration_weight_details($patient_ids);
        }else{
            $weight_details = array();
        }
        $weight_lose = getWeightLossByGroup($weight_details);
        //print_r($weight_lose);
        $curriculum = [(object) [ 'uuid' => $group_detail->group_id, 'curriculum_program_type_name' => $group_detail->curriculum_program_type_name],                            ];
        $session_list = $this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum);
        $group_session_detail = collect($this->group_model->get_group_session_list($group_id));
        $session_group_list=array();

        $session_date = [];
        $session_topic_id =   [];
        foreach ($group_session_detail as $value) {
            if ($value->status == 0) {
                $session_date[$value->group_session_id] = $value->start_date;
                $session_topic_id[$value->group_session_id] = $value->topic_id;

            }
        }
        ksort($session_date);
        ksort($session_topic_id);
        $session_title="";
        $session_date = reset($session_date);
        $timezone = '';
        if(isset($_COOKIE['user_timezone'])) {
            $timezone =  $_COOKIE['user_timezone'];
        }

        if($session_date!="")
            $session_date = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $session_date)->timezone($timezone)->format('m/d/Y h:i A') : Carbon::createFromFormat( 'Y-m-d H:i:s', $session_date)->format('m/d/Y h:i A');

        $session_topic_id = reset($session_topic_id);
        if($session_topic_id!="")
        {
            $session_title = collect($session_list)->where('section_uuid', $session_topic_id)->first();
            $session_title = $session_title->display_week.' : '.$session_title->sectionTitle;
        }

        if(!empty($session_list))
        {

            foreach ($session_list as $key => $value)
            {

                if(count($group_session_detail)>0)
                {

                     if($group_session_detail->contains('topic_id',$value->section_uuid)) {
                         continue;
                     }
                }
                $session_group_list[$value->section_uuid] = $value->display_week.'. '.$value->sectionTitle;
            }
        }

        $this->data['group_detail'] = $group_detail;
        $this->data['group_id'] = $group_id;
        $this->data['coach_detail'] = $lead_coach;
        $this->data['session_group_list'] = $session_group_list;
        $this->data['session_title'] = $session_title;
        $this->data['session_date'] = $session_date;
        $this->data['weight_lose']=  $weight_lose;


        return view( 'leadcoach.mygroup.group-session', $this->data);
	}



    /**
     * @param
     * @return Load view for group session member
     */
    public function groupSessionList()
    {

        $group_id  = request('group_id', 0);

        $group_session_detail = $this->group_model->get_group_session_list($group_id);

        info('Calling Curriculum Server for Section Title');


        $value = @$group_session_detail[0]->curriculum_program_type_name;
        $session_list = '';
        if($value == 'DPP2' || $value == 'DPP')
        {
            $curriculum = [(object) [ 'uuid' => $group_session_detail[0]->group_id, 'curriculum_program_type_name' => $group_session_detail[0]->curriculum_program_type_name],                            ];
            $session_list = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
        }

        $timezone = '';
        if(isset($_COOKIE['user_timezone'])) {
            $timezone =  $_COOKIE['user_timezone'];
        }

        $data = collect();

        foreach ($group_session_detail as $value) {
            $current_session_title_name = '';
            if($session_list != '')
                $current_session_title_name = $session_list->where('section_uuid', $value->topic_id)->toArray();


            if(!empty($current_session_title_name))
            {
                $topic_title = '';
                foreach ($current_session_title_name as $session_name)
                {
                    $topic_title = 'Session ' . $session_name->display_week . ' : '. $session_name->sectionTitle;
                }
            }
            else
            {
                info('Sending Data To DataTableService');
                return $this->dataTableService->dataTableWithIds($data);

            }

            $start_date = '';
            if(!empty($value->start_date))
            {
                $start_date = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $value->start_date)->timezone($timezone)->format('m/d/Y h:i A') : Carbon::createFromFormat( 'Y-m-d H:i:s', $value->start_date)->format('m/d/Y h:i A');
            }
            $unlock_content_date = '';
            if(!empty($value->unlock_content_date))
            {
                $unlock_content_date = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $value->unlock_content_date)->timezone($timezone)->format('m/d/Y') : Carbon::createFromFormat( 'Y-m-d H:i:s', $value->unlock_content_date)->format('m/d/Y');
            }

            $row = [
                'hidden_id'             => '',
                'topic_title'           =>  '<a href="'.route('leadcoach.group-session-detail', ['group_session_id' => $value->group_session_id]).'">'.$topic_title.'</a>',
                'session_date'          =>  $start_date,
                'location'              =>  $value->location,
                'in_person'             =>  $value->inperson_count,
                'makeup'                =>  $value->makeup_count,
                'unlock_content_date'   =>  $unlock_content_date,
                'status'                =>  ($value->status>0  )?'Completed':'Pending',
                'Action'                =>  '<a  title="Edit Group Session" data-toggle="modal" data-id="'.$value->group_session_id.' " data-name =" '.$session_name->display_week .'.'. $session_name->sectionTitle.'" data-target="#modal_update-session" class="btn font-dark btn-sm btn-hide sbold "><i class="fa fa-edit"></i></a>',
            ];



            $data->push(array_values($row));


        }
        //print_r($data);
        info('Sending Data To DataTableService');
        return $this->dataTableService->dataTableWithIds($data);




    }




       public function  groupSessionStore()
       {

            $group_id  = request('group_id', 0);
            $curriculum_program_type  = request('curriculum_program_type', 0);
            $session_id  = request('session', 0);
            $date  = request('date', 0);
            if($date!=0)
            $date = date("Y-m-d", strtotime($date));
            $time  = request('time', 0);
            if($time!=0)
            $time = date('H:i:s', strtotime($time));
            $location  = request('location', 0);
            $unlock_content_date  = request('unlock_content_date', 0);
            if($unlock_content_date!=0)
            $unlock_content_date = date("Y-m-d", strtotime($unlock_content_date));
            $create_all_session  = request('create_all_session', 0);
            $session_uuid_array = collect();

           $timezone = '';
           if(isset($_COOKIE['user_timezone'])) {
               $timezone =  $_COOKIE['user_timezone'];
           }

           $start_date = $date.''.$time;
           $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $start_date,$timezone)->setTimezone('UTC');

           $unlock_content_date = $unlock_content_date.''.$time;
           $unlock_content_date = Carbon::createFromFormat('Y-m-d H:i:s', $unlock_content_date,$timezone)->setTimezone('UTC');

          $group_detail = $this->group_model->get_group_detail($group_id);

           info('Calling Curriculum Server for Section Title');

           $curriculum = [(object) [ 'uuid' => $group_detail->group_id, 'curriculum_program_type_name' => $group_detail->curriculum_program_type_name],                            ];
           $session_list = $this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum);
           $group_session_detail = collect($this->group_model->get_group_session_list($group_id));
           $session_group_list=array();
           $group_member_list  = collect($this->group_model->get_group_member_list($group_id, $this->active_facility_id));
           $group_member_id  = $group_member_list->where('user_type', 'PATIENT')->pluck( 'group_member_id' )->toArray();



           if(!empty($session_list))
           {

               $group_session =  new GroupSession;

               if($create_all_session==0)
               {
                   if (!$group_session_detail->contains('topic_id', $session_id))
                   {
                       $group_session->group_id = $group_id;
                       $group_session->topic_id = $session_id;
                       $group_session->start_date = $start_date;
                       $group_session->location = $location;
                       $group_session->timestamp = date("Y-m-d H:i:s");
                       $group_session->unlock_content_date = $unlock_content_date . ' ' . $time;
                       $group_session->save();
                       $session_uuid_array->push($session_id);
                       $group_session_id = $group_session->group_session_id;
                        if(!empty($group_member_id))
                        {
                          /*  print_r($group_member_id); exit;*/
                            foreach ($group_member_id as $group_member_id_value)
                           {
                             $group_session_attandance = new SessionAttendance;
                             $group_session_attandance->group_session_id= $group_session_id ;
                             $group_session_attandance->group_member_id= $group_member_id_value;
                             $group_session_attandance->timestamp = date("Y-m-d H:i:s");
                               $group_session_attandance->save();

                           }

                        }



                   }
               }
                else
               {

                    $loop_count= 1;
                    foreach ($session_list as $key => $value)
                   {

                         if (count($group_session_detail) > 0 &&  $group_session_detail->contains('topic_id', $value->section_uuid))
                         {
                           continue;
                         }
                         if ($create_all_session == 1)
                         {
                           $group_session =  new GroupSession;
                           $group_session->group_id = $group_id;
                           $group_session->topic_id = $value->section_uuid;
                           if($loop_count>1)
                           {
                               $start_date = strtotime("+7 days", strtotime($start_date));
                               $start_date  =  date("Y-m-d H:i:s", $start_date);
                               $group_session->start_date = $start_date;
                           }
                           else
                           {
                               $group_session->start_date = $start_date;
                           }
                           $group_session->location = $location;
                           $group_session->timestamp = date("Y-m-d H:i:s");
                           if($loop_count>1)
                           {
                                $unlock_content_date = strtotime("+1 days", strtotime($start_date));
                                $unlock_content_date =  date("Y-m-d H:i:s", $unlock_content_date);
                                $group_session->unlock_content_date = $unlock_content_date;
                           }
                           else
                           {
                                 $group_session->unlock_content_date = $unlock_content_date;
                           }

                           $group_session->save();
                           $session_uuid_array->push($value->section_uuid);
                           $group_session_id = $group_session->group_session_id;
                           if(!empty($group_member_id))
                           {
                             foreach ($group_member_id as $group_member_id_value)
                             {
                                 $group_session_attandance = new SessionAttendance;
                                 $group_session_attandance->group_session_id= $group_session_id ;
                                 $group_session_attandance->group_member_id= $group_member_id_value;
                                 $group_session_attandance->timestamp = date("Y-m-d H:i:s");
                                 $group_session_attandance->save();
                             }
                           }

                         }

                          $loop_count++;
                   }
               }


           }


               return $response = [
                   'success' => "new session added successfully",
                   'session_uuid_array' =>$session_uuid_array,
               ];
       }




   public function getGroupSessionDetail()
   {



       $group_session_id = request('group_session_id', 0);
       $timezone = '';
       if(isset($_COOKIE['user_timezone'])) {
           $timezone =  $_COOKIE['user_timezone'];
       }

       $group_session_detail = $this->group_model->get_group_session_detail_by_id($group_session_id);
       if(!empty($group_session_detail)) {
           info('Calling Curriculum Server for Section Title');
           $time = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s',$group_session_detail->start_date)->timezone($timezone)->format('h:i A') : Carbon::createFromFormat( 'Y-m-d H:i:s', $group_session_detail->start_date)->format('h:i A');
           $start_date = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s',$group_session_detail->start_date)->timezone($timezone)->format('m/d/Y') : Carbon::createFromFormat( 'Y-m-d H:i:s', $group_session_detail->start_date)->format('m/d/Y');
           $unlock_content_date = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s',$group_session_detail->unlock_content_date)->timezone($timezone)->format('m/d/Y') : Carbon::createFromFormat( 'Y-m-d H:i:s', $group_session_detail->unlock_content_date)->format('m/d/Y');

           $row = [

               'session' => '<option  value="' . $group_session_detail->topic_id . '"></option>',
               'start_date' => $start_date,
               'time' => $time,
               'location' => $group_session_detail->location,
               'unlock_content_date' => $unlock_content_date,


           ];


           return $response = [
               'success' => "new session added successfully",
               'group_session_detail' => $row,
           ];
       }

   }



   public function UpdateGroupSessionDetail()
   {

       $group_session_id  = request('group_session_id', 0);
       $date  = request('date', 0);
       if($date!=0)
       $date = date("Y-m-d", strtotime($date));
       $time  = request('time', 0);
       if($time!=0)
       $time = date('H:i:s', strtotime($time));

       $location  = request('location', 0);
       $unlock_content_date  = request('unlock_content_date', 0);
       if($unlock_content_date!=0)
       $unlock_content_date = date("Y-m-d", strtotime($unlock_content_date));
       $timezone = '';
       if(isset($_COOKIE['user_timezone'])) {
           $timezone =  $_COOKIE['user_timezone'];
       }

       $start_date = $date.''.$time;
       $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $start_date,$timezone)->setTimezone('UTC');
       $unlock_content_date = $unlock_content_date.''.$time;
       $unlock_content_date = Carbon::createFromFormat('Y-m-d H:i:s', $unlock_content_date,$timezone)->setTimezone('UTC');
       $group_session_update = GroupSession::where('group_session_id', $group_session_id)->first();
       $group_session_update->start_date = $start_date;
       $group_session_update->location = $location;
       $group_session_update->timestamp = date("Y-m-d H:i:s");
       $group_session_update->unlock_content_date = $unlock_content_date;
       $group_session_update->update();
       return $response = [
           'success' => "Session updated successfully",

       ];



   }



    /**
     * @param
     * @return Load view for Member
     */
	public function group_session_detail($group_session_id)
    {
        $group_detail = $this->group_model->get_group_new_detail($group_session_id);

        if(empty($group_detail))
            return redirect()->intended('/'.$this->active_user_role.'/group');
        if($group_detail[0]->facility_id != $this->active_facility_id)
            return redirect()->intended('/'.$this->active_user_role.'/group');
        if($group_detail[0]->is_person == 0)
            return redirect()->intended('/'.$this->active_user_role.'/group');

        info('Calling Curriculum Server for Section Title');
        $curriculum = [(object) [ 'uuid' => $group_detail[0]->group_id, 'curriculum_program_type_name' => $group_detail[0]->curriculum_program_type_name],                            ];
        $session_list = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
        $group_session_detail = collect($this->group_model->get_group_session_list( $group_detail[0]->group_id));
        $prior_session_arr=[];
         foreach ($group_session_detail as $value)
         {
              if($value->group_session_id < $group_session_id)
                 $prior_session_arr[$value->group_session_id] =  $value->start_date;
         }
         krsort($prior_session_arr);
        $prior_session_date = reset($prior_session_arr);
        $session_start_date = $prior_session_date;
        $session_end_date = $group_detail[0]->start_date;
        if($session_start_date == "")
        {
            $session_start_date = Carbon::now();
            $session_end_date = Carbon::now();
        }

        /*for previous  group session id*/
        $previous_session_id = [];
        $array_group_session_id[] = $group_session_id;
        foreach ($group_session_detail as $value) {
            if ($group_session_id > $value->group_session_id)
                $previous_session_id[$value->group_session_id] = $value->group_session_id;

        }
        rsort($previous_session_id);
        $previous_session_id = reset($previous_session_id);
        if($previous_session_id!="")
            $array_group_session_id[] = $previous_session_id;

        /*   */
        $session_group_list = array();
        $group_member_list  = collect($group_detail);
        $patient_ids        = $group_member_list->pluck( 'patient_id' )->toArray();
        //print_r($patient_ids);
        $lead_coach         = $group_member_list->where('lead_user_id', $group_member_list[0]->lead_user_id)->first();
        $weight_lose="";
        if(count($patient_ids)>0) {
           $weight_details = $this->group_model->get_group_report_for_duration_weight_details($patient_ids);
        }else{
            $weight_details = array();
        }
       $weight_lose = getWeightLossByGroup($weight_details);


        if(count($lead_coach) == 0)
          abort(403, 'Unauthorized action.');

        $member_details = '';
        $count_for_unlock_content = 0;
        if(!empty($patient_ids))
        {

            $member_details = $this->apiGroupParticipationReportInperson($patient_ids,$group_session_id,$array_group_session_id,$session_start_date,$session_end_date);
            // for unlock content
            foreach($member_details as $value) {
                if(strtotime(date("Y-m-d H:i:s"))< strtotime($value['unlock_content_date']))
                {
                    $count_for_unlock_content++;
                }

            }

        }
        $current_session_title_name = '';
        if($session_list != '')
            $current_session_title_name = $session_list->where('section_uuid', $group_detail[0]->topic_id)->toArray();

        $topic_title = '';
        if(!empty($current_session_title_name))
        {

            foreach ($current_session_title_name as $session_name)
            {
                $topic_title = 'Session ' . $session_name->display_week . ' : '. $session_name->sectionTitle;
            }
        }

        $unlock_content_for_all=1;
        if(strtotime(date("Y-m-d H:i:s"))>strtotime($group_detail[0]->unlock_content_date))
            $unlock_content_for_all=0;

        $timezone = '';
        if(isset($_COOKIE['user_timezone'])) {
            $timezone =  $_COOKIE['user_timezone'];
        }
         $unlock_content_date ="";
         $start_date ="";
         if($group_detail[0]->unlock_content_date!="")
         $unlock_content_date = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $group_detail[0]->unlock_content_date)->timezone($timezone)->format('m/d/Y h:i A') : Carbon::createFromFormat( 'Y-m-d H:i:s', $group_detail[0]->unlock_content_date)->format('m/d/Y h:i A');

         if($group_detail[0]->start_date!="")
         $start_date = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $group_detail[0]->start_date)->timezone($timezone)->format('m/d/Y h:i A') : Carbon::createFromFormat( 'Y-m-d H:i:s', $group_detail[0]->start_date)->format('m/d/Y h:i A');

         if($prior_session_date!="")
         $prior_session_date = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $prior_session_date)->timezone($timezone)->format('m/d/Y h:i A') : Carbon::createFromFormat( 'Y-m-d H:i:s', $prior_session_date)->format('m/d/Y h:i A');

        $this->data['prior_session_date'] = $prior_session_date;
        $this->data['unlock_content_date'] = $unlock_content_date;
        $this->data['start_date'] = $start_date;
        $this->data['group_detail'] = $group_detail;
        $this->data['group_session_id'] = $group_session_id;
        $this->data['group_id'] = $group_detail[0]->group_id;
        $this->data['coach_detail'] = $lead_coach;
        $this->data['member_group_detail'] = $member_details;
        $this->data['topic_title'] =  $topic_title;
        $this->data['unlock_content_for_all']= $unlock_content_for_all;
        $this->data['count_for_unlock_content']= $count_for_unlock_content;
        $this->data['weight_lose']=  $weight_lose;


        return view( 'leadcoach.mygroup.group-session-detail', $this->data);
    }




     public function groupSessionAttendanceList()
     {
         $group_session_id = request('group_session_id',0);
         $group_detail = $this->group_model->get_group_new_detail($group_session_id);
         if(empty($group_detail))
             abort(403, 'Unauthorized action.');
         if($group_detail[0]->is_person == 0)
             abort(403, 'Unauthorized action.');
         info('Calling Curriculum Server for Section Title');
         $curriculum = [(object) [ 'uuid' => $group_detail[0]->group_id, 'curriculum_program_type_name' => $group_detail[0]->curriculum_program_type_name],                            ];
         $session_list = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
         $group_session_detail = collect($this->group_model->get_group_session_list( $group_detail[0]->group_id));

         $prior_session_arr=[];
         foreach ($group_session_detail as $value)
         {
             if($value->group_session_id < $group_session_id)
                 $prior_session_arr[$value->group_session_id] =  $value->start_date;
         }
         krsort($prior_session_arr);
         $prior_session_date = reset($prior_session_arr);
         $session_start_date = $prior_session_date;
         $session_end_date = $group_detail[0]->start_date;
         if($session_start_date == "")
         {
             $session_start_date = Carbon::now();
             $session_end_date = Carbon::now();
         }


         $session_group_list=array();
         $group_member_list  = collect($group_detail);
         $patient_ids        = $group_member_list->pluck( 'patient_id' )->toArray();
         $lead_coach         = $group_member_list->where('lead_user_id', $group_member_list[0]->lead_user_id)->first();
         if(count($lead_coach) == 0)
             abort(403, 'Unauthorized action.');
         $data = collect();
         $member_details = '';
         /*for previous  group session id*/
         $previous_session_id = [];
         $array_group_session_id[] = $group_session_id;


         foreach ($group_session_detail as $value) {
             if ($group_session_id > $value->group_session_id)
             $previous_session_id[$value->group_session_id] = $value->group_session_id;

         }
         rsort($previous_session_id);
         $previous_session_id = reset($previous_session_id);
          if($previous_session_id!="")
              $array_group_session_id[] = $previous_session_id;

         /*   */

         if(!empty($patient_ids))
         {
             //$member_details = $this->apiGroupParticipationReport($patient_ids,$group_session_id);

             $member_details = $this->apiGroupParticipationReportInperson($patient_ids,$group_session_id,$array_group_session_id,$session_start_date,$session_end_date);

             foreach($member_details as $value) {

                 $this_week_weight = '<p>This session';
                 if (isset($value['thisWeekWeight'])) {
                     $this_week_weight .= '<span class="pull-right sbold font-dark">' . sprintf("%4.1f", $value["thisWeekWeight"]) . '<small> lbs </small> </span>';
                 }
                 $this_week_weight .= '</p>';
                 // for prior weight
                 $prior_weight = '<p>Prior weight';
                 if (isset($value['priorWeight'])) {
                     $prior_weight .= '<span class="pull-right sbold">' . sprintf("%4.1f", $value['priorWeight']) . '<small>lbs</small></span>';
                 }
                 $prior_weight .= '</p>';
                 //for change since prior
                 $change_since_prior_weight = '<p>Change since prior <span class="pull-right sbold">';
                 if (isset($value['changeSincePriorWeight'])) {
                     if ($value['changeSincePriorWeight'] > 0) {
                         $change_since_prior_weight .= '<span class="glyphicon glyphicon-circle-arrow-up font-red-sunglo "></span>';
                     } else {
                         $change_since_prior_weight .= '<span class="glyphicon glyphicon-circle-arrow-down font-green-jungle"></span>';
                     }
                     $change_since_prior_weight .= sprintf("%4.1f", abs($value["changeSincePriorWeight"])) . '<small>lbs</small>';
                 }


                 $change_since_prior_weight .= '</span> </p>';
                 //for starting wieght
                 $starting_weight = '<p>Starting weight';
                 if (isset($value["startingWeight"])) {
                     $starting_weight .= '<span class="pull-right sbold">' . sprintf("%4.1f", $value["startingWeight"]) . '<small>lbs</small></span>';
                 }
                 $starting_weight .= '</p>';
                 //for change since start
                 $change_since_start = '<p>Change since start<span class="pull-right sbold">';
                 if (isset($value['changeSinceStartWeight'])) {
                     if ($value['changeSinceStartWeight'] > 0) {
                         $change_since_start .= '<span class="glyphicon glyphicon-circle-arrow-up font-red-sunglo"></span>';
                     } else {
                         $change_since_start .= '<span class="glyphicon glyphicon-circle-arrow-down font-green-jungle"></span>';
                     }
                     $change_since_start .= sprintf("%4.1f", abs($value["changeSinceStartWeight"])) . ' <small>lbs</small>';
                 }
                 if ($value['changeSinceStartWeightPercent']) {
                     $change_since_start .= "," . sprintf("%4.1f", abs($value['changeSinceStartWeightPercent'])) . '<small> % </small>';
                 }
                 $change_since_start .= '</span></p>';
                 //for goal achieved
                 $goal_achieved = '<p>Goal Achieved';
                 if (isset($value['goalAcheived'])) {
                     $goal_achieved .= '<span class="pull-right sbold">' . $value['goalAcheived'] . '<small>%</small></span>';
                 }
                 $goal_achieved .= '</p>';
                 //for this session
                 $this_session = '<p >This session';
                 if ($value["thisWeekActivityMinutes"] != "") {
                     $this_session .= '<span class="pull-right sbold">' . $value["thisWeekActivityMinutes"] . '<small>mins</small></span>';
                 }
                 $this_session .= '</p>';
                 //for prior session
                 $prior_session = '<p>Prior session';
                 if ($value["priorWeekActivityMinutes"] != "") {
                     $prior_session .= '<span class="pull-right sbold">' . $value["priorWeekActivityMinutes"] . '<small>mins</small></span>';
                 }
                 $prior_session .= '</p>';
                 //for change since prior
                 $change_since_prior = '<p>Change since prior';
                 if ($value["changeSincePriorActivity"] != "") {
                     $change_since_prior .= '<span class="pull-right sbold">';
                     if ($value["changeSincePriorActivity"] > 0) {
                         $change_since_prior .= '<span class="glyphicon glyphicon-circle-arrow-up font-red-sunglo"></span>';
                     } else {
                         $change_since_prior .= '<span class="glyphicon glyphicon-circle-arrow-down font-green-jungle"></span>';
                     }
                     $change_since_prior .= abs($value["changeSincePriorActivity"]) . '<small>mins</small></span>';
                 }
                 $change_since_prior .= '</p>';
                 //for since last session
                 $since_last_session = 'Since Last Session';
                 if ($value["logDaysCount"] != "" && $value["logsCount"] != "") {
                     $since_last_session .= '<p class="sbold">' . $value["logDaysCount"] . 'Days - ' . $value["logsCount"] . 'logs</p>';
                 }
                 //for module finished
                 $module_finished = "";


                 if ($value['attendance_type'] == "MakeUp") {
                     if(strtotime(date("Y-m-d H:i:s"))> strtotime($value['unlock_content_date']))
                     {
                         $module_finished .= '<p class ="text-danger"><b>Not Started </b></p>';
                         $module_finished .= '<p> Modules Finished';
                         if ($value['attendance_type'] == "MarkAsComplete" || $value['attendance_type'] == "MakeUp") {
                             $module_finished .= '<span class="pull-right sbold">';
                             if (isset($value["viewedContentCount"]) && isset($value["totalContentCount"])) {
                                 $module_finished .= $value["viewedContentCount"] . '/' . $value["totalContentCount"];
                             }
                             $module_finished .= '</span>';
                         }
                         $module_finished .= '</p>';
                     }
                     $module_finished .= '<p> Weight Recorded';
                     if ($value['weight_log_id'] != "")
                         $module_finished .= '<span class="pull-right sbold"> Yes </span>';
                     else
                         $module_finished .= '<span class="pull-right sbold">No</span>';

                     $module_finished .= '</p>';
                     if(strtotime(date("Y-m-d H:i:s"))> strtotime($value['unlock_content_date'])) {
                         $module_finished .= '<p><a href="#modal_mark-as-completed" data-session-attendance-id="' . $value["sessionAttendanceId"] . '" data-patient-id="' . $value["userTypeId"] . '" data-toggle="modal"> Mark as Completed </a></p>';
                     }

                 }

                 if ($value['attendance_type'] == "MarkAsComplete") {
                     $module_finished .= '<p>Marked Completed </p>';
                     $module_finished .= '<p> Modules Finished </p>';
                 }


                  $attendance = "";
                  if($value['attendance_type']=="")
                  {
                    if($value['weight_log_id']!="")
                        $attendance.= "InPerson";
                  }

                  if($value['attendance_type']!="")
                  $attendance.= $value['attendance_type'];

                 $attendance.='<span class="pull-right sbold">';
                 if($value['attendance_type']=="MakeUp" || $value['attendance_type']=="InPerson") {
                     if(strtotime(date("Y-m-d H:i:s"))< strtotime($value['unlock_content_date']))
                     $attendance .= '<a href="#modal_add-edit-attendance" data-session-id="' . $group_session_id . '" data-patient-id="' . $value["userTypeId"] . '" data-session-attendance-id="' . $value["sessionAttendanceId"] . '" title="Unlock Content" class="btn font-dark btn-sm btn-iconhs sbold " data-toggle="modal" > <i class="fa fa-unlock" style="font-size:16px;" data-original-title="Unlock Content"  data-toggle="tooltip"></i></a>';
                 }
                 if($value['notes']!="") {
                     $attendance .= '<i  class="glyphicon glyphicon-list-alt" aria-hidden="true" title="'.$value["notes"].'"></i>';
                 }
                 $attendance.= '</span>';

     $row = [
             'hidden_id'         => '',
             'id_name'           =>  ' <div class="col-sm-8"><span class="memberid">'.$value['userTypeId'].'</span></div>
                                        <div class="col-sm-4">
                                            <img class="user-pic rounded" src="'.$value['patient_profile_image'].'" width="45" height="45">
                                        </div>
                                        <div class="col-sm-12">
                                            <a href="'.url(session('userRole') . '/dashboard') .'/'. $value['userTypeId'].'" class="member-name sbold">'.$value['full_name'] .'</a>
                                        </div>
                                        ',
             'wieght'            =>  $this_week_weight.''.$prior_weight.''.$change_since_prior_weight.''.$starting_weight.''.$change_since_start.''.$goal_achieved,
             'activity'          =>  $this_session.''.$prior_session.''.$change_since_prior,
             'logging'           =>  $since_last_session,
             'attendance'        =>  $attendance,
             'makeupstatus'      =>   $module_finished,
             'action'            =>  '<a href="#modal_add-edit-attendance" data-session-id="'.$group_session_id.'" data-patient-id="'.$value["userTypeId"].'" data-session-attendance-id="'.$value["sessionAttendanceId"].'" title="Add/Edit Attendance" class="btn font-dark btn-sm btn-iconhs sbold " data-toggle="modal" > <i class="iconhs-addclass_ol" data-original-title="Add/Edit Attendance"  data-toggle="tooltip"></i></a>
                                                    <a href="#modal_add-edit-weight"  class="btn font-dark btn-sm btn-iconhs sbold " data-toggle="modal" data-session-attendance-id="'.$value["sessionAttendanceId"].'" title="Weight" data-patient-id="'.$value["userTypeId"].'" > <i class="iconhs-weight" data-original-title="Add/Edit Weight"  data-toggle="tooltip"></i></a>
                                                    <a href="#modal_add-edit-activity"  class="btn font-dark btn-sm btn-iconhs sbold " data-toggle="modal" data-patient-id="'.$value["userTypeId"].'"  data-session-attendance-id="'.$value["sessionAttendanceId"].'" title="Activity"> <i class="iconhs-shoe" data-original-title="Add/Edit Activity"  data-toggle="tooltip"></i></a>',
         ];



         $data->push(array_values($row));


             }

         }


    //print_r($data);
info('Sending Data To DataTableService');
return $this->dataTableService->dataTableWithIds($data);





}



    /**
     * it's for online person
     *
     * @return array
     */
    public function apiGroupParticipationReport($patient_ids,$group_session_id=0){

        info( 'Patient Ids are:', $patient_ids );
        $now = Carbon::now();
        info( 'DateTime Today is: ' . $now );
        if($now->isSunday()){
            $last_sunday = $now->copy()->startOfDay();
        }else{
            $last_sunday = new Carbon('last sunday');
        }

        $start = $last_sunday;
        $end   = $now;
        info( 'Processing Group report for: ' . $start . ' - ' . $end );

        $last_week_start = $start->copy()->subWeeks(1);
        $last_week_end = $start;

        info( 'Prior week will be: ' . $last_week_start . ' - ' . $last_week_end );
        $result = [];

        $patients = $this->group_detail->get_group_report_for_duration( $patient_ids, $start, $end, $last_week_start, $last_week_end,$group_session_id );

        $patient_weight = $this->group_detail->get_group_report_for_duration_weight_details( $patient_ids, $start, $end, $last_week_start, $last_week_end );


        if(!empty($patients)) {
            foreach ($patients as $p_key => $p_row) {
                $p_current_weight    		= $this->in_multiarray($p_row->patient_id, $patient_weight,"current_weight");
                $p_prior_week_weight 		= $this->in_multiarray($p_row->patient_id, $patient_weight,"prior_week_weight");
                $p_row->current_weight 		=  isset($p_current_weight) ? $p_current_weight : null;
                $p_row->prior_week_weight 	=  isset($p_prior_week_weight) ? $p_prior_week_weight : null;
            }
        }

        $this_week_activity_minutes  = collect( $this->group_detail->get_patients_synced_minutes( $patient_ids, $start, $end ) );

        $prior_week_activity_minutes = collect( $this->group_detail->get_patients_synced_minutes( $patient_ids, $last_week_start, $last_week_end ) );

        // week number has starting index 0 on curriculum server
        $patients = $this->curriculumServiceProvider->getUnitsCompletedInSection($patients);

        $patients = $this->curriculumServiceProvider->getSectionListByPatientType($patients);

        $group_weight_loss = $this->getWeightLossByGroup($patients, null);

        foreach ( $patients as $record ) {


            $is_week_one = ( $record->member_completed_week == 1 ) ? true : false;

            $goal = $this->getWeightLossByGroup([$record], null);

            $minutes = floatval( $record->manual_activity_minutes );
            $prior_week_minutes = intval( $record->prior_week_manual_activity_minutes );

            $this_week = $this_week_activity_minutes->where( 'patient_id', $record->patient_id )->first();
            if ( ! empty( $this_week ) ) {
                $minutes = $minutes + floatval( $this_week->total_activity_minutes_synced );
            }
            $last_week = $prior_week_activity_minutes->where( 'patient_id', $record->patient_id )->first();
            if ( ! empty( $last_week ) ) {
                $prior_week_minutes = $prior_week_minutes + floatval( $last_week->total_activity_minutes_synced );
            }
            // for week 1, starting_weight will be prior weight
            $prior_weight = $is_week_one ? $record->starting_weight : ( is_null( $record->prior_week_weight ) ? $record->prior_week_weight : floatval( $record->prior_week_weight ) );

            $goal_achieved = null;
            $change_since_start_weight = null;
            $change_since_start_weight_percent = null;
            // consider This week weight
            // if not present then fall back to prior weight
            if(!is_null($record->current_weight) ){
                $goal_achieved = round($goal->goal_completion_percentage);
                $change_since_start_weight = floatval($record->current_weight) - floatval($record->starting_weight);
                $change_since_start_weight_percent = ( ! empty( $record->current_weight ) ) ? ( ( $change_since_start_weight / $record->current_weight ) * 100 ) : $change_since_start_weight_percent;
            }else if(!empty($prior_weight)){
                $data_set = (object) [ 'starting_weight' => $record->starting_weight, 'current_weight' => $prior_weight, 'target_weight' => $record->target_weight];
                $prior_goal = $this->getWeightLossByGroup([$data_set], null);
                $goal_achieved = round($prior_goal->goal_completion_percentage);
                // for week 1, change since starting should be null, if current weight is not present.
                if(!$is_week_one){
                    $change_since_start_weight         = ( floatval( $prior_weight ) - floatval( $record->starting_weight ) );
                    $change_since_start_weight_percent = ( ! empty( $prior_weight ) ) ? ( ( $change_since_start_weight / floatval( $prior_weight ) ) * 100 ) : $change_since_start_weight_percent;
                }
            }

            $change_since_prior_weight   = null;
            $change_since_prior_activity = null;
            if ( ! empty( $record->current_weight ) && ! empty( $prior_weight ) ) {
                $change_since_prior_weight = $record->current_weight - $prior_weight;
            }
            if ( ! empty( $minutes ) && ! empty( $prior_week_minutes ) ) {
                $change_since_prior_activity = $minutes - $prior_week_minutes;
            }

            $patient_profile_image = env('APP_BASE_URL').'img/default-user.png';
            if($record->patient_profile_image != '')
            {
                $patient_profile_image = env('PROFILE_IMAGE_BASE_URL') . $record->patient_profile_image;
            }
            info( 'Patient Ids are:'.$record->patient_id.' Sattendance id '.$record->session_attendance_id);
            $result[] = [
                'userTypeId'                    => $record->patient_id,
                'sessionAttendanceId'           => $record->session_attendance_id,
                'notes'                         => $record->notes,
                'attendance_type'               => $record->attendance_type,
                'unlock_content_date'           => $record->unlock_content_date,
                'weight_log_id'                 => $record->weight_log_id,
                'full_name'                     => $record->full_name,
                'patient_profile_image'         => $patient_profile_image,
                'thisWeekWeight'                => is_null( $record->current_weight ) ? $record->current_weight : floatval( $record->current_weight ),
                'priorWeight'                   => $prior_weight,
                'startingWeight'                => $record->starting_weight,
                'targetWeight'                  => $record->target_weight,
                'goalAcheived'                  => $goal_achieved,
                'thisWeekActivityMinutes'       => $minutes,
                'priorWeekActivityMinutes'      => $is_week_one ? null : $prior_week_minutes,
                'changeSinceStartWeight'        => $change_since_start_weight,
                'changeSinceStartWeightPercent' => $change_since_start_weight_percent,
                'changeSincePriorWeight'        => $change_since_prior_weight,
                'changeSincePriorActivity'      => $change_since_prior_activity,
                'logDaysCount'                  => $record->days_logged,
                'logsCount'                     => $record->total_logs,
                'viewedContentCount'            => isset( $record->viewed_units_in_week ) ? ( $record->viewed_units_in_week ) : 0,
                'totalContentCount'             => isset( $record->units_in_week ) ? ( $record->units_in_week ) : 0,
                'programWeek'             		=> isset( $record->member_completed_week ) ? ( $record->member_completed_week + 1) : 1,
                'topicName'						=> isset( $record->sectionTitle ) ? ( $record->sectionTitle ) : null,
                'total_weight_loss'             => $group_weight_loss->total_weight_loss,
                'goal_completion_percentage'    => round($group_weight_loss->goal_completion_percentage),
            ];
        }
        return $result;
    }

    /**
     * it's for in person
     *
     * @return array
     */
    public function apiGroupParticipationReportInperson($patient_ids,$group_session_id,$array_group_session_id,$session_start_date,$session_end_date){

        info( 'Patient Ids are:', $patient_ids );
        $now = Carbon::now();
        info( 'DateTime Today is: ' . $now );
        if($now->isSunday()){
            $last_sunday = $now->copy()->startOfDay();
        }else{
            $last_sunday = new Carbon('last sunday');
        }

        $start = $last_sunday;
        $end   = $now;
        info( 'Processing Group report for: ' . $start . ' - ' . $end );

        $last_week_start = $start->copy()->subWeeks(1);
        $last_week_end = $start;

        info( 'Prior week will be: ' . $last_week_start . ' - ' . $last_week_end );

        $result = [];

        $patients = $this->group_detail->get_group_report_for_duration_for_inperson( $patient_ids, $start, $end, $last_week_start, $last_week_end, $group_session_id, $session_start_date, $session_end_date );


        //$patient_weight = $this->group_detail->get_group_report_for_duration_weight_details( $patient_ids, $start, $end, $last_week_start, $last_week_end );
        $patient_weight_minutes = $this->group_detail->get_group_report_for_duration_weight_details_for_inperson( $patient_ids, $group_session_id,$array_group_session_id);


        foreach ($patients as $patients_value)
        {
            $patients_value->this_session_weight = "";
            $patients_value->this_session_activity_minutes = "";
            $patients_value->priror_session_weight = "";
            $patients_value->prior_session_activity_minutes = "";
            $patients_value->current_weight =0;

         foreach($patient_weight_minutes as $patient_weight_minutes_value)
         {

             if($patients_value->group_session_id == $patient_weight_minutes_value->group_session_id  && $patients_value->patient_id == $patient_weight_minutes_value->patient_id)
             {
                 $patients_value->this_session_weight = $patient_weight_minutes_value->weight;
                 $patients_value->this_session_activity_minutes = $patient_weight_minutes_value->activity_minutes;
                 $patients_value->current_weight = $patient_weight_minutes_value->weight;


             }

             if($patients_value->group_session_id != $patient_weight_minutes_value->group_session_id  && $patients_value->patient_id == $patient_weight_minutes_value->patient_id)
             {
                 $patients_value->priror_session_weight = $patient_weight_minutes_value->weight;
                 $patients_value->prior_session_activity_minutes = $patient_weight_minutes_value->activity_minutes;
                 $patients_value->current_weight = $patient_weight_minutes_value->weight;


             }
         }


        }


        $group_weight_loss = $this->getWeightLossByGroup($patients, null);


        foreach ( $patients as $record ) {

            $this_session_actvity_minutes= null;
            $prior_session_activity_minutes = null;
            $change_since_prior_activity_minutes = null;
            $change_since_prior_weight =null;
            $goal_achieved = null;
            $change_since_start_weight = null;
            $change_since_start_weight_percent = null;
            $is_week_one = ( $record->member_completed_week == 1 ) ? true : false;
            $goal = $this->getWeightLossByGroup([$record], null);
            $this_session_actvity_minutes = $record->this_session_activity_minutes;
            $prior_session_activity_minutes = $record->prior_session_activity_minutes;
            if ( ! empty($this_session_actvity_minutes) && ! empty($prior_session_activity_minutes))
            $change_since_prior_activity_minutes = $this_session_actvity_minutes - $prior_session_activity_minutes;

            $this_session_weight = $record->this_session_weight;
            $prior_session_weight = count($array_group_session_id)==1 ? $record->starting_weight : $record->priror_session_weight ;
            if (!empty($this_session_weight) && ! empty($prior_session_weight))
                $change_since_prior_weight = $this_session_weight - $prior_session_weight;


            // consider This week weight
            // if not present then fall back to prior weight
            if(!is_null($record->current_weight) ){
                $goal_achieved = round($goal->goal_completion_percentage);
                $change_since_start_weight = floatval($record->current_weight) - floatval($record->starting_weight);
                $change_since_start_weight_percent = ( ! empty( $record->current_weight ) ) ? ( ( $change_since_start_weight / $record->current_weight ) * 100 ) : $change_since_start_weight_percent;
            }else if(!empty($prior_session_weight)){
                $data_set = (object) [ 'starting_weight' => $record->starting_weight, 'current_weight' => $prior_session_weight, 'target_weight' => $record->target_weight];
                $prior_goal = $this->getWeightLossByGroup([$data_set], null);
                $goal_achieved = round($prior_goal->goal_completion_percentage);
                // for week 1, change since starting should be null, if current weight is not present.
                if(count($array_group_session_id)>1){
                    $change_since_start_weight         = ( floatval( $prior_session_weight ) - floatval( $record->starting_weight ) );
                    $change_since_start_weight_percent = ( ! empty( $prior_session_weight ) ) ? ( ( $change_since_start_weight / floatval( $prior_session_weight ) ) * 100 ) : $change_since_start_weight_percent;
                }
            }


            $patient_profile_image = env('APP_BASE_URL').'img/default-user.png';
            if($record->patient_profile_image != '')
            {
                $patient_profile_image = env('PROFILE_IMAGE_BASE_URL') . $record->patient_profile_image;
            }
            info( 'Patient Ids are:'.$record->patient_id.' Sattendance id '.$record->session_attendance_id);
            $result[] = [
                'userTypeId'                    => $record->patient_id,
                'sessionAttendanceId'           => $record->session_attendance_id,
                'notes'                         => $record->notes,
                'attendance_type'               => $record->attendance_type,
                'unlock_content_date'           => $record->unlock_content_date,
                'weight_log_id'                 => $record->weight_log_id,
                'full_name'                     => $record->full_name,
                'patient_profile_image'         => $patient_profile_image,
                'thisWeekWeight'                => $this_session_weight,
                'priorWeight'                   => $prior_session_weight,
                'startingWeight'                => $record->starting_weight,
                'targetWeight'                  => $record->target_weight,
                'goalAcheived'                  => $goal_achieved,
                'thisWeekActivityMinutes'       => $this_session_actvity_minutes,
                'priorWeekActivityMinutes'      => $prior_session_activity_minutes,
                'changeSinceStartWeight'        => $change_since_start_weight,
                'changeSinceStartWeightPercent' => $change_since_start_weight_percent,
                'changeSincePriorWeight'        => $change_since_prior_weight,
                'changeSincePriorActivity'      => $change_since_prior_activity_minutes,
                'logDaysCount'                  => $record->days_logged,
                'logsCount'                     => $record->total_logs,
                'viewedContentCount'            => isset( $record->viewed_units_in_week ) ? ( $record->viewed_units_in_week ) : 0,
                'totalContentCount'             => isset( $record->units_in_week ) ? ( $record->units_in_week ) : 0,
                'programWeek'             		=> isset( $record->member_completed_week ) ? ( $record->member_completed_week + 1) : 1,
                'topicName'						=> isset( $record->sectionTitle ) ? ( $record->sectionTitle ) : null,
                'total_weight_loss'             => $group_weight_loss->total_weight_loss,
                'goal_completion_percentage'    => round($group_weight_loss->goal_completion_percentage),
            ];
        }



        return $result;
    }




    /**
     * @Desc: multidimensional arrays check key exist
     * @param
     *
     * @return array
     */
    function in_multiarray($elem,$patient_weight,$field)
    {
        foreach ($patient_weight as $p_key => $p_row) {
            if($p_row->patient_id == $elem)
                return $p_row->$field;
        }
        return null;
    }



    private function getWeightLossByGroup($patients, $no_data_message = 'Group target not set')
    {
        /*$patients = [
            (object) [ 'starting_weight' => 120, 'current_weight' => 110, 'target_weight' => 90],
            (object) [ 'starting_weight' => 130, 'current_weight' => 140, 'target_weight' => 100],
            (object) [ 'starting_weight' => 130, 'current_weight' => 130, 'target_weight' => 100],
            (object) [ 'starting_weight' => 150, 'current_weight' => 100, 'target_weight' => 120],
            (object) [ 'starting_weight' => 200, 'current_weight' => 150, 'target_weight' => 150],
            (object) [ 'starting_weight' => 150, 'current_weight' => 141, 'target_weight' => 100],
        ];*/



        $group_weight_loss          = (object) [ ];
        $weight_loss                = 0;
        $target_weight_loss         = 0;
        $goal_completion_percentage = 0;
        foreach ( $patients as $key => $patient ) {

            $starting_weight = floatval( $patient->starting_weight );
            $current_weight  = floatval( $patient->current_weight );
            $target_weight   = floatval( $patient->target_weight );

            if ( ! empty( $current_weight ) && $current_weight > 0 ) {
                // only +ve loss is counted towards group goal
                // ony till 100% is counted if done more than 100% loss.
                if($starting_weight >= $current_weight) {
                    if ( $current_weight >= $target_weight ) {
                        $weight_loss_temp = $starting_weight - $current_weight;

                    } else {
                        $weight_loss_temp = $starting_weight - $target_weight;

                    }
                    $weight_loss        = $weight_loss + $weight_loss_temp;
                    $target_weight_loss_temp = $starting_weight - $target_weight;
                    $target_weight_loss = $target_weight_loss + $target_weight_loss_temp;
                    info( 'Processing : ' . $key );
                    info( 'patient_id: ' . @$patient->patient_id . ' weight_loss: ' . $weight_loss_temp . ', target_weight_loss: ' . $target_weight_loss_temp . '  weight loss %: ' . ( $target_weight_loss_temp ? ( ( $weight_loss_temp / $target_weight_loss_temp ) * 100 ) : 0 ) );
                }
            }
        }
        info('Total weight_loss: '.$weight_loss. ' Total target_weight_loss: '.$target_weight_loss);
        if ( $weight_loss && $target_weight_loss ) {
            $goal_completion_percentage = ( $weight_loss / $target_weight_loss ) * 100;
        }
        else if($weight_loss == 0 && $target_weight_loss > 0 ){
            $goal_completion_percentage = 0;
        }
        else {
            $goal_completion_percentage = $no_data_message;
        }

        if ( is_numeric($goal_completion_percentage) && $goal_completion_percentage > 100 ) {
            $goal_completion_percentage = 100;
        }
        $group_weight_loss->total_weight_loss          = format_weight($weight_loss);
        $group_weight_loss->goal_completion_percentage = is_numeric($goal_completion_percentage) ? format_weight($goal_completion_percentage) : $goal_completion_percentage;

        info(json_encode($group_weight_loss));
        return $group_weight_loss;
    }



     public function getSessionAttendanceData()
     {
         $timezone = '';
         if(isset($_COOKIE['user_timezone']))
         {
             $timezone =  $_COOKIE['user_timezone'];
         }

         /*$group_id    = request('group_id', 0);
         $group_session_id  = request('group_session_id', 0);*/
         /*$patient = Patient::where('patient_id',$patient_id)->first();
         $user_id =$patient->user_id;
         $groupmember = GroupMember::where('group_id',$group_id)->where('user_id',$user_id)->first();
         $groupmember_id =$groupmember->group_member_id;*/
         $session_attendance_id  = request('session_attendance_id', 0);
         $patient_id  = request('patient_id', 0);
         $session_date = request('session_date', 0);
         $session_attendance = SessionAttendance::where('session_attendance_id',$session_attendance_id)->first();
         $unlock_content_date = '';
         $time ='';
         $total_activity_minutes ='';

         if($patient_id && $session_date!=0)
         {

             $end_date = Carbon::createFromFormat('Y-m-d H:i:s', $session_date, $timezone)->timezone('UTC');
             $end_date =  $end_date->copy()->endOfDay();
             $start_date = $end_date->copy()->startOfDay();
             $start_date = strtotime($start_date)*1000;
             $end_date = strtotime($end_date)*1000;
             $get_activity_data = $this->group_session_model->getActivityData($patient_id,$start_date, $end_date);
             $total_activity_minutes  = $get_activity_data->total_activity_minutes;
         }

          if($session_attendance->activity_minutes!="")
             $activity_minutes = $session_attendance->activity_minutes;
          elseif($total_activity_minutes!="")
              $activity_minutes = $total_activity_minutes;
          else
              $activity_minutes = "";


         if(!empty($session_attendance->unlock_content_date)) {
             $time = (!empty($timezone)) ? Carbon::createFromFormat('Y-m-d H:i:s', $session_attendance->unlock_content_date)->timezone($timezone)->format('h:i A') : Carbon::createFromFormat('Y-m-d H:i:s', $session_attendance->unlock_content_date)->format('h:i A');
         }
         if(!empty($session_attendance->unlock_content_date))
         {
             $unlock_content_date = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $session_attendance->unlock_content_date)->timezone($timezone)->format('m/d/Y') : Carbon::createFromFormat( 'Y-m-d H:i:s', $session_attendance->unlock_content_date)->format('m/d/Y');
         }
         info("Return  Session  Attendance data ");

         $row = [

             'session_attendance_id' => $session_attendance->session_attendance_id ,
             'group_session_id' => $session_attendance->group_session_id ,
             'attendance_type' => $session_attendance->attendance_type,
             'timestamp' => $session_attendance->timestamp,
             'activity_minutes' => $activity_minutes,
             'weight_log_id' => $session_attendance->weight_log_id,
             'notes' => $session_attendance->notes,
             'unlock_content_date' => $unlock_content_date,
             'is_paper_tracker' => $session_attendance->is_paper_tracker,
             'days_tracked' => $session_attendance->days_tracked,
              'time'   => $time,

         ];


         return $response = [
             'session_attendace' => $row,
             'success'=>true,
         ];


     }


   public function UpdateSessionAttendanceData()
   {

        /* sessoin attendance update code start here */

       $timezone = '';
       if(isset($_COOKIE['user_timezone'])) {
           $timezone =  $_COOKIE['user_timezone'];
       }
       $session_attendance_id    = request('session_attendance_id', 0);
       $attendance_type    = request('attendance_type', 0);
       $group_session_id    = request('group_session_id', 0);
       $days_logged = request('days_logged', 0);
       if($attendance_type=="MakeUp")
        $days_logged ="";
       $tracking_type    = request('tracking_type', 0);
       if($attendance_type=="MakeUp")
         $tracking_type = 0;


       $time  = request('time', 0);
       if($time!=0)
       $time = date('H:i:s', strtotime($time));

       $unlock_content_date  = request('unlock_content_date', 0);
       if($unlock_content_date!=0)
       $unlock_content_date = date("Y-m-d", strtotime($unlock_content_date));
       $unlock_content_date = $unlock_content_date.''.$time;
       $unlock_content_date = Carbon::createFromFormat('Y-m-d H:i:s', $unlock_content_date,$timezone)->setTimezone('UTC');
       $notes    = request('notes', 0);
       $session_attendance = SessionAttendance::where('session_attendance_id', $session_attendance_id)->first();
       $session_attendance->attendance_type = $attendance_type;
       $session_attendance->is_paper_tracker = $tracking_type;
       $session_attendance->timestamp = date("Y-m-d H:i:s");
       $session_attendance->unlock_content_date = $unlock_content_date;
       $session_attendance->days_tracked = $days_logged;
       $session_attendance->notes = $notes;
       $session_attendance->update();

       /* sessoin attendance update code end  here */

       /* unlock content for all button dispaly and hide code start Here */
       $count_for_unlock_content = 0;
       $group_detail = $this->group_model->get_group_new_detail($group_session_id);
       $group_session_detail = collect($this->group_model->get_group_session_list( $group_detail[0]->group_id));
       $prior_session_arr=[];
       foreach ($group_session_detail as $value)
       {
           if($value->group_session_id < $group_session_id)
               $prior_session_arr[$value->group_session_id] =  $value->start_date;
       }

       krsort($prior_session_arr);
       $prior_session_date = reset($prior_session_arr);
       $session_start_date = $prior_session_date;
       $session_end_date = $group_detail[0]->start_date;
       if($session_start_date == "")
       {
           $session_start_date = Carbon::now();
           $session_end_date = Carbon::now();
       }


       if(!empty($group_detail))
       {
           $group_member_list  = collect($group_detail);
           $patient_ids        = $group_member_list->pluck( 'patient_id' )->toArray();

           /*for previous group session id code start here */
           $previous_session_id = [];
           $array_group_session_id[] = $group_session_id;
           foreach ($group_session_detail as $value) {
               if ($group_session_id > $value->group_session_id)
                   $previous_session_id[$value->group_session_id] = $value->group_session_id;
           }
           rsort($previous_session_id);
           $previous_session_id = reset($previous_session_id);
           if($previous_session_id!="")
               $array_group_session_id[] = $previous_session_id;

           /*for previous group session id code End here */


           if(!empty($patient_ids))
           {
               $member_details = $this->apiGroupParticipationReportInperson($patient_ids,$group_session_id,$array_group_session_id,$session_start_date,$session_end_date);
               if(!empty($member_details))
               {
                   foreach ($member_details as $value)
                   {
                       if (strtotime(date("Y-m-d H:i:s")) < strtotime($value['unlock_content_date']))
                       {
                           $count_for_unlock_content++;
                       }

                   }
               }

           }
       }

      /* unlock content for all button dispaly and hide  code end Here */


       return $response = [
           'success' => "Session updated successfully",
           'count_for_unlock_content'=>$count_for_unlock_content,
       ];



   }


   public function UpdateSessionAttendanceActivityData()
   {

       $session_attendance_id    = request('session_attendance_id', 0);
       $minutes    = request('minutes', 0);
       $session_attendance = SessionAttendance::where('session_attendance_id', $session_attendance_id)->first();
       $session_attendance->activity_minutes = $minutes;
       $session_attendance->update();
       return $response = [
           'success' => "Session activity minutes updated successfully",

       ];

   }


    public function getSessionWeightData()
    {


        $session_attendance_id  = request('session_attendance_id', 0);
        $session_attendance = SessionAttendance::where('session_attendance_id',$session_attendance_id)->first();
        info("Return  Session  Attendance data ");
        if($session_attendance->weight_log_id!="")
        {

            $misc_log = MiscLog::where('log_id', $session_attendance->weight_log_id)->first();
            $row = ['weight' =>$misc_log->weight,'notes'=>$session_attendance->notes];
        }

        else {
            $row = ['weight' => '','notes'=>$session_attendance->notes];
        }

        return $response = [
            'session_attendace' => $row,
            'success' => true,

        ];


    }

    public function updateSessionWeightData()
    {

        $session_attendance_id  = request('session_attendance_id', 0);
        $patient_id  = request('patient_id', 0);
        $weight  = request('weight', 0);
        $session_attendance = SessionAttendance::where('session_attendance_id',$session_attendance_id)->first();
        info("Return  Session  Weight data ");
        if($session_attendance->weight_log_id!="")
        {
            $misc_log = MiscLog::where('log_id', $session_attendance->weight_log_id)->first();
            $misc_log->weight = $weight;
            $misc_log->update();
            return $response = [
                'success' => "Session Weight updated successfully",

            ];

        }

        else {

                  $log = new LogModel;
                  $log->log_id = UUID();
                  $log->created_on = (time()*1000);
                  $log->log_type =  'Weight';
                  $log->patient_id =  $patient_id;
                  $log->log_date_time =  date("Y-m-d H:i:s");
                  $log->save();
                  $log_id =  $log->log_id;
                  $misclog = new MiscLog;
                  $misclog->log_id = $log_id;
                  $misclog->timestamp = (time()*1000);
                  $misclog->weight = $weight;
                  $misclog->save();
                  $session_attendance = SessionAttendance::where('session_attendance_id', $session_attendance_id)->first();
                  $session_attendance->weight_log_id = $log_id;
                  $session_attendance->update();

            return $response = [
                'success' => "Session Weight updated successfully",

            ];

        }





    }

    public function unlockContentGetData()
    {

        $group_session_id  = request('group_session_id', 0);

        $group_session = GroupSession::where('group_session_id',$group_session_id)->first();
        $unlock_content_date = '';
        $timezone = '';
        $time ='';

        if(isset($_COOKIE['user_timezone']))
        {
            $timezone =  $_COOKIE['user_timezone'];
        }

        if(!empty($group_session->unlock_content_date)) {
            $time = (!empty($timezone)) ? Carbon::createFromFormat('Y-m-d H:i:s', $group_session->unlock_content_date)->timezone($timezone)->format('h:i A') : Carbon::createFromFormat('Y-m-d H:i:s', $group_session->unlock_content_date)->format('h:i A');
        }
        if(!empty($group_session->unlock_content_date))
        {
            $unlock_content_date = ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $group_session->unlock_content_date)->timezone($timezone)->format('m/d/Y') : Carbon::createFromFormat( 'Y-m-d H:i:s', $group_session->unlock_content_date)->format('m/d/Y');
        }
        info("Return  Session  Attendance data ");

        $row = [
            'unlock_content_date' => $unlock_content_date,
            'time'   => $time,

        ];


        return $response = [
            'session_attendace' => $row,
            'success'=>true,
        ];

    }

    public function updateUnlockContentData()
    {

        $timezone = '';
        if(isset($_COOKIE['user_timezone'])) {
            $timezone =  $_COOKIE['user_timezone'];
        }
        $group_session_id = request('group_session_id', 0);
        $time  = request('time', 0);
        if($time!=0)
            $time = date('H:i:s', strtotime($time));

        $unlock_content_date  = request('unlock_content_date', 0);
        if($unlock_content_date!=0)
            $unlock_content_date = date("Y-m-d", strtotime($unlock_content_date));
        $unlock_content_date = $unlock_content_date.''.$time;
        $unlock_content_date = Carbon::createFromFormat('Y-m-d H:i:s', $unlock_content_date,$timezone)->setTimezone('UTC');
        $group_session = GroupSession::where('group_session_id',$group_session_id)->first();
        $group_session->unlock_content_date = $unlock_content_date;
        $group_session->update();
        return $response = [
            'success' => "Unlock Content updated successfully",

        ];





    }


    public function updateMarkAsCompleted()
    {

        $session_attendance_id  = request('session_attendance_id', 0);
        $patient_id  = request('patient_id', 0);
        $weight  = request('weight', 0);
        $notes  = request('notes', 0);
        $session_attendance = SessionAttendance::where('session_attendance_id',$session_attendance_id)->first();
        info("Return  Session  Weight data ");
        if($session_attendance->weight_log_id!="")
        {
            $misc_log = MiscLog::where('log_id', $session_attendance->weight_log_id)->first();
            $misc_log->weight = $weight;
            $misc_log->update();

            $session_attendance->notes =$notes;
            $session_attendance->attendance_type ="MarkAsComplete";
            $session_attendance->update();

            return $response = [
                'success' => "Mark as completed successfully",

            ];

        }

        else {

            $log = new LogModel;
            $log->log_id = UUID();
            $log->created_on = (time()*1000);
            $log->log_type =  'Weight';
            $log->patient_id =  $patient_id;
            $log->log_date_time =  date("Y-m-d H:i:s");
            $log->save();
            $log_id =  $log->log_id;
            $misclog = new MiscLog;
            $misclog->log_id = $log_id;
            $misclog->timestamp = (time()*1000);
            $misclog->weight = $weight;
            $misclog->save();
            $session_attendance = SessionAttendance::where('session_attendance_id', $session_attendance_id)->first();
            $session_attendance->weight_log_id = $log_id;
            $session_attendance->notes = $notes;
            $session_attendance->attendance_type ="MarkAsComplete";
            $session_attendance->update();

            return $response = [
                'success' => "Mark as completed successfully",

            ];

        }




    }


}// End of Class

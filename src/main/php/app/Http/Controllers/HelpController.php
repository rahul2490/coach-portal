<?php

namespace HealthSlatePortal\Http\Controllers;

use Illuminate\Http\Request;

use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;

class HelpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "Help Data";
    }

    /**
     * protal traing webinar function .
     *it's show webinar video
     */


    public function portalTrainingWebinar()
    {
        return view( 'help.portal_training_webinar');
    }

    /**
     * app help video function .
     *it's show app help ios video
     */

    public function appHelpVideoIos()
    {
        return view( 'help.app_help_video_ios');
         //return "app help video";
    }


    /**
     * app help video function .
     *it's show app help android video
     */

    public function appHelpVideoAndroid()
    {
        //
        return view( 'help.app_help_video_android');
        //return "app help video android";
    }


}

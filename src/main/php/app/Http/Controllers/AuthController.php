<?php

namespace HealthSlatePortal\Http\Controllers;

use Illuminate\Http\Request;

use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\AuthServiceProvider;
use Input;
use Log;
use Redirect;
use Session;
use Validator;
use Cookie;
use Storage;

class AuthController extends Controller {

	/**
	 *  common variable for passing data to view
	 * @var array
	 */
	protected $data = array(
		'page_title' => 'Sign in'
	);

    protected $facility_model;

	/**
	 * Default route to redirect after login
	 * @var string
	 */
	protected $defaultHome = '/home';

	/**
	 * @var AuthServiceProvider
	 */
	protected $authServiceProvider;

	/**
	 * @param AuthServiceProvider $authServiceProvider
	 */
	function __construct( AuthServiceProvider $authServiceProvider, FacilityModel $facility_model ) {
		$this->authServiceProvider = $authServiceProvider;
        $this->facility_model  = $facility_model;
	}

	/**
	 * index
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index() {
	    $this->data['cobrand_id']=get_cobrand_id();
		return view( 'auth.login', $this->data );
	}

	/**
	 * loginHandler
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function loginHandler() {
		$validator = Validator::make(
			Input::all(), array(
			'username' => 'required|min:4',
			'password' => 'required|min:4',
		) );
		if ( $validator->passes() ) {
			$user = $this->authServiceProvider->getAccessToken( Input::get( 'username' ), Input::get( 'password' ) );
			return $this->loginUser($user);
		}

		$messages      = $validator->errors();
		$errorMessages = [ ];
		foreach ( $messages->all( '<p>:message</p>' ) as $message ) {
			$errorMessages[] = $message;
		}

		return Redirect::route( 'login' )->withInput( Input::except( 'password' ) )->with( 'errorMsg', implode( '', $errorMessages ) );
	}

	/**
	 * login by user access token handler
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function loginByTokenHandler(){

		$validator = Validator::make(
			Input::all(), array(
			'token' => 'required',
			'expiresIn' => 'required|integer',
		) );

		if ( $validator->failed() ) {
			Log::info("Invalid Request for Login By Token: ".$validator->errors()->toJson());
			return redirect()->route('login');
		}

		$user = $this->authServiceProvider->getAdminUserByToken(request('token'), null, request('expiresIn'));
		return $this->loginUser($user);
	}
	/**
	 * logout
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function logout() {
		Log::info( 'Processing logout request.' );
		Session::flush();
                Cookie::queue(Cookie::forget('access_token'));
                Cookie::queue(Cookie::forget('expires_in'));

		return Redirect::route( 'login' );
	}

	/**
	 * login user by Token Info object
	 *
	 * @param $user
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	private function loginUser( $user ) {
		if ( isset( $user->accessToken ) ) {

            $user->cobrandId = get_cobrand_id();

            Session::put('user', $user);
            Cookie::queue(Cookie::make('access_token', $user->accessToken, $user->expiresIn));
            Cookie::queue(Cookie::make('expires_in', $user->expiresIn, $user->expiresIn));

            Log::info('Getting All Facility Of The Logged In User');
            $response = $this->facility_model->get_admin_facility_list($user->userName, Session('user')->cobrandId);
            $user_role = $this->facility_model->get_user_info($user->userName);
            if (count($response) > 0){
                if (isset($_COOKIE['active_facility_' . $user_role->user_id])) {
                    $active_facility = $_COOKIE['active_facility_' . $user_role->user_id];
                    foreach ($response as $res) {
                        if ($res->facility_id == $active_facility) {
                            $active_facility_id = $active_facility;
                            break;
                        } else {
                            $active_facility_id = $response[0]->facility_id;
                            //print_r($active_facility_id);
                        }
                    }
                } else {
                    $active_facility_id = $response[0]->facility_id;
                }
        }else{
                $active_facility_id='';
            }
           // print_r($active_facility_id);die;

            if(!empty($user_role))
            {
                Session::put( 'userId', $user_role->user_id);
                Session::put( 'full_name', $user_role->full_name);
            }
            if($user->userRole == "ROLE_PROVIDER")
            {
                $user->userRole = $user_role->user_type;
            }
            Log::info('Total Facility Of The Logged In User: ' . count($response));
            if(count($response) > 0)
            {
                Session::put( 'facility', $response );
                Session::put( 'active_facility', $active_facility_id );
            }
            else
            {
                Session::put( 'facility', $response );
                Session::put( 'active_facility', '' );
            }

            if($user->userRole == 'Coach')
            {
                //print_r($facility);die;
                Session::put( 'userRole', 'leadcoach');
                return redirect()->intended('/leadcoach/home');
            }
            else if($user->userRole == 'Food Coach')
            {

                Session::put( 'userRole', 'foodcoach');
                return redirect()->intended('/foodcoach/home');
            }
            else if($user->userRole == 'Tech Support')
            {
                Session::put( 'userRole', 'techsupport');
                return redirect()->intended('/techsupport/home');
            }
            else if($user->userRole == 'ROLE_FACILITY_ADMIN')
            {
                Session::put( 'userRole', 'facilityadmin');
                return redirect()->intended('/facilityadmin/home');
            }
            else if($user->userRole == 'ADMIN' || $user->userRole == 'ROLE_ADMIN')
            {
                Session::put( 'userRole', 'admin');
                return redirect()->intended('/admin/home');
            }
		}
		return redirect()->route( 'login' )->withInput( Input::except( 'password' ) )->with( 'errorMsg', @$user->errorDetail );
	}
}

<?php

namespace HealthSlatePortal\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CurriculumServiceProvider;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\PatientModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\Patient;
use HealthSlatePortal\Models\Eloquent\MisfitActivityLog;
use HealthSlatePortal\Models\Eloquent\ActivityLogSummary;
use Illuminate\Support\Facades\Log;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\PatientHideUnhide;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\Message;
use HealthSlatePortal\Models\Eloquent\SessionAttendance;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\LogBookModel;
use HealthSlatePortal\Models\DashboardSettingModel;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use Edujugon\PushNotification\PushNotification;
use HealthSlatePortal\Helpers\MailServiceHelper;
use DB;
use Input;
use Validator;


class LogBookController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $user;
    protected $dataTableService;
    protected $group_model;
    protected $group_member;
    protected $curriculumServiceProvider;
    protected $mailServiceHelper;
    protected $logbook_model;
    protected $patient_model;
    protected  $dashboard_setting_model;
    protected $data = array();

    function __construct(PatientModel $patient_model, FacilityModel $facility_model, MemberModel $member_model, Facility $facility, CoacheModel $coach_model,
                          DataTableServiceProvider $dataTableService, User $user, GroupModel $group_model,LogBookModel $logbook_model, GroupMember $group_member, CurriculumServiceProvider $curriculumServiceProvider,MailServiceHelper $mailServiceHelper,DashboardSettingModel $dashboard_setting_model) {
        $this->facility_model       = $facility_model;
        $this->logbook_model        = $logbook_model;
        $this->coach_model          = $coach_model;
        $this->member_model         = $member_model;
        $this->group_member         = $group_member;
        $this->group_model          = $group_model;
        $this->patient_model       = $patient_model;
        $this->dashboard_setting_model = $dashboard_setting_model;
        $this->facility             = $facility;
        $this->user                 = $user;
        $this->dataTableService     = $dataTableService;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->mailServiceHelper = $mailServiceHelper;
        $this->group_db             = env('GROUP_DB_DATABASE');
        $this->active_facility_id   = session('active_facility');
        $this->active_user_id       = session('userId');
        $this->active_user_role     = session('userRole');
        $this->data['highchart_js'] = array('highcharts','highcharts-3d','highcharts-more');
        $this->data['common_js']    = array('tagging/jquery-migrate-1.2.1', 'tagging/jquery.caretposition', 'tagging/jquery.sew','logbook','dashboard');


    }


    function index($patient_id ='')
    {
        $timezone = '';
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }
        $this->data['patient_id']=$patient_id;
        $patient_info = $this->patient_model->get_patient_info($patient_id, $this->active_facility_id);

        if(empty($patient_info->patient_id))
        {
            return redirect()->intended('/'.$this->active_user_role.'/members');
        }

        $this->data['patient_info'] = $patient_info;
        $this->data['group_link'] = "";
        $this->data['session'] = "" ;
        $this->data['total_post']='';

        if($patient_info->group_id !="" )
        {
            $group_id   = array($patient_info->group_id);
            $is_person  = $patient_info->is_person;

            $completed_week         = $patient_info->completed_week;
            $group_session_detail   = collect($this->group_model->get_group_current_session_name($group_id));

            $curriculum_for_dpp     = '';
            $curriculum_for_dpp2    = '';

            if ($is_person == 1) {
                // Calling Curriculam server for Section list for both type DPP and DPP2
                $curriculum_program_type_name = $group_session_detail->unique('curriculum_program_type_name')->pluck('curriculum_program_type_name' )->toArray();
                //print_r(($curriculum_program_type_name)); exit;
                if(count($curriculum_program_type_name) > 0)
                {
                    foreach ($curriculum_program_type_name as $value)
                    {
                        if($value == 'DPP2' || $value == 'DPP')
                        {
                            $curriculum = [
                                (object) [ 'uuid' => 'inperson', 'curriculum_program_type_name' => $value],
                            ];

                            if($value == 'DPP')
                                $curriculum_for_dpp  = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                            if($value == 'DPP2')
                                $curriculum_for_dpp2 = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                        }
                    }
                }
            }

            if($completed_week == 0)
                $completed_week = 1;
            $total_sessions  = array();
            if($is_person != 1)
                $group_link = route('leadcoach.group-detail', ['group_id' => $patient_info->group_id]);
            else
            {
                $group_link = route('leadcoach.group-session', ['group_id' => $patient_info->group_id]);
                $week_group = 0;

                $current_session_name = '';
                if(count($group_session_detail) > 0)
                {
                    $group_current_session = $group_session_detail->where('group_id', $patient_info->group_id)->last();
                    //print_r($group_current_session);
                    if(!empty($group_current_session->topic_id))
                    {
                        if($curriculum_for_dpp != '')
                        {
                            $total_sessions = $curriculum_for_dpp;
                            $current_session_name = $curriculum_for_dpp->where('section_uuid', $group_current_session->topic_id)->toArray();
                        }

                        if($curriculum_for_dpp2 != '')
                        {
                            $total_sessions = $curriculum_for_dpp2;
                            $current_session_name = $curriculum_for_dpp2->where('section_uuid', $group_current_session->topic_id)->toArray();
                        }


                        if(!empty($current_session_name))
                        {
                            foreach ($current_session_name as $val)
                                $week_group =  $val->display_week;
                        }
                    }

                }
            }

            $this->data['group_link']  = $group_link;
            if($is_person == 1)
                $this->data['session'] = $week_group."/".count($total_sessions) ;
            else
                $this->data['session'] = "NA" ;

            // Batch Count Start
            $read_count = GroupMember::where('group_id', $group_id)->where('user_id',$this->active_user_id )->first();
            if(!empty($read_count))
            {
                $post_read_time =  $read_count->post_read_time;
                $user_id= $this->active_user_id;
                $group_post = $this->group_model->get_post_read_count($group_id,$post_read_time,$user_id);
            }
            if(!empty($group_post)){
                $count_post=$group_post[0]->total_post;
                if($count_post==0){
                    $count_post='';
                }
            }
            else{
                $count_post='';
            }
            $this->data['total_post'] = $count_post;
            // Batch Count End
        }

        $this->data['profile_detail'] = $this->dashboard_setting_model->getProfileDetail($patient_id);

        $now = Carbon::now();
        if(!empty($timezone))
          $now = Carbon::createFromFormat('Y-m-d H:i:s', $now)->timezone($timezone);
        else
          $now = Carbon::createFromFormat('Y-m-d H:i:s', $now);

        $week_end      = $now->copy()->endOfDay();
        $week_start    = $now->copy()->subDays(6)->startOfDay();
        $month_end   = $now->copy()->startOfDay();
        $month_start   = $now->copy()->subMonth();

        $date_array = [
            'week_start'  =>  $week_start->format('m/d/Y'),
            'week_end'    =>  $week_end->format('m/d/Y'),
            'month_start'  => $month_start->format('m/d/Y'),
            'month_end'    => $month_end->format('m/d/Y'),

        ];

        $patient_data = Patient::where('patient_id',$patient_id)->first();
        $patient_type = $patient_data->diabetes_type_id;
        $user_id =  $patient_data->user_id;
        $user_data = User::where('user_id',$user_id)->first();
        $offset_key  = $user_data->offset_key;
        $this->data['user_timezone']  = $user_data->offset_key;
        $this->data['patient_type']= $patient_type;


         if(!empty($offset_key))
         {
             //$start = Carbon::createFromFormat('Y-m-d H:i:s', $week_start,$timezone)->setTimezone('UTC');
             $start = Carbon::createFromFormat('Y-m-d H:i:s', $week_start,$offset_key)->setTimezone('UTC');
             //$end = Carbon::createFromFormat('Y-m-d H:i:s', $week_end,$timezone)->setTimezone('UTC');
             $end = Carbon::createFromFormat('Y-m-d H:i:s', $week_end,$offset_key)->setTimezone('UTC');

         }
         else
         {
             $start = Carbon::createFromFormat('Y-m-d H:i:s', $week_start);
             $end = Carbon::createFromFormat('Y-m-d H:i:s', $week_end);

         }


        //echo  $start = $week_start;
        //echo  $end =    $week_end;

        $current_date = strtotime($start)*1000;
        $compare_time = strtotime($end)*1000;

        $output=collect();
        $data=collect();

        $log_book=collect();
        $meal_type ="All";
        $log_data = $this->logbook_model->get_logbook_for_patient($patient_id, $current_date, $compare_time,$meal_type, $offset_key);
        $data = $data->merge($log_data);
        $weight_data = $this->logbook_model->get_patient_weight_by_log($patient_id, $current_date, $compare_time, $offset_key);
        $data = $data->merge($weight_data);
        $log_activity_data = $this->logbook_model->get_logbook_for_patient_activity($patient_id, $current_date, $compare_time, $offset_key);
        $data = $data->merge($log_activity_data);
        $log_activity_minute = $this->logbook_model->get_logbook_for_patient_activity_minutes($patient_id, $current_date, $compare_time, $offset_key);
        $data = $data->merge($log_activity_minute);
        $log_activity_minute_log_sumarry  = $this->logbook_model->get_logbook_for_patient_activity_min_for_log_sumarry($patient_id, $current_date, $compare_time, $offset_key);
        $data = $data->merge($log_activity_minute_log_sumarry);
        if($patient_type==1)
        {
            $glucose_data = $this->logbook_model->get_logbook_for_glucose($patient_id, $current_date, $compare_time, $offset_key);
            $data = $data->merge($glucose_data);
            $medication_data = $this->logbook_model->get_logbook_for_medication($patient_id, $current_date, $compare_time, $offset_key);
            $data = $data->merge($medication_data);
        }

        $output=$data;
        $output=$output->sortByDesc('created_on');

        //print_r($output);

        $this->data['logbook_detail']= $output->groupBy('date');


        $this->data['date_array'] = $date_array;


        return view( 'common.log_book', $this->data );

   }

    public function log_book_by_filter()
    {
        if (request()->ajax()) {
            $patient_id = request('patient_id');
            $meal_type = request('meal_type');
            $timezone = '';
            if (isset($_COOKIE['user_timezone'])) {
                $timezone = $_COOKIE['user_timezone'];
            }

            $patient_data = Patient::where('patient_id',$patient_id)->first();
            $user_id =  $patient_data->user_id;
            $user_data = User::where('user_id',$user_id)->first();
            $user_timezone  = $user_data->offset_key;
            $offset_key  = $user_data->offset_key;



            $date = request('date');
            $date = explode(' - ', $date);
            $start = Carbon::createFromFormat('m/d/Y', $date[0])->setTime(0, 0, 0);
            $end = Carbon::createFromFormat('m/d/Y', $date[1])->setTime(23, 59, 59);

              if(!empty($offset_key)) {
                  $start = Carbon::createFromFormat('Y-m-d H:i:s', $start, $offset_key)->setTimezone('UTC');
                  $end = Carbon::createFromFormat('Y-m-d H:i:s', $end, $offset_key)->setTimezone('UTC');
              }

            $current_date = strtotime($start) * 1000;
            $compare_time = strtotime($end) * 1000;

            $this->data['log_book_data'] = $this->get_log_book($patient_id, $current_date, $compare_time, $meal_type);
           // print_r($this->data['log_book_data']); exit;

            $return_html = "";
            $return_html .= '<div class="col-sm-12 meal-log">';
            if(count($this->data['log_book_data'])>0) {
            foreach ($this->data['log_book_data'] as $detail => $logs) {
                $return_html .= '<div class="meal-title col-sm-12"><strong>' . $detail . '</strong></div>';
                foreach ($logs as $log) {
                    if (in_array($log->log_type, array('Lunch', 'Dinner', 'Breakfast', 'Snack'))) {
                        $log_data = $log->calories;
                        $type = " Cal";
                        $class = "fa fa-cutlery";
                        $div_class = "log-btn meal";
                    } else if (in_array($log->log_type, array('Weight'))) {
                        $log_data = $log->weight;
                        $type = " lbs";
                        $class = "fa fa-dashboard";
                        if(isset($log->serviceName) && $log->serviceName =="NokiaScale")
                            $class = "iconhs-nokia_scale";
                        elseif(isset($log->serviceName) && $log->serviceName =="HealthKit")
                            $class = "iconhs-healthkit_icon";
                        elseif( isset($log->serviceName) && $log->serviceName =="GoogleFit")
                            $class = "iconhs-googlefit_icon";
                        elseif( isset($log->serviceName) && $log->serviceName =="Manual")
                            $class = "iconhs-manual";
                        elseif( isset($log->serviceName) && $log->serviceName =="Class")
                            $class = "fa fa-dashboard";
                        elseif( isset($log->serviceName) && $log->serviceName =="Aria")
                            $class = "iconhs-weight_aria";
                        elseif( isset($log->serviceName) && $log->serviceName =="AvivaMeter")
                            $class = "iconhs-avivameter";
                        elseif( isset($log->serviceName) && $log->serviceName =="BodyTrace")
                            $class = "iconhs-bodytrace";
                           // $class = "fa fa-dashboard";
                           $div_class = "log-btn weight";
                    } else if (in_array($log->log_type, array('ActivityMinute','minutesFairlyActive','minutesVeryActive'))) {

                        if($log->log_type=="ActivityMinute") {
                            $log_data = $log->minutes_performed;
                            //$log->log_type = $log->activity_log_type;
                            $log->log_type = "Activity";
                        }
                        else {
                            $log_data = $log->steps;
                            //if($log->activity_type!="")
                            //$log->log_type = $log->activity_type;
                            $log->log_type = "Activity";

                        }
                        $type = " Min";
                        $class = "iconhs-shoe";
                        if(isset($log->serviceName) && $log->serviceName =="fitbit")
                            $class = "iconhs-fitbit";
                        elseif(isset($log->serviceName) && $log->serviceName =="HealthKit")
                            $class = "iconhs-healthkit_icon";
                        elseif( isset($log->serviceName) && $log->serviceName =="GoogleFit")
                            $class = "iconhs-googlefit_icon";
                        $div_class = "log-btn activity";
                        //$class = "iconhs-shoe";

                    }
                    else if (in_array($log->log_type, array('Glucose'))) {
                        $log_data = $log->glucose_level;
                        $type = " mg/dl";
                        $class = "iconhs-glucose";
                        $div_class = "log-btn activity";
                    }
                    else if (in_array($log->log_type, array('Medication'))) {
                        $log_data = $log->quantityTaken;
                        $type = " meds";
                        $class = "iconhs-medication";
                        $div_class = "log-btn activity";
                    }
                    else {
                        $log_data = $log->steps;
                        $type = " Steps";
                        $class = "iconhs-shoe";
                        $div_class = "log-btn activity";
                    }


                    $return_html .= '<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="btn btn-block btn-default ' . $div_class . '">';
                    if ($log->log_type != '') {
                        $return_html .= '<span class="badge badge-danger"> <i aria-hidden="true" class="' . $class . '"></i> </span>
            <h5>' . $log->log_type . '</h5>';
                    } else {
                        $return_html .= '<span class="badge badge-danger"> <i aria-hidden="true" class="' . $class . '"></i> </span>
                <h5>Activity</h5>';
                    }
                    if ($log_data != '') {
                        $return_html .= '<h6> <strong>' . $log_data . '</strong></h6>';
                    } else {
                        $return_html .= '<h6> <strong>0</strong></h6>';
                    }
                    $return_html .= '<strong><h5>' . $type . '</h5></strong>
            </div>
            <strong><p class="text-center"><small>' .$log->date_time. '</small></p></strong>
            </div>';
                }
            }
         }
        else
        {
            $return_html.='<div  style="padding:15px; background-color:#94A0B2; margin-top:15px; text-align:center;">
                                <span>No Logs Found.</span></div>';
        }

            $return_html.='</div>';

            return $return_html;




        }
    }




    public function get_log_book($patient_id, $current_date, $compare_time,$meal_type)
    {
        $patient_data = Patient::where('patient_id',$patient_id)->first();
        $patient_type = $patient_data->diabetes_type_id;
        $user_id =  $patient_data->user_id;
        $user_data = User::where('user_id',$user_id)->first();
        $offset_key  = $user_data->offset_key;

        $this->data['patient_id'] = $patient_id;
        $meal_name = array('Lunch','Dinner','Breakfast','Snack');
        $output=collect();
        $data=collect();
        $log_book=collect();
        if(in_array($meal_type,$meal_name)) {
            $log_data = $this->logbook_model->get_logbook_for_patient($patient_id, $current_date, $compare_time,$meal_type, $offset_key);
            $data = $data->merge($log_data);
        }else if($meal_type == 'Weight') {
            $weight_data = $this->logbook_model->get_patient_weight_by_log($patient_id, $current_date, $compare_time, $offset_key);
            $data = $data->merge($weight_data);
        }else if($meal_type == 'Steps'){
            $log_activity_data = $this->logbook_model->get_logbook_for_patient_activity($patient_id, $current_date, $compare_time, $offset_key);
            $data = $data->merge($log_activity_data);
        }else if($meal_type == 'ActivityMinute'){
            $log_activity_minute = $this->logbook_model->get_logbook_for_patient_activity_minutes($patient_id, $current_date, $compare_time, $offset_key);
            $data = $data->merge($log_activity_minute);
            $log_activity_minute_log_sumarry  = $this->logbook_model->get_logbook_for_patient_activity_min_for_log_sumarry($patient_id, $current_date, $compare_time, $offset_key);
            $data = $data->merge($log_activity_minute_log_sumarry);
        }else if($meal_type == 'Glucose'){
            $glucose_data = $this->logbook_model->get_logbook_for_glucose($patient_id, $current_date, $compare_time, $offset_key);
            $data = $data->merge($glucose_data);   }

        else if($meal_type == 'Medication'){
            $medication_data = $this->logbook_model->get_logbook_for_medication($patient_id, $current_date, $compare_time, $offset_key);
            $data = $data->merge($medication_data);

        }else{

            $meal_type = "All";
            $log_data = $this->logbook_model->get_logbook_for_patient($patient_id, $current_date, $compare_time,$meal_type, $offset_key);
            $data = $data->merge($log_data);
            $weight_data = $this->logbook_model->get_patient_weight_by_log($patient_id, $current_date, $compare_time, $offset_key);
            $data = $data->merge($weight_data);
            $log_activity_data = $this->logbook_model->get_logbook_for_patient_activity($patient_id, $current_date, $compare_time, $offset_key);
            $data = $data->merge($log_activity_data);
            $log_activity_minute = $this->logbook_model->get_logbook_for_patient_activity_minutes($patient_id, $current_date, $compare_time, $offset_key);
            $data = $data->merge($log_activity_minute);
            $log_activity_minute_log_sumarry  = $this->logbook_model->get_logbook_for_patient_activity_min_for_log_sumarry($patient_id, $current_date, $compare_time, $offset_key);
            $data = $data->merge($log_activity_minute_log_sumarry);
            if($patient_type==1)
            {
                $glucose_data = $this->logbook_model->get_logbook_for_glucose($patient_id, $current_date, $compare_time, $offset_key);
                $data = $data->merge($glucose_data);
                $medication_data = $this->logbook_model->get_logbook_for_medication($patient_id, $current_date, $compare_time, $offset_key);
                $data = $data->merge($medication_data);
            }



        }
        $output=$data;
        $output=$output->sortByDesc('created_on');

        $this->data['logbook_detail']= $output->groupBy('date');

        return  $this->data['logbook_detail'];

    }


    public function get_log_book_by_date_range_weekly(Request $request){
        if (request()->ajax()) {
            $timezone = '';
            if (isset($_COOKIE['user_timezone'])) {
                $timezone = $_COOKIE['user_timezone'];
            }
            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $clicked        =  $request->input('clicked');
            $meal_type        =  $request->input('meal_type');
            $date = explode(' - ', $date);
            $start = Carbon::createFromFormat('m/d/Y', $date[0])->setTime(0, 0, 0);
            $end = Carbon::createFromFormat('m/d/Y', $date[1])->setTime(23, 59, 59);
            if($clicked == 'right')
            {
                $daily_start  = $start->copy()->addDays(7);
                $daily_end    = $end->copy()->addDays(7);
            }
            else
            {
                $daily_start  = $start->copy()->subDays(7);
                $daily_end    = $end->copy()->subDays(7);
            }

            $patient_data = Patient::where('patient_id',$patient_id)->first();
            $user_id =  $patient_data->user_id;
            $user_data = User::where('user_id',$user_id)->first();
            $user_timezone  = $user_data->offset_key;
            $offset_key  = $user_data->offset_key;


            if(!empty($offset_key)) {
                $start = Carbon::createFromFormat('Y-m-d H:i:s', $daily_start, $offset_key)->setTimezone('UTC');
                $end = Carbon::createFromFormat('Y-m-d H:i:s', $daily_end, $offset_key)->setTimezone('UTC');
            }
             $current_date = strtotime($start)*1000;
             $compare_time = strtotime($end)*1000;

            $this->data['log_book_data'] = $this->get_log_book($patient_id, $current_date, $compare_time,$meal_type);


            $return_html="";


            $return_html.='<div class="col-sm-12 meal-log">';
            if(count($this->data['log_book_data'])>0) {
                foreach ($this->data['log_book_data'] as $detail => $logs) {
                    $return_html .= '<div class="meal-title col-sm-12"><strong>' . $detail . '</strong></div>';
                    foreach ($logs as $log) {
                        if (in_array($log->log_type, array('Lunch', 'Dinner', 'Breakfast', 'Snack'))) {
                            $log_data = $log->calories;
                            $type = " Cal";
                            $class = "fa fa-cutlery";
                            $div_class = "log-btn meal";
                        } else if (in_array($log->log_type, array('Weight'))) {
                            $log_data = $log->weight;
                            $type = " lbs";
                            $class = "fa fa-dashboard";
                            if(isset($log->serviceName) && $log->serviceName =="NokiaScale")
                                $class = "iconhs-nokia_scale";
                            elseif(isset($log->serviceName) && $log->serviceName =="HealthKit")
                                $class = "iconhs-healthkit_icon";
                            elseif( isset($log->serviceName) && $log->serviceName =="GoogleFit")
                                $class = "iconhs-googlefit_icon";
                            elseif( isset($log->serviceName) && $log->serviceName =="Manual")
                                $class = "iconhs-manual";
                            elseif( isset($log->serviceName) && $log->serviceName =="Class")
                                $class = "fa fa-dashboard";
                            elseif( isset($log->serviceName) && $log->serviceName =="Aria")
                                $class = "iconhs-weight_aria";
                            elseif( isset($log->serviceName) && $log->serviceName =="AvivaMeter")
                                $class = "iconhs-avivameter";
                            elseif( isset($log->serviceName) && $log->serviceName =="BodyTrace")
                                $class = "iconhs-bodytrace";
                           // $class = "fa fa-dashboard";
                            $div_class = "log-btn weight";
                        }
                        else if (in_array($log->log_type, array('ActivityMinute','minutesFairlyActive','minutesVeryActive'))) {

                            if($log->log_type=="ActivityMinute") {
                                $log_data = $log->minutes_performed;
                                 $log->log_type = "Activity";
                            }
                            else
                             {
                                $log_data = $log->steps;
                                $log->log_type = "Activity";

                             }
                            $type = " Min";
                            $class = "iconhs-shoe";

                            if(isset($log->serviceName) && $log->serviceName =="fitbit")
                             $class = "iconhs-fitbit";
                            elseif(isset($log->serviceName) && $log->serviceName =="HealthKit")
                             $class = "iconhs-healthkit_icon";
                            elseif( isset($log->serviceName) && $log->serviceName =="GoogleFit")
                              $class = "iconhs-googlefit_icon";
                              $div_class = "log-btn activity";
                             // $class = "iconhs-shoe";
                        }
                        else if (in_array($log->log_type, array('Glucose'))) {
                            $log_data = $log->glucose_level;
                            $type = " mg/dl";
                            $class = "iconhs-glucose";
                            $div_class = "log-btn activity";
                        }
                        else if (in_array($log->log_type, array('Medication'))) {
                            $log_data = $log->quantityTaken;
                            $type = " meds";
                            $class = "iconhs-medication";
                            $div_class = "log-btn activity";
                        }
                        else {
                            $log_data = $log->steps;
                            $type = " Steps";
                            $class = "iconhs-shoe";
                            $div_class = "log-btn activity";
                        }


                        $return_html .= '<div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
            <div class="btn btn-block btn-default ' . $div_class . '">';
                        if ($log->log_type != '') {
                            $return_html .= '<span class="badge badge-danger"> <i aria-hidden="true" class="' . $class . '"></i> </span>
            <h5>' . $log->log_type . '</h5>';
                        } else {
                            $return_html .= '<span class="badge badge-danger"> <i aria-hidden="true" class="' . $class . '"></i> </span>
                <h5>Activity</h5>';
                        }
                        if ($log_data != '') {
                            $return_html .= '<h6> <strong>' . $log_data . '</strong></h6>';
                        } else {
                            $return_html .= '<h6> <strong>0</strong></h6>';
                        }
                        $return_html .= '<strong><h5>' . $type . '</h5></strong>
            </div>
                       
                      
            <strong><p class="text-center"><small>' . $log->date_time . '</small></p></strong>
            </div>';
                    }
                }

            }
            else
            {
                $return_html.='<div  style="padding:15px; background-color:#94A0B2; margin-top:15px; text-align:center;">
                                <span>No Logs Found.</span></div>';
            }

            $return_html.='</div>';





            return response()->json(
                [
                    'success'   => $return_html,
                    'start'     => $daily_start->format('m/d/Y'),
                    'end'       => $daily_end->format('m/d/Y'),
                     'current' => $current_date,
                    'compare' => $compare_time,

                ]
            );
        }
    }

  // it's for  get  fit bit data and delete fit bit data

    public function get_log_fitbit_data(Request $request)
    {

        $patient_id = $request->patient_id;
        $fitbit_data_id = $request->fitbit_data_id;
        if($request->fitbit_data_id!="")
        {
            $server_date = Carbon::now();
            $days = $fitbit_data_id;
            $fitbit_days = $fitbit_data_id - 1;
            $fitbit_date = Carbon::now()->copy()->startOfDay()->subDays($days)->format("Y-m-d");
            $activity_start_date  = Carbon::now()->copy()->subDays($days)->setTime(0, 0, 0);
            $patient = Patient::where('patient_id', $patient_id)->first();
            $patient_uuid = $patient->uuid;
            $pateint_device_data = $this->logbook_model->patient_device_data($patient_uuid);
            $pateint_service_log_data = $this->logbook_model->update_patient_service_log($patient_uuid, $activity_start_date);

            if (isset($pateint_device_data->patient_device_id) && $pateint_service_log_data) {
                $third_party_user_id = $pateint_device_data->third_party_user_id;
                $fitbit_notification_data = $this->logbook_model->get_fitbit_notification_data($third_party_user_id);


                if (isset($fitbit_notification_data->fitbit_notification_id)) {
                    $count = $fitbit_notification_data->count + 1;
                    $update_fitbit_notification_data = $this->logbook_model->update_fitbit_notification_data($fitbit_notification_data->fitbit_notification_id, $count);

                } else {
                    $insert_fitbit_notification_data = $this->logbook_model->insert_fitbit_notification_data($third_party_user_id, $server_date);

                }

                $activity_log_summary = "DELETE FROM activity_log_summary WHERE patient_id = $patient_id  AND DATE_FORMAT(FROM_UNIXTIME((timestamp + log_time_offset)/1000), '%Y-%m-%d') >= '".$fitbit_date."'";
                $activity_log_summary_res = DB::delete($activity_log_summary);
                $misfit_log = "DELETE FROM misfit_activity_log WHERE patient_id = $patient_id AND  DATE_FORMAT(FROM_UNIXTIME((timestamp + log_time_offset)/1000), '%Y-%m-%d') >= '".$fitbit_date."'";
                $misfit_log_res = DB::delete($misfit_log);



            }

        }


        return response()->json(
            [
                'success'   => 'success',


            ]
        );


    }




}//End of Class

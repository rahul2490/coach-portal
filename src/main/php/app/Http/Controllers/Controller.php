<?php

namespace HealthSlatePortal\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController {
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	protected function isTableDownloadRequest() {
		$action = request( 'action', null );

		return ( in_array( $action, [ 'csv' ] ) ? true : false );
	}

	/**
	 * redirectBack
	 *      Common redirect handler
	 *
	 * @param string $redirect_to
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	protected function redirectBack($redirect_to = 'home'){
		return redirect( $redirect_to );
	}
}

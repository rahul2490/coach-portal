<?php

namespace HealthSlatePortal\Http\Controllers\techsupport;
use Illuminate\Http\Request;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\Eloquent\User;
use Session;
use Validator;

class HomeController extends Controller {

	/**
	 *  common variable for passing data to view
	 * @var array
	 */
	protected $data = array();

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
    /**
     *index function .
     * (it's show the welcome page and show the popup of password expired)
     */

	public function index() {
        $datediff="";
        $last_password_update_date ="";
        $user_id = session('userId');
        $user =  User::where('user_id',$user_id)->first();

        if($user->password_reset_date!="")
            $last_password_update_date = date("Y-m-d",strtotime($user->password_reset_date));
        elseif($user->password_modified_time!="")
            $last_password_update_date = date("Y-m-d", $user->password_modified_time/1000);
        else
            $last_password_update_date = date("Y-m-d",strtotime($user->registration_date));

        $today_date = time();
        $last_password_update_date  = strtotime($last_password_update_date);
        $datediff = $today_date - $last_password_update_date;
        $datediff =  floor($datediff / (60 * 60 * 24));
        $this->data['date_diff'] = $datediff;
        return view( 'welcome', $this->data );
	}


    /**
     * Change facility from top right corner list menu
     *
     * @return \Illuminate\Http\Response
     */
    public function facilityChange(Request $request) {
        if (request()->ajax()) {

            $validator = Validator::make(request()->input(), [
                'facility' => 'required',
            ]);

            if ($validator->fails()) {
                return $response = [
                    'error' => "Invalid Data Sending!",
                ];
            }

            Session::put( 'active_facility', $request->input('facility'));
            return $response = [
                'success' => 'Facility Updated Successfully',
            ];
        }
    }


}

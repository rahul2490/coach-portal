<?php

namespace HealthSlatePortal\Http\Controllers;
use Illuminate\Http\Request;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\Eloquent\UserPasswordHistory;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\EmailNotificationMessage;
use HealthSlatePortal\Helpers\MailServiceHelper;
use HealthSlatePortal\Helpers\SmsServiceHelper;
use HealthSlatePortal\Models\FacilityModel;
use Session;
use Validator;
use DB;
use Hash;

class LoginController extends Controller {

	/**
	 *  common variable for passing data to view
	 * @var array
	 */
	protected $data = array();
    protected $facility_model;

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */

    function __construct(MailServiceHelper $mailServiceHelper,SmsServiceHelper $smsServiceHelper,FacilityModel $facility_model) {
        $this->facility_model  = $facility_model;
        $this->mailServiceHelper = $mailServiceHelper;
        $this->smsServiceHelper =  $smsServiceHelper;


    }

    /**
     *  forgot password function
     * (it's check  email is exit or not in data base  and send mail and message to user for reset password)
     */

    public function fogotPassword(Request $request) {

        $validator = Validator::make(request()->input(), ['email' => 'required',]);
        if ($validator->fails()) {
            info('Forgot Password Validation Failed');
            return $response = [
                'error' => 'Please enter your email',
            ];
        }

        $user = User::where('email', $request->email)->where('user_type','!=', 'PATIENT')->first();
        if(empty($user))
        {
            return response()->json([
                'error' => 'Email not found into our system.'
            ]);
        }

       if(!empty($user))
        {


            $user_id = $user->user_id;
            $user_email = $user->email;
            $user_first_name = $user->first_name;
            $token = md5(uniqid(rand(), true));
            $token_expiry = time()* 1000 ;
            $sent_time  = time() ;
            $is_expired = 0;
            $pin_code =  rand ( 10000 , 99999 );
            $cobrand_id=get_cobrand_id();
            if($cobrand_id==2){
                $link =  env('APP_BASE_URL_SOLERA')."/token/".$token;
                $sms_body = "Please use following pin while resetting your password:".$pin_code;
                $email_body=  "<html><body><br/>Hello,<br/><br/>You're receiving this e-mail because you forgot your SoleraOne Program account password. Please use the link below to reset your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The SoleraOne Team<br/><br/></body></html>";

            }else{
                $link =  env('APP_BASE_URL')."/token/".$token;
                $sms_body = "Please use following pin while resetting your password:".$pin_code;
                $email_body=  "<html><body><br/>Hello,<br/><br/>You're receiving this e-mail because you forgot your HealthSlate Program account password. Please use the link below to reset your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The HealthSlate Team<br/><br/></body></html>";

            }
            EmailNotificationMessage::where('user_id', '=', $user_id)->update(array('is_expired' => DB::raw(1)));
            $email_notification =  new EmailNotificationMessage;
            $email_notification->user_id = $user_id;
            $email_notification->email_body = $email_body;
            $email_notification->sms_body = $sms_body;
            $email_notification->token = $token;
            $email_notification->token_expiry = $token_expiry;
            $email_notification->pin_code = $pin_code;
            $email_notification->sent_time = $sent_time;
            $email_notification->is_expired = DB::raw(0);
            $email_notification->save();
            $name = $user_first_name;
            if($cobrand_id==2){
                $message ="<html><body>You're receiving this e-mail because you forgot your SoleraOne Program account password. Please use the link below to reset your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";

            }else{
                $message ="<html><body>You're receiving this e-mail because you forgot your HealthSlate Program account password. Please use the link below to reset your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";

            }
            $body = ['name'=>$name,'content' => $message,'subject' =>'Reset Password Request' ];
            $to = $user_email;
            info( 'Processing send email request' );
            //mail sending function is call here
            $this->mailServiceHelper->sendEmail($body, trim($to));
            $sms_to = $user->phone;
            info( 'Processing send message request' );
            //sms sending function is call here
            if($user->phone!=""){
            $return_sms_id = $this->smsServiceHelper->sendSms($sms_body,$sms_to);
            if($return_sms_id)
                info( " Message Send Successfully  to user id :  $user_id ");
            else
                info( " Message not Send Successfully to user id : $user_id" );
            }
        }

        return response()->json(
            [
                'success' => 'An email has been sent successfully to your email address.'
            ]
        );


	}

    /**
     *  Reset password function
     * (it's check token expired or not and show reset password page)
     */

	public function resetPassword($token)
    {
        $this->data['cobrand_id'] = get_cobrand_id();
        $this->data['token']= $token;
        $emailnotificationmessage = EmailNotificationMessage::where('token', '=', $token)->first();
        if($emailnotificationmessage->is_expired)
            $this->data['token_exipred']= "expired";
        return view('auth.resetpassword',$this->data);
    }

    /**
     *  Reset password store function
     * (it's store new password in data base)
     */

    public function resetPasswordStore(Request $request)
    {
      $reset_password_token = $request->reset_password_token;
       $password = $request->password;
       $repeat_password = $request->repeat_password;
       $pin_code =  $request->pin_code;

       $validator = Validator::make(request()->input(), [
            'password' => 'required|min:8',
            'repeat_password' => 'required|min:8|same:password',
            'pin_code'=>'required'
        ]);

         if ($validator->fails()) {
             info('Repeat Password Validation Failed');
             $all_error="";
             $errors = $validator->messages();
             if (!empty($errors)) {
                 foreach ($errors->all() as $error) {
                     $all_error.= $error. "<br>" ;
                 }
             }
             return $response = [
                 'error' => $all_error,
             ];
         }
            $emailnotificationmessage = EmailNotificationMessage::where('token', '=', $reset_password_token)->first();
            $time = strtotime("-1 year", time());
            $date = date("Y-m-d", $time);
            $user_password_history = $this->facility_model->get_user_password_history($emailnotificationmessage->user_id, $date);

            $old_password_count = 0;
            if(count($user_password_history) > 0)
            {
                foreach ($user_password_history as $key => $value)
                {
                    if (Hash::check($request->password, $value->password))
                    {
                        $old_password_count++;
                    }
                }
            }

            if($old_password_count > 0)
            {
                return response()->json([
                    'error' => trans('common.password_already_entered')
                ]);
            }



            //email notification table update
           $email_notification_res = EmailNotificationMessage::where('token', '=', $reset_password_token)->where('pin_code', '=', $pin_code)->update(['is_expired' => DB::raw(1)]);
           if($email_notification_res)
           {
               //$bcrypt_password = Hash::make($password);
               $bcrypt_password_get = Hash::make($password);
               $bcrypt_password_remove_char = substr($bcrypt_password_get, 3);
               $bcrypt_password = "$2a".$bcrypt_password_remove_char;
               $password_reset_date = date('Y-m-d H:i:s');
               //user table update
               $userupdate = User::find($emailnotificationmessage->user_id);
               $userupdate->password = $bcrypt_password;
               $userupdate->password_reset_date = $password_reset_date;
               $userupdate->update();
               $user = User::find($emailnotificationmessage->user_id)->first();
               // insert data in user history table
               $user_password_history = new UserPasswordHistory;
               $user_password_history->user_id     = $emailnotificationmessage->user_id;
               $user_password_history->password    = $bcrypt_password;
               $user_password_history->timestamp   = date('Y-m-d H:i:s');
               $user_password_history->save();
               // oauth_access_token , oauth_refresh_token table data deleted
               DB::delete("DELETE oauth_access_token , oauth_refresh_token  FROM  oauth_access_token  LEFT JOIN oauth_refresh_token  ON
               oauth_access_token.token_id= oauth_refresh_token.token_id  WHERE  oauth_access_token.user_name = '$user->email'");
               return response()->json(
                   [
                       'success' => 'your password reset successfully and you will redirect on login page after 5 seconds',
                       'redirect_url'=> route('login'),
                   ]
               );

           }
           else
           {
               return response()->json([
                   'error' => 'Invalid pin Code'
               ]);

           }

    }


    /**
     *  change  password  function
     */
    public function changePassword(Request $request)
    {

        if ($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'old_password'      => 'required',
                'new_password'      => 'required',
                'confirm_password'  => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'error' => trans('common.unable_to_update_password')
                ]);
            }

            $data = User::where('email', '=', session('user')->userName)->first();

            if (!Hash::check($request->old_password, $data->password))
            {
                return response()->json([
                    'error' => trans('common.wrong_password')
                ]);
            }
            $time = strtotime("-1 year", time());
            $date = date("Y-m-d", $time);
            $user_password_history = $this->facility_model->get_user_password_history($data->user_id, $date);
            $old_password_count = 0;
            if(count($user_password_history) > 0)
            {
                foreach ($user_password_history as $key => $value)
                {
                    if (Hash::check($request->new_password, $value->password))
                    {
                        $old_password_count++;
                    }
                }
            }

            if($old_password_count > 0)
            {
                return response()->json([
                    'error' => trans('common.password_already_entered')
                ]);
            }

            //$bcrypt_password = Hash::make($request->new_password);
            $bcypt_password_get = Hash::make($request->new_password);
            $bcrypt_password_remove_char = substr($bcypt_password_get, 3);
            $bcrypt_password = "$2a".$bcrypt_password_remove_char;
            $data->password = $bcrypt_password;
            $data->password_reset_date = date('Y-m-d H:i:s');
            $data->password_modified_time = time()*1000 ;
            $data->update();

            $user_password_history = new UserPasswordHistory;
            $user_password_history->user_id     = $data->user_id;
            $user_password_history->password    = $bcrypt_password;
            $user_password_history->timestamp   = date('Y-m-d H:i:s');
            $user_password_history->save();

            return response()->json(
                [
                    'success' => trans_choice('common.record_stored_updated_success', 1, [ 'action' => 'Updated'])
                ]
            );


        }
    }




}

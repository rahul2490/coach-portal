<?php

namespace HealthSlatePortal\Http\Controllers;
use Carbon\Carbon;
use HealthSlatePortal\Models\Eloquent\Provider;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\PatientModel;
use HealthSlatePortal\Models\CurriculumServiceProvider;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\PatientMotivationImage;
use HealthSlatePortal\Models\Eloquent\Patient;
use Illuminate\Support\Facades\Log;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\PatientHideUnhide;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\CoachNotes;
use HealthSlatePortal\Models\Eloquent\Message;
use HealthSlatePortal\Models\Eloquent\SessionAttendance;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\DashboardSettingModel;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use Edujugon\PushNotification\PushNotification;
use HealthSlatePortal\Helpers\MailServiceHelper;
use ChristofferOK\LaravelEmojiOne\LaravelEmojiOne;
use HealthSlatePortal\Helpers\RequestHelper;
use DB;
use Input;
use Validator;
use DateTime;
use Session;

class DashboardController extends Controller {

    protected $facility_model;
    protected $patient_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $user;
    protected $provider;
    protected $dataTableService;
    protected $group_model;
    protected $group_member;
    protected $curriculumServiceProvider;
    protected $mailServiceHelper;
    protected  $dashboard_setting_model;
    protected $data = array();

    function __construct(  RequestHelper $requestHelper, PatientModel $patient_model,FacilityModel $facility_model, MemberModel $member_model, Facility $facility, CoacheModel $coach_model,
                          DataTableServiceProvider $dataTableService, Provider $provider, User $user, GroupModel $group_model, GroupMember $group_member, CurriculumServiceProvider $curriculumServiceProvider,MailServiceHelper $mailServiceHelper,DashboardSettingModel $dashboard_setting_model) {
        $this->requestHelper = $requestHelper;
        $this->dashboard_setting_model = $dashboard_setting_model;
        $this->facility_model       = $facility_model;
        $this->patient_model       = $patient_model;
        $this->coach_model          = $coach_model;
        $this->member_model         = $member_model;
        $this->group_member         = $group_member;
        $this->group_model          = $group_model;
        $this->facility             = $facility;
        $this->user                 = $user;
        $this->provider             = $provider;
        $this->dataTableService     = $dataTableService;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->mailServiceHelper    = $mailServiceHelper;
        $this->group_db             = env('GROUP_DB_DATABASE');
        $this->active_facility_id   = session('active_facility');
        $this->active_user_id       = session('userId');
        $this->active_user_role     = session('userRole');
        $this->data['highchart_js'] = array('highcharts','highcharts-3d','highcharts-more');
        $this->data['js']           = array('message');
        $this->data['common_js']    = array('dashboard','tagging/jquery-migrate-1.2.1', 'tagging/jquery.caretposition', 'tagging/jquery.sew');
        //$this->data['common_js']    = array('tagging/jquery-migrate-1.2.1', 'tagging/jquery.caretposition', 'tagging/jquery.sew');


    }


    function index($patient_id)
    {

        $timezone = '';
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }

        $this->data['group_link'] = "";
        $this->data['session'] = "" ;
        $patient_info = $this->patient_model->get_patient_info($patient_id, $this->active_facility_id);
        $uuid = $patient_info->uuid;
        //print_r($patient_info);
        $this->data['patient_info'] = $patient_info;
        if(empty($patient_info->patient_id))
        {
           return redirect()->intended('/'.$this->active_user_role.'/members');
        }

        $current_weight = $this->patient_model->get_patient_current_weight($patient_info->patient_id);
        if(!empty($current_weight->current_weight) && !empty($patient_info->starting_weight) && !empty($patient_info->target_weight)) {
            $targeted_weight = $patient_info->starting_weight - $patient_info->target_weight;
            $actual_weight = $patient_info->starting_weight - $current_weight->current_weight;
            $weight_lose= $actual_weight*100/$targeted_weight;
            if($weight_lose < 0){
                $weight_lose=0.0;
            }else if($weight_lose > 100){
                $weight_lose=100;
            }
        }else{
            $weight_lose= 0.0;
        }

        if(empty($weight_lose)){
            $this->data['weight_lose']=0.0;
        }else{
            $this->data['weight_lose'] =number_format($weight_lose,1);
        }


        if(empty($current_weight->current_weight))
            $current_weight = '0.0';
        else
            $current_weight = number_format($current_weight->current_weight,1);

        $this->data['current_weight'] = $current_weight;
        $this->data['total_post']='';

        if($patient_info->group_id !="" )
        {
            $group_id   = array($patient_info->group_id);
            $is_person  = $patient_info->is_person;

            $completed_week         = $patient_info->completed_week;
            $group_session_detail   = collect($this->group_model->get_group_current_session_name($group_id));

            $curriculum_for_dpp     = '';
            $curriculum_for_dpp2    = '';

            if ($is_person == 1) {
                // Calling Curriculam server for Section list for both type DPP and DPP2
                $curriculum_program_type_name = $group_session_detail->unique('curriculum_program_type_name')->pluck('curriculum_program_type_name' )->toArray();
                //print_r(($curriculum_program_type_name)); exit;
                if(count($curriculum_program_type_name) > 0)
                {
                    foreach ($curriculum_program_type_name as $value)
                    {
                        if($value == 'DPP2' || $value == 'DPP')
                        {
                            $curriculum = [
                                (object) [ 'uuid' => 'inperson', 'curriculum_program_type_name' => $value],
                            ];

                            if($value == 'DPP')
                                $curriculum_for_dpp  = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                            if($value == 'DPP2')
                                $curriculum_for_dpp2 = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                        }
                    }
                }
            }

            if($completed_week == 0)
                $completed_week = 1;
            $total_sessions  = array();
            if($is_person != 1)
                $group_link = route('leadcoach.group-detail', ['group_id' => $patient_info->group_id]);
            else
            {
                $group_link = route('leadcoach.group-session', ['group_id' => $patient_info->group_id]);
                $week_group = 0;

                $current_session_name = '';
                if(count($group_session_detail) > 0)
                {
                    $group_current_session = $group_session_detail->where('group_id', $patient_info->group_id)->last();
                    //print_r($group_current_session);
                    if(!empty($group_current_session->topic_id))
                    {
                        if($curriculum_for_dpp != '')
                        {
                            $total_sessions = $curriculum_for_dpp;
                            $current_session_name = $curriculum_for_dpp->where('section_uuid', $group_current_session->topic_id)->toArray();
                        }

                        if($curriculum_for_dpp2 != '')
                        {
                            $total_sessions = $curriculum_for_dpp2;
                            $current_session_name = $curriculum_for_dpp2->where('section_uuid', $group_current_session->topic_id)->toArray();
                        }


                        if(!empty($current_session_name))
                        {
                            foreach ($current_session_name as $val)
                                $week_group =  $val->display_week;
                        }
                    }

                }
            }

            $this->data['group_link']  = $group_link;
            if($is_person == 1)
                $this->data['session'] = $week_group."/".count($total_sessions) ;
            else
                $this->data['session'] = "NA" ;

            // Batch Count Start
            $read_count = GroupMember::where('group_id', $group_id)->where('user_id',$this->active_user_id )->first();
            if(!empty($read_count))
            {
                $post_read_time =  $read_count->post_read_time;
                $user_id= $this->active_user_id;
                $group_post = $this->group_model->get_post_read_count($group_id,$post_read_time,$user_id);
            }
            if(!empty($group_post)){
                $count_post=$group_post[0]->total_post;
                if($count_post==0){
                    $count_post='';
                }
            }
            else{
                $count_post='';
            }
            $this->data['total_post'] = $count_post;
            // Batch Count End
        }

        $now = Carbon::now();
        if(!empty($timezone))
            $now = Carbon::createFromFormat('Y-m-d H:i:s', $now)->timezone($timezone);
        else
            $now = Carbon::createFromFormat('Y-m-d H:i:s', $now);

        if($now->isMonday()){
            $last_week_0 = $now->copy()->startOfDay();
        }else{
            $last_week_0 = new Carbon('last monday');
        }
        $last_week_1 = $last_week_0->copy()->subWeeks(1);
        $last_week_2 = $last_week_1->copy()->subWeeks(1);
        $last_week_3 = $last_week_2->copy()->subWeeks(1);

        $daily_end      = $now->copy()->endOfDay();
        $daily_start    = $now->copy()->subDays(6)->startOfDay();

        $monthly_end     = $now->copy()->startOfDay();
        $monthly_start   = $last_week_0->copy()->subWeeks(6);

        $date_array = [
           'daily_start'    => $daily_start->format('m/d/Y'),
           'daily_end'      => $daily_end->format('m/d/Y'),
           'monthly_start'  => $monthly_start->format('m/d/Y'),
           'monthly_end'    => $monthly_end->format('m/d/Y'),

            'last_week_0'  => $last_week_0,
            'last_week_1'  => $last_week_1,
            'last_week_2'  => $last_week_2,
            'last_week_3'  => $last_week_3,
        ];

        $this->data['engagement_detail_daily']=$this->get_patient_engagement_daily($uuid,$daily_start, $daily_end);
        $this->data['engagement_detail_weekly']=$this->get_patient_engagement_weekly($uuid, $monthly_end, $monthly_start);

        $this->data['patient_weight_daily']  = $this->calculate_patient_weight_daily($patient_id, $daily_start, $daily_end);
        $this->data['patient_weight_weekly'] = $this->calculate_patient_weight_weekly($patient_id, $monthly_end, $monthly_start);

        $this->data['physical_activity_steps_daily']   = $this->calculate_physical_activity_steps_daily($patient_id, $daily_start, $daily_end);
        $this->data['physical_activity_steps_weekly']  = $this->calculate_physical_activity_steps_weekly($patient_id, $monthly_end, $monthly_start);

        $this->data['physical_activity_minutes_daily']   = $this->calculate_physical_activity_minutes_daily($patient_id, $daily_start, $daily_end);
        $this->data['physical_activity_minutes_weekly']  = $this->calculate_physical_activity_minutes_weekly($patient_id, $monthly_end, $monthly_start);

        $this->data['patient_meal_logged_daily']   = $this->calculate_patient_meal_logged_daily($patient_id, $daily_start, $daily_end);
        $this->data['patient_meal_logged_weekly']  = $this->calculate_patient_meal_logged_weekly($patient_id, $monthly_end, $monthly_start);


        $this->data['patient_meal_source_daily']   = $this->calculate_patient_meal_source_daily($patient_id, $daily_start, $daily_end);
        $this->data['patient_meal_source_weekly']  = $this->calculate_patient_meal_source_weekly($patient_id, $monthly_end, $monthly_start);
        //print_r($this->data['patient_meal_logged_daily'] );die;

        $this->data['date_array'] = $date_array;

        $patient_conversation_info = $this->patient_model->get_patient_conversation_info($patient_id);

        $patient_communications_info = $this->patient_model->get_patient_communications_info($patient_id);

        if(!empty($patient_communications_info->last_message_sent_to_patient))
        {
            if (!empty($patient_communications_info->last_message_sent_to_patient)) {
                $date = ($patient_communications_info->last_message_sent_to_patient/1000);
                $date = date("Y-m-d H:i:s", $date);
                $communication_timestamp = (!empty($timezone)) ? Carbon::createFromFormat('Y-m-d H:i:s', $date)->timezone($timezone)->format('h:i A d M Y') : Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('h:i A d M Y');
                $patient_communications_info->last_message_sent_to_patient = $communication_timestamp;
            }else{
                $patient_communications_info->last_message_sent_to_patient = 'N/A';
            }
        }
        else
            $patient_communications_info->last_message_sent_to_patient = 'N/A';

        if(!empty($patient_communications_info->last_message_of_patient))
        {
            if (!empty($patient_communications_info->last_message_of_patient)) {
                $date = ($patient_communications_info->last_message_of_patient/1000);
                $date = date("Y-m-d H:i:s", $date);
                $communication_timestamp = (!empty($timezone)) ? Carbon::createFromFormat('Y-m-d H:i:s', $date)->timezone($timezone)->format('h:i A d M Y') : Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('h:i A d M Y');
                $patient_communications_info->last_message_of_patient = $communication_timestamp;
            }else{
                $patient_communications_info->last_message_of_patient = 'N/A';
            }
        }
        else
            $patient_communications_info->last_message_of_patient = 'N/A';

        $this->data['patient_communications_info'] = $patient_communications_info;


        $patient_goals_info = $this->patient_model->get_patient_goals_info($patient_id);
        $patient_goals = array();
        $week_days = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');
        foreach ($patient_goals_info as $value)
        {
            $reminder_settings = json_decode($value->reminder_settings);
            $reminder_time = '';
            if(!empty($reminder_settings))
            {
                $reminder_days = 'Reminder: ';
                foreach ($reminder_settings as $setting)
                {
                    $reminder_days .= ', ' . $week_days[$setting->dayOfWeek - 1]  ;
                    $reminder_time  = $setting->reminderTime;
                }
                $reminder_days = str_replace(': , ', ': ', $reminder_days);
            }
            else
                $reminder_days = '';

                $array  =  (object) [
                    'goal_name'             =>  $value->goal_name,
                    'reminder_settings'     =>  $reminder_days,
                    'reminder_time'         =>  $reminder_time,
                    'is_notification_on'    =>  $value->is_notification_on,
                    'patient_goal_id'       =>  $value->patient_goal_id,
                    'goal_type'             =>  $value->goal_type,
                ];
                array_push($patient_goals, $array);
        }

        $this->data['patient_goals_info'] = $patient_goals;
        $this->data['patient_goals_statics_info_array'] = array();
        if(!empty($patient_goals))
        {
            $patient_goals_statics_info = $this->patient_model->get_patient_goals_statics_info($patient_id,$last_week_3,$patient_goals[0]->patient_goal_id);
            $patient_goals_statics_info_array = array();
            if(!empty($patient_goals_statics_info)) {
                foreach ($patient_goals_statics_info as $value) {
                    array_push($patient_goals_statics_info_array, date("Y-m-d", strtotime($value->goal_due_date_time)));
                }
            }
            $this->data['patient_goals_statics_info_array'] = $patient_goals_statics_info_array;
        }


        $patient_coach_notes = $this->patient_model->get_coach_notes_info($patient_id);

        $this->data['patient_coach_notes']=array();
        $this->data['patient_conversation_info']=array();


        foreach ($patient_coach_notes as $notes) {
            $notes_timestamp = '';
        if (!empty($notes->notes_date)) {
            $no_date= ($notes->notes_date/1000);
            $notes_date =date("Y-m-d H:i:s",$no_date);
            $notes_timestamp = (!empty($timezone)) ? Carbon::createFromFormat('Y-m-d H:i:s', $notes_date)->timezone($timezone) : Carbon::createFromFormat('Y-m-d H:i:s', $notes_date);
            $notes_timestamp = $notes_timestamp->diffForHumans();
        }else{
            $notes_timestamp='';
        }

             $array  = array(
                'first_name'    =>  $notes->first_name,
                'last_name'     =>  $notes->last_name,
                'image_path'    =>  $notes->image_path,
                 'provider_full_name' => $notes->provider_full_name,
                'notes'         =>  $notes->notes,
                'notes_date'    =>  $notes_timestamp,
            );

             array_push($this->data['patient_coach_notes'],$array);
        }

        //print_r($patient_coach_notes);

        $emoji = new LaravelEmojiOne();
        $this->data['patient_conversation_info'] = array();
        if(!empty($patient_conversation_info))
        {
            foreach ($patient_conversation_info as $conversation) {
                $conversation_timestamp = '';
                if (!empty($conversation->timestamp)) {
                    $co_date        = ($conversation->timestamp/1000);
                    $con_date       = date("Y-m-d H:i:s",$co_date);
                    $conversation_timestamp = (!empty($timezone)) ? Carbon::createFromFormat('Y-m-d H:i:s', $con_date)->timezone($timezone) : Carbon::createFromFormat('Y-m-d H:i:s', $con_date);
                    $conversation_timestamp = $conversation_timestamp->diffForHumans();
                }else{
                    $conversation_timestamp = '';
                }

                $profile_image = env('APP_BASE_URL').'/img/default-user.png';
                if($conversation->image_path != '' && $conversation->owner == 'PATIENT')
                {
                    $profile_image = env('PROFILE_IMAGE_BASE_URL') . $conversation->image_path;
                }

                if($conversation->owner == 'PATIENT')
                {
                    $full_name = $conversation->full_name;
                }
                else
                {
                    $full_name = $conversation->provider_full_name;
                }

                $array  = array(
                    'full_name'     =>  $full_name,
                    'image_path'    =>  $profile_image,
                    'message'       =>  $emoji->shortnameToImage($conversation->message),
                    'message_image_path' => $conversation->message_image_path,
                    'timestamp'     =>  $conversation_timestamp,
                );
                array_push($this->data['patient_conversation_info'],$array);
            }
        }

        if($patient_info->last_login_date != 'N/A' && $patient_info->offset_key != '')
        {
            $timezone   = $patient_info->offset_key;
            $patient_info->last_login_date  = Carbon::createFromFormat('Y-m-d H:i:s', $patient_info->last_login_date,"UTC")->setTimezone($timezone)->format('h:i A d M Y');
        }

        if(!empty($patient_info->description))
            $patient_info->description = $emoji->shortnameToImage($patient_info->description);

        $this->data['patient_conversation_info'] = array_reverse($this->data['patient_conversation_info']);

        $all_coaches    = $this->coach_model->get_facility_coach_list(session('active_facility'));
        $resultArray    = $this->_group_coachListBuilder( $all_coaches, [$this->active_user_id] );
        $lead_coach = array();
        $lead_coach = $resultArray['lead_coach'];
        $this->data['uuid']           = $uuid;
        $this->data['patient_id']           = $patient_id;
        $this->data['leadcoach_foodcoach']  = $lead_coach;
        //print_r($this->data['patient_coach_notes']);die;
        return view( 'common.dashboard', $this->data );
    }


    private function calculate_patient_weight_daily($patient_id, $daily_start, $daily_end)
    {
        $utc_start  = $daily_start;
        $utc_end    = $daily_end;

        $patient_data   = Patient::where('patient_id',$patient_id)->first();
        $user_id        =  $patient_data->user_id;
        $user_data      = User::where('user_id',$user_id)->first();
        $offset_key     = $user_data->offset_key;

        if(!empty($offset_key))
        {
            $utc_start  = Carbon::createFromFormat('Y-m-d H:i:s', $utc_start,$offset_key)->setTimezone('UTC');
            $utc_end    = Carbon::createFromFormat('Y-m-d H:i:s', $utc_end,$offset_key)->setTimezone('UTC');
        }

        $patient_weight_info = $this->patient_model->get_patient_weight($patient_id, $utc_start, $utc_end);

        $daily_date = array();
        $average_weight = 0;
        $lowest_weight  = 0;
        $highest_weight = 0;
        $actual_weight  = 0;

        $temp_date = '';
        for($i=0; $i<7; $i++)
        {
            if($i == 0)
            {
                $week_day_name[] = $daily_end->format('D');
                $week_day_date[] = $daily_end->format('Y-m-d');
                $temp_date       = $daily_end;
            }
            else
            {
                $week_day_name[]    = $temp_date->copy()->subDay(1)->format('D');
                $week_day_date[]    = $temp_date->copy()->subDay(1)->format('Y-m-d');
                $temp_date          = $temp_date->copy()->subDay(1);
            }
        }

        $week_day_name = array_reverse($week_day_name);
        $week_day_date = array_reverse($week_day_date);

        if(!empty($patient_weight_info))
        {
            $daily_weight_data = array();
            $patient_weight_data = collect($patient_weight_info);

            //$weight = $patient_weight_data->pluck('weight')->all();

            foreach ($week_day_date as $date)
            {
                if($patient_weight_data->contains('patient_log_date_time', $date))
                {
                    $weight = $patient_weight_data->where('patient_log_date_time', $date)->first();
                    $actual_weight += 1;
                    $daily_weight_data[] = round($weight->weight,0);
                }
                else
                    $daily_weight_data[] = null;
            }
            if(!empty($daily_weight_data) && count($daily_weight_data) > 0)
            {
                $min = array_diff($daily_weight_data, array(null));
                $highest_weight = max($daily_weight_data) == null ? 0 : round(max($daily_weight_data), 1);
                if(count($min) <= 0)
                    $lowest_weight  = 0;
                else
                    $lowest_weight  = min($min) == null ? 0 : round(min($min),1);
                if($actual_weight <= 0)
                    $actual_weight = 1;
                $average_weight = round(array_sum($daily_weight_data)/$actual_weight,1);
            }
        }

        if(empty($patient_weight_info))
        {
            for($i=0; $i<7; $i++)
            {
                $daily_weight_data[] = null;
            }
        }

        $data = [
            'daily_weight_data' => $daily_weight_data,
            'week_day_name'     => $week_day_name,
            'average_weight'    => $average_weight,
            'lowest_weight'     => $lowest_weight,
            'highest_weight'    => $highest_weight,
            'actual_weight'     => $actual_weight
        ];
        return $data;
    }

    private function calculate_patient_weight_weekly($patient_id, $monthly_end, $monthly_start)
    {

        $utc_start  = $monthly_start;
        $utc_end    = $monthly_end;

        $patient_data   = Patient::where('patient_id',$patient_id)->first();
        $user_id        =  $patient_data->user_id;
        $user_data      = User::where('user_id',$user_id)->first();
        $offset_key     = $user_data->offset_key;

        if(!empty($offset_key))
        {
            $utc_start  = Carbon::createFromFormat('Y-m-d H:i:s', $utc_start,$offset_key)->setTimezone('UTC');
            $utc_end    = Carbon::createFromFormat('Y-m-d H:i:s', $utc_end,$offset_key)->setTimezone('UTC');
        }

        $patient_weight_info = $this->patient_model->get_patient_weight($patient_id, $utc_start, $utc_end);

        $daily_date = array();
        $average_weight = 0;
        $lowest_weight  = 0;
        $highest_weight = 0;
        $actual_weight  = 0;
        $num_days_weight_entry = array();

        $temp_date = '';

        for($i=0; $i<7; $i++)
        {
            if($i == 0)
            {
                if($monthly_end->isMonday()){
                    $last_week_0 = $monthly_end->copy()->startOfDay();
                }else{
                    $last_week_0 = $monthly_end->copy()->startOfWeek();
                }

                $week_day_name[] = $last_week_0->format('m/d');
                $week_day_date[] = $last_week_0->format('Y-m-d');
                $temp_date       = $last_week_0;
            }
            else
            {
                $week_day_name[]    = $temp_date->copy()->subWeek(1)->format('m/d');
                $week_day_date[]    = $temp_date->copy()->subWeek(1)->format('Y-m-d');
                $temp_date          = $temp_date->copy()->subWeek(1);
            }
        }

        $week_day_name = array_reverse($week_day_name);
        $week_day_date = array_reverse($week_day_date);

        if(!empty($patient_weight_info))
        {
            $daily_weight_data  = array();
            $final_weight_data  = array();
            $patient_weight_data = collect($patient_weight_info);

            //print_r($week_day_date);
            foreach ($week_day_date as $date)
            {

                $start_date     = Carbon::createFromFormat( 'Y-m-d', $date )->setTime( 0, 0 );
                $end_date       = $start_date->copy()->addWeek(1)->format('Y-m-d');
                $start_date     = $date;

                foreach ($patient_weight_data as $weight_data)
                {
                    if($weight_data->patient_log_date_time >= $start_date && $weight_data->patient_log_date_time < $end_date)
                    {
                        $daily_weight_data[$date][] = round($weight_data->weight,0);
                        $num_days_weight_entry[$weight_data->patient_log_date_time] = $weight_data->patient_log_date_time;
                    }
                }
            }

            foreach ($week_day_date as $date)
            {
                if (array_key_exists($date,$daily_weight_data))
                {
                    $final_weight_data[] = round((array_sum($daily_weight_data[$date])/count($daily_weight_data[$date])),0);
                    $actual_weight += 1;
                }
                else
                    $final_weight_data[] = null;
            }

            //$weight = $patient_weight_data->pluck('weight')->all();
            if(!empty($final_weight_data) && count($final_weight_data) > 0)
            {
                $min = array_diff($final_weight_data, array(null));
                $highest_weight = max($final_weight_data) == null ? 0 : round(max($final_weight_data), 1);
                if(count($min) <= 0)
                    $lowest_weight  = 0;
                else
                    $lowest_weight  = min($min) == null ? 0 : round(min($min),1);
                if($actual_weight <= 0)
                    $actual_weight = 1;
                $average_weight = round(array_sum($final_weight_data)/$actual_weight,1);
            }
        }

        if(empty($patient_weight_info))
        {
            for($i=0; $i<7; $i++)
            {
                $final_weight_data[] = null;
            }
        }

        $data = [
            'weekly_weight_data'        => $final_weight_data,
            'weekly_day_name'           => $week_day_name,
            'weekly_average_weight'     => $average_weight,
            'weekly_lowest_weight'      => $lowest_weight,
            'weekly_highest_weight'     => $highest_weight,
            'weekly_actual_weight'      => count($num_days_weight_entry) <= 0 ? 0 : count($num_days_weight_entry)
        ];
        return $data;
    }

    private function calculate_physical_activity_steps_weekly($patient_id, $monthly_end, $monthly_start)
    {
        $timezone = '';
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }
        $utc_start  = $monthly_start;
        $utc_end    = $monthly_end;
        if(!empty($timezone))
        {
            $utc_start  = Carbon::createFromFormat('Y-m-d H:i:s', $monthly_start,$timezone)->setTimezone('UTC');
            $utc_end    = Carbon::createFromFormat('Y-m-d H:i:s', $monthly_end,$timezone)->setTimezone('UTC');
        }
        $activity_steps = collect($this->patient_model->get_patient_physical_activity_steps($patient_id, $utc_start, $utc_end));
        //print_r($activity_steps);

        $daily_date = array();
        $total_days_logged = array();
        $daily_activity_steps = array();
        $total_steps = 0;
        $activity_log = 0;
        $temp_date = '';

        for($i=0; $i<7; $i++)
        {
            if($i == 0)
            {
                if($monthly_end->isMonday()){
                    $last_week_0 = $monthly_end->copy()->startOfDay();
                }else{
                    $last_week_0 = $monthly_end->copy()->startOfWeek();
                }

                $week_day_name[] = $last_week_0->format('m/d');
                $week_day_date[] = $last_week_0->format('Y-m-d');
                $temp_date       = $last_week_0;
            }
            else
            {
                $week_day_name[]    = $temp_date->copy()->subWeek(1)->format('m/d');
                $week_day_date[]    = $temp_date->copy()->subWeek(1)->format('Y-m-d');
                $temp_date          = $temp_date->copy()->subWeek(1);
            }
        }

        $week_day_name = array_reverse($week_day_name);
        $week_day_date = array_reverse($week_day_date);

        if(!empty($activity_steps))
        {
            $daily_weight_data  = array();
            $final_weight_data  = array();
            $activity_steps = collect($activity_steps);

            foreach ($week_day_date as $date)
            {

                $start_date  = Carbon::createFromFormat( 'Y-m-d', $date )->setTime( 0, 0 );
                $end_date       = $start_date->copy()->addWeek(1)->format('Y-m-d');
                $start_date     = $date;
                foreach ($activity_steps as $steps)
                {
                    if($steps->patient_date_format >= $start_date && $steps->patient_date_format < $end_date)
                    {
                        $daily_activity_steps[$date][] = round($steps->steps,0);
                        $total_steps            += round($steps->steps,0);
                        $activity_log           += $steps->num_steps_entries;
                        $total_days_logged[$steps->patient_date_format] = $steps->steps;
                    }
                }
            }

            foreach ($week_day_date as $date)
            {
                if (array_key_exists($date,$daily_activity_steps))
                {
                    $final_weight_data[] = round(array_sum($daily_activity_steps[$date]),0);
                }
                else
                    $final_weight_data[] = 0;
            }
            if(count($total_days_logged) > 0)
            {
                $total_steps = round(($total_steps/count($total_days_logged)),2);
            }
        }

        if(empty($activity_steps))
        {
            for($i=0; $i<7; $i++)
            {
                $final_weight_data[] = 0;
            }
        }

        $data = [
            'physical_activity_steps_weekly' => $final_weight_data,
            'week_day_name'     => $week_day_name,
            'total_steps'       => $total_steps,
            'activity_log'      => $activity_log,
        ];
        return $data;
    }

    private function calculate_physical_activity_steps_daily($patient_id, $daily_start, $daily_end)
    {
        $timezone = '';
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }
        $utc_start  = $daily_start;
        $utc_end    = $daily_end;
        if(!empty($timezone))
        {
            $utc_start  = Carbon::createFromFormat('Y-m-d H:i:s', $daily_start,$timezone)->setTimezone('UTC');
            $utc_end    = Carbon::createFromFormat('Y-m-d H:i:s', $daily_end,$timezone)->setTimezone('UTC');
        }

        $activity_steps = collect($this->patient_model->get_patient_physical_activity_steps($patient_id, $utc_start, $utc_end));

        $daily_activity_steps = array();
        $temp_date = '';
        $total_steps = 0;
        $activity_log = 0;
        $total_days_logged = array();
        for($i=0; $i<7; $i++)
        {
            if($i == 0)
            {
                $week_day_name[] = $daily_end->format('D');
                $week_day_date[] = $daily_end->format('Y-m-d');
                $temp_date       = $daily_end;
            }
            else
            {
                $week_day_name[]    = $temp_date->copy()->subDay(1)->format('D');
                $week_day_date[]    = $temp_date->copy()->subDay(1)->format('Y-m-d');
                $temp_date          = $temp_date->copy()->subDay(1);
            }
        }
        $week_day_date = array_reverse($week_day_date);

        if(!empty($activity_steps))
        {

            $activity_steps = collect($activity_steps);

            foreach ($week_day_date as $date)
            {
                if($activity_steps->contains('patient_date_format', $date))
                {
                    $steps = $activity_steps->where('patient_date_format', $date)->first();
                    $daily_activity_steps[] = round($steps->steps,0);
                    $total_steps    += round($steps->steps,0);
                    $activity_log   += $steps->num_steps_entries;
                    $total_days_logged[$steps->patient_date_format] = $steps->steps;
                }
                else
                    $daily_activity_steps[] = 0;
            }
        }

        if(empty($activity_steps))
        {
            for($i=0; $i<7; $i++)
            {
                $daily_activity_steps[] = 0;
            }
        }
        if(count($total_days_logged) > 0)
        {
            $total_steps = round(($total_steps/count($total_days_logged)),2);
        }

        $data = [
            'physical_activity_steps_daily' => $daily_activity_steps,
            'week_day_name'     => $week_day_name,
            'total_steps'       => $total_steps,
            'activity_log'      => $activity_log,
        ];
        return $data;
    }

    private function calculate_physical_activity_minutes_daily($patient_id, $daily_start, $daily_end)
    {

        $utc_start  = $daily_start;
        $utc_end    = $daily_end;

        $patient_data   = Patient::where('patient_id',$patient_id)->first();
        $user_id        =  $patient_data->user_id;
        $user_data      = User::where('user_id',$user_id)->first();
        $offset_key     = $user_data->offset_key;

        if(!empty($offset_key))
        {
            $utc_start  = Carbon::createFromFormat('Y-m-d H:i:s', $utc_start,$offset_key)->setTimezone('UTC');
            $utc_end    = Carbon::createFromFormat('Y-m-d H:i:s', $utc_end,$offset_key)->setTimezone('UTC');
        }

        $activity_minute        = collect($this->patient_model->get_patient_physical_activity_minute($patient_id, $utc_start, $utc_end));
        $activity_log_summary   = collect($this->patient_model->get_patient_physical_activity_minutes($patient_id, $utc_start, $utc_end));

        $daily_activity_steps = array();
        $total_days_logged = array();
        $temp_date = '';
        $total_minute = 0;
        $activity_log = 0;
        for($i=0; $i<7; $i++)
        {
            if($i == 0)
            {
                $week_day_name[] = $daily_end->format('D');
                $week_day_date[] = $daily_end->format('Y-m-d');
                $temp_date       = $daily_end;
            }
            else
            {
                $week_day_name[]    = $temp_date->copy()->subDay(1)->format('D');
                $week_day_date[]    = $temp_date->copy()->subDay(1)->format('Y-m-d');
                $temp_date          = $temp_date->copy()->subDay(1);
            }
        }
        $week_day_date = array_reverse($week_day_date);

        if(!empty($activity_minute))
        {
            $activity_minute = collect($activity_minute);
            foreach ($week_day_date as $date)
            {
                if($activity_minute->contains('patient_date_format', $date))
                {
                    $steps = $activity_minute->where('patient_date_format', $date)->first();
                    $daily_activity_steps[$date][] = round($steps->steps,0);
                    $total_minute    += round($steps->steps,0);
                    $activity_log    += $steps->num_steps_entries;
                    $total_days_logged[$date] = $steps->num_steps_entries;
                }
            }
        }

        if(!empty($activity_log_summary))
        {
            $activity_log_summary = collect($activity_log_summary);
            foreach ($week_day_date as $date)
            {
                if($activity_log_summary->contains('patient_log_date_time', $date))
                {
                    $steps = $activity_log_summary->where('patient_log_date_time', $date)->first();
                    $daily_activity_steps[$date][]  = round($steps->total_minutes_performed,0);
                    $total_minute                   += round($steps->total_minutes_performed,0);
                    $activity_log                   += $steps->num_minute_entries;
                    $total_days_logged[$date] = $steps->num_minute_entries;
                }
            }
        }

        foreach ($week_day_date as $date)
        {
            if (array_key_exists($date,$daily_activity_steps))
            {
                $final_weight_data[] = round(array_sum($daily_activity_steps[$date]),0);
            }
            else
                $final_weight_data[] = 0;
        }

        if(empty($activity_minute) && empty($activity_log_summary))
        {
            for($i=0; $i<7; $i++)
            {
                $final_weight_data[] = 0;
            }
        }

        if(count($total_days_logged) > 0)
        {
            $total_minute = round(($total_minute/count($total_days_logged)),2);
        }

        $data = [
            'physical_activity_minute_daily' => $final_weight_data,
            'total_minute_daily'        => $total_minute,
            'activity_log_daily'        => $activity_log,
        ];
        return $data;
    }

    private function calculate_physical_activity_minutes_weekly($patient_id, $monthly_end, $monthly_start)
    {
        $utc_start  = $monthly_start;
        $utc_end    = $monthly_end;

        $patient_data   = Patient::where('patient_id',$patient_id)->first();
        $user_id        =  $patient_data->user_id;
        $user_data      = User::where('user_id',$user_id)->first();
        $offset_key     = $user_data->offset_key;

        if(!empty($offset_key))
        {
            $utc_start  = Carbon::createFromFormat('Y-m-d H:i:s', $utc_start,$offset_key)->setTimezone('UTC');
            $utc_end    = Carbon::createFromFormat('Y-m-d H:i:s', $utc_end,$offset_key)->setTimezone('UTC');
        }

        $activity_minute        = collect($this->patient_model->get_patient_physical_activity_minute($patient_id, $utc_start, $utc_end));
        $activity_log_summary   = collect($this->patient_model->get_patient_physical_activity_minutes($patient_id, $utc_start, $utc_end));

        $daily_date = array();
        $daily_activity_steps = array();
        $total_minute = 0;
        $activity_log = 0;
        $temp_date = '';

        for($i=0; $i<7; $i++)
        {
            if($i == 0)
            {
                if($monthly_end->isMonday()){
                    $last_week_0 = $monthly_end->copy()->startOfDay();
                }else{
                    $last_week_0 = $monthly_end->copy()->startOfWeek();
                }

                $week_day_name[] = $last_week_0->format('m/d');
                $week_day_date[] = $last_week_0->format('Y-m-d');
                $temp_date       = $last_week_0;
            }
            else
            {
                $week_day_name[]    = $temp_date->copy()->subWeek(1)->format('m/d');
                $week_day_date[]    = $temp_date->copy()->subWeek(1)->format('Y-m-d');
                $temp_date          = $temp_date->copy()->subWeek(1);
            }
        }

        $week_day_name = array_reverse($week_day_name);
        $week_day_date = array_reverse($week_day_date);
        $daily_weight_data  = array();
        $final_weight_data  = array();
        $total_days_logged  = array();
        if(!empty($activity_minute))
        {
            $activity_minute = collect($activity_minute);
            foreach ($week_day_date as $date)
            {
                $start_date     = Carbon::createFromFormat( 'Y-m-d', $date )->setTime( 0, 0 );
                $end_date       = $start_date->copy()->addWeek(1)->format('Y-m-d');
                $start_date     = $date;
                foreach ($activity_minute as $steps)
                {
                    if($steps->patient_date_format >= $start_date && $steps->patient_date_format < $end_date)
                    {
                        $daily_activity_steps[$date][] = round($steps->steps,0);
                        $total_minute               += round($steps->steps,0);
                        $activity_log               += $steps->num_steps_entries;
                        $total_days_logged[$date]   = $steps->num_steps_entries;
                    }
                }
            }
        }

        if(!empty($activity_log_summary))
        {
            $activity_log_summary = collect($activity_log_summary);
            foreach ($week_day_date as $date)
            {

                $start_date  = Carbon::createFromFormat( 'Y-m-d', $date )->setTime( 0, 0 );
                $end_date       = $start_date->copy()->addWeek(1)->format('Y-m-d');
                $start_date     = $date;
                foreach ($activity_log_summary as $steps)
                {
                    if($steps->patient_log_date_time >= $start_date && $steps->patient_log_date_time < $end_date)
                    {
                        $daily_activity_steps[$date][] = round($steps->total_minutes_performed,0);
                        $total_minute               += round($steps->total_minutes_performed,0);
                        $activity_log               += $steps->num_minute_entries;
                        $total_days_logged[$date]   = $steps->num_minute_entries;
                    }
                }
            }
        }

        foreach ($week_day_date as $date)
        {
            if (array_key_exists($date,$daily_activity_steps))
            {
                $final_weight_data[] = round(array_sum($daily_activity_steps[$date]),0);
            }
            else
                $final_weight_data[] = 0;
        }

        if(empty($activity_minute) && empty($activity_log_summary))
        {
            for($i=0; $i<7; $i++)
            {
                $final_weight_data[] = 0;
            }
        }

        if(count($total_days_logged) > 0)
        {
            $total_minute = round(($total_minute/count($total_days_logged)),2);
        }


        $data = [
            'physical_activity_minute_weekly'   => $final_weight_data,
            'total_minute_weekly'               => $total_minute,
            'activity_log_daily'                => $activity_log,
        ];
        return $data;
    }


    private function calculate_patient_meal_logged_daily($patient_id, $daily_start, $daily_end)
    {
        $utc_start  = $daily_start;
        $utc_end    = $daily_end;

        $patient_data   = Patient::where('patient_id',$patient_id)->first();
        $user_id        =  $patient_data->user_id;
        $user_data      = User::where('user_id',$user_id)->first();
        $offset_key     = $user_data->offset_key;

        if(!empty($offset_key))
        {
            $utc_start  = Carbon::createFromFormat('Y-m-d H:i:s', $utc_start,$offset_key)->setTimezone('UTC');
            $utc_end    = Carbon::createFromFormat('Y-m-d H:i:s', $utc_end,$offset_key)->setTimezone('UTC');
        }

        $patient_weight_info = $this->patient_model->get_patient_meal_logged($patient_id, $utc_start, $utc_end);

        $daily_date = array();
        $temp_date = '';
        for($i=0; $i<7; $i++)
        {
            if($i == 0)
            {
                $week_day_name[] = $daily_end->format('D');
                $week_day_date[] = $daily_end->format('Y-m-d');
                $temp_date       = $daily_end;
            }
            else
            {
                $week_day_name[]    = $temp_date->copy()->subDay(1)->format('D');
                $week_day_date[]    = $temp_date->copy()->subDay(1)->format('Y-m-d');
                $temp_date          = $temp_date->copy()->subDay(1);
            }
        }
        $week_day_date = array_reverse($week_day_date);

        if(!empty($patient_weight_info))
        {
            $daily_weight_data = array();
            $patient_weight_data = collect($patient_weight_info);

            foreach ($week_day_date as $date)
            {
                if($patient_weight_data->contains('patient_log_date_time', $date))
                {
                    $weight = $patient_weight_data->where('patient_log_date_time', $date)->first();
                    $daily_weight_data[] = round($weight->num_meal_logged_entries,0);
                }
                else
                    $daily_weight_data[] = 0;
            }
        }

        if(empty($patient_weight_info))
        {
            for($i=0; $i<7; $i++)
            {
                $daily_weight_data[] = 0;
            }
        }

        $data = [
            'daily_meal_logged_data' => $daily_weight_data,
        ];
        return $data;
    }

    private function calculate_patient_meal_logged_weekly($patient_id, $monthly_end, $monthly_start)
    {
        $utc_start  = $monthly_start;
        $utc_end    = $monthly_end;

        $patient_data   = Patient::where('patient_id',$patient_id)->first();
        $user_id        =  $patient_data->user_id;
        $user_data      = User::where('user_id',$user_id)->first();
        $offset_key     = $user_data->offset_key;

        if(!empty($offset_key))
        {
            $utc_start  = Carbon::createFromFormat('Y-m-d H:i:s', $utc_start,$offset_key)->setTimezone('UTC');
            $utc_end    = Carbon::createFromFormat('Y-m-d H:i:s', $utc_end,$offset_key)->setTimezone('UTC');
        }

        $patient_meal_logged = $this->patient_model->get_patient_meal_logged($patient_id, $utc_start, $utc_end);
        //print_r($patient_meal_logged);
        $temp_date = '';
        $meal_logged = array();
        for($i=0; $i<7; $i++)
        {
            if($i == 0)
            {
                if($monthly_end->isMonday()){
                    $last_week_0 = $monthly_end->copy()->startOfDay();
                }else{
                    $last_week_0 = $monthly_end->copy()->startOfWeek();
                }

                $week_day_name[] = $last_week_0->format('m/d');
                $week_day_date[] = $last_week_0->format('Y-m-d');
                $temp_date       = $last_week_0;
            }
            else
            {
                $week_day_name[]    = $temp_date->copy()->subWeek(1)->format('m/d');
                $week_day_date[]    = $temp_date->copy()->subWeek(1)->format('Y-m-d');
                $temp_date          = $temp_date->copy()->subWeek(1);
            }
        }

        $week_day_name = array_reverse($week_day_name);
        $week_day_date = array_reverse($week_day_date);

        if(!empty($patient_meal_logged))
        {
            $patient_meal_logged = collect($patient_meal_logged);
            foreach ($week_day_date as $date)
            {

                $start_date     = Carbon::createFromFormat( 'Y-m-d', $date )->setTime( 0, 0 );
                $end_date       = $start_date->copy()->addWeek(1)->format('Y-m-d');
                $start_date     = $date;
                foreach ($patient_meal_logged as $logged)
                {
                    if($logged->patient_log_date_time >= $start_date && $logged->patient_log_date_time < $end_date)
                    {
                        $meal_logged[$date][]  = $logged->num_meal_logged_entries;
                    }
                }
            }
        }

        foreach ($week_day_date as $date)
        {
            if (array_key_exists($date,$meal_logged))
            {
                $final_weight_data[] = array_sum($meal_logged[$date]);
            }
            else
                $final_weight_data[] = 0;
        }

        if(empty($patient_meal_logged))
        {
            for($i=0; $i<7; $i++)
            {
                $final_weight_data[] = 0;
            }
        }

        $data = [
            'daily_meal_logged_data' => $final_weight_data,
            'weekly_day_name'        => $week_day_name,
        ];
        return $data;
    }


    private function calculate_patient_meal_source_daily($patient_id, $daily_start, $daily_end)
    {
        $utc_start  = $daily_start;
        $utc_end    = $daily_end;

        $patient_data   = Patient::where('patient_id',$patient_id)->first();
        $user_id        =  $patient_data->user_id;
        $user_data      = User::where('user_id',$user_id)->first();
        $offset_key     = $user_data->offset_key;

        if(!empty($offset_key))
        {
            $utc_start  = Carbon::createFromFormat('Y-m-d H:i:s', $utc_start,$offset_key)->setTimezone('UTC');
            $utc_end    = Carbon::createFromFormat('Y-m-d H:i:s', $utc_end,$offset_key)->setTimezone('UTC');
        }

        $patient_meal_source = collect($this->patient_model->get_patient_meal_source($patient_id, $utc_start, $utc_end));

        $actual     = array('Homemade','Store Bought','Dine Out');
        $plan       = array('Homemade','Store Bought','Dine Out');

        $actual_meal = array();
        $plan_meal   = array();

        foreach($actual as $val)
        {
            if($patient_meal_source->contains('meal_source', $val))
            {
                $meal_source    = $patient_meal_source->where('meal_source', $val)->all();
                $actual_meal[]  = count($meal_source);
            }
            else
                $actual_meal[] = 0;
        }

        foreach($plan as $val)
        {
            if($patient_meal_source->contains('planned_meal_source', $val))
            {
                $meal_source    = $patient_meal_source->where('planned_meal_source', $val)->all();
                $plan_meal[]    = count($meal_source);
            }
            else
                $plan_meal[] = 0;
        }

        $data = [
            'daily_meal_source_actual'    => $actual_meal,
            'daily_meal_source_plan'      => $plan_meal,
        ];
        return $data;
    }

    private function calculate_patient_meal_source_weekly($patient_id, $monthly_end, $monthly_start)
    {
        $utc_start  = $monthly_start;
        $utc_end    = $monthly_end;

        $patient_data   = Patient::where('patient_id',$patient_id)->first();
        $user_id        =  $patient_data->user_id;
        $user_data      = User::where('user_id',$user_id)->first();
        $offset_key     = $user_data->offset_key;

        if(!empty($offset_key))
        {
            $utc_start  = Carbon::createFromFormat('Y-m-d H:i:s', $utc_start,$offset_key)->setTimezone('UTC');
            $utc_end    = Carbon::createFromFormat('Y-m-d H:i:s', $utc_end,$offset_key)->setTimezone('UTC');
        }

        $patient_meal_source = collect($this->patient_model->get_patient_meal_source($patient_id, $utc_start, $utc_end));

        $actual     = array('Homemade','Store Bought','Dine Out');
        $plan       = array('Homemade','Store Bought','Dine Out');

        $actual_meal = array();
        $plan_meal   = array();

        foreach($actual as $val)
        {
            if($patient_meal_source->contains('meal_source', $val))
            {
                $meal_source    = $patient_meal_source->where('meal_source', $val)->all();
                $actual_meal[]  = count($meal_source);
            }
            else
                $actual_meal[] = 0;
        }

        foreach($plan as $val)
        {
            if($patient_meal_source->contains('planned_meal_source', $val))
            {
                $meal_source    = $patient_meal_source->where('planned_meal_source', $val)->all();
                $plan_meal[]    = count($meal_source);
            }
            else
                $plan_meal[] = 0;
        }

        $data = [
            'weekly_meal_source_actual'    => $actual_meal,
            'weekly_meal_source_plan'      => $plan_meal,
        ];
        return $data;
    }




    /**
     * @param $all_coaches
     *
     * @return array
     */
    private function _group_coachListBuilder( $all_coaches, $user_ids )
    {
        $foodCoach = [];
        $leadCoach = [];

        foreach ($all_coaches as $coach) {

            if(in_array($coach->user_id, $user_ids))
            {
                continue;
            }
            $providers     = [ ];
            $providersLead = [ ];
            if ($coach->user_type == "Coach" || $coach->user_type == "Food Coach") {
                $providersLead['full_name']     = @$coach->first_name . " " . @$coach->last_name;
                $providersLead['provider_id']   = $coach->provider_id;
                $providersLead['user_id']       = $coach->user_id;
                $providersLead['email']         = $coach->email;
            }

            if ( ! empty( $providersLead )) {
                $leadCoach[] = $providersLead;
            }
        }
        return [ 'lead_coach' => $leadCoach ];
    }

    public function add_new_coach_notes(Request $request)
    {
        if (request()->ajax()) {

            $validator = Validator::make(request()->input(), [
                'patient_id' => 'required',
                'add_coach_notes' => 'required',
            ]);

            if ($validator->fails()) {
                info('Add New Coach Notes Validation Failed');
                foreach ($validator->errors()->all() as $key => $value) {
                    return $response = [
                        'error' => $value,
                    ];
                    break;
                }
            }
            info('Add New Coach Validation Success');

            $user = $this->provider->where('user_id', $this->active_user_id)->first();

            $notes = new CoachNotes;
            if(!empty($request->ddNotifyCoaches))
            {
                $notes->notified_coaches    = implode(",", $request->ddNotifyCoaches);
            }
            // for url
            $url            = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
            $request->add_coach_notes   = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $request->add_coach_notes);
            // this regular expresion add span tag in @
            $request->add_coach_notes   = preg_replace('/@(\w+)/', '<span>@${1}</span> ', $request->add_coach_notes);
            $notes->notes               = $request->notify_type .' - '. $request->add_coach_notes;
            $notes->patient_id          = $request->patient_id;
            $notes->provider_id         = $user->provider_id;
            $notes->notes_date          = (time() * 1000);
            $notes->save();

            if (!$notes) {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'add new notes']),
                    'title'     => trans('common.add_new_notes_title')
                ]);
            }

            $all_coaches    = $this->coach_model->get_facility_coach_list(session('active_facility'));
            $resultArray    = $this->_group_coachListBuilder( $all_coaches, [$this->active_user_id] );
            $lead_coach     = collect($resultArray['lead_coach']);



            if(!empty($request->ddNotifyCoaches) || !empty($request->foodcoach_user_id))
            {
                if(!empty($request->foodcoach_user_id))
                {
                    $foodcoach_user = $this->user->where('user_id', $request->foodcoach_user_id)->first();

                    info('Sending Mail to User: '. $request->foodcoach_user_id );
                    $name = $foodcoach_user->first_name;
                    if(Session::get('user')->cobrandId == 2){
                        $link    = env('APP_BASE_URL_SOLERA');
                        $message = "New coach notes have been added for one of your members to view <a href='".$link."'>Click Here</a>";
                    }else{
                        $link    = env('APP_BASE_URL');
                        $message = "New coach notes have been added for one of your members to view <a href='".$link."'>Click Here</a>";
                    }
                    $body = ['name'=> $name, 'content' => $message,'subject' =>'New Coach Notes' ];
                    $to = $foodcoach_user->email;
                    $this->mailServiceHelper->sendEmail($body, trim($to));
                }

                if(!empty($request->ddNotifyCoaches))
                {
                   foreach ($request->ddNotifyCoaches as $coach_id)
                   {
                       $list_coach_id = '';
                       foreach ($lead_coach as $coach_ids)
                       {
                           if($coach_ids['provider_id'] == $coach_id)
                                $list_coach_id = $coach_ids['user_id'];
                       }

                       if($list_coach_id != '')
                       {
                           $coach_detail = $this->user->where('user_id', $list_coach_id)->first();
                           info('Sending Mail to User: '. $coach_id );
                           $name = $coach_detail->first_name;
                           if(Session::get('user')->cobrandId == 2){
                               $link    = env('APP_BASE_URL_SOLERA');
                               $message = "New coach notes have been added for one of your members to view <a href='".$link."'>Click Here</a>";
                           }else{
                               $link    = env('APP_BASE_URL');
                               $message = "New coach notes have been added for one of your members to view <a href='".$link."'>Click Here</a>";
                           }
                           $body = ['name'=> $name, 'content' => $message,'subject' =>'New Coach Notes' ];
                           $to   = $coach_detail->email;
                           $this->mailServiceHelper->sendEmail($body, trim($to));
                       }
                   }
                }
            }


            return $response = [
                'success' => trans('common.add_new_notes'),
                'title'     => trans('common.add_new_notes_title')
            ];
        }
    }



    public function get_coach_notes(Request $request)
    {
        $patient_id = $request->patient_id;
        $patient_coach_notes = $this->patient_model->get_coach_notes_info($patient_id);

        $timezone = '';
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }

        $coach_notes = array();
        if(!empty($patient_coach_notes))
        {
            $count = 0;
            foreach ($patient_coach_notes as $notes) {
                $count += 1;
                if($count <= 5)
                    continue;

                $notes_timestamp = '';
                if (!empty($notes->notes_date)) {
                    $no_date = ($notes->notes_date/1000);
                    $notes_date = date("Y-m-d H:i:s", $no_date);
                    $notes_timestamp = (!empty($timezone)) ? Carbon::createFromFormat('Y-m-d H:i:s', $notes_date)->timezone($timezone) : Carbon::createFromFormat('Y-m-d H:i:s', $notes_date);
                    $notes_timestamp = $notes_timestamp->diffForHumans();
                }else{
                    $notes_timestamp = '';
                }

                $array  = array(
                    'first_name'    =>  $notes->first_name,
                    'last_name'     =>  $notes->last_name,
                    'image_path'    =>  $notes->image_path,
                    'notes'         =>  $notes->notes,
                    'provider_full_name' => $notes->provider_full_name,
                    'notes_date'    =>  $notes_timestamp,
                );
                array_push($coach_notes, $array);
            }
        }
        $str = '';

        $image_url = env('APP_BASE_URL').'/img/default-user.png';
        if(!empty($coach_notes)) {
            foreach ($coach_notes as $notes) {
                $str .=  '<div class="item">
                            <div class="item-head">
                                <div class="item-details">
                                    <img class="item-pic rounded" src="'.$image_url.'">
                                    <a href="javascript:void(0)" class="item-name primary-link">'. $notes['provider_full_name'] .'</a>
                                    <span class="item-label"></span>
                                </div>
                                <span class="item-status">' . $notes['notes_date'] . '</span>
                            </div>
                            <div class="item-body">' . $notes['notes'] . '</div>
                        </div>';
            }
        }

        return $response = [
            'success' => $str,
        ];
    }


    public function get_all_goals_of_patient(Request $request)
    {
        $patient_id = $request->patient_id;
        $patient_goals_info = collect($this->patient_model->get_patient_goals_info($patient_id));
        $patient_goals_info = $patient_goals_info->sortByDesc('reminder_settings');
        $patient_goals = array();
        $week_days = array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun');

        foreach ($patient_goals_info as $value)
        {
            $reminder_settings = json_decode($value->reminder_settings);
            if(!empty($reminder_settings))
            {
                $reminder_days = 'Reminder: ';
                $reminder_time = '';
                foreach ($reminder_settings as $setting)
                {
                    $reminder_days .= ', ' . $week_days[$setting->dayOfWeek - 1]  ;
                    $reminder_time  = $setting->reminderTime;
                }
                $reminder_days = str_replace(': , ', ': ', $reminder_days);
            }
            else
            {
                $reminder_days = '';
                $reminder_time ='';
            }

            $array  =  (object) [
                'goal_name'             =>  $value->goal_name,
                'reminder_settings'     =>  $reminder_days,
                'reminder_time'         =>  $reminder_time,
                'is_notification_on'    =>  $value->is_notification_on,
                'patient_goal_id'      =>  $value->patient_goal_id,
                'goal_type'            =>   $value->goal_type,
            ];
            array_push($patient_goals, $array);
        }

        /* its for chart date */
        $now = Carbon::now();
        if($now->isMonday()){
            $last_week_0 = $now->copy()->startOfDay();
        }else{
            $last_week_0 = new Carbon('last monday');
        }
        $last_week_0;
        $last_week_1 = $last_week_0->copy()->subWeeks(1);
        $last_week_2 = $last_week_1->copy()->subWeeks(1);
        $last_week_3 = $last_week_2->copy()->subWeeks(1);

        $daily_end      = $now->copy()->startOfDay();
        $daily_start    = $daily_end->copy()->subDays(6);

        $monthly_end     = $now->copy()->startOfDay();
        $monthly_start   = $monthly_end->copy()->subWeeks(7);


        $str = '';

        if(!empty($patient_goals)) {
            foreach ($patient_goals as $goal) {
                 $active ='Deactivated';
                 $class = " ";
                 if($goal->reminder_settings!="") {
                     $active = 'Active';
                     $class = "badge-green-jungle";
                 }

                $actvity_class =  "iconhs-spoon font-green-jungle";
                 if( $goal->goal_type=="Physical Activity")
                 $actvity_class =  "iconhs-shoe font-yellow-crusta";


                $patient_goals_statics_info = $this->patient_model->get_patient_goals_statics_info($patient_id,$last_week_3,$goal->patient_goal_id);
                 $patient_goals_statics_info_array = array();
                if(!empty($patient_goals_statics_info)) {
                    foreach ($patient_goals_statics_info as $value) {
                        array_push($patient_goals_statics_info_array, date("Y-m-d", strtotime($value->goal_due_date_time)));
                    }
                }

                $str .= '<tr>
                            <td class="goals-icon" >
                                <div class=""><h3 class=""><i class=" '.$actvity_class.' "></i> </h3></div>
                            </td>
                            <td class="">
                                <div class="goal-details">
                                    <div class="font-md font-dark sbold"> '. $goal->goal_name .' </div>
                                    <div class="font-sm font-grey-mint"> '. $goal->reminder_settings .' </div>
                                    <div class="font-sm font-grey-salsa"><i class="fa fa-clock-o"></i> '. $goal->reminder_time .' </div>
                                        <div class=" badge '.$class.' font-sm font-white uppercase" > '.$active.' </div >
                                </div>
                            </td>
                            <td class="">
                                <div id="chart_div_new_195" class="chart_activity1">
                                    <div class="challenge-graph-single margin-top-10">
                                        <span class="badges">
                                            <span class="badge badge-total">'.count($patient_goals_statics_info_array).' checkins</span>
                                        </span>
                                        <span class="frequency-graph frequency-graph-weekly">
                                            <div class="grid">
                                                <ul>
                                                    <li  style="bottom:7px; '.((in_array($last_week_3->copy()->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").'"></li>
                                                    <li class="labels_li">'.$last_week_3->format('m/d').'</li>
                                                    <li  style="bottom:14px;'.((in_array($last_week_3->copy()->addDay(1)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:21px; '.((in_array($last_week_3->copy()->addDay(2)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:28px; '.((in_array($last_week_3->copy()->addDay(3)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:35px; '.((in_array($last_week_3->copy()->addDay(4)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:42px; '.((in_array($last_week_3->copy()->addDay(5)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:49px;'.((in_array($last_week_3->copy()->addDay(6)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                  
                                                </ul>
                                                <ul>
                                                    <li  style="bottom:7px; '.((in_array($last_week_2->copy()->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li class="labels_li">'.$last_week_2->format('m/d').'</li>
                                                    <li  style="bottom:14px; '.((in_array($last_week_2->copy()->addDay(1)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:21px; '.((in_array($last_week_2->copy()->addDay(2)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:28px; '.((in_array($last_week_2->copy()->addDay(3)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:35px; '.((in_array($last_week_2->copy()->addDay(4)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:42px; '.((in_array($last_week_2->copy()->addDay(5)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:49px; '.((in_array($last_week_2->copy()->addDay(6)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                </ul>
                                                <ul>
                                                    <li  style="bottom:7px; '.((in_array($last_week_1->copy()->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li class="labels_li">'.$last_week_1->format('m/d').'</li>
                                                    <li  style="bottom:14px; '.((in_array($last_week_1->copy()->addDay(1)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:21px; '.((in_array($last_week_1->copy()->addDay(2)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:28px; '.((in_array($last_week_1->copy()->addDay(3)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:35px; '.((in_array($last_week_1->copy()->addDay(4)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:42px; '.((in_array($last_week_1->copy()->addDay(5)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li  style="bottom:49px; '.((in_array($last_week_1->copy()->addDay(6)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                </ul>
                                                <ul>
                                                    <li style="bottom:7px; '.((in_array($last_week_0->copy()->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li class="labels_li">'.$last_week_0->format('m/d').'</li>
                                                    <li style="bottom:14px; '.((in_array($last_week_0->copy()->addDay(1)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li style="bottom:21px; '.((in_array($last_week_0->copy()->addDay(2)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li style="bottom:28px; '.((in_array($last_week_0->copy()->addDay(3)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li style="bottom:35px; '.((in_array($last_week_0->copy()->addDay(4)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li style="bottom:42px; '.((in_array($last_week_0->copy()->addDay(5)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                    <li style="bottom:49px; '.((in_array($last_week_0->copy()->addDay(6)->format('Y-m-d'), $patient_goals_statics_info_array))?'background-color:#69921f;':"").' "></li>
                                                </ul>
                                                <ul></ul>
                                            </div>
                                        </span>
                                    </div>
                                </div>
                            </td>
                    </tr>';

            }
        }

        return $response = [
            'success' => $str,
        ];
    }



    public function motivation_image_update(Request $request)
    {
        $user_id =  $this->active_user_id ;
        if($request->hasFile('uploadpostfile'))
        {
            $validator = Validator::make($request->all(), [
                'uploadpostfile' => 'mimes:jpg,jpeg,png'
            ]);

            if ($validator->fails())
            {
                return response()->json(
                    [
                        'error' => 'Please upload only image file'
                    ]
                );
            }
        }
        if (request()->ajax()) {
            $patient_id  =  $request->input('patient_id');
            $text        =  $request->input('post_description');

            if($request->hasFile('uploadpostfile'))
            {
                $file = $request->file('uploadpostfile');
                if($file) {
                    $destinationPath = config('healthslate.motivation_images_save_url');
                    $file_attachment_name = md5(time() . uniqid()) . '.' . $file->getClientOriginalExtension();
                    $file_name = $file_attachment_name;
                    $file->move($destinationPath, $file_attachment_name);
                    info('Motivation Image Saving Attached File to Folder: ' . $file_name);
                }

                $user = PatientMotivationImage::firstOrNew(array('patient_id' => $patient_id));
                $user->description      = $text;
                $user->image_name       = $file_name;
                $user->is_public        = 1;
                $user->save();
            }
            else{
                $user = PatientMotivationImage::firstOrNew(array('patient_id' => $patient_id));
                $user->description      = $text;
                $user->is_public        = 1;
                $user->save();
            }

            if(!empty($user))
            {
                return response()->json(
                    [
                        'success' => trans('common.motivation_image_add'),
                        'title'   => trans('common.motivation_image_add_title'),
                    ]
                );
            }
            else
            {
                return response()->json(
                    [
                        'error' => trans('common.unable_to_do_action', [ 'action' => trans('common.motivation_image_add_title')]),
                        'title'     => trans('common.motivation_image_add_title')
                    ]
                );
            }

        }
    }


    public function user_profile_image_update(Request $request)
    {
        if($request->hasFile('upload_user_image'))
        {
            $validator = Validator::make($request->all(), [
                'upload_user_image' => 'mimes:jpg,jpeg,png'
            ]);

            if ($validator->fails())
            {
                return response()->json(
                    [
                        'error' => 'Please upload only image file'
                    ]
                );
            }
        }
        if (request()->ajax()) {
            $patient_id  =  $request->input('patient_id');

            if($request->hasFile('upload_user_image'))
            {
                $file = $request->file('upload_user_image');
                if($file) {
                    $destinationPath = config('healthslate.user_profile_images_save_url');
                    $file_attachment_name = md5(time() . uniqid()) . '.' . $file->getClientOriginalExtension();
                    $file_name = '/profileImages/'. $file_attachment_name;
                    $file->move($destinationPath, $file_attachment_name);
                    info('User Profile Image Saving Attached File to Folder: ' . $file_name);
                }

                if($file_name != '')
                {
                    $data =  Patient::find($patient_id);
                    $data->image_path   = $file_name;
                    $data->update();
                }
            }

            if(!empty($data))
            {
                return response()->json(
                    [
                        'success' => trans('common.user_profile_image_add'),
                        'title'   => trans('common.user_profile_image_add_title'),
                    ]
                );
            }
            else
            {
                return response()->json(
                    [
                        'error' => trans('common.unable_to_do_action', [ 'action' => trans('common.user_profile_image_add_title')]),
                        'title' => trans('common.user_profile_image_add_title')
                    ]
                );
            }

        }
    }




    public function get_patient_weight_summary_chart_data(Request $request)
    {
        if (request()->ajax()) {
            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $clicked        =  $request->input('clicked');

            $date = explode(' - ', $date);
            $start  = Carbon::createFromFormat( 'm/d/Y', $date[0] )->setTime( 0, 0 );
            $end    = Carbon::createFromFormat( 'm/d/Y', $date[1] )->setTime( 0, 0 );

            if($clicked == 'right')
            {
                $daily_start  = $start->copy()->addWeek(1);
                $daily_end    = $end->copy()->addWeek(1);
            }
            else
            {
                $daily_start  = $start->copy()->subWeek(1);
                $daily_end    = $end->copy()->subWeek(1);
            }
            $patient_weight_daily  = $this->calculate_patient_weight_daily($patient_id, $daily_start, $daily_end);

            if(empty($patient_weight_daily))
            {
                return response()->json(
                    [
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Update Chart Data']),
                        'title' => 'Weight Summary Chart'
                    ]
                );
            }

            return response()->json(
                [
                    'success'   => $patient_weight_daily,
                    'start'     => $daily_start->format('m/d/Y'),
                    'end'       => $daily_end->format('m/d/Y'),
                ]
            );
        }
    }

    public function get_weekly_weigh_summary_chart_data(Request $request)
    {
        if (request()->ajax()) {
            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $clicked        =  $request->input('clicked');

            $date = explode(' - ', $date);
            $start  = Carbon::createFromFormat( 'm/d/Y', $date[0] )->setTime( 0, 0 );
            $end    = Carbon::createFromFormat( 'm/d/Y', $date[1] )->setTime( 0, 0 );

            if($clicked == 'right')
            {
                $daily_start  = $start->copy()->addWeek(7);
                $daily_end    = $end->copy()->addWeek(7);
            }
            else
            {
                $daily_start  = $start->copy()->subWeek(7);
                $daily_end    = $end->copy()->subWeek(7);
            }
            $patient_weight_daily  = $this->calculate_patient_weight_weekly($patient_id, $daily_end, $daily_start);

            if(empty($patient_weight_daily))
            {
                return response()->json(
                    [
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Update Chart Data']),
                        'title' => 'Weight Summary Chart'
                    ]
                );
            }

            return response()->json(
                [
                    'success'   => $patient_weight_daily,
                    'start'     => $daily_start->format('m/d/Y'),
                    'end'       => $daily_end->format('m/d/Y'),
                ]
            );
        }
    }


    public function get_meals_logged_chart_data(Request $request)
    {
        if (request()->ajax()) {
            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $clicked        =  $request->input('clicked');

            $date = explode(' - ', $date);
            $start  = Carbon::createFromFormat( 'm/d/Y', $date[0] )->setTime( 0, 0 );
            $end    = Carbon::createFromFormat( 'm/d/Y', $date[1] )->setTime( 0, 0 );

            if($clicked == 'right')
            {
                $daily_start  = $start->copy()->addWeek(1);
                $daily_end    = $end->copy()->addWeek(1);
            }
            else
            {
                $daily_start  = $start->copy()->subWeek(1);
                $daily_end    = $end->copy()->subWeek(1);
            }
            $patient_weight_daily  = $this->calculate_patient_meal_logged_daily($patient_id, $daily_start, $daily_end);

            if(empty($patient_weight_daily))
            {
                return response()->json(
                    [
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Update Chart Data']),
                        'title' => 'Meal Logged Chart'
                    ]
                );
            }

            return response()->json(
                [
                    'success'   => $patient_weight_daily,
                    'start'     => $daily_start->format('m/d/Y'),
                    'end'       => $daily_end->format('m/d/Y'),
                ]
            );
        }
    }

    public function get_weekly_meals_logged_chart_data(Request $request)
    {
        if (request()->ajax()) {
            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $clicked        =  $request->input('clicked');

            $date = explode(' - ', $date);
            $start  = Carbon::createFromFormat( 'm/d/Y', $date[0] )->setTime( 0, 0 );
            $end    = Carbon::createFromFormat( 'm/d/Y', $date[1] )->setTime( 0, 0 );

            if($clicked == 'right')
            {
                $daily_start  = $start->copy()->addWeek(7);
                $daily_end    = $end->copy()->addWeek(7);
            }
            else
            {
                $daily_start  = $start->copy()->subWeek(7);
                $daily_end    = $end->copy()->subWeek(7);
            }
            $patient_weight_daily  = $this->calculate_patient_meal_logged_weekly($patient_id, $daily_end, $daily_start);

            if(empty($patient_weight_daily))
            {
                return response()->json(
                    [
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Update Chart Data']),
                        'title' => 'Weight Summary Chart'
                    ]
                );
            }

            return response()->json(
                [
                    'success'   => $patient_weight_daily,
                    'start'     => $daily_start->format('m/d/Y'),
                    'end'       => $daily_end->format('m/d/Y'),
                ]
            );
        }
    }


    public function get_meal_source_chart_data(Request $request)
    {
        if (request()->ajax()) {
            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $clicked        =  $request->input('clicked');

            $date = explode(' - ', $date);
            $start  = Carbon::createFromFormat( 'm/d/Y', $date[0] )->setTime( 0, 0 );
            $end    = Carbon::createFromFormat( 'm/d/Y', $date[1] )->setTime( 0, 0 );

            if($clicked == 'right')
            {
                $daily_start  = $start->copy()->addWeek(1);
                $daily_end    = $end->copy()->addWeek(1);
            }
            else
            {
                $daily_start  = $start->copy()->subWeek(1);
                $daily_end    = $end->copy()->subWeek(1);
            }
            $meal_source  = $this->calculate_patient_meal_source_daily($patient_id, $daily_start, $daily_end);

            if(empty($meal_source))
            {
                return response()->json(
                    [
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Update Chart Data']),
                        'title' => 'Meal Logged Chart'
                    ]
                );
            }

            return response()->json(
                [
                    'success'   => $meal_source,
                    'start'     => $daily_start->format('m/d/Y'),
                    'end'       => $daily_end->format('m/d/Y'),
                ]
            );
        }
    }

    public function get_weekly_meal_source_chart_data(Request $request)
    {
        if (request()->ajax()) {
            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $clicked        =  $request->input('clicked');

            $date = explode(' - ', $date);
            $start  = Carbon::createFromFormat( 'm/d/Y', $date[0] )->setTime( 0, 0 );
            $end    = Carbon::createFromFormat( 'm/d/Y', $date[1] )->setTime( 0, 0 );

            if($clicked == 'right')
            {
                $daily_start  = $start->copy()->addWeek(7);
                $daily_end    = $end->copy()->addWeek(7);
            }
            else
            {
                $daily_start  = $start->copy()->subWeek(7);
                $daily_end    = $end->copy()->subWeek(7);
            }
            $meal_source  = $this->calculate_patient_meal_source_weekly($patient_id, $daily_end, $daily_start);

            if(empty($meal_source))
            {
                return response()->json(
                    [
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Update Chart Data']),
                        'title' => 'Weight Summary Chart'
                    ]
                );
            }

            return response()->json(
                [
                    'success'   => $meal_source,
                    'start'     => $daily_start->format('m/d/Y'),
                    'end'       => $daily_end->format('m/d/Y'),
                ]
            );
        }
    }



    public function get_physical_activity_steps_daily(Request $request)
    {
        if (request()->ajax()) {
            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $clicked        =  $request->input('clicked');

            $date = explode(' - ', $date);
            $start  = Carbon::createFromFormat( 'm/d/Y', $date[0] )->setTime( 0, 0 );
            $end    = Carbon::createFromFormat( 'm/d/Y', $date[1] )->setTime( 0, 0 );

            if($clicked == 'right')
            {
                $daily_start  = $start->copy()->addWeek(1);
                $daily_end    = $end->copy()->addWeek(1);
            }
            else
            {
                $daily_start  = $start->copy()->subWeek(1);
                $daily_end    = $end->copy()->subWeek(1);
            }

            $steps      = $this->calculate_physical_activity_steps_daily($patient_id, $daily_start, $daily_end);
            $minute     = $this->calculate_physical_activity_minutes_daily($patient_id, $daily_start, $daily_end);

            if(empty($steps) && empty($minute))
            {
                return response()->json(
                    [
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Physical Activity Chart Data']),
                        'title' => 'Physical Activity Chart'
                    ]
                );
            }

            return response()->json(
                [
                    'success'   => $steps,
                    'minute'    => $minute,
                    'start'     => $daily_start->format('m/d/Y'),
                    'end'       => $daily_end->format('m/d/Y'),
                ]
            );
        }
    }


    public function get_physical_activity_weekly(Request $request)
    {
        if (request()->ajax()) {
            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $clicked        =  $request->input('clicked');

            $date = explode(' - ', $date);
            $start  = Carbon::createFromFormat( 'm/d/Y', $date[0] )->setTime( 0, 0 );
            $end    = Carbon::createFromFormat( 'm/d/Y', $date[1] )->setTime( 0, 0 );

            if($clicked == 'right')
            {
                $daily_start  = $start->copy()->addWeek(7);
                $daily_end    = $end->copy()->addWeek(7);
            }
            else
            {
                $daily_start  = $start->copy()->subWeek(7);
                $daily_end    = $end->copy()->subWeek(7);
            }
            $steps      = $this->calculate_physical_activity_steps_weekly($patient_id, $daily_end, $daily_start);
            $minute     = $this->calculate_physical_activity_minutes_weekly($patient_id, $daily_end, $daily_start);

            if(empty($steps) && empty($minute))
            {
                return response()->json(
                    [
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Physical Activity Chart Data']),
                        'title' => 'Physical Activity Chart'
                    ]
                );
            }

            return response()->json(
                [
                    'success'   => $steps,
                    'minute'    => $minute,
                    'start'     => $daily_start->format('m/d/Y'),
                    'end'       => $daily_end->format('m/d/Y'),
                ]
            );
        }
    }

    public function get_patient_engagement_daily($uuid,$daily_start, $daily_end)
    {
        $timezone = '';
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }

        if (!empty($timezone)) {
            $utc_start = Carbon::createFromFormat('Y-m-d H:i:s', $daily_start, $timezone)->setTimezone('UTC');
            $utc_end = Carbon::createFromFormat('Y-m-d H:i:s', $daily_end, $timezone)->setTimezone('UTC');
        }

        $daily_date = array();
        $temp_date = '';
        for($i=0; $i<7; $i++)
        {
            if($i == 0)
            {
                $week_day_name[] = $daily_end->format('D');
                $week_day_date[] = $daily_end->format('Y-m-d');
                $temp_date       = $daily_end;
            }
            else
            {
                $week_day_name[]    = $temp_date->copy()->subDay(1)->format('D');
                $week_day_date[]    = $temp_date->copy()->subDay(1)->format('Y-m-d');
                $temp_date          = $temp_date->copy()->subDay(1);
            }
        }
        $week_day_date = array_reverse($week_day_date);
        $base_url = config('rest.GET_engagement_users_report_by_id');
        $request = [
            'patient_id' =>$uuid,// '35a70a77-42b2-4160-bd05-c1f829c7fc54',
            'full' => true,
        ];
        $whole_data=collect();
        $engagement_data = $this->curriculumServiceProvider->requestEngegament($base_url, $request);
        //print_r($engagement_data);die;
        if(array_key_exists("errorMessage",$engagement_data)){
            $whole_data = array();
        }else{
            foreach ($engagement_data as $eng) {
                $date = date('Y-m-d', strtotime($eng->time));
                $engagement_row=[
                    'time'=>$date,
                    'engagement'=>$eng->engagement->this_day,
                ];

                $whole_data = $whole_data->push($engagement_row);
                $whole_data->toArray();
            }
        }
        //print_r($whole_data);die;


        if(!empty($whole_data))
        {
            $this_day = array();
            $patient_engagement_data = collect($whole_data);

            foreach ($week_day_date as $date)
            {
                if($patient_engagement_data->contains('time', $date))
                {
                    $eng_data = $patient_engagement_data->where('time', $date)->first();
                    $this_day[] = round($eng_data['engagement'],0);
                }
                else
                    $this_day[] = 0;
            }

        }
        if (empty($whole_data)) {
            for ($i = 0; $i < 7; $i++) {
                $this_day[] = 0;
            }
        }
        $data = ['engagement' => $this_day];
        return $data;

    }

    public function get_patient_engagement_weekly($uuid,$monthly_start, $monthly_end)
    {
        $timezone = '';
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }

        if (!empty($timezone)) {
            $utc_start = Carbon::createFromFormat('Y-m-d H:i:s', $monthly_start, $timezone)->setTimezone('UTC');
            $utc_end = Carbon::createFromFormat('Y-m-d H:i:s', $monthly_end, $timezone)->setTimezone('UTC');
        }

        $daily_date = array();
        $temp_date = '';
        for($i=0; $i<7; $i++)
        {
            if($i == 0)
            {
                if($monthly_end->isMonday()){
                    $last_week_0 = $monthly_end->copy()->startOfDay();
                }else{
                    $last_week_0 = $monthly_end->copy()->startOfWeek();
                }

                $week_day_name[] = $last_week_0->format('m/d');
                $week_day_date[] = $last_week_0->format('Y-m-d');
                $temp_date       = $last_week_0;
            }
            else
            {
                $week_day_name[]    = $temp_date->copy()->subWeek(1)->format('m/d');
                $week_day_date[]    = $temp_date->copy()->subWeek(1)->format('Y-m-d');
                $temp_date          = $temp_date->copy()->subWeek(1);
            }
        }

        $week_day_name = array_reverse($week_day_name);
        $week_day_date = array_reverse($week_day_date);

        $base_url = config('rest.GET_engagement_users_report_by_id');
        $request = [
            'patient_id' => $uuid,//'35a70a77-42b2-4160-bd05-c1f829c7fc54',
            'full' => true,
        ];
        $whole_data=collect();
        $engagement_data = $this->curriculumServiceProvider->requestEngegament($base_url, $request);
        if(array_key_exists("errorMessage",$engagement_data)){
            $whole_data = array();
        }else{
            foreach ($engagement_data as $eng) {
                $date = date('Y-m-d', strtotime($eng->time));
                $engagement_row=[
                    'time'=>$date,
                    'engagement'=>$eng->engagement->this_day,
                ];

                $whole_data = $whole_data->push($engagement_row);
                $whole_data->toArray();
            }
        }

        if(!empty($whole_data))
        {
            $this_day = array();
            $patient_engagement_data = collect($whole_data);

            foreach ($week_day_date as $date)
            {
                if($patient_engagement_data->contains('time', $date))
                {
                    $eng_data = $patient_engagement_data->where('time', $date)->first();
                    $this_day[] = round($eng_data['engagement'],0);
                }
                else
                    $this_day[] = 0;
            }

        }
        if (empty($whole_data)) {
            for ($i = 0; $i < 7; $i++) {
                $this_day[] = 0;
            }
        }
        $data = [
            'engagement' => $this_day,
            'weekly_day_name'=>$week_day_name,
        ];
        return $data;

    }

    public function get_daily_engagement_data(Request $request)
    {
        if (request()->ajax()) {
            $timezone = '';
            if (isset($_COOKIE['user_timezone'])) {
                $timezone = $_COOKIE['user_timezone'];
            }

            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $clicked        =  $request->input('clicked');

            $date = explode(' - ', $date);
            $start  = Carbon::createFromFormat( 'm/d/Y', $date[0] )->setTime( 0, 0 );
            $end    = Carbon::createFromFormat( 'm/d/Y', $date[1] )->setTime( 0, 0 );

            if($clicked == 'right')
            {
                $daily_start  = $start->copy()->addWeek(1);
                $daily_end    = $end->copy()->addWeek(1);
            }
            else
            {
                $daily_start  = $start->copy()->subWeek(1);
                $daily_end    = $end->copy()->subWeek(1);
            }

            $daily_date = array();
            $temp_date = '';
            for($i=0; $i<7; $i++)
            {
                if($i == 0)
                {
                    $week_day_name[] = $daily_end->format('D');
                    $week_day_date[] = $daily_end->format('Y-m-d');
                    $temp_date       = $daily_end;
                }
                else
                {
                    $week_day_name[]    = $temp_date->copy()->subDay(1)->format('D');
                    $week_day_date[]    = $temp_date->copy()->subDay(1)->format('Y-m-d');
                    $temp_date          = $temp_date->copy()->subDay(1);
                }
            }
            $week_day_date = array_reverse($week_day_date);
            $base_url = config('rest.GET_engagement_users_report_by_id');
            $request = [
                'patient_id' => $patient_id,//'35a70a77-42b2-4160-bd05-c1f829c7fc54',
                'full' => true,
            ];
            $whole_data=collect();
            $engagement_data = $this->curriculumServiceProvider->requestEngegament($base_url, $request);

            if(array_key_exists("errorMessage",$engagement_data)){
                $whole_data = array();
            }else{
                foreach ($engagement_data as $eng) {
                    $date = date('Y-m-d', strtotime($eng->time));
                    $engagement_row=[
                        'time'=>$date,
                        'engagement'=>$eng->engagement->this_day,
                    ];

                    $whole_data = $whole_data->push($engagement_row);
                    $whole_data->toArray();
                }
            }

            if(!empty($whole_data))
            {
                $this_day = array();
                $patient_engagement_data = collect($whole_data);

                foreach ($week_day_date as $date)
                {
                    if($patient_engagement_data->contains('time', $date))
                    {
                        $eng_data = $patient_engagement_data->where('time', $date)->first();
                        $this_day[] = round($eng_data['engagement'],0);
                    }
                    else
                        $this_day[] = 0;
                }

            }
            if (empty($whole_data)) {
                for ($i = 0; $i < 7; $i++) {
                    $this_day[] = 0;
                }
            }

            if(empty($this_day))
            {
                return response()->json(
                    [
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Update Chart Data']),
                        'title' => 'Engagement Chart'
                    ]
                );
            }

            return response()->json(
                [
                    'success'   => $this_day,
                    'start'     => $daily_start->format('m/d/Y'),
                    'end'       => $daily_end->format('m/d/Y'),
                ]
            );
        }
    }

    public function get_weekly_engagement_data(Request $request)
    {
        if (request()->ajax()) {
            $patient_id     =  $request->input('patient_id');
            $date           =  $request->input('date');
            $clicked        =  $request->input('clicked');

            $date = explode(' - ', $date);
            $start  = Carbon::createFromFormat( 'm/d/Y', $date[0] )->setTime( 0, 0 );
            $end    = Carbon::createFromFormat( 'm/d/Y', $date[1] )->setTime( 0, 0 );

            if($clicked == 'right')
            {
                $monthly_start  = $start->copy()->addWeek(7);
                $monthly_end    = $end->copy()->addWeek(7);
            }
            else
            {
                $monthly_start  = $start->copy()->subWeek(7);
                $monthly_end    = $end->copy()->subWeek(7);
            }

            $daily_date = array();
            $temp_date = '';
            for($i=0; $i<7; $i++)
            {
                if($i == 0)
                {
                    if($monthly_end->isMonday()){
                        $last_week_0 = $monthly_end->copy()->startOfDay();
                    }else{
                        $last_week_0 = $monthly_end->copy()->startOfWeek();
                    }

                    $week_day_name[] = $last_week_0->format('m/d');
                    $week_day_date[] = $last_week_0->format('Y-m-d');
                    $temp_date       = $last_week_0;
                }
                else
                {
                    $week_day_name[]    = $temp_date->copy()->subWeek(1)->format('m/d');
                    $week_day_date[]    = $temp_date->copy()->subWeek(1)->format('Y-m-d');
                    $temp_date          = $temp_date->copy()->subWeek(1);
                }
            }

            $week_day_name = array_reverse($week_day_name);
            $week_day_date = array_reverse($week_day_date);

            $base_url = config('rest.GET_engagement_users_report_by_id');
            $request = [
                'patient_id' =>$patient_id,// '35a70a77-42b2-4160-bd05-c1f829c7fc54',
                'full' => true,
            ];
            $whole_data=collect();
            $engagement_data = $this->curriculumServiceProvider->requestEngegament($base_url, $request);
            if(array_key_exists("errorMessage",$engagement_data)){
                $whole_data = array();
            }else{
                foreach ($engagement_data as $eng) {
                    $date = date('Y-m-d', strtotime($eng->time));
                    $engagement_row=[
                        'time'=>$date,
                        'engagement'=>$eng->engagement->this_day,
                    ];

                    $whole_data = $whole_data->push($engagement_row);
                    $whole_data->toArray();
                }
            }

            if(!empty($whole_data))
            {
                $this_day = array();
                $patient_engagement_data = collect($whole_data);

                foreach ($week_day_date as $date)
                {
                    if($patient_engagement_data->contains('time', $date))
                    {
                        $eng_data = $patient_engagement_data->where('time', $date)->first();
                        $this_day[] = round($eng_data['engagement'],0);
                    }
                    else
                        $this_day[] = 0;
                }

            }
            if (empty($whole_data)) {
                for ($i = 0; $i < 7; $i++) {
                    $this_day[] = 0;
                }
            }

            if(empty($this_day))
            {
                return response()->json(
                    [
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Update Chart Data']),
                        'title' => 'Engagement Chart'
                    ]
                );
            }

            return response()->json(
                [
                    'success'   => $this_day,
                    'start'     => $monthly_start->format('m/d/Y'),
                    'end'       => $monthly_end->format('m/d/Y'),
                ]
            );
        }
    }



}//End of Class

<?php

namespace HealthSlatePortal\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CurriculumServiceProvider;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\PatientModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\Patient;
use HealthSlatePortal\Models\Eloquent\Target;
use HealthSlatePortal\Models\Eloquent\TargetMeal;
use Illuminate\Support\Facades\Log;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\PatientHideUnhide;
use HealthSlatePortal\Models\Eloquent\PatientDietaryInfo;
use HealthSlatePortal\Models\Eloquent\PatientDiabetesInfo;
use HealthSlatePortal\Models\Eloquent\UserMessagePreference;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\Reminder;
use HealthSlatePortal\Models\Eloquent\Device;
use HealthSlatePortal\Models\Eloquent\Message;
use HealthSlatePortal\Models\Eloquent\SessionAttendance;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\DashboardSettingModel;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use Edujugon\PushNotification\PushNotification;
use HealthSlatePortal\Helpers\MailServiceHelper;
use DB;
use Input;
use Validator;
use Session;
use Hash;


class DashboardSetting extends Controller {

    protected $facility_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $user;
    protected $dataTableService;
    protected $group_model;
    protected $group_member;
    protected $curriculumServiceProvider;
    protected $mailServiceHelper;
    protected  $dashboard_setting_model;
    protected $patient_model;
    protected $data = array();

    function __construct(PatientModel $patient_model, FacilityModel $facility_model, MemberModel $member_model, Facility $facility, CoacheModel $coach_model,
                          DataTableServiceProvider $dataTableService, User $user, GroupModel $group_model, GroupMember $group_member, CurriculumServiceProvider $curriculumServiceProvider,MailServiceHelper $mailServiceHelper,DashboardSettingModel $dashboard_setting_model) {
        $this->facility_model       = $facility_model;
        $this->dashboard_setting_model = $dashboard_setting_model;
        $this->patient_model       = $patient_model;
        $this->coach_model          = $coach_model;
        $this->member_model         = $member_model;
        $this->group_member         = $group_member;
        $this->group_model          = $group_model;
        $this->facility             = $facility;
        $this->user                 = $user;
        $this->dataTableService     = $dataTableService;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->mailServiceHelper = $mailServiceHelper;
        $this->group_db             = env('GROUP_DB_DATABASE');
        $this->cobrand_id       = Session::get('user')->cobrandId;
        $this->active_facility_id   = session('active_facility');
        $this->active_user_id       = session('userId');
        $this->active_user_role     = session('userRole');
        $this->data['highchart_js'] = array('highcharts','highcharts-3d','highcharts-more');
        $this->data['common_js']    = array('setting','dashboard');
       // $this->data['common_js']    = array('dashboard');


    }


    function index($patient_id)
    {

        $this->data['patient_id']   = $patient_id;
        $patient_info = $this->patient_model->get_patient_info($patient_id, $this->active_facility_id);

        $this->data['patient_info'] = $patient_info;
        $this->data['group_link'] = "";
        $this->data['session'] = "" ;
        $this->data['total_post']='';

        if(empty($patient_info->patient_id))
        {
            return redirect()->intended('/'.$this->active_user_role.'/members');
        }

        if($patient_info->group_id !="" )
        {
            $group_id   = array($patient_info->group_id);
            $is_person  = $patient_info->is_person;

            $completed_week         = $patient_info->completed_week;
            $group_session_detail   = collect($this->group_model->get_group_current_session_name($group_id));

            $curriculum_for_dpp     = '';
            $curriculum_for_dpp2    = '';

            if ($is_person == 1) {
                // Calling Curriculam server for Section list for both type DPP and DPP2
                $curriculum_program_type_name = $group_session_detail->unique('curriculum_program_type_name')->pluck('curriculum_program_type_name' )->toArray();
                //print_r(($curriculum_program_type_name)); exit;
                if(count($curriculum_program_type_name) > 0)
                {
                    foreach ($curriculum_program_type_name as $value)
                    {
                        if($value == 'DPP2' || $value == 'DPP')
                        {
                            $curriculum = [
                                (object) [ 'uuid' => 'inperson', 'curriculum_program_type_name' => $value],
                            ];

                            if($value == 'DPP')
                                $curriculum_for_dpp  = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                            if($value == 'DPP2')
                                $curriculum_for_dpp2 = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                        }
                    }
                }
            }

            if($completed_week == 0)
                $completed_week = 1;
            $total_sessions  = array();
            if($is_person != 1)
                $group_link = route('leadcoach.group-detail', ['group_id' => $patient_info->group_id]);
            else
            {
                $group_link = route('leadcoach.group-session', ['group_id' => $patient_info->group_id]);
                $week_group = 0;

                $current_session_name = '';
                if(count($group_session_detail) > 0)
                {
                    $group_current_session = $group_session_detail->where('group_id', $patient_info->group_id)->last();
                    //print_r($group_current_session);
                    if(!empty($group_current_session->topic_id))
                    {
                        if($curriculum_for_dpp != '')
                        {
                            $total_sessions = $curriculum_for_dpp;
                            $current_session_name = $curriculum_for_dpp->where('section_uuid', $group_current_session->topic_id)->toArray();
                        }

                        if($curriculum_for_dpp2 != '')
                        {
                            $total_sessions = $curriculum_for_dpp2;
                            $current_session_name = $curriculum_for_dpp2->where('section_uuid', $group_current_session->topic_id)->toArray();
                        }


                        if(!empty($current_session_name))
                        {
                            foreach ($current_session_name as $val)
                                $week_group =  $val->display_week;
                        }
                    }

                }
            }

            $this->data['group_link']  = $group_link;
            if($is_person == 1)
                $this->data['session'] = $week_group."/".count($total_sessions) ;
            else
                $this->data['session'] = "NA" ;

            // Batch Count Start
            $read_count = GroupMember::where('group_id', $group_id)->where('user_id',$this->active_user_id )->first();
            if(!empty($read_count))
            {
                $post_read_time =  $read_count->post_read_time;
                $user_id= $this->active_user_id;
                $group_post = $this->group_model->get_post_read_count($group_id,$post_read_time,$user_id);
            }
            if(!empty($group_post)){
                $count_post=$group_post[0]->total_post;
                if($count_post==0){
                    $count_post='';
                }
            }
            else{
                $count_post='';
            }
            $this->data['total_post'] = $count_post;
            // Batch Count End
        }
        $this->data['user_role'] = $this->active_user_role ;
        $this->data['state']  = state();
        $this->data['profile_detail'] = $this->dashboard_setting_model->getProfileDetail($patient_id);
        $reminder_detail = $this->dashboard_setting_model->getReminderDetail($patient_id);

        if(!empty($reminder_detail))
        {
            foreach ($reminder_detail as $value) {
                if ($value->meal_type == "Snack")
                    $this->data['Snack'] = $value->meal_time;
                if ($value->meal_type == "Dinner")
                    $this->data['Dinner'] = $value->meal_time;
                if ($value->meal_type == "Breakfast")
                    $this->data['Breakfast'] = $value->meal_time;
                if ($value->meal_type == "Lunch")
                    $this->data['Lunch'] = $value->meal_time;
                $this->data['is_snack_time'] = $value->is_snack_time;
                $this->data['is_breakfast_time'] = $value->is_breakfast_time;
                $this->data['is_lunch_time'] = $value->is_lunch_time;
                $this->data['is_dinner_time'] = $value->is_dinner_time;
                $this->data['weight_time']  = $value->weight_time;
                $this->data['time_of_day']  = $value->time_of_day;
                $this->data['day_of_week']  = $value->day_of_week;
                $this->data['date_of_month']  = $value->date_of_month;
                $this->data['type_of_reminder']  = $value->type_of_reminder;
                $this->data['sms']  = $value->sms;
                $this->data['email']  = $value->email;
                $this->data['is_in_app_message_notify']  = $value->is_in_app_message_notify;
                $this->data['is_coach_communication_reminder']  = $value->is_coach_communication_reminder;
                $this->data['is_meal_time']  = $value->is_meal_time;
                $this->data['is_weight_time']  = $value->is_weighing_myself;
                $this->data['target_id']  = $value->target_id;
            }
        }

         $this->data['facility_list']= $this->coach_model->get_all_facility_list_for_admin($this->cobrand_id);
        //print_r($this->data['reminder_detail']);


        return view( 'common.setting', $this->data );
    }

    function setting()
    {


        return view( 'common.setting', $this->data );

    }

    public function updateMemberProfile(Request $request)
    {

        if (request()->ajax()) {
            $registration_id ="";
            $device_type ="";

          $validator = Validator::make(request()->input(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'email' => 'required|unique:users,email,' . $request->user_id . ',user_id',
                'phone' => 'required',
                'temp_password' => 'min:6',
                'dob' => 'required',
                'gender' => 'required',
                'mrn' => 'required',
                'group_display_name' => 'required',
                'height_feet' => 'required',
                'height_inch' => 'required',
                'activity' => 'required',
                'starting_weight' => 'required',
                 'calorie_budget' => 'required',

            ]);

            if ($validator->fails()) {
                info('update member Validation Failed');
                foreach ($validator->errors()->all() as $key => $value) {
                    return $response = [
                        'error' => $value,
                    ];
                    break;
                }
            }


            $user = User::find($request->user_id);
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->city = $request->city;
            $user->state = $request->state;
            $user->zip_code = $request->zip;
            /* update temp password for patient*/
            $password=$request->temp_password;
            $bcrypt_password_get = Hash::make($password);
            $bcrypt_password_remove_char = substr($bcrypt_password_get, 3);
            $bcrypt_password = "$2a".$bcrypt_password_remove_char;
            $user->temp_password =$bcrypt_password ;


            if ($request->group_visibility == 1)
                $user->group_visibility = "Private";
            else
                $user->group_visibility = "Public";
            $user->group_display_name = $request->group_display_name;
            $user->address = $request->address1;

            $user->update();

            $patient = patient::find($request->patient_id);
            $patient->gender = $request->gender;
            $patient->dob = date("Y-m-d", strtotime($request->dob));
            //$patient->nickname = $request->nick_name;
            $patient->mail_address2 = $request->address2;
            $patient->education = $request->education;
            $patient->state = $request->state;
            $patient->language = $request->language;
            $patient->mrn = $request->mrn;
            if($this->active_user_role == "admin")
            {
                if($request->facility)
                  $patient->facility_id = $request->facility;

            }
            $patient->height_inch = $request->height_inch;
            $patient->height_feet = $request->height_feet;
            $patient->update();

            $target = Target::where('patient_id', $request->patient_id)->first();
            if (isset($target->target_id)) {
                $target->activity_steps = $request->activity;
                $target->starting_weight = $request->starting_weight;
                $target->update();
            } else {
                $target = new Target;
                $target->patient_id = $request->patient_id;
                $target->activity_steps = $request->activity;
                $target->starting_weight = $request->starting_weight;
                $target->save();

            }

            $target_id = $target->target_id;
            $target_meal = TargetMeal::where('target_id', $target_id)->where('meal_type', 'Daily')->first();
            if (isset($target_meal->meal_target_id)) {
                $target_meal->daily_calorie_budget = $request->calorie_budget;
                $target_meal->update();
            } else {
                $target_meal = new TargetMeal;
                $target_meal->daily_calorie_budget = $request->calorie_budget;
                $target_meal->target_id = $target_id;
                $target_meal->meal_type = "Daily";

                $target_meal->save();

            }


            $dietary_restrictions = "";
            $meal_preparer = "";
            $pateint_dietary_info = PatientDietaryInfo::where('patient_id', $request->patient_id)->first();


            if (isset($pateint_dietary_info->dietary_info_id)) {

                if (!empty($request->dieatry_restriction)) {
                    $dietary_restrictions = implode(", ", $request->dieatry_restriction);
                    $dietary_restrictions = rtrim($dietary_restrictions, ', ');
                    $pateint_dietary_info->dietary_restrictions = $dietary_restrictions;
                }
                else
                {
                    $pateint_dietary_info->dietary_restrictions = $dietary_restrictions;
                }

                if (!empty($request->primary_meal_prepare)) {
                    $meal_preparer = implode(", ", $request->primary_meal_prepare);
                    $meal_preparer = rtrim($meal_preparer, ', ');
                    $pateint_dietary_info->meal_preparer = $meal_preparer;
                }
                else
                {
                    $pateint_dietary_info->meal_preparer = $meal_preparer;
                }
                $pateint_dietary_info->update();
            } else {
                $pateint_dietary_info = new PatientDietaryInfo;
                $pateint_dietary_info->patient_id = $request->patient_id;
                if (!empty($request->dieatry_restriction)) {
                    $dietary_restrictions = implode(", ", $request->dieatry_restriction);
                    $dietary_restrictions = rtrim($dietary_restrictions, ', ');
                    $pateint_dietary_info->dietary_restrictions = $dietary_restrictions;
                }
                else
                {
                    $pateint_dietary_info->dietary_restrictions = $dietary_restrictions;
                }

                if (!empty($request->primary_meal_prepare)) {
                    $meal_preparer = implode(", ", $request->primary_meal_prepare);
                    $meal_preparer = rtrim($meal_preparer, ', ');
                    $pateint_dietary_info->meal_preparer = $meal_preparer;
                }
                else
                {
                    $pateint_dietary_info->meal_preparer = $meal_preparer;
                }
                $pateint_dietary_info->save();

            }

            /* get device information for notification code start here  */
            $target_id = "";
            $taget_meal_info="";
            $taget_reminder_info="";
            $device_info = $this->dashboard_setting_model->get_device_information($request->patient_id);
            $taget_reminder_info = $this->dashboard_setting_model->get_taget_reminder_information($request->patient_id);
            $target_meal_array =[];

            if(!empty($taget_reminder_info)) {
                $target_meal_array['targetId']= $taget_reminder_info->target_id;
                $target_meal_array['startingWeight']= $taget_reminder_info->starting_weight;
                $target_meal_array['targetWeight']= $taget_reminder_info->target_weight;
                $target_meal_array['weightFrequency']= $taget_reminder_info->weight_frequency;
                $target_meal_array['glucoseMin']= $taget_reminder_info->glucose_min;
                $target_meal_array['glucoseMax']= $taget_reminder_info->glucose_max;
                $target_meal_array['source']= $taget_reminder_info->target_source;
                $target_meal_array['glucoseMin2']= $taget_reminder_info->glucose_min_2;
                $target_meal_array['glucoseMax2']= $taget_reminder_info->glucose_max_2;
                $target_meal_array['activitySteps']= $taget_reminder_info->activity_steps;
                $target_meal_array['activityMinutes']= $taget_reminder_info->activity_minutes;
                $target_meal_array['weightTime']= $taget_reminder_info->weight_time;
                $target_meal_array['reminder']= array('reminderId'=> $taget_reminder_info->reminder_id,'isMealTime'=> $taget_reminder_info->is_meal_time == 0 ? "false" : "true" ,'isGlucoseTesting'=> $taget_reminder_info->is_glucose_testing == 0 ? "false" : "true",'isGoalActionPlan'=> $taget_reminder_info->is_goal_action_plan == 0 ? "false" : "true",'isNewTopicAvailable'=> $taget_reminder_info->is_new_topic_available == 0 ? "false" : "true",'isWeighingMyself'=> $taget_reminder_info->is_weighing_myself == 0 ? "false" : "true",'isSnackTime'=> $taget_reminder_info->is_snack_time == 0 ? "false" : "true",'isBreakfastTime'=> $taget_reminder_info->is_breakfast_time == 0 ? "false" : "true",'isLunchTime'=> $taget_reminder_info->is_lunch_time == 0 ? "false" : "true" ,'isDinnerTime'=> $taget_reminder_info->is_dinner_time == 0 ? "false" : "true");
                $target_meal_array['medTargets']= array();
                $target_meal_array['reminders']= array('reminderId'=> $taget_reminder_info->reminder_id,'isMealTime'=> $taget_reminder_info->is_meal_time == 0 ? "false" : "true" ,'isGlucoseTesting'=> $taget_reminder_info->is_glucose_testing == 0 ? "false" : "true",'isGoalActionPlan'=> $taget_reminder_info->is_goal_action_plan == 0 ? "false" : "true",'isNewTopicAvailable'=> $taget_reminder_info->is_new_topic_available == 0 ? "false" : "true",'isWeighingMyself'=> $taget_reminder_info->is_weighing_myself == 0 ? "false" : "true",'isSnackTime'=> $taget_reminder_info->is_snack_time == 0 ? "false" : "true",'isBreakfastTime'=> $taget_reminder_info->is_breakfast_time == 0 ? "false" : "true",'isLunchTime'=> $taget_reminder_info->is_lunch_time == 0 ? "false" : "true" ,'isDinnerTime'=> $taget_reminder_info->is_dinner_time == 0 ? "false" : "true");
                $target_meal_array['glucoseTargets']= array();
                $target_meal_array['mealTargets']= array();
            }
            $target_id = $taget_reminder_info->target_id;
            $target_info_array =[];
            if($target_id!="")
            {
                $taget_meal_info = $this->dashboard_setting_model->get_taget_meal_information($target_id);
                if(!empty($taget_meal_info))
                {
                    foreach($taget_meal_info as $taget_meal_info_val )
                    {
                        $target_info_array['mealTargetId'] = $taget_meal_info_val->meal_target_id;
                        $target_info_array['mealType'] = $taget_meal_info_val->meal_type;
                        $target_info_array['mealTime'] = $taget_meal_info_val->meal_time;
                        $target_info_array['carbTarget'] = $taget_meal_info_val->carb_target;
                        $target_info_array['dailyFatBudget'] = $taget_meal_info_val->daily_fat_budget;
                        $target_info_array['dailyCalorieBudget'] = $taget_meal_info_val->daily_calorie_budget;
                        $target_info_array['dailyWaterBudget'] = $taget_meal_info_val->daily_water_budget;
                        array_push($target_meal_array['mealTargets'],$target_info_array);
                    }
                }

            }

            Log::info('Target Meal Info: ' . print_r($target_meal_array, true));
            Log::info('device Response: ' . print_r($device_info, true));
            if(!empty($device_info))
           {
                 if($device_info->device_type != "") {
                   info('Sending Push notification to User: ' . $device_info->patient_id . ' Device Type: ' . $device_info->device_type);
                   if ($device_info->registration_id != '' && $device_info->device_type == 'Android') {
                       $push = new PushNotification('gcm');
                       $push->setMessage(['data' => ['data' => ['actions' => 'target', 'payload' => json_encode($target_meal_array) ],],])->setDevicesToken($device_info->registration_id)->send();
                       Log::info('Notification Response: ' . print_r($push->getFeedback(), true));
                   } else if ($device_info->registration_id != '' && $device_info->device_type == 'IOS') {
                       $push = new PushNotification('apn');
                       $push->setMessage(['aps' => ['content-available' => 1,  'event' => 'target'],])->setDevicesToken($device_info->registration_id)->send();
                       Log::info('Notification Response: ' . print_r($push->getFeedback(), true));
                   }
               }

           }

            /* get device information for notification code end here  */

            return $response = [
                'success' => "Profile Update successfully",
            ];

        }

    }


    public function updateMemberDiabetesInfo(Request $request)
    {

        if (request()->ajax())
        {

            $validator = Validator::make(request()->input(), [
                'diagnosis' => 'required',
                'year_of_diagnosis' => 'required',
                'time_since_last_diabetes_education' => 'required',
                'diabetes_support' => 'required',
                'being_treated_depression' => 'required',

            ]);

            if ($validator->fails()) {
                info('update member Validation Failed');
                foreach ($validator->errors()->all() as $key => $value) {
                    return $response = [
                        'error' => $value,
                    ];
                    break;
                }
            }
            $diabetes_support="";
            $pateint_diabetes_info = PatientDiabetesInfo::where('patient_id', $request->patient_id)->first();
            if(isset($pateint_diabetes_info->diabetes_info_id))
            {

                $pateint_diabetes_info->diagnosis = $request->diagnosis;
                $pateint_diabetes_info->diagnosis_date = $request->year_of_diagnosis;
                $pateint_diabetes_info->time_since_last_diabetes_edu = $request->time_since_last_diabetes_education;
                if(!empty($request->diabetes_support)) {
                    $diabetes_support = implode(", ",$request->diabetes_support);
                    $diabetes_support = rtrim($diabetes_support, ', ');
                    $pateint_diabetes_info->diabetes_support =  $diabetes_support ;
                }
                else
                {
                    $pateint_diabetes_info->diabetes_support = $diabetes_support;
                }
                $pateint_diabetes_info->treated_depression = $request->being_treated_depression;
                $pateint_diabetes_info->update();


            }
            else
            {
                $pateint_diabetes_info = new PatientDiabetesInfo;
                $pateint_diabetes_info->patient_id = $request->patient_id;
                $pateint_diabetes_info->diagnosis = $request->diagnosis;
                $pateint_diabetes_info->diagnosis_date = $request->year_of_diagnosis;
                $pateint_diabetes_info->time_since_last_diabetes_edu = $request->time_since_last_diabetes_education;
                if(!empty($request->diabetes_support)) {
                    $diabetes_support = implode(", ",$request->diabetes_support);
                    $diabetes_support = rtrim($diabetes_support, ', ');
                    $pateint_diabetes_info->diabetes_support =  $diabetes_support ;
                }
                else
                {
                    $pateint_diabetes_info->diabetes_support = $diabetes_support;
                }
                $pateint_diabetes_info->treated_depression = $request->being_treated_depression;
                $pateint_diabetes_info->save();


            }

            return $response = [
                'success' => "Diabetes information updated successfully",
            ];

        }

    }


    public function updateMemberReminder(Request $request)
    {



        if (request()->ajax()) {

            $validator = Validator::make(request()->input(), [
                'breakfast' => 'required',
                'lunch' => 'required',
                'snack' => 'required',
                'dinner' => 'required',
                'weight_reminder' => 'required',

            ]);

            if ($validator->fails()) {
                info('update member Reminder Failed');
                foreach ($validator->errors()->all() as $key => $value) {
                    return $response = [
                        'error' => $value,
                    ];
                    break;
                }
            }


            $target_meal = TargetMeal::where('target_id', $request->target_id)->where('meal_type', 'Lunch')->first();
            $target_meal->meal_time = $request->lunch;
            $target_meal->update();
            $target_meal = TargetMeal::where('target_id', $request->target_id)->where('meal_type', 'Dinner')->first();
            $target_meal->meal_time = $request->dinner;
            $target_meal->update();
            $target_meal = TargetMeal::where('target_id', $request->target_id)->where('meal_type', 'Breakfast')->first();
            $target_meal->meal_time = $request->breakfast;
            $target_meal->update();
            $target_meal = TargetMeal::where('target_id', $request->target_id)->where('meal_type', 'Snack')->first();
            $target_meal->meal_time = $request->snack;
            $target_meal->update();

            $target = Target::where('target_id', $request->target_id)->first();
            $target->weight_time = $request->weight_reminder;
            $target->update();
            info("target meal updated successfully");

            $user_message_preference =  UserMessagePreference::where('user_id', $request->user_id)->first();
            if(isset($user_message_preference->user_messaging_preference_id)) {
                if ($request->week && $request->frequency!= "Once/Month")
                    $user_message_preference->day_of_week = $request->week;
                else
                    $user_message_preference->day_of_week = "";

                if($request->month && $request->frequency == "Once/Month")
                     $user_message_preference->date_of_month = $request->month;
                else
                    $user_message_preference->date_of_month = "";

                $user_message_preference->type_of_reminder = $request->frequency;
                $user_message_preference->time_of_day = $request->time;

                if ($request->is_email)
                    $user_message_preference->email = $request->is_email;
                else
                    $user_message_preference->email = 0;

                if ($request->is_app_notification)
                    $user_message_preference->is_in_app_message_notify = $request->is_app_notification;
                else
                    $user_message_preference->is_in_app_message_notify = 0;

                if ($request->is_coach_communication_reminder)
                    $user_message_preference->is_coach_communication_reminder = $request->is_coach_communication_reminder;
                else
                    $user_message_preference->is_coach_communication_reminder = 0;

                $user_message_preference->update();
                info("user message preference updated successfully");
            }
            else
            {
                $user_message_preference =  new UserMessagePreference;
                $user_message_preference->user_id = $request->user_id;
                if ($request->week && $request->frequency!= "Once/Month")
                    $user_message_preference->day_of_week = $request->week;
                else
                    $user_message_preference->day_of_week = "";

                if($request->month && $request->frequency == "Once/Month")
                    $user_message_preference->date_of_month = $request->month;
                else
                    $user_message_preference->date_of_month = "";

                $user_message_preference->type_of_reminder = $request->frequency;
                $user_message_preference->time_of_day = $request->time;

                if ($request->is_email)
                    $user_message_preference->email = $request->is_email;
                else
                    $user_message_preference->email = 0;

                if ($request->is_app_notification)
                    $user_message_preference->is_in_app_message_notify = $request->is_app_notification;
                else
                    $user_message_preference->is_in_app_message_notify = 0;

                if ($request->is_coach_communication_reminder)
                    $user_message_preference->is_coach_communication_reminder = $request->is_coach_communication_reminder;
                else
                    $user_message_preference->is_coach_communication_reminder = 0;

                $user_message_preference->save();
                info("user message preference inserted successfully");

            }



            $reminder =  Reminder::where('target_id', $request->target_id)->first();


            if($request->is_log_meal_time) {

                if ($request->is_breakfast_time)
                    $reminder->is_breakfast_time = $request->is_breakfast_time;
                else
                {
                    $reminder->is_breakfast_time = 0;

                }
                if ($request->is_lunch_time)
                    $reminder->is_lunch_time = $request->is_lunch_time;
                else
                {
                    $reminder->is_lunch_time = 0;

                }
                if ($request->is_dinner_time)
                    $reminder->is_dinner_time = $request->is_dinner_time;
                else
                {
                    $reminder->is_dinner_time = 0;

                }
                if ($request->is_snack_time)
                    $reminder->is_snack_time = $request->is_snack_time;
                else
                {

                    $reminder->is_snack_time = 0;

                }
                if ($request->is_log_meal_time)
                    $reminder->is_meal_time = $request->is_log_meal_time;
                else
                    $reminder->is_meal_time = 0;


            }
            else
            {
                $reminder->is_breakfast_time = 0;
                $reminder->is_lunch_time = 0;
                $reminder->is_dinner_time = 0;
                $reminder->is_snack_time = 0;
                $reminder->is_meal_time = 0;

            }

            if($request->is_weight_time)
                $reminder->is_weighing_myself = $request->is_weight_time;
            else
                $reminder->is_weighing_myself = DB::raw(0);

            $reminder->update();

            /* get device information for notification code start here  */
            $target_id = "";
            $taget_meal_info="";
            $taget_reminder_info="";
            $device_info = $this->dashboard_setting_model->get_device_information($request->patient_id);
            $taget_reminder_info = $this->dashboard_setting_model->get_taget_reminder_information($request->patient_id);
            $target_meal_array =[];

            if(!empty($taget_reminder_info)) {
                $target_meal_array['targetId']= $taget_reminder_info->target_id;
                $target_meal_array['startingWeight']= $taget_reminder_info->starting_weight;
                $target_meal_array['targetWeight']= $taget_reminder_info->target_weight;
                $target_meal_array['weightFrequency']= $taget_reminder_info->weight_frequency;
                $target_meal_array['glucoseMin']= $taget_reminder_info->glucose_min;
                $target_meal_array['glucoseMax']= $taget_reminder_info->glucose_max;
                $target_meal_array['source']= $taget_reminder_info->target_source;
                $target_meal_array['glucoseMin2']= $taget_reminder_info->glucose_min_2;
                $target_meal_array['glucoseMax2']= $taget_reminder_info->glucose_max_2;
                $target_meal_array['activitySteps']= $taget_reminder_info->activity_steps;
                $target_meal_array['activityMinutes']= $taget_reminder_info->activity_minutes;
                $target_meal_array['weightTime']= $taget_reminder_info->weight_time;
                $target_meal_array['reminder']= array('reminderId'=> $taget_reminder_info->reminder_id,'isMealTime'=> $taget_reminder_info->is_meal_time == 0 ? "false" : "true" ,'isGlucoseTesting'=> $taget_reminder_info->is_glucose_testing == 0 ? "false" : "true",'isGoalActionPlan'=> $taget_reminder_info->is_goal_action_plan == 0 ? "false" : "true",'isNewTopicAvailable'=> $taget_reminder_info->is_new_topic_available == 0 ? "false" : "true",'isWeighingMyself'=> $taget_reminder_info->is_weighing_myself == 0 ? "false" : "true",'isSnackTime'=> $taget_reminder_info->is_snack_time == 0 ? "false" : "true",'isBreakfastTime'=> $taget_reminder_info->is_breakfast_time == 0 ? "false" : "true",'isLunchTime'=> $taget_reminder_info->is_lunch_time == 0 ? "false" : "true" ,'isDinnerTime'=> $taget_reminder_info->is_dinner_time == 0 ? "false" : "true");
                $target_meal_array['medTargets']= array();
                $target_meal_array['reminders']= array('reminderId'=> $taget_reminder_info->reminder_id,'isMealTime'=> $taget_reminder_info->is_meal_time == 0 ? "false" : "true" ,'isGlucoseTesting'=> $taget_reminder_info->is_glucose_testing == 0 ? "false" : "true",'isGoalActionPlan'=> $taget_reminder_info->is_goal_action_plan == 0 ? "false" : "true",'isNewTopicAvailable'=> $taget_reminder_info->is_new_topic_available == 0 ? "false" : "true",'isWeighingMyself'=> $taget_reminder_info->is_weighing_myself == 0 ? "false" : "true",'isSnackTime'=> $taget_reminder_info->is_snack_time == 0 ? "false" : "true",'isBreakfastTime'=> $taget_reminder_info->is_breakfast_time == 0 ? "false" : "true",'isLunchTime'=> $taget_reminder_info->is_lunch_time == 0 ? "false" : "true" ,'isDinnerTime'=> $taget_reminder_info->is_dinner_time == 0 ? "false" : "true");
                $target_meal_array['glucoseTargets']= array();
                $target_meal_array['mealTargets']= array();
            }
            $target_id = $taget_reminder_info->target_id;
            $target_info_array =[];
            if($target_id!="")
            {
                $taget_meal_info = $this->dashboard_setting_model->get_taget_meal_information($target_id);
                if(!empty($taget_meal_info))
                {
                    foreach($taget_meal_info as $taget_meal_info_val )
                    {
                        $target_info_array['mealTargetId'] = $taget_meal_info_val->meal_target_id;
                        $target_info_array['mealType'] = $taget_meal_info_val->meal_type;
                        $target_info_array['mealTime'] = $taget_meal_info_val->meal_time;
                        $target_info_array['carbTarget'] = $taget_meal_info_val->carb_target;
                        $target_info_array['dailyFatBudget'] = $taget_meal_info_val->daily_fat_budget;
                        $target_info_array['dailyCalorieBudget'] = $taget_meal_info_val->daily_calorie_budget;
                        $target_info_array['dailyWaterBudget'] = $taget_meal_info_val->daily_water_budget;
                        array_push($target_meal_array['mealTargets'],$target_info_array);
                    }
                }

            }

            Log::info('Target Meal Info: ' . print_r($target_meal_array, true));
            Log::info('device Response: ' . print_r($device_info, true));
            if(!empty($device_info))
            {
                if($device_info->device_type != "") {
                    info('Sending Push notification to User: ' . $device_info->patient_id . ' Device Type: ' . $device_info->device_type);
                    if ($device_info->registration_id != '' && $device_info->device_type == 'Android') {
                        $push = new PushNotification('gcm');
                        $push->setMessage(['data' => ['data' => ['actions' => 'target', 'payload' => json_encode($target_meal_array) ],],])->setDevicesToken($device_info->registration_id)->send();
                        Log::info('Notification Response: ' . print_r($push->getFeedback(), true));
                    } else if ($device_info->registration_id != '' && $device_info->device_type == 'IOS') {
                        $push = new PushNotification('apn');
                        $push->setMessage(['aps' => ['content-available' => 1,  'event' => 'target'],])->setDevicesToken($device_info->registration_id)->send();
                        Log::info('Notification Response: ' . print_r($push->getFeedback(), true));
                    }
                }

            }

            /* get device information for notification code end here  */


            info("reminder updated successfully");
            return $response = [
                'success' => "Reminders Update successfully",
            ];



        }







    }




}//End of Class

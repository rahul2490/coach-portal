<?php

namespace HealthSlatePortal\Http\Controllers\admin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CurriculumServiceProvider;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\Patient;
use Illuminate\Support\Facades\Log;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\PatientHideUnhide;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\Message;
use HealthSlatePortal\Models\Eloquent\SessionAttendance;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use Edujugon\PushNotification\PushNotification;
use DB;

class MemberController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $user;
    protected $dataTableService;
    protected $group_model;
    protected $group_member;
    protected $curriculumServiceProvider;
    protected $data = array();

    function __construct( FacilityModel $facility_model, MemberModel $member_model, Facility $facility, CoacheModel $coach_model,
                          DataTableServiceProvider $dataTableService, User $user, GroupModel $group_model, GroupMember $group_member, CurriculumServiceProvider $curriculumServiceProvider) {
        $this->facility_model       = $facility_model;
        $this->coach_model          = $coach_model;
        $this->member_model         = $member_model;
        $this->group_member         = $group_member;
        $this->group_model          = $group_model;
        $this->facility             = $facility;
        $this->user                 = $user;
        $this->dataTableService     = $dataTableService;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->group_db             = env('GROUP_DB_DATABASE');
        $this->active_facility_id   = session('active_facility');
        $this->active_user_id       = session('userId');
        $this->active_user_role     = session('userRole');
        $this->data['js']           = array('message', 'member','member-recruitment');
        $this->data['common_js']    = array('tagging/jquery-migrate-1.2.1', 'tagging/jquery.caretposition', 'tagging/jquery.sew');
    }


    /**
     * To render member page
     * (it's show member page)
     */
	public function index($group_id = '') {

        $timezone = '';
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }

        $today_date = Carbon::now();
        if(!empty($timezone))
            $today_date = Carbon::createFromFormat('Y-m-d H:i:s', $today_date)->timezone($timezone);
        else
            $today_date = Carbon::createFromFormat('Y-m-d H:i:s', $today_date);

        $group_list = $this->group_model->get_all_group_list($this->active_facility_id);
        $today_birthdays_count  = $this->group_model->get_today_birthday_data($this->active_facility_id,$today_date);
        $facility_group_list = array();
        if(!empty($group_list))
        {
            $facility_group_list[''] = 'Select Group';
            foreach ($group_list as $key => $value)
            {
                $facility_group_list[$value->group_id] = $value->name;
            }
        }
        $this->data['today_birthdays_count']= $today_birthdays_count->birthday_count;
        $this->data['facility_group_list']  = $facility_group_list;
        $this->data['member_group_id']      = base64_decode($group_id);
        info('Total Group Found: ' . count($facility_group_list));
        return view( 'admin.member.member', $this->data);
	}


    /**
     * member list function
     * (it's load data on member page )
     */
    public function membersList(Request $request)
    {
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 0);
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }

        $today_date = Carbon::now();
        $today_date =strtotime($today_date)*1000;
        $pastweek_date = Carbon::now()->copy()->startOfDay()->subDays(28);
        $pastweek_date =strtotime($pastweek_date)*1000;
        $start                  = request('start', 0);
        $length                 = request('length', 0);
        $datatable_request      = array('start' => $start, 'length' => $length);

        $member_filter  = $request->member_filter;
        $group_filter   = $request->group_filter;
        $another_filter   = $request->another_filter;
        $birthday_date="";
        if($request->birthdays_today && $request->birthdays_today!="")
        {
            $birthday_date=  Carbon::now();
            if(!empty($timezone))
                $birthday_date = Carbon::createFromFormat('Y-m-d H:i:s', $birthday_date)->timezone($timezone);
            else
                $birthday_date = Carbon::createFromFormat('Y-m-d H:i:s', $birthday_date);

            $member_filter = "";
            $group_filter = "";
            $another_filter = "";
        }


        $response       = $this->member_model->get_member_list($this->active_facility_id, $member_filter, $group_filter,$another_filter, $this->active_user_id, $datatable_request, $birthday_date);

        $response_total = count($response);
        $response       = collect($response);

        info('Total Members Found: ' . count($response));

        $sorting_status = true;

        $data    = collect();
        $danger  = collect();
        $warning = collect();
        $success = collect();
        $white   = collect();
        $active  = collect();
        $final_data = collect();

        $color_class = '';
        $status = '';

        $timezone = '';
        if(isset($_COOKIE['user_timezone'])) {
            $timezone =  $_COOKIE['user_timezone'];
        }

        foreach ($response as $member) {
            $check_engagement   = get_engagement_score($member);
            $color_class        = $check_engagement['color_class'];
            $status             = $check_engagement['status'];

            $member->color_class    = $color_class;
            $member->color_status   = $status;
            $member->status_type    = $check_engagement['status_type'];

            if($sorting_status)
            {
                if($member->hide_unhide == 1)
                    $active->push($member);
                else if($color_class == 'danger')
                    $danger->push($member);
                else if($color_class == 'warning')
                    $warning->push($member);
                else if($color_class == 'success')
                    $success->push($member);
                else if($color_class == 'white')
                    $white->push($member);
                else if($color_class == 'active')
                    $active->push($member);
            }
            else
            {
                $active->push($member);
            }
        }
        $danger   = $danger->sortByDesc('pin_unpin');
        $warning  = $warning->sortByDesc('pin_unpin');
        $success  = $success->sortByDesc('pin_unpin');

        $data = $data->merge($danger);
        $data = $data->merge($warning);
        $data = $data->merge($success);

        $white_sorting_for_pin      = collect();
        $white_sorting_for_unpin    = collect();
        foreach ($white as $white_sorting) {
            if($white_sorting->pin_unpin == 1)
                $white_sorting_for_pin->push($white_sorting);
            else
                $white_sorting_for_unpin->push($white_sorting);
        }

        $active_sorting_for_pin       = collect();
        $active_sorting_for_unpin     = collect();
        foreach ($active as $active_sorting) {
            if($active_sorting->pin_unpin == 1)
                $active_sorting_for_pin->push($active_sorting);
            else
                $active_sorting_for_unpin->push($active_sorting);
        }

        $data = $data->merge($white_sorting_for_pin);
        $data = $data->merge($white_sorting_for_unpin);

        $data = $data->merge($active_sorting_for_pin);
        $data = $data->merge($active_sorting_for_unpin);

        $data = $data->slice($request['start'], $request['length']);

        $patient_ids    = $data->pluck( 'patient_id' )->toArray();
        $member_message_detail      = $this->member_model->get_member_message_detail($patient_ids,$user_id='');
        if(!empty($member_message_detail))
        {
            foreach ($response as $row) {
                foreach ($member_message_detail as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->total_unread_message            = $value->total_unread_message;
                        //$row->last_message_sent_to_patient    = $value->last_message_sent_to_patient;
                        $row->last_unread_message_of_patient  = $value->last_unread_message_of_patient;
                    }
                }
            }
        }

        $get_weight_detail  = $this->member_model->get_patient_current_weight_date($patient_ids,$pastweek_date,$today_date);
        if(!empty($get_weight_detail))
        {
            foreach ($response as $row) {
                foreach ($get_weight_detail as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->last_weight_date  = $value->last_weight_date;
                        //$row->past_week_count   = $value->past_week_count;

                    }
                }
            }
        }

        $get_engagement_score  = $this->member_model->get_patient_engagement_score($patient_ids);
        if(!empty($get_engagement_score))
        {
            foreach ($response as $row) {
                foreach ($get_engagement_score as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->prior_week_engagement_score       = round(($value->prior_week_engagement_score / (($value->prior_week_engagement_score_total_days == 0 )? 1 : $value->prior_week_engagement_score_total_days)),0);
                        $row->current_week_engagement_score     = round(($value->current_week_engagement_score / (($value->current_week_engagement_score_total_days == 0) ? 1 : $value->current_week_engagement_score_total_days)),0);
                    }
                }
            }
        }

        $last_sent_member_detail  = $this->member_model->get_last_message_sent_to_patient($patient_ids, $this->active_user_id);
        if(!empty($last_sent_member_detail))
        {
            foreach ($response as $row) {
                foreach ($last_sent_member_detail as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->last_message_sent_to_patient    = $value->last_message_sent_to_patient;
                    }
                }
            }
        }


        $recommended_message_response   = collect($this->member_model->get_recommended_message_list());
        $count = 1;
        $milestone_risk = '';
        foreach ($data as $member) {

            $color_class        = $member->color_class;
            $status             = $member->color_status;
            $status_type        = $member->status_type;

            $recommended_message = '<a data-id="'.$member->user_id.'" data-toggle="modal" href="#direct-msg-coach" class="btn grey-gallery btn-sm btn-outline sbold ">View Message</a>';
            if($color_class == 'danger' || $color_class == 'warning')
            {
                if($count == 1){
                    $in_app_message_list  = collect($this->member_model->get_in_app_message_list($patient_ids));
                    $count += 1;
                }
                $message_text = '';
                $message_type         = $recommended_message_response->where('status', $status_type);
                $in_app_message_type  = $in_app_message_list->where('patient_id', $member->patient_id);

                if(count($in_app_message_type) > 0 && count($message_type) > 0)
                {
                    $count_sent_message = 0;
                    $predefined_message_id = [];
                    foreach ($in_app_message_type as $sent_message)
                    {
                        $check_sent_message = collect($message_type)->where('message_text', $sent_message->message)->first();

                        if(count($check_sent_message) > 0)
                        {
                            $count_sent_message += 1;
                            $predefined_message_id[] = $check_sent_message->predefined_message_id;
                            //echo $sent_message->message_text;
                        }
                    }

                    if($count_sent_message == count($message_type))
                    {
                        $message_text = 'Already Sent';
                        //Already Sent All Message for week
                    }
                    else
                    {
                        foreach ($message_type->all() as $check_message)
                        {
                            if(in_array($check_message->predefined_message_id, $predefined_message_id))
                            {
                                continue;
                            }
                            else
                            {
                                $message_text = $check_message->message_text;
                                break;
                            }
                        }
                    }

                }
                elseif(count($message_type) > 0)
                {
                    $message_type = $message_type->first();
                    $message_text = $message_type->message_text;
                }

                if($message_text != 'Already Sent')
                    $milestone_risk = '<div class="col-sm-12"><p class="recm-msg"><i>'.str_limit($message_text,50).'</i></p></div>';
                else
                    $milestone_risk = '<div class="col-sm-12"><p class="recm-msg"><i>'.str_limit($message_text,50).'</i></p></div>';
            }




            $last_message_sent_to_patient   = '';
            $last_unread_message_of_patient = '';
            $here = Carbon::now($timezone);
            $last_weight_date = '';
            $past_week_count = '';
            if(!empty($member->last_message_sent_to_patient))
            {
                $last_message_sent_to_patient    = Carbon::createFromTimestamp($member->last_message_sent_to_patient/1000)->format('Y-m-d H:i:s');
                $last_message_sent_to_patient    =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $last_message_sent_to_patient)->timezone($timezone) : $last_message_sent_to_patient;
                $here->addSeconds($last_message_sent_to_patient->offset - $here->offset);
                $last_message_sent_to_patient     = '<small>'.$last_message_sent_to_patient->diffForHumans($here).'</small>';
                $last_message_sent_to_patient     =  str_replace("before","ago",$last_message_sent_to_patient);
            }
            if(!empty($member->last_unread_message_of_patient))
            {
                $last_unread_message_of_patient    = Carbon::createFromTimestamp($member->last_unread_message_of_patient/1000)->format('Y-m-d H:i:s');
                $last_unread_message_of_patient    =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $last_unread_message_of_patient)->timezone($timezone) : $last_unread_message_of_patient;
                $here->addSeconds($last_unread_message_of_patient->offset - $here->offset);
                $last_unread_message_of_patient = '(' . $last_unread_message_of_patient->diffForHumans($here). ')';
                $last_unread_message_of_patient =  str_replace("before","ago",$last_unread_message_of_patient);
            }

            if(!empty($member->last_weight_date))
            {

                $last_weight_date    = Carbon::createFromTimestamp($member->last_weight_date/1000)->format('Y-m-d H:i:s');
                $last_weight_date    =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $last_weight_date)->timezone($timezone) : $last_weight_date;
                $last_weight_date    =  "Last Weight: ".$last_weight_date->format('m/d');
            }

            if(($member->prior_week_engagement_score == 0 AND $member->current_week_engagement_score == 0))
                $past_week_count =  'Past Week: '.$member->prior_week_engagement_score.' <i class="glyphicon glyphicon-circle-arrow-down font-red-soft"></i> '.$member->current_week_engagement_score;
            elseif($member->prior_week_engagement_score <= $member->current_week_engagement_score)
                $past_week_count =  'Past Week: '.$member->prior_week_engagement_score.' <i class="glyphicon glyphicon-circle-arrow-up font-green-jungle"></i> '.$member->current_week_engagement_score;
            else
                $past_week_count =  'Past Week: '.$member->prior_week_engagement_score.' <i class="glyphicon glyphicon-circle-arrow-down font-red-soft"></i> '.$member->current_week_engagement_score;

            //Check user pinned or not
            $pin_unpin = '<a data-toggle="tooltip" title="Pin" data-value="1" data-id="'.$member->user_id.'" class="btn font-grey-salsa btn-sm btn-pin sbold pin_unpin member-list" data-original-title="Pin"><i class="iconhs-pin"></i></a>';
            if($member->pin_unpin == 1)
            {
                $pin_unpin = '<a data-toggle="tooltip" title="Unpin" data-value="0" data-id="'.$member->user_id.'" class="btn font-dark btn-sm btn-pin sbold pin_unpin member-list" data-original-title="Unpin"><i class="iconhs-pin"></i></a>';
            }

            //Check user hide or not
            $hide_unhide = '<a data-toggle="tooltip" title="Hide" data-value="1" data-id="'.$member->user_id.'" class="btn font-dark btn-sm btn-hide sbold hide_unhide member-list" data-original-title="Hide" aria-describedby="tooltip124891"><i class="fa fa-eye"></i></a>';
            if($member->hide_unhide == 1)
            {
                $color_class    = 'active';
                $status         = '';
                $recommended_message = '';
                $hide_unhide = '<a data-toggle="tooltip" title="Unhide" data-value="0" data-id="'.$member->user_id.'" class="btn font-dark btn-sm btn-hide sbold hide_unhide member-list" data-original-title="Unhide" aria-describedby="tooltip124891"><i class="fa fa-eye-slash"></i></a>';
            }

            $asign_group    = '';
            $remove_member  = '<a  title="Delete" data-toggle="modal" data-id="'.$member->user_id.'" data-target="#modal_prospective_member" class="btn font-dark btn-sm btn-hide sbold member-list"><i class="fa fa-remove"></i></a>';

            $member_detail = '<a  title="Member Details" data-toggle="modal" data-id="'.$member->user_id.'" data-target="#member_detail" class="btn font-dark btn-sm btn-hide sbold member-list"><i class="fa fa-info-circle"></i></a>';
            $patient_profile_image = env('APP_BASE_URL').'/img/default-user.png';
            if($member->patient_profile_image != '')
            {
                $patient_profile_image = env('PROFILE_IMAGE_BASE_URL') . $member->patient_profile_image;
            }

            $row = [
                'hidden_id'                 =>  '',
                'patient_id'                => '<div class="col-sm-6 '.$color_class.'"><span class="memberid">'.$member->patient_id.' </span></br> Week '.abs($member->member_completed_week).' </div><div class="col-sm-6"> <img class="user-pic rounded"  src="'.$patient_profile_image.'"></br><a href="'.url('admin/dashboard') .'/'. $member->patient_id.'" class="member-name sbold">'.str_limit($member->first_name).' '.str_limit($member->last_name).'</a></div>',

                'status(engagement score)'  => $milestone_risk,
                'recommended message'       => '<span  class="sbold">'.$past_week_count.'</span><br><br><span  class="sbold">'.$last_weight_date.'</span>',

                'last sent'                 => $last_message_sent_to_patient,
                'message'                   => '<div class="btn-group"><a data-id="'.$member->user_id.'" data-toggle="modal" href="#direct-msg-coach" class="btn grey-gallery btn-sm btn-outline sbold ">Message</a></div>',
                'signed_on'                 => ( ! empty( $timezone ) && !empty($member->registration_date) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $member->registration_date)->timezone($timezone)->format('m/d/Y h:i A') : '',
                'action'                    => $asign_group.' '.$pin_unpin.' '.$hide_unhide .' '. $remove_member .' '. $member_detail,

            ];

            $final_data->push(array_values($row));
        }

        info('Sending Data To DataTableService');

        $final_response['data']              = $final_data;
        $final_response['recordsTotal']      = $response_total;
        $final_response['recordsFiltered']   = $response_total;
        return response()->json( $final_response );
    }




    public function patient_hide_unhide(Request $request)
    {
        if (request()->ajax()) {
            $user_id    = request('user_id', 0);
            $value      = request('value', 0);

            $patient = Patient::where('user_id', $user_id)->first();

            if(empty($patient))
            {
                info('Member not found into Database USERID:' . $user_id);
                return response()->json([
                    'error' => 'Member not found into Database'
                ]);
            }

            info('Updating status for USERID:' . $user_id);
            $user = PatientHideUnhide::firstOrNew(array('patient_id' => $patient->patient_id, 'user_id' => $this->active_user_id));
            $user->hide_unhide  = $value;
            $user->date         = date('Y-m-d H:i:s');
            $user->save();

            info('Member Status Updated Successfully: PatientId' . $patient->patient_id);

            return $response = [
                'success' => 'Member Status Updated Successfully!',
            ];
        }
        else
        {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'update status'])
            ]);
        }
    }


    public function patient_pin_unpin(Request $request)
    {
        if (request()->ajax()) {
            $user_id    = request('user_id', 0);
            $value      = request('value', 0);

            $patient = Patient::where('user_id', $user_id)->first();

            if(empty($patient))
            {
                info('Member not found into Database USERID:' . $user_id);
                return response()->json([
                    'error' => 'Member not found into Database'
                ]);
            }

            info('Updating status for USERID:' . $user_id);
            $user = PatientHideUnhide::firstOrNew(array('patient_id' => $patient->patient_id, 'user_id' => $this->active_user_id));
            $user->pin_unpin    = $value;
            $user->date         = date('Y-m-d H:i:s');
            $user->save();

            info('Member Status Updated Successfully: PatientId' . $patient->patient_id);

            return $response = [
                'success' => 'Member Status Updated Successfully!',
            ];
        }
        else
        {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'update status'])
            ]);
        }
    }

}//End of Class

<?php

namespace HealthSlatePortal\Http\Controllers\admin;

use HealthSlatePortal\Models\Eloquent\Provider;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\FacilityProvider;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\UserRole;
use HealthSlatePortal\Models\Eloquent\EmailNotificationMessage;
use HealthSlatePortal\Helpers\MailServiceHelper;
use HealthSlatePortal\Helpers\SmsServiceHelper;
use Illuminate\Support\Facades\Log;
use Validator;
use Session;
use DB;

class CoachController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $facilityProvider;
    protected $coach_model;
    protected $dataTableService;
    protected $user;
    protected $mailServiceHelper;
	protected $data = array();


    function __construct( FacilityProvider $facilityProvider,  MailServiceHelper $mailServiceHelper, SmsServiceHelper $smsServiceHelper, FacilityModel $facility_model, Facility $facility, CoacheModel $coach_model, DataTableServiceProvider $dataTableService, User $user) {
        $this->facility_model       = $facility_model;
        $this->coach_model          = $coach_model;
        $this->facility             = $facility;
        $this->facilityProvider     = $facilityProvider;
        $this->dataTableService     = $dataTableService;
        $this->mailServiceHelper    = $mailServiceHelper;
        $this->smsServiceHelper     =  $smsServiceHelper;
        $this->active_user_role     = session('userRole');
        $this->cobrand_id       = Session::get('user')->cobrandId;
        $this->user = $user;
        $this->data['js']           = array('coach');
    }

	/**
	 * Display a listing of Coach List.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {
        //$this->data['coaches'] = $this->coach_model->get_facility_coach_list(session('active_facility'));

        /*$all_coaches = $this->coach_model->get_cobrand_all_coach_list(session('active_facility'));

        $resultArray = $this->_coachListBuilder( $all_coaches );
        $this->data['lead_coach'] = array();
        $this->data['food_coach'] = array();
        $this->data['lead_coach'] = $resultArray['lead_coach'];
        $this->data['food_coach'] = $resultArray['food_coach'];

        info('Total Coaches Found: ' . count($all_coaches));*/

        $cobrand_id       = Session::get('user')->cobrandId  ;
        $this->data['facility']    = $this->facility_model->get_all_facility_list($cobrand_id);
        $this->data['facility_list']= $this->coach_model->get_all_facility_list_for_admin($cobrand_id);

        return view( 'admin.coach.coach', $this->data);
	}


    /**
     * @param $all_coaches
     *
     * @return array
     */
    private function _coachListBuilder( $all_coaches )
    {
        $foodCoach = [];
        $leadCoach = [];
        $response = $this->coach_model->get_facility_coach_list(session('active_facility'));
        $coach_ids = [];
        if(!empty($response))
        {
            foreach ($response as $coach) {
                $coach_ids[] = $coach->provider_id;
            }
        }

        foreach ($all_coaches as $coach) {
            if(in_array($coach->provider_id, $coach_ids))
            {
                continue;
            }
            $providers     = [ ];
            $providersLead = [ ];
            if ($coach->user_type == "Coach") {
                $providersLead[$coach->provider_id] = @$coach->first_name . " " . @$coach->last_name;
            } else if ($coach->user_type == "Food Coach") {
                $providers[$coach->provider_id] = @$coach->first_name . " " . @$coach->last_name;
            }
            if ( ! empty( $providers )) {
                $foodCoach[] = $providers;
            }
            if ( ! empty( $providersLead )) {
                $leadCoach[] = $providersLead;
            }
        }
        return [ 'food_coach' => $foodCoach, 'lead_coach' => $leadCoach ];
    }


    /**
     * Display all Coaches Of The Facility.
     *
     * @return \Illuminate\Http\Response
     */
	public function coachesList()
    {
        $response = $this->coach_model->get_facility_coach_list_for_admin(session('active_facility'), 0 , $this->active_user_role);
        info('Total Coaches Found: ' . count($response));
        $data = collect();

        foreach ($response as $coach) {

            $notification = ($coach->is_sms_enabled == 1) ? '<a title="SMS Enabled" class="btn font-dark btn-hide sbold"><i class="fa fa-mobile"></i></a>' : '<a title="SMS Disabled" class="btn font-grey-salsa btn-hide sbold"><i class="fa fa-mobile"></i></a>';
            $notification = ($coach->is_email_enabled == 1) ? "$notification<a title=\"Email Enabled\" class=\"btn font-dark btn-hide sbold\"><i class=\"fa fa-envelope\"></i></a>" : "$notification<a title=\"Email Disabled\" class=\"btn font-grey-salsa btn-hide sbold\"><i class=\"fa fa-envelope\"></i></a>" ;

            if($coach->user_type=="ROLE_FACILITY_ADMIN")
            $action = '<a title="Edit" data-provider_id="'.base64_encode($coach->user_id).'"  href="#modal_EditFacilityAdmin" data-toggle="modal" class="btn font-dark btn-hide sbold"><i class="fa fa-edit"></i></a>';
            else
             $action = '<a title="Edit" href="'.route('admin.coach-edit', ['coach_id' => base64_encode($coach->provider_id)]).'" class="btn font-dark btn-hide sbold"><i class="fa fa-edit"></i></a>';

            $coach->user_type = ($coach->user_type =="ROLE_FACILITY_ADMIN")? 'Facility Admin': $coach->user_type;

            $row = [
                'hidden_id'         => '',
                'provider_id'       => $coach->provider_id,
                'name'              => $coach->first_name . ' ' . $coach->last_name,
                'email'             => $coach->email,
                'phone'             => $coach->phone,
                'user_type'         => $coach->user_type,
                'is_email_enabled'  => $notification,
                'facility_name'     => $coach->facility_names,
                 'action'            => $action

            ];
            $data->push(array_values($row));
        }
        info('Sending Data To DataTableService');
        return $this->dataTableService->dataTableWithIds($data, 'coaches');
    }


    /**
     * Process delete request of delete Coach From Facility Provider Table
     *
     * @return \Illuminate\Http\JsonResponse
     */
    /*public function delete() {
        echo $coach_id = request('id');

        $deleted = false;
        $response = $this->facilityProvider
            ->where('providers_provider_id', $coach_id)
            //->where('facility_facility_id', session('active_facility'))
            ->get();
        // print_r($response);
         $loggedInUser = session('user');
        $user = $this->user->where('email', $loggedInUser->userName)->first();
         print_r($user); exit;

        if (empty($user) || empty($response)) {
            info('Delete Coach From Facility');
            return response()->json([
                'error' => trans('common.invalid_request_data'),
            ]);
        }
        info('Deleting Coach From Facility CoachID: ' . $coach_id . ' Request by: ' . $loggedInUser->userName);

        $deleted = $this->facilityProvider
            ->where('providers_provider_id', $coach_id)
            ->where('facility_facility_id', session('active_facility'))
            ->delete();

        if (!$deleted) {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'remove coach'])
            ]);
        }

        return response()->json(
            [
                'success' => 'Coach removed from this facility Successfully.'
            ]
        );
    }*/


    /**
     * Process assign coach For Facility Provider Table
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignCoach() {
        $membership = request('membership');

        if($membership == 1)
            $coach_id = request('select_multi_food');
        else
            $coach_id = request('select_multi_life');

        $save = false;

        $response = $this->facilityProvider
            ->where('providers_provider_id', $coach_id)
            ->where('facility_facility_id', session('active_facility'))
            ->get();

        $loggedInUser = session('user');
        $user = $this->user->where('email', $loggedInUser->userName)->first();

        if (empty($user) || count($response) != 0 ) {
            info('Error to Assign New Coach For Facility');
            return response()->json([
                'error' => trans('common.invalid_request_data'),
            ]);
        }
        info('Assign New Coach For Facility CoachID: ' . $coach_id . ' Request by: ' . $loggedInUser->userName . ' Facility: ' . session('active_facility'));

        $save = new FacilityProvider;
        $save->providers_provider_id    = $coach_id;
        $save->facility_facility_id     = session('active_facility');
        $save->save();

        if (!$save) {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'assign coach'])
            ]);
        }

        return response()->json(
            [
                'success' => 'Coach assign to this facility successfully.'
            ]
        );
    }



    /**
     * Get Detail of the Coach
     *
     * @return \Illuminate\Http\Response
     */
    public function coachEdit($coach_id = null) {
        $this->data['add_coach']    = false;
        if ($coach_id == 'add') {
            $this->data['coach']        = array();
            $this->data['state']      = state();
            $this->data['coach_type']   = coach_type();
            $this->data['add_coach']    = true;
            $this->data['ignore']       = '';
            $this->data['facility_list']= $this->coach_model->get_all_facility_list_for_admin($this->cobrand_id);
            return view( 'admin.coach.edit-coach', $this->data );
        }

        $response = $this->coach_model->get_facility_coach_list('',base64_decode($coach_id),$this->active_user_role);
        info('Total Coaches Found: ' . count($response));
        $data = collect($response);
        $this->data['coach']        = $data->first();

        $this->data['coach_type']   = coach_type();
        $this->data['state']      = state();
        $this->data['ignore']      = 'ignore';
        $this->data['facility_list']= $this->coach_model->get_all_facility_list_for_admin($this->cobrand_id);
        return view( 'admin.coach.edit-coach', $this->data );
    }



    /**
     * Update Details of the Coach
     *
     * @return \Illuminate\Http\Response
     */
    public function coachEditStore(Request $request) {

        if (request()->ajax()) {

            $user_id = base64_decode(request()->input('user_id'));
            $validator = Validator::make(request()->input(), [
                'user_id'       => 'required',
                'first_name'    => 'required',
                'last_name'     => 'required',
                //'coach_type'    => 'required',
                'email'         => 'required|unique:users,email,'. $user_id.',user_id',
                'phone'         => 'required',
                'city'         => 'required',
                'state'         => 'required',

            ]);

            if ($validator->fails()) {
                info('Coach Update Validation Failed');
                foreach ($validator->errors()->all() as $key => $value)
                {
                    return $response = [
                        'error' => $value,
                    ];
                    break;
                }
            }

            info('Coach update validation success');

            $data = User::find($user_id);
            $data->first_name       = $request->first_name;
            $data->last_name        = $request->last_name;
            $data->email            = $request->email;
            $data->phone            = $request->phone;
            $data->city            = $request->city;
            $data->state           = $request->state;
            $data->update();

            $provider = Provider::find(base64_decode($request->coach_id));
            $provider->is_sms_enabled       = $request->is_sms_enabled;
            $provider->is_email_enabled     = $request->is_email_enabled;
            $provider->acuity_link          = $request->acuity_link;
            $provider->update();


            if(count($request->facility_list)>0) {
                foreach ($request->facility_list as $facility_list_value) {


                    $facility_provider = FacilityProvider::where('facility_facility_id',$facility_list_value)->where('providers_provider_id',$provider->provider_id)->first();

                     if(isset($facility_provider->facility_provider_id))
                     {

                     }
                     else
                     {
                      $facility_provider = new FacilityProvider;
                      $facility_provider->providers_provider_id = $provider->provider_id;
                      $facility_provider->facility_facility_id = $facility_list_value;
                      $facility_provider->save();

                     }

                }
            }


            if (!$data || !$provider) {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'update coach'])
                ]);
            }

            Session::flash('message', trans( 'common.coach_edit' ));

            return $response = [
                'success' => 'Coach updated Successfully!',
                'redirect_url'=> route('admin.coaches'),
            ];
        }
    }


    /**
     * Add Details of the Coach
     *
     * @return \Illuminate\Http\Response
     */
    public function coachAddStore(Request $request) {


        if (request()->ajax()) {

            $user_id = base64_decode(request()->input('user_id'));
            $validator = Validator::make(request()->input(), [
                'first_name'    => 'required',
                'last_name'     => 'required',
                'coach_type'    => 'required',
                'email'         => 'required|unique:users,email,'. $user_id.',user_id',
                'phone'         => 'required',
                'city'         => 'required',
                'state'         => 'required',
            ]);

            if ($validator->fails()) {
                info('Add New Coach Validation Failed');
                foreach ($validator->errors()->all() as $key => $value)
                {
                    return $response = [
                        'error' => $value,
                    ];
                    break;
                }
            }

            info('Add New Coach validation success');

            $data = new User;
            $data->first_name       = $request->first_name;
            $data->last_name        = $request->last_name;
            $data->user_type        = $request->coach_type;
            $data->email            = $request->email;
            $data->phone            = $request->phone;
            $data->city            = $request->city;
            $data->state           = $request->state;
            $data->is_enabled       = 1;
            $data->registration_date  = date('Y-m-d H:i:s');
            $data->cobrand_id       = Session::get('user')->cobrandId  ;
            $data->save();
            info('Add New user successfully');
            $provider = new Provider;
            $provider->user_id              = $data->user_id;
            $provider->type                 = $request->coach_type;
            $provider->is_sms_enabled       = $request->is_sms_enabled;
            $provider->is_email_enabled     = $request->is_email_enabled;
            $provider->acuity_link          = $request->acuity_link;
            $provider->save();

            info('Add New provider successfully');

            if(count($request->facility_list)>0) {
                foreach ($request->facility_list as $facility_list_value) {
                    $facility_provider = new FacilityProvider;
                    $facility_provider->providers_provider_id = $provider->provider_id;
                    $facility_provider->facility_facility_id = $facility_list_value;
                    $facility_provider->save();
                }
            }
            info('Add New facility provider successfully');
            $user_role = new UserRole;
            $user_role->authority  = "ROLE_PROVIDER";
            $user_role->user_id    =    $data->user_id;
            $user_role->save();
            info('Add New user role successfully');

            if (!$data || !$provider || !$facility_provider || !$user_role) {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'add new coach'])
                ]);
            }

            /* email and message send  */

            $user_id = $data->user_id;
            $user_email = $data->email;
            $user_first_name = $data->first_name;
            $token = md5(uniqid(rand(), true));
            $token_expiry = time()* 1000 ;
            $sent_time  = time() ;
            $is_expired = 0;
            $pin_code =  rand ( 10000 , 99999 );
            $sms_body = "Please use following pin while resetting your password:" . $pin_code;
            if(Session::get('user')->cobrandId==2) {
                $link = env('APP_BASE_URL_SOLERA') . "token/" . $token;
                $email_body=  "<html><body><br/>Hello,<br/><br/> Please use the link below to create your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The HealthSlate Team<br/><br/></body></html>";
            }
            else {
                  $link = env('APP_BASE_URL') . "token/" . $token;
                $email_body=  "<html><body><br/>Hello,<br/><br/> Please use the link below to create your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The HealthSlate Team<br/><br/></body></html>";
            }


            EmailNotificationMessage::where('user_id', '=', $user_id)->update(array('is_expired' => DB::raw(1)));
            $email_notification =  new EmailNotificationMessage;
            $email_notification->user_id = $user_id;
            $email_notification->email_body = $email_body;
            $email_notification->sms_body = $sms_body;
            $email_notification->token = $token;
            $email_notification->token_expiry = $token_expiry;
            $email_notification->pin_code = $pin_code;
            $email_notification->sent_time = $sent_time;
            $email_notification->is_expired = DB::raw(0);
            $email_notification->save();
            $name = $user_first_name;
            if(Session::get('user')->cobrandId==2)
                $message = "<html><body>You're receiving this e-mail because you forgot your SoleraOne Program account password. Please use the link below to reset your password: <br/><a href='" . $link . "'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";
            else
                $message = "<html><body>You're receiving this e-mail because you forgot your HealthSlate Program account password. Please use the link below to reset your password: <br/><a href='" . $link . "'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";
            $body = ['name'=>$name,'content' => $message,'subject' =>'Reset Password Request' ];
            $to = $user_email;
            info( 'Processing send email request' );
            //mail sending function is call here
            $this->mailServiceHelper->sendEmail($body, trim($to));

            if (preg_match_all("/[\+]/", $data->phone)!=1)
                $sms_to = "+1".$data->phone;
            else
                $sms_to = $data->phone;

            info( 'Processing send message request' );
            //sms sending function is call here
            if($data->phone!=""){
                $return_sms_id = $this->smsServiceHelper->sendSms($sms_body,$sms_to);
                if($return_sms_id)
                    info( " Message Send Successfully  to user id :  $user_id ");
                else
                    info( " Message not Send Successfully to user id : $user_id" );
            }



            /*  */
            Session::flash('message', trans( 'common.new_coach_add' ));

            return $response = [
                'success' => 'Add new coach Successfully!',
                'redirect_url'=> route('admin.coaches'),
            ];
        }
    }

    /**
     * Coach reset password function

     */

    public function coachResetPassword(Request $request)
    {
        //fetch user information
        info('fetch user information');
        $user = User::where('user_id', $request->user_id)->where('user_type','!=', 'PATIENT')->first();
        if(empty($user))
        {

            info('user data not found in database user id : '.$request->user_id);
            return response()->json([
                'error' => 'User not found into our system.'
            ]);
        }

        if(!empty($user))
        {


            $user_id = $user->user_id;
            $user_email = $user->email;
            $user_first_name = $user->first_name;
            $token = md5(uniqid(rand(), true));
            $token_expiry = time()* 1000 ;
            $sent_time  = time() ;
            $is_expired = 0;
            $pin_code =  rand ( 10000 , 99999 );
            if(Session::get('user')->cobrandId==2){
                $link =  env('APP_BASE_URL_SOLERA')."token/".$token;
                $sms_body = "Please use following pin while resetting your password:".$pin_code;
                $email_body=  "<html><body><br/>Hello,<br/><br/>You're receiving this e-mail because you forgot your SoleraOne Program account password. Please use the link below to reset your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The SoleraOne Team<br/><br/></body></html>";

            }else{
                $link =  env('APP_BASE_URL')."token/".$token;
                $sms_body = "Please use following pin while resetting your password:".$pin_code;
                $email_body=  "<html><body><br/>Hello,<br/><br/>You're receiving this e-mail because you forgot your HealthSlate Program account password. Please use the link below to reset your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The HealthSlate Team<br/><br/></body></html>";

            }
             EmailNotificationMessage::where('user_id', '=', $user_id)->update(array('is_expired' => DB::raw(1)));
            $email_notification =  new EmailNotificationMessage;
            $email_notification->user_id = $user_id;
            $email_notification->email_body = $email_body;
            $email_notification->sms_body = $sms_body;
            $email_notification->token = $token;
            $email_notification->token_expiry = $token_expiry;
            $email_notification->pin_code = $pin_code;
            $email_notification->sent_time = $sent_time;
            $email_notification->is_expired = DB::raw(0);
            $email_notification->save();
            $name = $user_first_name;
            if(Session::get('user')->cobrandId==2){
                $message ="<html><body>You're receiving this e-mail because you forgot your SoleraOne Program account password. Please use the link below to reset your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";
            }else{
                $message ="<html><body>You're receiving this e-mail because you forgot your HealthSlate Program account password. Please use the link below to reset your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";

            }
            $body = ['name'=>$name,'content' => $message,'subject' =>'Reset Password Request' ];
            $to = $user_email;
            info( 'Processing send email request' );
            //mail sending function is call here
            $this->mailServiceHelper->sendEmail($body, trim($to));
            if (preg_match_all("/[\+]/", $user->phone)!=1)
                $sms_to = "+1".$user->phone;
            else
                $sms_to = $user->phone;

            info( 'Processing send message request' );
            //sms sending function is call here
            if($user->phone!=""){
                $return_sms_id = $this->smsServiceHelper->sendSms($sms_body,$sms_to);
                if($return_sms_id)
                    info( " Message Send Successfully  to user id :  $user_id ");
                else
                    info( " Message not Send Successfully to user id : $user_id" );
            }
        }

        return response()->json(
            [
                'success' => 'An email has been sent successfully to your email address.'
            ]
        );
    }



     public function editFacilityAdmin(Request $request)
     {

         $coach_id = $request->provider_id;
         $response = $this->coach_model->get_admin_facility_list('',base64_decode($coach_id),$this->active_user_role);
         //print_r($response); exit;
         info('Total Coaches Found: ' . count($response));
         $data = collect($response);
         $facility_list = $data->first();
         $facility_id="";
         $facility_name="";
         if($facility_list->facility_id!="")
         $facility_id = explode(",",$facility_list->facility_id);
         if($facility_list->facility_names!="")
         $facility_name = explode(", ",$facility_list->facility_names);



         return response()->json(
             [
                 'success' => $data->first(),
                 'facility_name'=> $facility_name,
                 'facility_id'=> $facility_id,

             ]
         );



     }

    public function updateFacilityAdmin(Request $request)
    {

        $user_id = base64_decode($request->provider_id);
        $user = User::find($user_id);
        $user->first_name       = $request->admin_fname;
        $user->last_name        = $request->admin_lname;
        $user->phone            = $request->admin_phone;
        $user->update();

        $provider = Provider::Where('user_id',$user_id)->first();
        $provider_id = $provider->provider_id;


        if(count($request->select_facility)>0)
        {

            foreach ($request->select_facility as $facility_list_value) {
                $facility_provider = FacilityProvider::where('facility_facility_id',$facility_list_value)->where('providers_provider_id',$provider_id)->first();
                if(isset($facility_provider->facility_provider_id))
                {


                }
                else
                {
                    $facility_provider = new FacilityProvider;
                    $facility_provider->providers_provider_id = $provider_id;
                    $facility_provider->facility_facility_id = $facility_list_value;
                    $facility_provider->save();

                }

            }


        }

        return response()->json(
            [
                'success' => "Admin Updated successfully",


            ]
        );



    }

}

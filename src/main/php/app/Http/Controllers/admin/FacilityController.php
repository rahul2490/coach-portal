<?php

namespace HealthSlatePortal\Http\Controllers\admin;

use Illuminate\Http\Request;
use Validator;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\Eloquent\FacilityProvider;
use Illuminate\Support\Facades\Log;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\UserRole;
use HealthSlatePortal\Models\Eloquent\Provider;
use HealthSlatePortal\Models\Eloquent\UserPasswordHistory;
use HealthSlatePortal\Models\Eloquent\EmailNotificationMessage;
use HealthSlatePortal\Helpers\MailServiceHelper;
use HealthSlatePortal\Helpers\SmsServiceHelper;
use Session;
use Hash;
use DB;

class FacilityController extends Controller {

    protected $facility_model;
    protected $coach_model;
    protected $facility;
    protected $facilityProvider;
    protected $data = array();


    function __construct(MailServiceHelper $mailServiceHelper,SmsServiceHelper $smsServiceHelper,FacilityProvider $facilityProvider, FacilityModel $facility_model,CoacheModel $coach_model, Facility $facility, User $user ) {
        $this->mailServiceHelper = $mailServiceHelper;
        $this->smsServiceHelper =  $smsServiceHelper;
        $this->facility_model  = $facility_model;
        $this->coach_model = $coach_model;
        $this->facility        = $facility;
        $this->facilityProvider = $facilityProvider;
        $this->user = $user;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $cobrand_id       = Session::get('user')->cobrandId  ;
        $this->data['facility']    = $this->facility_model->get_all_facility_list($cobrand_id);
        $this->data['facility_list']= $this->coach_model->get_all_facility_list_for_admin($cobrand_id);
        //print_r($facility_data);die;

       // $this->data['facility']  = Facility::where('cobrand_id', '=', 1)->get();

        return view( 'admin.facility.facility',$this->data);
    }

    /*-------------------------Edit facility------------------*/

    public function edit_facility($id)
    {
        $facility_id = base64_decode($id);
        $cobrand_facility_admins    = $this->facility_model->get_cobrand_all_facility_admin_list();
       // print_r($cobrand_facility_admins);
        $facility_admin             = $this->facility_model->get_facility_admin_list($facility_id);
        //print_r($facility_admin);
        $admin_ids = [];
        if(!empty($facility_admin))
        {
            foreach ($facility_admin as $admin) {
                $admin_ids[] = $admin->provider_id;
            }
        }

        $this->data['cobrand_facility_admins'] = collect($cobrand_facility_admins)->filter( function ( $provider ) use($admin_ids) {
            return !in_array( $provider->provider_id, $admin_ids );
        } );
        info( 'Total Co-Brand Coach: ' . count($this->data['cobrand_facility_admins']));

        $this->data['facility_admin'] = collect($facility_admin)->filter( function ( $provider ) {
            return !in_array( $provider->email, [session('user')->userName] );
        } );
        info( 'Total Facility Admin: ' . count($this->data['facility_admin']));

        $this->data['facility']  = Facility::where('facility_id', '=', $facility_id)->first();
        $this->data['state']                    = state();
        $this->data['timezone']                 = timezone();

        return view( 'admin.facility.edit_admin_facility',$this->data);

    }

    /* ----------- add facility ----------- */
    public function add_facility(){
        $this->data['state']                    = state();
        $this->data['timezone']                 = timezone();
        $this->data['cobrand_facility_admins']   = $this->facility_model->get_cobrand_all_facility_admin_list();
        return view( 'admin.facility.add_admin_facility', $this->data);
    }


    public function create_new_facility_admin(Request $request){
        if (request()->ajax()) {
            $email = $request->admin_email;
            $facility_list = $request->select_facility;
            $response = User::where('email', $email)->get();
            //print_r(count($response));die;
            if (count($response) == 0) {

                $data = new User;
                $data->first_name = $request->admin_fname;
                $data->last_name = $request->admin_lname;
                $data->is_enabled = 1;
                $data->email = $request->admin_email;
                $data->phone = $request->admin_phone;
                $data->user_type = 'ROLE_FACILITY_ADMIN';
                $data->cobrand_id = Session::get('user')->cobrandId;
                $data->save();

                $user_id = $data->user_id;
                $email= $data->email;
                $first_name=$data->first_name;
                $phone = $data->phone;

                $data = new UserRole;
                $data->authority = "ROLE_FACILITY_ADMIN";
                $data->user_id = $user_id;
                $data->save();

                $data = new Provider;
                $data->type = "Facility Admin";
                //$data->facility_id = $value;
                $data->user_id = $user_id;
                $data->save();
                $provider_id = $data->provider_id;
                info('Assign New Facility  For Facility Admin: ');

                if(count($facility_list)>0) {
                    foreach ($facility_list as $key => $value) {


                        $save = new FacilityProvider;
                        $save->providers_provider_id = $provider_id;
                        $save->facility_facility_id = $value;
                        $save->save();
                    }
                }
                info('assign admin to facility');

                /* email and message send  */

                $user_id = $user_id;
                $email= $email;
                $user_first_name = $first_name;
                $token = md5(uniqid(rand(), true));
                $token_expiry = time()* 1000 ;
                $sent_time  = time() ;
                $is_expired = 0;
                $pin_code =  rand ( 10000 , 99999 );
                $sms_body = "Please use following pin while resetting your password:" . $pin_code;
                if(Session::get('user')->cobrandId==2) {
                    $link = env('APP_BASE_URL_SOLERA') . "token/" . $token;
                    $email_body=  "<html><body><br/>Hello,<br/><br/> Please use the link below to create your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The HealthSlate Team<br/><br/></body></html>";
                }
                else {
                    $link = env('APP_BASE_URL') . "token/" . $token;
                    $email_body=  "<html><body><br/>Hello,<br/><br/> Please use the link below to create your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The HealthSlate Team<br/><br/></body></html>";
                }


                EmailNotificationMessage::where('user_id', '=', $user_id)->update(array('is_expired' => DB::raw(1)));
                $email_notification =  new EmailNotificationMessage;
                $email_notification->user_id = $user_id;
                $email_notification->email_body = $email_body;
                $email_notification->sms_body = $sms_body;
                $email_notification->token = $token;
                $email_notification->token_expiry = $token_expiry;
                $email_notification->pin_code = $pin_code;
                $email_notification->sent_time = $sent_time;
                $email_notification->is_expired = DB::raw(0);
                $email_notification->save();
                $name = $user_first_name;
                if(Session::get('user')->cobrandId==2)
                    $message = "<html><body>You're receiving this e-mail because you forgot your SoleraOne Program account password. Please use the link below to reset your password: <br/><a href='" . $link . "'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";
                else
                    $message = "<html><body>You're receiving this e-mail because you forgot your HealthSlate Program account password. Please use the link below to reset your password: <br/><a href='" . $link . "'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";
                $body = ['name'=>$name,'content' => $message,'subject' =>'Reset Password Request' ];
                $to = $email;
                info( 'Processing send email request' );
//mail sending function is call here
                $this->mailServiceHelper->sendEmail($body, trim($to));

                if (preg_match_all("/[\+]/", $phone)!=1)
                    $sms_to = "+1".$phone;
                else
                    $sms_to = $phone;

                info( 'Processing send message request' );
//sms sending function is call here
                if($phone!=""){
                    $return_sms_id = $this->smsServiceHelper->sendSms($sms_body,$sms_to);
                    if($return_sms_id)
                        info( " Message Send Successfully  to user id :  $user_id ");
                    else
                        info( " Message not Send Successfully to user id : $user_id" );
                }

                /*email and sms send code end*/
                return response()->json([
                    'success' => 'Facility admin added successfully.',
                ]);

            } else {
                return response()->json([
                    'error' => 'Facility admin email already exist.',
                ]);
            }
        }
    }






    /**
     * facility store function
     * (it's update data in facility table)
     */

    public function facilityStore(Request $request)
    {

        if ($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'name'                  => 'required',
                'state'                 => 'required',
                'city'                  => 'required',
                'zip'                   => 'required',
                'address'               => 'required',
                'timezone'              => 'required',
                'contact_info'          => 'required',
                'contact_person_name'   => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('admin/facility')
                    ->withErrors($validator)
                    ->withInput();
            }
            $facility_id=$request->facility_id;


            $data = Facility::find($facility_id);
            $data->name                         = $request->name;
            $data->state                        = $request->state;
            $data->city                         = $request->city;
            $data->zip                          = $request->zip;
            $data->address                      = $request->address;
            $data->timezone_name                = timezone()[$request->timezone];
            $data->timezone_offset              = $request->timezone;
            $data->timezone_offset_millis       = $request->timezone * 60 * 60 * 1000;
            $data->contact_info                 = $request->contact_info;
            $data->contact_person_name          = $request->contact_person_name;
            $data->partner_id                   = $request->partner_id;
            $data->cdc_organization_code        = $request->cdc_organization_code;
            $data->is_notification_enabled      = isset($request->is_notification_enabled) ? $request->is_notification_enabled: DB::raw(0);
            $data->is_skip_consent              = isset($request->is_skip_consent) ? $request->is_skip_consent: DB::raw(0);
            $data->is_chatbot_enabled           = isset($request->is_chatbot_enabled) ? $request->is_chatbot_enabled: 0;
            $data->update();
            info( 'facility update succesfully' );

//            $facility = [];
//            foreach(session('facility') as $key => &$meal)
//            {
//                if( $facility_id == $meal->facility_id)
//                {
//                    $facility[] = (object) [
//                        'provider_id'   =>  $meal->provider_id,
//                        'name'          =>  $request->name,
//                        'facility_id'   =>  $meal->facility_id
//                    ];
//                }
//                else
//                {
//                    $facility[] = (object) [
//                        'provider_id'   =>  $meal->provider_id,
//                        'name'          =>  $meal->name,
//                        'facility_id'   =>  $meal->facility_id
//                    ];
//                }
//            }
//            Session::put( 'facility', $facility);
            return redirect()->route('admin.facility')->with('success', 'Facility Updated Successfully!');
        }
    }


    public function addfacilityStore(Request $request)
    {


        if ($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'name'                  => 'required',
                'state'                 => 'required',
                'city'                  => 'required',
                'zip'                   => 'required',
                'address'               => 'required',
                'timezone'              => 'required',
                'contact_info'          => 'required',
                'contact_person_name'   => 'required',

            ]);

            if ($validator->fails()) {
                return redirect('admin/facility')
                    ->withErrors($validator)
                    ->withInput();
            }

          if($request->admin_email!="") {
              $email_count = User::where('email', $request->admin_email)->get();
              if (count($email_count) > 0)
                  return redirect()->route('admin.add_facility')->with('success', 'Facility Admin  already exist');

          }



                // $facility_id=$request->facility_id;


           // $data = Facility::find($facility_id);
            $data = new Facility;
            $data->name                         = $request->name;
            $data->state                        = $request->state;
            $data->city                         = $request->city;
            $data->zip                          = $request->zip;
            $data->address                      = $request->address;
            $data->timezone_name                = timezone()[$request->timezone];
            $data->timezone_offset              = $request->timezone;
            $data->timezone_offset_millis       = $request->timezone * 60 * 60 * 1000;
            $data->contact_info                 = $request->contact_info;
            $data->contact_person_name          = $request->contact_person_name;
            $data->partner_id                   = $request->partner_id;
            $data->cdc_organization_code        = $request->cdc_organization_code;
            $data->is_notification_enabled      = isset($request->is_notification_enabled) ? $request->is_notification_enabled: DB::raw(0);
            $data->is_skip_consent              = isset($request->is_skip_consent) ? $request->is_skip_consent: DB::raw(0);
            $data->is_chatbot_enabled           = isset($request->is_chatbot_enabled) ? $request->is_chatbot_enabled: 0;
            $data->cobrand_id       = Session::get('user')->cobrandId  ;


            $data->save();
            info( 'facility insert succesfully' );
            $facility_id = $data->facility_id;

            $facility_admin = $request->select_facility_admin;

            if(count($request->select_facility_admin) == 0 && $request->admin_fname=="" && $request->admin_lname=="" && $request->admin_email=="" && $request->admin_phone=="")
            {
                return redirect()->route('admin.add_facility')->with('error', 'Unable to assign facility admin, Please contact admin or try again after some time');

            }

            $response = $this->facilityProvider
                ->whereIn('providers_provider_id', $facility_admin)
                ->where('facility_facility_id', $facility_id)
                ->get();

            $loggedInUser = session('user');

            if (count($response) != 0 ) {
                info('Facility admin already exist.');

                return redirect()->route('admin.add_facility')->with('error', 'Facility admin already exist.');
            }

            $save = false;
            if(count($facility_admin)>0) {
                foreach ($facility_admin as $key => $value) {
                    info('Assign New Facility Admin For Facility: ' . $facility_id . ' Request by: ' . $loggedInUser->userName . ' Facility: ' . $value);
                    $save = new FacilityProvider;
                    $save->providers_provider_id = $value;
                    $save->facility_facility_id = $facility_id;
                    $save->save();
                }
            }
            info( 'assign admin to facility' );

            $email= $request->admin_email;
            $response = User::where('email', $email)->get();
            //print_r(count($response));die;
                if (count($response) == 0 ) {
                $data = new User;
                $data->first_name = $request->admin_fname;
                $data->last_name = $request->admin_lname;
                $data->is_enabled = 1;
                $data->email = $request->admin_email;
                $data->phone = $request->admin_phone;
                $data->user_type = 'ROLE_FACILITY_ADMIN';
                $data->cobrand_id = Session::get('user')->cobrandId;
                $data->save();

                $user_id = $data->user_id;
                $email= $data->email;
                $first_name = $data->first_name;
                $phone= $data->phone;

                $data = new UserRole;
                $data->authority = "ROLE_FACILITY_ADMIN";
                $data->user_id = $user_id;
                $data->save();

                $data = new Provider;
                $data->type = "Facility Admin";
                $data->facility_id = $facility_id;
                $data->user_id = $user_id;
                $data->save();

                $provider_id = $data->provider_id;

                $data = new facilityProvider;
                $data->providers_provider_id = $provider_id;
                $data->facility_facility_id = $facility_id;
                $data->save();


                /* email and message send  */

                /*$user_id = $user_id;
                $email= $email;*/
                $user_first_name = $first_name;
                $token = md5(uniqid(rand(), true));
                $token_expiry = time()* 1000 ;
                $sent_time  = time() ;
                $is_expired = 0;
                $pin_code =  rand ( 10000 , 99999 );
                $sms_body = "Please use following pin while resetting your password:" . $pin_code;
                if(Session::get('user')->cobrandId==2) {
                    $link = env('APP_BASE_URL_SOLERA') . "token/" . $token;
                    $email_body=  "<html><body><br/>Hello,<br/><br/> Please use the link below to create your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The HealthSlate Team<br/><br/></body></html>";
                }
                else {
                    $link = env('APP_BASE_URL') . "token/" . $token;
                    $email_body=  "<html><body><br/>Hello,<br/><br/> Please use the link below to create your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The HealthSlate Team<br/><br/></body></html>";
                }


                EmailNotificationMessage::where('user_id', '=', $user_id)->update(array('is_expired' => DB::raw(1)));
                $email_notification =  new EmailNotificationMessage;
                $email_notification->user_id = $user_id;
                $email_notification->email_body = $email_body;
                $email_notification->sms_body = $sms_body;
                $email_notification->token = $token;
                $email_notification->token_expiry = $token_expiry;
                $email_notification->pin_code = $pin_code;
                $email_notification->sent_time = $sent_time;
                $email_notification->is_expired = DB::raw(0);
                $email_notification->save();
                $name = $user_first_name;
                if(Session::get('user')->cobrandId==2)
                    $message = "<html><body>You're receiving this e-mail because you forgot your SoleraOne Program account password. Please use the link below to reset your password: <br/><a href='" . $link . "'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";
                else
                    $message = "<html><body>You're receiving this e-mail because you forgot your HealthSlate Program account password. Please use the link below to reset your password: <br/><a href='" . $link . "'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";
                $body = ['name'=>$name,'content' => $message,'subject' =>'Reset Password Request' ];
                $to = $email;
                info( 'Processing send email request' );
//mail sending function is call here
                $this->mailServiceHelper->sendEmail($body, trim($to));

                if (preg_match_all("/[\+]/", $phone)!=1)
                    $sms_to = "+1".$phone;
                else
                    $sms_to = $phone;

                info( 'Processing send message request' );
//sms sending function is call here
                if($phone!=""){
                    $return_sms_id = $this->smsServiceHelper->sendSms($sms_body,$sms_to);
                    if($return_sms_id)
                        info( " Message Send Successfully  to user id :  $user_id");
                    else
                        info( " Message not Send Successfully to user id : $user_id" );
                }

                /*email and sms send code end*/
            }


            $facility = [];
            foreach(session('facility') as $key => &$meal)
            {
                if( $facility_id == $meal->facility_id)
                {
                    $facility[] = (object) [
                        'provider_id'   =>  $meal->provider_id,
                        'name'          =>  $request->name,
                        'facility_id'   =>  $meal->facility_id
                    ];
                }
                else
                {
                    $facility[] = (object) [
                        'provider_id'   =>  $meal->provider_id,
                        'name'          =>  $meal->name,
                        'facility_id'   =>  $meal->facility_id
                    ];
                }
            }
            Session::put( 'facility', $facility);


            return redirect()->route('admin.add_facility')->with('success', 'Facility Inserted Successfully!');
        }
    }





    /**
     * Process assign Facility to Facility Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignFacility() {

        $facility_id = request('facility_id');

        $facility_admin = request('select_facility_admin' , 0);

        if($facility_admin == 0)
        {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'assign facility admin'])
            ]);
        }

        $response = $this->facilityProvider
            ->whereIn('providers_provider_id', $facility_admin)
            ->where('facility_facility_id', $facility_id)
            ->get();

        $loggedInUser = session('user');

        if (count($response) != 0 ) {
            info('Facility admin already exist.');
            return response()->json([
                'error' => 'Facility admin already exist.',
            ]);
        }

        $save = false;
        foreach ($facility_admin as $key => $value)
        {
            info('Assign New Facility Admin For Facility: ' . $facility_id . ' Request by: ' . $loggedInUser->userName . ' Facility: ' . $value);
            $save = new FacilityProvider;
            $save->providers_provider_id    = $value;
            $save->facility_facility_id     = $facility_id;
            $save->save();
        }

        if (!$save) {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'assign coach'])
            ]);
        }

        return response()->json(
            [
                'success' => 'Facility admin assign to this facility successfully!'
            ]
        );
    }

    public function delete_facility_admin(){
         $coach_id = request('admin_id');
         $facility_id = request('facility_id');
         $deleted = false;

         $response = $this->facilityProvider
            ->where('providers_provider_id', $coach_id)
            ->where('facility_facility_id', $facility_id)
            ->get();
        $loggedInUser = session('user');
        //echo $loggedInUser->userName;

        $user = $this->user->where('email',
            $loggedInUser->userName)->first();

        if (empty($user) || empty($response)) {
            info('Delete Coach From Facility');
            return response()->json([
                'error' => trans('common.invalid_request_data'),
            ]);
        }
        info('Deleting Coach From Facility CoachID: ' . $coach_id . ' Request by: ' . $loggedInUser->userName);
        $deleted = $this->facilityProvider
            ->where('providers_provider_id', $coach_id)
            ->where('facility_facility_id', $facility_id)
            ->delete();
        if (!$deleted) {
            return response()->json([
                'error' => trans('common.unable_to_do_action',
                    [ 'action' => 'remove coach'])
            ]);
        }
        return response()->json(
            [
                'success' => 'Coach removed from this facility Successfully.'
            ]
        );
    }





}

<?php

namespace HealthSlatePortal\Http\Controllers\facilityadmin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\Eloquent\Provider;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CurriculumServiceProvider;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\Patient;
use Illuminate\Support\Facades\Log;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\PatientHideUnhide;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\Message;
use HealthSlatePortal\Models\Eloquent\SessionAttendance;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use Edujugon\PushNotification\PushNotification;
use DB;

class MemberController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $user;
    protected $provider;
    protected $dataTableService;
    protected $group_model;
    protected $group_member;
    protected $curriculumServiceProvider;
    protected $data = array();

    function __construct( FacilityModel $facility_model, MemberModel $member_model, Facility $facility, CoacheModel $coach_model,
                          DataTableServiceProvider $dataTableService, User $user, Provider $provider, GroupModel $group_model, GroupMember $group_member, CurriculumServiceProvider $curriculumServiceProvider) {
        $this->facility_model       = $facility_model;
        $this->coach_model          = $coach_model;
        $this->member_model         = $member_model;
        $this->group_member         = $group_member;
        $this->group_model          = $group_model;
        $this->facility             = $facility;
        $this->user                 = $user;
        $this->provider             = $provider;
        $this->dataTableService     = $dataTableService;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->group_db             = env('GROUP_DB_DATABASE');
        $this->active_facility_id   = session('active_facility');
        $this->active_user_id       = session('userId');
        $this->active_user_role     = session('userRole');
        $this->data['js']           = array('message', 'member','member-recruitment');
        $this->data['common_js']    = array('tagging/jquery-migrate-1.2.1', 'tagging/jquery.caretposition', 'tagging/jquery.sew');
    }


    /**
     * To render member page
     * (it's show member page)
     */
	public function index($group_id = '') {

        $timezone = '';
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }

        $today_date = Carbon::now();
        if(!empty($timezone))
            $today_date = Carbon::createFromFormat('Y-m-d H:i:s', $today_date)->timezone($timezone);
        else
            $today_date = Carbon::createFromFormat('Y-m-d H:i:s', $today_date);


        $group_list = $this->group_model->get_all_group_list($this->active_facility_id);
        $today_birthdays_count  = $this->group_model->get_today_birthday_data($this->active_facility_id,$today_date);

        $facility_group_list = array();
        if(!empty($group_list))
        {
            $facility_group_list[''] = 'Select Group';
            foreach ($group_list as $key => $value)
            {
                $facility_group_list[$value->group_id] = $value->name;
            }
        }
        $this->data['today_birthdays_count']= $today_birthdays_count->birthday_count;
        $this->data['facility_group_list']  = $facility_group_list;
        $this->data['member_group_id']      = base64_decode($group_id);
        info('Total Group Found: ' . count($facility_group_list));
        return view( 'facilityadmin.member.member', $this->data);
	}


    /**
     * member list function
     * (it's load data on member page )
     */
    public function membersList(Request $request)
    {
        ini_set("memory_limit",-1);
        ini_set('max_execution_time', 0);
        $timezone = '';
        if (isset($_COOKIE['user_timezone'])) {
            $timezone = $_COOKIE['user_timezone'];
        }

        $today_date = Carbon::now();
        $today_date =strtotime($today_date)*1000;
        $pastweek_date = Carbon::now()->copy()->startOfDay()->subDays(28);
        $pastweek_date =strtotime($pastweek_date)*1000;

        $start                  = request('start', 0);
        $length                 = request('length', 0);
        $datatable_request      = array('start' => $start, 'length' => $length);

        $member_filter  = $request->member_filter;
        $group_filter   = $request->group_filter;
        $another_filter   = $request->another_filter;
        $birthday_date="";
        if($request->birthdays_today && $request->birthdays_today!="")
        {
            $birthday_date=  Carbon::now();
            if(!empty($timezone))
                $birthday_date = Carbon::createFromFormat('Y-m-d H:i:s', $birthday_date)->timezone($timezone);
            else
                $birthday_date = Carbon::createFromFormat('Y-m-d H:i:s', $birthday_date);

            $member_filter = "";
            $group_filter = "";
            $another_filter = "";
        }

        $response       = $this->member_model->get_member_list($this->active_facility_id, $member_filter, $group_filter,$another_filter, $this->active_user_id, $datatable_request, $birthday_date);
        $response_total = count($response);
        $response       = collect($response);

        /*$patient_ids    = $response->pluck( 'patient_id' )->toArray();
        if(0)
        {
            $message_detail = $this->member_model->get_member_message_detail($patient_ids);
            foreach ($response as $row) {
                foreach ($message_detail as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->total_unread_message            = $value->total_unread_message;
                        $row->last_message_sent_to_patient    = $value->last_message_sent_to_patient;
                        $row->last_unread_message_of_patient  = $value->last_unread_message_of_patient;
                    }
                }
            }
            $hide_unhide_detail = $this->member_model->get_member_hide_unhide_detail($patient_ids, $this->active_user_id);
            foreach ($response as $row) {
                foreach ($hide_unhide_detail as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->hide_unhide   = $value->hide_unhide;
                        $row->pin_unpin     = $value->pin_unpin;
                    }
                }
            }
            $patient_metrics_detail = $this->member_model->get_member_patient_metrics_detail($patient_ids, $this->active_user_id);
            foreach ($response as $row) {
                foreach ($patient_metrics_detail as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->engagement_score                  = $value->engagement_score;
                        $row->is_at_milestone_risk              = $value->is_at_milestone_risk;
                        $row->is_missing_starting_weight        = $value->is_missing_starting_weight;
                        $row->is_weight_increasing              = $value->is_weight_increasing;
                        $row->is_not_losing__wt_low_engagement  = $value->is_not_losing__wt_low_engagement;
                        $row->is_decrease_in_weight_logging     = $value->is_decrease_in_weight_logging;
                        $row->is_engagement_declining           = $value->is_engagement_declining;
                        $row->is_no_step_reported_3_days        = $value->is_no_step_reported_3_days;
                        $row->is_not_responded_to_2_messages    = $value->is_not_responded_to_2_messages;
                        $row->is_avg_step_declining             = $value->is_avg_step_declining;
                        $row->is_on_track                       = $value->is_on_track;
                    }
                }
            }
        }*/

        info('Total Members Found: ' . count($response));

        $sorting_status = true;

        $data    = collect();
        $danger  = collect();
        $warning = collect();
        $success = collect();
        $white   = collect();
        $active  = collect();
        $final_data = collect();

        $color_class = '';
        $status = '';

        foreach ($response as $member) {
            $check_engagement   = get_engagement_score($member);
            $color_class        = $check_engagement['color_class'];
            $status             = $check_engagement['status'];

            $member->color_class    = $color_class;
            $member->color_status   = $status;
            $member->status_type    = $check_engagement['status_type'];

            if($sorting_status)
            {
                if($member->hide_unhide == 1)
                    $active->push($member);
                else if($color_class == 'danger')
                    $danger->push($member);
                else if($color_class == 'warning')
                    $warning->push($member);
                else if($color_class == 'success')
                    $success->push($member);
                else if($color_class == 'white')
                    $white->push($member);
                else if($color_class == 'active')
                    $active->push($member);
            }
            else
            {
                $active->push($member);
            }
        }
        $danger   = $danger->sortByDesc('pin_unpin');
        $warning  = $warning->sortByDesc('pin_unpin');
        $success  = $success->sortByDesc('pin_unpin');

        $data = $data->merge($danger);
        $data = $data->merge($warning);
        $data = $data->merge($success);

        $white_sorting_for_pin      = collect();
        $white_sorting_for_unpin    = collect();
        foreach ($white as $white_sorting) {
            if($white_sorting->pin_unpin == 1)
                $white_sorting_for_pin->push($white_sorting);
            else
                $white_sorting_for_unpin->push($white_sorting);
        }

        $active_sorting_for_pin       = collect();
        $active_sorting_for_unpin     = collect();
        foreach ($active as $active_sorting) {
            if($active_sorting->pin_unpin == 1)
                $active_sorting_for_pin->push($active_sorting);
            else
                $active_sorting_for_unpin->push($active_sorting);
        }

        $data = $data->merge($white_sorting_for_pin);
        $data = $data->merge($white_sorting_for_unpin);

        $data = $data->merge($active_sorting_for_pin);
        $data = $data->merge($active_sorting_for_unpin);

        $data = $data->slice($request['start'], $request['length']);

        $patient_ids    = $data->pluck( 'patient_id' )->toArray();
        $member_message_detail      = $this->member_model->get_member_message_detail($patient_ids,$user_id='');
        if(!empty($member_message_detail))
        {
            foreach ($response as $row) {
                foreach ($member_message_detail as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->total_unread_message            = $value->total_unread_message;
                        //$row->last_message_sent_to_patient    = $value->last_message_sent_to_patient;
                        $row->last_unread_message_of_patient  = $value->last_unread_message_of_patient;
                    }
                }
            }
        }


        $get_weight_detail  = $this->member_model->get_patient_current_weight_date($patient_ids,$pastweek_date,$today_date);
        if(!empty($get_weight_detail))
        {
            foreach ($response as $row) {
                foreach ($get_weight_detail as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->last_weight_date  = $value->last_weight_date;
                        //$row->past_week_count   = $value->past_week_count;

                    }
                }
            }
        }

        $get_engagement_score  = $this->member_model->get_patient_engagement_score($patient_ids);
        if(!empty($get_engagement_score))
        {
            foreach ($response as $row) {
                foreach ($get_engagement_score as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->prior_week_engagement_score       = round(($value->prior_week_engagement_score / (($value->prior_week_engagement_score_total_days == 0 )? 1 : $value->prior_week_engagement_score_total_days)),0);
                        $row->current_week_engagement_score     = round(($value->current_week_engagement_score / (($value->current_week_engagement_score_total_days == 0) ? 1 : $value->current_week_engagement_score_total_days)),0);
                    }
                }
            }
        }


        $last_sent_member_detail  = $this->member_model->get_last_message_sent_to_patient($patient_ids, $this->active_user_id);
        if(!empty($last_sent_member_detail))
        {
            foreach ($response as $row) {
                foreach ($last_sent_member_detail as $value) {
                    if($row->patient_id == $value->patient_id)
                    {
                        $row->last_message_sent_to_patient    = $value->last_message_sent_to_patient;
                    }
                }
            }
        }

        $recommended_message_response   = collect($this->member_model->get_recommended_message_list());
        $count = 1;
        $milestone_risk = '';
        foreach ($data as $member) {

            $color_class         = $member->color_class;
            $status              = $member->color_status;
            $status_type         = $member->status_type;


            if($color_class == 'danger' || $color_class == 'warning')
            {
                if($count == 1){
                    $in_app_message_list  = collect($this->member_model->get_in_app_message_list($patient_ids));
                    $count += 1;
                }
                $message_text = '';
                $message_type         = $recommended_message_response->where('status', $status_type);
                $in_app_message_type  = $in_app_message_list->where('patient_id', $member->patient_id);

                if(count($in_app_message_type) > 0 && count($message_type) > 0)
                {
                    $count_sent_message = 0;
                    $predefined_message_id = [];
                    foreach ($in_app_message_type as $sent_message)
                    {
                        $check_sent_message = collect($message_type)->where('message_text', $sent_message->message)->first();

                        if(count($check_sent_message) > 0)
                        {
                            $count_sent_message += 1;
                            $predefined_message_id[] = $check_sent_message->predefined_message_id;
                            //echo $sent_message->message_text;
                        }
                    }

                    if($count_sent_message == count($message_type))
                    {
                        $message_text = 'Already Sent';
                        //Already Sent All Message for week
                    }
                    else
                    {
                        foreach ($message_type->all() as $check_message)
                        {
                            if(in_array($check_message->predefined_message_id, $predefined_message_id))
                            {
                                continue;
                            }
                            else
                            {
                                $message_text = $check_message->message_text;
                                break;
                            }
                        }
                    }

                }
                elseif(count($message_type) > 0)
                {
                    $message_type = $message_type->first();
                    $message_text = $message_type->message_text;
                }

                if($message_text != 'Already Sent')
                    $milestone_risk = '<div class="col-sm-12"><p class="recm-msg"><i>'.str_limit($message_text,50).'</i></p></div>';
                else
                    $milestone_risk = '<div class="col-sm-12"><p class="recm-msg"><i>'.str_limit($message_text,50).'</i></p></div>';
            }

            $timezone = '';
            if(isset($_COOKIE['user_timezone'])) {
                $timezone =  $_COOKIE['user_timezone'];
            }
            
            $last_message_sent_to_patient   = '';
            $last_unread_message_of_patient = '';
            $here = Carbon::now($timezone);
            $last_weight_date = '';
            $past_week_count = '';
            if(!empty($member->last_message_sent_to_patient))
            {
                $last_message_sent_to_patient    = Carbon::createFromTimestamp($member->last_message_sent_to_patient/1000)->format('Y-m-d H:i:s');
                $last_message_sent_to_patient    =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $last_message_sent_to_patient)->timezone($timezone) : $last_message_sent_to_patient;
                $here->addSeconds($last_message_sent_to_patient->offset - $here->offset);
                $last_message_sent_to_patient     = '<small>'.$last_message_sent_to_patient->diffForHumans($here).'</small>';
                $last_message_sent_to_patient     =  str_replace("before","ago",$last_message_sent_to_patient);
            }
            if(!empty($member->last_unread_message_of_patient))
            {
                $last_unread_message_of_patient    = Carbon::createFromTimestamp($member->last_unread_message_of_patient/1000)->format('Y-m-d H:i:s');
                $last_unread_message_of_patient    =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $last_unread_message_of_patient)->timezone($timezone) : $last_unread_message_of_patient;
                $here->addSeconds($last_unread_message_of_patient->offset - $here->offset);
                $last_unread_message_of_patient = '(' . $last_unread_message_of_patient->diffForHumans($here). ')';
                $last_unread_message_of_patient =  str_replace("before","ago",$last_unread_message_of_patient);
            }

            if(!empty($member->last_weight_date))
            {

                $last_weight_date    = Carbon::createFromTimestamp($member->last_weight_date/1000)->format('Y-m-d H:i:s');
                $last_weight_date    =  ( ! empty( $timezone ) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $last_weight_date)->timezone($timezone) : $last_weight_date;
                $last_weight_date    =  "Last Weight: ".$last_weight_date->format('m/d');
            }

            if(($member->prior_week_engagement_score == 0 AND $member->current_week_engagement_score == 0))
                $past_week_count =  'Past Week: '.$member->prior_week_engagement_score.' <i class="glyphicon glyphicon-circle-arrow-down font-red-soft"></i> '.$member->current_week_engagement_score;
            elseif($member->prior_week_engagement_score <= $member->current_week_engagement_score)
                $past_week_count =  'Past Week: '.$member->prior_week_engagement_score.' <i class="glyphicon glyphicon-circle-arrow-up font-green-jungle"></i> '.$member->current_week_engagement_score;
            else
                $past_week_count =  'Past Week: '.$member->prior_week_engagement_score.' <i class="glyphicon glyphicon-circle-arrow-down font-red-soft"></i> '.$member->current_week_engagement_score;


            //Check user pinned or not
            $pin_unpin = '<a data-toggle="tooltip" title="Pin" data-value="1" data-id="'.$member->user_id.'" class="btn font-grey-salsa btn-sm btn-pin sbold pin_unpin member-list" data-original-title="Pin"><i class="iconhs-pin"></i></a>';
            if($member->pin_unpin == 1)
            {
                $pin_unpin = '<a data-toggle="tooltip" title="Unpin" data-value="0" data-id="'.$member->user_id.'" class="btn font-dark btn-sm btn-pin sbold pin_unpin member-list" data-original-title="Unpin"><i class="iconhs-pin"></i></a>';
            }

            //Check user hide or not
            $hide_unhide = '<a data-toggle="tooltip" title="Hide" data-value="1" data-id="'.$member->user_id.'" class="btn font-dark btn-sm btn-hide sbold hide_unhide member-list" data-original-title="Hide" aria-describedby="tooltip124891"><i class="fa fa-eye"></i></a>';
            if($member->hide_unhide == 1)
            {
                $color_class    = 'active';
                $status         = '';
                $recommended_message = '';
                $hide_unhide = '<a data-toggle="tooltip" title="Unhide" data-value="0" data-id="'.$member->user_id.'" class="btn font-dark btn-sm btn-hide sbold hide_unhide member-list" data-original-title="Unhide" aria-describedby="tooltip124891"><i class="fa fa-eye-slash"></i></a>';
            }

            $asign_group    = '';
            $remove_member  = '';
            if(session('userRole') == 'facilityadmin')
            {
                $asign_group = '<a  href="#modal_assignGroup" data-toggle="modal" data-uid="'.$member->user_id.'" title="Group" class="btn font-dark btn-sm btn-hide sbold member-list"><i class="fa fa-users"></i>  </a>';
                if($member->group_user_id != '')
                    $asign_group = '<a  href="#modal_assignGroup" data-toggle="modal" data-uid="'.$member->user_id.'" title="Group" class="btn font-grey-salsa btn-sm btn-hide sbold member-list"><i class="fa fa-users"></i>  </a>';

                $remove_member = '<a  title="Delete" data-toggle="modal" data-id="'.$member->user_id.'" data-target="#modal_prospective_member" class="btn font-dark btn-sm btn-hide sbold member-list"><i class="fa fa-remove"></i></a>';
            }

            $member_detail = '<a  title="Member Details" data-toggle="modal" data-id="'.$member->user_id.'" data-target="#member_detail" class="btn font-dark btn-sm btn-hide sbold member-list"><i class="fa fa-info-circle"></i></a>';

            $patient_profile_image = env('APP_BASE_URL').'/img/default-user.png';
            if($member->patient_profile_image != '')
            {
                $patient_profile_image = env('PROFILE_IMAGE_BASE_URL') . $member->patient_profile_image;
            }

            $row = [
                'hidden_id'                 =>  '',
                'patient_id'                => '<div class="col-sm-6 '.$color_class.'"><span class="memberid">'.$member->patient_id.' </span></br> Week '. abs($member->member_completed_week).' </div><div class="col-sm-6"> <img class="user-pic rounded"  src="'.$patient_profile_image.'"></br><a href="'.url('facilityadmin/dashboard') .'/'. $member->patient_id.'" class="member-name sbold">'.str_limit($member->first_name).' '.str_limit($member->last_name).'</a></div>',

                'status(engagement score)'  => $milestone_risk,
                'recommended message'       => '<span  class="sbold">'.$past_week_count.'</span><br><br><span  class="sbold">'.$last_weight_date.'</span>',

                'last sent'                 => $last_message_sent_to_patient,
                'unread'                    => '<div class="btn-group"><a data-id="'.$member->user_id.'" data-toggle="modal" href="#direct-msg-coach" class="btn grey-gallery btn-sm btn-outline sbold ">Message</a></div>',
                'signed_on'                 => ( ! empty( $timezone ) && !empty($member->registration_date) ) ? Carbon::createFromFormat('Y-m-d H:i:s', $member->registration_date)->timezone($timezone)->format('m/d/Y h:i A') : '',
                'action'                    => $asign_group.' '.$pin_unpin.' '.$hide_unhide .' '. $remove_member .' '. $member_detail,

            ];

            $final_data->push(array_values($row));
        }

        info('Sending Data To DataTableService');

        $final_response['data']              = $final_data;
        $final_response['recordsTotal']      = $response_total;
        $final_response['recordsFiltered']   = $response_total;
        return response()->json( $final_response );
    }



    public function patient_hide_unhide(Request $request)
    {
        if (request()->ajax()) {
            $user_id    = request('user_id', 0);
            $value      = request('value', 0);

            $patient = Patient::where('user_id', $user_id)->first();

            if(empty($patient))
            {
                info('Member not found into Database USERID:' . $user_id);
                return response()->json([
                    'error' => 'Member not found into Database'
                ]);
            }

            info('Updating status for USERID:' . $user_id);
            $user = PatientHideUnhide::firstOrNew(array('patient_id' => $patient->patient_id, 'user_id' => $this->active_user_id));
            $user->hide_unhide  = $value;
            $user->date         = date('Y-m-d H:i:s');
            $user->save();

            info('Member Status Updated Successfully: PatientId' . $patient->patient_id);

            return $response = [
                'success' => 'Member Status Updated Successfully!',
            ];
        }
        else
        {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'update status'])
            ]);
        }
    }


    public function patient_pin_unpin(Request $request)
    {
        if (request()->ajax()) {
            $user_id    = request('user_id', 0);
            $value      = request('value', 0);

            $patient = Patient::where('user_id', $user_id)->first();

            if(empty($patient))
            {
                info('Member not found into Database USERID:' . $user_id);
                return response()->json([
                    'error' => 'Member not found into Database'
                ]);
            }

            info('Updating status for USERID:' . $user_id);
            $user = PatientHideUnhide::firstOrNew(array('patient_id' => $patient->patient_id, 'user_id' => $this->active_user_id));
            $user->pin_unpin    = $value;
            $user->date         = date('Y-m-d H:i:s');
            $user->save();

            info('Member Status Updated Successfully: PatientId' . $patient->patient_id);

            return $response = [
                'success' => 'Member Status Updated Successfully!',
            ];
        }
        else
        {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'update status'])
            ]);
        }
    }



    public function memberDetail()
    {
        if (request()->ajax()) {
            $user_id  = request('id' , 0);
            if($user_id)
            {
                $response = $this->member_model->get_member_detail_group_detail(session('active_facility'), $user_id);
                $data = array();
                if(!empty($response))
                {

                    $group_name = $response->group_name != '' ? $response->group_name : 'N/A';
                    $offset_key = $response->offset_key != '' ? $response->offset_key : 'N/A';
                    $data = '<address>
                                <strong>Full Name</strong>
                                <br>
                                ' .$response->full_name . '
                            </address>
                            <address>
                                <strong>Email</strong>
                                <br>
                                <a href="mailto:#"> ' . $response->email . ' </a>
                            </address>
                            <address>
                                <strong>Group Name</strong>
                                <br>
                                ' . $group_name . '
                            </address>
                            <address>
                                <strong>Timezone</strong>
                                <br>
                                ' .$offset_key . '
                            </address>';
                }

                return $response = [
                    'success' => $data,
                ];
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'get detail'])
                ]);
            }
        }
    }



    public function memberGroupDetail()
    {
        if (request()->ajax()) {
            $user_id  = request('uid' , 0);
            if($user_id)
            {
                $group_list = $this->group_model->get_all_group_list($this->active_facility_id);
                $data = $this->member_model->get_member_detail_group_detail($this->active_facility_id, $user_id);
                return $response = [
                    'success' => $data,
                    'group_list' => $group_list,
                ];
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'get detail'])
                ]);
            }
        }
    }



    public function addMemberToGroup()
    {

        if (request()->ajax()) {

            $group_id  = request('group_id' , 0);
            $user_id   = request('uid' , 0);

            info('Checking into Group Member table for alredy exist Member into Group');
            $group_response = $this->group_member->where('group_id', $group_id)->where('user_id', $user_id)->get();
            if(count($group_response) > 0)
            {
                info('Selected Member already exist into that Group UserId: ' . $user_id);
                return response()->json([
                    'error' => 'Selected Member already exist into that Group'
                ]);
            }

            if($group_id != '' && $user_id != '')
            {
                $user_old_group_detail = $this->member_model->get_member_detail_group_detail($this->active_facility_id, $user_id);

                if(!empty($user_old_group_detail->group_id) && !empty($user_old_group_detail->group_user_id))
                {
                    if($user_old_group_detail->is_person == 1)
                    {
                        info('Deleting a in_person session_attendance,group_member Group Member: ' . $user_old_group_detail->patient_id);
                        $response = DB::table("$this->group_db.group_member")->where('user_id', $user_id)->get();
                        $group_member_id=$response[0]->group_member_id;
                        if($group_member_id != ''){
                            DB::table("$this->group_db.session_attendance")->where('group_member_id', $group_member_id)->delete();
                        }
                        DB::table("$this->group_db.post_comment")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.post")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.group_member")->where('user_id', $user_id)->where('group_id', $user_old_group_detail->group_id)->delete();
                        DB::table("group_user")->where('user_id', $user_id)->where('group_id', $user_old_group_detail->group_id)->delete();

                    }
                    else
                    {
                        info('Deleting a Online Group post_comment,post,group_member Member: ' . $user_old_group_detail->patient_id);
                        DB::table("$this->group_db.post_comment")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.post")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.group_member")->where('user_id', $user_id)->where('group_id', $user_old_group_detail->group_id)->delete();
                        DB::table("group_user")->where('user_id', $user_id)->where('group_id', $user_old_group_detail->group_id)->delete();
                    }
                }


                $loggedInUser = session('user');
                info('Assign New Member To GroupID: ' . $group_id . ' Request by: ' . $loggedInUser->userName . ' Facility: ' . session('active_facility'));

                $group_member_id = new GroupMember;
                $group_member_id->group_id      = $group_id;
                $group_member_id->user_id       = $user_id;
                $group_member_id->timestamp     = date('Y-m-d H:i:s');
                $group_member_id->save();

                $group_user_id = new GroupUser;
                $group_user_id->group_id      = $group_id;
                $group_user_id->user_id       = $user_id;
                $group_user_id->save();

                $new_group_detail = $this->group_model->get_group_detail($group_id);
                if($group_member_id)
                {
                    if($user_old_group_detail->patient_id)
                    {
                        info('Updating Patient Lead and Food Coach Ids of the Patient: ' . $user_old_group_detail->patient_id);

                        $lead_user_id = '';
                        if(!empty($new_group_detail->lead_user_id))
                        {
                            $lead_user_id  = $this->provider->where('user_id', $new_group_detail->lead_user_id)->first();
                            $lead_user_id  = $lead_user_id->provider_id;
                        }

                        $primary_food_coach_id = '';
                        if(!empty($new_group_detail->primary_food_coach_id))
                        {
                            $primary_food_coach_id  = $this->provider->where('user_id', $new_group_detail->primary_food_coach_id)->first();
                            $primary_food_coach_id  = $primary_food_coach_id->provider_id;
                        }

                        $patient_detail =  Patient::find($user_old_group_detail->patient_id);
                        $patient_detail->lead_coach_id          = $lead_user_id;
                        $patient_detail->primary_food_coach_id  = $primary_food_coach_id;
                        $patient_detail->update();


                        info('Checking into sharing_preference Table to add Meals and Activity preference');
                        $sharing_preference = DB::table("sharing_preference")->where('patient_id', $user_old_group_detail->patient_id)->get();
                        if(!$sharing_preference)
                        {
                            info('Member not exist into sharing_preference MemberId: ' . $user_old_group_detail->patient_id);
                            DB::table('sharing_preference')->insert([
                                ['patient_id' => $user_old_group_detail->patient_id, 'meals' => 0, 'activity' => 0],
                            ]);
                            info('Member added to sharing_preference MemberId: ' . $user_old_group_detail->patient_id);
                        }
                    }
                }

                // To update capacity of the group
                info('Total Group Member into New Group: '. $new_group_detail->total_member);
                if($new_group_detail->capacity <= $new_group_detail->total_member)
                {
                    $group = Groups::find($new_group_detail->group_id);
                    $group->is_full  = 1;
                    $group->update();
                    info("Group Capacity was: $new_group_detail->capacity So it's Full Now GorupId: $new_group_detail->group_id" );
                }


                if($new_group_detail->is_person != 1)
                {
                    $cobrandId = get_cobrand_id();
                    if($cobrandId == 1) {

                        info('Group is Online so sending in-app message to Member');
                        $member_coach_detail = $this->group_model->get_coach_detail($new_group_detail->lead_user_id);
                        $auto_mesage_detail  = $this->group_model->get_auto_messages_day_one();
                        if (!empty($auto_mesage_detail)) {
                            info('Sending auto message to Member');
                            foreach ($auto_mesage_detail as $value) {
                                $message = str_replace('##coachFname##', $member_coach_detail->first_name, $value->template);
                                $message = str_replace('##coachCity##',  $member_coach_detail->city, $message);
                                $message = str_replace('##coachState##', $member_coach_detail->state, $message);
                                $message = str_replace('##acuityLink##', $member_coach_detail->acuity_link, $message);

                                if ($user_old_group_detail->device_type == 'IOS')
                                    $message = str_replace('##appGuideVideoURL##', 'https://healthslate-1.wistia.com/medias/l9whlf8srw', $message);
                                else
                                    $message = str_replace('##appGuideVideoURL##', 'https://healthslate-1.wistia.com/medias/3ohz3tx2ge', $message);

                                $message_id = new Message;
                                $message_id->message = $message;
                                $message_id->owner = 'PROVIDER';
                                $message_id->read_status = DB::raw(0);
                                $message_id->timestamp = (time() * 1000);
                                $message_id->patient_id = $user_old_group_detail->patient_id;
                                $message_id->user_id    = $member_coach_detail->user_id;
                                $message_id->save();
                            }
                        }
                        info('Calling Curriculum Server for Section Title');
                        $curriculumn = $this->curriculumServiceProvider->getCurriculumV2SectionList($user_old_group_detail);
                        if (!empty($curriculumn)) {
                            foreach ($curriculumn as $value) {
                                if ($value->type == strtolower($user_old_group_detail->curriculum_program_type_name)) {
                                    $message_id = new Message;
                                    $message_id->message = "Session $value->display_week \"$value->sectionTitle\" is now available. You can find it under Menu/Videos (Sessions).";
                                    $message_id->owner = 'PROVIDER';
                                    $message_id->read_status = DB::raw(0);
                                    $message_id->timestamp = (time() * 1000);
                                    $message_id->patient_id =    $user_old_group_detail->patient_id;
                                    $message_id->user_id    =    $member_coach_detail->user_id;
                                    $message_id->save();
                                }
                                break;
                            }
                        }
                    }

                    info('Sending Push notification to User: ' . $user_old_group_detail->patient_id .' Device Type: ' . $new_group_detail->device_type);
                    if($new_group_detail->registration_id != ''  && $new_group_detail->device_type == 'Android')
                    {
                        $push = new PushNotification('gcm');
                        $push->setMessage([
                            'data' => [
                                'data' => [
                                    'actions' => 'addedMemberToGroup',
                                    'payload' => ''
                                ],
                            ],
                        ])
                        ->setDevicesToken($new_group_detail->registration_id)
                        ->send();
                        Log::info('Notification Response: ' . print_r($push->getFeedback(), true) );
                    }
                    else if($new_group_detail->registration_id != ''  && $new_group_detail->device_type == 'IOS')
                    {
                        $push = new PushNotification('apn');
                        $push->setMessage([
                            'aps' => [
                                'alert' => 'You are assigned to group',
                                'sound' => 'default',
                            ],
                            'event' => 'addedMemberToGroup',
                        ])->setDevicesToken($new_group_detail->registration_id)->send();
                        Log::info('Notification Response: ' . print_r($push->getFeedback(), true) );
                    }
                }
                else
                {
                    info('New group is In Person');
                    $group_session = $this->group_model->get_group_session_detail($new_group_detail->group_id);
                    if(count($group_session) > 0)
                    {
                        foreach ($group_session as $session_value)
                        {
                            $session_id = new SessionAttendance;
                            $session_id->group_member_id    = $group_member_id->group_member_id;
                            $session_id->group_session_id   = $session_value->group_session_id;
                            $session_id->timestamp          = date('Y-m-d H:i:s');
                            $session_id->save();
                        }
                    }
                }

                return $response = [
                    'success' => "Member assign to new group successfully!",
                ];
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'Assign Member'])
                ]);
            }
        }
    }



    public function removeMemberFromToGroup()
    {
        if (request()->ajax()) {

            $group_id  = request('group_id' , 0);
            $user_id   = request('uid' , 0);
            info('Checking into Group Member table for alredy exist Member into Group');
            $group_response = $this->group_member->where('group_id', $group_id)->where('user_id', $user_id)->first();
            if(count($group_response) > 0)
            {
                info('Removing Member from Group: ' . $group_id);
                $new_group_detail = $this->group_model->get_group_detail($group_id);
                if(!empty($new_group_detail->group_id))
                {
                    if($new_group_detail->is_person == 1)
                    {
                        info('Deleting a in_person session_attendance,group_member Group Member: ' . $new_group_detail->patient_id);
                        DB::table("$this->group_db.session_attendance")->where('group_member_id', $group_response->group_member_id)->delete();
                        DB::table("$this->group_db.post_comment")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.post")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.group_member")->where('user_id', $user_id)->where('group_id', $new_group_detail->group_id)->delete();
                        DB::table("group_user")->where('user_id', $user_id)->where('group_id', $new_group_detail->group_id)->delete();
                    }
                    else
                    {
                        info('Deleting a Online Group post_comment,post,group_member Member: ' . $new_group_detail->patient_id);
                        DB::table("$this->group_db.group_member")->where('user_id', $user_id)->where('group_id', $new_group_detail->group_id)->delete();
                        DB::table("$this->group_db.post_comment")->where('user_id', $user_id)->delete();
                        DB::table("$this->group_db.post")->where('user_id', $user_id)->delete();
                        DB::table("group_user")->where('user_id', $user_id)->where('group_id', $new_group_detail->group_id)->delete();
                    }

                    // To update capacity of the group
                    info('Total Group Member into Group: '. $new_group_detail->total_member);
                    if($new_group_detail->capacity <= $new_group_detail->total_member)
                    {
                        $group = Groups::find($new_group_detail->group_id);
                        $group->is_full  = 1;
                        $group->update();
                        info("Group Capacity was: $new_group_detail->capacity So it's Full Now GorupId: $new_group_detail->group_id" );
                    }

                    return $response = [
                        'success' => 'Member removed from Group successfully!',
                    ];
                }
            }
            else
            {
                info('Selected Member not exist into that Group UserId: ' . $user_id);
                return response()->json([
                    'error' => 'Selected Member not exist into that Group'
                ]);
            }
        }
    }


}//End of Class

<?php

namespace HealthSlatePortal\Http\Controllers\facilityadmin;

use Illuminate\Http\Request;
use Validator;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use Illuminate\Support\Facades\Log;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\UserPasswordHistory;
use HealthSlatePortal\Models\Eloquent\FacilityProvider;
use Session;
use Hash;
use DB;
class FacilityController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $facilityProvider;
	protected $data = array();


    function __construct( FacilityProvider $facilityProvider, FacilityModel $facility_model, Facility $facility, User $user) {
        $this->facility_model  = $facility_model;
        $this->facility        = $facility;
        $this->facilityProvider = $facilityProvider;
        $this->user = $user;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
     */
	public function index() {
        $admin_ids=array();
        $cobrand_facility_admins    = $this->facility_model->get_cobrand_all_facility_admin_list();
        $facility_admin             = $this->facility_model->get_facility_admin_list(session('active_facility'));

        if(!empty($facility_admin))
        {
            foreach ($facility_admin as $admin) {
                $admin_ids[] = $admin->provider_id;
            }
        }

        $this->data['cobrand_facility_admins'] = collect($cobrand_facility_admins)->filter( function ( $provider ) use($admin_ids) {
            return !in_array( $provider->provider_id, $admin_ids );
        } );
        info( 'Total Co-Brand Coach: ' . count($this->data['cobrand_facility_admins']));

        $this->data['facility_admin'] = collect($facility_admin)->filter( function ( $provider ) {
            return !in_array( $provider->email, [session('user')->userName] );
        } );
        info( 'Total Facility Admin: ' . count($this->data['facility_admin']));

        $this->data['facility']                 = $this->facility_model->get_facility_detail(session('active_facility'));
        $this->data['state']                    = state();
        $this->data['timezone']                 = timezone();

	    return view( 'facilityadmin.facility.facility', $this->data );
	}



    /**
     * facility store function
     * (it's update data in facility table)
     */

    public function facilityStore(Request $request)
    {

        if ($request->isMethod('post')) {

            $validator = Validator::make($request->all(), [
                'name'                  => 'required',
                'state'                 => 'required',
                'city'                  => 'required',
                'zip'                   => 'required',
                'address'               => 'required',
                'timezone'              => 'required',
                'contact_info'          => 'required',
                'contact_person_name'   => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('facilityadmin/facility')
                    ->withErrors($validator)
                    ->withInput();
            }


            $data = Facility::find(session('active_facility'));
            $data->name                         = $request->name;
            $data->state                        = $request->state;
            $data->city                         = $request->city;
            $data->zip                          = $request->zip;
            $data->address                      = $request->address;
            $data->timezone_name                = timezone()[$request->timezone];
            $data->timezone_offset              = $request->timezone;
            $data->timezone_offset_millis       = $request->timezone * 60 * 60 * 1000;
            $data->contact_info                 = $request->contact_info;
            $data->contact_person_name          = $request->contact_person_name;
            $data->partner_id                   = $request->partner_id;
            $data->cdc_organization_code        = $request->cdc_organization_code;
            $data->is_notification_enabled      = isset($request->is_notification_enabled) ? $request->is_notification_enabled: DB::raw(0);
            $data->is_skip_consent              = isset($request->is_skip_consent) ? $request->is_skip_consent: DB::raw(0);
            $data->is_chatbot_enabled           = isset($request->is_chatbot_enabled) ? $request->is_chatbot_enabled: 0;
            $data->update();
            info( 'facility update succesfully' );

            $facility = [];
            foreach(session('facility') as $key => &$meal)
            {
                if(session('active_facility') == $meal->facility_id)
                {
                    $facility[] = (object) [
                        'provider_id'   =>  $meal->provider_id,
                        'name'          =>  $request->name,
                        'facility_id'   =>  $meal->facility_id
                    ];
                }
                else
                {
                    $facility[] = (object) [
                        'provider_id'   =>  $meal->provider_id,
                        'name'          =>  $meal->name,
                        'facility_id'   =>  $meal->facility_id
                    ];
                }
            }
            Session::put( 'facility', $facility);
            return redirect()->route('facilityadmin.facility')->with('success', 'Facility Updated Successfully!');
        }
    }




    /**
     * Process assign Facility to Facility Admin
     *
     * @return \Illuminate\Http\JsonResponse
     */
    /*public function assignFacility() {

        $facility_admin = request('select_facility_admin' , 0);

        if($facility_admin == 0)
        {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'assign facility admin'])
            ]);
        }

        $response = $this->facilityProvider
            ->whereIn('providers_provider_id', $facility_admin)
            ->where('facility_facility_id', session('active_facility'))
            ->get();

        $loggedInUser = session('user');

        if (count($response) != 0 ) {
            info('Facility admin already exist.');
            return response()->json([
                'error' => 'Facility admin already exist.',
            ]);
        }

        $save = false;
        foreach ($facility_admin as $key => $value)
        {
           info('Assign New Facility Admin For Facility: ' . session('active_facility') . ' Request by: ' . $loggedInUser->userName . ' Facility: ' . $value);
           $save = new FacilityProvider;
           $save->providers_provider_id    = $value;
           $save->facility_facility_id     = session('active_facility');
           $save->save();
        }

        if (!$save) {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'assign coach'])
            ]);
        }

        return response()->json(
            [
                'success' => 'Facility admin assign to this facility successfully!'
            ]
        );
    }*/





}

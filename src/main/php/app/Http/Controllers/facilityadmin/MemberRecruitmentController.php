<?php

namespace HealthSlatePortal\Http\Controllers\facilityadmin;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Validator;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\MemberRecruitmentModel;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\Patient;
use HealthSlatePortal\Models\Eloquent\Target;
use HealthSlatePortal\Models\Eloquent\TargetMeal;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\Reminder;
use HealthSlatePortal\Models\Eloquent\UserMessagePreference;
use Illuminate\Support\Facades\Log;
use HealthSlatePortal\Helpers\MailServiceHelper;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use Session;
use DB;
class MemberRecruitmentController extends Controller {

    protected $member_recruitment_model;
    protected $member_recruitment;
    protected $coach_model;
    protected $mailServiceHelper;
    protected $dataTableService;
    protected $data = array();


    function __construct( MemberRecruitmentModel $member_recruitment_model, CoacheModel $coach_model,MailServiceHelper $mailServiceHelper,DataTableServiceProvider $dataTableService) {
        $this->member_recruitment_model  = $member_recruitment_model;
        $this->coach_model          = $coach_model;
        $this->mailServiceHelper    = $mailServiceHelper;
        $this->dataTableService     = $dataTableService;
        $this->active_user_role     = session('userRole');
        $this->cobrand_id       = Session::get('user')->cobrandId;
        $this->data['js']           = array('message', 'member','member-recruitment');

    }

    /**
     * index function
     *
     * @return \Illuminate\Http\Response
     * (it's show add member page)
     */
    public function index() {

        $all_coaches = $this->coach_model->get_facility_coach_list(session('active_facility'));
        $resultArray = $this->_coachListBuilder( $all_coaches );
        $this->data['lead_coach'] = array();
        $this->data['food_coach'] = array();
        $this->data['lead_coach'] = $resultArray['lead_coach'];
        $this->data['food_coach'] = $resultArray['food_coach'];
        $this->data['state']         = state();
        $this->data['diabetes_type'] = add_member_diabetes_type();
        if($this->active_user_role =='admin'){
            $this->data['facility_list']= $this->coach_model->get_all_facility_list_for_admin($this->cobrand_id);
            return view( 'facilityadmin.memberrecruitment.admin_add_member',$this->data);
        }else{
            return view( 'facilityadmin.memberrecruitment.add_member',$this->data);
        }

    }

    public function get_all_coaches() {
        if (request()->ajax()) {
            $facility_id = request('facility_id');
            $all_coaches = $this->coach_model->get_facility_coach_list($facility_id);
            $resultArray = $this->_coachListBuilder($all_coaches);
            $this->data['lead_coach'] = array();
            $this->data['food_coach'] = array();
            $lead_coach = $resultArray['lead_coach'];
            $food_coach = $resultArray['food_coach'];
            return response()->json(
                [
                    'foodcoach'=>$food_coach,
                    'leadcoach'=>$lead_coach,
                    'success' => true
                ]
            );

        }


    }


      private function _coachListBuilder( $all_coaches )
     {
        $foodCoach = [];
        $leadCoach = [];
        foreach ($all_coaches as $coach) {
            $providers     = [ ];
            $providersLead = [ ];
            if ($coach->user_type == "Coach") {
                $providersLead[$coach->provider_id] = @$coach->first_name . " " . @$coach->last_name;
            } else if ($coach->user_type == "Food Coach") {
                $providers[$coach->provider_id] = @$coach->first_name . " " . @$coach->last_name;
            }
            if ( ! empty( $providers )) {
                $foodCoach[] = $providers;
            }
            if ( ! empty( $providersLead )) {
                $leadCoach[] = $providersLead;
            }
        }
        return [ 'food_coach' => $foodCoach, 'lead_coach' => $leadCoach ];
      }



    /**
     Member store function
     *(it's store add member page data in database )
     */

    public function addMemberStore(Request $request)
    {

        if ($request->isMethod('post')) {
            //it's check validation
            $validator = Validator::make($request->all(), [
                'first_name'            => 'required|min:2',
                'last_name'             => 'required|min:2',
                'email'                 => 'required|email|unique:users',
                'diabetes_type'         => 'required',
                'zip'                   =>'numeric',
                'phone_number'          =>'numeric',


            ]);

            if ($validator->fails()) {
                return redirect($this->active_user_role . '/addmember')
                    ->withErrors($validator)
                    ->withInput();
            }


           //it's store data in user table
            $data = new User;
            $data->first_name                    = $request->first_name;
            $data->last_name                     = $request->last_name;
            $data->email                         = $request->email;
            $data->state                         = $request->state;
            $data->city                          = $request->city;
            $data->zip_code                      = $request->zip;
            $data->address                       = $request->address;
            $data->phone                         = $request->phone_number;
            $data->gender                        = $request->gender;

            $data->user_type                     = 'PATIENT' ;
            $data->cobrand_id                    = Session::get('user')->cobrandId  ;
            $data->save();

            //it's store data in patient table
            $user_id = $data->user_id;
            $patient = new Patient;
            $patient->user_id                     = $user_id;
            $patient->dob                         = $request->date_of_birth != '' ? date("Y-m-d", strtotime($request->date_of_birth)) : null;
            $patient->gender                      = $request->gender;
            $patient->diabetes_type_id            = $request->diabetes_type;
            $patient->patient_status              = $request->patient_status;
            $patient->lead_coach_id               = $request->life_style_coach;
            $patient->primary_food_coach_id       = $request->food_coach;
            $patient->mrn                         = $request->mrn;
            $patient->state                       = $request->state;
            $patient->uuid                        = UUID();
            $patient->invitation_sent_date        = (time() * 1000);
            $patient->is_approved                 =  3;
            $patient->is_invited                  =  1;
            $patient->is_consent_accepted         =  1;
            $patient->is_one_day_email_sent       =  0;
            $patient->is_two_day_email_sent       =  0;
            $patient->is_three_day_email_sent     =  0;
            $patient->approval_date               =  date('Y-m-d h:i:s');
            $patient->email_communication         = 'Yes';
            $patient->patient_status              = 'Invited';
            if($this->active_user_role =='admin'){
                $patient->facility_id                 = $request->facility_list;;
            }else{
                $patient->facility_id                 = session('active_facility');
            }

            $patient->save();

            $patient_id = $patient->patient_id;

            $target = new Target;
            $target->starting_weight= 0.0;
            $target->target_weight= 0;
            $target->weight_frequency ="Once/Day";
            $target->glucose_min= 80;
            $target->glucose_max= 180;
            $target->patient_id=  $patient_id;
            $target->target_source= "PATIENT";
            $target->glucose_min_2= 80;
            $target->glucose_max_2= 130;
            $target->activity_steps=  3000;
            //$target->activity_minutes= "NULL";
            $target->weight_time = "06:45 AM";
            $target->save();
            $target_id = $target->target_id;


            $reminder = new Reminder;
            $reminder->is_meal_time = 1;
            $reminder->is_glucose_testing = 0;
            $reminder->is_goal_action_plan = 0;
            $reminder->is_new_topic_available = 0;
            $reminder->target_id= $target_id;

            if($request->diabetes_type==1)
            $reminder->is_weighing_myself=  DB::raw('0');
            else
            $reminder->is_weighing_myself=  DB::raw('1');
            $reminder->is_snack_time = 1;
            $reminder->is_breakfast_time = 1;
            $reminder->is_lunch_time = 1;
            $reminder->is_dinner_time = 1;
            $reminder->save();
            $reminder_id = $reminder->reminder_id;


            $target_meal_array = array(
                array(
                    "meal_type" => "Dinner",
                    "meal_time" => "07:30 PM",
                    "carb_target" => "0",
                    "target_id" => $target_id,
                    "daily_fat_budget" => NULL,
                    "daily_calorie_budget" => NULL,
                    "daily_water_budget"=> NULL,
                ),

                array(
                    "meal_type" => "Snack",
                    "meal_time" => "03:30 PM",
                    "carb_target" => "0",
                    "target_id" => $target_id,
                    "daily_fat_budget" => NULL,
                    "daily_calorie_budget" => NULL,
                    "daily_water_budget"=>NULL,
                ),
                array(
                    "meal_type" => "Lunch",
                    "meal_time" => "01:30 PM",
                    "carb_target" => "0",
                    "target_id" => $target_id,
                    "daily_fat_budget" => NULL,
                    "daily_calorie_budget" => NULL,
                    "daily_water_budget"=>NULL,
                ),
                array(
                    "meal_type" => "Daily",
                    "meal_time" => "NULL",
                    "carb_target" => NULL,
                    "target_id" => $target_id,
                    "daily_fat_budget" => NULL,
                    "daily_calorie_budget" => "0",
                    "daily_water_budget"=>"8",
                ),
                array(
                    "meal_type" => "Breakfast",
                    "meal_time" => "09:00 AM",
                    "carb_target" => "0",
                    "target_id" => $target_id,
                    "daily_fat_budget" => NULL,
                    "daily_calorie_budget" => NULL,
                    "daily_water_budget"=> NULL,
                )
            );


             if (count($target_meal_array)>0) {

                 for ($i = 0; $i < count($target_meal_array); $i++) {

                     $target_meal = new TargetMeal;
                     $target_meal->meal_type = $target_meal_array[$i]['meal_type'];
                     $target_meal->meal_time = $target_meal_array[$i]['meal_time'];
                     $target_meal->carb_target = $target_meal_array[$i]['carb_target'];
                     $target_meal->target_id = $target_meal_array[$i]['target_id'];
                     $target_meal->daily_fat_budget = $target_meal_array[$i]['daily_fat_budget'];
                     $target_meal->daily_calorie_budget = $target_meal_array[$i]['daily_calorie_budget'];
                     $target_meal->daily_water_budget = $target_meal_array[$i]['daily_water_budget'];
                     $target_meal->save();
                 }
             }


             $user_message_prefrence = new UserMessagePreference;
             $user_message_prefrence->user_id = $user_id;
             $user_message_prefrence->sms = 0;
             $user_message_prefrence->email = 0;
             $user_message_prefrence->is_in_app_message_notify = 1;
             $user_message_prefrence->save();


            info('add member successfully');

            //send mail function use here
            if($request->patient_status=="" && $request->offline_member=="")
            {   $name= $request->first_name;
                if(Session::get('user')->cobrandId== 2 ){
                    $message = " Thank you for joining the SoleraOne Program. Please click below to download the Mobile App: <br> 
                        For Android:<br> <a href='https://play.google.com/store/apps/details?id=com.atoz.healthslate.patientapp.soleraone'>https://play.google.com/store/apps/details?id=com.atoz.healthslate.patientapp.soleraone</a><br>For iOS: <br> <a href='https://itunes.apple.com/us/app/soleraone/id1218161083?mt=8'>https://itunes.apple.com/us/app/soleraone/id1218161083?mt=8</a>";                    $body = ['name'=>$name, 'content' => $message,'subject' =>'SoleraOne Mobile App Download Link ' ];
                    $body = ['name'=>$name, 'content' => $message,'subject' =>'SoleraOne Mobile App Download Link ' ];
                }else{
                    $message = " Thank you for joining the HealthSlate Program. Please click below to download the Mobile App: <br> 
                             For Android:<br> <a href='https://play.google.com/store/apps/details?id=com.atoz.healthslate.patientapp'>https://play.google.com/store/apps/details?id=com.atoz.healthslate.patientapp</a><br>For iOS: <br> <a href='https://itunes.apple.com/us/app/healthslate-diabetes-management/id990695811?mt=8'>https://itunes.apple.com/us/app/healthslate-diabetes-management/id990695811?mt=8</a>   ";
                    $body = ['name'=>$name, 'content' => $message,'subject' =>'HealthSlate Mobile App Download Link ' ];
                }

                $to = $request->email;
                $this->mailServiceHelper->sendEmail($body, trim($to));
            }


            return redirect($this->active_user_role . '/addmember')->with('success', 'Member  Added Successfully!');
        }
    }


    /**
     Prospective Member function
     *(it's show prospective member page )
     */

    public function prospectiveMember()
    {

        if(Session::get('userRole')=="admin")
        {
            return view( 'admin.memberrecruitment.prospective_member', $this->data);
        }
           return view( 'facilityadmin.memberrecruitment.prospective_member', $this->data);
    }


    /**
    Prospective Member list function
     *(it's load data on prospective member page )
     */

    public function prospectiveMembersList()
    {


        $response = $this->member_recruitment_model->get_prospective_member_list(session('active_facility'));
        info('Total Members Found: ' . count($response));
        $data = collect();
        foreach ($response as $member) {


            $row = [
                'hidden_id'             => '',
                'patient_id'            => $member->patient_id,
                'mrn'                   => ($member->mrn!="" && $member->mrn!="NULL")?'<a title="MRN" data-toggle="modal" data-patient-id="'.$member->patient_id.'" data-mrn-id="'.$member->mrn.'"  data-target="#modal_mrn" id="mrn_'.$member->patient_id.'" > '.$member->mrn.' </a>':'<a title="MRN" data-toggle="modal" data-patient-id="'.$member->patient_id.'" data-mrn-id="'.$member->mrn.'"  data-target="#modal_mrn" id="mrn_'.$member->patient_id.'"  >Add</a>',
                'name'                  => $member->first_name . ' ' . $member->last_name,
                'email'                 => $member->email,
                'phone'                 => $member->phone,
                'requested'             => isset($member->is_invited) ? 'Invited' : 'Requested',
                'date_requested'        => Carbon::createFromFormat( 'Y-m-d H:i:s', $member->invitation_sent_date )->format(  config( 'healthslate.default_date_format' ) ),
                'consent_form_accepted' => isset($member->is_consent_accepted) ? 'Yes' : 'No',
                'action_assigned'       => '',
                'patient_status'        => $member->patient_status,
                'action'                => '<a title="Remove" data-toggle="modal" data-id="'.$member->u_id.'" data-target="#modal_prospective_member" class="btn btn-icon-only red"><i class="fa fa-trash"></i></a> &nbsp; <a title="Resend Invitation" data-id-for-mail ="'.$member->u_id.'" class="btn btn-icon-only green invitation_send"><i class="fa fa-envelope"></i></a>'
            ];

            if(Session::get('userRole')=="admin")
            {
                $row = array_slice($row, 0, 5, true) +
                array('facility' => $member->facility_name) +
                array_slice($row, 5, count($row) - 1, true) ;
            }

            $data->push(array_values($row));

        }
        info('Sending Data To DataTableService');
        return $this->dataTableService->dataTableWithIds($data);
    }

    /**
    Prospective delete function
     *(it's delete  data on prospective member page )
     */

    public function delete(Request $request ) {
         //it's get data from patient table
        $patient = Patient::where('user_id', $request->id)->first();
        $patient_id =  $patient->patient_id;
        //it's update patient table data
        $data =  Patient::find($patient_id);
        $data->patient_state                     = $request->patientstate;
        $data->patient_status                    = 'DELETED';
        $data->is_deleted                        =  '1';
        $data->update();
        //it's update user table data
        $user = User::where('user_id', $request->id)->first();
        $email = "_".$request->id.'_'.$user->email;
        $user->email= $email;
        $user->update();
        GroupMember::where('user_id', $request->id)->delete();
        GroupUser::where('user_id', $request->id)->delete();



        if (!$data) {
            info( 'unable to delete patient table' );
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'unable to remove '])
            ]);
        }
        info( 'Member deleted successfully' );
        return response()->json(
            [
                'success' => 'Member deleted successfully.',
                'patient_id' => $patient_id
            ]
        );

    }

    /**
    Prospective send invitation function
     *(it's send  invitation to  prospective member )
     */
    public function invitationSend(Request $request)
    {
        //it's get user data from user table
        $user = User::where('user_id', $request->id)->first();
         $user_name =  $user->first_name;
         $email =  $user->email;
         $name= $user_name;
        if(Session::get('user')->cobrandId== 2 ){
            $message = " Thank you for joining the Soleraone Program. Please click below to download the Mobile App: <br> 
                        For Android:<br> <a href='https://play.google.com/store/apps/details?id=com.atoz.healthslate.patientapp.soleraone'>https://play.google.com/store/apps/details?id=com.atoz.healthslate.patientapp.soleraone</a><br>For iOS: <br> <a href='https://itunes.apple.com/us/app/soleraone/id1218161083?mt=8'>https://itunes.apple.com/us/app/soleraone/id1218161083?mt=8</a>";
            $body = ['name'=>$name, 'content' => $message,'subject' =>'SoleraOne Mobile App Download Link ' ];

        }else{
            $message = " Thank you for joining the HealthSlate Program. Please click below to download the Mobile App: <br> 
                             For Android:<br> <a href='https://play.google.com/store/apps/details?id=com.atoz.healthslate.patientapp'>https://play.google.com/store/apps/details?id=com.atoz.healthslate.patientapp</a><br>For iOS: <br> <a href='https://itunes.apple.com/us/app/healthslate-diabetes-management/id990695811?mt=8'>https://itunes.apple.com/us/app/healthslate-diabetes-management/id990695811?mt=8</a>   ";
            $body = ['name'=>$name, 'content' => $message,'subject' =>'HealthSlate Mobile App Download Link ' ];
        }

          $to = $email;
          $this->mailServiceHelper->sendEmail($body, trim($to));
          return response()->json(
            [
                'success' => 'Invitation send successfully.'
            ]
        );
    }

    /**
    Prospective mrn save function
     *(it save mrn number )
     */
    public function mrnSave(Request $request)
    {
       // it's save and mrn number
        $data = Patient::find($request->id);
        $data->mrn = $request->mrn;
        $data->update();
        info( 'mrn save successfully' );
        return response()->json(
            [
                'success' => 'MRN save successfully.'
            ]
        );
    }



}// End of file

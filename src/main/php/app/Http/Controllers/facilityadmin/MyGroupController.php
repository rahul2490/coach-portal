<?php

namespace HealthSlatePortal\Http\Controllers\facilityadmin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\MemberModel;
use HealthSlatePortal\Models\GroupModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\Groups;
use HealthSlatePortal\Models\Eloquent\GroupMember;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\GroupUser;
use HealthSlatePortal\Models\CurriculumServiceProvider;
use HealthSlatePortal\Models\Eloquent\Provider;
use HealthSlatePortal\Models\Eloquent\Patient;
use Illuminate\Support\Facades\Log;
use Validator;
use Session;
use DB;
class MyGroupController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $coach_model;
    protected $member_model;
    protected $group_model;
    protected $group_member;
    protected $dataTableService;
    protected $curriculumServiceProvider;
    protected $user;
    protected $data = array();

    function __construct( FacilityModel $facility_model, MemberModel $member_model,
                          Facility $facility, CoacheModel $coach_model, GroupModel $group_model,
                          DataTableServiceProvider $dataTableService, User $user, GroupMember $group_member,CurriculumServiceProvider $curriculumServiceProvider) {
        $this->facility_model   = $facility_model;
        $this->coach_model      = $coach_model;
        $this->member_model     = $member_model;
        $this->group_model      = $group_model;
        $this->group_member     = $group_member;
        $this->facility         = $facility;
        $this->curriculumServiceProvider = $curriculumServiceProvider;
        $this->user             = $user;
        $this->dataTableService = $dataTableService;
        $this->group_db         = env('GROUP_DB_DATABASE');
        $this->data['js']       = array('group','coach');
    }


    /**
     * @param
     * @return Load view for Member
     */
	public function index() {
        $all_coaches = $this->coach_model->get_facility_coach_list(session('active_facility'));
        $resultArray = $this->_coachListBuilder( $all_coaches );
        $this->data['lead_coach'] = array();
        $this->data['food_coach'] = array();
        $this->data['lead_coach'] = $resultArray['lead_coach'];
        $this->data['food_coach'] = $resultArray['food_coach'];
        $this->data['diabetes_type']  = diabetes_type();
        return view( 'facilityadmin.mygroup.group', $this->data);
	}


    private function _coachListBuilder( $all_coaches )
    {
        $foodCoach = [];
        $leadCoach = [];
        foreach ($all_coaches as $coach) {
            $providers     = [ ];
            $providersLead = [ ];
            if ($coach->user_type == "Coach") {
                $providersLead[] = (object)
                                    [
                                        'user_id'   => $coach->user_id,
                                        'full_name' => @$coach->first_name . " " . @$coach->last_name,
                                        'email'     => $coach->email
                                    ];
            } else if ($coach->user_type == "Food Coach") {
                $providers[] = (object)
                                [
                                    'user_id'   => $coach->user_id,
                                    'full_name' => @$coach->first_name . " " . @$coach->last_name,
                                    'email'     => $coach->email
                                ];
            }
            if ( ! empty( $providers )) {
                $foodCoach[] = $providers;
            }
            if ( ! empty( $providersLead )) {
                $leadCoach[] = $providersLead;
            }
        }
        return [ 'food_coach' => $foodCoach, 'lead_coach' => $leadCoach ];
    }


    /**
     * @param $active_facility
     * @return Json response of Group Data
     */
    public function groupList()
    {
        $group    = request('group' , 0);
        $capacity = request('capacity' , 0);
        $response = collect($this->group_model->get_facility_group_list(session('active_facility'), $group, $capacity));

        info('Total Group Found: ' . count($response));
        $data = collect();

        if(count($response) > 0)
        {
            $group_ids = $response->pluck( 'group_id' )->toArray();
            $curriculum_for_dpp     = '';
            $curriculum_for_dpp2    = '';
            if ($group != 'online') {
                // Calling Curriculam server for Section list for both type DPP and DPP2
                $group_session_detail = collect($this->group_model->get_group_current_session_name($group_ids));
                $curriculum_program_type_name = $group_session_detail->unique('curriculum_program_type_name')->pluck( 'curriculum_program_type_name' )->toArray();
                if(count($curriculum_program_type_name) > 0)
                {
                    foreach ($curriculum_program_type_name as $value)
                    {
                        if($value == 'DPP2' || $value == 'DPP')
                        {
                            $curriculum = [
                                (object) [ 'uuid' => $group, 'curriculum_program_type_name' => $value],
                            ];
                            if($value == 'DPP')
                                $curriculum_for_dpp  = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                            if($value == 'DPP2')
                                $curriculum_for_dpp2 = collect($this->curriculumServiceProvider->getCurriculumV2SectionList($curriculum));
                        }
                    }
                }
            }

            foreach ($response as $group) {

                $group_members = $this->group_model->get_group_coaches_detail($group->group_id);
                $coach_list = '';$food_coach = ''; $life_coach = '';
                if(!empty($group_members))
                {
                    foreach ($group_members as $key => $value)
                    {
                        if($value->lead_user_id == $value->user_id)
                            $life_coach  = "<span>$value->full_name <strong>(Lifestyle Coach)</strong></span><br>";
                        else if ($value->primary_food_coach_id == $value->user_id)
                            $food_coach  = "<span>$value->full_name <strong>(Food Coach)</strong></span><br>";
                        else
                            $coach_list .= "<span>$value->full_name <strong>(Coach)</strong><a  title=\"Remove Coach\" data-toggle=\"modal\" data-uid=".$value->user_id." data-gid=".$group->group_id." data-target=\"#remove_group_coach\" class=\"font-dark btn-sm btn-hide sbold \"> <i class=\"fa fa-remove\"></i>  </a></span><br>";
                    }
                    $life_coach .= $food_coach .' '. $coach_list;
                }

                if($group->completed_week == 0)
                    $group->completed_week = 1;

                $icon_group = $group->is_person == 0 ? 'iconhs-onlinegroup' : 'iconhs-inperson';
                $week_group = $group->is_person == 0 ? 'Week ' . $group->completed_week : 'Session ' .$group->completed_week;

                $delete_group = '';
                if($group->total_users == 0)
                    $delete_group = '<a  title="Delete Group" data-toggle="modal" data-id="'.$group->group_id.'" data-target="#remove_group" class="btn font-dark btn-sm btn-hide sbold pull-left"> <i class="fa fa-remove"></i>  </a>';
                $link = '';
                if($group->group_id)
                    $link = 'href="'.route('facilityadmin.members', ['group_id' => base64_encode($group->group_id)]).'"';

                if($group->is_person == 1)
                {
                    $week_group = 'Session has not started yet';

                    $current_session_name = '';
                    if(count($group_session_detail) > 0)
                    {
                        $group_current_session = $group_session_detail->where('group_id', $group->group_id)->last();

                        if(!empty($group_current_session->topic_id))
                        {
                            if($curriculum_for_dpp != '')
                                $current_session_name = $curriculum_for_dpp->where('section_uuid', $group_current_session->topic_id)->toArray();
                            if(count($current_session_name) == 0 && $curriculum_for_dpp2 != '')
                                $current_session_name = $curriculum_for_dpp2->where('section_uuid', $group_current_session->topic_id)->toArray();

                            if(!empty($current_session_name))
                            {
                                foreach ($current_session_name as $session_name)
                                {
                                    $week_group = 'Session ' . $session_name->display_week . ' : '. str_limit($session_name->sectionTitle, 40);
                                }
                            }
                        }

                    }
                }

                $row = [
                    'hidden_id'       => '',
                    'group_name'      => '<div class="col-sm-1"><span class="group-type"><i class="'.$icon_group.'"></i></span></div>
                                            <div class="col-sm-11">
                                                <p>'.$group->name.'<a class="pull-right font-dark" '.$link.' > '.$group->total_users.' Members</a> </p>
                                                <p><strong>'.$week_group.'</strong><span class="pull-right">Started-'.date('M d',strtotime($group->timestamp)).'</span> </p>
                                          </div>',

                    'status'          => $life_coach,

                    'action'          => '                                        
                                        <a  title="Edit Group" data-toggle="modal" data-id="'.$group->group_id.'" data-target="#edit_group_detail" class="btn font-dark btn-sm btn-hide sbold pull-left"><i class="fa fa-edit"></i></a>
                                        <a  href="#assign_coach_to_group" data-id="'.$group->group_id.'" data-toggle="modal" title="Add Coach" class="btn font-dark btn-sm btn-hide sbold pull-left"> <i class="fa fa-plus"></i>  </a>
                                        '.$delete_group.'
                                        <a title="Group Wall" href="'.route('facilityadmin.group-wall', ['group_id' => base64_encode($group->group_id)]).'" class="btn font-dark btn-sm btn-hide sbold pull-left"><i class="iconhs-groupwall"></i></a>
                                        ',

                ];
                $data->push(array_values($row));
            }
        }

        info('Sending Data To DataTableService');
        return $this->dataTableService->dataTableWithIds($data);
    }



   public function comingsoon()
   {
    return view("facilityadmin.mygroup.comingsoon");

   }



   public function createGroup(Request $request)
   {
       if (request()->ajax()) {

           $messages = [
               'name.unique'    => 'Group Name already taken.',
           ];

           $validator = Validator::make(request()->input(), [
               'name'               => 'required|unique:groups.groups,name',
               'diabetesTypeId'     => 'required',
               'capacity'           => 'required|numeric',
               'group_type'         => 'required',
               'preferredLanguage'  => 'required',
               'selectLifeCoach'    => 'required',
           ], $messages);

           if ($validator->fails()) {
               info('Create Group Validation Failed');
               foreach ($validator->errors()->all() as $key => $value)
               {
                   return $response = [
                       'error' => $value,
                   ];
                   break;
               }
           }
           info('Create Group Validation Success');
           $loggedInUser = session('user');
           $user = $this->user->where('email', $loggedInUser->userName)->first();

           $total_member = 0;
           if(!empty($request->selectLifeCoach))
               $total_member = 1;
           if(!empty($request->selectFoodCoach))
               $total_member += 1;
           if($request->group_type == 1)
              $request->selectFoodCoach="";
           $group = new Groups;
           $group->name                     = $request->name;
           $group->lead_user_id             = $request->selectLifeCoach;
           $group->facility_admin_id        = $user->user_id;
           $group->primary_food_coach_id    = isset($request->selectFoodCoach) ? $request->selectFoodCoach : '';
           $group->timestamp                = date('Y-m-d H:i:s');
           $group->is_person                = $request->group_type;
           $group->capacity                 = $request->capacity;
           $group->is_full                  = $request->capacity <= $total_member ? 1 : 0;
           $group->diabetes_type_id         = $request->diabetesTypeId;
           $group->is_auto_post_enabled	    = $request->autoPostEnable == 1 ? 1 : 0 ;
           $group->auto_post_time	        = $request->autoPostTime;
           $group->preferred_language	    = $request->preferredLanguage;
           $group->facility_id              = session('active_facility');
           $group->save();

            if($group && !empty($request->selectLifeCoach))
            {
                $group_member_ld = new GroupMember;
                $group_member_ld->group_id      = $group->group_id;
                $group_member_ld->user_id       = $request->selectLifeCoach;
                $group_member_ld->timestamp     = date('Y-m-d H:i:s');
                $group_member_ld->save();

                // entery in group user table
                $group_user_ld = new GroupUser;
                $group_user_ld->group_id      = $group->group_id;
                $group_user_ld->user_id       = $request->selectLifeCoach;
                $group_user_ld->save();

            }
           /*if($group && !empty($request->selectFoodCoach))
           {
               $group_member_fd = new GroupMember;
               $group_member_fd->group_id      = $group->group_id;
               $group_member_fd->user_id       = $request->selectFoodCoach;
               $group_member_fd->timestamp     = date('Y-m-d H:i:s');
               $group_member_fd->save();
           }*/

           if (!$group || !$group_member_ld) {
               return response()->json([
                   'error' => trans('common.unable_to_do_action', [ 'action' => 'create group'])
               ]);
           }

           return $response = [
               'success' => 'New Group Created Successfully!',
           ];
       }
   }



    public function updateGroup(Request $request)
    {
        if (request()->ajax()) {

            $messages = [
                'name.unique'    => 'Group Name already taken.',
            ];

            $validator = Validator::make(request()->input(), [
                'id'                 => 'required',
                'name'               => 'required|unique:groups.groups,name,'. $request->id.',group_id',
                //'diabetesTypeId'     => 'required',
                'capacity'           => 'required|numeric',
                'preferredLanguage'  => 'required',
            ], $messages);

            if ($validator->fails()) {
                info('Edit Group Validation Failed');
                foreach ($validator->errors()->all() as $key => $value)
                {
                    return $response = [
                        'error' => $value,
                    ];
                    break;
                }
            }
            info('Edit Group Validation Success');

            $group_detail = $this->group_model->get_group_detail($request->id);
            info('Total Group Member: '. $group_detail->total_member);
            $group = Groups::find($request->id);

            if($request->selectLifeCoach != $group->lead_user_id)
            {
                $this->group_member->where('group_id', $group->group_id)->where('user_id', $request->selectLifeCoach)->delete();
                $this->group_member->where('group_id', $group->group_id)->where('user_id', $group->lead_user_id)->delete();
                DB::table("group_user")->where('user_id', $group->lead_user_id)->where('group_id', $group->group_id)->delete();

                $group_member_ld = new GroupMember;
                $group_member_ld->group_id      = $group->group_id;
                $group_member_ld->user_id       = $request->selectLifeCoach;
                $group_member_ld->timestamp     = date('Y-m-d H:i:s');
                $group_member_ld->save();

                // entery in group user table
                $group_user_ld = new GroupUser;
                $group_user_ld->group_id      = $group->group_id;
                $group_user_ld->user_id       = $request->selectLifeCoach;
                $group_user_ld->save();

                //it's for patient leadcoach id update
                $group_list  = $this->group_model->get_group_member_list($group->group_id,session('active_facility'));
                $provider   =  Provider::where('user_id',$request->selectLifeCoach)->first();
                $provider_id = $provider->provider_id;
                if(!empty($group_list))
                {
                    foreach ($group_list as $group_list_value)
                    {

                        if($group_list_value->user_type == "PATIENT")
                        {
                            $patient   =  Patient::where('patient_id',$group_list_value->patient_id)->first();
                            $patient->lead_coach_id = $provider_id;
                            $patient->update();
                        }

                    }

                }

                $group->lead_user_id  = $request->selectLifeCoach;




            }

            /*if($request->selectFoodCoach != $group->primary_food_coach_id && $group->is_person != 1)
            {
                $this->group_member->where('group_id', $group->group_id)->where('user_id', $request->selectFoodCoach)->delete();
                $this->group_member->where('group_id', $group->group_id)->where('user_id', $group->primary_food_coach_id)->delete();
                DB::table("group_user")->where('user_id', $group->primary_food_coach_id)->where('group_id', $group->group_id)->delete();
                DB::table("group_user")->where('user_id', $request->selectFoodCoach)->where('group_id', $group->group_id)->delete();

               $group_member_fd = new GroupMember;
               $group_member_fd->group_id      = $group->group_id;
               $group_member_fd->user_id       = $request->selectFoodCoach;
               $group_member_fd->timestamp     = date('Y-m-d H:i:s');
               $group_member_fd->save();

               $group->primary_food_coach_id    = isset($request->selectFoodCoach) ? $request->selectFoodCoach : '';
            }*/

            $group->name                     = $request->name;
            $group->capacity                 = $request->capacity;
            $group->is_full                  = $request->capacity <= $group_detail->total_member ? 1 : 0;

            if(!empty($request->diabetesTypeId))
                $group->diabetes_type_id         = $request->diabetesTypeId;

            $group->is_auto_post_enabled	 = $request->autoPostEnable == 1 ? 1 : 0 ;
            $group->auto_post_time	         = $request->autoPostTime;
            $group->preferred_language	     = $request->preferredLanguage;
            $group->update();

            //$this->group_member->where('group_id', $request->id)->where('user_id', $group_detail->lead_user_id)->update(['user_id' => $request->selectLifeCoach]);
            //if(!empty($request->selectFoodCoach))
                //$this->group_member->where('group_id', $request->id)->where('user_id', $group_detail->primary_food_coach_id)->update(['user_id' => $request->selectFoodCoach]);

            if (!$group) {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'Edit Group'])
                ]);
            }

            return $response = [
                'success' => 'Edit Group Successfully!',
            ];
        }
    }




    public function groupDetail()
    {
        if (request()->ajax()) {
            $group_id  = request('id' , 0);
            if($group_id)
            {
                $data = $this->group_model->get_group_detail($group_id);
                return $response = [
                    'success' => $data,
                ];
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'get detail'])
                ]);
            }
        }
    }



    /**
     * Process delete a Group From Facility
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteGroup() {

        $group_id = request('id');
        info('Deleting Group GroupId: ' . $group_id);
        $group_detail = $this->group_model->get_group_detail($group_id);
        if(!empty($group_detail))
        {
            if($group_detail->is_person)
            {
                info('Deleting a in_person Group');
                DB::table("$this->group_db.group_session")->where('group_id', $group_id)->delete();
                DB::table("$this->group_db.post_comment")->where('group_id', $group_id)->delete();
                DB::table("$this->group_db.post")->where('group_id', $group_id)->delete();
                $this->group_member->where('group_id', $group_id)->delete();
                DB::table("group_user")->where('group_id', $group_id)->delete();
                $is_person = Groups::find($group_id);
                $is_person->delete();

                if (!$is_person) {
                    return response()->json([
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Delete Group'])
                    ]);
                }
            }
            else
            {
                info('Deleting a Online Group');
                DB::table("$this->group_db.post_comment")->where('group_id', $group_id)->delete();
                DB::table("$this->group_db.post")->where('group_id', $group_id)->delete();
                $this->group_member->where('group_id', $group_id)->delete();
                DB::table("group_user")->where('group_id', $group_id)->delete();
                $online_group = Groups::find($group_id);
                $online_group->delete();

                if (!$online_group) {
                    return response()->json([
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Delete Group'])
                    ]);
                }
            }
        }

        return response()->json(
            [
                'success' => 'Group deleted Successfully!'
            ]
        );
    }



    public function removeCoachFromGroup()
    {
        if (request()->ajax()) {
            $group_id   = request('gid' , 0);
            $user_id    = request('uid' , 0);
            info('Deleting a Coach from Group: ' . $group_id);
            if($group_id)
            {
                $data = $this->group_member->where('group_id', $group_id)->where('user_id', $user_id)->delete();
                DB::table("group_user")->where('user_id', $user_id)->where('group_id', $group_id)->delete();
                if(!$data)
                {
                    return response()->json([
                        'error' => trans('common.unable_to_do_action', [ 'action' => 'Remove Coach'])
                    ]);
                }

                return $response = [
                    'success' => 'Coach Removed Successfully!',
                ];
            }
            else
            {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'Remove Coach'])
                ]);
            }
        }
    }




    public function getFoodLifestyleCoachData()
    {
        $group_id       = request('group_id' , 0);
        $all_coaches    = $this->coach_model->get_facility_coach_list(session('active_facility'));
        $group_members  = collect($this->group_model->get_group_coaches_detail($group_id));
        $user_ids       = $group_members->pluck( 'user_id' )->toArray();
        $resultArray    = $this->_group_coachListBuilder( $all_coaches, $user_ids );

        $lead_coach = array();
        $food_coach = array();
        $lead_coach = $resultArray['lead_coach'];
        $food_coach = $resultArray['food_coach'];

        return response()->json(
            [
                'leadcoach' => $lead_coach,
                'foodcoach' => $food_coach,
                'success'=> true
            ]
        );
    }


    /**
     * @param $all_coaches
     *
     * @return array
     */
    private function _group_coachListBuilder( $all_coaches, $user_ids )
    {
        $foodCoach = [];
        $leadCoach = [];
        $response = $this->coach_model->get_facility_coach_list(session('active_facility'));

        foreach ($all_coaches as $coach) {

            if(in_array($coach->user_id, $user_ids))
            {
                continue;
            }
            $providers     = [ ];
            $providersLead = [ ];
            if ($coach->user_type == "Coach") {
                //$providersLead[$coach->provider_id] = @$coach->first_name . " " . @$coach->last_name;
                $providersLead['full_name']     = @$coach->first_name . " " . @$coach->last_name;
                $providersLead['provider_id']   = $coach->user_id;
                $providersLead['email']         = $coach->email;

            } else if ($coach->user_type == "Food Coach") {
                $providers['full_name']     = @$coach->first_name . " " . @$coach->last_name;
                $providers['provider_id']   = $coach->user_id;
                $providers['email']         = $coach->email;
            }
            if ( ! empty( $providers )) {
                $foodCoach[] = $providers;
            }
            if ( ! empty( $providersLead )) {
                $leadCoach[] = $providersLead;
            }
        }
        return [ 'food_coach' => $foodCoach, 'lead_coach' => $leadCoach ];
    }



    /**
     * Process assign coach to Group
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignCoachToGroup()
    {

        if (request()->ajax()) {
            $coach_type = request('membership');
            $group_id   = request('group_id');

            if ($coach_type == 1)
                $coach_id = request('select_multi_food');
            else
                $coach_id = request('select_multi_life');

            $data = $this->group_member->where('group_id', $group_id)->where('user_id', $coach_id)->get();
            if(count($data) > 0)
            {
                return response()->json([
                    'error' => 'Selected Coach already exist into that Group'
                ]);
            }

            $loggedInUser = session('user');
            info('Assign New Coach To GroupID: ' . $group_id . ' Request by: ' . $loggedInUser->userName . ' Facility: ' . session('active_facility'));

            $group_member_ld = new GroupMember;
            $group_member_ld->group_id      = $group_id;
            $group_member_ld->user_id       = $coach_id;
            $group_member_ld->timestamp     = date('Y-m-d H:i:s');
            $group_member_ld->save();

            $group_user_id = new GroupUser;
            $group_user_id->group_id      = $group_id;
            $group_user_id->user_id       = $coach_id;
            $group_user_id->save();

            if (!$group_member_ld) {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', ['action' => 'Assign Coach'])
                ]);
            }

            return response()->json(
                [
                    'success' => 'Coach Assign To Group Successfully!'
                ]
            );
        }
    }


}

<?php

namespace HealthSlatePortal\Http\Controllers\facilityadmin;

use HealthSlatePortal\Models\Eloquent\Provider;
use Illuminate\Http\Request;
use HealthSlatePortal\Helpers\DataTableServiceProvider;
use HealthSlatePortal\Http\Requests;
use HealthSlatePortal\Http\Controllers\Controller;
use HealthSlatePortal\Models\FacilityModel;
use HealthSlatePortal\Models\CoacheModel;
use HealthSlatePortal\Models\Eloquent\Facility;
use HealthSlatePortal\Models\Eloquent\FacilityProvider;
use HealthSlatePortal\Models\Eloquent\User;
use HealthSlatePortal\Models\Eloquent\UserRole;
use HealthSlatePortal\Models\Eloquent\EmailNotificationMessage;
use HealthSlatePortal\Helpers\MailServiceHelper;
use HealthSlatePortal\Helpers\SmsServiceHelper;
use Illuminate\Support\Facades\Log;
use Validator;
use Session;
use DB;

class CoachController extends Controller {

    protected $facility_model;
    protected $facility;
    protected $facilityProvider;
    protected $coach_model;
    protected $dataTableService;
    protected $user;
    protected $mailServiceHelper;
	protected $data = array();


    function __construct( FacilityProvider $facilityProvider,  MailServiceHelper $mailServiceHelper, SmsServiceHelper $smsServiceHelper, FacilityModel $facility_model, Facility $facility, CoacheModel $coach_model, DataTableServiceProvider $dataTableService, User $user) {
        $this->facility_model       = $facility_model;
        $this->coach_model          = $coach_model;
        $this->facility             = $facility;
        $this->facilityProvider     = $facilityProvider;
        $this->dataTableService     = $dataTableService;
        $this->mailServiceHelper    = $mailServiceHelper;
        $this->smsServiceHelper     =  $smsServiceHelper;
        $this->user                 = $user;
        $this->data['js']           = array('coach');
    }

	/**
	 * Display a listing of Coach List.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

        return view( 'facilityadmin.coach.coach', $this->data);
	}


    /**
     * @param $all_coaches
     *
     * @return array
     */
    private function _coachListBuilder( $all_coaches )
    {
        $foodCoach = [];
        $leadCoach = [];
        $response = $this->coach_model->get_facility_coach_list(session('active_facility'));
        $coach_ids = [];
        if(!empty($response))
        {
            foreach ($response as $coach) {
                $coach_ids[] = $coach->provider_id;
            }
        }


        foreach ($all_coaches as $coach) {


            if(in_array($coach->provider_id, $coach_ids))
            {
                continue;
            }
            $providers     = [ ];
            $providersLead = [ ];
            if ($coach->user_type == "Coach") {
                //$providersLead[$coach->provider_id] = @$coach->first_name . " " . @$coach->last_name;
                $providersLead['full_name'] = @$coach->first_name . " " . @$coach->last_name;
                $providersLead['provider_id'] = @$coach->provider_id;
                $providersLead['email'] = @$coach->email;

            } else if ($coach->user_type == "Food Coach") {
                $providers['full_name'] = @$coach->first_name . " " . @$coach->last_name;
                $providers['provider_id'] = @$coach->provider_id;
                $providers['email'] = @$coach->email;
            }
            if ( ! empty( $providers )) {
                $foodCoach[] = $providers;
            }
            if ( ! empty( $providersLead )) {
                $leadCoach[] = $providersLead;
            }
        }
        return [ 'food_coach' => $foodCoach, 'lead_coach' => $leadCoach ];
    }


    /**
     * Display all Coaches Of The Facility.
     *
     * @return \Illuminate\Http\Response
     */
	public function coachesList()
    {
        $response = $this->coach_model->get_facility_coach_list(session('active_facility'));
        info('Total Coaches Found: ' . count($response));
        $data = collect();

        foreach ($response as $coach) {
           if($coach->user_type != "Tech Support")
           {
            $notification = ($coach->is_sms_enabled == 1) ? '<a title="SMS Enabled" class="btn font-dark btn-hide sbold"><i class="fa fa-mobile"></i></a>' : '<a title="SMS Disabled" class="btn font-grey-salsa btn-hide sbold"><i class="fa fa-mobile"></i></a>';
            $notification = ($coach->is_email_enabled == 1) ? "$notification<a title=\"Email Enabled\" class=\"btn font-dark btn-hide sbold\"><i class=\"fa fa-envelope\"></i></a>" : "$notification<a title=\"Email Disabled\" class=\"btn font-grey-salsa btn-hide sbold\"><i class=\"fa fa-envelope\"></i></a>" ;
            $row = [
                'hidden_id'         => '',
                'provider_id'       => $coach->provider_id,
                'name'              => $coach->first_name . ' ' . $coach->last_name,
                'email'             => $coach->email,
                'phone'             => $coach->phone,
                'user_type'         => $coach->user_type,
                'is_email_enabled'  => $notification,
                'action'            => '<a title="Edit" href="'.route('facilityadmin.coach-edit', ['coach_id' => base64_encode($coach->provider_id)]).'" class="btn font-dark btn-hide sbold"><i class="fa fa-edit"></i></a>
                                    <a title="Remove" data-toggle="modal" data-id="'.$coach->provider_id.'" data-target="#remove_coach" class="btn font-dark btn-hide sbold"><i class="fa fa-times"></i></a>'

            ];
            $data->push(array_values($row));
           }
        }
        info('Sending Data To DataTableService');
        return $this->dataTableService->dataTableWithIds($data, 'coaches');
    }


    /**
     * Process delete request of delete Coach From Facility Provider Table
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete() {
        $coach_id = request('id');
        $deleted = false;
        $response = $this->facilityProvider
            ->where('providers_provider_id', $coach_id)
            ->where('facility_facility_id', session('active_facility'))
            ->get();

        $loggedInUser = session('user');
        $user = $this->user->where('email', $loggedInUser->userName)->first();

        if (empty($user) || empty($response)) {
            info('Delete Coach From Facility');
            return response()->json([
                'error' => trans('common.invalid_request_data'),
            ]);
        }
        info('Deleting Coach From Facility CoachID: ' . $coach_id . ' Request by: ' . $loggedInUser->userName);

        $deleted = $this->facilityProvider
            ->where('providers_provider_id', $coach_id)
            ->where('facility_facility_id', session('active_facility'))
            ->delete();

        if (!$deleted) {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'remove coach'])
            ]);
        }

        return response()->json(
            [
                'success' => 'Coach removed from this facility Successfully.'
            ]
        );
    }


    /**
     * Process assign coach For Facility Provider Table
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignCoach() {
        $membership = request('membership');

        if($membership == 1)
            $coach_id = request('select_multi_food');
        else
            $coach_id = request('select_multi_life');

        $save = false;

        $response = $this->facilityProvider
            ->where('providers_provider_id', $coach_id)
            ->where('facility_facility_id', session('active_facility'))
            ->get();

        $loggedInUser = session('user');
        $user = $this->user->where('email', $loggedInUser->userName)->first();

        if (empty($user) || count($response) != 0 ) {
            info('Error to Assign New Coach For Facility');
            return response()->json([
                'error' => trans('common.invalid_request_data'),
            ]);
        }
        info('Assign New Coach For Facility CoachID: ' . $coach_id . ' Request by: ' . $loggedInUser->userName . ' Facility: ' . session('active_facility'));

        $save = new FacilityProvider;
        $save->providers_provider_id    = $coach_id;
        $save->facility_facility_id     = session('active_facility');
        $save->save();

        if (!$save) {
            return response()->json([
                'error' => trans('common.unable_to_do_action', [ 'action' => 'assign coach'])
            ]);
        }

        return response()->json(
            [
                'success' => 'Coach assign to this facility successfully.'
            ]
        );
    }



    /**
     * Get Detail of the Coach
     *
     * @return \Illuminate\Http\Response
     */
    public function coachEdit($coach_id = null) {
        $this->data['add_coach']    = false;
        if ($coach_id == 'add') {
            $this->data['coach']        = array();
            $this->data['state']      = state();
            $this->data['coach_type']   = coach_type();
            $this->data['add_coach']    = true;
            return view( 'facilityadmin.coach.edit-coach', $this->data );
        }

        $response = $this->coach_model->get_facility_coach_list(session('active_facility'), base64_decode($coach_id));
        info('Total Coaches Found: ' . count($response));
        $data = collect($response);
        $this->data['coach']        = $data->first();
        $this->data['coach_type']   = coach_type();
        $this->data['state']      = state();
        return view( 'facilityadmin.coach.edit-coach', $this->data );
    }



    /**
     * Update Details of the Coach
     *
     * @return \Illuminate\Http\Response
     */
    public function coachEditStore(Request $request) {

        /*$subject    = 'Cached ';
        $message    = "Cached. Please <a href=''>Click Here</a> to view";
        $body       = ['content' => $message ];
        $to         = 'rajkumar.patidar62@gmail.com';
        $this->mailServiceHelper->sendEmail( $subject, $body, trim( $to ) );*/


        if (request()->ajax()) {

            $user_id = base64_decode(request()->input('user_id'));
            $validator = Validator::make(request()->input(), [
                'user_id'       => 'required',
                'first_name'    => 'required',
                'last_name'     => 'required',
                //'coach_type'    => 'required',
                'email'         => 'required|unique:users,email,'. $user_id.',user_id',
                'phone'         => 'required',
                'city'         => 'required',
                'state'         => 'required',

            ]);

            if ($validator->fails()) {
                info('Coach Update Validation Failed');
                foreach ($validator->errors()->all() as $key => $value)
                {
                    return $response = [
                        'error' => $value,
                    ];
                    break;
                }
            }

            info('Coach update validation success');

            $data = User::find($user_id);
            $data->first_name       = $request->first_name;
            $data->last_name        = $request->last_name;
            $data->email            = $request->email;
            $data->phone            = $request->phone;
            $data->city            = $request->city;
            $data->state           = $request->state;
            $data->update();

            $provider = Provider::find(base64_decode($request->coach_id));
            $provider->is_sms_enabled       = $request->is_sms_enabled;
            $provider->is_email_enabled     = $request->is_email_enabled;
            $provider->acuity_link          = $request->acuity_link;
            $provider->update();

            if (!$data || !$provider) {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'update coach'])
                ]);
            }

            Session::flash('message', trans( 'common.coach_edit' ));

            return $response = [
                'success' => 'Coach updated Successfully!',
                'redirect_url'=> route('facilityadmin.coaches'),
            ];
        }
    }


    /**
     * Add Details of the Coach
     *
     * @return \Illuminate\Http\Response
     */
    public function coachAddStore(Request $request) {

        if (request()->ajax()) {

            $user_id = base64_decode(request()->input('user_id'));
            $validator = Validator::make(request()->input(), [
                'first_name'    => 'required',
                'last_name'     => 'required',
                'coach_type'    => 'required',
                'email'         => 'required|unique:users,email,'. $user_id.',user_id',
                'phone'         => 'required',
                'city'         => 'required',
                'state'         => 'required',
            ]);

            if ($validator->fails()) {
                info('Add New Coach Validation Failed');
                foreach ($validator->errors()->all() as $key => $value)
                {
                    return $response = [
                        'error' => $value,
                    ];
                    break;
                }
            }

            info('Add New Coach validation success');

            $data = new User;
            $data->first_name       = $request->first_name;
            $data->last_name        = $request->last_name;
            $data->user_type        = $request->coach_type;
            $data->email            = $request->email;
            $data->phone            = $request->phone;
            $data->city            = $request->city;
            $data->state           = $request->state;
            $data->is_enabled       = 1;
            $data->registration_date  = date('Y-m-d H:i:s');
            $data->cobrand_id       = Session::get('user')->cobrandId  ;
            $data->save();

            $provider = new Provider;
            $provider->user_id              = $data->user_id;
            $provider->type                 = $request->coach_type;
            $provider->is_sms_enabled       = $request->is_sms_enabled;
            $provider->is_email_enabled     = $request->is_email_enabled;
            $provider->acuity_link          = $request->acuity_link;
            $provider->save();

            $facility_provider = new FacilityProvider;
            $facility_provider->providers_provider_id      = $provider->provider_id;
            $facility_provider->facility_facility_id       = session('active_facility');
            $facility_provider->save();

            $user_role = new UserRole;
            $user_role->authority  = "ROLE_PROVIDER";
            $user_role->user_id    =    $data->user_id;
            $user_role->save();


            if (!$data || !$provider || !$facility_provider) {
                return response()->json([
                    'error' => trans('common.unable_to_do_action', [ 'action' => 'add new coach'])
                ]);
            }


            /* email and message send  */

            $user_id = $data->user_id;
            $user_email = $data->email;
            $user_first_name = $data->first_name;
            $token = md5(uniqid(rand(), true));
            $token_expiry = time()* 1000 ;
            $sent_time  = time() ;
            $is_expired = 0;
            $pin_code =  rand ( 10000 , 99999 );
            $sms_body = "Please use following pin while resetting your password:" . $pin_code;
            if(Session::get('user')->cobrandId==2) {
                $link = env('APP_BASE_URL_SOLERA') . "token/" . $token;
                $email_body=  "<html><body><br/>Hello,<br/><br/> Please use the link below to create your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The HealthSlate Team<br/><br/></body></html>";
            }
            else {
                $link = env('APP_BASE_URL') . "token/" . $token;
                $email_body=  "<html><body><br/>Hello,<br/><br/> Please use the link below to create your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The HealthSlate Team<br/><br/></body></html>";
            }


            EmailNotificationMessage::where('user_id', '=', $user_id)->update(array('is_expired' => DB::raw(1)));
            $email_notification =  new EmailNotificationMessage;
            $email_notification->user_id = $user_id;
            $email_notification->email_body = $email_body;
            $email_notification->sms_body = $sms_body;
            $email_notification->token = $token;
            $email_notification->token_expiry = $token_expiry;
            $email_notification->pin_code = $pin_code;
            $email_notification->sent_time = $sent_time;
            $email_notification->is_expired = DB::raw(0);
            $email_notification->save();
            $name = $user_first_name;
            if(Session::get('user')->cobrandId==2)
                $message = "<html><body>You're receiving this e-mail because you forgot your SoleraOne Program account password. Please use the link below to reset your password: <br/><a href='" . $link . "'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";
            else
                $message = "<html><body>You're receiving this e-mail because you forgot your HealthSlate Program account password. Please use the link below to reset your password: <br/><a href='" . $link . "'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";
            $body = ['name'=>$name,'content' => $message,'subject' =>'Reset Password Request' ];
            $to = $user_email;
            info( 'Processing send email request' );
//mail sending function is call here
            $this->mailServiceHelper->sendEmail($body, trim($to));

            if (preg_match_all("/[\+]/", $data->phone)!=1)
                $sms_to = "+1".$data->phone;
            else
                $sms_to = $data->phone;

            info( 'Processing send message request' );
//sms sending function is call here
            if($data->phone!=""){
                $return_sms_id = $this->smsServiceHelper->sendSms($sms_body,$sms_to);
                if($return_sms_id)
                    info( " Message Send Successfully  to user id :  $user_id ");
                else
                    info( " Message not Send Successfully to user id : $user_id" );
            }

            Session::flash('message', trans( 'common.new_coach_add' ));


            return $response = [
                'success' => 'Add new coach Successfully!',
                'redirect_url'=> route('facilityadmin.coaches'),
            ];
        }
    }

    /**
     * Coach reset password function

     */

    public function coachResetPassword(Request $request)
    {
        //fetch user information
        info('fetch user information');
        $user = User::where('user_id', $request->user_id)->where('user_type','!=', 'PATIENT')->first();
        if(empty($user))
        {

            info('user data not found in database user id : '.$request->user_id);
            return response()->json([
                'error' => 'User not found into our system.'
            ]);
        }

        if(!empty($user))
        {


            $user_id = $user->user_id;
            $user_email = $user->email;
            $user_first_name = $user->first_name;
            $token = md5(uniqid(rand(), true));
            $token_expiry = time()* 1000 ;
            $sent_time  = time() ;
            $is_expired = 0;
            $pin_code =  rand ( 10000 , 99999 );
            if(Session::get('user')->cobrandId==2){
                $link =  env('APP_BASE_URL_SOLERA')."token/".$token;
                $sms_body = "Please use following pin while resetting your password:".$pin_code;
                $email_body=  "<html><body><br/>Hello,<br/><br/>You're receiving this e-mail because you forgot your SoleraOne Program account password. Please use the link below to reset your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The SoleraOne Team<br/><br/></body></html>";

            }else{
                $link =  env('APP_BASE_URL')."token/".$token;
                $sms_body = "Please use following pin while resetting your password:".$pin_code;
                $email_body=  "<html><body><br/>Hello,<br/><br/>You're receiving this e-mail because you forgot your HealthSlate Program account password. Please use the link below to reset your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/><br/>Sincerely,<br/>The HealthSlate Team<br/><br/></body></html>";

            }
            EmailNotificationMessage::where('user_id', '=', $user_id)->update(array('is_expired' => DB::raw(1)));
            $email_notification =  new EmailNotificationMessage;
            $email_notification->user_id = $user_id;
            $email_notification->email_body = $email_body;
            $email_notification->sms_body = $sms_body;
            $email_notification->token = $token;
            $email_notification->token_expiry = $token_expiry;
            $email_notification->pin_code = $pin_code;
            $email_notification->sent_time = $sent_time;
            $email_notification->is_expired = DB::raw(0);
            $email_notification->save();
            $name = $user_first_name;
            if(Session::get('user')->cobrandId==2){
                $message ="<html><body>You're receiving this e-mail because you forgot your SoleraOne Program account password. Please use the link below to reset your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";

            }else{
                $message ="<html><body>You're receiving this e-mail because you forgot your HealthSlate Program account password. Please use the link below to reset your password: <br/><a href='".$link."'>Click Here</a><br/><br/>If you did not request a password reset, you may ignore this message.<br/></body></html>";

            }
            $body = ['name'=>$name,'content' => $message,'subject' =>'Reset Password Request' ];
            $to = $user_email;
            info( 'Processing send email request' );
            //mail sending function is call here
            $this->mailServiceHelper->sendEmail($body, trim($to));
            if (preg_match_all("/[\+]/", $user->phone)!=1)
                 $sms_to = "+1".$user->phone;
            else
                 $sms_to = $user->phone;

            info( 'Processing send message request' );
            //sms sending function is call here

            if($user->phone!=""){
                $return_sms_id = $this->smsServiceHelper->sendSms($sms_body,$sms_to);
                info(" Message success and  failure  info :  $return_sms_id ");
                if($return_sms_id)
                 info(" Message Send Successfully  to user id :  $user_id ");
                else
                    info(" Message not Send Successfully to user id : $user_id");

            }
        }

        return response()->json(
            [
                'success' => 'An email has been sent successfully to your email address.'
            ]
        );
    }



    public function getFoodLifestyleCoachData()
    {

        $all_coaches = $this->coach_model->get_cobrand_all_coach_list(session('active_facility'));
        $resultArray = $this->_coachListBuilder( $all_coaches );


        $lead_coach = array();
        $food_coach = array();
        $lead_coach = $resultArray['lead_coach'];
        $food_coach = $resultArray['food_coach'];

        return response()->json(
            [
                'leadcoach' => $lead_coach,
                'foodcoach' => $food_coach,
                'success'=> true
            ]
        );
    }




}

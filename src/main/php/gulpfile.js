var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
 mix

 /**
  *     Compile Less files
  */
     .less(['hs-main.less','media.less'], 'resources/assets/vendor/tmp/hs-main-compiled.css')
 /**
  *     Combine & Minify CSS files
  */
     /*.styles([
      '../vendor/bootstrap/dist/css/bootstrap.min.css',
      '../vendor/font-awesome/css/font-awesome.min.css',
      'AdminLTE.css',
      'skins/skin-green.css',
      '../vendor/datatables/media/css/dataTables.bootstrap.css',
      '../vendor/tmp/hs-main-compiled.css'
     ], 'public/css/application.css')*/

     .styles([
         '../global/plugins/font-awesome/css/font-awesome.min.css',
         '../global/plugins/simple-line-icons/simple-line-icons.min.css',
         '../global/plugins/bootstrap/css/bootstrap.min.css',
         '../global/plugins/bootstrap-datetimepicker/css/bootstrap-datepicker3.min.css',
         '../global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',

         '../global/css/components.css',
         '../global/plugins/datatables/datatables.min.css',
         '../global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

         '../global/plugins/select2/css/select2.min.css',
         '../global/plugins/select2/css/select2-bootstrap.min.css',
         '../global/plugins/bootstrap-toastr/toastr.min.css',
         '../global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
         '../global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
         '../global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

         '../layouts/layout3/css/layout.min.css',
         '../layouts/layout3/css/themes/default.min.css',
         '../layouts/layout3/css/Glyphter.css',

         '../apps/css/todo-2.min.css',
         '../apps/css/inbox.min.css',

         '../pages/css/login.min.css',
         '../pages/css/profile.min.css',
         '../pages/css/profile-2.min.css',
         '../pages/css/about.min.css',
         '../layouts/layout3/css/custom.css',

     ], 'public/css/application.css')

     /*.styles(['../vendor/select2/dist/css/select2.min.css'], 'public/css/select2.css')*/

     /**
      *     Copy fonts & images to public folder
      */

     .copy(
     'resources/assets/vendor/bootstrap/fonts',
     'public/build/fonts'
     )
    .copy(
         'resources/assets/vendor/font-awesome/fonts',
         'public/build/fonts'
     )
     .copy(
         'resources/assets/global/plugins/simple-line-icons/fonts',
         'public/build/fonts'
     )
     .copy(
     'resources/assets/fonts',
     'public/build/fonts'
     )
     .copy(
     'resources/assets/img',
     'public/img'
 )
     /*.copy(
         'resources/assets/apps',
         'public/apps'
     )*/
     .copy(
         'resources/assets',
         'public/assets'
     )
     .copy(
         'resources/assets/pages/css/error.min.css',
         'public/css'
     )
     .copy(
         'resources/assets/js',
         'public/js'
     )

     .copy(
         'resources/assets/help',
         'public/assets/help'
     )


     /**
      *     Combine & Minify js files
      */
     /*.scripts([
      '../vendor/jquery/dist/jquery.js',
      '../vendor/bootstrap/dist/js/bootstrap.js',
      '../vendor/datatables/media/js/jquery.dataTables.js',
      '../vendor/datatables/media/js/dataTables.bootstrap.js',
      '../vendor/js-cookie/src/js.cookie.js',
      '../vendor/moment/min/moment.min.js',
      'app.js',
      'healthslate.js',
      'hs-main.js',
      'create-suggested-meal.js',
      'manage-messages.js'
     ], 'public/js/application.js')*/


     .scripts([
         '../global/plugins/jquery.min.js',
         '../global/plugins/jquery-ui/jquery-ui.min.js',
         '../global/plugins/bootstrap/js/bootstrap.min.js',
         '../global/plugins/bootstrap-datetimepicker/js/bootstrap-datepicker.min.js',

         '../global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
         '../global/scripts/app.js',
         '../global/scripts/datatable.js',
         '../global/plugins/datatables/datatables.min.js',
         '../global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js',

         '../global/plugins/moment.min.js',
         // '../global/plugins/highcharts/js/highcharts.js',
         // '../global/plugins/highcharts/js/highcharts-3d.js',
         // '../global/plugins/highcharts/js/highcharts-more.js',

         '../global/plugins/select2/js/select2.full.min.js',
         '../global/plugins/jquery-validation/js/jquery.validate.min.js',
         '../global/plugins/jquery-validation/js/additional-methods.min.js',

         '../global/plugins/jquery.blockui.min.js',
         '../global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
         '../global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
         '../global/plugins/jquery.sparkline.min.js',

         '../global/plugins/bootstrap-toastr/toastr.min.js',
         '../global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
         '../global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
         '../global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
         '../pages/scripts/ui-toastr.min.js',
         '../pages/scripts/ui-blockui.min.js',

         '../global/plugins/jquery-idle-timeout/jquery.idletimeout.js',
         '../global/plugins/jquery-idle-timeout/jquery.idletimer.js',

         '../global/scripts/app.js',
         '../layouts/layout3/scripts/layout.js',
         '../pages/scripts/login.min.js',
         '../pages/scripts/profile.min.js',

         'healthslate.js',
         'hs-main.js',
         'facility.js',
         'page-loader.js',
         'message-tag.js',
         'ui-idletimeout.js',
         //'dashboard.js',


     ], 'public/js/application.js')



     /*.scripts([
         '../vendor/select2/dist/js/select2.min.js'
     ], 'public/js/select2.js');*/



     /**
      * Do Versioning of assets
      */
     mix.version([
          'css/error.min.css',
          'css/application.css', 'js/message.js','js/member.js','js/member-recruitment.js','js/group.js',
          'js/application.js', 'js/coach.js','js/setting.js'
        ]);
    });
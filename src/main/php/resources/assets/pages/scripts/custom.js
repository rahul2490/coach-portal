/**
Custom module for you to write your own javascript functions
**/
var Custom = function () {

    // private functions & variables

    var myFunc = function(text) {
        alert(text);
    }

    // public functions
    return {

        //main function
        init: function () {
            //initialize here something.

        },
        initrangeslider: function () {
            //initialize here something.
            $("#range_2").ionRangeSlider({
                grid: true,
            from: 5,
            values: [
                "Very Slow", "Slow",
                "Medium ", "Fast",
                "Very Fast",
            ]
            });
            $("#range_1").ionRangeSlider({
                grid: true,
            from: 5,
            values: [
                "Very Slow", "Slow",
                "Medium ", "Fast",
                "Very Fast",
            ]
            });


        },

        initdatetimepicker:function(){

        $(".form_datetime").datetimepicker({
            autoclose: true,
            isRTL: App.isRTL(),
            format: "dd MM yyyy - hh:ii",
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
        });
        },

        initmonthlychart:function(){
 

        },

        

        //some helper function
        doSomeStuff: function () {
            myFunc();
        }

    };

}();

jQuery(document).ready(function() {    
   Custom.initrangeslider(); 
   Custom.initdatetimepicker(); // you can initiate this separately also in the page scripts also;
    Custom.initmonthlychart();
    $('#modal_add_activity').on('shown.bs.modal', function (e) {
         Custom.initrangeslider(); 
   Custom.initdatetimepicker();
  // do something...
    });
});

/***
Usage
***/
//Custom.doSomeStuff();
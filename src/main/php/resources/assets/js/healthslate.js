/**
 * Created by 31/08/2017 on 31/08/2017.
 */

//To set cookies
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
/*! jstz - v1.0.4 - 2012-12-18 */
(function(e){var t=function(){"use strict";var e="s",n=function(e){var t=-e.getTimezoneOffset();return t!==null?t:0},r=function(e,t,n){var r=new Date;return e!==undefined&&r.setFullYear(e),r.setDate(n),r.setMonth(t),r},i=function(e){return n(r(e,0,2))},s=function(e){return n(r(e,5,2))},o=function(e){var t=e.getMonth()>7?s(e.getFullYear()):i(e.getFullYear()),r=n(e);return t-r!==0},u=function(){var t=i(),n=s(),r=i()-s();return r<0?t+",1":r>0?n+",1,"+e:t+",0"},a=function(){var e=u();return new t.TimeZone(t.olson.timezones[e])},f=function(e){var t=new Date(2010,6,15,1,0,0,0),n={"America/Denver":new Date(2011,2,13,3,0,0,0),"America/Mazatlan":new Date(2011,3,3,3,0,0,0),"America/Chicago":new Date(2011,2,13,3,0,0,0),"America/Mexico_City":new Date(2011,3,3,3,0,0,0),"America/Asuncion":new Date(2012,9,7,3,0,0,0),"America/Santiago":new Date(2012,9,3,3,0,0,0),"America/Campo_Grande":new Date(2012,9,21,5,0,0,0),"America/Montevideo":new Date(2011,9,2,3,0,0,0),"America/Sao_Paulo":new Date(2011,9,16,5,0,0,0),"America/Los_Angeles":new Date(2011,2,13,8,0,0,0),"America/Santa_Isabel":new Date(2011,3,5,8,0,0,0),"America/Havana":new Date(2012,2,10,2,0,0,0),"America/New_York":new Date(2012,2,10,7,0,0,0),"Asia/Beirut":new Date(2011,2,27,1,0,0,0),"Europe/Helsinki":new Date(2011,2,27,4,0,0,0),"Europe/Istanbul":new Date(2011,2,28,5,0,0,0),"Asia/Damascus":new Date(2011,3,1,2,0,0,0),"Asia/Jerusalem":new Date(2011,3,1,6,0,0,0),"Asia/Gaza":new Date(2009,2,28,0,30,0,0),"Africa/Cairo":new Date(2009,3,25,0,30,0,0),"Pacific/Auckland":new Date(2011,8,26,7,0,0,0),"Pacific/Fiji":new Date(2010,11,29,23,0,0,0),"America/Halifax":new Date(2011,2,13,6,0,0,0),"America/Goose_Bay":new Date(2011,2,13,2,1,0,0),"America/Miquelon":new Date(2011,2,13,5,0,0,0),"America/Godthab":new Date(2011,2,27,1,0,0,0),"Europe/Moscow":t,"Asia/Yekaterinburg":t,"Asia/Omsk":t,"Asia/Krasnoyarsk":t,"Asia/Irkutsk":t,"Asia/Yakutsk":t,"Asia/Vladivostok":t,"Asia/Kamchatka":t,"Europe/Minsk":t,"Australia/Perth":new Date(2008,10,1,1,0,0,0)};return n[e]};return{determine:a,date_is_dst:o,dst_start_for:f}}();t.TimeZone=function(e){"use strict";var n={"America/Denver":["America/Denver","America/Mazatlan"],"America/Chicago":["America/Chicago","America/Mexico_City"],"America/Santiago":["America/Santiago","America/Asuncion","America/Campo_Grande"],"America/Montevideo":["America/Montevideo","America/Sao_Paulo"],"Asia/Beirut":["Asia/Beirut","Europe/Helsinki","Europe/Istanbul","Asia/Damascus","Asia/Jerusalem","Asia/Gaza"],"Pacific/Auckland":["Pacific/Auckland","Pacific/Fiji"],"America/Los_Angeles":["America/Los_Angeles","America/Santa_Isabel"],"America/New_York":["America/Havana","America/New_York"],"America/Halifax":["America/Goose_Bay","America/Halifax"],"America/Godthab":["America/Miquelon","America/Godthab"],"Asia/Dubai":["Europe/Moscow"],"Asia/Dhaka":["Asia/Yekaterinburg"],"Asia/Jakarta":["Asia/Omsk"],"Asia/Shanghai":["Asia/Krasnoyarsk","Australia/Perth"],"Asia/Tokyo":["Asia/Irkutsk"],"Australia/Brisbane":["Asia/Yakutsk"],"Pacific/Noumea":["Asia/Vladivostok"],"Pacific/Tarawa":["Asia/Kamchatka"],"Africa/Johannesburg":["Asia/Gaza","Africa/Cairo"],"Asia/Baghdad":["Europe/Minsk"]},r=e,i=function(){var e=n[r],i=e.length,s=0,o=e[0];for(;s<i;s+=1){o=e[s];if(t.date_is_dst(t.dst_start_for(o))){r=o;return}}},s=function(){return typeof n[r]!="undefined"};return s()&&i(),{name:function(){return r}}},t.olson={},t.olson.timezones={"-720,0":"Etc/GMT+12","-660,0":"Pacific/Pago_Pago","-600,1":"America/Adak","-600,0":"Pacific/Honolulu","-570,0":"Pacific/Marquesas","-540,0":"Pacific/Gambier","-540,1":"America/Anchorage","-480,1":"America/Los_Angeles","-480,0":"Pacific/Pitcairn","-420,0":"America/Phoenix","-420,1":"America/Denver","-360,0":"America/Guatemala","-360,1":"America/Chicago","-360,1,s":"Pacific/Easter","-300,0":"America/Bogota","-300,1":"America/New_York","-270,0":"America/Caracas","-240,1":"America/Halifax","-240,0":"America/Santo_Domingo","-240,1,s":"America/Santiago","-210,1":"America/St_Johns","-180,1":"America/Godthab","-180,0":"America/Argentina/Buenos_Aires","-180,1,s":"America/Montevideo","-120,0":"Etc/GMT+2","-120,1":"Etc/GMT+2","-60,1":"Atlantic/Azores","-60,0":"Atlantic/Cape_Verde","0,0":"Etc/UTC","0,1":"Europe/London","60,1":"Europe/Berlin","60,0":"Africa/Lagos","60,1,s":"Africa/Windhoek","120,1":"Asia/Beirut","120,0":"Africa/Johannesburg","180,0":"Asia/Baghdad","180,1":"Europe/Moscow","210,1":"Asia/Tehran","240,0":"Asia/Dubai","240,1":"Asia/Baku","270,0":"Asia/Kabul","300,1":"Asia/Yekaterinburg","300,0":"Asia/Karachi","330,0":"Asia/Kolkata","345,0":"Asia/Kathmandu","360,0":"Asia/Dhaka","360,1":"Asia/Omsk","390,0":"Asia/Rangoon","420,1":"Asia/Krasnoyarsk","420,0":"Asia/Jakarta","480,0":"Asia/Shanghai","480,1":"Asia/Irkutsk","525,0":"Australia/Eucla","525,1,s":"Australia/Eucla","540,1":"Asia/Yakutsk","540,0":"Asia/Tokyo","570,0":"Australia/Darwin","570,1,s":"Australia/Adelaide","600,0":"Australia/Brisbane","600,1":"Asia/Vladivostok","600,1,s":"Australia/Sydney","630,1,s":"Australia/Lord_Howe","660,1":"Asia/Kamchatka","660,0":"Pacific/Noumea","690,0":"Pacific/Norfolk","720,1,s":"Pacific/Auckland","720,0":"Pacific/Tarawa","765,1,s":"Pacific/Chatham","780,0":"Pacific/Tongatapu","780,1,s":"Pacific/Apia","840,0":"Pacific/Kiritimati"},typeof exports!="undefined"?exports.jstz=t:e.jstz=t})(this);
//# sourceMappingURL=public-application.js.map



//Make sure jQuery has been loaded before this file
if (typeof jQuery === "undefined") {
    throw new Error("App requires jQuery");
}

$.HS = {};
(function ($) {

    'use strict';

    var tz = jstz.determine(); // Determines the time zone of the browser client
    var timezone = tz.name();
    setCookie('user_timezone', timezone,1);

    if ($.fn.DataTable) {
        /**
         * Log an error message instead of showing alert, fallback to exception.
         *
         * @param {object} settings dataTables settings object
         * @param {int} level log error messages, or display them to the user
         * @param {string} msg error message
         * @param {int} tn Technical note id to get more information about the error.
         * @private
         */
        $.fn.dataTableExt.sErrMode = function (settings, level, msg, tn) {
            msg = 'DataTables warning: ' +
            (settings !== null ? 'table id=' + settings.sTableId + ' - ' : '') + msg;

            if (tn) {
                msg += '. For more information about this error, please see ' +
                'http://datatables.net/tn/' + tn;
            }
            if (window.console && console.log) {
                console.log(msg);
            }
            else {
                throw new Error(msg);
            }
        };

    }

    $.HS = {

        common_error_message: 'Some Unexpected error occurred, Please refresh the page or try after some time.',

        /**
         * On Image Callback for image lazy loading.
         *
         * @param _this
         * @param img_url
         * @param error
         */
        on_img: function (_this, img_url, error) {

            if (_this.is('img')) {
                _this.attr('src', img_url).removeClass('pending');
            } else {
                _this.css('background-image', 'url(' + img_url + ')').removeClass('pending');
            }
            if (error === true) {
                _this.addClass('no-img');
                var error_css = $.trim(_this.data('error-css'));
                if (error_css.length) {
                    _this.attr('style', _this.attr('style') + ';' + error_css);
                }
            }

        },

        /**
         * Lazy load Images which are pending only
         */
        lazy_load_images: function () {
            var that = this;

            $('.lazy-load.pending').each(function () {
                var _this = $(this);
                var sm = $.trim(_this.data('src'));

                var no_img_url = $('.parameters [name="alt_no_image_url"]').val();
                if (sm != "") {
                    $('<img/>').attr('src', sm).load(function () {
                        that.on_img(_this, sm);
                        $(this).remove();
                    }).error(function () {
                        that.on_img(_this, no_img_url, true);
                        $(this).remove();
                    });
                } else {
                    that.on_img(_this, no_img_url, true);
                }
            });
        },

        /**
         * ajax_response_validator
         *  custom ajax request response validator
         *
         * @param {string} response
         * @param {function} callback
         * @param {boolean} parseJSON
         * @returns {Boolean}
         */
        ajax_response_validator: function (response, callback, parseJSON) {
            var ajaxResponse = response;
            try {
                ajaxResponse = JSON.parse(ajaxResponse);
            } catch (e) {
                if (parseJSON === true) {
                    ajaxResponse = '';
                }
            }
            finally {
                /**
                 * redirecting If Needed
                 *  It should be called after every ajax/post/get call to server
                 */
                if (ajaxResponse != "" && ajaxResponse.redirect_to != undefined) {
                    if (ajaxResponse.wait != undefined) {

                        setTimeout(function () {
                            window.location = ajaxResponse.redirect_to;
                        }, (parseInt(ajaxResponse.wait) * 1000));

                    } else {
                        window.location = ajaxResponse.redirect_to;
                    }
                    // commenting as it was preventing additional callback
                    //return false;
                }
            }
            if (typeof callback === "function") {
                if (parseJSON === true) {
                    callback.apply(this, [ajaxResponse]);
                } else {
                    callback.apply(this, [response]);
                }
            }
        },

        /**
         * call_jquery_ajax
         *        Common function for calling ajax post/get request
         *
         * @param url
         * @param datastring
         * @param parseJSON : only be true when Response::json is not used
         * @param beforeCall
         * @param callback
         * @param method
         * @param showLoading
         * @param loadingElement
         * @param async
         * @returns {*}
         */
        call_jquery_ajax: function (url, datastring, parseJSON, beforeCall, callback, method, showLoading, loadingElement, async) {
            var that = this;
            if (typeof async == "undefined") {
                async = true;
            }
            if (typeof showLoading == "undefined") {
                showLoading = false;
            }
            if (typeof method == "undefined") {
                method = 'POST';
            }
            if (method != 'POST' && method != 'GET') {
                return false;
            }

            if (showLoading && loadingElement) {
                //jQuery(loadingElement).show();
                $('#loding_icon').trigger('click');
            }

            var jqxhr = jQuery.ajax({
                url: url,
                data: datastring,
                type: method,
                async: async,
                global: false,
                cache: false,
                beforeSend: beforeCall
            })
                .done(function (response) {
                    that.ajax_response_validator(response, callback, parseJSON);
                })
                .fail(function () {
                    if (console && console.log) {
                        console.log('ajax failed');
                    }
                })
                .always(function () {
                    if (loadingElement) {
                        jQuery(loadingElement).hide();
                    }
                });
            return jqxhr;
        },

        /**
         * Build bootstrap alert in a placeholder
         *
         * @param append_to
         * @param message
         * @param type
         * @param destroy_after
         * @returns {boolean}
         */
        build_notification: function (append_to, message, type, destroy_after) {
            var _target = $(append_to);
            if (_target.length != 1) {
                return false;
            }
            if (type == undefined) {
                type = 'success';
            }
            if (destroy_after == undefined) {
                destroy_after = 5;
            }
            var _html = $('<div role="alert" class="alert alert-' + type + ' alert-dismissible fade in">\
                    <button aria-label="Close" data-dismiss="alert" class="close" type="button"><span aria-hidden="true">×</span></button> \
                    <p> ' + message + ' </p>  </div>');
            setTimeout(function () {
                _html.slideUp(function () {
                    $(this).remove();
                });
            }, (parseInt(destroy_after) * 1000));
            _target.append(_html);
            return true;
        },

        /**
         *  convert timestamp to user timezone date time
         */
        refresh_dates: function () {
            var default_format = $('.parameters [name="default_js_date_time_format"]').val();
            $('.timestamp:not(.done)').each(function () {
                var _this = $(this),
                    timestamp = moment.unix($.trim(_this.data('timestamp'))),
                    format = $.trim(_this.data('format'));
                if (format.length == 0) {
                    format = default_format;
                }
                _this.addClass('done');
                if (timestamp.isValid()) {
                    _this.text(timestamp.format(format)).attr('title', 'In Your Timezone');
                }
            });
        },

        /**
         * get JSON from string
         * @param text
         * @returns {{}}
         */
        getJSON: function (text) {
            var response = {};
            try {
                response = JSON.parse(text);
            } catch (e) {
            }
            return response;
        },

        /**
         *  Convert a Image Data URL to Raw Data String
         * @param dataURL
         * @returns {*}
         */
        getRawImageData: function (dataURL) {
            return dataURL.replace(/^data:image\/(png|jpg|jpeg|gif);base64,/, '');
        },

        /**
         * Initialize App
         */
        init: function () {
            var that = this;

            if (typeof(window.sessionStorage) == 'undefined') {
                // defining dummy function
                // for browsers not supporting sessionStorage
                window.sessionStorage = {
                    clear: function () {
                    },
                    getItem: function () {
                    },
                    setItem: function () {
                    },
                    removeItem: function () {
                    }
                };
            }

            /**
             *  global ajax Complete event for datatables
             */
            $(document).ajaxComplete(function (event, xhr, settings) {
                if (xhr.responseJSON) {
                    that.ajax_response_validator(xhr.responseJSON);
                }
            });

            setTimeout(function () {
                that.lazy_load_images();
            }, 300);

            // Do Tz conversion for `.timestamp` elements
            that.refresh_dates();
        }
    };
})(jQuery);
$(document).ready(function() {
       /* var $radios = $('input:radio[name=options]');
        if($radios.is(':checked') === false) {
            $radios.filter('[value=1]').prop('checked', true);
        }



    $("input[name=options]:radio").change(function () {
        var option=$(this).val();
        var patient_id=$('#patient_id').val();
        var filter_by_type = $("#FilterByType").val();
        if(option==1){
            $.ajax({
                url: APP_BASE_URL+'logbook/log_book_by_week',
                data: {
                    'patient_id': patient_id,
                    'date': $('.week_date_range').text(),
                    'meal_type': filter_by_type,
                },
                type: "get",
                success : function (response) {
                    $('#full-log-book').empty();
                    $('#full-log-book').html(response);
                }

            });
        }else{
            $.ajax({
                url: APP_BASE_URL+'logbook/log_book_by_month',
                data: {
                    'patient_id': patient_id,
                    'date': $('.month_date_range').text(),
                    'meal_type': filter_by_type,
                },
                type: "get",
                success : function (response) {
                    $('#full-log-book').empty();
                    $('#full-log-book').html(response);
                }

            });
        }

    });
*/


    $("#FilterByType").change(function () {
        var meal_type = $('#FilterByType').val();
        var patient_id = $('#patient_id').val();
        var date_range = $('.week_date_range').text();
        $('#loding_icon').trigger('click');
        $.ajax({
            url: APP_BASE_URL + 'logbook/log_book_by_filter',
            data: {
                patient_id: patient_id, meal_type: meal_type, date:date_range,
            },
            type: "get",
            success: function (response) {
                $('#full-log-book').empty();
                $('#full-log-book').html(response);
                window.setTimeout(function () {App.stopPageLoading();}, 1000);
            }

        });


    });

    // $('body').on('click', '.fa-angle-left', function (e) {
    //     var option = $('input[name=options]:checked').val();
    //     alert(option);
    // });
/*
    $('body').on('click', '.date-filter', function (e) {
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }
        var option = $('input[name=options]:checked').val();
        if(option==1){
            $.ajax({
                url: APP_BASE_URL + 'logbook/get_log_book_by_date_range_weekly',
                data: {
                    'patient_id':8011,
                    'date': $('.date_range').text(),
                    'clicked':clicked,
                },
                type: "get",
                success : function (response) {

                }
            });
        }else{
            $.ajax({
                url: APP_BASE_URL + 'logbook/get_log_book_by_date_range_monthly',
                data: {
                    'patient_id':8011,
                    'date': $('.date_range').text(),
                    'clicked':clicked,
                },
                type: "get",
                success : function (response) {
                }
            });
        }

    });*/



    $('body').on('click', '.week', function (e) {

        var meal_type  = $('#FilterByType').val();
        var patient_id = $('#patient_id').val();
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }
        $('#loding_icon').trigger('click');

            $.ajax({
                url: APP_BASE_URL + 'logbook/get_log_book_by_date_range_weekly',
                data: {
                    'patient_id':patient_id,
                    'date': $('.week_date_range').text(),
                    'clicked':clicked,
                    'meal_type':meal_type,
                },
                type: "get",
                success : function (response) {

                        $('.week_date_range').text(response.start + ' - ' + response.end);
                        $('#full-log-book').empty();
                        $('#full-log-book').html(response.success);
                         window.setTimeout(function () {App.stopPageLoading();}, 1000);




                }
            });


    });


    /*$('body').on('click', '.month', function (e) {
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }


        $.ajax({
            url: APP_BASE_URL + 'logbook/get_log_book_by_date_range_monthly',
            data: {
                'patient_id':6689,
                'date': $('.month_date_range').text(),
                'clicked':clicked,
            },
            type: "get",
            success : function (response) {

                if (response.success != undefined) {
                    $('.month_date_range').text(response.start + ' - ' + response.end);
                }

            }
        });


    });
*/











    /* js for switch */

    $(".month-switch").on ('click', function(){
        //alert('weeew');
        $(this).parents('.portlet').find('.portlet-body-for-date.month_for_date_change').removeClass('hidden');
        $(this).parents('.portlet').find('.portlet-body-for-date.week_for_date_change').addClass('hidden');
    });
    $(".week-switch").on ('click', function(){
        //alert('weeew');
        $(this).parents('.portlet').find('.portlet-body-for-date.week_for_date_change').removeClass('hidden');
        $(this).parents('.portlet').find('.portlet-body-for-date.month_for_date_change').addClass('hidden');
    });


    /*    */


    $('#fit_bit_data').on('change', function () {
          var fitbit_data_id  =  $(this).val();
         var patient_id = $("#patient_id").val();
            toastr['info']('<div style="margin-top: 5px;">Do you want to remove data from log book ?</div><div><button type="button" class="btn btn-default pull-right cancel "  style="margin-top: 10px;">Cancel</button><button style="margin-left: 5px;margin-top:10px;" type="button" id="yes_remove" class="btn btn-danger">Remove</button></div>', 'Remove Data From Logbook?',
                {
                    closeButton: true,
                    preventDuplicates: true,
                    "positionClass": "toast-top-center",
                    "showDuration": "10000",
                    "timeOut": "15000",
                    "extendedTimeOut": "10000",
                    "preventDuplicates": true,
                    allowHtml: true,
                    onShown: function (toast) {

                        $("#yes_remove").click(function () {

                            $.ajax({
                                url: APP_BASE_URL + 'logbook/get_log_fitbit_data',
                                data: {
                                    'patient_id':patient_id,
                                    'fitbit_data_id': fitbit_data_id,

                                },
                                type: "get",
                                success : function (response) {

                                    if (response.success != undefined) {

                                        //toastr["success"]("Data Removed Sucessfully", "Removed");
                                        window.location.reload();
                                    }

                                }
                            });

                        });

                        $(".cancel").click(function () {
                            $("#fit_bit_data").find('option:eq(0)').prop('selected', true);
                        });




                    }
                });

    });



});
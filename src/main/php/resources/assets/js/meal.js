
/* js for window load*/
$(window).load(function() {
        $('.btn-bg').each(function () {
            if ($(this).width() != $(this).height()) {
                $(this).height($(this).width());
            }
        });
        $('.btn-bg img').each(function () {
            if ($(this).width() < $(this).height()) {
                $(this).height($(this).width());
                $(this).css('width', 'auto');
            }
        });
    });


    $(document).ready(function() {

    $("input[name=options]:radio").change(function () {
        var option=$(this).val();
        var patient_id=$('#patient_id').val();
        if(option==1){
            $.ajax({
                url: APP_BASE_URL+'pastmeal/past_meal_by_week',
                data: {
                    patient_id: patient_id,
                },
                type: "get",
                success : function (response) {
                    $('#all-meal-log').empty();
                    $('#all-meal-log').html(response);
                }

            });
        }else{
            $.ajax({
                url: APP_BASE_URL+'pastmeal/past_meal_by_month',
                data: {
                    patient_id: patient_id,
                },
                type: "get",
                success : function (response) {
                   $('#all-meal-log').empty();
                   $('#all-meal-log').html(response);
                }

            });
        }

    });


    $("#ddFilterBy").change(function () {
        var meal_type=$('#ddFilterBy').val();
        var patient_id=$('#patient_id').val();
        if($('.week').hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }
        $('#loding_icon').trigger('click');

            $.ajax({
                url: APP_BASE_URL+'pastmeal/past_meal_by_filter',
                data: {
                    patient_id: patient_id, meal_type: meal_type,clicked:clicked,date: $('.week_date_range').text(),
                },
                type: "get",
                success : function (response) {
                    if ( response.length != 0 ){
                        $('#all-meal-log').empty();
                        $('#all-meal-log').html(response);
                        $('.btn-bg').each(function () {
                            if ($(this).width() != $(this).height()) {
                                $(this).height($(this).width());
                            }
                        });
                        $('.btn-bg img').each(function () {
                            if ($(this).width() < $(this).height()) {
                                $(this).height($(this).width());
                                $(this).css('width', 'auto');
                            }
                        });
                    }else{
                        $('#all-meal-log').empty();
                        $('#all-meal-log').html('<div class="alert alert-warning margiv-top-10"> <center><span>No data found</span></center></div>');

                    }
                    window.setTimeout(function () {App.stopPageLoading();}, 1000);

                }

            });


    });


        $('body').on('click', '.week', function (e) {

            var meal_type  = $('#ddFilterBy').val();
            var patient_id = $('#patient_id').val();
            if($(this).hasClass('left'))
            {
                var clicked = 'left';
            }
            else
            {
                var clicked = 'right';
            }
            $('#loding_icon').trigger('click');

            $.ajax({
                url: APP_BASE_URL + 'pastmeal/get_past_meal_by_date_range_weekly',
                data: {
                    'patient_id':patient_id,
                    'date': $('.week_date_range').text(),
                    'clicked':clicked,
                     'meal_type':meal_type,
                },
                type: "get",
                success : function (response) {
                    $('.week_date_range').text(response.start + ' - ' + response.end);
                    if ( response.success.length != 0 ){
                        $('#all-meal-log').empty();
                        $('#all-meal-log').html(response.success);
                        $('.btn-bg').each(function () {
                            if ($(this).width() != $(this).height()) {
                                $(this).height($(this).width());
                            }
                        });
                        $('.btn-bg img').each(function () {
                            if ($(this).width() < $(this).height()) {
                                $(this).height($(this).width());
                                $(this).css('width', 'auto');
                            }
                        });
                    }else{
                        $('#all-meal-log').empty();
                        $('#all-meal-log').html('<div class="alert alert-warning margiv-top-10"> <center><span>No data found</span></center></div>');

                    }
                    window.setTimeout(function () {App.stopPageLoading();}, 1000);
                }
            });


        });

    $('body').on('click','.delete_meal', function (event) {
        // $(this).parents('.overlay').addClass('pointerevent');
        event.stopImmediatePropagation();
        toastr.clear();
        var log_id= $(this).attr('data-log-id');
        toastr['info']('<div style="margin-top: 5px;">Are you sure to Delete this meal?</div><div><button type="button" class="btn btn-default pull-right" style="margin-top: 10px;">Cancel</button><button style="margin-left: 5px;margin-top: 10px;" type="button" id="yes_remove" class="btn btn-danger">Remove</button></div>', 'Remove meal From past meal?',
            {
                closeButton: true,
                preventDuplicates: true,
                "positionClass": "toast-top-center",
                "showDuration": "10000",
                "timeOut": "15000",
                "extendedTimeOut": "10000",
                "preventDuplicates": true,
                allowHtml: true,
                onShown: function (toast) {
                    $("#yes_remove").click(function () {
                        $.ajax({
                            url: APP_BASE_URL+'pastmeal/remove_past_meal',
                            data: {
                                log_id:log_id,
                            },
                            type: "get",
                            success : function (response) {
                                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                                if (response.success != undefined) {
                                    $('#meal_'+log_id).remove();
                                    toastr["success"](response.success, "Meal Deleted Successfully");
                                }else if (response.error != undefined) {
                                    toastr["error"](response.error, "Error in meal delete");
                                }

                            }

                        });
                    });
                }
            });
        return false;

    });





});



$(document).ready(function() {
    //make the height of two widget boxes equal
    var boxHeight = $('.box.purple .portlet-body').height();
    $('.box.green-jungle .portlet-body').css({'height': boxHeight+22 });

    $("#modal_addsession").on('shown.bs.modal', function () {
        $(".ddCoachesList").select2({
            width: null,
        });
        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });
        $(".form_datetime").datetimepicker({
            autoclose: true,
            isRTL: App.isRTL(),
            format: "dd MM yyyy - hh:ii",
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
        });
    });

    $("#modal_memberscheduling").on('shown.bs.modal', function () {
        $(".ddscheduledays").select2({
            width: null,
        });
        $(".ddscheduletime").select2({
            width: null,
        });
        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });
    });
    $("#modal_add2donotes").on('shown.bs.modal', function () {
        $(".ddNotifyCoaches").select2({
            width: null,
        });
        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });
    });

    $(".daily-switch").on ('click', function(){
        //alert('weeew');
        $(this).parents('.portlet').find('.portlet-body.daily').removeClass('hidden');
        $(this).parents('.portlet').find('.portlet-body.weekly').addClass('hidden');
    });
    $(".weekly-switch").on ('click', function(){
        //alert('weeew');
        $(this).parents('.portlet').find('.portlet-body.weekly').removeClass('hidden');
        $(this).parents('.portlet').find('.portlet-body.daily').addClass('hidden');
    });

    $('.profile-userpic').each(function(){
        if ($(this).width() != $(this).height()){
            $(this).height($(this).width());
            if  ($(this).children('img').width() > $(this).children('img').height()){
                $(this).children('img').css('height','100%');
                $(this).children('img').css('width','auto');
            }}
    });


    $('#weight_summary_weekly').highcharts({
        chart: {
            type: 'line',
            events: {
                load: function(event) {
                    $('#portlet-weekly-weight').addClass('hidden');
                }
            }
        },
        title: {
            text: ''
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: weekly_day_name,
            title: {
                text: 'Weeks'
            }

        },
        yAxis: {
            title: {
                text: 'Pounds'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' lbs'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'Weight',
            connectNulls: true,//connect nulls
            data: weekly_weight_data
        }]
    });


    $('body').on('click', '.weekly_weight_daily', function (e) {
        var _this = $(this);
        var patient_id = $('#patient_id').val();
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }

        if(patient_id)
        {
            $('#loding_icon').trigger('click');
            $.ajax({
                url: APP_BASE_URL + 'weekly-weight-summary-chart-data',
                data: {
                    'patient_id':patient_id,
                    'date': $('.weekly_weight_daily_date').text(),
                    'clicked':clicked,
                },
                type: "get",
                success : function (response) {
                    window.setTimeout(function() { App.stopPageLoading();}, 1000);
                    if (response.success != undefined) {
                        $('.weekly_weight_daily_date').text(response.start + ' - ' + response.end);

                        $('.weekly_actual_weight').text(response.success.weekly_actual_weight);
                        $('.weekly_average_weight').text(response.success.weekly_average_weight);
                        $('.weekly_lowest_weight').text(response.success.weekly_lowest_weight);
                        $('.weekly_highest_weight').text(response.success.weekly_highest_weight);

                        $('#weight_summary_weekly').highcharts({
                            chart: {
                                type: 'line',
                                events: {
                                    load: function(event) {
                                      //  $('#portlet-weekly-weight').addClass('hidden');
                                    }
                                }
                            },
                            title: {
                                text: ''
                            },
                            legend: {
                                enabled: false
                            },
                            xAxis: {
                                categories: response.success.weekly_day_name,
                                title: {
                                    text: 'Weeks'
                                }

                            },
                            yAxis: {
                                title: {
                                    text: 'Pounds'
                                }
                            },
                            tooltip: {
                                shared: true,
                                valueSuffix: ' lbs'
                            },
                            credits: {
                                enabled: false
                            },
                            plotOptions: {
                                areaspline: {
                                    fillOpacity: 0.5
                                }
                            },
                            series: [{
                                name: 'Weight',
                                connectNulls: true,//connect nulls
                                data: response.success.weekly_weight_data
                            }]
                        });
                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, response.title);
                    }
                }
            });
        }
    });



    // LINE CHART 1
    $('#weight_summary_daily').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: week_day_name,
            title: {
                text: 'Day of Week'
            }

        },
        yAxis: {
            title: {
                text: 'Pounds'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' lbs'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'Weight ',
            connectNulls: true,//connect nulls
            data: patient_weight_daily
        }]
    });

    $('body').on('click', '.patient_weight_daily', function (e) {
        var _this = $(this);
        var patient_id = $('#patient_id').val();
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }

        if(patient_id)
        {
            $('#loding_icon').trigger('click');
            $.ajax({
                url: APP_BASE_URL + 'patient-weight-summary-chart-data',
                data: {
                    'patient_id':patient_id,
                    'date': $('.weight_daily_date').text(),
                    'clicked':clicked,
                },
                type: "get",
                success : function (response) {
                    window.setTimeout(function() { App.stopPageLoading();}, 1000);
                    if (response.success != undefined) {
                        $('.weight_daily_date').text(response.start + ' - ' + response.end);

                        $('.actual_weight').text(response.success.actual_weight);
                        $('.average_weight').text(response.success.average_weight);
                        $('.lowest_weight').text(response.success.lowest_weight);
                        $('.highest_weight').text(response.success.highest_weight);

                        // LINE CHART 1
                        $('#weight_summary_daily').highcharts({
                            chart: {
                                type: 'line'
                            },
                            title: {
                                text: ''
                            },
                            legend: {
                                enabled: false
                            },
                            xAxis: {
                                categories: week_day_name,
                                title: {
                                    text: 'Day of Week'
                                }

                            },
                            yAxis: {
                                title: {
                                    text: 'Pounds'
                                }
                            },
                            tooltip: {
                                shared: true,
                                valueSuffix: ' lbs'
                            },
                            credits: {
                                enabled: false
                            },
                            plotOptions: {
                                areaspline: {
                                    fillOpacity: 0.5
                                }
                            },
                            series: [{
                                name: 'Weight ',
                                connectNulls: true,//connect nulls
                                data: response.success.daily_weight_data
                            }]
                        });
                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, response.title);
                    }
                }
            });
        }
    });


    // LINE CHART 2

    $('#highchart_2-w').highcharts({
        chart: {
            type: 'line',
            spacingBottom: 30,
            events: {
                load: function(event) {
                    $('#portlet-weekly-engagement').addClass('hidden');
                }
            }
        },
        legend: {
            enabled: false
        },
        title: {
            text: ''
        },

        xAxis: {
            categories: weekly_day_name,
            title: {
                text: 'Weeks'
            }
        },
        yAxis: {
            title: {
                text: 'Engagement Score'
            }
        },

        plotOptions: {
            area: {
                fillOpacity: 0.5
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Engagement Score',
            data: engagement_detail_weekly
        }]
    });

    $('#highchart_2').highcharts({
        chart: {
            type: 'line',
            spacingBottom: 30
        },
        legend: {
            enabled: false
        },
        title: {
            text: ''
        },

        xAxis: {
            categories: week_day_name,
            title: {
                text: 'Day of Week'
            }
        },
        yAxis: {
            title: {
                text: 'Engagement Score'
            }
        },

        plotOptions: {
            area: {
                fillOpacity: 0.5
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Engagement Score',
            data: engagement_detail_daily
        }]
    });

    $('body').on('click', '.engagement_detail_daily', function (e) {
        var _this = $(this);
        var patient_id = $('#uuid').val();
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }

        if(patient_id)
        {
            $('#loding_icon').trigger('click');
            $.ajax({
                url: APP_BASE_URL + 'daily-engagement-data',
                data: {
                    'patient_id':patient_id,
                    'date': $('.engagement_daily').text(),
                    'clicked':clicked,
                },
                type: "get",
                success : function (response) {
                    window.setTimeout(function() { App.stopPageLoading();}, 1000);
                    if (response.success != undefined) {
                        $('.engagement_daily').text(response.start + ' - ' + response.end);
                        $('#highchart_2').highcharts({
                            chart: {
                                type: 'line',
                                spacingBottom: 30
                            },
                            legend: {
                                enabled: false
                            },
                            title: {
                                text: ''
                            },

                            xAxis: {
                                categories: week_day_name,
                                title: {
                                    text: 'Day of Week'
                                }
                            },
                            yAxis: {
                                title: {
                                    text: 'Engagement Score'
                                }
                            },

                            plotOptions: {
                                area: {
                                    fillOpacity: 0.5
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [{
                                name: 'Engagement Score',
                                data: response.success,
                            }]
                        });
                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, response.title);
                    }
                }
            });
        }
    });

    $('body').on('click', '.engagement_detail_weekly', function (e) {
        var _this = $(this);
        var patient_id = $('#uuid').val();
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }

        if(patient_id)
        {
            $('#loding_icon').trigger('click');
            $.ajax({
                url: APP_BASE_URL + 'weekly-engagement-data',
                data: {
                    'patient_id':patient_id,
                    'date': $('.engagement_weekly').text(),
                    'clicked':clicked,
                },
                type: "get",
                success : function (response) {
                    window.setTimeout(function() { App.stopPageLoading();}, 1000);
                    if (response.success != undefined) {
                        $('.engagement_weekly').text(response.start + ' - ' + response.end);
                        $('#highchart_2').highcharts({
                            chart: {
                                type: 'line',
                                spacingBottom: 30
                            },
                            legend: {
                                enabled: false
                            },
                            title: {
                                text: ''
                            },

                            xAxis: {
                                categories: week_day_name,
                                title: {
                                    text: 'Day of Week'
                                }
                            },
                            yAxis: {
                                title: {
                                    text: 'Engagement Score'
                                }
                            },

                            plotOptions: {
                                area: {
                                    fillOpacity: 0.5
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [{
                                name: 'Engagement Score',
                                data: response.success,
                            }]
                        });
                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, response.title);
                    }
                }
            });
        }
    });

    $('body').on('click', '.physical_activity_weekly_minute_icon', function (e) {
        var _this = $(this);
        var patient_id = $('#patient_id').val();
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }

        if(patient_id)
        {
            $('#loding_icon').trigger('click');
            $.ajax({
                url: APP_BASE_URL + 'physical-activity-weekly',
                data: {
                    'patient_id':patient_id,
                    'date': $('.physical_activity_weekly_minute').text(),
                    'clicked':clicked,
                },
                type: "get",
                success : function (response) {
                    window.setTimeout(function() { App.stopPageLoading();}, 1000);
                    if (response.success != undefined) {
                        $('.physical_activity_weekly_minute').text(response.start + ' - ' + response.end);
                        $('.physical_activity_steps_weekly_step').text(response.success.total_steps);
                        $('.physical_activity_steps_weekly_activity_logs').text(response.success.activity_log + response.minute.activity_log_daily);
                        $('.physical_activity_steps_weekly_time').text(response.minute.total_minute_weekly);

                        $('#highchart_3-w').highcharts({
                            chart: {
                                type: 'column',
                            },
                            title: {
                                text: ''
                            },
                            legend: {
                                enabled: false
                            },
                            xAxis: {
                                categories: response.success.week_day_name,
                                title: {
                                    text: 'Weeks'
                                }
                            },
                            yAxis: {
                                title: {
                                    text: 'Steps'
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [{
                                name: 'Steps',
                                data: response.success.physical_activity_steps_weekly
                            }]
                        });

                        $('#highchart_6-w').highcharts({
                            chart: {
                                type: 'line',

                            },
                            legend: {
                                enabled: false
                            },
                            title: {
                                text: ''
                            },
                            xAxis: {
                                categories: response.success.week_day_name,
                                title: {
                                    text: 'Weeks'
                                }
                            },
                            yAxis: {
                                title: {
                                    text: 'Minutes'
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [ {
                                name: 'Minutes',
                                data: response.minute.physical_activity_minute_weekly
                            }]
                        });
                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, response.title);
                    }
                }
            });
        }
    });

    // LINE CHART3
    $('#highchart_3-w').highcharts({
        chart: {
            type: 'column',
        },
        title: {
            text: ''
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: weekly_day_name,
            title: {
                text: 'Weeks'
            }
        },
        yAxis: {
            title: {
                text: 'Steps'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Steps',
            data: physical_activity_steps_weekly
        }]
    });


    $('body').on('click', '.physical_activity_steps_daily', function (e) {
        var _this = $(this);
        var patient_id = $('#patient_id').val();
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }

        if(patient_id)
        {
            $('#loding_icon').trigger('click');
            $.ajax({
                url: APP_BASE_URL + 'physical-activity-steps-daily',
                data: {
                    'patient_id':patient_id,
                    'date': $('.physical_activity_steps').text(),
                    'clicked':clicked,
                },
                type: "get",
                success : function (response) {
                    window.setTimeout(function() { App.stopPageLoading();}, 1000);
                    if (response.success != undefined) {
                        $('.physical_activity_steps').text(response.start + ' - ' + response.end);
                        $('.physical_activity_steps_step').text(response.success.total_steps);
                        $('.physical_activity_steps_activity_logs').text(response.success.activity_log + response.minute.activity_log_daily);
                        $('.physical_activity_steps_time').text(response.minute.total_minute_daily);

                        $('#highchart_3').highcharts({
                            chart: {
                                type: 'column'
                            },
                            legend: {
                                enabled: false
                            },
                            title: {
                                text: ''
                            },
                            xAxis: {
                                categories: week_day_name,
                                title: {
                                    text: 'Day of Week'
                                }
                            },
                            yAxis: {
                                title: {
                                    text: 'Steps'
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [{
                                name: 'Steps',
                                data: response.success.physical_activity_steps_daily
                            }]
                        });

                        $('#highchart_6').highcharts({
                            chart: {
                                type: 'line',
                                events: {
                                    load: function(event) {
                                    //    $('#portlet-weekly-activity').addClass('hidden');
                                    }
                                }
                            },
                            legend: {
                                enabled: false
                            },
                            title: {
                                text: ''
                            },
                            xAxis: {
                                categories: week_day_name,
                                title: {
                                    text: 'Day of Week'
                                }
                            },
                            yAxis: {
                                title: {
                                    text: 'Minutes'
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [ {
                                name: 'Minutes',
                                data: response.minute.physical_activity_minute_daily
                            }]
                        });
                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, response.title);
                    }
                }
            });
        }
    });

    //
    $('#highchart_3').highcharts({
        chart: {
            type: 'column'
        },
        legend: {
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: week_day_name,
            title: {
                text: 'Day of Week'
            }
        },
        yAxis: {
            title: {
                text: 'Steps'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Steps',
            data: physical_activity_steps_daily
        }]
    });


    ///linechart 6
    $('#highchart_6-w').highcharts({
        chart: {
            type: 'line',

        },
        legend: {
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: weekly_day_name,
            title: {
                text: 'Weeks'
            }
        },
        yAxis: {
            title: {
                text: 'Minutes'
            }
        },
        credits: {
            enabled: false
        },
        series: [ {
            name: 'Minutes',
            data: physical_activity_minute_weekly
        }]
    });

    $('#highchart_6').highcharts({
        chart: {
            type: 'line',
            events: {
                load: function(event) {
                    $('#portlet-weekly-activity').addClass('hidden');
                }
            }
        },
        legend: {
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: week_day_name,
            title: {
                text: 'Day of Week'
            }
        },
        yAxis: {
            title: {
                text: 'Minutes'
            }
        },
        credits: {
            enabled: false
        },
        series: [ {
            name: 'Minutes',
            data: physical_activity_minute_daily
        }]
    });


    // LINE CHART 4
    $('#highchart_4-w').highcharts({
        chart: {
            type: 'line',
            events: {
                load: function(event) {
                    $('#portlet-weekly-logedmeal').addClass('hidden');
                }
            }
        },
        title: {
            text: ''
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: weekly_day_name,
            title: {
                text: 'Weeks'
            }

        },
        yAxis: {
            title: {
                text: 'Meals Logged'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' logged'
        },
        credits: {
            enabled: false
        },
        plotOptions: {

        },
        series: [{
            name: 'Meals',
            data: weekly_meal_logged
        }]
    });

    $('body').on('click', '.weekly_patient_meal_logged', function (e) {
        var _this = $(this);
        var patient_id = $('#patient_id').val();
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }

        if(patient_id)
        {
            $('#loding_icon').trigger('click');
            $.ajax({
                url: APP_BASE_URL + 'weekly-meals-logged-chart-data',
                data: {
                    'patient_id':patient_id,
                    'date': $('.weekly_meal_logged').text(),
                    'clicked':clicked,
                },
                type: "get",
                success : function (response) {
                    window.setTimeout(function() { App.stopPageLoading();}, 1000);
                    if (response.success != undefined) {
                        $('.weekly_meal_logged').text(response.start + ' - ' + response.end);
                        $('#highchart_4-w').highcharts({
                            chart: {
                                type: 'line',
                                events: {
                                    load: function(event) {
                                       // $('#portlet-weekly-logedmeal').addClass('hidden');
                                    }
                                }
                            },
                            title: {
                                text: ''
                            },
                            legend: {
                                enabled: false
                            },
                            xAxis: {
                                categories: response.success.weekly_day_name,
                                title: {
                                    text: 'Weeks'
                                }

                            },
                            yAxis: {
                                title: {
                                    text: 'Meals Logged'
                                }
                            },
                            tooltip: {
                                shared: true,
                                valueSuffix: ' logged'
                            },
                            credits: {
                                enabled: false
                            },
                            plotOptions: {

                            },
                            series: [{
                                name: 'Meals',
                                data: response.success.daily_meal_logged_data
                            }]
                        });
                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, response.title);
                    }
                }
            });
        }
    });

    $('#highchart_4').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        legend: {
            enabled: false
        },
        xAxis: {
            categories: week_day_name,
            title: {
                text: 'Day of Week'
            }
        },
        yAxis: {
            title: {
                text: 'Meals Logged'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' logged'
        },
        credits: {
            enabled: false
        },
        plotOptions: {

        },
        series: [{
            name: 'Meals',
            data: meal_logged_daily
        }]
    });


    $('body').on('click', '.patient_meal_logged_daily', function (e) {
        var _this = $(this);
        var patient_id = $('#patient_id').val();
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }

        if(patient_id)
        {
            $('#loding_icon').trigger('click');
            $.ajax({
                url: APP_BASE_URL + 'meals-logged-chart-data',
                data: {
                    'patient_id':patient_id,
                    'date': $('.meal_logged_daily').text(),
                    'clicked':clicked,
                },
                type: "get",
                success : function (response) {
                    window.setTimeout(function() { App.stopPageLoading();}, 1000);
                    if (response.success != undefined) {
                        $('.meal_logged_daily').text(response.start + ' - ' + response.end);
                        $('#highchart_4').highcharts({
                            chart: {
                                type: 'line'
                            },
                            title: {
                                text: ''
                            },
                            legend: {
                                enabled: false
                            },
                            xAxis: {
                                categories: week_day_name,
                                title: {
                                    text: 'Day of Week'
                                }
                            },
                            yAxis: {
                                title: {
                                    text: 'Meals Logged'
                                }
                            },
                            tooltip: {
                                shared: true,
                                valueSuffix: ' logged'
                            },
                            credits: {
                                enabled: false
                            },
                            plotOptions: {

                            },
                            series: [{
                                name: 'Meals',
                                data: response.success.daily_meal_logged_data
                            }]
                        });
                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, response.title);
                    }
                }
            });
        }
    });

    // patient_meal_logged_daily
    $('#highchart_5-w').highcharts({
        chart: {
            type: 'column',
            events: {
                load: function(event) {
                    $('#portlet-weekly-mealsource').addClass('hidden');
                }
            }
        },
        legend: {
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [
                'Homemade',
                'Store Bought',
                'Dine Out',
            ],
            title: {
                text: 'Meal Sources'
            }
        },
        yAxis: {
            title: {
                text: 'No. of Meals'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Actual',
            data: meal_source_weekly_actual
        },
            {
                name: 'Plan',
                data: meal_source_weekly_plan
            }
        ]
    });

    $('body').on('click', '.weekly_patient_meal_source', function (e) {
        var _this = $(this);
        var patient_id = $('#patient_id').val();
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }

        if(patient_id)
        {
            $('#loding_icon').trigger('click');
            $.ajax({
                url: APP_BASE_URL + 'weekly-meal-source-chart-data',
                data: {
                    'patient_id':patient_id,
                    'date': $('.weekly_meal_source').text(),
                    'clicked':clicked,
                },
                type: "get",
                success : function (response) {
                    window.setTimeout(function() { App.stopPageLoading();}, 1000);
                    if (response.success != undefined) {
                        $('.weekly_meal_source').text(response.start + ' - ' + response.end);
                        $('#highchart_5-w').highcharts({
                            chart: {
                                type: 'column',
                                events: {
                                    load: function(event) {
                                      //  $('#portlet-weekly-mealsource').addClass('hidden');
                                    }
                                }
                            },
                            legend: {
                                enabled: false
                            },
                            title: {
                                text: ''
                            },
                            xAxis: {
                                categories: [
                                    'Homemade',
                                    'Store Bought',
                                    'Dine Out',
                                ],
                                title: {
                                    text: 'Meal Sources'
                                }
                            },
                            yAxis: {
                                title: {
                                    text: 'No. of Meals'
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [{
                                name: 'Actual',
                                data: response.success.weekly_meal_source_actual
                            },
                                {
                                    name: 'Plan',
                                    data: response.success.weekly_meal_source_plan
                                }
                            ]
                        });
                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, response.title);
                    }
                }
            });
        }
    });

    $('#highchart_5').highcharts({
        chart: {
            type: 'column'
        },
        legend: {
            enabled: false
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: [
                'Homemade',
                'Store Bought',
                'Dine Out',
            ],
            title: {
                text: 'Meal Sources'
            }
        },
        yAxis: {
            title: {
                text: 'No. of Meals'
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Actual',
            data: meal_source_daily_actual
        },
            {
                name: 'Plan',
                data: meal_source_daily_plan
            }

        ]
    });

    $('body').on('click', '.patient_meal_source_daily', function (e) {
        var _this = $(this);
        var patient_id = $('#patient_id').val();
        if($(this).hasClass('left'))
        {
            var clicked = 'left';
        }
        else
        {
            var clicked = 'right';
        }

        if(patient_id)
        {
            $('#loding_icon').trigger('click');
            $.ajax({
                url: APP_BASE_URL + 'meal-source-chart-data',
                data: {
                    'patient_id':patient_id,
                    'date': $('.meal_source_daily').text(),
                    'clicked':clicked,
                },
                type: "get",
                success : function (response) {
                    window.setTimeout(function() { App.stopPageLoading();}, 1000);
                    if (response.success != undefined) {
                        $('.meal_source_daily').text(response.start + ' - ' + response.end);
                        $('#highchart_5').highcharts({
                            chart: {
                                type: 'column'
                            },
                            legend: {
                                enabled: false
                            },
                            title: {
                                text: ''
                            },
                            xAxis: {
                                categories: [
                                    'Homemade',
                                    'Store Bought',
                                    'Dine Out',
                                ],
                                title: {
                                    text: 'Meal Sources'
                                }
                            },
                            yAxis: {
                                title: {
                                    text: 'No. of Meals'
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [{
                                name: 'Actual',
                                data: response.success.daily_meal_source_actual
                            },
                                {
                                    name: 'Plan',
                                    data: response.success.daily_meal_source_plan
                                }

                            ]
                        });
                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, response.title);
                    }
                }
            });
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    //initiate select2 om modal pop up  direct message coach
    $("#direct-msg-coach").on('shown.bs.modal', function () {


        $(".saved-messages").select2({
            width: null,
        });

        $(".canned-messages").select2({
            width: null,
        });

        $(".recom-messages").select2({
            width: null,
        });
        $("#filter-messages").select2({
            width: null,
        });
        // $(".search-messages").select2({
        //     width: null,
        // });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        //change recommended msg to send
        $('.saved-messages').on ('change', function(){
            var recentmsg = $(".saved-messages option:selected"). text();
            if(recentmsg === "Select Saved Message"){
                $(".recomend-text").val('');
            }
            else{
                $(".recomend-text").val(recentmsg);
            }
        });
        $('.canned-messages').on ('change', function(){
            var recentmsg = $(".canned-messages option:selected"). text();
            if(recentmsg === "Select Canned Message"){
                $(".recomend-text").val('');
            }
            else{
                $(".recomend-text").val(recentmsg);
            }
        });

        $('.recom-messages').on ('change', function(){
            var recentmsg = $(".recom-messages option:selected"). text();
            if(recentmsg === "Select Recommended Message"){
                $(".recomend-text").val('');
            }
            else{
                $(".recomend-text").val(recentmsg);
            }
        });
        //scroll chet div to bottom
        var container = $("#direct-message-scroll");
        container.slimScroll({
            scrollTo: container[0].scrollHeight
        });

        //mark as read sho whide
        $('.markread').on ('click', function(){
            bootbox.confirm("You have not replied to the patient, Are you sure you want to mark the messages as read?", function(result) {

                // alert("Confirm result: "+result);
            });
            $(this).addClass('hide');
        });
        $(".recomend-text").on ('focus', function(){
            $('.markread').addClass('hide');
        });
        //create new
        $(" .btn-new-msg").on('click', function(){
            $(".recomend-text").val('');
        })


    });

    ///initiate coach direct msg on page...


    $(".saved-messages").select2({
        width: null,
    });

    $(".canned-messages").select2({
        width: null,
    });

    $(".recom-messages").select2({
        width: null,
    });
    $("#filter-messages").select2({
        width: null,
    });
    // $(".search-messages").select2({
    //     width: null,
    // });

    $("button[data-select2-open]").click(function() {
        $("#" + $(this).data("select2-open")).select2("open");
    });

    //change recommended msg to send
    $('.saved-messages').on ('change', function(){
        var recentmsg = $(".saved-messages option:selected"). text();
        if(recentmsg === "Select Saved Message"){
            $(".recomend-text").val('');
        }
        else{
            $(".recomend-text").val(recentmsg);
        }
    });
    $('.canned-messages').on ('change', function(){
        var recentmsg = $(".canned-messages option:selected"). text();
        if(recentmsg === "Select Canned Message"){
            $(".recomend-text").val('');
        }
        else{
            $(".recomend-text").val(recentmsg);
        }
    });

    $('.recom-messages').on ('change', function(){
        var recentmsg = $(".recom-messages option:selected"). text();
        if(recentmsg === "Select Recommended Message"){
            $(".recomend-text").val('');
        }
        else{
            $(".recomend-text").val(recentmsg);
        }
    });
    //scroll chet div to bottom
    var container = $("#direct-message-scroll");
    container.slimScroll({
        scrollTo: container[0].scrollHeight
    });

    //mark as read sho whide
    $('.markread').on ('click', function(){
        bootbox.confirm("You have not replied to the patient, Are you sure you want to mark the messages as read?", function(result) {

            // alert("Confirm result: "+result);
        });
        $(this).addClass('hide');
    });
    $(".recomend-text").on ('focus', function(){
        $('.markread').addClass('hide');
    });
    //create new
    $(" .btn-new-msg").on('click', function(){
        $(".recomend-text").val('');
    })

});

/*------------------dashboard setting page------------------------------------------*/
$( document ).ready(function() {

    $('[data-toggle="tooltip"]').tooltip();

    $('.date-picker').datepicker({   });
    $(".form_datetime").timepicker({
        autoclose: true,
        showSeconds: true,
        minuteStep: 1
    });

    $("#ddDietaryRestrictions").select2({
        width: null,
    });
    $("#ddMealPreparer").select2({
        width: null,
    });
    $("#ddDiabetesSupport").select2({
        width: null,
    });
    $("#habits").select2({
        width: null,
    });


});


function formatOption (option)
{
    var $option = $(
        '<div><strong>' + option.text + '</strong></div><div>' + option.title + '</div>'
    );
    return $option;
}




/* file attachment code for post start here */
$("#post_attachment").click(function(){
    $("#uploadpostfile").click();
});

/* file attachment code for post start here */
$("#user_profile_image_attachment").click(function(){
    $("#upload_user_image").click();
});

$('#uploadpostfile').on('change', function() {
    var filename = this.value;
    var lastIndex = filename.lastIndexOf("\\");
    if (lastIndex >= 0) {
        filename = filename.substring(lastIndex + 1);
    }
    var files = $('#uploadpostfile')[0].files;
    for (var i = 0; i < files.length; i++) {
        $('#file_name').val(files[i].name );
    }
    readURL(this);
});

$('#upload_user_image').on('change', function() {
    var filename = this.value;
    var lastIndex = filename.lastIndexOf("\\");
    if (lastIndex >= 0) {
        filename = filename.substring(lastIndex + 1);
    }
    var files = $('#upload_user_image')[0].files;
    for (var i = 0; i < files.length; i++) {
        $('#user_profile_file_name').val(files[i].name );
    }
    read_user_profile_image(this);
});

function read_user_profile_image(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.user_profile_image').show();
            $('.user_profile_image').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.img-box').show();
            $('.motivation_image').show();
            $('.motivation_image').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

// Older "accept" file extension method. Old docs: http://docs.jquery.com/Plugins/Validation/Methods/accept
$.validator.addMethod( "extension1", function( value, element, param ) {
    param = typeof param === "string" ? param.replace( /,/g, "|" ) : "png|jpe?g|gif";
    return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
}, $.validator.format( "Only image files uploading supported" ) );

$("#add_motivation_image_text").validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block help-block-error', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: ".ignore",  // validate all fields including form hidden input
    messages: {
    },
    rules: {
        post_description: {
            required: {
                depends: function () {
                    $(this).val($.trim($(this).val()));
                    return true;
                }
            },
            maxlength: 200,
        },
        uploadpostfile:{
            extension1: "jpg|jpeg|png"
        }

    },

    errorPlacement: function (error, element) { // render error placement for each input type
        var cont = $(element).parent('.input-group');
        var cont2 = $(element).parent('.form-group');
        if (cont) {
            cont.after(error);
        }
        if (cont2) {
            $(element).after(error);
        }
        if (cont&&cont2){
            cont.after(error);
        }
    },
    highlight: function (element) { // hightlight error inputs

        $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
    },
    unhighlight: function (element) { // revert the change done by hightlight
        $(element)
            .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },
    success: function (label) {
        label
            .closest('.form-group').removeClass('has-error'); // set success class to the control group
    },
    submitHandler: function (form) {
        var _form = $('#add_motivation_image_text');
        $("#add_motivation_image_text .add").attr('disabled', 'disabled');

        var formData = new FormData($('#add_motivation_image_text')[0]);
        $('#hs-loader').trigger('click');
        $.ajax({
            url: _form.attr('action'),
            data: formData,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    $('#modal_addmotivation').modal('hide');
                    toastr["success"](response.success, response.title);
                    window.location.reload();
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, response.title);
                }
            },

        });
    }
});


$("#add_user_profile_image").validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block help-block-error', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: ".ignore",  // validate all fields including form hidden input
    messages: {
    },
    rules: {
        upload_user_image:{
            extension1: "jpg|jpeg|png"
        }
    },

    errorPlacement: function (error, element) { // render error placement for each input type
        var cont = $(element).parent('.input-group');
        var cont2 = $(element).parent('.form-group');
        if (cont) {
            cont.after(error);
        }
        if (cont2) {
            $(element).after(error);
        }
        if (cont&&cont2){
            cont.after(error);
        }
    },
    highlight: function (element) { // hightlight error inputs

        $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
    },
    unhighlight: function (element) { // revert the change done by hightlight
        $(element)
            .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },
    success: function (label) {
        label
            .closest('.form-group').removeClass('has-error'); // set success class to the control group
    },
    submitHandler: function (form) {

        var _form = $('#add_user_profile_image');
        $("#add_user_profile_image .add").attr('disabled', 'disabled');

        var formData = new FormData($('#add_user_profile_image')[0]);
        $('#hs-loader').trigger('click');
        $.ajax({
            url: _form.attr('action'),
            data: formData,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    $('#modal_add_profile_image').modal('hide');
                    toastr["success"](response.success, response.title);
                    window.location.reload();
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, response.title);
                    $("#add_user_profile_image .add").removeAttr('disabled', 'disabled');
                }
            },

        });
    }
});




$("#modal_addnotes").on('show.bs.modal', function () {

    $('#add_new_coach_notes').get(0).reset();
    $("#add_new_coach_notes").validate().resetForm();

    $(".ddNotifyCoaches").select2({
        width: null,
        templateResult: formatOption
    });

    $("button[data-select2-open]").click(function() {
        $("#" + $(this).data("select2-open")).select2("open");
    });
});


$("#add_new_coach_notes").validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block help-block-error', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: ".ignore",  // validate all fields including form hidden input
    messages: {
    },
    rules: {
        add_coach_notes: {
            required: true,
            maxlength: 1500,
        },
        /*'ddNotifyCoaches[]': {
            required: true,
        }*/
    },

    errorPlacement: function (error, element) {
        var cont = $(element).parent('.input-group');
        var cont2 = $(element).parent('.form-group');
        if (cont) {
            cont.after(error);
        }
        if (cont2) {
            $(element).after(error);
        }
        if (cont&&cont2){
            cont.after(error);
        }
    },
    highlight: function (element) { // hightlight error inputs

        $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
    },
    unhighlight: function (element) { // revert the change done by hightlight
        $(element)
            .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },
    success: function (label) {
        label
            .closest('.form-group').removeClass('has-error'); // set success class to the control group
    },
    submitHandler: function (form) {

        var _form = $('#add_new_coach_notes');
        var id =  $('#patient_id').val();
        if(id){
            $("#add_new_coach_notes .add").attr('disabled', 'disabled');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    toastr["success"](response.success, response.title);
                    $('#modal_addnotes').modal('hide');
                    window.location.reload();
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, response.title);
                }
                $("#add_new_coach_notes .add").removeAttr('disabled', 'disabled');
            }, 'POST', true, '#hs-loader');
        }
    }
});




$("#modal_viewnotes").on('show.bs.modal', function () {

    var _form   =  $('#get_coach_notes');
    var id      =  $('#patient_id').val();
    if(id){
        $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
            window.setTimeout(function() { App.stopPageLoading();}, 1000);
            if (response.success != undefined) {
                $('.get_coach_notes').empty();
                $('.get_coach_notes').append(response.success);
            }
            else if (response.error != undefined) {
                toastr["error"](response.error, response.title);
            }
        }, 'GET', true, '#hs-loader');
    }
});


$("#modal_goals").on('show.bs.modal', function () {

    var _form   =  $('#get_all_goals_of_patient');
    var id      =  $('#patient_id').val();
    if(id){
        $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {

            window.setTimeout(function() { App.stopPageLoading();}, 1000);
            if (response.success != undefined) {
                $('.remove_all_goals').empty();
                $('.remove_all_goals').html(response.success);
            }
            else if (response.error != undefined) {
                toastr["error"](response.error, response.title);
            }
        }, 'GET', true, '#hs-loader');
    }
});







$(document).ready(function() {


    $('.btn-bg').each(function () {
        if ($(this).width() != $(this).height()) {
            $(this).height($(this).width());
        }
    });
    $('.btn-bg img').each(function () {
        if ($(this).width() < $(this).height()) {
            $(this).height($(this).width());
            $(this).css('width', 'auto');
        }
    });







// This function for text change of add notes
    $("#notify_type").change(function(){

        if($("#notify_type").val() == "Pre Call")
            $(".add_all_call_notes").text("Add Pre Call Notes");
        if($("#notify_type").val() == "Post Call")
            $(".add_all_call_notes").text("Add Post Call Notes");
        if($("#notify_type").val() == "Other")
            $(".add_all_call_notes").text("Add Other Notes");

    });

});




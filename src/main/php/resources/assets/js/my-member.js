/**
 * Created by rpatidar on 31/08/2017.
 */

jQuery(document).ready(function ($) {

    /*$('#member_list').dataTable({
     processing: false,
     serverSide: false,
     pageLength: 50,
     bLengthChange: false,
     order: [[1, 'desc']],
     deferRender: true,
     aoColumns: [
     { "bSortable": false },
     { "bSortable": true },
     { "bSortable": true },
     { "bSortable": true },
     { "bSortable": true },
     { "bSortable": true },
     { "bSortable": true }
     ],
     responsive: {
     details: {
     type: 'column',
     target: 'tr'
     }
     },
     columnDefs: [ {
     className: 'control',
     orderable: false,
     targets:   0
     },{ className: "dt-right", "targets": [1,4,5,6] } ],
     });*/


    /**
     * Show Coach Delete Modal Handler
     */

    ///


    $(".sort-messages").select2({
        width: null,
    });

    group_filter = $("#group_filter option:selected").val();
    another_filter = $("#another_filter option:selected").val();
    member_filter = JSON.parse( localStorage.getItem( 'member_filter_' + btoa(USER_ID) ));
    if (typeof member_filter !== 'undefined' && member_filter !== null && (typeof group_filter == 'undefined' || group_filter == '') )
    {
        if(member_filter.user_id == USER_ID)
        {
            if(member_filter.member_filter == 'member_group' && member_filter.group_filter != '' && member_filter.member_filter == 'member_without_group' && member_filter.another_filter != ''  )
            {
                $('#member_filter').val( member_filter.member_filter).trigger('change');
            }
            else
                $('#member_filter').val( member_filter.member_filter).trigger('change');


            if (typeof member_filter.group_filter !== 'undefined' && member_filter.group_filter !== null && member_filter.group_filter !== '' && group_filter == ''  )
            {
                $('.group_filter').show();
                $('#group_filter').val( member_filter.group_filter).trigger('change');
            }
            if (typeof member_filter.another_filter !== 'undefined' && member_filter.another_filter !== null && member_filter.another_filter !== '')
            {
                $('.another_filter').show();
                $('#another_filter').val( member_filter.another_filter).trigger('change');
            }

        }
    }
    else if(typeof group_filter !== 'undefined' && group_filter !== null && group_filter != '')
    {
        var newURL = location.href.substring(0, location.href.lastIndexOf("/") + 1);
        window.history.pushState('object', document.title, newURL);
        $('#member_filter').val( 'member_group' ).trigger('change');
        var member_filter = {};
        member_filter.user_id = USER_ID;
        member_filter.member_filter  = 'member_group';
        member_filter.group_filter   = $("#group_filter option:selected").val();
        localStorage.setItem( 'member_filter_' + btoa(USER_ID), JSON.stringify(member_filter) );
    }
    // else if(typeof member_filter !== 'undefined' && member_filter != null && member_filter.another_filter != null)
    // {
    //     // if(member_filter.member_filter == 'member_without_group' && member_filter.another_filter != '' )
    //     // {
    //     //     $('#member_filter').val( member_filter.member_filter).trigger('change');
    //     //     console.log(member_filter);
    //     // }
    //     // else {
    //     //     $('#member_filter').val(member_filter.member_filter).trigger('change');
    //     // }
    //     //     $('.group_filter').hide();
    //     //     $('.another_filter').show();
    //     //     $('#another_filter').val( member_filter.another_filter).trigger('change');
    //
    //
    // }



    $('#member_detail').on('show.bs.modal', function (e) {
        $('.member_data').empty();
        var _form = $('#patient_detail');
        _form.find('[name="id"]').val('');
        var id = $(e.relatedTarget).data('id');
        if (id) {
            if (!$(e.relatedTarget).hasClass('clicked')) {
                $(e.relatedTarget).addClass('clicked');
                _form.find('[name="id"]').val(id);
                $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                    window.setTimeout(function () {
                        App.stopPageLoading();
                    }, 1000);
                    if (response.success != undefined) {
                        $(response.success).appendTo('.member_data');
                        /*$.each(response.success, function (key, user) {
                         var member_data = "<li>" + user + "</li>";
                         $(member_data).appendTo('.member_data');
                         });*/
                        $(e.relatedTarget).removeClass('clicked');
                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, "Member Detail");
                    }
                }, _form.attr('method'), true, '#hs-loader');

            }
        }
    });


    /**
     * Member List Datatable Function
     */

    var membertab = $('#member_list').DataTable({

        stateSave: true,
        processing: false,
        serverSide: true,
        pageLength: 50,
        bLengthChange: false,
        "sPaginationType": "full_numbers",
        //"bStateSave": true, // presumably saves state for reloads
        // "pagingType": "full",
        //order: [[1, 'desc']],
        "language": {
            "emptyTable": "We did not find any members that match based on the filters selected above, please adjust the filters and try again",
            "loadingRecords": "Please wait - loading...",
            // processing: $('#loding_icon').trigger('click')
        },
        aoColumns: [
            {"bSortable": false},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": false},
            {"bSortable": true},
            {"bSortable": false}
        ],
        "preDrawCallback": function () {
            $('#loding_icon').trigger('click');
        },
        "initComplete": function () {
            var input = $('.dataTables_filter input');
            input.unbind();
            $searchButton = $('<button type="button" id="go_btn"class="btn btn-sm default">Go</button>').click(function () {
                $('#birth_date').val('');
                membertab.search(input.val()).draw();
            });


            input.bind('keyup', function(e) {
                if(e.keyCode == 13) {
                    membertab.search(input.val()).draw();
                }
            });
            $('#member_list_filter').append($searchButton);
        },
        "ajax": {
            "url": $('#member_list').data('action'),
            "type": "GET",
            "data": function (data) {
                data.group_filter  = $("#group_filter option:selected").val();
                data.member_filter = $("#member_filter option:selected").val();
                data.another_filter = $("#another_filter option:selected").val();
                data.birthdays_today = $("#birth_date").val();
                data._token = $("#token").val();

            }
        },
        "fnDrawCallback": function () {
            window.setTimeout(function () {
                App.stopPageLoading();
            }, 1000);
            $('div.danger').parent('td').addClass('danger');
            $('div.success').parent('td').addClass('success');
            $('div.warning').parent('td').addClass('warning');
            $('div.white').parent('td').addClass('white');
            $('div.active').parent('td').addClass('active');


        },
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [{
            className: 'control',
            orderable: false,
            targets: 0,
        }, {className: "dt-center", "targets": [1,4]}],
    });


    $('#member_filter').on('change', function () {
        $('#birth_date').val('');
        var member_filter = {};
        member_filter.user_id = USER_ID;
        if ($("#member_filter option:selected").val() == 'member_group') {
            $('.another_filter').hide();
            $('.group_filter').show();
            member_filter.member_filter  = $("#member_filter option:selected").val();
            member_filter.group_filter = '';

        }else if ($("#member_filter option:selected").val() == 'member_without_group') {
            $('.group_filter').hide();
            $('.another_filter').show();
            $('.another_filter').val('all');
            if ($("#another_filter").val() == 'all') {
                $('#loding_icon').trigger('click');
                membertab.ajax.reload();

            }
            member_filter.member_filter  = $("#member_filter option:selected").val();
            member_filter.another_filter = $("#another_filter option:selected").val();
        }
        else {
            $('.group_filter').hide();
            $('.another_filter').hide();
            $('#group_filter').val('').trigger('change');
            $('#another_filter').val('').trigger('change');
            $('#loding_icon').trigger('click');
            membertab.ajax.reload();
            member_filter.member_filter  = $("#member_filter option:selected").val();
            member_filter.group_filter = '';
            member_filter.another_filter = '';
        }
        localStorage.setItem( 'member_filter_' + btoa(USER_ID), JSON.stringify(member_filter) );
    });

    $('#group_filter').on('change', function () {
         $('#birth_date').val('');
        var member_filter = {};
        if ($("#group_filter option:selected").val() != '') {
            $('#loding_icon').trigger('click');
            membertab.ajax.reload();

            member_filter = JSON.parse( localStorage.getItem( 'member_filter_' + btoa(USER_ID) ));
            if (typeof member_filter !== 'undefined' && member_filter !== null)
            {
                member_filter.group_filter = $("#group_filter option:selected").val();
                localStorage.setItem( 'member_filter_' + btoa(USER_ID), JSON.stringify(member_filter) );
            }
            else
            {
                var member_filter = {};
                member_filter.user_id = USER_ID;
                member_filter.member_filter  = $("#member_filter option:selected").val();
                member_filter.group_filter   = $("#group_filter option:selected").val();
                localStorage.setItem( 'member_filter_' + btoa(USER_ID), JSON.stringify(member_filter) );
            }
        }

    });

    $('#another_filter').on('change', function () {
        $('#birth_date').val('');
        var member_filter = {};
        if ($("#another_filter option:selected").val() != '') {
            $('#loding_icon').trigger('click');
            membertab.ajax.reload();
            member_filter = JSON.parse( localStorage.getItem( 'member_filter_' + btoa(USER_ID) ));
            if (typeof member_filter !== 'undefined' && member_filter !== null)
            {
                member_filter.another_filter = $("#another_filter option:selected").val();
                localStorage.setItem( 'member_filter_' + btoa(USER_ID), JSON.stringify(member_filter) );
            }
            else
            {
                var member_filter = {};
                member_filter.user_id = USER_ID;
                member_filter.member_filter  = $("#member_filter option:selected").val();
                member_filter.another_filter   = $("#another_filter option:selected").val();
                localStorage.setItem( 'member_filter_' + btoa(USER_ID), JSON.stringify(member_filter) );
            }
        }

    });


    /**
     * On click assign Member button open model
     */
    $('#remove_member_from_group').on('click', function (e) {

        if ($('#group-list').val() != '') {
            toastr['info']('<div style="margin-top: 5px;">Are you sure to remove this Member from that Group?</div><div><button type="button" class="btn btn-default pull-right" style="margin-top: 10px;">Cancel</button><button style="margin-left: 5px;margin-top: 10px;" type="button" id="yes_remove" class="btn btn-danger">Remove</button></div>', 'Remove Member From Group?',
                {
                    closeButton: true,
                    preventDuplicates: true,
                    "positionClass": "toast-top-center",
                    "showDuration": "10000",
                    "timeOut": "15000",
                    "extendedTimeOut": "10000",
                    "preventDuplicates": true,
                    allowHtml: true,
                    onShown: function (toast) {
                        $("#yes_remove").click(function () {
                            var _this = $(this);
                            var _form = $('#assign_member_to_group');
                            $.HS.call_jquery_ajax(_form.attr('remove-from-group'), _form.serialize(), false, false, function (response) {
                                _this.removeClass('clicked');
                                window.setTimeout(function () {
                                    App.stopPageLoading();
                                }, 1000);
                                if (response.success != undefined) {
                                    toastr["success"](response.success, "Member Removed From Group");

                                    var uid = $("#assign_member_to_group").find("[name='uid']").val();
                                    if ($("td").find("[data-uid='" + uid + "']").hasClass('font-grey-salsa')) {
                                        $("td").find("[data-uid='" + uid + "']").attr('class', 'btn font-dark btn-sm btn-hide sbold');
                                    }
                                    else if ($("td").find("[data-uid='" + uid + "']").hasClass('font-dark')) {
                                        $("td").find("[data-uid='" + uid + "']").attr('class', 'btn font-grey-salsa btn-sm btn-hide sbold');
                                    }

                                    $('#modal_assignGroup').modal('hide');
                                }
                                else if (response.error != undefined) {
                                    toastr["error"](response.error, "Member Removed From Group");
                                }
                            }, 'POST', true, '#hs-loader');
                        });
                    }
                });
        }
    });


    $('body').on('click', '.hide_unhide', function (e) {
        var id = $(this).data('id');
        var value = $(this).data('value');
        var msg = "Unhide Member";
        if(value==1)
            msg = "Hide Member";
        if (id != '' && !$(this).hasClass('clicked')) {
            $(this).addClass('clicked');
            $('#loding_icon').trigger('click');
            $.ajax({
                type: 'POST',
                url: APP_BASE_URL + '/hide-unhide',
                data: 'user_id=' + id + '&_token=' + $('#token').val() + ',&value=' + value,
                success: function (data) {
                    if (data.success != undefined) {
                        toastr["success"](data.success, msg);
                        membertab.ajax.reload(null, false);
                    }
                    else if (data.error != undefined) {
                        window.setTimeout(function () {
                            App.stopPageLoading();
                        }, 1000);
                        toastr["error"](data.error, msg);
                    }
                    $(this).removeClass('clicked');
                },
            });
        }
    });


    $('body').on('click', '.pin_unpin', function (e) {
        var id = $(this).data('id');
        var value = $(this).data('value');
        var msg = "Unpin Member";
        if(value==1)
            msg = "Pin Member";
        if (id != '' && !$(this).hasClass('clicked')) {
            $(this).addClass('clicked');
            $('#loding_icon').trigger('click');
            $.ajax({
                type: 'POST',
                url: APP_BASE_URL + '/pin-unpin',
                data: 'user_id=' + id + '&_token=' + $('#token').val() + ',&value=' + value,
                success: function (data) {
                    if (data.success != undefined) {
                        toastr["success"](data.success, msg);
                        membertab.ajax.reload(null, false);
                    }
                    else if (data.error != undefined) {
                        window.setTimeout(function () {
                            App.stopPageLoading();
                        }, 1000);
                        toastr["error"](data.error, msg);
                    }
                    $(this).removeClass('clicked');
                },
            });
        }
    });

    //initiate select2 om modal pop up edit recommended message
    $("#modal_assignGroup").on('show.bs.modal', function (e) {

        $('#assign_member_to_group').get(0).reset();
        $("#assign_member_to_group").validate().resetForm();

        $('#assign_member_to_group .member_name').text('');
        $('#assign_member_to_group .group-name').text('');

        $('#group-list').val('').trigger('change');

        $(".group-list1").select2({
            width: null,
            allowClear: true
        });

        $(".group-list1").on("select2:select", function () {
            var data = $('.group-list1').select2('data');
            var datanew = data[0].text;
            if (datanew == 'Select') {
                $('.grp-selct-msg').text('You have removed member from the Group');
                $('#modal_assignGroup h4.group-name').text('');
            }
            else {
                $('.grp-selct-msg').text('');
                $('#modal_assignGroup h4.group-name').text(datanew);
            }
        })

        $(".btn-unset-grp").on('click', function () {

            $(".group-list1").val(null).trigger('change');
            $('.grp-selct-msg').text('You have removed member from the Group');
            $('#modal_assignGroup h4.group-name').text('');
        });

        $("button[data-select2-open]").click(function () {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        get_all_group_details(e, '');

    });


    function get_all_group_details(e, type) {
        var _form = $('#assign_member_to_group');
        if (type == '') {
            _form.find('[name="uid"]').val('');
            var uid = $(e.relatedTarget).data('uid');
            _form.find('[name="uid"]').val(uid);
        }
        $("#add_to_group_submit").attr('disabled', 'disabled');
        $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
            window.setTimeout(function () {
                App.stopPageLoading();
            }, 1000);
            $('#group-list').children('option:not(:first)').remove();
            if (response.group_list != undefined) {
                $.each(response.group_list, function (key, value) {
                    if (type == 'checked') {
                        if (value.is_full != 1)
                            $("#group-list").append('<option value="' + value.group_id + '">' + value.name + '</option>');
                    }
                    else
                        $("#group-list").append('<option value="' + value.group_id + '">' + value.name + '</option>');
                });
            }
            if (response.success != undefined) {

                $('#assign_member_to_group .member_name').text(response.success.full_name + ' ' + response.success.patient_id);

                if (response.success.group_name != null)
                    $('#assign_member_to_group .group-name').text(response.success.group_name);

                if (response.success.group_id != undefined) {
                    $('#group-list').val(response.success.group_id).trigger('change');
                    $('#remove_member_from_group').show();
                }
                else {
                    $('#remove_member_from_group').hide();
                }
            }
            else if (response.error != undefined) {
                toastr["error"](response.error, "Assign Member to Group");
            }
            $("#add_to_group_submit").removeAttr('disabled', 'disabled');
        }, 'GET', true, '#hs-loader');
    }


    $('#inlineCheckbox21').on('change', function (e) {

        if ($("#inlineCheckbox21").is(':checked'))
            get_all_group_details(e, 'checked');
        else
            get_all_group_details(e, 'unchecked');
    });

    /**
     * Member assign to Group page validation
     */
    $("#assign_member_to_group").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {},
        rules: {
            group_id: {
                required: true
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont && cont2) {
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {

            var selected_group_value = $('#group-list').val();

            var _this = $(this);
            var _form = $('#assign_member_to_group');
            $.HS.call_jquery_ajax(_form.attr('assign-member-to-group'), _form.serialize(), false, false, function (response) {
                _this.removeClass('clicked');
                window.setTimeout(function () {
                    App.stopPageLoading();
                }, 1000);
                if (response.success != undefined) {

                    toastr["success"](response.success, "Assign Member to Group");

                    var uid = $("#assign_member_to_group").find("[name='uid']").val();
                    /*if ($("td").find("[data-uid='" + uid + "']").hasClass('font-grey-salsa')) {
                     $("td").find("[data-uid='" + uid + "']").attr('class', 'btn font-dark btn-sm btn-hide sbold');
                     }
                     else if ($("td").find("[data-uid='" + uid + "']").hasClass('font-dark')) {
                     $("td").find("[data-uid='" + uid + "']").attr('class', 'btn font-grey-salsa btn-sm btn-hide sbold');
                     }*/
                    if(selected_group_value!="")
                    {
                        $("td").find("[data-uid='" + uid + "']").attr('class', 'btn font-grey-salsa btn-sm btn-hide sbold');
                    }
                    else
                    {
                        $("td").find("[data-uid='" + uid + "']").attr('class', 'btn font-dark btn-sm btn-hide sbold');
                    }

                    //$('#member_filter').trigger('change');
                    $('#modal_assignGroup').modal('hide');
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Assign Member to Group");
                }
            }, 'POST', true, '#hs-loader');
        }
    });


    //initiate select2 om modal pop up edit recommended message
    $("#modal_assignGroup2").on('shown.bs.modal', function () {

        $(".group-list").select2({
            width: null,
            allowClear: true
        });
        $(".group-list").on("select2:select", function () {
            var data = $('.group-list').select2('data');
            var datanew = data[0].text;
            if (datanew == 'Select') {
                $('.grp-selct-msg').text('You have removed member from the Group');
                $('#modal_assignGroup2 h4.group-name').text('');
            }
            else {
                $('.grp-selct-msg').text('');
                $('#modal_assignGroup2 h4.group-name').text(datanew);
            }
        })

        $(".btn-unset-grp").on('click', function () {

            $(".group-list").val(null).trigger('change');
            $('.grp-selct-msg').text('You have removed member from the Group');
            $('#modal_assignGroup2 h4.group-name').text('');
        });

        $("button[data-select2-open]").click(function () {
            $("#" + $(this).data("select2-open")).select2("open");
        });

    });



    $('#birthday_today').click(function () {
        $("#birth_date").val('Birthdays today');
        $('.another_filter').hide();
        $('.group_filter').hide();
        $("#member_filter").val('all').trigger('change.select2');
        membertab.ajax.reload();


    });

});



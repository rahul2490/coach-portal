$(document).ready(function () {

   // var users_data = [];
    var patient_msg_reload_timer;

    $("#direct_msg_by_coach").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {

            send_message: {
                required: "Please enter a message"
            }
        },
        rules: {
            send_message: {
                required: true
            },
            uploadfile:{
                extension: "jpg|jpeg|png|pdf"
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _form = $('#direct_msg_by_coach');
            var id = _form.find('[name="id"]').val();
            //var formData = new FormData(_form);
            var formData = new FormData($('#direct_msg_by_coach')[0]);

            if(id){
                $('#loding_icon').trigger('click');
                clearInterval(patient_msg_reload_timer);
                $("#direct_msg_by_coach .send").attr('disabled', 'disabled');
                $("#direct_msg_by_coach .save").attr('disabled', 'disabled');
                $('#saved-messages').children('option:not(:first)').remove();
                $.ajax({
                    url: APP_BASE_URL +'/saveDirectMessage',
                    data: formData,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {

                        if (response.success != undefined) {
                            $(response.success).appendTo('.chat_message');
                            $('.remove').parent('div').remove();
                            $("#uploadfile").val(null);

                            $('#direct_msg_by_coach #send_message').val('');

                            if (response.last_message_id != null) {
                                _form.find('[name="last_message_id"]').val(response.last_message_id);
                            }

                            /* For saved message */
                            if (response.saved_message != undefined) {
                                $.each(response.saved_message, function (key, value) {
                                    $("#saved-messages").append('<option value="' + key + '">' + value + '</option>');
                                });
                            }

                            //scroll chet div to bottom
                            var container = $("#direct-message-scroll");
                            container.slimScroll({
                                scrollTo: container[0].scrollHeight,
                                wheelStep : 10,
                                touchScrollStep : 75
                            });
                            direct_msg_to_patient_reload();
                        }
                        else if (response.error != undefined) {
                            toastr["error"](response.error, "Direct Messages");
                        }
                        window.setTimeout(function() { App.stopPageLoading();}, 1000);
                        $("#direct_msg_by_coach .send").removeAttr('disabled', 'disabled');
                        $("#direct_msg_by_coach .save").removeAttr('disabled', 'disabled');
                    },

                });

            }
        }
    });



/*it's for direct message full page messaging */
if($('body.direct_message_full_page').length) {

        $('#direct_msg_by_coach').get(0).reset();
        $("#direct_msg_by_coach").validate().resetForm();
        $('.chat_message').empty();

        $(".saved-messages").select2({
            width: null,
        });

        $(".canned-messages").select2({
            width: null,
        });

        $(".recom-messages").select2({
            width: null,
        });
        /*$("#filter-messages").select2({
            width: null,
        });*/

        $("#filter-messages").select2({
            width: null,
            sorter: function(data) {
                return data.sort(function(a, b) {
                    return a.text < b.text ? -1 : a.text > b.text ? 1 : 0;
                });
            }
            }).on("select2:select", function (e) {
                $('.select2-selection__rendered li.select2-selection__choice').sort(function(a, b) {
                    return $(a).text() < $(b).text() ? -1 : $(a).text() > $(b).text() ? 1 : 0;
                }).prependTo('.select2-selection__rendered');
            });

        $("button[data-select2-open]").click(function () {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        //change recommended msg to send
        $('.saved-messages').on('change', function () {
            var recentmsg = $(".saved-messages option:selected").val();
            if (recentmsg === "Select Saved Message") {
                $(".recomend-text").val('');
            }
            else {
                $(".recomend-text").val(recentmsg);
            }
        });
        $('.canned-messages').on('change', function () {
            var recentmsg = $(".canned-messages option:selected").text();
            if (recentmsg === "Select Canned Message") {
                $(".recomend-text").val('');
            }
            else {
                $(".recomend-text").val(recentmsg);
            }
        });

        $('.recom-messages').on('change', function () {
            var recentmsg = $(".recom-messages option:selected").val();
            if (recentmsg === "Select Recommended Message") {
                $(".recomend-text").val('');
            }
            else {
                $(".recomend-text").val(recentmsg);
            }
        });

        //mark as read sho whide
       /* $('.markread').on('click', function () {
            //toastr.clear();
            toastr['info']('<div style="margin-top: 5px;">You have not replied to the patient, Are you sure you want to mark the messages as read?</div><div><button type="button" class="btn btn-default pull-right" style="margin-top: 10px;">Cancel</button><button style="margin-left: 5px;margin-top: 10px;" type="button" id="yes_markread" class="btn btn-danger">Yes</button></div>', 'Mark as read?',
                {
                    closeButton: true,
                    preventDuplicates: true,
                    "positionClass": "toast-top-center",
                    "showDuration": "10000",
                    "timeOut": "15000",
                    "extendedTimeOut": "10000",
                    "preventDuplicates": true,
                    "maxOpened": 1,

                    allowHtml: true,
                    onShown: function (toast) {
                        $("#yes_markread").click(function () {
                            var _this = $(this);

                            var patient_id= $('.markread').attr('data-id');


                            $.ajax({
                                url: APP_BASE_URL +'/markreadmessage',
                                data: 'patient_id='+patient_id,
                                type: 'POST',

                                success: function (response) {


                                    window.setTimeout(function() { App.stopPageLoading();}, 1000);

                                    if (response.success != undefined) {

                                        $('.markread').hide();
                                        toastr["success"](response.success, "Mark as read");
                                    }

                                },

                            });



                        });
                    }
                });



        });*/

        $(".recomend-text").on('focus', function () {
            $('.markread').addClass('hide');
        });
        //create new
        $(" .btn-new-msg").on('click', function () {
            $(".recomend-text").val('');
        });


        $('#direct_msg_by_coach .member_name').text('');
        $('#filter-messages').children('option:not(:first)').remove();

         var _form = $('#direct_msg_by_coach');
         _form.find('[name="last_message_id"]').val('');
          var id = _form.find('[name="id"]').val();

        $('#recom-messages').children('option:not(:first)').remove();
        $('#saved-messages').children('option:not(:first)').remove();

        if (id) {
            _form.find('[name="id"]').val(id);
            clearInterval(patient_msg_reload_timer);
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                window.setTimeout(function () {
                    App.stopPageLoading();
                }, 1000);
                if (response.success != undefined) {

                    if (response.member_detail != undefined) {
                        $('#direct_msg_by_coach .member_name').text(response.member_detail.name);
                        $('#direct_msg_by_coach .avatar').attr('src', response.member_detail.image);
                        $('.markread').attr('data-id',response.member_detail.patient_id);
                        _form.find('[name="last_message_id"]').val(response.member_detail.last_message_id);
                    }

                    if (response.message != undefined) {
                        $.each(response.message, function (key, data) {
                            $(data).appendTo('.chat_message');
                        });
                    }

                    if (response.provider_list != undefined) {
                        $.each(response.provider_list, function (key, value) {
                            $("#filter-messages").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }

                    if (response.recommended_message != undefined) {
                        $.each(response.recommended_message, function (key, value) {
                            $("#recom-messages").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }

                    if (response.saved_message != undefined) {
                        $.each(response.saved_message, function (key, value) {
                            $("#saved-messages").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }

                    var customItemTemplate = "<div><span />&nbsp;<small /></div>";
                    function elementFactory(element, e) {
                        var template = $(customItemTemplate).find('span')
                            .text('@' + e.val).end()
                            .find('small')
                            .text("(" + e.meta + ")").end();
                        element.append(template);
                    };

                    var users_data = [];
                    if (response.tagging_list != undefined) {
                        $.each(response.tagging_list, function (key, value) {
                            users_data.push({val: value.name, meta: value.type});
                        });
                    }
                    _form.find('[name="send_message"]').sew({repeat: false, unique: true, values: users_data, elementFactory: elementFactory});

                    if(response.markread_status==1)
                    {
                        $(".markread").hide();
                    }
                    else
                    {
                        $(".markread").show();
                    }
                    //scroll chet div to bottom
                    var container = $("#direct-message-scroll");
                    container.slimScroll({
                        scrollTo: container[0].scrollHeight,
                        wheelStep : 10,
                        touchScrollStep : 75
                    });

                    direct_msg_to_patient_reload();

                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Direct Messages");
                }
            }, 'GET', true, '#hs-loader');

        }
}
/* Here is  end for direct message full page messaging  */



//initiate select2 om modal pop up  direct message coach
    $("#direct-msg-coach").on('show.bs.modal', function (e) {

        toastr.clear();
        $('.chat_message').empty();
        $('#direct_msg_by_coach').get(0).reset();
        $("#direct_msg_by_coach").validate().resetForm();


        $(".saved-messages").select2({
            width: null,
        });

        $(".canned-messages").select2({
            width: null,
        });

        $(".recom-messages").select2({
            width: null,
        });

        $("#filter-messages").select2({
            width: null,
            sorter: function(data) {
                return data.sort(function(a, b) {
                    return a.text < b.text ? -1 : a.text > b.text ? 1 : 0;
                });
            }
        }).on("select2:select", function (e) {
            $('.select2-selection__rendered li.select2-selection__choice').sort(function(a, b) {
                return $(a).text() < $(b).text() ? -1 : $(a).text() > $(b).text() ? 1 : 0;
            }).prependTo('.select2-selection__rendered');
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        //change recommended msg to send
        $('.saved-messages').on ('change', function(){
            var recentmsg = $(".saved-messages option:selected").val();
            if(recentmsg === "Select Saved Message"){
                $(".recomend-text").val('');
            }
            else{
                $(".recomend-text").val(recentmsg);
            }
        });

        $('.canned-messages').on ('change', function(){
            var recentmsg = $(".canned-messages option:selected"). text();
            if(recentmsg === "Select Canned Message"){
                $(".recomend-text").val('');
            }
            else{
                $(".recomend-text").val(recentmsg);
            }
        });

        $('.recom-messages').on ('change', function(){
            var recentmsg = $(".recom-messages option:selected").val();
            if(recentmsg === "Select Recommended Message"){
                $(".recomend-text").val('');
            }
            else{
                $(".recomend-text").val(recentmsg);
            }
        });


        $(".recomend-text").on ('focus', function(){
            $('.markread').addClass('hide');
        });
        //create new
        $(".btn-new-msg").on('click', function(){
            $(".recomend-text").val('');
        });

        $(".recomend-text").val($(e.relatedTarget).data('recomm_message'));


        $('#direct_msg_by_coach .member_name').text('');
        $('#filter-messages').children('option:not(:first)').remove();
        $('#recom-messages').children('option:not(:first)').remove();
        $('#saved-messages').children('option:not(:first)').remove();

        var _form = $('#direct_msg_by_coach');
        _form.find('[name="id"]').val('');
        _form.find('[name="last_message_id"]').val('');
        var id = $(e.relatedTarget).data('id');


        _form.find('.new_window_href').attr("href", APP_BASE_URL + USER_ROLE +'/direct-message/'+btoa(id));
        if(id){
            _form.find('[name="id"]').val(id);
            clearInterval(patient_msg_reload_timer);
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {

                    if (response.member_detail != undefined) {
                        $('#direct_msg_by_coach .member_name').text(response.member_detail.name);
                        $('#direct_msg_by_coach .avatar').attr('src', response.member_detail.image);
                        $('.markread').attr('data-id',response.member_detail.patient_id);
                        _form.find('[name="last_message_id"]').val(response.member_detail.last_message_id);
                    }

                    if (response.message != undefined) {

                         $.each(response.message, function(key, data) {
                            $(data).appendTo('.chat_message');

                             if((Object.keys(response.message).length-1)== key) {
                                 setTimeout(function(){
                                     var container = $("#direct-message-scroll");
                                     container.slimScroll({
                                         scrollTo: container[0].scrollHeight,
                                         wheelStep : 10,
                                         touchScrollStep : 75
                                     });
                                 }, 1000);
                             }
                        });
                    }

                    if (response.provider_list != undefined) {
                        $.each(response.provider_list, function (key, value) {
                            $("#filter-messages").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }

                    if (response.recommended_message != undefined) {
                        $.each(response.recommended_message, function (key, value) {
                            $("#recom-messages").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }

                    if (response.saved_message != undefined) {
                        $.each(response.saved_message, function (key, value) {
                            $("#saved-messages").append('<option value="' + key + '">' + value + '</option>');
                        });
                    }

                    var customItemTemplate = "<div><span />&nbsp;<small /></div>";
                    function elementFactory(element, e) {
                        var template = $(customItemTemplate).find('span')
                            .text('@' + e.val).end()
                            .find('small')
                            .text("(" + e.meta + ")").end();
                        element.append(template);
                    };

                    var users_data = [];
                    if (response.tagging_list != undefined) {
                        $.each(response.tagging_list, function (key, value) {
                            users_data.push({val: value.name, meta: value.type});
                        });
                    }
                    _form.find('[name="send_message"]').sew({repeat: false, unique: true, values: users_data, elementFactory: elementFactory});

                    if(response.markread_status==1)
                    {
                        $(".markread").hide();
                    }
                    else
                    {
                        $(".markread").show();
                    }

                    direct_msg_to_patient_reload();

                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Direct Messages");
                }
            }, 'GET', true, '#hs-loader');

        }

    });


    $('#direct-msg-coach').on('hidden.bs.modal', function () {
        clearInterval(patient_msg_reload_timer);
        $('#member_list').DataTable().ajax.reload(null, false);
    });


    function direct_msg_to_patient_reload()
    {
        var delay = 10000; //5 minutes counted in milliseconds.
        patient_msg_reload_timer = setInterval(function(){
            var _form = $('#direct_msg_by_coach');
            var id = _form.find('[name="id"]').val();
            var last_message_id = _form.find('[name="last_message_id"]').val();
            if(id && last_message_id){
                $.HS.call_jquery_ajax(_form.attr('reload-direct-message'), _form.serialize(), false, false, function (response) {
                    //window.setTimeout(function() { App.stopPageLoading();}, 1000);
                    if (response.success != undefined) {

                        if (response.message != undefined) {
                            $.each(response.message, function(key, data) {
                                $(data).appendTo('.chat_message');
                            });
                        }

                        if (response.member_detail != undefined && response.member_detail.last_message_id != null) {
                            _form.find('[name="last_message_id"]').val(response.member_detail.last_message_id);
                        }

                        //scroll chet div to bottom
                        if(response.member_detail.last_message_id != null)
                        {
                            var container = $("#direct-message-scroll");
                            container.slimScroll({
                                scrollTo: container[0].scrollHeight,
                                wheelStep : 10,
                                touchScrollStep : 75
                            });
                        }
                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, "Direct Messages");
                    }
                }, 'GET', false, '#hs-loader');
            }
        }, delay);
    }


    //mark as read sho whide
    $('#direct-msg-coach .markread').on('click', function () {

        toastr['info']('<div style="margin-top: 5px;">You have not replied to the member, Are you sure you want to mark the messages as read?</div><div><button type="button" class="btn btn-default pull-right" style="margin-top: 10px;">Cancel</button><button style="margin-left: 5px;margin-top: 10px;" type="button" id="yes_markread" class="btn btn-danger">Yes</button></div>', 'Mark as read?',
            {
                closeButton: true,
                preventDuplicates: true,
                "positionClass": "toast-top-center",
                "showDuration": "10000",
                "timeOut": "15000",
                "extendedTimeOut": "10000",
                "preventDuplicates": true,
                //"tapToDismiss": false,
                allowHtml: true,
                onShown: function (toast) {
                    $("#yes_markread").click(function () {
                        var _this = $(this);
                        var patient_id= $('.markread').attr('data-id');
                        $.ajax({
                            url: APP_BASE_URL +'/markreadmessage',
                            data: 'patient_id='+patient_id,
                            type: 'POST',

                            success: function (response) {
                                window.setTimeout(function() { App.stopPageLoading();}, 1000);

                                if (response.success != undefined) {
                                    $('#direct-msg-coach').modal('hide');
                                    $('.markread').css("display","none");
                                    toastr["success"](response.success, "Mark as read");
                                }

                            },

                        });
                    });
                }
            });
    });


$('.messages').on('click', function(){
    $('#direct-msg-coach').modal('show');
});

$("select option:selected").text();
//initiate tool tip
$('[data-toggle="tooltip"]').tooltip();


$("#filter-messages").on('change', function() {
    if($("#filter-messages option:selected").val() != '')
    {
        $('.chat_message').empty();

        var _form = $('#direct_msg_by_coach');
        var id = _form.find('[name="id"]').val();
        if(id){
            clearInterval(patient_msg_reload_timer);
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {

                if (response.success != undefined) {
                    if (response.message != undefined) {
                        $.each(response.message, function(key, data) {
                            $(data).appendTo('.chat_message');
                        });
                    }

                    if (response.member_detail != undefined && response.member_detail.last_message_id != null) {
                        _form.find('[name="last_message_id"]').val(response.member_detail.last_message_id);
                    }

                    //scroll chet div to bottom
                    var container = $("#direct-message-scroll");
                    container.slimScroll({
                        scrollTo: container[0].scrollHeight,
                        wheelStep : 10,
                        touchScrollStep : 75
                    });

                    direct_msg_to_patient_reload();
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Direct Messages");
                }
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
            }, 'GET', true, '#hs-loader');
        }
    }
});


$("#direct-msg-coach .expand").on('click', function() {

    if($("#direct-msg-coach").find('.modal-dialog').hasClass('modal-lg'))
    {
        $("#direct-msg-coach").find('.modal-dialog').attr('class', 'modal-dialog modal-full');
    }
    else if($("#direct-msg-coach").find('.modal-dialog').hasClass('modal-full'))
    {
        $("#direct-msg-coach").find('.modal-dialog').attr('class', 'modal-dialog modal-lg');
    }
});



/**
 * Member assign to Group page validation
 */

// Older "accept" file extension method. Old docs: http://docs.jquery.com/Plugins/Validation/Methods/accept
$.validator.addMethod( "extension", function( value, element, param ) {
    param = typeof param === "string" ? param.replace( /,/g, "|" ) : "png|jpe?g|gif";
    return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
}, $.validator.format( "Only image/pdf files uploading supported" ) );





/* file attachment code start here */

$("#attachment").click(function(){
    $("#uploadfile").click();
});

$('#uploadfile').on('change', function() {

    var filename = this.value;
    var lastIndex = filename.lastIndexOf("\\");
    if (lastIndex >= 0) {
        filename = filename.substring(lastIndex + 1);
    }
    var files = $('#uploadfile')[0].files;
    for (var i = 0; i < files.length; i++) {
        $("#file_upload_show").html('<div id="tab_images_uploader_filelist" class="col-md-11 col-sm-11"><div class="alert alert-warning added-files" id="uploaded_file_o_1bs7ktva0hhsijfcln1fpi1o137">' + files[i].name +  '<span class="status label label-info"></span>&nbsp;<a href="javascript:;" style="margin-top:-5px" class="remove pull-right btn btn-sm red"><i class="fa fa-times"></i> remove</a></div></div>');

    }
});

/* file attachemt code end here */

    /* file attachment remove code start here */
    $(document).on('click', '.remove', function() {
        $(this).parent('div').remove();
        $("#uploadfile").val(null);

    });


});



/**
 * Created by rpatidar on 31/08/2017.
 */
var coach_tab="";
jQuery(document).ready(function ($) {

    /**
     * Coach List Datatable Function
     */

    coach_tab =   $('#admin_coach_list').DataTable({
        processing: false,
        serverSide: false,
        pageLength: 50,
        bLengthChange: false,
        "sPaginationType": "full_numbers",
        order: [[1, 'desc']],
        "language": {
            "emptyTable": "Coach not exist into this facility.",
            "loadingRecords": "Please wait - loading..."
        },
        aoColumns: [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false },
            { "bSortable": false }
        ],
        "ajax": {
            "url": $('#admin_coach_list').data('action'),
            "type": "GET",
        },
        "fnDrawCallback" : function() {
            window.setTimeout(function() { App.stopPageLoading();}, 1000);
        },
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0,
        },
            { className: "dt-right", "targets": [1] },
            { className: "dt-center", "targets": [6,7] },
        ],
    });


    /**
     * Coach List Datatable Function
     */
    $('#coach_list').dataTable({
        processing: false,
        serverSide: false,
        pageLength: 50,
        bLengthChange: false,
        "sPaginationType": "full_numbers",
        order: [[1, 'desc']],
        "language": {
            "emptyTable": "Coach not exist into this facility.",
            "loadingRecords": "Please wait - loading..."
        },
        aoColumns: [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": false }
        ],
        "ajax": {
            "url": $('#coach_list').data('action'),
            "type": "GET",
        },
        "fnDrawCallback" : function() {
            window.setTimeout(function() { App.stopPageLoading();}, 1000);
        },
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0,
        },
        { className: "dt-right", "targets": [1] },
        { className: "dt-center", "targets": [6,7] },
        ],
    });


    /* Assign Coach Show Model */

    $("#modal_assigncoach").on('show.bs.modal', function (e) {

        $('.select_multi_coach_food').hide();
        $('.select_multi_coach_life').hide();
        $('#assign_coach_to_facility').get(0).reset();
        $("#assign_coach_to_facility").validate().resetForm();

        var _form = $('#assign_coach_to_facility');
        $("#lead_coach").children().remove("optgroup");
        $("#food_coach").children().remove("optgroup");
        $('#lead_coach').children('option').remove();
        $('#food_coach').children('option').remove();



            $.HS.call_jquery_ajax(_form.attr('assign_coach_data'), _form.serialize(), false, false, function (response) {

                window.setTimeout(function () {
                    App.stopPageLoading();
                }, 1000);

                if (response.success != undefined) {

                    if (response.leadcoach != undefined) {

                        $("#lead_coach").append(' <optgroup label="Lifestyle Coach">');
                        $.each(response.leadcoach, function (key, value) {

                            $("#lead_coach").append('<option title="'+ value.email+'"  value="' + value.provider_id+ '">' + value.full_name + '</option>');
                        });
                        $("#lead_coach").append(' </optgroup>');


                        $("#lead_coach").select2({
                            width: null,
                            templateResult: formatOption
                        });


                    }

                    if (response.foodcoach != undefined) {

                        $("#food_coach").append(' <optgroup label="Food Coach">');
                        $.each(response.foodcoach, function (key, value) {

                            $("#food_coach").append('<option  title="'+ value.email+'"  value="' + value.provider_id+ '">' + value.full_name + '</option>');
                        });
                        $("#food_coach").append(' </optgroup>');

                        $("#food_coach").select2({
                            width: null,
                            templateResult: formatOption
                        });


                    }

                    function formatOption (option)
                    {
                        var $option = $(
                            '<div><strong>' + option.text + '</strong></div><div>' + option.title + '</div>'
                        );
                        return $option;
                    }

                }



                else if (response.error != undefined) {
                    toastr["error"](response.error, "Assign Coach");
                }
            },'POST', true, '#hs-loader');




    });

    /* */


    /**
     * Assign Coach from coach list page validation
     */
    $("#assign_coach_to_facility").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
            select_multi_food: {
                maxlength: jQuery.validator.format("Max {0} Coach must be selected"),
                minlength: jQuery.validator.format("At least {0} Coach must be selected")
            },
            select_multi_life: {
                maxlength: jQuery.validator.format("Max {0} Coach must be selected"),
                minlength: jQuery.validator.format("At least {0} Coach must be selected")
            }

        },
        rules: {
            select_multi_food: {
                required: true,
                minlength: 1,
                maxlength: 1
            },
            select_multi_life: {
                required: true,
                minlength: 1,
                maxlength: 1
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _this = $(this);
            var _form = $('#assign_coach_to_facility');
            $('#modal_assigncoach').modal('hide');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                _this.removeClass('clicked');
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                        toastr["success"](response.success, "Assign Coach");
                        $('#coach_list').DataTable().ajax.reload(null, false);

                        if($("#assign_coach_to_facility input[type='radio']:checked").val() == 1)
                        {
                            $("#food_coach option[value='" + $("#food_coach").val() + "']").remove();
                            $("#food_coach").select2();
                        }
                        else if($("#assign_coach_to_facility input[type='radio']:checked").val() == 2)
                        {
                            $("#lead_coach option[value='" + $("#lead_coach").val() + "']").remove();
                            $("#lead_coach").select2();
                        }
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Assign Coach");
                }
            }, _form.attr('method'), true, '#hs-loader');
        }
    });


    /**
     * Assign Coach from coach list page validation
     */


   $("#facility_list").on("change", function(event) {
        $(this).valid() ;
    });


    $("#coach_edit_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            first_name: {
                minlength: 2,
                required: true
            },
            last_name: {
                minlength: 2,
                required: true
            },
            email: {
                email: true,
                required: true
            },
            phone: {
                required: true

            },
            coach_type: {
                required: true
            },
            "facility_list[]": {
              required:true
            },
            city:{
                required: true
            },
            state:{
                required: true
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _this = $(this);
            var _form = $('#coach_edit_form');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    //toastr["success"](response.success, "Update Detail");
                    if(_form.find('[name="user_id"]').val() == undefined)
                    {
                        _form.find('[name="email"]').val('');
                        _form.find('[name="last_name"]').val('');
                        _form.find('[name="first_name"]').val('');
                        _form.find('[name="phone"]').val('');
                        _form.find('[name="acuity_link"]').val('');
                        _form.find('[name="city"]').val('');
                        _form.find('[name="state"]').val('');
                         $("#facility_list").select2('val','All');
                    }

                    if(response.redirect_url!=undefined)
                     window.location.href = response.redirect_url;
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Update Detail");
                }
            }, _form.attr('method'), true, '#hs-loader');
        }
    });


    $("#coach_reset_password_btn").click(function(){

       var  user_id = $(this).data("id")  ;
       //alert(APP_BASE_URL);
        $.ajax({
            type: 'POST',
            url: APP_BASE_URL + 'facilityadmin/coach-reset-password',
            data:"user_id="+user_id,

            success: function (response) {
                if (response.success != undefined) {

                    toastr["success"](response.success, "Reset Password");
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Reset Password");
                }
            }
        });


    });


    $("#admin_coach_reset_password_btn").click(function(){

        var  user_id = $(this).data("id")  ;
        //alert(APP_BASE_URL);
        $.ajax({
            type: 'POST',
            url: APP_BASE_URL + 'admin/coach-reset-password',
            data:"user_id="+user_id,

            success: function (response) {
                if (response.success != undefined) {

                    toastr["success"](response.success, "Reset Password");
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Reset Password");
                }
            }
        });


    });




    /**
     * On click assign coach button open model
     */
    $('#modal_assign_coach').on('click', function (e) {
        toastr.clear();
        $("#food_coach").val('').trigger('change');
        $("#lead_coach").val('').trigger('change');
        $("#food_coach").select2({
            width: null,
            allowClear: true
        });
        $("#lead_coach").select2({
            width: null,
            allowClear: true
        });
    });





    /**
     *  Onclick Assign Coach modal radio button show hide food coach and lead coach dropdown
     */
    $(".membership").on('click', function(){
        if ($(this).val() == 1) {
            $('.select_multi_coach_food').show();
            $('.select_multi_coach_life').hide();
            $('#food_coach').removeClass('ignore').show();
            $('#lead_coach').addClass('ignore').hide();
            $("#lead_coach").val('').trigger('change');
        } else if($(this).val() == 2) {
            $('.select_multi_coach_life').show();
            $('.select_multi_coach_food').hide();
            $('#lead_coach').removeClass('ignore').show();
            $('#food_coach').addClass('ignore').hide();
            $("#food_coach").val('').trigger('change');
        }
    });


    /**
    * Show Coach Delete Modal Handler
    */
    $('#remove_coach').on('show.bs.modal', function (e) {
        var _form = $('#delete-msg');
        _form.find('[name="id"]').val('');
        var id = $(e.relatedTarget).data('id');
        if(id){
            _form.find('[name="id"]').val(id);
        }
    });


    /**
     * Remove Coach From facility Handler
     */
    $('#remove_coach_from_facility').on('click', function(){
        var _this = $(this);
        var _form = $('#delete-msg');
        if (_this.hasClass('clicked')) {
            return;
        }
        $('#remove_coach').modal('hide');
        $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
            window.setTimeout(function() { App.stopPageLoading();}, 1000);
            if (response.success != undefined) {
                var row_id = _form.find('[name="id"]').val();
                $('td:contains("'+row_id+'")').parent().hide(800);
                $('.child').hide();
                toastr["success"](response.success, "Remove Coach");
            }
            else if (response.error != undefined) {
                toastr["error"](response.error, "Remove Coach");
            }
        }, _form.attr('method'), true, '#hs-loader');
    });


    $(".select2, .select2-multiple").select2({
        width: null,
        allowClear: true
    });

    $("button[data-select2-open]").click(function() {
        $("#" + $(this).data("select2-open")).select2("open");
    });


    $(".select_facility_list").select2({
        allowClear: false
    });


});





//Make sure jQuery has been loaded before hs-main.js
if (typeof jQuery === "undefined") {
    throw new Error("HS requires jQuery");
}

//
// Pipelining function for DataTables. To be used to the `ajax` option of DataTables
//
$.fn.dataTable.pipeline = function ( opts ) {
    // Configuration options
    var conf = $.extend( {
        pages: 5,     // number of pages to cache
        url: '',      // script url
        data: null,   // function or object with parameters to send to the server
        method: 'POST' // Ajax HTTP method
    }, opts );

    // Private variables for storing the cache
    var cacheLower = -1;
    var cacheUpper = null;
    var cacheLastRequest = null;
    var cacheLastJson = null;

    return function ( request, drawCallback, settings ) {
        var ajax          = false;
        var requestStart  = request.start;
        var drawStart     = request.start;
        var requestLength = request.length;
        var requestEnd    = requestStart + requestLength;

        if ( settings.clearCache ) {
            // API requested that the cache be cleared
            ajax = true;
            settings.clearCache = false;
        }
        else if ( cacheLower < 0 || requestStart < cacheLower || requestEnd > cacheUpper ) {
            // outside cached data - need to make a request
            ajax = true;
        }
        else if ( JSON.stringify( request.order )   !== JSON.stringify( cacheLastRequest.order ) ||
            JSON.stringify( request.columns ) !== JSON.stringify( cacheLastRequest.columns ) ||
            JSON.stringify( request.search )  !== JSON.stringify( cacheLastRequest.search )
        ) {
            // properties changed (ordering, columns, searching)
            ajax = true;
        }

        // Store the request for checking next time around
        cacheLastRequest = $.extend( true, {}, request );

        if ( ajax ) {
            // Need data from the server
            if ( requestStart < cacheLower ) {
                requestStart = requestStart - (requestLength*(conf.pages-1));

                if ( requestStart < 0 ) {
                    requestStart = 0;
                }
            }

            cacheLower = requestStart;
            cacheUpper = requestStart + (requestLength * conf.pages);

            request.start = requestStart;
            request.length = requestLength*conf.pages;

            // Provide the same `data` options as DataTables.
            if ( $.isFunction ( conf.data ) ) {
                // As a function it is executed with the data object as an arg
                // for manipulation. If an object is returned, it is used as the
                // data object to submit
                var d = conf.data( request );
                if ( d ) {
                    $.extend( request, d );
                }
            }
            else if ( $.isPlainObject( conf.data ) ) {
                // As an object, the data given extends the default
                $.extend( request, conf.data );
            }

            settings.jqXHR = $.ajax( {
                "type":     conf.method,
                "url":      conf.url,
                "data":     request,
                "dataType": "json",
                "cache":    false,
                "success":  function ( json ) {
                    cacheLastJson = $.extend(true, {}, json);

                    if ( cacheLower != drawStart ) {
                        json.data.splice( 0, drawStart-cacheLower );
                    }
                    if ( requestLength >= -1 ) {
                        json.data.splice( requestLength, json.data.length );
                    }

                    drawCallback( json );
                }
            } );
        }
        else {
            json = $.extend( true, {}, cacheLastJson );
            json.draw = request.draw; // Update the echo for each response
            json.data.splice( 0, requestStart-cacheLower );
            json.data.splice( requestLength, json.data.length );

            drawCallback(json);
        }
    }
};

// Register an API method that will empty the pipelined data, forcing an Ajax
// fetch on the next draw (i.e. `table.clearPipeline().draw()`)
$.fn.dataTable.Api.register( 'clearPipeline()', function () {
    return this.iterator( 'table', function ( settings ) {
        settings.clearCache = true;
    } );
} );

jQuery(document).ready(function ($) {

    // Initializing App

    $.HS.init();

    $('body').delegate('.redirect-to', 'click', function () {
        var link = $.trim($(this).data('href'));
        if (link.length) {
            window.location = link;
        }
    });

    /**
     *  Hide modal loader when iframe is ready
     */
    $('#meal-rec-mail').on('load', function () {
        $('#hs-frame-loader').hide();
        $(this).contents().find("a").on("click", function (event) {
            event.preventDefault();
            event.stopPropagation();
            return false;
        });
    });

    /**
     *  build iframe URL and show it in modal
     */
    $('.my-patients .hs-table, .build-week-summary').on('click', '.show-summary-history', function (e) {
        var _this = $(this),
                _modal = $('#meal-recommendations'),
                patient_id = $.trim(_this.data('patient-id')),
                log_id = $.trim(_this.data('log-id')),
                sent = $.trim(_this.data('sent')),
                url = $.trim(_modal.find('#meal-rec-mail').data('src'));
        if (!_this.hasClass('disabled') && patient_id.length && log_id.length && url.length) {
            $('#hs-frame-loader').show();
            _modal.find('#meal-rec-mail').attr('src', url + '/' + patient_id + '/' + log_id);
            if (sent.length) {
                _modal.find('.mail-status').addClass('sent');
            } else {
                _modal.find('.mail-status').removeClass('sent');
            }
            _modal.modal('show');
        }
        return false;
    });


    /**
     * Removing session storage when moved away
     */
    if ($('body.my-patients, body.build-week-summary').length == 0) {
        window.sessionStorage.removeItem('myMembers');
        window.sessionStorage.removeItem('myMemberTable');
    }

    /**
     * Common function to initialize select message drop-down
     * @param _select
     * @param onSelection
     */
    var initialize_select_old_messages = function (_select, onSelection) {
        _select.prepend('<option/>').select2({
            dropdownParent: _select.parent(),
            minimumInputLength: 0,
            placeholder: {
                id: 0,
                text: 'Search a message'
            },
            ajax: {
                url: _select.data('src'),
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term,
                        only_me: _select.parents('.form-group:first').find('[name="only_me"]:checked').val()
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: data.items ? data.items : []
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) {
                return markup;
            },
            templateResult: function (row) {
                if (row.loading)
                    return row.text;

                var markup =
                        "<div class='select2-result-repository no-padding clearfix'>" +
                        "<div class='select2-result-repository__title'>" + row.title + "</div>";
                markup +=
                        "<div class='select2-result-repository__statistics'>" +
                        "<div class='select2-result-meta_info'> <b>Owner</b>: " + (row.updater ? row.updater : '') + "</div>" +
                        "</div>" +
                        "</div>";

                return markup;
            },
            templateSelection: function (row) {
                if (row.title) {
                    onSelection(row);
                }
                return row.title ? row.title : row.text;
            }
        });
    };

    var string_to_html_text = function (text, _target) {
        _target.html('');
        if (text.length) {
            text = text.split(/\r?\n/g);
            $.each(text, function (k, str) {
                var e = $('<span/>').text($.trim(str));
                _target.append(e).append($('<br/>')).append('\n');
            });
        }
    };

    if ($('body.meal-info').length) {
        $('form.meal-food-form').on('submit', function (e) {
            var _this = $(this);

            if (_this.find('[name="meal_food_ids"]').val() == "") {
                _this.find('.suggested-meals').html('');
                return false;
            }
            if (_this.hasClass('submitted')) {
                return false;
            }
            _this.addClass('submitted');
            _this.find('.suggested-meals').html('');
            $.HS.call_jquery_ajax(_this.attr('action'), _this.serialize(), false, false, function (response) {
                _this.removeClass('submitted');
                if (response.success != undefined && response.html != undefined) {
                    _this.find('.suggested-meals').html(response.html);
                    $.HS.lazy_load_images();
                } else if (response.error != undefined) {
                    _this.find('.suggested-meals').html(response.error);
                } else {

                }
            }, _this.attr('method'), true, '#suggested-loader');
        }).trigger('submit');

        /**
         * Build previous meal view
         */
        $('form.prev-meal-form').on('submit', function (e) {
            var _this = $(this);
            $.HS.call_jquery_ajax(_this.attr('action'), _this.serialize(), false, false, function (response) {
                if (response.success != undefined && response.html != undefined) {
                    $('#previous-meal-placeholder').html(response.html);
                    $.HS.lazy_load_images();
                    $('#previous-meals-carousel').carousel({
                        wrap: false,
                        keyboard: false
                    }).carousel('pause');
                } else if (response.error != undefined) {
                    $('#previous-meal-placeholder').html(response.error);
                } else {

                }
            }, _this.attr('method'), true, '#previous-m-loader');
        }).trigger('submit');

    }

    /**
     * my patients screen
     */
    else if ($('body.my-patients').length) {
        // init datatable
        var _table = $('.hs-table');
        var src = $.trim(_table.data('src'));
        var table = _table.DataTable({
            processing: true,
            lengthChange: false,
            bAutoWidth: false,
            serverSide: true,
            stateSave: true,
            stateDuration: -1,
            order: [[8, 'desc']],
            pageLength: $('.parameters [name="list_datatable_row_per_page"]').val(),
            stateSaveParams: function (settings, data) {
                data.filter = {
                    logged_meal: $('select[name="logged_meal"]').val(),
                    report_sent: $('select[name="report_sent"]').val()
                };
            },
            stateLoadParams: function (settings, data) {
                if (typeof data.filter == "object") {
                    var _filter_form = $('#filter-form');
                    $.each(data.filter, function (name, value) {
                        _filter_form.find('[name="' + name + '"]').val(value);
                    });
                }
            },
            stateSaveCallback: function (settings, data) {
                window.sessionStorage.setItem('myMemberTable', JSON.stringify(data));
            },
            stateLoadCallback: function (settings) {
                return $.HS.getJSON(window.sessionStorage.getItem('myMemberTable'));
            },
            ajax: src,
            fnServerParams: function (aoData) {
                aoData['logged_meal'] = $('#filter-form select[name="logged_meal"]').val();
                aoData['report_sent'] = $('#filter-form select[name="report_sent"]').val();
            },
            columns: [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                {"orderData": 10},
                {visible: false, searchable: false},
                {visible: false, searchable: false, type: 'num'}
            ],
            createdRow: function (row, data, index) {
                if (data[9]) {
                    $(row).data('href', data[9])
                            .addClass('redirect-to pointer')
                            .data('toggle', 'tooltip')
                            .attr('data-toggle', 'tooltip')
                            .attr('title', 'Click on the row to Build Weekly Meal Review');
                }
            },
            drawCallback: function (settings) {
                var members = this.api().ajax.json().members;
                window.sessionStorage.setItem('myMembers', members ? JSON.stringify(members) : []);
                $.HS.refresh_dates();
            },
            preDrawCallback: function (oSettings) {
                $('.hs-dt-filter').removeClass('no-margin');
            }
        });

        $('#filter-form .trigger-dt-change').change(function () {
            table.draw();
        });

    } else if ($('body.build-week-summary').length) {

        var _summary_form = $('#send-summary');

        /**
         * Create Previous and Next Links from Session Storage
         * @type {{}}
         */
        var myMembers = $.HS.getJSON(window.sessionStorage.getItem('myMembers'));
        var patient_id = parseInt(_summary_form.find('[name="patient_id"]').val());
        var index = $.inArray(patient_id, myMembers);
        if (index !== -1) {
            var prev = myMembers[index - 1] ? myMembers[index - 1] : null;
            var next = myMembers[index + 1] ? myMembers[index + 1] : null;
            if (prev) {
                $('#prev-patient').attr('href', $('#prev-patient').attr('href') + '/' + prev).removeClass('hide');
            }
            if (next) {
                $('#next-patient').attr('href', $('#next-patient').attr('href') + '/' + next).removeClass('hide');
            }
        }

        $('#pre-report-list').on('change', function () {
            var _btn = $(this).parent().find('.show-summary-history');
            if (_btn.length) {
                var log_id = $(this).val();
                var sent = $.trim($(this).find('option[value="' + log_id + '"]').data('sent'));
                if (log_id.length && !isNaN(log_id)) {
                    _btn.data('log-id', log_id).data('sent', $(this).find('option[value="' + log_id + '"]').data('sent')).removeClass('disabled');
                } else {
                    _btn.addClass('disabled').data('sent', '');
                }
            }
        }).select2();

        $('#choose-sm-modal').on('show.bs.modal', function (e) {
            var _form = $(this).find('form#load-sm');
            if (_form.hasClass('init-done')) {
                return true;
            }
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                var _placeholder = $('#sm-placehoder');
                if (response.success != undefined && response.html != undefined) {
                    _form.addClass('init-done');
                    _placeholder.html(response.html);
                    $.HS.lazy_load_images();

                } else if (response.error != undefined) {
                    $.HS.build_notification(_placeholder, response.error, 'danger');
                } else {
                    $.HS.build_notification(_placeholder, $.HS.common_error_message, 'danger');
                }
            }, _form.attr('method'), true, '#hs-sml-loader');
        });

        $('form#load-rm').on('submit', function (e) {
            var _form = $(this);
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                var _placeholder = $('#rm-placehoder');
                if (response.success != undefined && response.html != undefined) {
                    _placeholder.append(response.html);
                    if ((response.records % 4) != 0 || response.records == 0) {
                        $('#btn-rm-submit').hide();
                    }
                    $.HS.lazy_load_images();
                    var page = $('#load-rm [name="page"]').val();
                    page = parseInt(page) + 1;
                    $('#load-rm [name="page"]').val(page);
                } else if (response.error != undefined) {
                    $.HS.build_notification(_placeholder, response.error, 'danger');
                } else {
                    $.HS.build_notification(_placeholder, $.HS.common_error_message, 'danger');
                }
            }, _form.attr('method'), true, '#hs-rml-loader');
        });

        $('#choose-rm-modal').on('show.bs.modal', function (e) {
            var _this = $(this);
            if (_this.hasClass('init-done')) {
                return true;
            }
            $('form#load-rm').submit();
            _this.addClass('init-done');
        });

        $('#btn-rm-submit').on('click', function (e) {
            $('form#load-rm').submit();
        });

        $('#choose-rm-modal .meal-info').on('click', function () {
            var _this = $(this);
            var _content = _this.find('.meal-block').html();
            $('.selected-recent-meal').html(_content);
            _summary_form.find('[name="recent_meal_id"]').val(_this.data('food-log-summary-id'));
            $('#choose-rm-modal').modal('hide');
        });

        $('#choose-sm-modal').on('click', '.meal-info', function () {
            var _this = $(this);
            var _content = _this.find('.meal-block').html();
            $('.selected-suggested-meal').html(_content);
            $('.wmr-message .create-sm-msg').remove();
            _summary_form.find('[name="suggested_meal_id"]').val(_this.data('food-log-summary-id'));
            $('#choose-sm-modal').modal('hide');
        });

        /**
         *
         */
        $('#modify-message').on('show.bs.modal', function (e) {
            var _modal = $(this);
            var _message_content = $.trim($('#message-content:not(.isEmpty)').text());
            _modal.find('textarea').val(_message_content);
        });

        /**
         *
         */
        $('#modify-message .update-btn').on('click', function () {
            var _modal = $(this).parents('.modal:first');
            var _target = $('#message-content');
            var msg = $.trim(_modal.find('textarea').val());
            _target.removeClass('isEmpty');
            if (msg.length == 0) {
                msg = _target.data('no-content');
                _target.addClass('isEmpty');
            }
            string_to_html_text(msg, _target);
            _summary_form.find('[name="message"]').val((_target.hasClass('isEmpty') ? '' : msg));
            _modal.find('[data-dismiss="modal"]:first').trigger('click');
        }).trigger('click');

        /**
         *
         */
        $('#confirm-wmr-save .confirm-btn').on('click', function () {
            var _this = $(this);
            if (_this.hasClass('clicked')) {
                return;
            }
            _summary_form.find('[name="send_now"]').val($.trim($('#send_now_chk:checked').val()));
            _this.addClass('clicked');
            $('#confirm-wmr-save').modal('hide');
            $.HS.call_jquery_ajax(_summary_form.attr('action'), _summary_form.serialize(), false, false, function (response) {
                _this.removeClass('clicked');
                var _placeholder = $('#alert-placeholder');
                if (response.success != undefined) {
                    _this.addClass('clicked');
                    $('#btn-schedule').attr('disabled', 'disabled');
                    $.HS.build_notification(_placeholder, response.success, 'success');
                } else if (response.error != undefined) {
                    $.HS.build_notification(_placeholder, response.error, 'danger');
                } else {
                    $.HS.build_notification(_placeholder, $.HS.common_error_message, 'danger');
                }
            }, _summary_form.attr('method'), true, '#hs-loader');

        });

        /**
         * Update Text box with message selected from select box.
         * @param data
         */
        var on_message_selected = function (data) {
            var _modal = $('#modify-message');
            var msg = data.message ? data.message : null;
            if (msg) {
                _modal.find('textarea').val(msg);
            }
        };
        /**
         * initializing select box to select existing messages from library
         */
        initialize_select_old_messages($('#old-msg'), on_message_selected);
    } else if ($('body.search-patient').length) {
        $('select#search-patient-input')
                .select2({
                    placeholder: {
                        id: '0', // the value of the option
                        text: 'Search a patient'
                    },
                    minimumInputLength: 1,
                    ajax: {
                        url: $('select#search-patient-input').data('src'),
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                q: params.term
                            };
                        },
                        processResults: function (data, params) {
                            return {
                                results: data.items ? data.items : []
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    },
                    templateResult: function (patient) {
                        if (patient.loading)
                            return patient.text;

                        var markup =
                                "<div class='select2-result-repository clearfix'>" +
                                "<div class='select2-result-repository__title'>" + patient.text + "</div>";
                        markup += "<div class='select2-result-repository__statistics'>" +
                                "<div class='select2-result-meta_info'> <b>Patient Id</b>: " + (patient.patient_id ? patient.patient_id : '') + "</div>" +
                                "<div class='select2-result-meta_info'> <span><b>User Id</b>: " + (patient.user_id ? patient.user_id : '') + "</span></div>" +
                                "<div class='select2-result-meta_info'> <b>Facility</b>: " + (patient.facility_id ? patient.facility_id : '') + "</div>" +
                                "</div>" +
                                "</div>";

                        return markup;
                    },
                    templateSelection: function (patient) {
                        if (patient.html) {
                            $('.detail-box').html(patient.html);
                        } else {
                            $('.detail-box').html('');
                        }
                        return patient.text;
                    }
                });
    } else if ($('body.create-group-wmr').length) {

        $('[name="header_msg"]').on('keyup', function () {
            var _this = $(this);
            var _target = $('#header-msg');
            string_to_html_text(_this.val(), _target);
            $('#send-group-wmr [name="header"]').val(_this.val());
        });

        var on_selection = function (data) {
            if (data.message) {
                $('#message-content').html((data.message).split(/\r?\n/g).join('<br/>')).removeClass('isEmpty');
                $('#send-group-wmr [name="message"]').val(data.message);
                $('.confirm-btn').removeAttr('disabled');
            } else {
                $('#message-content').html($('#message-content').data('no-content')).addClass('isEmpty');
                $('#send-group-wmr [name="message"]').val('');
                $('.confirm-btn').attr('disabled', 'disabled');
            }
        };
        initialize_select_old_messages($('#old-msg'), on_selection);


        var validate_send_group_wmr = function (_form, _placeholder) {
            _placeholder.html('');
            return true;
        };

        $('#send-group-wmr-btn').on('click', function () {
            var _this = $(this);
            var _form = $('#send-group-wmr');
            var _placeholder = $('#alert-placeholder');
            if (_this.hasClass('clicked') || !validate_send_group_wmr(_form, _placeholder)) {
                return false;
            }
            _this.addClass('clicked');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                _this.removeClass('clicked');
                if (response.success != undefined) {
                    _this.addClass('clicked');
                    $('.confirm-btn').attr('disabled', 'disabled');
                    $.HS.build_notification(_placeholder, response.success, 'success margin-t-10');
                } else if (response.error != undefined) {
                    $.HS.build_notification(_placeholder, response.error, 'danger margin-t-10');
                } else {
                    $.HS.build_notification(_placeholder, $.HS.common_error_message, 'danger margin-t-10');
                }
            }, _form.attr('method'), true, '#hs-loader');
        });

    }



    /**
     * Forgot Password validation
     */
    $("#forgot_password").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            email: {
                email: true,
                required: true
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _this = $(this);
            var _form = $('#forgot_password');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                //alert(response);
                if (response.success != undefined) {
                    // is list page
                    toastr["success"](response.success, "Forget Password");
                    $('#forgot_password').get(0).reset();
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Forget Password");
                }
            }, _form.attr('method'), false, '#hs-loader');
        }
    });




    var table = $('#sample_2');
    /*$('#coach_list').dataTable({
        processing: false,
        serverSide: false,
        pageLength: -1,
        bLengthChange: false,
        order: [[0, 'desc']],
        aoColumns: [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false }
        ],
        "ajax": {
            "url": "coachesList",
            "type": "POST"
        },
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0,
        },{ className: "dt-right", "targets": [1,4] }],
    });
*/






    /*$('#member_list').dataTable({
        processing: true,
        serverSide: true,
        pageLength: 50,
        length : -1,
        bLengthChange: false,
        order: [[0, 'desc']],
        deferRender: true,
        aoColumns: [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true }
        ],
        "ajax": $.fn.dataTable.pipeline( {
            url: 'membersList',
            pages: 5 // number of pages to cache
        } ),
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0
        },{ className: "dt-right", "targets": [1,4,5,6] } ],
    });*/





});

/* validation for reset password */

/**
 * Forgot Password validation
 */
$("#reset_password").validate({
    errorElement: 'span', //default input error message container
    errorClass: 'help-block help-block-error', // default input error message class
    focusInvalid: false, // do not focus the last invalid input
    ignore: "",  // validate all fields including form hidden input
    messages: {
    },
    rules: {
        password: {
            required: true,
            minlength:8
        },
        repeat_password: {
            required: true,
            minlength:8,
            equalTo:"#password"
        },
        pin_code: {
            required: true
        }

    },

    errorPlacement: function (error, element) { // render error placement for each input type
        var cont = $(element).parent('.input-group');
        var cont2 = $(element).parent('.form-group');
        if (cont) {
            cont.after(error);
        }
        if (cont2) {
            $(element).after(error);
        }
        if (cont&&cont2){
            cont.after(error);
        }
    },
    highlight: function (element) { // hightlight error inputs

        $(element)
            .closest('.form-group').addClass('has-error'); // set error class to the control group
    },
    unhighlight: function (element) { // revert the change done by hightlight
        $(element)
            .closest('.form-group').removeClass('has-error'); // set error class to the control group
    },
    success: function (label) {
        label
            .closest('.form-group').removeClass('has-error'); // set success class to the control group
    },
    submitHandler: function (form) {
        var _this = $(this);
        var _form = $('#reset_password');
        $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
            //alert(response);
            if (response.success != undefined) {
                // is list page
                toastr["success"](response.success, "Reset Password");
                $('#reset_password').get(0).reset();
                if(response.redirect_url!=undefined) {
                    setTimeout(function(){
                    window.location.href = response.redirect_url;
                    }, 5000);
                }
            }
            else if (response.error != undefined) {
                toastr["error"](response.error, "Reset Password");
            }
        }, _form.attr('method'), false, '#hs-loader');
    }
});

/*   */


/* code for help page for ios video */
if($('body.app_help_video_ios').length) {

    $(document).ready(function () {

        $.ajax({
            type: 'GET',
            url: APP_BASE_URL + '/assets/help/js/videos.json',
            //async: false,
            dataType: 'json',
            complete: function (data) {
                //alert(data);
                var html = '';
                var jsonresponse = data.responseJSON.videos.ios;
                //(jsonresponse);
                var i=1;
                $.each(jsonresponse, function (key, value) {

                    html += '<li class="list-group-item col-sm-4" data-videourl="' + value.videoUid + '" ><div class="grey-item" href=""><div class="icon-div"> <img class="img-responsive" src="https://embed-ssl.wistia.com/deliveries/' + value.thumbnailUrl + '.jpg?image_crop_resized=200x120" alt="" >   </div><div class="content-div"><h2>' + value.title + '  </h2><p class="view-status">  <strong>Video: ' + value.duration + ' </strong>/ Created On ' + value.dateCreated + '</p></div><div class="clearfix"></div></div></li>'
                 if(i%3==0)
                 {
                     html +='<div class="clearfix"></div>'
                 }
                  i++;
                });
                $('.video-data').append(html);

            }
        });
        //var showData = $('.html');
        $(document).on('click', '.list-group-item', function () {
            var vidid = $(this).attr('data-videourl');
            var title = $(this).find('.content-div').find('h2').text();
            window.location.href = 'videodetailios?videoid=' + vidid + '&title=' + title;
        });


    });


//alert("czzxzcx")

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
        //alert(sPageURL);
        //alert(sURLVariables);
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    var tech = getUrlParameter('videoid');
    var titletext = getUrlParameter('title');
//alert(titletext);
    $('.wistia_embed').addClass('wistia_async_' + tech);
    $('h2.clr-green').text(titletext);
    console.log('wistia_async_' + tech);
  }

/* code end for help page for ios video */



/* code for help page for android video */
if($('body.app_help_video_android').length) {
    //alert("ssss");

    $(document).ready(function () {
        $.ajax({
            type: 'GET',
            url: APP_BASE_URL + '/assets/help/js/videosandroid.json',
            //async: false,
            dataType: 'json',
            complete: function (data) {
                var html = '';
                var jsonresponse = data.responseJSON.videos.android;
                $.each(jsonresponse, function (key, value) {
                    html += '<li class="list-group-item col-sm-4" data-videourl="' + value.videoUid + '" ><div class="grey-item" href=""><div class="icon-div"> <img class="img-responsive" src="https://embed-ssl.wistia.com/deliveries/' + value.thumbnailUrl + '.jpg?image_crop_resized=200x120" alt="" >   </div><div class="content-div"><h2>' + value.title + '  </h2><p class="view-status">  <strong>Video: ' + value.duration + ' </strong>/ Created On ' + value.dateCreated + '</p></div><div class="clearfix"></div></div></li>'
                });
                $('.video-data-android').append(html);

            }
        });
        //var showData = $('.html');
        $(document).on('click', '.list-group-item', function () {
            var vidid = $(this).attr('data-videourl');
            var title = $(this).find('.content-div').find('h2').text();
            window.location.href = 'videodetailandriod?videoid=' + vidid + '&title=' + title;
        });


    });


    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    var tech = getUrlParameter('videoid');
    var titletext = getUrlParameter('title');
    $('.wistia_embed').addClass('wistia_async_' + tech);
    $('h2.clr-green').text(titletext);
    console.log('wistia_async_' + tech);
}
/* code end for help page for android video */
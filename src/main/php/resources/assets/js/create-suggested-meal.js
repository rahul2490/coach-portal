/**
 * Created by rpatidar on 31/08/2017.
 * Create Suggested Meal Modal Event & Actions.
 */

jQuery(document).ready(function ($) {

    // execute below bindings and actions only when `build-suggested-meal` modal exists
    if ($('#build-suggested-meal').length) {
        /**
         *  reset add food form
         */
        $('form.add-form').on('reset', function () {
            $('.serving-units').hide();
            $('form.add-form [name="log_id"]').val('');
            $('form.add-form [name="food_id"]').data('cache', []).val('').trigger('change');
            $('form.add-form select[name="food_search"]').val('').trigger('change');
            $('form.add-form [name="food_name"]').val('').data('detail', {});
            $('form.add-form .add-food-btn').attr('disabled', 'disabled');
            $('form.add-form [name="serving"]').attr('disabled', 'disabled').val('');
            $('form.add-form [name="unit"]').attr('disabled', 'disabled').html('').val('');
        });

        $('#build-suggested-meal').on('show.bs.modal', function () {
            $('form.add-form').trigger('reset');

            // resetting create form
            $('form.create-form .food_input').remove();
            $('form.create-form [name="meal_name"]').attr('disabled','disabled');
            $('#build-suggested-meal .unlock-btn').show();
            $('#remove-img').hide();
            $('.s-meal-img-preview').attr('src', $('.s-meal-img-preview').data('src'));
            $('form.create-form').find('[name="meal_image"],[name="imageContent"],[name="notes"]').val('');
            $('form.create-form').trigger('redraw');
        });

        $('#build-suggested-meal #remove-img').on('click', function(){
            $('.s-meal-img-preview').attr('src', $('.s-meal-img-preview').data('src'));
            $('form.create-form [name="meal_image"], form.create-form [name="imageContent"]').val('');
            $(this).hide();
        });
        /**
         * add food, food name typeadhead
         */
        var _food_name_search = $('form.add-form select[name="food_search"]');
        _food_name_search
            .select2({
                multiple: true,
                dropdownParent: _food_name_search.parent(),
                minimumInputLength: 2,
                maximumSelectionLength: 1,
                maximumInputLength: 30,
                ajax: {
                    url: _food_name_search.data('src'),
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term
                        };
                    },
                    processResults: function (data, params) {
                        // for single search result, API results a object instead of array
                        // handling that
                        if (data.food != undefined && data.food.food_id != undefined) {
                            data.food = [data.food];
                        }
                        return {
                            results: $.map(data.food ? data.food : [], function (v) {
                                v.id = v.food_id;
                                return v;
                            })
                        };
                    },
                    cache: true
                },
                escapeMarkup: function (markup) {
                    return markup;
                },

                templateResult: function (food) {
                    if (food.loading) return food.text;

                    var additional = '';
                    if (food.food_type == "Brand" && food.brand_name != undefined) {
                        additional = " (" + food.brand_name + ")";
                    }
                    var markup =
                        "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__title'>" + food.food_name + additional + "</div>" +
                        "<div>" +
                        "<small> " + (food.food_description ? food.food_description : '') + "</small>" +
                        "</div>" +
                        "</div>";

                    return markup;
                },

                templateSelection: function (food) {
                    if (food.food_id) {
                        $('form.add-form [name="food_id"]').val(food.food_id).trigger('change');
                    }
                    return food.food_name;
                }
            })
            .on('select2:unselect', function () {
                if ($.trim($(this).val()) == "") {
                    $('form.add-form').trigger('reset');
                }
            });

        /**
         * Build units Dropdown from data source
         *
         * @param data
         * @param choose
         */
        var build_units_list = function (data, choose, qty) {
            var _units_dd = $('form.add-form [name="unit"]');
            _units_dd.html('');
            if (data.length) {
                var selected_serving = {};
                $.each(data, function (k, v) {
                    var option = $('<option />', {
                        val: v.serving_unit,
                        text: v.serving_unit
                    });
                    if ((choose == undefined && k == 0) || choose == v.serving_unit) {
                        selected_serving = v;
                        option.attr('selected', 'selected');
                    }
                    _units_dd.append(option);
                });

                _units_dd.removeAttr('disabled');
                if (qty == undefined) {
                    qty = selected_serving.serving_qty;
                }
                qty = parseFloat(qty);
                $('form.add-form [name="serving"]').val(qty).removeAttr('disabled');
                $('[data-is="serving"]').text(qty.toFixed(1));
                $('[data-is="unit"]').text(selected_serving.serving_unit);
                $('[data-is="fat"]').text((selected_serving.fat * qty).toFixed(1));
                $('[data-is="carb"]').text((selected_serving.carb * qty).toFixed(1));
                $('[data-is="protein"]').text((selected_serving.protein * qty).toFixed(1));
                $('[data-is="calories"]').text((selected_serving.calories * qty).toFixed(1));
                if ($('.serving-units').is(':hidden')) {
                    $('.serving-units').slideDown();
                }
            }

        };

        /**
         * Fetch Food Details when a food is selected in typeahead
         */
        $('form.add-form [name="food_id"]').on('change', function () {
            var _this = $(this);
            var food_id = $.trim(_this.val());
            var last_food_id = _this.data('last');
            $(this).data('last', food_id);
            if (food_id != "" && food_id != last_food_id) {
                var src = _this.data('src');
                $.HS.call_jquery_ajax(src + '/' + food_id, {}, false, false, function (response) {
                    if (response.food_id != undefined) {
                        var unit_serving = [];
                        // for single serving unit, API results a object instead of array
                        // handling that
                        if (response.servings != undefined && response.servings.serving != undefined) {
                            if (response.servings.serving.serving_id != undefined) {
                                response.servings.serving = [response.servings.serving];
                            }
                            $.each(response.servings.serving, function (k, v) {
                                unit_serving.push({
                                    carb: parseFloat(v.carbohydrate),
                                    fat: parseFloat(v.fat),
                                    protein: parseFloat(v.protein),
                                    calories: parseFloat(v.calories),
                                    serving_qty: parseFloat(v.number_of_units),
                                    serving_unit: v.measurement_description,
                                    serving_id: v.serving_id
                                });
                            });
                            _this.data('cache', unit_serving);
                            build_units_list(unit_serving);
                        }
                        $('form.add-form [name="food_name"]').val(response.food_name);
                        $('form.add-form .add-food-btn').removeAttr('disabled');
                    }
                    else {
                        $('form.add-form').trigger('reset');
                    }
                }, 'GET', true, '#food-loader');
            }
        });

        $('form.add-form [name="unit"]').on('change', function () {
            var _this = $(this);
            var unit_serving = $('form.add-form [name="food_id"]').data('cache');
            if (unit_serving.length) {
                build_units_list(unit_serving, _this.val());
            }
        });

        $('form.add-form [name="serving"]').on('keyup', function () {
            var _this = $(this);
            var qty = $.trim(_this.val());
            $('form.add-form .add-food-btn').removeAttr('disabled');
            if (qty.length && !qty.match(/\.$/) && !isNaN(qty) && !isNaN(parseFloat(qty)) && parseFloat(qty) > 0) {
                var unit_serving = $('form.add-form [name="food_id"]').data('cache');
                if (unit_serving.length) {
                    build_units_list(unit_serving, $('form.add-form [name="unit"]').val(), parseFloat(qty));
                }
            } else {
                $('form.add-form .add-food-btn').attr('disabled', 'disabled');
            }

        });

        $('form.add-form .add-food-btn').on('click', function () {
            var _form = $('form.add-form');
            var _create_form = $('form.create-form');
            var foodSourceId = _form.find('[name="food_id"]').val();
            var meal = {
                foodName: _form.find('[name="food_name"]').val(),
                numberOfServings: _form.find('[name="serving"]').val(),
                servingSizeUnit: _form.find('[name="unit"]').val(),
                protein: $('[data-is="protein"]').text(),
                carbs: $('[data-is="carb"]').text(),
                fats: $('[data-is="fat"]').text(),
                calories: $('[data-is="calories"]').text(),
                foodSource: "fatSecret",
                foodSourceId: foodSourceId
            };
            var food_input = $('<input />', {
                type: 'hidden',
                class: 'food_input',
                name: 'food[' + foodSourceId + ']'
            }).val(JSON.stringify(meal));
            if (_create_form.find('.food_input[name="food[' + foodSourceId + ']"]').length) {
                _create_form.find('.food_input[name="food[' + foodSourceId + ']"]').replaceWith(food_input);
            } else {
                _create_form.append(food_input);
            }
            $('form.create-form').trigger('redraw');
            $('form.add-form').trigger('reset');
        });

        $('form.create-form').on('redraw', function () {
            var _table = $('.food-list');
            var _form = $(this);
            var totalFat = 0,
                totalCarb = 0,
                totalProtein = 0,
                totalCal = 0,
                names = [];
            _table.find('.no-foods').hide();
            if (_form.find('.food_input').length) {
                // iterate each input and build table row for each
                _form.find('.food_input').each(function () {
                    var food = $.HS.getJSON($(this).val());
                    if (food.foodSourceId != undefined) {
                        totalFat += parseFloat(food.fats ? food.fats : 0);
                        totalCarb += parseFloat(food.carbs ? food.carbs : 0);
                        totalProtein += parseFloat(food.protein ? food.protein : 0);
                        totalCal += parseFloat(food.calories ? food.calories : 0);
                        names.push(food.foodName ? food.foodName : '');
                        var html = '<tr data-food-source-id="' + (food.foodSourceId ? food.foodSourceId : '') + '">\
                       <td class="text-left">' + (food.foodName ? food.foodName : '') + '</td>\
                       <td>' + (food.numberOfServings ? food.numberOfServings : '') + '</td>\
                       <td>' + (food.servingSizeUnit ? food.servingSizeUnit : '') + '</td>\
                       <td>' + (food.fats ? food.fats : '') + '</td>\
                       <td>' + (food.carbs ? food.carbs : '') + '</td>\
                       <td>' + (food.protein ? food.protein : '') + '</td>\
                       <td colspan="6">\
                       <span title="Remove" role="button" class="fa fa-remove fa-lg text-red margin-r-5"></span>\
                       <span title="Edit" role="button" class="fa fa-edit fa-lg disabled hide"></span>\
                       </td>\
                       </tr>';
                        if (_table.find('[data-food-source-id="' + food.foodSourceId + '"]').length) {
                            _table.find('[data-food-source-id="' + food.foodSourceId + '"]').replaceWith(html);
                        } else {
                            _table.find('.add-form-row:first').before(html);
                        }
                    }
                });
            } else {
                _table.children('tr:not(.no-foods):not(.add-form-row)').remove();
                _table.find('.no-foods').show();
            }
            $('[data-is="total-cal"]').text(totalCal.toFixed(1));
            $('[data-is="total-fat"]').text(totalFat.toFixed(1));
            $('[data-is="total-carb"]').text(totalCarb.toFixed(1));
            $('[data-is="total-protein"]').text(totalProtein.toFixed(1));
            $('#total_cal').val(totalCal.toFixed(1));
            $('#total_fat').val(totalFat.toFixed(1));
            $('#total_carb').val(totalCarb.toFixed(1));
            $('#total_protein').val(totalProtein.toFixed(1));
            _form.find('[name="meal_name"]:disabled').val(names.join(', '));
        });

        $('.meal-items').on('click', '.fa-remove', function () {
            var id = $(this).parents('tr:first').data('food-source-id');
            $('form.create-form .food_input[name="food[' + id + ']"]').remove();
            $(this).parents('tr:first').remove();
            $('form.create-form').trigger('redraw');
        });

        $('#build-suggested-meal .unlock-btn').on('click', function () {
            $(this).parents('form:first').find('[name="meal_name"]').removeAttr('disabled');
            $(this).hide();
        });

        $('#build-suggested-meal .select-image').on('click', function () {
            $(this).parents('form:first').find('[name="meal_image"]').trigger('click');
        });

        $('#build-suggested-meal [name="meal_image"]').on("change", function () {
            var _img = $(".s-meal-img-preview"),
                file = $(this)[0].files[0],
                fileName = file.name;
            if ($(this).val().length > 0) {
                if (this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var dataURL = e.target.result;
                        _img.attr('src', dataURL);
                        $('#image_content').val($.HS.getRawImageData(dataURL));
                        $('#remove-img').show();
                    };
                    reader.readAsDataURL(this.files[0]);
                }
            }
            /*
             var extractFileExtension = getFileExtension(fileName);
             isAllowUploading = true;
             if(validFileExtensions.indexOf(extractFileExtension) == -1){
             isAllowUploading = false;
             console.log("Only image files uploading supported");
             }*/
        });

        var validate_create_form = function (_form) {
            var _placeholder = $('#sm-alert-placeholder').html('');
            if(_form.find('.food_input').length == 0){
                $.HS.build_notification(_placeholder, 'Please add some foods first.', 'danger');
                return false;
            }
            else if($.trim(_form.find('[name="meal_name"]').val()).length == 0){
                $.HS.build_notification(_placeholder, ' The meal name field is required.', 'danger');
                return false;
            }

            return true;
        };

        $('#create-suggested').on('click', function () {
            var _this = $(this),
                _form = $('form.create-form');
            if (_this.hasClass('clicked') || !validate_create_form(_form)) {
                return;
            }
            _form.find('[name="meal_name"]').removeAttr('disabled');
            _this.addClass('clicked');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                _this.removeClass('clicked');
                var _placeholder = $('#sm-alert-placeholder');
                $('.unlock-btn:visible').next('[name="meal_name"]').attr('disabled','disabled');
                if (response.success != undefined) {
                    $.HS.build_notification(_placeholder, response.success, 'success');
                    _this.parents('.modal:first').modal('hide');
                    $('.wmr-message .create-sm-msg').html($('<b/>').text('Please choose a Suggested Meal'));
                    $('.wmr-message .toggle-hide').removeClass('hide');
                    $('.sm-picker').attr('data-target', '#choose-sm-modal');
                    $('form#load-sm.init-done').removeClass('init-done');
                    $('#choose-sm-modal').modal({
                        keyboard: false,
                        backdrop: 'static',
                        show: true
                    });
                }
                else if (response.validation_error != undefined) {
                    $.HS.build_notification(_placeholder, response.validation_error.join(''), 'danger');
                }
                else if (response.error != undefined) {
                    $.HS.build_notification(_placeholder, response.error, 'danger');
                } else {
                    $.HS.build_notification(_placeholder, $.HS.common_error_message, 'danger');
                }
            }, _form.attr('method'), true, '#hs-create-s-loader');
        });
    }
});
/**
 * Created by rpatidar on 31/08/2017.
 */
jQuery(document).ready(function ($) {

        /**
     *  group-schedule-session  List Datatable Function
     */
  var group_schedule_tab =  $('#group_schedule_session').DataTable({
        processing: false,
        serverSide: false,
        pageLength: 50,
        bLengthChange: false,
        "language": {
            "emptyTable": "No Session exist .",
            "loadingRecords": "Please wait - loading..."
        },
        aoColumns: [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false },

        ],
        "ajax": {
                    "url": $('#group_schedule_session').data('action'),
                    "type": "GET",
                    "data": function (data)
                    {
                        data.group_id      = $("#group_id").val();

                    }

        },
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0,
        },{ className: "dt-center", "targets": [5,4,8]  }
        ],

    });





    /**
     * group-schedule-session-attendance  List Datatable Function
     */
    var group_schedule_session_attendance_tab =  $('#group_schedule_session_attendance').DataTable({
        processing: false,
        serverSide: false,
        pageLength: 50,
        bLengthChange: false,
        "language": {
            "emptyTable": "No Member exist .",
            "loadingRecords": "Please wait - loading..."
        },
        aoColumns: [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false },
            { "bSortable": false },


        ],
        "ajax": {
            "url": $('#group_schedule_session_attendance').data('action'),
            "type": "GET",
            "data": function (data)
            {
                data.group_session_id  = $("#group_session_id").val();

            }

        },
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0,
        },{ className: "dt-center", "targets": [5]  }
        ],

    });



    $("#modal_schedule-session").on('shown.bs.modal', function () {
        $('#schedule_session_form').get(0).reset();
        $("#schedule_session_form").validate().resetForm();
        $("#groupSessionsTopicNameList1").select2({
            width: null,
        });
        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(".schedule_form_datetime").datepicker({
            autoclose: true,
            isRTL: App.isRTL(),
            //pickerPosition: (App.isRTL() ? "top-right" : "top-left"),
            orientation: "top",
            format: 'mm/dd/yyyy',
            todayHighlight: true

        });

            $('#schedule_time').replaceWith('<input type="text" class="form-control" id="schedule_time" name="time" aria-required="true" aria-describedby="schedule_time-error" aria-invalid="false">');
            $("#schedule_time").timepicker({
            defaultTime:'current'
        });



           });



    $("#modal_update-session").on('shown.bs.modal', function (e) {
        $(".groupSessionsTopicNameList").select2({
            width: null,
        });
        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(".schedule_update_form_datetime").datepicker({
            autoclose: true,
            isRTL: App.isRTL(),
            orientation: "top",
            todayHighlight: true
        });
        $('#schedule_session_update_form').get(0).reset();
        $("#schedule_session_update_form").validate().resetForm();

        var _form = $('#schedule_session_update_form');
        _form.find('[name="group_session_id"]').val('');
        var id = $(e.relatedTarget).data('id');
        var session_name = $(e.relatedTarget).data('name');
        _form.find('[name="session"]').val(session_name);

        if (id)
        {
            _form.find('[name="group_session_id"]').val(id);
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {

                window.setTimeout(function () {
                    App.stopPageLoading();
                }, 1000);
                if (response.success != undefined) {

                 if(response.group_session_detail!='')
                 {

                     _form.find('[name="date"]').val(response.group_session_detail.start_date);
                     _form.find('[name="location"]').val(response.group_session_detail.location);
                     $('#update_select_date').datepicker('update',response.group_session_detail.start_date);
                     $('#update_content_date').datepicker('update',response.group_session_detail.unlock_content_date);
                     $("#schedule_update_time").replaceWith('<input type="text" class="form-control" id="schedule_update_time" name="time" readonly="" aria-required="true" aria-invalid="false" aria-describedby="schedule_update_time-error">');
                     $("#schedule_update_time").timepicker({
                     defaultTime: response.group_session_detail.time
                     });

                 }

                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Update session");
                }
            },'GET', true, '#hs-loader');

        }


    });


    /* modal unlock content for all */

    $("#modal_unlock_content_for_all").on('shown.bs.modal', function (e) {
        $('#modal_unlock_content_for_all_form').get(0).reset();
        $("#modal_unlock_content_for_all_form").validate().resetForm();

        var _form = $('#modal_unlock_content_for_all_form');
        var group_session_id =  _form.find('[name="group_session_id"]').val();


        if (group_session_id)
        {

            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {

                window.setTimeout(function () {
                    App.stopPageLoading();
                }, 1000);
                if (response.success != undefined) {

                    if(response.group_session_detail!='')
                    {

                        $("#unlock_content_date_for_all").replaceWith('<input type="text" size="16" name="unlock_content_date" id="unlock_content_date_for_all" readonly class="form-control" aria-required="true">');
                        if(response.session_attendace.unlock_content_date!="") {

                            $(".unlock_content_date_for_all").datepicker({
                                format: 'mm/dd/yyyy',
                                autoclose: true
                            }).datepicker("update", response.session_attendace.unlock_content_date);

                        }
                        else
                        {

                            $(".unlock_content_date_for_all").datepicker({
                                format: 'mm/dd/yyyy',
                                autoclose: true
                            }).datepicker("update",new Date());

                        }

                        $("#unlock_content_time_for_all").replaceWith('<input type="text" class="form-control" id="unlock_content_time_for_all" name="time" readonly>');
                        if(response.session_attendace.time!="")
                        {

                            $("#unlock_content_time_for_all").timepicker({
                                defaultTime: response.session_attendace.time
                            });
                        }
                        else
                        {
                            $("#unlock_content_time_for_all").timepicker({
                                defaultTime: '8:30 AM'
                            });

                        }



                    }

                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Update session");
                }
            },'GET', true, '#hs-loader');

        }


    });


    /*   */



    /* modal add/edit  attandance */

    $("#modal_add-edit-attendance").on('shown.bs.modal', function (e) {

        $('#attendance_title').text('');
        $('#modal_add_edit_attendance_form').get(0).reset();
        $("#modal_add_edit_attendance_form").validate().resetForm();
        $(".for_makeup").css("display","block");
        var _form = $('#modal_add_edit_attendance_form');
        _form.find('[name="session_attendance_id"]').val('');
        _form.find('[name="attendance_type"]').prop("checked", false);
        _form.find('[name="tracking_type"]').prop("checked", false);
         var session_attendance_id = $(e.relatedTarget).data('session-attendance-id');
         var unlock_content_date = $("#unlock_content_date").val();
        var unlock_content_time = $("#unlock_content_time").val();

        if (session_attendance_id)
        {
            _form.find('[name="session_attendance_id"]').val(session_attendance_id);

            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {

                window.setTimeout(function () {
                    App.stopPageLoading();
                }, 1000);
                if (response.success != undefined) {

                    if(response.session_attendace!= undefined)
                    {

                        _form.find('[name="group_session_id"]').val(response.session_attendace.group_session_id);
                        /*__form.find('[name="member_id"]').val(response.session_attendace.group_member_id);
                        _form.find('[name="session_attandance_id"]').val(response.session_attendace.session_attendance_id);*/
                         if(response.session_attendace.attendance_type!="" && response.session_attendace.attendance_type!= null) {
                             _form.find('[name="attendance_type"][value="' + response.session_attendace.attendance_type + '"]').prop("checked", true);
                           if(response.session_attendace.attendance_type== "MakeUp")
                                 $(".for_makeup").css("display","none");

                         }
                         else
                             _form.find('[name="attendance_type"][value="InPerson"]').prop("checked", true);

                        if(response.session_attendace.is_paper_tracker!="")
                             _form.find('[name="tracking_type"][value="'+response.session_attendace.is_paper_tracker+'"]').prop("checked", true);
                        else
                             _form.find('[name="tracking_type"][value="'+response.session_attendace.is_paper_tracker+'"]').prop("checked", true);

                        $("#attendance_update_content_date").replaceWith('<input type="text" size="16" name="unlock_content_date" id="attendance_update_content_date" readonly="" class="form-control" aria-required="true " aria-invalid="false">');
                        if(response.session_attendace.unlock_content_date!="") {

                            $(".attendance_content_date").datepicker({
                                format: 'mm/dd/yyyy',
                                autoclose: true
                            }).datepicker("update", response.session_attendace.unlock_content_date);

                            $("#attendance_title").text("Edit Attendance");

                        }
                        else
                        {

                            $(".attendance_content_date").datepicker({
                                format: 'mm/dd/yyyy',
                                autoclose: true
                            }).datepicker("update",unlock_content_date);

                            $("#attendance_title").text("Add Attendance");
                        }



                        if(response.session_attendace.days_tracked!="")
                              _form.find('[name="days_logged"]').val(response.session_attendace.days_tracked);
                        if(response.session_attendace.notes!="")
                            _form.find('[name="notes"]').val(response.session_attendace.notes);

                        $("#attendance_time").replaceWith('<input type="text" class="form-control" id="attendance_time" name="time" readonly="" aria-required="true" aria-invalid="false" >');
                        if(response.session_attendace.time!="")
                        {

                            $("#attendance_time").timepicker({
                                defaultTime: response.session_attendace.time
                            });
                        }
                        else
                        {
                            $("#attendance_time").timepicker({
                                defaultTime: unlock_content_time
                            });

                        }

                    }

                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Update session");
                }
            },'GET', true, '#hs-loader');

        }






    });



    /* */


   /* For add edit attendance form disable and enable */

    $('#modal_add_edit_attendance_form').find('[name="attendance_type"]').on('click',function(){
        if($(this).val() == "MakeUp")
         $(".for_makeup").css("display","none");
        else
            $(".for_makeup").css("display","block");

    });


   /*  */


/* modal_add-edit-activity */

    $("#modal_add-edit-activity").on('shown.bs.modal', function (e) {
        $('#activity_title').text('');
        $('#modal_add_edit_activity_form').get(0).reset();
        $("#modal_add_edit_activity_form").validate().resetForm();

        var _form = $('#modal_add_edit_activity_form');
        _form.find('[name="session_attendance_id"]').val('');

        var session_attendance_id = $(e.relatedTarget).data('session-attendance-id');
        var patient_id = $(e.relatedTarget).data('patient-id');
        var session_date = $("#session_date").val();

        if (session_attendance_id)
        {
            _form.find('[name="session_attendance_id"]').val(session_attendance_id);
            _form.find('[name="patient_id"]').val(patient_id);
            _form.find('[name="session_date"]').val(session_date);

            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {

                window.setTimeout(function () {
                    App.stopPageLoading();
                }, 1000);
                if (response.success != undefined) {

                    if(response.session_attendace!= undefined)
                    {


                        if(response.session_attendace.activity_minutes!="") {
                            _form.find('[name="minutes"]').val(response.session_attendace.activity_minutes);
                            $("#activity_title").text('Edit Activity');
                        }
                        else
                        {
                            $("#activity_title").text('Add Activity');
                        }

                    }

                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Update session");
                }
            },'GET', true, '#hs-loader');

        }


    });



/*   */


    /* modal_add-edit-weight */

    $("#modal_add-edit-weight").on('shown.bs.modal', function (e) {

        $('#weight_title').text('');
        $('#modal_add_edit_weight_form').get(0).reset();
        $("#modal_add_edit_weight_form").validate().resetForm();

        var _form = $('#modal_add_edit_weight_form');
        _form.find('[name="session_attendance_id"]').val('');
        _form.find('[name="patient_id"]').val('');
        var session_attendance_id = $(e.relatedTarget).data('session-attendance-id');
        var patient_id = $(e.relatedTarget).data('patient-id');

        if (session_attendance_id && patient_id)
        {
            _form.find('[name="session_attendance_id"]').val(session_attendance_id);
            _form.find('[name="patient_id"]').val(patient_id);
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {

                window.setTimeout(function () {
                    App.stopPageLoading();
                }, 1000);
                if (response.success != undefined) {

                    if(response.session_attendace!= undefined)
                    {
                        if(response.session_attendace.weight!="") {
                            _form.find('[name="weight"]').val(response.session_attendace.weight);
                            $("#weight_title").text('Edit Weight');
                        }
                        else
                        {
                            $("#weight_title").text('Add Weight');
                        }
                    }

                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Update session");
                }
            },'GET', true, '#hs-loader');

        }


    });



    /*   */

    /* modal mark as completed */

    $("#modal_mark-as-completed").on('shown.bs.modal', function (e) {


        $('#modal_mark_as_completed_form').get(0).reset();
        $("#modal_mark_as_completed_form").validate().resetForm();

        var _form = $('#modal_mark_as_completed_form');
        _form.find('[name="session_attendance_id"]').val('');
        _form.find('[name="patient_id"]').val('');
        var session_attendance_id = $(e.relatedTarget).data('session-attendance-id');
        var patient_id = $(e.relatedTarget).data('patient-id');

        if (session_attendance_id && patient_id)
        {
            _form.find('[name="session_attendance_id"]').val(session_attendance_id);
            _form.find('[name="patient_id"]').val(patient_id);
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {

                window.setTimeout(function () {
                    App.stopPageLoading();
                }, 1000);
                if (response.success != undefined) {

                    if(response.session_attendace!= undefined)
                    {
                        if(response.session_attendace.weight!="")
                            _form.find('[name="weight"]').val(response.session_attendace.weight);

                        if(response.session_attendace.notes!="")
                            _form.find('[name="notes"]').val(response.session_attendace.notes);
                    }

                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Update session");
                }
            },'GET', true, '#hs-loader');

        }


    });



    /*   */



    $('[data-toggle="tooltip"]').tooltip();

    $("#schedule_session_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            location: {
                required: true
            },
            session: {
                required: true
            },

            date: {
                required: true
            },
            time: {
                required: true
            },
            unlock_content_date: {
                required: true
            }

        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {

            var _this = $(this);
            var _form = $('#schedule_session_form');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {


                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    // is list page
                    toastr["success"]('Schedule a session added successfully', 'Schedule A Session');
                    $('#schedule_session_form').get(0).reset();


                    if(response.session_uuid_array !="undefined")
                    {
                        $.each(response.session_uuid_array, function (key, value) {

                            $("#groupSessionsTopicNameList1 option[value='"+value+"']").remove();

                        });

                    }

                    $("#modal_schedule-session").modal('hide');
                    group_schedule_tab.ajax.reload();


                }


                else if (response.error != undefined) {
                    toastr["error"](response.error, 'Change Password');
                }
            }, _form.attr('method'), true, '#hs-loader');

        }
    });





    $("#schedule_session_update_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            location: {
                required: true
            },

            date: {
                required: true
            },
            time: {
                required: true
            },
            unlock_content_date: {
                required: true
            }

        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {

            var _this = $(this);
            var _form = $('#schedule_session_update_form');
            $.HS.call_jquery_ajax(_form.attr('update_group_session_detail'), _form.serialize(), false, false, function (response) {


                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    // is list page
                    toastr["success"](response.success, 'Schedule A Session');
                    $('#schedule_session_form').get(0).reset();


                    if(response.session_uuid_array !="undefined")
                    {
                        $.each(response.session_uuid_array, function (key, value) {

                            $("#groupSessionsTopicNameList1 option[value='"+value+"']").remove();

                        });

                    }

                    $("#modal_update-session").modal('hide');
                    group_schedule_tab.ajax.reload();


                }


                else if (response.error != undefined) {
                    toastr["error"](response.error, 'Schedule A Session');
                }
            }, 'POST', true, '#hs-loader');

        }
    });



    $("#modal_add_edit_attendance_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
        },
        rules: {

            unlock_content_date: {
                required: true
            }

        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {

            var _this = $(this);
            var _form = $('#modal_add_edit_attendance_form');
            $.HS.call_jquery_ajax(_form.attr('update_session_attendance'), _form.serialize(), false, false, function (response) {


                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    // is list page
                    group_schedule_session_attendance_tab.ajax.reload();
                    $("#modal_add-edit-attendance").modal('hide');
                    if(response.count_for_unlock_content != 0)
                    $("#unlock_content_for_all_btn").removeClass('hide');
                    else
                    $("#unlock_content_for_all_btn").addClass('hide');
                    toastr["success"](response.success, 'Session Attendance');



                }


                else if (response.error != undefined) {
                    toastr["error"](response.error, 'Session Attendance');
                }
            }, 'POST', true, '#hs-loader');

        }
    });

    $.validator.addMethod('positiveNumber',
        function (value) {
            return Number(value) > 0;
        }, 'Enter a positive number.');

    $("#modal_add_edit_activity_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
        },
        rules: {

            minutes: {
                required: true,
                number: true,
                positiveNumber:true
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {

            var _this = $(this);
            var _form = $('#modal_add_edit_activity_form');
            $.HS.call_jquery_ajax(_form.attr('update_session_attendance_activity'), _form.serialize(), false, false, function (response) {


                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    // is list page
                    group_schedule_session_attendance_tab.ajax.reload(null, false);
                    $("#modal_add-edit-activity").modal('hide');
                    toastr["success"](response.success, 'Session Attendance');



                }


                else if (response.error != undefined) {
                    toastr["error"](response.error, 'Session Attendance');
                }
            }, 'POST', true, '#hs-loader');

        }
    });


    $("#modal_add_edit_weight_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
        },
        rules: {

            weight: {
                required: true,
                number: true,
                positiveNumber:true
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {

            var _this = $(this);
            var _form = $('#modal_add_edit_weight_form');
            $.HS.call_jquery_ajax(_form.attr('update_session_attendance_weight'), _form.serialize(), false, false, function (response) {

                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    // is list page
                    group_schedule_session_attendance_tab.ajax.reload(null, false);
                    $("#modal_add-edit-weight").modal('hide');
                    toastr["success"](response.success, 'Session Attendance');



                }


                else if (response.error != undefined) {
                    toastr["error"](response.error, 'Session Attendance');
                }
            }, 'POST', true, '#hs-loader');

        }
    });





    /* modal for unlock content form */

    $("#modal_unlock_content_for_all_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
        },
        rules: {

            unlock_content_date: {
                required: true

           },

            time: {
                required: true

            }

        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {

            var _this = $(this);
            var _form = $('#modal_unlock_content_for_all_form');
            $.HS.call_jquery_ajax(_form.attr('update_unlock_content_get_data'), _form.serialize(), false, false, function (response) {

                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    // is list page
                    group_schedule_session_attendance_tab.ajax.reload(null, false);
                    $("#modal_unlock_content_for_all").modal('hide');
                    toastr["success"](response.success, 'Session Attendance');
                }

                else if (response.error != undefined) {
                    toastr["error"](response.error, 'Session Attendance');
                }
            }, 'POST', true, '#hs-loader');

        }
    });


    /*   */



    /* modal for mark as completed form */

    $("#modal_mark_as_completed_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
        },
        rules: {

            weight: {
                required: true

            },

            notes: {
                required: true

            }

        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {

            var _this = $(this);
            var _form = $('#modal_mark_as_completed_form');
            $.HS.call_jquery_ajax(_form.attr('mark_as_complete'), _form.serialize(), false, false, function (response) {

                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    // is list page
                    group_schedule_session_attendance_tab.ajax.reload();
                    $("#modal_mark-as-completed").modal('hide');
                    toastr["success"](response.success, 'Session Attendance');



                }


                else if (response.error != undefined) {
                    toastr["error"](response.error, 'Session Attendance');
                }
            }, 'POST', true, '#hs-loader');

        }
    });


    /*   */



});



$( document ).ready(function() {

    $('[data-toggle="tooltip"]').tooltip();

    $('.date-picker').datepicker({
        autoclose: true,
        orientation: "bottom"


    });
    $(".form_datetime").timepicker({
        autoclose: true,
        //showSeconds: true,
        minuteStep: 1
    });

    $("#ddDietaryRestrictions").select2({
        width: null,
    });
    $("#ddMealPreparer").select2({
        width: null,
    });
    $("#ddDiabetesSupport").select2({
        width: null,
    });
    $("#habits").select2({
        width: null,
    });


    /**
     * Mrn  form validation
     */
    $("#form_member_profile").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            first_name: {

                required: true
            },
           last_name: {

                required: true
            },
            email: {

                required: true,
                email:true
            },
            phone: {

                required: true
             },
            gender: {

                required: true
            },
            mrn: {
                required:true
            },
            group_display_name: {
                    required:true
                },
            height_feet: {
                required:true
            },
            height_inch: {
                required:true
            },
            activity: {
                required:true
            },
            starting_weight: {
                required:true
            },
            goal_steps: {
                required:true
            },
            calorie_budget: {
                required:true,
                min:1200
            },
            temp_password:{
                minlength:6
            },
            dob:{
                required:true
            }




        },

        errorPlacement: function (error, element) { // render error placement for each input type
            // alert("asa");
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _form = $('#form_member_profile');

            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
               // $('#modal_mrn').modal('hide');
                window.setTimeout(function() { App.stopPageLoading();}, 1000);

                if (response.success != undefined) {

                    toastr["success"](response.success, "Profile");
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Profile");
                }
            }, _form.attr('method'), true, '#hs-loader');
        }
    });



    $("#form_member_diabetes_info").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            diagnosis: {

                required: true
            },
            year_of_diagnosis: {

                required: true
            },
            time_since_last_diabetes_education: {

                required: true

            },
            "diabetes_support[]": {

                required: true

            },
            being_treated_depression: {
                required:true

            }




        },

        errorPlacement: function (error, element) { // render error placement for each input type
            // alert("asa");
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
           // var cont3 = $(element).parents('.mt-radio-inline');
            if (element.attr("type") == "radio") {
                error.insertAfter($(element).parents('.mt-radio-inline'));
            }
            else {
                if (cont) {
                    cont.after(error);
                }
                if (cont2) {
                    $(element).after(error);
                }
                if (cont&&cont2){
                    cont.after(error);
                }
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _form = $('#form_member_diabetes_info');

            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                // $('#modal_mrn').modal('hide');
                window.setTimeout(function() { App.stopPageLoading();}, 1000);

                if (response.success != undefined) {

                    toastr["success"](response.success, "Diabetes Information");
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Diabetes Information");
                }
            }, _form.attr('method'), true, '#hs-loader');
        }
    });


    /* validate for reminder */
    $("#form_member_reminder").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            breakfast: {

                required: true
            },
            lunch: {

                required: true
            },
           snack: {

                required: true

            },
            dinner: {

                required: true

            }




        },

        errorPlacement: function (error, element) { // render error placement for each input type
            // alert("asa");
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _form = $('#form_member_reminder');

            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                // $('#modal_mrn').modal('hide');
                window.setTimeout(function() { App.stopPageLoading();}, 1000);

                if (response.success != undefined) {

                    toastr["success"](response.success, "Reminder");
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Reminder");
                }
            }, _form.attr('method'), true, '#hs-loader');
        }
    });


    /* */

    /* */
   /* $("#dob").datepicker({
        autoclose: true,
        orientation: "bottom"

    });*/


    /* for toggle */


    $('.mainmeals').parent().find('span').on('click', function (event, state) {

            $('.ckmeals').prop("checked",$('.mainmeals'). prop("checked"));
            $('.ckmeals').bootstrapSwitch('state', $('.mainmeals'). prop("checked"));
    });

    $('.ckmeals').parent().find('span').on('click', function (event, state) {


        if($(this).parent().find('.ckmeals'). prop("checked") == true) {
            $('.mainmeals').prop("checked",true);
            $('.mainmeals').bootstrapSwitch('state', true);
        }
        else
        {
            $('.ckmeals').each(function(){

                if($(this).prop("checked")==true){

                    $('.mainmeals').prop("checked",true);
                    $('.mainmeals').bootstrapSwitch('state', true);
                    return false;
                }
                $('.mainmeals').prop("checked",false);
                $('.mainmeals').bootstrapSwitch('state', false);
            });
        }


    });

    $("#frequency").change(function(){

        if($(this).val() == "Once/Month")
        {

            $("#d_month").removeClass("hide");
            $("#d_week").addClass("hide");
        }

        else {

            $("#d_week").removeClass("hide");
            $("#d_month").addClass("hide");

        }

    });




});
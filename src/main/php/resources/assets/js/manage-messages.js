/**
 * Created by rpatidar on 31/08/2017.
 */
jQuery(document).ready(function ($) {


    /**
     * Events for Manage Message page
     */
    if ($('body.manage-message').length) {
        // is manage Message page
        var _table = $('.hs-table');

        var table = _table.DataTable({
            processing: true,
            lengthChange: false,
            bAutoWidth: false,
            serverSide: true,
            order: [[4, 'desc']],
            pageLength: $('.parameters [name="list_datatable_row_per_page"]').val(),
            ajax: $.trim(_table.data('src')),
            fnServerParams: function (aoData) {
                aoData['only_me'] = $('input[name="only_me"]:checked').val();
            },
            columns: [
                null,
                null,
                null,
                null,
                {orderData: 6, searchable: false},
                {orderable: false, searchable: false},
                {visible: false, searchable: false}
            ],
            drawCallback: function (settings) {
                $.HS.refresh_dates();
            },
            preDrawCallback: function (oSettings) {
                $('.hs-dt-filter').removeClass('no-margin');
            },
            initComplete: function (settings, json) {
                $('.dataTables_filter').prepend(
                    $('<div/>')
                        .addClass('checkbox')
                        .append($('<label/>')
                        .html('Only created by me <input type="checkbox" name="only_me" value="me">')
                    )
                );
            }
        });

        /**
         * Refresh Data Table when clicked on `only_me`
         */
        $('.table-responsive').on('click', 'input[name="only_me"]', function(){
            table.draw();
        });
    }
    else if($('body.all-scheduled-messages').length){
        var _table = $('.hs-table');

        var table = _table.DataTable({
            processing: true,
            lengthChange: false,
            bAutoWidth: false,
            serverSide: true,
            order: [[0, 'desc']],
            pageLength: $('.parameters [name="list_datatable_row_per_page"]').val(),
            ajax: $.trim(_table.data('src')),
            fnServerParams: function (aoData) {
                aoData['include_sent'] = $('input[name="include_sent"]:checked').val();
            },
            columns: [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                {orderable: false, searchable: false}
            ],
            drawCallback: function (settings) {
                $.HS.refresh_dates();
            },
            preDrawCallback: function (oSettings) {
            },
            initComplete: function (settings, json) {
                $('.dataTables_filter').prepend(
                    $('<div/>')
                        .addClass('checkbox')
                        .append($('<label/>')
                            .html('Include Sent Messages <input type="checkbox" name="include_sent" value="yes">')
                    )
                );
            }
        });

        /**
         * Refresh Data Table when clicked on `include_sent`
         */
        $('.table-responsive').on('click', 'input[name="include_sent"]', function(){
            table.draw();
        });

        /**
         * Delete Scheduled Message Handler
         */
        $('#remove-scheduled-msg').on('click', function(){
            var _this = $(this);
            var _form = $('#delete-msg');
            if (_this.hasClass('clicked')) {
                return;
            }
            _this.addClass('clicked');
            _this.parents('.modal:first').modal('hide');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                _this.removeClass('clicked');
                var _placeholder = $('#alert-placeholder');
                if (response.success != undefined) {
                    $.HS.build_notification(_placeholder, response.success, 'success');
                    if($('table.hs-table').length){
                        // is list page
                        $('table.hs-table').DataTable().draw();
                    }
                }
                else if (response.error != undefined) {
                    $.HS.build_notification(_placeholder, response.error, 'danger');
                } else {
                    $.HS.build_notification(_placeholder, $.HS.common_error_message, 'danger');
                }
            }, _form.attr('method'), true, '#hs-loader');
        });
    }
    /**
     * create / edit message view events
     */
    if($('body.create-message').length || $('.modal#create-wmr-message').length){

        /**
         * Validate Create message form
         * @param _form
         * @returns {boolean}
         */
        var validate_msg_form = function (_form) {
            var _placeholder = $('#form-alert-placeholder').html('');
            if($.trim(_form.find('[name="title"]').val()).length == 0){
                $.HS.build_notification(_placeholder, 'The title field is required.', 'danger');
                return false;
            }
            else if($.trim(_form.find('[name="message"]').val()).length == 0){
                $.HS.build_notification(_placeholder, ' The message field is required.', 'danger');
                return false;
            }
            return true;
        };

        /**
         * Create / edit message handler
         */
        $('#msg-form').on('submit', function(){
            var _this = $(this);
            var _form = _this;
            var in_modal = _form.parents('.modal:first').length;
            if (_this.hasClass('clicked') || !validate_msg_form(_this)) {
                return;
            }
            _this.addClass('clicked');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                _this.removeClass('clicked');
                var _placeholder = $('#form-alert-placeholder');
                if (response.success != undefined) {
                    $.HS.build_notification(_placeholder, response.success, 'success');
                    _form.find('.btn[type="submit"]').attr('disabled','disabled');
                    if(in_modal){
                        _form.parents('.modal:first').modal('hide');
                        if($('#modify-message').length){
                            $('#modify-message').modal('show');
                        }
                    }
                }
                else if (response.validation_error != undefined && response.validation_error.length) {
                    $.HS.build_notification(_placeholder, response.validation_error.join(''), 'danger');
                }
                else if (response.error != undefined) {
                    $.HS.build_notification(_placeholder, response.error, 'danger');
                } else {
                    $.HS.build_notification(_placeholder, $.HS.common_error_message, 'danger');
                }
            }, _form.attr('method'), true, in_modal ? '#hs-create-m-loader' :'#hs-loader');
            return false;
        });

        /**
         * Reset create message Modal Before showing
         */
        $('#create-wmr-message').on('show.bs.modal', function (e) {
            var _modal = $(this);
            _modal.find('#form-alert-placeholder').html('');
            _modal.find('[name="title"]').val('');
            _modal.find('textarea').val('');
            _modal.find('.btn[type="submit"]').removeAttr('disabled');
        });

    }
});
$( document ).ready(function() {

    if(DATE_DIFFERENCE!= undefined) {

        if (DATE_DIFFERENCE >= 45 && DATE_DIFFERENCE < 60) {
            $('#modal_password_expiration').modal('show');
        }
    }



    setTimeout(function(){
        $(".select_facility_admin_add").select2({

        });
    }, 300);


     $("#click_change_passowrd").click(function(){
         $('#modal_password_expiration').modal('hide');
         $('#change_password').modal('show');

     });


    $("button[data-select2-open]").click(function() {
        $("#" + $(this).data("select2-open_add")).select2("open");
    });
    $(".select2, .select2-multiple").select2({

    });

    /**
     * Logout Facility From Home Page Icon
     */
    $('.icon-logout').on('click', function() {
        var _this = $(this);
        window.location.href = _this.data('action');
    });

    var table = $('#sample_2');
    $('#sample_2').dataTable({
    });
    $("#modal_addFacilityAdmin").on('show.bs.modal', function () {
        $(".select_facility_admin").select2({
            width: null
        });
        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

    });

    $("#modal_newFacilityAdmin").on('show.bs.modal', function () {
        $('#create-new-facility-admin').get(0).reset();
        $("#create-new-facility-admin").validate().resetForm();
        $(".select_facility_admin_new").select2({
            width: null,
            allowClear: false
        });
        $("#select_facility_list").select2('val','ALL');
        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

    });


    $("#modal_EditFacilityAdmin").on('show.bs.modal', function (e) {

        $("#edit-facility-admin").validate().resetForm();
        $(".assigned-facility-list > .row").children().remove();

        $(".select_facility_admin_new").select2({
            width: null,
            allowClear: false
        });
        $("#select_facility_list").select2('val','ALL');
         $("#select_facility_list>option").removeAttr('disabled');
        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });
       var provider_id= $(e.relatedTarget).data('provider_id');

        var _form = $('#edit-facility-admin');

         _form.find('[name="provider_id"]').val('');

       if(provider_id) {
            _form.find('[name="provider_id"]').val(provider_id);
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {

                window.setTimeout(function () { App.stopPageLoading(); }, 1000);
                //$('.group-members').children('option').remove();

                if (response.facility_id != undefined)
                {

                    $.each(response.facility_id, function (key, value) {

                    $('#select_facility_list>option[value='+value+']').attr('disabled','disabled');

                    });


                }

                if (response.facility_name != undefined)
                {

                    $.each(response.facility_name, function (key, value) {

                        $(".assigned-facility-list > .row").append( '<li class="col-sm-6">' +value +'</li>');

                    });


                }
                if (response.success != undefined) {

                    _form.find('[name="admin_fname"]').val(response.success.first_name);
                    _form.find('[name="admin_lname"]').val(response.success.last_name);
                    _form.find('[name="admin_phone"]').val(response.success.phone);
                    _form.find('[name="admin_email"]').val(response.success.email);
                    $('#admin_coach_reset_password_btn').attr('data-id',response.success.user_id);
                }

                if (response.error != undefined) {
                    toastr["error"](response.error, "Assign Member to Group");
                }

            }, 'GET', true, '#hs-loader');

        }


    });





    $(".select2, .select2-multiple").select2({
        width: null
    });
    $("button[data-select2-open]").click(function() {
        $("#" + $(this).data("select2-open")).select2("open");
    });


    /**
     * Change facility from drop down nav bar
     */
    // selected_facility = JSON.parse( localStorage.getItem( 'user_facility_' + btoa(USER_ID) ));
    // $('.facility_dd').val(selected_facility);
    // console.log(selected_facility);

    $('.facilityDropdown').on('click', function () {

        var _this = $(this);
       // console.log(_this);
        var post_var = {'facility': _this.data("facility") };
        var facility=_this.data("facility");
        setCookie('active_facility_'+USER_ID, _this.data("facility"));
        $.HS.call_jquery_ajax(_this.data('action'), post_var, false, false, function (response) {
            window.setTimeout(function() { App.stopPageLoading();}, 1000);
            if (response.success != undefined) {
                window.location.reload();

            } else if (response.error != undefined) {

            }
        },
            _this.attr('method'), true, '#hs-loader');

    });



    /**
     * Facility edit form validation
     */

    $.validator.addMethod("alphanumRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9]+$/i.test(value);
    }, "Please enter only letter(s) and/or number(s).");


    $("#form_facilityedit").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        messages: {
            cdc_organization_code:{
                maxlength: "The maximum length of CDC Organization Code is 25 characters.",
            }
        },
        rules: {

            name: {
                minlength: 2,
                required: true
            },
            state: {
                required: true
            },
            city: {
                required: true
            },
            zip: {
                required: true,
                number: true
            },
            address: {
                required: true,
            },
            timezone: {
                required: true
            },
            cdc_organization_code:{
                maxlength: 25,
                alphanumRegex:true
            },
            contact_info: {
                required: true

            },
            contact_person_name: {
                minlength: 2,
                required: true
            },
            admin_fname: {
                required: function (element) {
                    return $('#select_facility_admin_add').val() == null;
                }
            },

            admin_lname: {
                required: function (element) {
                    return $('#select_facility_admin_add').val() == null;
                }
            },

            admin_email: {
                email:true,
                 required: function (element) {
                    return $('#select_facility_admin_add').val() == null;
                }

            },

            admin_phone: {
                required: function (element) {
                    return $('#select_facility_admin_add').val() == null;
                }
            }

        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            form.submit();
        }
    });


    /**
     * On click assign coach button open model
     */
    $('#change_password_modal').on('click', function (e) {
        $('#changepassword').get(0).reset();
        $("#changepassword").validate().resetForm();
    });


    /**
     * Change Password form validation
     */
    $("#changepassword").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            old_password: {
                minlength: 8,
                required: true
            },
            new_password: {
                minlength: 8,
                required: true
            },
            confirm_password: {
                minlength: 8,
                equalTo : "#new_password",
                required: true
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _this = $(this);
            var _form = $('#changepassword');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                _this.removeClass('clicked');
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                        // is list page
                        toastr["success"]('Password updated successfully.', 'Change Password');
                        $('#changepassword').get(0).reset();
                        $("#change_password").modal('hide');
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, 'Change Password');
                }
            }, _form.attr('method'), true, '#hs-loader');
        }
    });





    /**
     * Assign Coach from coach list page validation
     */
    $("#assign_facility_to_facility_admin").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
            "select_facility_admin[]": {
                minlength: jQuery.validator.format("Please select a facility admin"),
            }
        },
        rules: {
            "select_facility_admin[]": {
                required: true,
                minlength: 1,
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _this = $(this);
            var _form = $('#assign_facility_to_facility_admin');
            $('#modal_addFacilityAdmin').modal('hide');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    toastr["success"](response.success, "Add Facility Admin to Facility");
                    window.location.reload();
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Add Facility Admin to Facility");
                }
            }, _form.attr('method'), true, '#hs-loader');
        }
    });



    /**
     * Create new Facility admin for super admin  facility page
     */
    $("#create-new-facility-admin").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {

        },
        rules: {
            "select_facility[]": {
                required: true
            },
            admin_fname:{
                required:true
            },
            admin_lname:{
                required:true
            },
            admin_email:{
                email: true,
                required:true

              },
            admin_phone:{
                required:true
            }

        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _this = $(this);
            var _form = $('#create-new-facility-admin');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    toastr["success"](response.success, "");
                    $('#modal_newFacilityAdmin').modal('hide');

                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "");
                }
            }, _form.attr('method'), true, '#hs-loader');
        }
    });


    /**
     * Create new Facility admin for super admin  facility page
     */
    $("#edit-facility-admin").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {

        },
        rules: {

            admin_fname:{
                required:true
            },
            admin_lname:{
                required:true
            },
            admin_email:{
                email: true,
                required:true

            },
            admin_phone:{
                required:true
            }

        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _this = $(this);
            var _form = $('#edit-facility-admin');
            $.HS.call_jquery_ajax(_form.attr('update'), _form.serialize(), false, false, function (response) {
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    toastr["success"](response.success, "Update Facility Admin Successfully");
                      coach_tab.ajax.reload();

                    $('#modal_EditFacilityAdmin').modal('hide');

                }
                else if (response.error != undefined) {
                    toastr.clear();
                    toastr["error"](response.error, "Facility Admin Email Already Exists");
                }
            }, _form.attr('method'), true, '#hs-loader');
        }
    });
















    /**
     * On click assign Member button open model
     */
    $('.facility_admin').on('click', function (e) {
        var admin_id=$(this).attr('admin_id');
        var facility_id=$(this).attr('facility_id');
        //alert(facility_id);
        if ($('#group-list').val() != '') {
            toastr['info']('<div style="margin-top: 5px;">Are you sure to remove this Admin from that Facility?</div><div><button type="button" class="btn btn-default pull-right" style="margin-top: 10px;">Cancel</button><button style="margin-left: 5px;margin-top: 10px;" type="button" id="yes_remove" class="btn btn-danger">Remove</button></div>', 'Remove Admin From Facility?',
                {
                    closeButton: true,
                    preventDuplicates: true,
                    "positionClass": "toast-top-center",
                    "showDuration": "10000",
                    "timeOut": "15000",
                    "extendedTimeOut": "10000",
                    "preventDuplicates": true,
                    allowHtml: true,
                    onShown: function (toast) {
                        $("#yes_remove").click(function () {

                            $.ajax({
                                url: APP_BASE_URL +'admin/facility-delete',
                                data: {
                                    admin_id: admin_id,
                                    facility_id:facility_id
                                },
                                type: "post",
                                dataType : "json",
                                success : function (success) {
                                   // console.log(success);
                                    window.location.href = APP_BASE_URL +'admin/facility';
                                }

                            });
                        });
                    }
                });
        }
    });



    /**
     * admin facility List Datatable Function
     */
    // $('#admin_facility_list').dataTable({
    //     processing: false,
    //     serverSide: false,
    //     pageLength: 50,
    //     bLengthChange: false,
    //     order: [[1, 'desc']],
    //     "language": {
    //         "emptyTable": "Coach not exist into this facility.",
    //         "loadingRecords": "Please wait - loading..."
    //     },
    //     aoColumns: [
    //         { "bSortable": false },
    //         { "bSortable": true },
    //         { "bSortable": true },
    //         { "bSortable": true },
    //         { "bSortable": false },
    //         { "bSortable": true },
    //         { "bSortable": false }
    //     ],
    //
    //     responsive: {
    //         details: {
    //             type: 'column',
    //             target: 'tr'
    //         }
    //     },
    //     columnDefs: [ {
    //         className: 'control',
    //         orderable: false,
    //         targets:   0,
    //     },
    //         { className: "dt-right", "targets": [1,4] },
    //         { className: "dt-center", "targets": [6,7] },
    //     ],
    // });

    var table = $('#admin_facility_list');
    $('#admin_facility_list').dataTable({
        processing: false,
        serverSide: false,
        pageLength: 50,
        "sPaginationType": "full_numbers",
        order: [[1, 'desc']],
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        aoColumns: [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false },
        ],
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0,
        },
            { className: "dt-right", "targets": [1,4] },
            { className: "dt-center", "targets": [7] },
        ],
        "bLengthChange": false,
    });



});







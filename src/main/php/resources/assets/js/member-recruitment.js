$( document ).ready(function() {

    /* member recruitment form validation */
    /**
     * Facility edit form validation
     */
    $("#form_add_member_recruitment").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            first_name: {
                minlength: 2,
                required: true
            },
            last_name: {
                minlength: 2,
                required: true
            },
            email: {
                required: true,
                email:true
            },
            mrn: {


            },
            confirm_mrn: {

                equalTo:"#mrn"
            },


            zip: {

                number: true
            },

            phone_number:{
                required: true
            },

            diabetes_type: {
                required: true
            },

            facility_list: {
                required: true
            }

        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    /* */


    /* member recruitment form validation */
    /**
     * Mrn  form validation
     */
    $("#member_mrn_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            mrn: {

                required: true
            },
            confirm_mrn: {

                required: true,
                equalTo:"#mrn"
            }



        },

        errorPlacement: function (error, element) { // render error placement for each input type
           // alert("asa");
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _form = $('#member_mrn_form');
            var patient_id=_form.find('[name="id"]').val();
            var mrn=_form.find('[name="mrn"]').val();
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                $('#modal_mrn').modal('hide');
                window.setTimeout(function() { App.stopPageLoading();}, 1000);

                if (response.success != undefined) {
                    $('#mrn_'+patient_id).text(mrn);
                    $('#mrn_'+patient_id).attr('data-mrn-id',mrn);
                    toastr["success"](response.success, "MRN ");
                }
                else if (response.error != undefined) {
                    //toastr["error"](response.error, "Remove Prospective Member");
                }
            }, _form.attr('method'), true, '#hs-loader');
        }
    });


     /* */
     $("#date_of_birth").datepicker({
         autoclose: true,
         orientation: "bottom",
         endDate: new Date()


     });


    /**
     *  prospective Member List Datatable Function
     */
var xtab =   $('#prospective_member_list').dataTable({
        processing: false,
        serverSide: false,
        pageLength: 50,
        bLengthChange: false,
        "sPaginationType": "full_numbers",

        order: [[1, 'desc']],
        "language": {
            "emptyTable": "Member not exist into this facility.",
            "loadingRecords": "Please wait - loading..."
        },
        aoColumns: [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false }

        ],
        "ajax": {
            "url": $('#prospective_member_list').data('action'),
            "type": "POST",
            "data": function (data) {

                data.another_search = $("#another_search_m option:selected").val();

            }

        },
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0,
        },{ className: "dt-right", "targets": [1,5,7] }],

    });


    /**
     * admin  prospective Member List Datatable Function
     */
      $('#admin_prospective_member_list').dataTable({
        processing: false,
        serverSide: false,
        pageLength: 50,
        bLengthChange: false,
        "sPaginationType": "full_numbers",

        order: [[1, 'desc']],
        "language": {
            "emptyTable": "Member not exist into this facility.",
            "loadingRecords": "Please wait - loading..."
        },
        aoColumns: [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false },
            { "bSortable": false }

        ],
        "ajax": {
            "url": $('#admin_prospective_member_list').data('action'),
            "type": "POST",
            "data": function (data) {

                data.another_search = $("#another_search_m option:selected").val();

            }

        },
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0,
        },{ className: "dt-right", "targets": [1,4,5,7] }],

    });




















    /*$('#another_search_m').on('change', function() {

       xtab.ajax.reload();
    });*/





    /*$('#custome_search').on( 'keyup', function () {
        xtab
            .columns( 3 )
            .search( this.value )
            .draw();
    } );*/


    /*xtab.columns( '.select-filter' ).every( function () {
        var that = this;

        // Create the select list and search operation
        var select = $('<select />')
            .appendTo(
                this.footer()
            )
            .on( 'change', function () {
                that
                    .search( $(this).val() )
                    .draw();
            } );

        // Get the search data for the first column and add to the select list
        this
            .cache( 'search' )
            .sort()
            .unique()
            .each( function ( d ) {
                select.append( $('<option value="'+d+'">'+d+'</option>') );
            } );
    } );
*/

    /**
     * Show prospective member Modal Handler
     */
    $('#modal_prospective_member').on('show.bs.modal', function (e) {
        var _form = $('#prespective_member_delete_msg');
        _form.find('[name="id"]').val('');
        var id = $(e.relatedTarget).data('id');
        //alert(id);
        if(id){
            _form.find('[name="id"]').val(id);
        }
    });



    $('#remove_prospective_member').on('click', function(){
        var _this = $(this);
        var _form = $('#prespective_member_delete_msg');
        if (_this.hasClass('clicked')) {
            return;
        }
        $('#modal_prospective_member').modal('hide');
        $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
            //alert(response.patient_id);
            window.setTimeout(function() { App.stopPageLoading();}, 1000);

            if (response.success != undefined) {
               // alert(response.patient_id);
                var row_id = response.patient_id;
                $('td:contains("'+row_id+'")').parent().hide(800);
                $('.child').hide();
                toastr["success"](response.success, "Remove Member");
            }
            else if (response.error != undefined) {
                toastr["error"](response.error, "Remove Member");
            }
        }, _form.attr('method'), true, '#hs-loader');
    });



    /* send invitation */

    $('body').on('click','.invitation_send', function(){

        var user_id = $(this).attr("data-id-for-mail");
        var _form = $('#invitaiton_send_form');
        _form.find('[name="id"]').val(user_id);
        $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
            //alert(response);
            window.setTimeout(function() { App.stopPageLoading();}, 1000);

            if (response.success != undefined) {
                // alert(response.patient_id);
                var row_id = response.patient_id;
                $('td:contains("'+row_id+'")').parent().hide(800);
                toastr["success"](response.success, "Invitation Send");
            }
            else if (response.error != undefined) {
                toastr["error"](response.error, "Invitation Send");
            }
        }, _form.attr('method'), true, '#hs-loader');

    });





    /* add mrn number */
    $('#modal_mrn').on('show.bs.modal', function (e) {
        var _form = $('#member_mrn_form');
        _form.find('[name="id"]').val('');
        var patient_id = $(e.relatedTarget).data('patient-id');
        var mrn_id = $(e.relatedTarget).attr('data-mrn-id');
        if(patient_id){
            _form.find('[name="id"]').val(patient_id);
        }

        _form.find('[name="mrn"]').val(mrn_id);
        _form.find('[name="confirm_mrn"]').val(mrn_id);

    });

    /* Add lifestyle coach and coach by facility*/

    $('body').on('change','.facility_list', function (e) {
        var facility_id =  $('.facility_list').val();
            $.ajax({
                url: APP_BASE_URL +'admin/allCoaches',
                data: {
                    facility_id:facility_id,
                },
                type: "post",
                success : function (response) {
                    $("#lifestyle_coach option").remove();
                    $("#food_coach option").remove();
                    jQuery.each( response.leadcoach, function( i, val ) {
                        jQuery.each( val, function( key, value ) {
                            $("#lifestyle_coach").append('<option value="' + key + '">' + value + '</option>');
                        });
                    });
                    jQuery.each( response.foodcoach, function( i, val ) {
                        jQuery.each( val, function( key, value ) {
                            $("#food_coach").append('<option value="' + key + '">' + value + '</option>');
                        });
                    });
                }
            });

    });

    /* end add comment for post*/






});
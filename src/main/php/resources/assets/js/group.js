/**
 * Created by rpatidar on 31/08/2017.
 */
jQuery(document).ready(function ($) {

    //initiate select2 om modal pop up  create new group
    $("#create-new-group").on('shown.bs.modal', function () {


        //alert($('.timepicker-default').val());
        $('#create_new_group').get(0).reset();
        $('.timepicker-default').val('12:00 PM');
        $("#create_new_group").validate().resetForm();

        $(".select-life-coach").select2({
            width: null,
            templateResult: formatOption
        });

        $(".select-food-coach").select2({
            width: null,
            templateResult: formatOption
        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

        $(".timepicker-default").timepicker(
        {
            autoclose:!0,timeFormat: 'h:i A',
            defaultTime: '12:00 PM'
        });


    });



    function formatOption (option)
    {
        var $option = $(
            '<div><strong>' + option.text + '</strong></div><div>' + option.title + '</div>'
        );
        return $option;
    }

    /**
     *  Onclick Assign Coach modal radio button show hide food coach and lead coach dropdown
     */
    $(".group_type").on('click', function(){
        if ($(this).val() == 0) {
            $('.selectfoodcoach').show();
            $('.selectlifecoach').show();
            $('#selectfoodcoach').removeClass('ignore').show();
            $('#selectlifecoach').removeClass('ignore').show();
        } else if($(this).val() == 1) {
            $('.selectfoodcoach').hide();
            $('.selectlifecoach').show();
            $('#selectfoodcoach').addClass('ignore').hide();
            $('#selectlifecoach').removeClass('ignore').show();
        }
    });


    /**
     * Update Group Details
     */
    $('#edit_group_detail').on('show.bs.modal', function (e) {
        $('#edit_mygroup_detail').get(0).reset();
        $("#edit_mygroup_detail").validate().resetForm();
        var _form = $('#edit_mygroup_detail');
        _form.find('[name="id"]').val('');
        var id = $(e.relatedTarget).data('id');
        if(id){
            _form.find('[name="id"]').val(id);
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {

                    _form.find('[name="name"]').val(response.success.name);
                    _form.find('[name="capacity"]').val(response.success.capacity);
                    _form.find('[name="autoPostTime"]').val(response.success.auto_post_time);
                    _form.find('[name="diabetesTypeId"]').val(response.success.diabetes_type_id).trigger('change');
                    _form.find('[name="preferredLanguage"]').val(response.success.preferred_language).trigger('change');

                    if(response.success.total_users > 0)
                    {
                        _form.find('[name="diabetesTypeId"]').addClass('ignore');
                        $("#edit_mygroup_detail #diabetesTypeList").attr('disabled','disabled');
                    }
                    else
                    {
                        _form.find('[name="diabetesTypeId"]').removeClass('ignore');
                        $("#edit_mygroup_detail #diabetesTypeList").removeAttr('disabled');
                    }

                    if (response.success.is_person == 1) {
                        $('.selectfoodcoach').hide();
                        $('.selectlifecoach').show();
                        _form.find('[name="selectFoodCoach"]').addClass('ignore').hide();
                       // _form.find('[name="selectLifeCoach"]').addClass('ignore').show();
                    } else {
                        $('.selectfoodcoach').show();
                        $('.selectlifecoach').show();
                        _form.find('[name="selectFoodCoach"]').removeClass('ignore');
                        _form.find('[name="selectLifeCoach"]').removeClass('ignore');

                        _form.find('[name="selectFoodCoach"]').val(response.success.primary_food_coach_id).trigger('change');
                    }
                    _form.find('[name="selectLifeCoach"]').val(response.success.lead_user_id).trigger('change');

                    //$("#edit_mygroup_detail #selectfoodcoach").attr('disabled','disabled');
                    //$("#edit_mygroup_detail #selectlifecoach").attr('disabled','disabled');

                    if(response.success.is_auto_post_enabled == 1)
                    {
                        $('.autoPostEnable').prop('checked', true);
                        $('.timepicker-default').attr('readonly', false);
                    }
                    else
                    {
                        $('.autoPostEnable').prop('checked', false);
                        $('.timepicker-default').attr('readonly', true);
                    }

                    $(".timepicker-default").timepicker(
                        {
                            autoclose:!0,timeFormat: 'h:i A'
                    });

                    $(".select-life-coach").select2({
                        width: null,
                        templateResult: formatOption
                    });

                    $(".select-food-coach").select2({
                        width: null,
                        templateResult: formatOption
                    });

                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Member Detail");
                }
            }, 'GET', true, '#hs-loader');
        }
    });



    /**
     * Show Coach Delete Modal Handler
     */
    $('#remove_group').on('show.bs.modal', function (e) {
        var _form = $('#delete-group');
        _form.find('[name="id"]').val('');
        var id = $(e.relatedTarget).data('id');
        if(id){
            _form.find('[name="id"]').val(id);
        }
    });


    /**
     * Show Coach Delete Modal Handler
     */
    $('#remove_group_coach').on('show.bs.modal', function (e) {
        $('#assign_coach_to_groups').get(0).reset();
        $("#assign_coach_to_groups").validate().resetForm();
        var _form = $('#remove-coach-from-group');
        _form.find('[name="uid"]').val('');
        _form.find('[name="gid"]').val('');
        var uid = $(e.relatedTarget).data('uid');
        var gid = $(e.relatedTarget).data('gid');
        if(uid && gid){
            _form.find('[name="uid"]').val(uid);
            _form.find('[name="gid"]').val(gid);
        }
    });



    /**
     * Show Coach Delete Modal Handler
     */
    $('#assign_coach_to_group').on('show.bs.modal', function (e) {

        $('.select_multi_coach_food').hide();
        $('.select_multi_coach_life').hide();
        $('#assign_coach_to_groups').get(0).reset();
        $("#assign_coach_to_groups").validate().resetForm();

        $("#lead_coach").children().remove("optgroup");
        $("#food_coach").children().remove("optgroup");
        $('#lead_coach').children('option').remove();
        $('#food_coach').children('option').remove();

        var _form = $('#assign_coach_to_groups');
        _form.find('[name="group_id"]').val('');
        var id = $(e.relatedTarget).data('id');
        if(id){
            _form.find('[name="group_id"]').val(id);
        }

        $.HS.call_jquery_ajax(_form.attr('assign_coach_data'), _form.serialize(), false, false, function (response) {

            window.setTimeout(function () { App.stopPageLoading(); }, 1000);

            if (response.success != undefined) {

                if (response.leadcoach != undefined) {

                    $("#lead_coach").append(' <optgroup label="Lifestyle Coach">');
                    $.each(response.leadcoach, function (key, value) {

                        $("#lead_coach").append('<option title="'+ value.email+'"  value="' + value.provider_id+ '">' + value.full_name + '</option>');
                    });
                    $("#lead_coach").append(' </optgroup>');

                    $("#lead_coach").select2({
                        width: null,
                        templateResult: formatOption
                    });
                }

                if (response.foodcoach != undefined) {

                    $("#food_coach").append(' <optgroup label="Food Coach">');
                    $.each(response.foodcoach, function (key, value) {

                        $("#food_coach").append('<option  title="'+ value.email+'"  value="' + value.provider_id+ '">' + value.full_name + '</option>');
                    });
                    $("#food_coach").append(' </optgroup>');

                    $("#food_coach").select2({
                        width: null,
                        templateResult: formatOption
                    });
                }

                function formatOption (option)
                {
                    var $option = $(
                        '<div><strong>' + option.text + '</strong></div><div>' + option.title + '</div>'
                    );
                    return $option;
                }
            }
            else if (response.error != undefined) {
                toastr["error"](response.error, "Assign Coach");
            }
        },'POST', true, '#hs-loader');
    });



    /**
     * Assign Coach from coach list page validation
     */
    $("#assign_coach_to_groups").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
            select_multi_food: {
                maxlength: jQuery.validator.format("Max {0} Coach must be selected"),
                minlength: jQuery.validator.format("At least {0} Coach must be selected")
            },
            select_multi_life: {
                maxlength: jQuery.validator.format("Max {0} Coach must be selected"),
                minlength: jQuery.validator.format("At least {0} Coach must be selected")
            }

        },
        rules: {
            select_multi_food: {
                required: true,
                minlength: 1,
                maxlength: 1
            },
            select_multi_life: {
                required: true,
                minlength: 1,
                maxlength: 1
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _this = $(this);
            var _form = $('#assign_coach_to_groups');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                _this.removeClass('clicked');
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    toastr["success"](response.success, "Assign Coach");
                    $('#loding_icon').trigger('click');
                    group_list.ajax.reload(null, false);
                    $('#assign_coach_to_group').modal('hide');
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Assign Coach");
                }
            }, _form.attr('method'), true, '#hs-loader');
        }
    });


    /**
     * Remove Group From Facility Handler
     */
    $('#remove_group_from_facility').on('click', function(){
        var _this = $(this);
        var _form = $('#delete-group');
        if (_this.hasClass('clicked')) {
            return;
        }
        $('#remove_group').modal('hide');
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                 window.setTimeout(function() { App.stopPageLoading();}, 1000);
                 if (response.success != undefined) {
                    toastr["success"](response.success, "Delete Group");
                     $('#loding_icon').trigger('click');
                     group_list.ajax.reload(null, false);
                 }
                 else if (response.error != undefined) {
                    toastr["error"](response.error, "Delete Group");
                 }
             }, _form.attr('method'), true, '#hs-loader');
    });



    /**
     * Remove Coach From Group Handler
     */
    $('#remove_coach_from_group').on('click', function(){
        var _this = $(this);
        var _form = $('#remove-coach-from-group');
        if (_this.hasClass('clicked')) {
            return;
        }

        $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
            window.setTimeout(function() { App.stopPageLoading();}, 1000);
            if (response.success != undefined) {
                toastr["success"](response.success, "Remove Coach");
                $('#loding_icon').trigger('click');
                group_list.ajax.reload(null, false);
                $('#remove_group_coach').modal('hide');
            }
            else if (response.error != undefined) {
                toastr["error"](response.error, "Remove Coach");
            }
        }, _form.attr('method'), true, '#hs-loader');
    });


    /**
     *  On click auto post time
     */
    $(".autoPostEnable").on('click', function(){
        if ($(this).prop('checked')==true){
            $('.timepicker-default').attr('readonly', false);
        }
        else
            $('.timepicker-default').attr('readonly', true);
    });

    /**
     * Group Member List Datatable Function
     */
    $('#group_member_list').dataTable({
        processing: false,
        serverSide: false,
        pageLength: 50,
        bLengthChange: false,
        order: [[1, 'desc']],
        "language": {
            "emptyTable": "Member not exist into this facility.",
            "loadingRecords": "Please wait - loading..."
        },
        aoColumns: [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true }
        ],
        dom: 'Bfrtip',
        buttons: [
            {
                text: 'Show All',
                action: function ( e, dt, node, config ) {
                }
            }
        ],
        "ajax": {
            "url": $('#group_member_list').data('action'),
            "type": "POST",
        },
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0,
        },{ className: "dt-right", "targets": [1,4] }],
    });



    /**
     * Group List Datatable Function
     */
    var group_list =  $('#group_list').DataTable({
        stateSave: true,
        processing: false,
        searching: true,
        serverSide: false,
        pageLength: 50,
        bLengthChange: false,
        "sPaginationType": "full_numbers",
        //order: [[1, 'desc']],
        "language": {
            "emptyTable": "We did not find any groups that match based on the filters selected above, please adjust the filters and try again",
            "loadingRecords": "Please wait - loading..."
        },
        aoColumns: [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": false },
            { "bSortable": false }
        ],
        "ajax": {
            "url": $('#group_list').data('action'),
            "type": "GET",
            "data": function (data) {
                data.group      = $("#filterGroup option:selected").val();
                data.capacity   = $("#filterCapacity option:selected").val();
            }
        },
        "fnDrawCallback" : function() {
            window.setTimeout(function() { App.stopPageLoading();}, 1000);
            $('div.danger').parent('td').addClass('danger');
            $('div.success').parent('td').addClass('success');
            $('div.warning').parent('td').addClass('warning');
            $('div.white').parent('td').addClass('white');
            $('div.active').parent('td').addClass('active');
        },
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [ {
            className: 'control',
            orderable: false,
            targets:   0,
        }],
    });

    $('#filterGroup').on('change', function() {
        $('#loding_icon').trigger('click');
        group_list.ajax.reload(null, false);
    });

    $('#filterCapacity').on('change', function() {
        $('#loding_icon').trigger('click');
        group_list.ajax.reload(null, false);
    });




    /**
     * Create new facility
     */
    $("#create_new_group").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            name: {
                required: true
            },
            diabetesTypeId: {
                required: true
            },
            capacity: {
                required: true,digits: true,range: [1, 100]
            },
            group_type: {
                required: true
            },
            preferredLanguage: {
                required: true
            },
            /*selectFoodCoach: {
                required: true
            },*/
            selectLifeCoach: {
                required: true
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (element.attr("name") == "group_type") {
                error.insertAfter("#group_type-error");
            } else {
                if (cont) {
                    cont.after(error);
                }
                if (cont2) {
                    $(element).after(error);
                }
                if (cont&&cont2){
                    cont.after(error);
                }
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
             var _this = $(this);
             var _form = $('#create_new_group');
             $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
             _this.removeClass('clicked');
             window.setTimeout(function() { App.stopPageLoading();}, 1000);
             if (response.success != undefined) {
                    toastr["success"](response.success, 'Create Group');
                    $('#create-new-group').modal('hide');
                    $('#loding_icon').trigger('click');
                    $('#filterGroup').val('all').change();
                    //group_list.ajax.reload(null, false);
             }
             else if (response.error != undefined) {

                toastr["error"](response.error, 'Create Group');
             }
             }, _form.attr('method'), true, '#hs-loader');
        }
    });



    /*For group popup life style coach on change */
    $(".select-life-coach").on("change", function(event) {
        $(this).valid() ;
    });



   /* $(".select-food-coach").on("change", function(event) {
        $(this).valid() ;
    });*/
    /**
     * Create new facility
     */
    $("#edit_mygroup_detail").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            name: {
                required: true
            },
            diabetesTypeId: {
                required: true
            },
            capacity: {
                required: true,digits: true,range: [1, 100]
            },
            group_type: {
                required: true
            },
            preferredLanguage: {
                required: true
            },
            /*selectFoodCoach: {
                required: true
            },*/
            selectLifeCoach: {
                required: true
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var _this = $(this);
            var _form = $('#edit_mygroup_detail');
            $.HS.call_jquery_ajax(_form.attr('create-group'), _form.serialize(), false, false, function (response) {
                _this.removeClass('clicked');
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    toastr["success"](response.success, 'Edit Group');
                    $('#edit_group_detail').modal('hide');

                    $('#loding_icon').trigger('click');
                    group_list.ajax.reload(null, false);
                }
                else if (response.error != undefined) {

                    toastr["error"](response.error, 'Edit Group');
                }
            }, 'POST', true, '#hs-loader');
        }
    });


    $('.group-members').on("select2:select", function(e) {
        $('.group-members option:selected').each(function(){
            if($(this).val() == 'all')
            {
                $(".group-members").val('all').trigger("change");
            }
        });
    });


    $('.group-names').on("select2:select", function(e) {
        $('.group-names option:selected').each(function(){
            if($(this).val() == 'all')
            {
                $(".group-names").val('all').trigger("change");
            }
        });
    });


    /* jquey for  messaging lead coach in my group page  start here  */

    ///init message group memeber modal
    $("#msg-group-member").on('shown.bs.modal', function (e) {

        $('#message_group_member_form').get(0).reset();
        $("#message_group_member_form").validate().resetForm();
        $('.message_group_member_group_name').text('');
        $("#file_upload_show").html('');
        $("#uploadfile").val(null);
        $(".recom-messages2").select2({
            width: null,

        });

        $(".group-members").select2({
            width: null,
            allowClear: true,
            placeholder: 'Select Group Members',

        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });
        $(".recom-messages2").select2('val', 'Select Recommended Message');
        //change recommended msg to send
        $('.recom-messages2').on ('change', function(){
            var recentmsg = $(".recom-messages2 option:selected").val();
            if(recentmsg === "Select Recommended Message"){
                $(".recomend-text").val('');
            }
            else{
                $(".recomend-text").val(recentmsg);
            }
        });

        var _form = $('#message_group_member_form');

        _form.find('[name="group_id"]').val('');
        var group_id = $(e.relatedTarget).data('id');

        if(group_id) {
            _form.find('[name="group_id"]').val(group_id);
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {

                window.setTimeout(function () { App.stopPageLoading(); }, 1000);
                $('.group-members').children('option').remove();

                if (response.group_list != undefined)
                {

                    $.each(response.group_list, function (key, value) {

                        $(".group-members").append('<option title="'+ value.email+' " value="' + value.user_id + '">' + value.full_name +'</option>');

                    });

                    $(".group-members").select2({
                        width: null,
                        templateResult: formatOption
                    });

                    function formatOption (option)
                    {
                        var $option = $(
                            '<div><strong>' + option.text + '</strong></div><div>' + option.title + '</div>'
                        );
                        return $option;
                    }
                }
                if (response.group_name != undefined) {

                    $(".message_group_member_group_name").text(response.group_name);
                }

                if (response.error != undefined) {
                    toastr["error"](response.error, "Assign Member to Group");
                }

            }, 'GET', true, '#hs-loader');

        }

    });


    $('input[type="checkbox"]'). click(function() {
        if ($(this).prop("checked") == true) {
            $('#group-names').prop('disabled', 'true');
        }
        else if ($(this).prop("checked") == false) {
            $('#group-names').removeAttr('disabled');
        }
    });


    $("#post-on-group").on('shown.bs.modal', function (e) {

        $('#uploadfile').val(null);
        $('.added-files').remove();
        $('#post_group_form').get(0).reset();
        $('#group-names').removeAttr('disabled');
        $("#post_group_form").validate().resetForm();
        $("#file_upload_show").html('');
        $(".uploadfile").val(null);
        $(".recom-messages2").select2({
            width: null,

        });

        $(".group-names").select2({
            width: null,
            allowClear: true,
            placeholder: 'Select Groups',

        });

        $("button[data-select2-open]").click(function() {
            $("#" + $(this).data("select2-open")).select2("open");
        });

    });

    $("#post_group_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            message: {
                //required: true
                required: function (element) {
                    if ($(".uploadfile").val()=='') {
                        // return "Please enter the reference transaction number ";
                        return true;
                    }else{
                        return false;
                    }
                }
            },
            'group_names[]':{
                required: function (element) {
                    if ($("#group-names").val()=='') {
                        // return "Please enter the reference transaction number ";
                        return true;
                    }else{
                        return false;
                    }
                }
            },
            uploadfile:{
                required: function (element) {
                    if ($("#message").val()=='') {
                        // return "Please enter the reference transaction number ";
                        return true;
                    }else{
                        return false;
                    }
                },
                extension: "jpg|jpeg|png"
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {

            var _form = $('#post_group_form');
            // var user_id = _form.find('[name="user_id"]').val();
            var formData = new FormData($('#post_group_form')[0]);

           // if(user_id){
                $('#loding_icon').trigger('click');
                $.ajax({
                    url: _form.attr('action'),
                    data: formData,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        window.setTimeout(function() { App.stopPageLoading();}, 1000);
                        if (response.success != undefined) {
                            $("#uploadfile").val(null);
                            $('#post-on-group').modal('hide');

                            toastr["success"](response.success, "Post Message");
                        }
                        else if (response.error != undefined) {
                            toastr["error"](response.error, "Error in Post Creation");
                        }
                    },

                });

            }
        //}
    });




    /*jquey for  messaging lead coach in my group page end here  */


    $("#attachment_post").click(function(){
        $("#uploadfile_post").click();
    });

    $('#uploadfile_post').on('change', function() {

        var filename = this.value;
        //alert(filename);
        var lastIndex = filename.lastIndexOf("\\");

        if (lastIndex >= 0) {
            filename = filename.substring(lastIndex + 1);
            //alert(filename);
        }
        var files = $('#uploadfile_post')[0].files;

        for (var i = 0; i < files.length; i++) {
            $("#file_upload_show_post").html('<div id="tab_images_uploader_filelist" class="col-md-11 col-sm-11"><div class="alert alert-warning added-files" id="uploaded_file_o_1bs7ktva0hhsijfcln1fpi1o137">' + files[i].name +  '<span class="status label label-info"></span>&nbsp;<a href="javascript:;" style="margin-top:-5px" class="remove pull-right btn btn-sm red"><i class="fa fa-times"></i> remove</a></div></div>');

        }

    });

    /* file attachemt code end here */

    /* file attachment remove code start here */
    $(document).on('click', '.remove_post', function() {
        $(this).parent('div').remove();
        $("#uploadfile_post").val(null);

    });


    /* file attachment code start here */

    $("#attachment").click(function(){
        $("#uploadfile").click();
    });

    $('#uploadfile').on('change', function() {

        var filename = this.value;
        //alert(filename);
        var lastIndex = filename.lastIndexOf("\\");

        if (lastIndex >= 0) {
            filename = filename.substring(lastIndex + 1);
            //alert(filename);
        }
        var files = $('#uploadfile')[0].files;

        for (var i = 0; i < files.length; i++) {
            $("#file_upload_show").html('<div id="tab_images_uploader_filelist" class="col-md-11 col-sm-11"><div class="alert alert-warning added-files" id="uploaded_file_o_1bs7ktva0hhsijfcln1fpi1o137">' + files[i].name +  '<span class="status label label-info"></span>&nbsp;<a href="javascript:;" style="margin-top:-5px" class="remove pull-right btn btn-sm red"><i class="fa fa-times"></i> remove</a></div></div>');

        }

    });

    /* file attachemt code end here */

    /* file attachment remove code start here */
    $(document).on('click', '.remove', function() {
        $(this).parent('div').remove();
        $("#uploadfile").val(null);

    });
    /* file attachment remove code end here */


    /* jquey for  messaging lead coach in my group page and validation and data send   */

// Older "accept" file extension method. Old docs: http://docs.jquery.com/Plugins/Validation/Methods/accept
    $.validator.addMethod( "extension", function( value, element, param ) {
        param = typeof param === "string" ? param.replace( /,/g, "|" ) : "png|jpe?g|gif";
        return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
    }, $.validator.format( "Only image files uploading supported" ) );



    $("#message_group_member_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
        },
        rules: {
            message: {
                required: true
            },
            'group_members[]':{
                required: true
            },
            uploadfile:{
                extension: "jpg|jpeg|png|pdf"
            }
        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont&&cont2){
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {

            var _form = $('#message_group_member_form');
            var user_id = _form.find('[name="user_id"]').val();
            var formData = new FormData($('#message_group_member_form')[0]);

            if(user_id){
                $('#loding_icon').trigger('click');
                $.ajax({
                    url: _form.attr('save_url'),
                    data: formData,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {
                        window.setTimeout(function() { App.stopPageLoading();}, 1000);
                        if (response.success != undefined) {
                            $('.remove').parent('div').remove();
                            $("#uploadfile").val(null);

                            $('.group-members').children('option').remove();
                            //$(".recom-messages2").select2('val', 'Select Recommended Message');
                            //$(".recomend-text").val('');
                            $('#msg-group-member').modal('hide');

                            toastr["success"](response.success, "Group Member Message");
                        }
                        else if (response.error != undefined) {
                            toastr["error"](response.error, "Group Member Message");
                        }
                    },

                });

            }
        }
    });


    /*--------------------------------------------------------Js For Groupwall Start Here------------------------------------------------------------*/



    /**
     * For Delete group post
     */
    $('body').on('click','.delete-group-post', function (e) {
        toastr.clear();
        var post_id= $(this).attr('post_id');
        var post_delete_url=$('#post_delete_url').val();


            toastr['info']('<div style="margin-top: 5px;">Are you sure to Delete this post?</div><div><button type="button" class="btn btn-default pull-right" style="margin-top: 10px;">Cancel</button><button style="margin-left: 5px;margin-top: 10px;" type="button" id="yes_remove" class="btn btn-danger">Remove</button></div>', 'Remove Post From Group?',
                {
                    closeButton: true,
                    preventDuplicates: true,
                    "positionClass": "toast-top-center",
                    "showDuration": "10000",
                    "timeOut": "15000",
                    "extendedTimeOut": "10000",
                    "preventDuplicates": true,
                      allowHtml: true,
                    onShown: function (toast) {
                        $("#yes_remove").click(function () {
                            $.ajax({
                                url: post_delete_url,
                                data: {
                                    post_id: post_id
                                },
                                type: "post",
                                dataType : "json",
                                success : function (response) {
                                    window.setTimeout(function() { App.stopPageLoading();}, 1000);
                                    if (response.success != undefined) {
                                        toastr.clear();
                                        $('#post_' + post_id).remove();
                                        toastr["success"](response.success, "Post Deleted Successfully");
                                    }else if (response.error != undefined) {
                                        toastr["error"](response.error, "Error in [ost delete");
                                    }

                                }

                            });
                        });
                    }
                });

    });

    /* for post window scroll */

     $(window).scroll(function ()
    {
        if($(document).height() <= $(window).scrollTop() + $(window).height())
        {
            loadmore();
        }
    });

    function loadmore()
    {
        var val = $("#row_no").val();
        var group_id= $("#group_id").val();
        var post_url=$('#post_url').val();

        $.ajax({
            type: 'post',
            url:post_url,

            data: {
                getresult:val,group_id:group_id,
            },
            success: function (response) {
                $(".post-list").append(response);

                // We increase the value by 10 because we limit the results by 10
                $("#row_no").val(Number(val)+20);

                $('.todo-comment img:not(".emojione"):not(".todo-userpic")').each(function () {
                    var parentWidth = $(this).parent('p').width();
                    var imgHeight = $(this).height();
                    var imgwidth = $(this).width();
                    if (imgHeight >= 450) {
                        if (imgwidth > imgHeight) {
                            $(this).css('height', 'auto');
                            $(this).css('max-height', '450px');
                            $(this).css('width', 'auto');
                        } else {
                            $(this).css('height', '450px');
                            $(this).css('width', 'auto');
                        }
                    }
                    else {
                        if (imgwidth >= parentWidth) {
                            $(this).css('height', 'auto');
                            $(this).css('width', '100%');
                        }
                        else {
                            $(this).css('height', 'auto');
                            $(this).css('width', 'auto');
                        }
                    }

                    $(this).after('</br>')
                });
            }
        });
    }
/* end scroll */

/* Post Like BY coach lead */



    $('body').on('click','.post-like', function (e) {
        var like =  $(this).attr('data-like');
        var post_id =  $(this).attr('post_id');
        var group_id =  $('#group_id').val();
        var page_url = $('#page_url').val();

                        if($(this).hasClass('green-jungle')){
                            $(this).removeClass('green-jungle');
                            $.ajax({
                                url: APP_BASE_URL + page_url +'/remove_like_post_by_coach',
                                data: {
                                    post_id: post_id,group_id:group_id,
                                },
                                type: "post",
                                success : function (response) {
                                    if(response > 0){
                                    $('#like_'+post_id).html(' '+response+' ');
                                    }else{
                                    $('#like_'+post_id).html(' ');
                                    }
                                }
                            });
                        }else{
                            $(this).addClass('green-jungle');
                            $.ajax({
                                url: APP_BASE_URL + page_url+'/like_post_by_coach',
                                data: {
                                    post_id: post_id,group_id:group_id,
                                },
                                type: "post",
                                success : function (response) {
                                    $('#like_'+post_id).html(' '+response+' ');
                                }

                            });
                        }
   });

    /* End Post Like BY coach lead */

    /* for comment pop up*/

    $('body').on('click','.post-comment', function (e) {
        var post_id =  $(this).attr('post_id');
        $('#comment_'+post_id).show();

        });
    /* End comment pop up*/

    /* Add comment for post*/

    $('body').on('click','.add-comment', function (e) {
        $('.add-comment').attr('disabled',true);
        var post_id =  $(this).attr('post_id');
        var group_id =$('#group_id').val();
        var comment=$('#text_'+post_id).val();
        var comment_length=comment.trim();
        var total_comment = $('#total_comment_'+post_id).html();
        var page_url = $('#page_url').val();
        if(comment_length !=''){
        $.ajax({
            url: APP_BASE_URL +page_url+'/add_comment',
            data: {
                post_id: post_id,comment:comment,group_id:group_id,
            },
            type: "post",
            success : function (response) {
                $('.add-comment').removeAttr('disabled');
                if(total_comment.trim().length==0){
                    total_comment=0;
                }
               var t_comment= parseInt(total_comment)+1;
               $('.media-comment_'+post_id).append(response);
               $('#comment_'+post_id).hide();
               $('#text_'+post_id).val('');
               $('#total_comment_'+post_id).html(' '+t_comment+' ');
            }
        });
        }else{
            $('.add-comment').removeAttr('disabled');
            $('.text-error').show();
         $('.text-error').text('This field is required');
         $('#text_'+post_id).css('border','1px solid red');
        }
    });

    /* end add comment for post*/



    /**
     * For Delete comment of post
     */
    $('body').on('click','.delete-post-comment', function (e) {
        toastr.clear();
        var comment_id= $(this).attr('post_comment_id');
        var post_id= $(this).attr('post_id');
        var page_url = $('#page_url').val();
        toastr['info']('<div style="margin-top: 5px;">Are you sure to Delete this comment?</div><div><button type="button" class="btn btn-default pull-right" style="margin-top: 10px;">Cancel</button><button style="margin-left: 5px;margin-top: 10px;" type="button" id="yes_remove" class="btn btn-danger">Remove</button></div>', 'Remove comment From post?',
            {
                closeButton: true,
                preventDuplicates: true,
                "positionClass": "toast-top-center",
                "showDuration": "10000",
                "timeOut": "15000",
                "extendedTimeOut": "10000",
                "preventDuplicates": true,
                allowHtml: true,
                onShown: function (toast) {
                    $("#yes_remove").click(function () {
                        $.ajax({
                            url: APP_BASE_URL + page_url +'/remove_post_comment',
                            data: {
                                comment_id: comment_id,post_id:post_id,
                            },
                            type: "post",
                            success : function (response) {
                                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                                if (response.success != undefined) {
                                    $('#post_comment_'+comment_id).remove();
                                    $('#total_comment_'+post_id).html(' '+response.data+' ');
                                    toastr["success"](response.success, "Comment Deleted Successfully");
                                }else if (response.error != undefined) {
                                    toastr["error"](response.error, "Error in [ost delete");
                                }

                            }

                        });
                    });
                }
            });

    });


    /**
     * For get post Details
     */
    $('#edit_post').on('show.bs.modal', function (e) {

        var _form = $('#edit_post_form');
        _form.find('[name="id"]').val('');
        _form.find('[name="post_description"]').html('');
        var id = $(e.relatedTarget).data('id');
        if(id){
            _form.find('[name="id"]').val(id);
            $.HS.call_jquery_ajax(_form.attr('action'), _form.serialize(), false, false, function (response) {
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                if (response.success != undefined) {
                    $('#edit_post_form').get(0).reset();
                    var desc = response.success.description;
                    // var regex='/(<img (\s*\S*\s)*src\s*=\s*(\"|\')\S*\.(jpg|jpeg|JPEG|JPG|png|PNG|gif|GIF)(\"|\')(\s*\S*\s*)*/*>)/';
                    var regex = /<br\s*[\/]?>/gi;
                    desc = desc.replace(regex, "\n");
                    myspan = document.createElement('span');
                    $(myspan).html(desc);
                    $('#post_content').html($(myspan).html().replace(/\n/g, "<br />"));
                    // var desc = response.success.description;
                    // var patt = /<a href="(.*?)"/g;
                    // if(match=patt.exec(desc)){
                    //     content=match[1];
                    // }else{
                    //     content=desc;
                    // }
                    // $('#post_content').html(content);
                    // _form.find('[name="post_description"]').html(response.success.description);
                    _form.find('[name="post_id"]').val(response.success.post_id);
                    _form.find('[name="group_id"]').val(response.success.group_id);


                    if(response.success.media_url!= null){
                        $('.img-box').show();
                        $('.post_image').show();
                        $('#file_name').val(response.success.media_url);
                        $('.post_image').attr('src','');
                        $('.post_image').attr('src',response.success.media_url);
                        $('.del_img').attr('media_url',response.success.img_path);
                        $('.del_img').attr('post_id',response.success.post_id);
                    }else{
                        $('.img-box').hide();

                    }


                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Error");
                }
            }, 'POST', true, '#hs-loader');
        }
    });


    /* For update post details*/

    $("#edit_post_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {},
        rules: {
            post_description: {
                required: {
                    depends: function () {
                        $(this).val();
                        return true;
                    }
                },
                maxlength: 1500,
            },
            uploadpostfile: {
                extension: "jpg|jpeg|png"
            }

        },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                cont.after(error);
            }
            if (cont2) {
                $(element).after(error);
            }
            if (cont && cont2) {
                cont.after(error);
            }
        },
        highlight: function (element) { // hightlight error inputs

            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            var formData = new FormData($('#edit_post_form')[0]);
            var file_name = $('#file_name').val();
            $('#hs-loader').trigger('click');
            var page_url = $('#page_url').val();
            var content = $('#post_content').html();
            $('#post_content').find('img').replaceWith(function () {
                return this.alt;
            });
                // var content=$('#post_content').find('a').text();
                // $('#post_content').find('a').replaceWith(function () {
                //     return content;
                // });

            $('#post_content').find('a').each(function() {
                var content=$(this).text();
                 $(this).replaceWith(function () {
                    return content;
                });
            });

            var content = $('#post_content').html();
            formData.append("post_description", content);
            if ($('#post_content').text().length == 0) {
                toastr["error"]('Error','Please Fill Text In Post');
                return false;
            }
            $.ajax({
                url: APP_BASE_URL + page_url + '/update-post',
                data: formData,
                type: 'POST',
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    window.setTimeout(function () {
                        App.stopPageLoading();
                    }, 1000);
                    if (response.success != undefined) {
                        // is list page
                        toastr["success"]('Post update successfully', 'Post Updated');
                        $("#edit_post").modal('hide');
                        var input = $("input[name=newpostfile]");
                        var fileName = input.val();

                        if (fileName) { // returns true if the string is not empty
                            input.val('');
                        }
                        $("#desc_" + response.data.post_id).html('');
                        $("#desc_" + response.data.post_id).html(response.data.description);
                        if (response.data.post_update == 1) {
                            $('#img_' + response.data.post_id).remove();
                            if(response.data.post_media_url != null){
                                $("#desc_" + response.data.post_id).after('<img width="100%" id="img_' + response.data.post_id + '" src="'+ response.data.post_media_url + '">')
                                $('.todo-comment img:not(".emojione"):not(".todo-userpic")').each(function () {
                                    var parentWidth = $(this).parent('p').width();
                                    var imgHeight = $(this).height();
                                    var imgwidth = $(this).width();
                                    if (imgHeight >= 450) {
                                        if (imgwidth > imgHeight) {
                                            $(this).css('height', 'auto');
                                            $(this).css('max-height', '450px');
                                            $(this).css('width', 'auto');
                                        } else {
                                            $(this).css('height', '450px');
                                            $(this).css('width', 'auto');
                                        }
                                    }
                                    else {
                                        if (imgwidth >= parentWidth) {
                                            $(this).css('height', 'auto');
                                            $(this).css('width', '100%');
                                        }
                                        else {
                                            $(this).css('height', 'auto');
                                            $(this).css('width', 'auto');
                                        }
                                    }

                                    $(this).after('</br>')
                                });
                            }else{
                                $('#img_' + response.data.post_id).remove();
                            }

                            // $('#img_'+response.data.post_id).attr('src',LOG_IMAGE_BASE_URL +'patientapp-groups'+response.data.post_media_url);
                        } else {
                            $('#img_' + response.data.post_id).remove();
                        }


                    }
                    else if (response.error != undefined) {
                        toastr["error"](response.error, 'Error in update post');
                    }
                },

            });

    }




    });




    /* file attachment code for post start here */

    $("#post_attachment").click(function(){
        $("#uploadpostfile").click();
        $('.del_img').attr('media_url','null');

    });

    $('#uploadpostfile').on('change', function() {

        var filename = this.value;
       // alert(filename);
        var lastIndex = filename.lastIndexOf("\\");
        if (lastIndex >= 0) {
            filename = filename.substring(lastIndex + 1);
        }
        var files = $('#uploadpostfile')[0].files;

        for (var i = 0; i < files.length; i++) {
            $('#file_name').val(LOG_IMAGE_BASE_URL +'patientapp-groups/images/postImages/'+files[i].name );
            //console.log(files[i].name );
        }

        readURL(this);

    });


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.img-box').show();
                $('.post_image').show();
                $('.post_image').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }

    }



    /* file attachemt code for post end here */

    /* file attachment remove for post code start here */
    $(document).on('click', '.del_img', function() {
        $('.img-box').hide();
        $('#file_name').val('');
        $('#new_post').val('');
        var page_url = $('#page_url').val();
        var media_url= $(this).attr('media_url');
        var post_id= $(this).attr('post_id');
        var input = $("input[name=uploadpostfile]");
        var fileName = input.val();

        if(fileName) { // returns true if the string is not empty
            input.val('');
        }
        if(media_url != 'null') {
            $.ajax({
                url: APP_BASE_URL + page_url + '/delete-post-image',
                data: {
                    post_id: post_id,
                },
                type: "post",
                success: function (response) {
                    window.setTimeout(function () {
                        App.stopPageLoading();
                    }, 1000);
                    if (response.success != undefined) {
                        toastr["success"](response.success, "Post Image Deleted Successfully");
                    } else if (response.error != undefined) {
                        toastr["error"](response.error, "Error in post image delete");
                    }

                }

            });
        }

    });
    /* file attachment remove code end here */

    /* for add new post */

    $("#new_attachment").click(function(){
        $("#newpostfile").click();
    });

    $('#newpostfile').on('change', function() {

        var filename = this.value;
        // alert(filename);
        var lastIndex = filename.lastIndexOf("\\");
        if (lastIndex >= 0) {
            filename = filename.substring(lastIndex + 1);
        }
        var files = $('#newpostfile')[0].files;
        $('.badge').show();

        // for (var i = 0; i < files.length; i++) {
        //     $('#file_name').val(LOG_IMAGE_BASE_URL +'patientapp-groups/images/postImages/'+files[i].name );
        //     //console.log(files[i].name );
        // }
    });


    var group_id =  $('#group_id').val();//this is used for post update
    $('#post_group_id').val(group_id);//this is used for post update

    $.validator.addMethod( "extension1", function( value, element, param ) {
        param = typeof param === "string" ? param.replace( /,/g, "|" ) : "png|jpe?g|gif";
        return this.optional( element ) || value.match( new RegExp( "\\.(" + param + ")$", "i" ) );
    }, $.validator.format( "Only image uploading supported" ) );


    $("#new_post_form").validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block help-block-error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: ".ignore",  // validate all fields including form hidden input
        messages: {
            "new_post": {
                required: jQuery.validator.format("Please Enter Post."),
            },
            "newpostfile": {
                required: jQuery.validator.format("Please Select A file."),
            },
        },
        rules: {
            new_post: {
                required: function (element) {
                    if ($("#newpostfile").val()=='') {
                        // return "Please enter the reference transaction number ";
                        return true;
                    }else{
                        return false;
                    }
                },
                maxlength: 1500,
            },
            newpostfile:{
                required: function (element) {
                    if ($("#new_post").val()=='') {
                        // return "Please enter the reference transaction number ";
                        return true;
                    }else{
                        return false;
                    }
                },
                extension: "jpg|jpeg|png"
            }
           },

        errorPlacement: function (error, element) { // render error placement for each input type
            var cont = $(element).parent('.input-group');
            var cont2 = $(element).parent('.form-group');
            if (cont) {
                //cont.after(error);
                $('.btn-cont').after(error);
                $('#new_post-error').css('color','red');
                $('#new_post').css('border','1px solid red');

            }
            if (cont2) {
                //$(element).after(error);
                $('.btn-cont').after(error);
                $('#newpostfile-error').css('color','red');

            }
            if (cont&&cont2){
                //cont.after(error);
            }

            //$('#new_post-error').text('This is required');
        },
        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },
        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },
        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },
        submitHandler: function (form) {
            $('#new_post').css('border','');
            var formData = new FormData($('#new_post_form')[0]);
            $('.add-post').attr('disabled',true);
            var page_url = $('#page_url').val();
            var file_name = $('#file_name').val();
            $.ajax({
                url: APP_BASE_URL +page_url+'/add-new-post',
                data: formData,
                type: 'POST',
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    window.setTimeout(function() { App.stopPageLoading();}, 1000);
                    if (response.success != undefined) {
                        $('.add-post').removeAttr('disabled');
                        $('#new_post').val('');
                        $('.badge').hide()
                        toastr["success"]('Post created successfully', 'Post created');
                        $(".post-list").prepend(response.return_html)
                        $('.todo-comment img:not(".emojione"):not(".todo-userpic")').each(function () {
                            var parentWidth = $(this).parent('p').width();
                            var imgHeight = $(this).height();
                            var imgwidth = $(this).width();
                            if (imgHeight >= 450) {
                                if (imgwidth > imgHeight) {
                                    $(this).css('height', 'auto');
                                    $(this).css('max-height', '450px');
                                    $(this).css('width', 'auto');
                                } else {
                                    $(this).css('height', '450px');
                                    $(this).css('width', 'auto');
                                }
                            }
                            else {
                                if (imgwidth >= parentWidth) {
                                    $(this).css('height', 'auto');
                                    $(this).css('width', '100%');
                                }
                                else {
                                    $(this).css('height', 'auto');
                                    $(this).css('width', 'auto');
                                }
                            }

                            $(this).after('</br>')
                        });
                       }
                    else if (response.error != undefined) {
                        $('.add-post').removeAttr('disabled');
                        toastr["error"](response.error, 'Error in create post');
                    }
                },

            });
        }
    });


    /*--------------------------------------------------------Js For Groupwall End Here------------------------------------------------------------*/


});

$( window ).load(function() {
    $('.todo-comment img:not(".emojione"):not(".todo-userpic")').each(function () {
        var parentWidth = $(this).parent('p').width();
        var imgHeight = $(this).height();
        var imgwidth = $(this).width();
        if (imgHeight >= 450) {
            if (imgwidth > imgHeight) {
                $(this).css('height', 'auto');
                $(this).css('max-height', '450px');
                $(this).css('width', 'auto');
            } else {
                $(this).css('height', '450px');
                $(this).css('width', 'auto');
            }
        }
        else {
            if (imgwidth >= parentWidth) {
                $(this).css('height', 'auto');
                $(this).css('width', '100%');
            }
            else {
                $(this).css('height', 'auto');
                $(this).css('width', 'auto');
            }
        }

        $(this).after('</br>')
    });
})




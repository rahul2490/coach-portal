$(document).ready(function () {

   // var users_data = [];
    var patient_msg_reload_timer;



    /*var _form = $('#direct_msg_by_coach');
    var id = _form.find('[name="id"]').val();
    //var formData = new FormData(_form);
    var formData = new FormData($('#direct_msg_by_coach')[0]);

    if(id){
        $('#loding_icon').trigger('click');
        clearInterval(patient_msg_reload_timer);
        $("#direct_msg_by_coach .send").attr('disabled', 'disabled');
        $("#direct_msg_by_coach .save").attr('disabled', 'disabled');
        $.ajax({
            url: APP_BASE_URL +'/saveDirectMessage',
            data: formData,
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            success: function (response) {

                if (response.success != undefined) {
                    $(response.success).appendTo('.chat_message');
                    $('.remove').parent('div').remove();
                    $("#uploadfile").val(null);

                    $('#direct_msg_by_coach #send_message').val('');

                    if (response.last_message_id != null) {
                        _form.find('[name="last_message_id"]').val(response.last_message_id);
                    }

                    //scroll chet div to bottom
                    var container = $("#direct-message-scroll");
                    container.slimScroll({
                        scrollTo: container[0].scrollHeight
                    });
                    direct_msg_to_patient_reload();
                }
                else if (response.error != undefined) {
                    toastr["error"](response.error, "Direct Messages");
                }
                window.setTimeout(function() { App.stopPageLoading();}, 1000);
                $("#direct_msg_by_coach .send").removeAttr('disabled', 'disabled');
                $("#direct_msg_by_coach .save").removeAttr('disabled', 'disabled');
            },

        });
    }*/


    $('body').on('click','.coach_send_direct_message', function (e) {

        if(!$('.coach_send_direct_message').hasClass('clicked'))
        {
            var id = $(e.target).data('id');
            var recomm_message = ($(e.target).data('recomm_message'));
            if(id != '' && recomm_message != '')
            {
                $('.coach_send_direct_message').addClass('clicked');
                $('.coach_send_direct_message').attr('disabled', 'disabled');
                formData = new FormData();
                formData.append( 'id', id);
                formData.append( 'send_message', recomm_message);

                $('#loding_icon').trigger('click');
                $.ajax({
                    url: APP_BASE_URL +'/saveDirectMessage',
                    data: formData,
                    type: 'POST',
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (response) {

                        if (response.success != undefined) {
                            toastr["success"]("Recommended Message Sent Successfully", "Recommended Messages");
                            $('#member_list').DataTable().ajax.reload(null, false);
                        }
                        else if (response.error != undefined) {
                            toastr["error"](response.error, "Recommended Messages");
                        }
                        window.setTimeout(function() { App.stopPageLoading();}, 1000);
                        $('.coach_send_direct_message').removeClass('clicked');
                        $('.coach_send_direct_message').removeAttr('disabled', 'disabled');
                    }
                });
            }
        }


    });


});// End of JS



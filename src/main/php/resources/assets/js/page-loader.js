/**
 * Created by rpatidar on 31/08/2017.
 */
jQuery(document).ready(function ($) {

    /**
     * To Show page loading animation
     */
    var UIBlockUI = function() {
        var handleSample = function() {

            $('#loding_icon').click(function() {
                App.startPageLoading({animate: true});
            });
        }
        return {
            init: function() {
                handleSample();
            }
        };
    }();
    UIBlockUI.init();


    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };




});
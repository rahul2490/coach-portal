/**
 * Created by rpatidar on 31/08/2017.
 */
jQuery(document).ready(function ($) {

    $('#table-my-group-detail').dataTable({
        pageLength: 50,
        "language": {
            "emptyTable": "No Members Available",
            "loadingRecords": "Please wait - loading..."
        },
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [
            {className: 'control',
                orderable: false, targets: [0]},
        ],
        bLengthChange: false,
        bFilter: false

    });


    $('[data-toggle="tooltip"]').tooltip();

});



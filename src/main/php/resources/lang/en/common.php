<?php
/**
 * User: rpatidar
 * Date: 31/08/2017
 */

return [

	/*
	|--------------------------------------------------------------------------
	| Common Language Lines
	|--------------------------------------------------------------------------
	|
	|
	*/

	'internal_server_error'         => 'An unexpected error occurred. Please refresh the page or try again after some time.',
	'invalid_request_data'          => 'Please check the data you have submitted and try again.',
	'table_loading_records'         => 'Loading Records',
	'table_no_records'              => 'No Records found.',
	'record_stored_updated_success' => 'Record :action successfully|Records :action successfully',
	'redirecting_please_wait'       => 'Redirecting....please wait or click <a href=":location">here</a>',
	'confirm_record_action'         => 'Are you sure, you want to :action this :element?',
	'no_suggested_meals'            => 'Suggested List is empty',
	'list_empty_message'            => ':element List is empty',
	'no_suggested_meal_create_one'  => 'Member has no Suggested Meals. Please create some.',
	'no_recent_meal'                => 'Member has no Recent Meals.',
	'no_of_meals_logged'            => 'Meal logged: :count|Meals logged: :count',
	'meals'                         => ':count meal|:count meals',
	'unable_to_send_email'          => 'Unable to send email, Please contact admin or try again after some time.',
	'unable_to_store_content'       => 'Unable to store content, Please contact admin or try again after some time.',
	'unable_to_do_action'           => 'Unable to :action, Please contact admin or try again after some time.',
	'cal'                           => 'calorie|calories',
	'note_greeting_will_be_added'   => 'Note: Greeting message will be added automatically',

    'unable_to_update_password'     => 'Please fill all required fields',
    'wrong_password'                => 'Wrong Old Password',
    'password_already_entered'      => 'Password already entered in previous 1 year',


    'no_message_history'            => 'No Message History',
    'new_coach_add'      => 'Coach added successfully',
    'coach_edit'      => 'Coach updated successfully',

    'motivation_image_add'          =>  'Motivation Image Added Successfully',
    'motivation_image_add_title'    =>  'Add Motivation',
    'user_profile_image_add'          =>  'Profile Image Added Successfully',
    'user_profile_image_add_title'    =>  'Add Profile Image',
    'add_new_notes'         =>  'Notes added successfully',
    'add_new_notes_title'   =>  'Add Notes'
];

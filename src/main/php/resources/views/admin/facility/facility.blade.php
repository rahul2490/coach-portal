@extends('include.admin-layout')

@section('page-title')
    Facility
@stop

@section('body-class')

@stop


@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->

        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route('admin.home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Facility</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">

                                        <div class="caption font-blue-ebonyclay">
                                            <i class="icon-settings font-blue-ebonyclay"></i>
                                            <span class="caption-subject bold uppercase">Facilities</span>
                                        </div>
                                        <div class="pull-right">
                                            <a class="btn green-jungle" href="#modal_newFacilityAdmin" data-backdrop="static" data-keyboard="false" data-toggle="modal">
                                                <i class="fa fa-tasks"></i> New Facility Admin
                                            </a>
                                            <a class="btn green-jungle" href="{{route('admin.add_facility')}}">
                                                <i class="fa fa-tasks"></i> Add Facility
                                            </a>
                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        @if(Session::has('success'))
                                            <div class="alert alert-warning margiv-top-10">
                                                <button class="close" data-close="alert"></button>
                                                <span> {!! Session::get('success') !!} </span>
                                            </div>
                                        @endif



                                        <table id="admin_facility_list" data-action="{{ route('admin.facility') }}" class="table table-striped table-bordered table-hover dt-responsive" aria-describedby="table_info" >
                                            <div class="alert alert-warning alert-dismissable" style="display:none">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                <span>Facility removed Successfully.</span>
                                            </div>
                                            <thead>
                                            <tr role="row">
                                                <th width="1%"></th>
                                                <th width="10%" class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 1px;" aria-label="">Facility ID</th>
                                                <th width="5%" class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 40px;" aria-label="">Facility Name</th>
                                                <th width="5%" class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 87px;" aria-label="">Contact Person Name</th>
                                                <th width="5%" class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 70px;" aria-label="">Contact Number</th>
                                                <th width="5%" class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 100px;" aria-label="">Notification Enabled? </th>
                                                <th width="5%" class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 50px;" aria-label="">Skip Recruitment Consent Form</th>
                                                {{--<th class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 50px;" aria-label="Type: activate to sort column ascending">Facility Admin Name</th>
                                                <th class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 50px;" aria-label="Type: activate to sort column ascending">Facility Admin Email</th>--}}
                                                <th width="5%" class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 80px;" aria-label="Action: activate to sort column ascending">Action</th>
                                            </tr>
                                            </thead>
                                            {{--<thead>--}}
                                            {{--<tr role="row">--}}


                                                {{--<th  class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" width="5%">Facility ID</th>--}}
                                                {{--<th width="5%">Facility Name</th>--}}
                                                {{--<th width="5%" data-orderable='false'>Contact Person Name</th>--}}
                                                {{--<th width="5%">Contact Number</th>--}}
                                                {{--<th width="5%">Notification Enabled? </th>--}}
                                                {{--<th width="5%"  data-orderable='false'>Skip Recruitment Consent Form</th>--}}
                                                {{--<th width="10%"  data-orderable='false'>Facility Admin Name</th>--}}
                                                {{--<th width="5%"  data-orderable='false'>Facility Admin Email</th>--}}
                                                {{--<th width="5%"  data-orderable='false'>Action</th>--}}

                                            {{--</tr>--}}
                                            {{--</thead>--}}
                                            <tbody role="alert" aria-live="polite" aria-relevant="all">



                                                    @foreach($facility as $facility)
                                                        <tr role="row" class="" data-row_id="">
                                                            <td></td>

                                                            <td class="dt-right">{{ $facility->facility_id }}</td>
                                                            <td>{{$facility->name  }}</td>
                                                            <td><span class="word-break">{{ $facility->contact_person_name}}</span></td>
                                                            <td class="dt-right"><span class="phone-number">{{ $facility->contact_info }}</span></td>
                                                            <td>{{ ($facility->is_notification_enabled == 1) ? 'Yes' : 'No' }}</td>
                                                            <td>{{ ($facility->is_skip_consent == 1) ? 'Yes' : 'No' }}</td>
                                                            {{--<td>{{ $facility->user_name}}</td>
                                                            <td>{{ $facility->email}}</td>--}}
                                                            <td><a href="{{ route('admin.edit_facility', ['coach_id' => base64_encode($facility->facility_id)])  }}" data-id="{{ $facility->facility_id }}"  class="btn font-dark btn-hide sbold"> <i class="fa fa-edit"></i></a></td>

                                                        </tr>

                                                    @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->


    <!--Add Facility Admin-->
    <div class="modal fade in" id="modal_newFacilityAdmin" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Create New Facility Admin </h4>
                </div>
                <div class="modal-body">
                    <div class="row profile">
                        <div class="col-sm-12">
                            {!! Form::open( ['method' => 'POST', 'id' => 'create-new-facility-admin', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('admin.create-new-facility-admin')] ) !!}
                            <div class="form-body">
                                <div class="form-group">
                                    <input id="" name="facility_id" class="form-control" value="{{$facility->facility_id}}" type="hidden">
                                    <label for="multiple" class="control-label"> Select Facility
                                    </label>
                                    <div class="input-group select2-bootstrap-append">
                                            <select id="select_facility_list" class="form-control select_facility_admin_new" name="select_facility[]" multiple>
                                                @foreach($facility_list as $facility)
                                                    <option value="{{$facility->facility_id}}">{{$facility->name}}</option>
                                                @endforeach

                                            </select>
                                        <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" data-select2-open="select_facility_admin">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">First Name </label>
                                    <input class="form-control" name="admin_fname"  type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Last Name</label>
                                    <input class="form-control" name="admin_lname"    type="text">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Email address </label>
                                    <input  name="admin_email" class="form-control"  type="text">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Phone </label>
                                    <input  name="admin_phone" class="form-control"  type="text">
                                </div>

                                <div class="form-group col-md-12 text-right">
                                    <button type="button" class="btn  btn-default" data-dismiss="modal" > &nbsp; Cancel &nbsp; </button>
                                    <button type="submit" class="btn green-jungle"> &nbsp; Save &nbsp; </button>
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@stop

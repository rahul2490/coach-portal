
@extends('include.admin-layout')

@section('page-title')
Facility
@stop

@section('body-class')

@stop


@section('content')

<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container-fluid">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="{{route('admin.home')}}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Facility</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="mt-content-body">
                {!! Form::open( array('url' => URL::route('admin.facility-store'), 'method' => 'post', 'id' => 'form_facilityedit', 'role' => 'form') ) !!}
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">

                                <div class="caption font-blue-ebonyclay">
                                    <i class="icon-settings font-blue-ebonyclay"></i>
                                    <span class="caption-subject bold uppercase">Edit Facility</span>
                                </div>
                                <div class="pull-right"> <a class="btn green-jungle" href="#modal_addFacilityAdmin" data-toggle="modal">
                                        <i class="fa fa-tasks"></i> Add Facility Admin
                                    </a>
                                </div>

                            </div>
                            <div class="portlet-body">
                            @if(Session::has('success'))
                            <div class="alert alert-warning margiv-top-10">
                                <button class="close" data-close="alert"></button>
                                <span> {!! Session::get('success') !!} </span>
                            </div>
                            @endif

                            @if($errors->has())
                            @foreach ($errors->all() as $error)
                            <div class="alert alert-danger margiv-top-10">
                                <button class="close" data-close="alert"></button>
                                <span> {{ $error }} </span>
                            </div>
                            @endforeach
                            @endif

                            <div class="col-md-6">
                                <h3 class="form-section">Details</h3>
                                <div class="form-group">
                                    <label class="control-label">Facility Name </label>
                                    <input type="text" placeholder="Facility Name" class="form-control" value="{{$facility->name}}" name="name" />
                                    <input id="" name="facility_id" class="form-control" value="{{$facility->facility_id}}" type="hidden">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">State</label>
                                    {!! Form::select('state', $state, $facility->state,  ['class' =>'form-control']) !!}
                                </div>

                                <div class="form-group">
                                    <label class="control-label">City</label>
                                    <input class="form-control" name="city" required value="{{$facility->city}}" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Zip</label>
                                    <input class="form-control" name="zip"  required="" value="{{$facility->zip}}" type="text">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Address</label>
                                    <textarea class="form-control" name="address" rows="2" placeholder="Address">{{$facility->address}}</textarea>

                                </div>
                                <div class="form-group">
                                    <label class="control-label">Time Zone</label>
                                    {!! Form::select('timezone', $timezone, $facility->timezone_offset,  ['class' =>'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3 class="form-section">Contact Info</h3>
                                <div class="form-group">
                                    <label class="control-label">Contact Number </label>
                                    <input class="form-control" name="contact_info" required="" value="{{$facility->contact_info}}" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Contact Person Name</label>
                                    <input class="form-control" name="contact_person_name"  required="" value="{{$facility->contact_person_name}}" type="text">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Partner ID </label>
                                    <input id="partner_id" name="partner_id" class="form-control" value="{{$facility->partner_id}}" type="text">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">CDC Organization Code</label>
                                    <input id="cdc_organization_code" name="cdc_organization_code" class="form-control" value="{{$facility->cdc_organization_code}}" type="text">
                                </div>

                                <div class="form-group">
                                    <div class="mt-checkbox-inline">
                                        <label class="mt-checkbox">
                                            {!! Form::checkbox('is_notification_enabled', 1, $facility->is_notification_enabled, ['class' => 'field']) !!}  Notifications enabled
                                            <span></span>
                                        </label>
                                        <label class="mt-checkbox">
                                            {!! Form::checkbox('is_skip_consent', 1, $facility->is_skip_consent, ['class' => 'field']) !!} Skip Consent Form
                                            <span></span>
                                        </label>
                                        <label class="mt-checkbox">
                                            {!! Form::checkbox('is_chatbot_enabled', 1, $facility->is_chatbot_enabled, ['class' => 'field']) !!} Enable Chatbot
                                            <span></span>
                                        </label>
                                    </div>
                                </div>



                                <div class="form-group">
                                    @if(count($facility_admin) > 0)
                                    <label for="multiple" class="control-label"> Facility Admins
                                    </label>
                                    <div class="facilityadminlist">
                                        <ul>
                                            <?php
                                            $facility_id=$facility->facility_id;
                                            ?>


                                            @foreach($facility_admin as $key => $admin)

                                            <li class=""> {{  $admin->full_name . ' (' . $admin->email . ')' }} @if(count($facility_admin)>1) <a title="Delete Group"  admin_id="{{  $admin->provider_id }}" facility_id="{{$facility_id}}" class="btn font-dark btn-sm btn-hide sbold pull-right facility_admin"> <i class="fa fa-remove"></i>  </a> @endif </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                </div>

                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="margiv-top-10">
                                <button type="submit" class="btn green-jungle">Submit</button>
                                {{--<button type="button" class="btn grey-salsa btn-outline">Cancel</button>--}}
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
</div>

<!-- END PAGE CONTENT BODY -->



<!--Add Facility Admin-->
<div class="modal fade in" id="modal_addFacilityAdmin" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Facility Admin to Facility</h4>
            </div>
            <div class="modal-body">
                <div class="row profile">
                    <div class="col-sm-12">
                        {!! Form::open( ['method' => 'POST', 'id' => 'assign_facility_to_facility_admin', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('admin.assign-facility-to-facility-admin')] ) !!}
                        <div class="form-body">
                            <div class="form-group">
                                <input id="" name="facility_id" class="form-control" value="{{$facility->facility_id}}" type="hidden">
                                <label for="multiple" class="control-label"> Select Admin
                                </label>
                                <div class="input-group select2-bootstrap-append">
                                    @if(count($cobrand_facility_admins) > 0)
                                    <select id="select_facility_admin" class="form-control select_facility_admin" name="select_facility_admin[]" multiple>
                                        @foreach($cobrand_facility_admins as $key => $admin)
                                        <option value="{{ $admin->providers_provider_id }}">{{  $admin->full_name }}</option>
                                        @endforeach
                                    </select>
                                    @else
                                        <select id="select_facility_admin" class="form-control select_facility_admin" name="select_facility_admin[]" multiple>
                                                <option value=""></option>
                                        </select>
                                    @endif
                                    <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" data-select2-open="select_facility_admin">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </span>
                                </div>
                            </div>

                            <div class="form-group col-md-12 text-right">
                                <button type="button" class="btn  btn-default" data-dismiss="modal" > &nbsp; Cancel &nbsp; </button>
                                <button type="submit" class="btn green-jungle"> &nbsp; Save &nbsp; </button>
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

@stop

@extends('include.admin-layout')

@section('page-title')
    Coaches
@stop

@section('body-class')

@stop


@section('content')

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container-fluid">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{route('admin.home')}}">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Coaches </span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="mt-content-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">

                                            <div class="caption font-blue-ebonyclay">
                                                <i class="icon-settings font-blue-ebonyclay"></i>
                                                <span class="caption-subject bold uppercase">Coaches</span>
                                            </div>
                                            <div class="actions">

                                                <a class="btn green-jungle" href="{{ route('admin.coach-edit', ['coach_id' => 'add']) }}">
                                                    <i class="fa fa-plus"></i> Add Coach
                                                </a>
                                            </div>

                                        </div>
                                        <div class="portlet-body">
                                            @if(Session::has('message'))

                                                <div class="alert alert-warning margiv-top-10">
                                                    <button class="close" data-close="alert"></button>
                                                    <span> {!! Session::get('message') !!} </span>
                                                </div>

                                            @endif

                                            <table id="admin_coach_list" data-action="{{ route('admin.coachesList') }}" class="table table-striped table-bordered table-hover dt-responsive" aria-describedby="table_info" >
                                                <div class="alert alert-warning alert-dismissable" style="display:none">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <span>Coach removed Successfully.</span>
                                                </div>
                                                <thead>
                                                    <tr role="row">

                                                        <th ></th>
                                                        <th width="5%">Coach ID</th>
                                                        <th width="5%">Name</th>
                                                        <th width="10%" data-orderable='false'>Email</th>
                                                        <th width="5%">Phone</th>
                                                        <th width="5%">Type </th>
                                                        <th width="5%"  data-orderable='false'>SMS/Email Alert</th>
                                                        <th width="15%"  data-orderable='false'>Assigned Facility</th>
                                                        <th width="3%"  data-orderable='false'>Action</th>

                                                    </tr>
                                                </thead>
                                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                                    <?php /*
                                                    <?php $div_flag = false ?>
                                                    @foreach($coaches as $key => $coach)
                                                        <tr role="row" class="{{ $div_flag ? 'odd' : 'even' }}" data-row_id="{{ $coach->provider_id }}">
                                                            <td></td>
                                                            <td>{{ $coach->provider_id }}</td>
                                                            <td>{{ $coach->first_name . ' ' . $coach->last_name }}</td>
                                                            <td><span class="word-break">{{ $coach->email}}</span></td>
                                                            <td><span class="phone-number">{{ $coach->phone }}</span></td>
                                                            <td>{{ $coach->user_type }}</td>
                                                            <td>{{ ($coach->is_email_enabled == 1) ? 'Yes' : 'No' }}</td>
                                                            <td>{{ ($coach->is_sms_enabled == 1) ? 'Yes' : 'No'}}</td>
                                                            <td><a data-toggle="modal" data-id="{{ $coach->provider_id }}" data-target="#remove_coach" class="btn dark btn-sm btn-outline sbold uppercase">Remove <i class="fa fa-trash"></i></a></td>
                                                        </tr>
                                                        <?php $div_flag = !$div_flag  ?>
                                                    @endforeach */ ?>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

        <div class="modal" id="remove_coach">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Remove Coach</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open( ['method' => 'POST', 'id' => 'delete-msg', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('admin.delete-coach') ] ) !!}
                        {!! Form::hidden('id', '' ) !!}
                        Are you sure you want to remove the Coach from this facility?
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" id="remove_coach_from_facility">Remove</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->



        <!--Edit Facility Admin-->
        <div class="modal fade in" id="modal_EditFacilityAdmin" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Edit Facility Admin </h4>
                    </div>
                    <div class="modal-body">
                        <div class="row profile">
                            <div class="col-sm-12">
                                {!! Form::open( ['method' => 'POST', 'id' => 'edit-facility-admin', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('admin.edit-facility-admin'), 'update' => route('admin.update-facility-admin')] ) !!}
                                <div class="form-body">
                                    <div class="form-group">
                                        <input id="" name="provider_id" class="form-control" value="" type="hidden">
                                        <label for="multiple" class="control-label"> Select Facility
                                        </label>
                                        <div class="input-group select2-bootstrap-append">
                                            <select id="select_facility_list" class="form-control select_facility_admin_new" name="select_facility[]" multiple>
                                                @foreach($facility_list as $facility)
                                                    <option value="{{$facility->facility_id}}">{{$facility->name}}</option>
                                                @endforeach

                                            </select>
                                            <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" data-select2-open="select_facility_admin">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">First Name </label>
                                        <input class="form-control" name="admin_fname"  type="text">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Last Name</label>
                                        <input class="form-control" name="admin_lname"    type="text">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Email address </label>
                                        <input  name="admin_email" class="form-control"  type="text" disabled>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Phone </label>
                                        <input  name="admin_phone" class="form-control"  type="text">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Assigned Facility</label>
                                        <div class=" assigned-facility-list">
                                            <ul class="row">

                                            </ul>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12 text-right">
                                        <button type="button" style="float:left;" class="btn green-jungle" id ="admin_coach_reset_password_btn" >Resend Email/Pin Code</button>
                                        <button type="button" class="btn  btn-default" data-dismiss="modal" > &nbsp; Cancel &nbsp; </button>
                                        <button type="submit" class="btn green-jungle"> &nbsp; Save &nbsp; </button>
                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

@stop

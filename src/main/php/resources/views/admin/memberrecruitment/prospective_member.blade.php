@extends('include.admin-layout')

@section('page-title')
   Prospective  Members
@stop

@section('body-class')

@stop

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->

        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route('admin.home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Prospective Members</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-blue-ebonyclay">
                                            <i class="icon-settings font-blue-ebonyclay"></i>
                                            <span class="caption-subject bold uppercase">Prospective Members</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                       <!-- <select id="another_search_m" ><option value="">Select</option><option value="1">1</option><option value="2">2</option> -->
                                        </select>
                                        <table id="admin_prospective_member_list" data-action="{{ route('prospectivememberlist') }}" class="table table-striped table-bordered table-hover dt-responsive" aria-describedby="table_info" >
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 1px;" aria-label="Member ID: activate to sort column ascending"></th>
                                                <th class="sorting_desc" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 40px;" aria-label="ID: activate to sort column ascending">ID</th>
                                                <th class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 40px;" aria-label="MRN: activate to sort column ascending">MRN</th>
                                                <th class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 87px;" aria-label="Name: activate to sort column ascending">Name</th>
                                                <th class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 90px;" aria-label="Email: activate to sort column ascending">Email</th>
                                                <th class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 90px;" aria-label="Facility Name: activate to sort column ascending">Facility Name</th>
                                                <th class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 40px;" aria-label="phone: activate to sort column ascending">Phone</th>
                                                <th class="sorting_disabled " role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 50px;" aria-label="Requested/Invited: activate to sort column ascending">Requested/Invited</th>
                                                <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 40px;" aria-label="Date Requested/Invited">Date Requested/Invited</th>
                                                <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 40px;" aria-label="Consent Form Accepted?">Consent Form Accepted?</th>
                                                <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 40px;" aria-label="Action Assigned">Action Assigned</th>
                                                <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 40px;" aria-label="Patient Status">Patient Status</th>
                                                <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 80px;" aria-label="Action">Action</th>

                                            </tr>
                                            </thead>
                                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                                            <?php /*
                                                    <?php $div_flag = false ?>
                                                    @foreach($members as $key => $member)
                                                        <tr role="row" class="{{ $div_flag ? 'odd' : 'even' }}">
                                                            <td class=""></td>
                                                            <td class="">{{ $member->patient_id }}</td>
                                                            <td class="">{{ $member->first_name . ' ' . $member->last_name }}</td>
                                                            <td class=""><span class="word-break">{{ $member->email}}</span></td>
                                                            <td class=""><span class="phone-number">{{ ( ! empty( $member->registration_date ) ) ? date( 'm/d/Y', strtotime($member->registration_date) ) : '' }}</span></td>
                                                            <td class="">{{ $member->device_mac_address }}</td>
                                                            <td class="">{{ $member->app_version }}</td>
                                                        </tr>
                                                        <?php $div_flag = !$div_flag  ?>
                                                    @endforeach  */ ?>
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    <!-- bootstrap modals start here-->
    <!--BEGIN MODAL-->
    <div class="modal" id="modal_prospective_member">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Remove Member</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open( ['method' => 'POST', 'id' => 'prespective_member_delete_msg', 'onsubmit' => 'return false;','role' => 'form', 'url' => 'delete-prospectivemember' ] ) !!}
                    {!! Form::hidden('id', '' ) !!}
                    <div class="form-group">
                        <label> Select Patient State </label>
                        <select  class="form-control" name="patientstate" id="patientstate">
                            <option value="Active">Active</option>
                            <option value="Deleted">Deleted</option>
                            <option value="Pending">Pending</option>
                            <option value="Suspended">Suspended</option>
                            <option value="Terminated">Terminated</option>
                        </select>

                    </div>

                     Are you sure you want to remove this Member?
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="remove_prospective_member">Remove</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!-- /.modal -->

    <!-- bootstrap  mrn modals start here-->
    <!--BEGIN MODAL-->
    <div class="modal" id="modal_mrn">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Member's MRN</h4>
                </div>
                {!! Form::open( ['method' => 'POST', 'id' => 'member_mrn_form', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('mrnsave') ] ) !!}
                <div class="modal-body">
                    {!! Form::hidden('id', '' ) !!}
                    <div class="form-group">
                        <label> Enter MRN </label>
                        <input type="text " name="mrn" id="mrn" class="form-control" placeholder="Enter MRN">


                    </div>
                    <div class="form-group">
                        <label> Enter Confirm  MRN </label>
                        <input type="text " name="confirm_mrn" id="confirm_mrn" class="form-control" placeholder="Enter Confirm MRN">
                    </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-danger"  >Save</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <!-- /.modal -->



    <script></script>
















    <!-- form for invitation send -->

    {!! Form::open( ['method' => 'POST', 'id' => 'invitaiton_send_form', 'onsubmit' => 'return false;','role' => 'form', 'url' => 'invitationsend' ] ) !!}
    {!! Form::hidden('id', '' ) !!}

    {!! Form::close() !!}





@stop

@extends('include.admin-layout')

@section('page-title')
    My Members
@stop

@section('body-class')

@stop

@section('content')

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container-fluid">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{route('admin.home')}}">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>My Members</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="mt-content-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption font-blue-ebonyclay">
                                                <i class="icon-settings font-blue-ebonyclay"></i>
                                                <span class="caption-subject bold uppercase">My Members</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">

                                            <div style="" class="col-md-4 col-sm-4 custom-filter-sort">
                                                <div id="" class="row dataTables_filter"><label class="control-label col-sm-2">Filter:</label>
                                                    <div class="col-sm-8">
                                                        <select id="member_filter" class="form-control input-sm input-small input-inline sort-messages" >
                                                            <option value="all">All Members</option>
                                                            <option value="hidden_members">Hidden Members</option>
                                                            <option value="in_person_member">In Person Members</option>
                                                            <option value="online_member">Online Members</option>
                                                            <option value="member_without_group">Members Without Group</option>
                                                            <option value="member_group">Members By Group Name</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            @if($member_group_id != '')
                                                <div class="col-md-4 col-sm-4 custom-filter-sort group_filter">
                                            @else
                                                <div style="display: none" class="col-md-4 col-sm-4 custom-filter-sort group_filter">
                                            @endif
                                                <div class="row dataTables_filter"><label class="control-label col-sm-2">Group:</label>
                                                    <div class="col-sm-8">
                                                        {!! Form::select('group', $facility_group_list, $member_group_id ,  ['id' => 'group_filter' ,'class' =>'form-control input-sm input-small input-inline sort-messages']) !!}
                                                    </div>
                                                </div>
                                            </div>

                                                        <div style="display: none" class="col-md-4 col-sm-4 custom-filter-sort another_filter">

                                                            <div class="row dataTables_filter"><label class="control-label col-sm-2">Members Without Groups:</label>
                                                                <div class="col-sm-8">
                                                                    {{--{!! Form::select('group', $facility_group_list, $member_group_id ,  ['id' => 'another_filter' ,'class' =>'form-control input-sm input-small input-inline sort-messages']) !!}--}}
                                                                    <select id="another_filter" class="form-control input-sm input-small input-inline another_filter">
                                                                        <option value="">Select Type</option>
                                                                        <option value="all">All</option>
                                                                        <option value="offline_members">Offline</option>
                                                                        <option value="ennrolled_members">Enrolled</option>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                        </div>



                                            <table class="table table-bordered dt-responsive member-table member-list" style="width:100%;margin-bottom:70px!important;" id="member_list" data-action="{{ route('admin.membersList') }}">
                                                <thead>
                                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="birth_date" id="birth_date">
                                                <tr>
                                                    <th> <button class="btn btn-xs btn-bday grey" id="birthday_today" @if($today_birthdays_count <= 0){{'disabled'}} @endif > Birthdays Today @if($today_birthdays_count > 0) <span class="glyphicon glyphicon-certificate"></span> @endif </button></th>
                                                    <th colspan="2" style="text-align:center;">Status</th>
                                                    <th style="text-align:center;" colspan="2">Recent Messages</th>
                                                    <th colspan="2"></th>
                                                </tr>
                                                <tr>
                                                    <th ></th>
                                                    <th width="10%"><div class="col-sm-6">ID</div> <div class="col-sm-6">Name</div></th>
                                                    <th width="5%">Milestone Risk</th>
                                                    <th width="5%">Effort</th>
                                                    <th width="1%"> Received by Coach/<br>Unread</th>
                                                    <th width="15%"> Message </th>
                                                    <th width="1%">Week in Program</th>
                                                    <th width="6%" data-orderable='false'>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>


                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

        <!-- modal section start here -->
        <!--modal Assign Group recommended message -->
        <div class="modal fade" id="modal_assignGroup" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Assign Member to Group</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open( ['method' => 'POST', 'id' => 'assign_member_to_group', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('facilityadmin.member-group-detail') , 'assign-member-to-group' => route('facilityadmin.add-member-to-group') , 'remove-from-group' => route('facilityadmin.remove-member-from-group') ] ) !!}
                            {!! Form::hidden('uid', '' ) !!}
                            <div class="form-group ">
                                <h3 class="font-green sbold margin-0 member_name"> Brain S - 1183 </h3>
                                <h4 class="group-name font-dark sbold"> Andrea 6th Group </h4>
                            </div>

                            <div class="form-group">
                                <label for="group-list" class="control-label"> Select New Group
                                </label>
                                <div class="input-group select2-bootstrap-append">
                                    {!! Form::select('group_id', $facility_group_list, '' , ['id' => 'group-list' ,'class' =>'form-control input-sm input-small input-inline sort-messages']) !!}
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-select2-open="group-list">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                </div>

                                <div class="mt-checkbox-inline">
                                    <label class="mt-checkbox">
                                        <input id="inlineCheckbox21" value="option1" type="checkbox">  Select Groups Only With Capacity
                                        <span></span>
                                    </label>
                                </div>
                                <div class="grp-selct-msg"></div>
                            </div>

                            <div class="form-group text-right">
                                <button type="button" class="btn btn-default btn-unset-grp" aria-hidden="true" id="remove_member_from_group"> &nbsp; Remove &nbsp; </button>
                                <button type="submit" class="btn green-jungle" id="add_to_group_submit"> &nbsp; Add &nbsp; </button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        @include('common.direct_message')

        <!-- modal section end here -->


        <div class="modal" id="member_detail">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Member Details</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open( ['method' => 'GET', 'id' => 'patient_detail', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('facilityadmin.member-detail') ] ) !!}
                        {!! Form::hidden('id', '' ) !!}
                        <ul class="member_data">
                        </ul>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->



            <!-- bootstrap modals start here-->
            <!--BEGIN MODAL-->
            <div class="modal" id="modal_prospective_member">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Remove Member</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open( ['method' => 'POST', 'id' => 'prespective_member_delete_msg', 'onsubmit' => 'return false;','role' => 'form', 'url' => 'delete-prospectivemember' ] ) !!}
                            {!! Form::hidden('id', '' ) !!}
                            <div class="form-group">
                                <label> Select Patient State </label>
                                <select  class="form-control" name="patientstate" id="patientstate">
                                    <option value="Active">Active</option>
                                    <option value="Deleted">Deleted</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Suspended">Suspended</option>
                                    <option value="Terminated">Terminated</option>
                                </select>

                            </div>

                            Are you sure you want to remove this Member?
                            {!! Form::close() !!}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-danger" id="remove_prospective_member">Remove</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

@stop


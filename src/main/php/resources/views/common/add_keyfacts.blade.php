
<!--BEGIN MODAL Add key facts-->
<div class="modal fade in" id="modal_addkeyfacts" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Key facts</h4>
            </div>
            <div class="modal-body">
                <form role="form" class="">
                    <div class="form-body">
                        <div class="form-group">
                            <label for="multiple" class="control-label"> Add Key Facts
                            </label>
                            <textarea class="form-control" placeholder="Type key facts">

                                                </textarea>
                        </div>
                        <div class="form-group col-md-12 text-right">
                            <button data-dismiss="modal" aria-hidden="true" type="button" class="btn btn-default"> &nbsp; Cancel &nbsp; </button>
                            <button type="button" class="btn green-jungle"> &nbsp; Add &nbsp; </button>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </form>

                <div class="clearfix"></div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
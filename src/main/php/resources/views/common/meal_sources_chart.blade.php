<!--meal sources -->
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption caption-md">
            <span class="caption-subject font-green-jungle  bold">Meals Sources</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided" data-toggle="buttons">

                <label class="active btn btn-transparent btn-no-border blue-oleo btn-outline btn-circle btn-sm daily-switch">
                    <input type="radio" name="options" class="toggle " id="option2">Daily</label>
                <label class="btn btn-transparent btn-no-border blue-oleo btn-outline btn-circle btn-sm weekly-switch">
                    <input type="radio" name="options" class="toggle " id="option2">Weekly</label>
            </div>
        </div>
    </div>
    <div class="portlet-body daily">
        <div  class="row text-right">
            <a class="btn patient_meal_source_daily left"><i class="fa fa-angle-left font-green"></i></a>
            <a class="btn meal_source_daily">{{ $date_array['daily_start'] }} - {{ $date_array['daily_end'] }}</a>
            <a class="btn patient_meal_source_daily right"><i class="fa fa-angle-right font-green"></i></a>
        </div>

        <div id="highchart_5" class="margin-top-20" style="height:175px;"></div>
    </div>
    <div id="portlet-weekly-mealsource" class="portlet-body weekly">
        <div  class="row text-right">
            <a class="btn weekly_patient_meal_source left"><i class="fa fa-angle-left font-green"></i></a>
            <a class="btn weekly_meal_source">{{ $date_array['monthly_start'] }} - {{ $date_array['monthly_end'] }}</a>
            <a class="btn weekly_patient_meal_source right"><i class="fa fa-angle-right font-green"></i></a>
        </div>

        <div id="highchart_5-w" class="margin-top-20" style="height:175px;"></div>
    </div>
</div>

@if(isset($patient_meal_source_daily))
    <script type="application/javascript">
        var meal_source_daily_actual = [];
        try{
            meal_source_daily_actual = JSON.parse('{!! json_encode($patient_meal_source_daily['daily_meal_source_actual']) !!}');
        }catch(e){}

        var meal_source_daily_plan = [];
        try{
            meal_source_daily_plan = JSON.parse('{!! json_encode($patient_meal_source_daily['daily_meal_source_plan']) !!}');
        }catch(e){}

        var meal_source_weekly_actual = [];
        try{
            meal_source_weekly_actual = JSON.parse('{!! json_encode($patient_meal_source_weekly['weekly_meal_source_actual']) !!}');
        }catch(e){}
        var meal_source_weekly_plan = [];
        try{
            meal_source_weekly_plan = JSON.parse('{!! json_encode($patient_meal_source_weekly['weekly_meal_source_plan']) !!}');
        }catch(e){}
    </script>
@endif
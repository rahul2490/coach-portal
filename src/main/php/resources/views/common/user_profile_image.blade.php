<!--BEGIN MODAL motivation -->
<div class="modal fade in" id="modal_add_profile_image"  role="basic" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Profile Image</h4>
            </div>
            <div class="modal-body">
                <!--start my motivation-->
                {!! Form::open( ['method' => 'POST', 'id' => 'add_user_profile_image', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('user-profile-image-update') ] ) !!}
                    <input type="hidden" id="user_profile_file_name" name="user_profile_file_name">
                    <input type="hidden" id="patient_id" name="patient_id" value="{{ $patient_id }}">
                    <input type="file"   name="upload_user_image" id="upload_user_image" style="display:none">
                    <div class="form-body">
                        <div class=" col-sm-6 col-sm-offset-3 margin-top-20 profile my-motivation">
                            <ul class="list-unstyled profile-nav">
                                <li>
                                    <img src="{{ env('PROFILE_IMAGE_BASE_URL'). $patient_info->image_path}}" class="img-responsive pic-bordered user_profile_image" alt="">
                                    <a href="javascript:;" class="profile-edit" id="user_profile_image_attachment"> edit </a>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-12 text-right">
                            <button data-dismiss="modal" aria-hidden="true" type="button" class="btn btn-default"> &nbsp; Cancel &nbsp; </button>
                            <button type="submit" class="btn green-jungle add"> &nbsp; Save &nbsp; </button>
                        </div>
                    </div>
                {!! Form::close() !!}
                <!--end motivation-->
                <div class="clearfix"></div>
                <style>
                    #upload_user_image-error
                    {
                        color:red;
                    }
                </style>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
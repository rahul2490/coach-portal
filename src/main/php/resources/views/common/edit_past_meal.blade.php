
@extends('include.admin-layout')

@section('page-title')
    Past Meal | Healthslate
@stop

@section('body-class')

@stop

@section('content')
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container-fluid">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{route(session('userRole').'.home')}}">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <a href="{{env("APP_BASE_URL").session('userRole').'/dashboard/'.$profile_detail->patient_id}}">Dashboard</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>

                            <a href="{{env("APP_BASE_URL").session('userRole').'/pastmeal/'.$profile_detail->patient_id}}">Past Meal</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span> Edit Past Meal</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="row">
                            <div class="col-md-12">

                                <!-- BEGIN PROFILE SIDEBAR -->
                            @include('common.dashboard_profile_sidebar')
                                <!-- END  PROFILE SIDEBAR -->
                                <!-- BEGIN PROFILE CONTENT -->
                                <div class="profile-content">
                                    <div class="row">
                                        <div class="col-sm-12 text-right margin-btm-10">
                                            <a class="btn btn-link" href="{{env("APP_BASE_URL").session('userRole').'/pastmeal/'.$profile_detail->patient_id}}" data-toggle=""> Past Meals <i class="fa fa-cutlery"></i> </a>
                                            @if(session('userRole')=="leadcoach")
                                                <a class="btn btn-link" href="{{ route('leadcoach.group-wall',['group_id'=>base64_encode($patient_info->group_id)] ) }}" data-toggle="" @if($patient_info->group_id==""){{ "disabled" }} @endif> Group Wall <i class="fa fa-users"></i> </a>
                                            @elseif(session('userRole')=="foodcoach")
                                                <a class="btn btn-link" href="{{ route('foodcoach.group-wall',['group_id'=>base64_encode($patient_info->group_id)] ) }}" data-toggle="" @if($patient_info->group_id==""){{ "disabled" }} @endif> Group Wall <i class="fa fa-users"></i> </a>
                                            @elseif(session('userRole')=="facilityadmin")
                                                <a class="btn btn-link" href="{{ route('facilityadmin.group-wall',['group_id'=>base64_encode($patient_info->group_id)] ) }}" data-toggle="" @if($patient_info->group_id==""){{ "disabled" }} @endif> Group Wall <i class="fa fa-users"></i> </a>
                                             @endif
                                            <a class="btn btn-link" href="#" data-toggle=""> Video Call <i class="fa fa-video-camera"></i> </a>
                                            <a class="btn btn-link" href="{{env("APP_BASE_URL").session('userRole').'/logbook/'.$profile_detail->patient_id}}" data-toggle=""> Log Book <i class="fa fa-file-text-o"></i> </a>
                                            <a class="btn btn-link" href="{{env("APP_BASE_URL").session('userRole').'/setting/'.$profile_detail->patient_id}}" data-toggle=""> Settings <i class="fa fa-gear"></i> </a>


                                        </div>

                                        <div class="col-md-12">

                                            <!--section 1 group charts -->
                                            <div class="portlet light bordered ">
                                                <!--      <div class="portlet-title">
                                                                       <div class="caption">
                                                                          <i class="fa fa-list-alt font-dark"></i>
                                                                          <span class="caption-subject font-dark bold uppercase">Food-Breakfast</span>
                                                                      </div>


                                                                  </div> -->
                                                <div class="portlet-body">
                                                    <div class="row profile">
                                                        <div class="col-md-3">
                                                            <ul class="list-unstyled profile-nav">
                                                                @foreach($past_meal_detail as $detail)
                                                                <li>
                                                                    <img src="{{$detail['image_name']}}" class="img-responsive pic-bordered" alt="" />
                                                                </li>



                                                                <!--<li>
                                                                    <a href="javascript:;"> Messages
                                                                        <span> 3 </span>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:;"> Friends </a>
                                                                </li>
                                                                <li>
                                                                    <a href="javascript:;"> Settings </a>
                                                                </li> -->
                                                            </ul>
                                                            <!--  <a href="javascript:;" class="font-dark btn-block text-center"> <i class="fa fa-heart font-red" ></i> Add to Favorites </a> -->
                                                        </div>
                                                        <div class="col-md-9">
                                                            <div class="row">
                                                                <div class="col-md-8 profile-info">
                                                                    <h1 class="font-green sbold uppercase">  {{$detail['meal_name']}} </h1>

                                                                    <p class=" ">
                                                                        <div class="btn-group">
                                                                    <p class="font-green sbold">
                                                                        {{$detail['type']}}
                                                                    </p>
                                                                </div>
                                                                </p>

                                                                <p class="margin-top-20"> {{$detail['log_time']}} </p>
                                                                <h4>
                                                                    <span class="label label-danger">{{$total_calories}} Cal</span>
                                                                </h4>

                                                            </div>
                                                            <!--end col-md-8-->
                                                            <div class="col-md-4">
                                                                <div class="portlet sale-summary">

                                                                    <div class="portlet-body">
                                                                        <ul class="list-unstyled">
                                                                            <li>
                                                                        <span class="sale-info"> Carbs
                                                                            <i class="fa fa-img-up"></i>
                                                                        </span>
                                                                                <span class="sale-num"> {{$total_carbs}} <small>g</small></span>
                                                                            </li>
                                                                            <li>
                                                                        <span class="sale-info"> Fats
                                                                            <i class="fa fa-img-down"></i>
                                                                        </span>
                                                                                <span class="sale-num"> {{$total_fats}} <small>g</small></span>
                                                                            </li>
                                                                            <li>
                                                                                <span class="sale-info"> Proteins</span>
                                                                                <span class="sale-num"> {{$total_protein}} <small>g</small> </span>
                                                                            </li>
                                                                            <li>
                                                                                <span class="sale-info"> TOTAL CALORIES </span>
                                                                                <span class="sale-num"> {{$total_calories}} <small>Cal</small> </span>
                                                                            </li>
                                                                            @endforeach
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--end col-md-4-->
                                                        </div>
                                                        <!--end row-->

                                                    </div>
                                                </div>
                                                <div class="row profile">
                                                    <div class="portlet-body col-sm-12">
                                                        <table class="table table-striped table-bordered table-advance table-hover">
                                                            <thead>
                                                            <tr>
                                                                <th>Items </th>
                                                                <th>Quantity </th>
                                                                <th>Unit </th>
                                                                <th>Calories </th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($item_detail as $idetail)
                                                            <tr>
                                                                <td>{{$idetail->food_name}}</td>
                                                                <td class="hidden-xs"> {{$idetail->quantity}} </td>
                                                                <td>{{$idetail->unit}}</td>
                                                                <td> {{round($idetail->quantity*$idetail->calories,1)}}</td>
                                                              </tr>
                                                                @endforeach


                                                            </tbody>
                                                        </table>






                                                    </div>


                                                </div>






                                                <div class="clearfix"></div>
                                            </div>
                                        </div>








                                    </div>
                                </div>
                            </div>
                            <!-- END PROFILE CONTENT -->
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

    </div>
    <!-- END CONTAINER -->
    @stop

@include('common.motivation')
@include('common.user_profile_image')











@extends('include.admin-layout')

@section('page-title')
    Logbook
@stop

@section('body-class')

@stop

@section('content')
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <!-- BEGIN PAGE CONTENT BODY -->
                    <div class="page-content">
                        <div class="container-fluid">
                            <!-- BEGIN PAGE BREADCRUMBS -->
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{route(session('userRole').'.home')}}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="{{env("APP_BASE_URL").session('userRole').'/dashboard/'.$patient_info->patient_id}}">Dashboard</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Logbook</span>
                                </li>
                            </ul>


                            <!-- END PAGE BREADCRUMBS -->
                            <!-- BEGIN PAGE CONTENT INNER -->
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-12">

                                        <!-- BEGIN PROFILE SIDEBAR -->
                                       {{-- <div class="profile-sidebar">
                                            <button type="button" class="btn btn-outline btn-icon-only green btn-sm pull-right"><i class="fa fa-pencil"></i></button>
                                            <!-- PORTLET MAIN -->
                                            <div class="portlet light profile-sidebar-portlet ">

                                                <!-- SIDEBAR USERPIC -->
                                                <div class="profile-userpic">
                                                    <img src="../assets/pages/media/profile/profile_user.jpg" class="img-responsive" alt=""> </div>
                                                <!-- END SIDEBAR USERPIC -->
                                                <!-- SIDEBAR USER TITLE -->
                                                <div class="profile-usertitle">
                                                    <div class="profile-usertitle-name"> Sundar Pichai <br/>
                                                        <small class="font-grey-salsa"> S_pichai | 19324 </small>
                                                    </div>
                                                    <!-- <div class="profile-usertitle-name"></div> -->
                                                    <div class="profile-usertitle-name"> <span class="badge badge-danger"> DPP-CALORIES </span></div>
                                                    <div class="profile-usertitle-job"> Andrea 6th Group </div>
                                                </div>
                                                <!-- END SIDEBAR USER TITLE -->
                                                <!-- SIDEBAR BUTTONS -->
                                                <div class="text-center margin-btm-10">
                                                    <a  class="btn btn-circle btn-icon-only grey-cascade"><i class="fa fa-phone"></i></a>
                                                    <a  class="btn btn-circle btn-icon-only grey-cascade "><i class="fa fa-envelope"></i></a>
                                                </div>
                                                <div class="clearfix"></div>
                                                <!-- END SIDEBAR BUTTONS -->

                                            </div>
                                            <!-- END PORTLET MAIN -->
                                            <!-- PORTLET group -->
                                            <div class="portlet light-green-bg">

                                                <div class="portlet-body">
                                                    <!-- start group info-->
                                                    <div class="col-sm-12  ">
                                                        <div class="col-sm-6 text-center">
                                                            <a class="btn btn-link font-lg font-green-jungle" href="javascript:;"><small> Week </small></br> <span class="sbold font-hg">8</span></a>
                                                        </div>
                                                        <div class="col-sm-6 text-center">
                                                            <a class="btn btn-link font-lg font-green-jungle" href="group-session-detail.html" title="Session detail"  data-toggle="tooltip" ><small> Session </small></br> <span class="sbold font-hg">8/12</span></a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <!--end group info-->

                                                </div>
                                            </div>



                                            <!-- PORTLET MAIN -->
                                            <div class="portlet light ">
                                                <!--start my motivation-->
                                                <div class="profile-desc-title font-dark margin-btm-10 "> Motivation </div>
                                                <div class="">
                                                    <!-- <h4 class="profile-desc-title"></h4> -->
                                                    <span class="profile-desc-text"> Lorem ipsum dolor sit amet diam nonummy nibh dolore. </span>
                                                    <div class="margin-top-20 profile my-motivation">
                                                        <ul class="list-unstyled profile-nav">
                                                            <li>
                                                                <img src="../assets/pages/img/page_general_search/09.jpg" class="img-responsive pic-bordered" alt="">
                                                                <a href="#modal_addmotivation" data-toggle="modal" class="profile-edit"> edit </a>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                </div>
                                                <!--end motivation-->


                                                <div class="portlet-body">
                                                    <!--    <div class="profile-desc-title font-dark margin-btm-10 "> Coach Info </div>
                                                           <div class="inbox">
                                                               <ul class="inbox-contacts">
                                                                   <li>
                                                                       <a href="javascript:;">
                                                                           <img class="contact-pic" src="../assets/pages/media/users/avatar4.jpg">
                                                                           <span class="contact-name">Adam Stone</span>
                                                                           <small class="text-right font-grey-salsa">Lifestyle Coach</small>
                                                                       </a>
                                                                   </li>
                                                                   <li>
                                                                       <a href="javascript:;">
                                                                           <img class="contact-pic" src="../assets/pages/media/users/avatar2.jpg">
                                                                           <span class="contact-name">Lisa Wong</span>
                                                                           <small class="text-right font-grey-salsa">Food Coach</small>
                                                                       </a>
                                                                   </li>
                                                               </ul>
                                                           </div>  -->

                                                    <div class="profile-desc-title font-dark margin-btm-10 "> Profile </div>
                                                    <div class="profile-desc-text">
                                                        <p>Type:<strong> Oraganizer </strong> </p>
                                                        <p>Readiness to Change:<strong> 6 </strong> </p>
                                                        <p>Preferred Coaching Style:<strong> Drill Sargent </strong> </p>
                                                        <p>Age:<strong> 42 </strong> </p>
                                                        <p>Race/Ethinicity:<strong> White, not Hispanic </strong> </p>
                                                        <p>Readiness to change at program start:<strong> 5</strong> </p>
                                                        <p>Barriers to success: <strong>Unhealthy lifestyles or eating patterns of family/partner</strong> </p>
                                                        <p id="keyfactsinfo" class="collapse">Key Facts:<strong> "Two Chidren Husband", "Likes meat and pot" </strong> </p>
                                                        <div class="clearfix"></div>
                                                        <p><a class="btn btn-sm btn-link font-dark pull-right"  data-toggle="collapse" data-target="#keyfactsinfo"> More </a></p>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    <div class="profile-desc-title font-dark margin-btm-10 "> Personality </div>
                                                    <div class="profile-desc-text">
                                                        <p>Extraversion:  <strong>4.5</strong></p>
                                                        <p>Agreeableness: <strong>2.7 </strong></p>
                                                        <p>Conscientiousness: <strong>7.8 </strong></p>
                                                        <p>Neuroticism: <strong>4.9 </strong></p>
                                                        <p>Openness to Experience: <strong>8.6 </strong></p>
                                                        <p>Past Weight Loss efforts: <strong>Have lost but always quickly regained </strong></p>
                                                        <p>Dietary restrictions: <strong>Gluten free, Egg free</strong></p>
                                                    </div>

                                                    <div class="profile-desc-title font-dark margin-btm-10 margin-top-20">  Tracking Info </div>
                                                    <div class="profile-desc-text">
                                                        <p>App:<strong> iOS </strong> </p>
                                                        <p>Device Version: <strong> 3.6 </strong> </p>
                                                        <p>Last Login Date: <strong> 06:20PM 26 AUG 2017</strong> </p>
                                                        <p>TimeZone: <strong> Newyork (America)</strong></p>
                                                        <p>Last Fitbit Sync: <strong> N/A </strong> </p>
                                                        <p>Body Scale: <strong>Nokia</strong> </p>

                                                    </div>


                                                </div>

                                            </div>
                                            <!-- END PORTLET MAIN -->






                                            <!--end links-->
                                        </div>--}}
                                        <!-- END  PROFILE SIDEBAR -->


                                        <!-- BEGIN PROFILE SIDEBAR -->
                                    @include('common.dashboard_profile_sidebar')
                                    <!-- END  PROFILE SIDEBAR -->
                                        <!-- BEGIN PROFILE CONTENT -->
                                        <div class="profile-content">
                                            <div class="row">
                                                @include('common.dashboard_header')

                                                <div class="col-md-12">

                                                    <!--section 1 group charts -->
                                                    <div class="portlet light ">
                                                        <div class="portlet-title ">
                                                            <div class="caption ">
                                                                <span class="caption-subject font-green-jungle bold ">Logbook</span>
                                                            </div>
                                                            <div class="actions">
                                                                <div class="btn-group btn-group-devided" data-toggle="buttons">

                                                                    <label class="active btn btn-transparent btn-no-border blue-oleo btn-outline btn-circle btn-sm week-switch">
                                                                        <input type="radio" name="options" value="1" class="toggle" id="option">Week</label>
                                                                   {{-- <label class="btn btn-transparent btn-no-border blue-oleo btn-outline btn-circle btn-sm month-switch">
                                                                        <input type="radio" name="options" value="2" class="toggle" id="option">Month</label>--}}
                                                                    <input type="hidden" id="patient_id" value="{{$patient_id}}">
                                                                </div>
                                                            </div>

                                                        </div>


                                                        <div class="portlet-body">
                                                            <div style="" class="col-md-4 col-sm-6">
                                                                <div id="" class="row "><label class="control-label col-sm-4">Filter by:</label>

                                                                    <div class="col-sm-8">

                                                                        <select id="FilterByType" class="form-control input-sm input-small input-inline">
                                                                            <option value="All">All</option>
                                                                            <option value="ActivityMinute">Activity Minutes</option>
                                                                            <option value="Breakfast">Breakfast</option>
                                                                            <option value="Lunch">Lunch</option>
                                                                            <option value="Snack">Snack</option>
                                                                            <option value="Dinner">Dinner</option>
                                                                            @if($patient_type==1)
                                                                            <option value="Glucose">Glucose</option>
                                                                            <option value="Medication">Medication</option>
                                                                            @endif
                                                                            <option value="Weight">Weight</option>
                                                                            <option value="Steps">Activity Steps</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div style="" class="col-md-4 col-sm-5">
                                                                <div id="" class="row "><label class="control-label col-sm-5">Fitbit Data:</label>
                                                                    <div class="col-sm-7">
                                                                        <select id="fit_bit_data" class="form-control input-sm input-small input-inline">
                                                                            <option>Select Days</option>
                                                                            <option value="1">1 Day</option>
                                                                            <option value="2">2 Day</option>
                                                                            <option value="3">3 Day</option>
                                                                            <option value="4">4 Day</option>
                                                                            <option value="5">5 Day</option>
                                                                            <option value="6">6 Day</option>
                                                                            <option value="7">7 Day</option>
                                                                            <option value="8">8 Day</option>
                                                                            <option value="9">9 Day</option>
                                                                            <option value="10">10 Day</option>
                                                                            <option value="11">11 Day</option>
                                                                            <option value="12">12 Day</option>
                                                                            <option value="13">13 Day</option>
                                                                            <option value="14">14 Day</option>
                                                                            <option value="15">15 Day</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-sm-6 text-right week_for_date_change portlet-body-for-date">
                                                              {{--  <a class="btn left date-filter">
                                                                    <i class="fa fa-angle-left font-green"></i>
                                                                </a>
                                                                <a class="btn date_range">10/26/2017 - 11/01/2017</a>
                                                                <a  class="btn right date-filter">
                                                                    <i  class="fa fa-angle-right font-green"></i>
                                                                </a>--}}

                                                                <a class="btn  week left"><i class="fa fa-angle-left font-green"></i></a>

                                                                <a class="btn week_date_range">{{ $date_array['week_start'] }} - {{ $date_array['week_end'] }}</a>

                                                                <a class="btn week right"><i class="fa fa-angle-right font-green"></i></a>


                                                            </div>

                                                           {{-- <div class="col-md-4 col-sm-6 text-right month_for_date_change portlet-body-for-date hidden">


                                                                <a class="btn  month left"><i class="fa fa-angle-left font-green"></i></a>

                                                                <a class="btn month_date_range">{{ $date_array['month_start'] }} - {{ $date_array['month_end'] }}</a>

                                                                <a class="btn month right"><i class="fa fa-angle-right font-green"></i></a>


                                                            </div>--}}



                                                            <div class="weight-log" id="full-log-book" >
                                                                <div class="col-sm-12 meal-log">

                                                                    @if(count($logbook_detail)>0)
                                                                    @foreach($logbook_detail as $detail=>$logs)

                                                                    <div class="meal-title col-sm-12"><strong>{{ $detail  }}</strong></div>
                                                                        @foreach($logs as $log)
                                                                            @if(in_array($log->log_type,array('Breakfast','Lunch','Dinner','Snack')))
                                                                                <?php $log_data= $log->calories;
                                                                                       $type=" Cal" ;
                                                                                $class="fa fa-cutlery";
                                                                                $div_class="log-btn meal";
                                                                                ?>
                                                                            @elseif(in_array($log->log_type,array('Weight')))
                                                                                <?php $log_data= $log->weight;
                                                                                $type=" lbs" ;
                                                                                $class="fa fa-dashboard";
                                                                                $div_class="log-btn weight";
                                                                                ?>

                                                                                  @if(isset($log->serviceName) && $log->serviceName =="NokiaScale")
                                                                                        <?php       $class = "iconhs-nokia_scale"; ?>
                                                                                    @elseif(isset($log->serviceName) && $log->serviceName =="HealthKit")
                                                                                        <?php       $class = "iconhs-healthkit_icon"; ?>
                                                                                    @elseif( isset($log->serviceName) && $log->serviceName =="GoogleFit")
                                                                                        <?php      $class = "iconhs-googlefit_icon"; ?>
                                                                                    @elseif( isset($log->serviceName) && $log->serviceName =="Manual")
                                                                                        <?php      $class = "iconhs-manual"; ?>
                                                                                    @elseif( isset($log->serviceName) && $log->serviceName =="Class")
                                                                                        <?php      $class = "fa fa-dashboard"; ?>
                                                                                    @elseif( isset($log->serviceName) && $log->serviceName =="Aria")
                                                                                        <?php      $class = "iconhs-weight_aria"; ?>
                                                                                    @elseif( isset($log->serviceName) && $log->serviceName =="AvivaMeter")
                                                                                        <?php      $class = "iconhs-avivameter"; ?>
                                                                                    @elseif( isset($log->serviceName) && $log->serviceName =="BodyTrace")
                                                                                        <?php      $class = "iconhs-bodytrace"; ?>
                                                                                    @endif

                                                                                @elseif(in_array($log->log_type, array('ActivityMinute','minutesFairlyActive','minutesVeryActive')))

                                                                                    @if($log->log_type == "ActivityMinute")
                                                                                    <?php
                                                                                        $log_data = $log->minutes_performed;
                                                                                        //$log->log_type = $log->activity_log_type;
                                                                                        $log->log_type = "Activity";
                                                                                    ?>
                                                                                    @else
                                                                                    <?php
                                                                                        $log_data = $log->steps;
                                                                                     ?>

                                                                                    {{--@if($log->activity_type!="")--}}
                                                                                    <?php
                                                                                       //$log->log_type = $log->activity_type;
                                                                                       $log->log_type = "Activity";
                                                                                    ?>
                                                                                    {{--@endif--}}
                                                                                    @endif
                                                                                    <?php
                                                                                    $type = " Min";
                                                                                    $class = "iconhs-shoe";
                                                                                    $div_class = "log-btn activity";
                                                                                    ?>

                                                                                        @if(isset($log->serviceName) && $log->serviceName =="fitbit")
                                                                                            <?php       $class = "iconhs-fitbit"; ?>
                                                                                        @elseif(isset($log->serviceName) && $log->serviceName =="HealthKit")
                                                                                            <?php       $class = "iconhs-healthkit_icon"; ?>
                                                                                        @elseif( isset($log->serviceName) && $log->serviceName =="GoogleFit")
                                                                                            <?php      $class = "iconhs-googlefit_icon"; ?>
                                                                                        @endif


                                                                                @elseif(in_array($log->log_type, array('Glucose')))
                                                                                    <?php
                                                                                    $log_data = $log->glucose_level;
                                                                                    $type = " mg/dl";
                                                                                    $class = "iconhs-glucose";
                                                                                    $div_class = "log-btn activity";
                                                                                    ?>
                                                                                    @elseif(in_array($log->log_type, array('Medication')))
                                                                                    <?php
                                                                                    $log_data = $log->quantityTaken;
                                                                                    $type = " meds";
                                                                                    $class = "iconhs-medication";
                                                                                    $div_class = "log-btn activity";
                                                                                    ?>
                                                                                @else
                                                                                <?php
                                                                                    $log_data= $log->steps;
                                                                                $type=" Steps" ;
                                                                                $class="iconhs-shoe";
                                                                                $div_class="log-btn activity";
                                                                                ?>

                                                                            @endif

                                                                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">




                                                                        <div class="btn btn-block btn-default {{$div_class}}">



                                                                            @if($log->log_type !='')
                                                                                <span class="badge badge-danger"> <i aria-hidden="true" class="{{$class}}"></i> </span>
                                                                            <h5>{{$log->log_type}}</h5>
                                                                                @else
                                                                                <span class="badge badge-danger"> <i aria-hidden="true" class="{{$class}}"></i> </span>
                                                                                <h5>{{'Activity'}}</h5>
                                                                            @endif
                                                                            @if($log_data !='')
                                                                            <h6> <strong>{{$log_data}}</strong></h6>
                                                                            @else
                                                                                <h6> <strong>{{'0'}}</strong></h6>
                                                                            @endif
                                                                            <strong><h5>{{$type}}</h5></strong>
                                                                        </div>

                                                                        <strong><p class="text-center"><small>{{ $log->date_time }}</small></p></strong>




                                                                    </div>


                                                                    @endforeach
                                                                    @endforeach
                                                                    @else
                                                                        <div  style="padding:15px; background-color:#94A0B2; margin-top:15px; text-align:center;">

                                                                           <span>No Logs Found.</span>
                                                                        </div>

                                                                    @endif


                                                                </div>



                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>








                                                </div>
                                            </div>
                                        </div>
                                        <!-- END PROFILE CONTENT -->
                                    </div>
                                </div>
                            </div>
                            <!-- END PAGE CONTENT INNER -->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT BODY -->
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->
            @include('common.motivation')
            @include('common.user_profile_image')
@stop


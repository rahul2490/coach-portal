<!--BEGIN MODAL Add notes-->
<div class="modal fade in" id="modal_memberscheduling" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Member Schedule</h4>
            </div>
            <div class="modal-body">

                <form role="form" class="">
                    <div class="form-body">

                        <div class="form-group">
                            <label for="multiple" class="control-label"> Select Days
                            </label>
                            <div class="input-group select2-bootstrap-append">
                                <select id="ddscheduledays" class="form-control ddscheduledays" name="ddscheduledays" multiple >
                                    <option value="">Weekdays</option>
                                    <option value="">Saturdays</option>
                                    <option value="">Sundays</option>
                                </select>
                                <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button" data-select2-open="multiple">
                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                </button>
                                                        </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="multiple" class="control-label"> Select Time
                            </label>
                            <div class="input-group select2-bootstrap-append">
                                <select id="ddscheduletime" class="form-control ddscheduletime" name="ddscheduletime" multiple >
                                    <option value="">Before 9 AM </option>
                                    <option value="">9 AM - noon</option>
                                    <option value="">Lunch</option>
                                    <option value="">Afternoon</option>
                                    <option value="">Evenings</option>
                                </select>
                                <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button" data-select2-open="multiple">
                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                </button>
                                                        </span>
                            </div>
                        </div>

                        <div class="form-group col-md-12 text-right">
                            <button data-dismiss="modal" aria-hidden="true" type="button" class="btn btn-default"> &nbsp; Cancel &nbsp; </button>
                            <button type="button" class="btn green-jungle"> &nbsp; Add &nbsp; </button>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </form>

                <div class="clearfix"></div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption caption-md">
            <span class="caption-subject font-green-jungle  bold">Physical Activity</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided" data-toggle="buttons">

                <label class="active btn btn-transparent btn-no-border blue-oleo btn-outline btn-circle btn-sm daily-switch">
                    <input type="radio" name="options" class="toggle " id="option2">Daily</label>
                <label class="btn btn-transparent btn-no-border blue-oleo btn-outline btn-circle btn-sm weekly-switch">
                    <input type="radio" name="options" class="toggle " id="option2">Weekly</label>
            </div>
        </div>
    </div>
    <div class="portlet-body daily">

        <div  class="row text-right margin-b-m-35">
            <a class="btn physical_activity_steps_daily left"><i class="fa fa-angle-left font-green"></i></a>
            <a class="btn physical_activity_steps">{{ $date_array['daily_start'] }} - {{ $date_array['daily_end'] }}</a>
            <a class="btn physical_activity_steps_daily right"><i class="fa fa-angle-right font-green"></i></a>
        </div>
        <div class="list-separated ">
            <span class="font-md font-green-jungle title"> Daily Averages </span>
        </div>
        <div class="row list-separated">
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="font-grey-mint font-sm"> Time </div>
                <div class=" font-lg font-red-flamingo"><span class="physical_activity_steps_time">{{ $physical_activity_minutes_daily['total_minute_daily'] }}</span>
                    <span class="font-xs font-grey-mint">min</span>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="font-grey-mint font-sm"> Steps </div>
                <div class=" font-lg font-purple"> <span class="physical_activity_steps_step">{{ $physical_activity_steps_daily['total_steps'] }}</span>
                    <span class="font-xs font-grey-mint"></span>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="font-grey-mint font-sm"> Activity Log </div>
                <div class=" font-lg font-blue-sharp"> <span class="physical_activity_steps_activity_logs">{{ $physical_activity_steps_daily['activity_log'] + $physical_activity_minutes_daily['activity_log_daily'] }}</span>
                    <span class="font-xs font-grey-mint">logged</span>
                </div>
            </div>
        </div>


        <div id="highchart_3" class="margin-top-20" style="height:175px;"></div>

        <div id="highchart_6" class="margin-top-20" style="height:175px;"></div>
    </div>

    <div id="portlet-weekly-activity" class="portlet-body weekly">

        <div  class="row text-right margin-b-m-35">
            <a class="btn physical_activity_weekly_minute_icon left"><i class="fa fa-angle-left font-green"></i></a>
            <a class="btn physical_activity_weekly_minute">{{ $date_array['monthly_start'] }} - {{ $date_array['monthly_end'] }}</a>
            <a class="btn physical_activity_weekly_minute_icon right"><i class="fa fa-angle-right font-green"></i></a>
        </div>
        <div class="list-separated ">
            <span class="font-md font-green-jungle title"> Weekly Averages </span>
        </div>
        <div class="row list-separated">
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="font-grey-mint font-sm"> Time </div>
                <div class=" font-lg font-red-flamingo"> <span class="physical_activity_steps_weekly_time">{{ $physical_activity_minutes_weekly['total_minute_weekly'] }}</span>
                    <span class="font-xs font-grey-mint">min</span>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="font-grey-mint font-sm"> Steps </div>
                <div class=" font-lg font-purple"> <span class="physical_activity_steps_weekly_step">{{ $physical_activity_steps_weekly['total_steps'] }}</span>
                    <span class="font-xs font-grey-mint"></span>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="font-grey-mint font-sm"> Activity Log </div>
                <div class=" font-lg font-blue-sharp"> <span class="physical_activity_steps_weekly_activity_logs">{{ $physical_activity_steps_weekly['activity_log'] + $physical_activity_minutes_weekly['activity_log_daily'] }}</span>
                    <span class="font-xs font-grey-mint">logged</span>
                </div>
            </div>
        </div>


        <div id="highchart_3-w" class="margin-top-20" style="height:175px;"></div>
        <div id="highchart_6-w" class="margin-top-20" style="height:175px;"></div>
    </div>
</div>


@if(isset($physical_activity_steps_daily))
    <script type="application/javascript">
        var physical_activity_steps_daily = [];
        try{
            physical_activity_steps_daily = JSON.parse('{!! json_encode($physical_activity_steps_daily['physical_activity_steps_daily']) !!}');
        }catch(e){}

        var physical_activity_steps_weekly = [];
        try{
            physical_activity_steps_weekly = JSON.parse('{!! json_encode($physical_activity_steps_weekly['physical_activity_steps_weekly']) !!}');
        }catch(e){}
    </script>
@endif


@if(isset($physical_activity_minutes_daily))
    <script type="application/javascript">
        var physical_activity_minute_daily = [];
        try{
            physical_activity_minute_daily = JSON.parse('{!! json_encode($physical_activity_minutes_daily['physical_activity_minute_daily']) !!}');
        }catch(e){}

        var physical_activity_minute_weekly = [];
        try{
            physical_activity_minute_weekly = JSON.parse('{!! json_encode($physical_activity_minutes_weekly['physical_activity_minute_weekly']) !!}');
        }catch(e){}
    </script>
@endif
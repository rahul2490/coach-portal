<!--BEGIN MODAL Add notes-->
<div class="modal fade in" id="modal_viewkeyfacts" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Keyfacts</h4>
            </div>
            <div class="modal-body">
                <ul class="list-unstyled list-modal">
                    <li class="">
                        <p class="">
                            <strong>Daniel Wong </strong> <small class="font-grey-salsa pull-right lowercase">a few seconds ago</small>
                        </p>
                        <p><span class="font-grey-salsa">Post Call</span> - Lorem ipsum dolor sit amet diam nonummy nibh dolore </p>
                    </li>

                    <li class="">
                        <p class="">
                            <strong>Daniel Wong </strong> <small class="font-grey-salsa pull-right lowercase">a few seconds ago</small>
                        </p>
                        <p><span class="font-grey-salsa">Post Call</span> - Lorem ipsum dolor sit amet diam nonummy nibh dolore </p>
                    </li>

                </ul>

                <div class="clearfix"></div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
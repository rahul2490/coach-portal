<!--BEGIN MODAL Add notes-->
<div class="modal fade in" id="modal_viewallsession" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Sessions</h4>
            </div>
            <div class="modal-body">
                <ul class="list-unstyled list-modal">
                    <li class="">
                        <p class="">
                            <strong><i class="fa fa-clock-o"> </i>09/28/2017 12:30 AM </strong>
                        </p>
                        <p>- Mahajan, SheetalF <small class="font-grey-salsa">Food Coach</small></p>
                    </li>

                    <li class="">
                        <p class="">
                            <strong><i class="fa fa-clock-o"> </i>09/28/2017 12:30 AM </strong>
                        </p>
                        <p>- Mahajan, SheetalF <small class="font-grey-salsa">Food Coach</small></p>
                    </li>

                </ul>

                <div class="clearfix"></div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
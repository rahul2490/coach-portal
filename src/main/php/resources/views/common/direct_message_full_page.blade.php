@extends('include.admin-layout')

@section('page-title')
    Members Data
@stop

@section('body-class')
direct_message_full_page
@stop

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->

        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route('facilityadmin.home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Members Data</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-blue-ebonyclay">
                                            <i class="icon-settings font-blue-ebonyclay"></i>
                                            <span class="caption-subject bold uppercase">Direct Messages</span>
                                        </div>
                                    </div>

                                    <div class="portlet-body" id="direct-msg-coach">
                                        <div class="modal-body">
                                            <div class="row">
                                                {!! Form::open( ['method' => 'POST', 'id' => 'direct_msg_by_coach', 'onsubmit' => 'return false;','role' => 'form', 'enctype'=>'multipart/form-data','url' => route('directMessage'), 'save-direct-message' => route('saveDirectMessage'), 'reload-direct-message' => route('reloadDirectMessage'),'files' => true  ] ) !!}
                                                {!! Form::hidden('id', $user_id ) !!}
                                                {!! Form::hidden('last_message_id', '' ) !!}
                                                <div  class="col-sm-12" id="chats">
                                                    <div class="modal-form-title">
                                                        <div class="col-sm-6 caption font-green">
                                                            <i class="fa fa-user font-green"></i>
                                                            <span class="caption-subject bold uppercase member_name">Brain S-1918</span><a class="label label-sm label-primary markread" data-id="" style="display:none;" > Mark as read <i class="fa fa-check"></i></a>
                                                        </div>
                                                        <div class="col-sm-6 form-horizontal">
                                                            <div class="">
                                                                <label class="col-md-4 control-label">Filter By</label>
                                                                <div class="input-group select2-bootstrap-append">
                                                                    <select id="filter-messages" class="form-control " name="filter-messages" >
                                                                        <option value="all">ALL</option>
                                                                    </select>
                                                                    <span class="input-group-btn">
                                                                            <button class="btn btn-default" type="button" data-select2-open="filter-messages">
                                                                                <span class="glyphicon glyphicon-search"></span>
                                                                            </button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                    @if(session('userRole') != 'admin' && session('userRole') != 'facilityadmin')
                                                        <div class="col-sm-4">

                                                            <div class="form-group">
                                                                <label for="recom-messages" class="control-label"> Canned Messages
                                                                </label>
                                                                <div class="input-group select2-bootstrap-append">
                                                                    <select id="recom-messages" class="form-control recom-messages" name="recom_messages" >
                                                                        <option value="">Select Canned Messages</option>
                                                                    </select>
                                                                    <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" data-select2-open="recom-messages">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </span>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label for="saved-messages" class="control-label"> My Saved Message
                                                                </label>
                                                                <div class="input-group select2-bootstrap-append">
                                                                    <select id="saved-messages" class="form-control saved-messages" name="saved_messages" >
                                                                        <option value="" >Select Saved Message</option>
                                                                    </select>
                                                                    <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" data-select2-open="saved-messages">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                        </span>
                                                                </div>
                                                            </div>

                                                            {{--<div class="form-group">
                                                                <label for="canned-messages" class="control-label"> Canned Message
                                                                </label>
                                                                <div class="input-group select2-bootstrap-append">
                                                                    <select id="canned-messages" class="form-control canned-messages" name="canned_messages" >
                                                                        <option value="">Select Canned Message</option>
                                                                        <option value="1">Let us go back to what was successful for you..</option>
                                                                        <option value="2">Just checking in to see how you’re doing..</option>
                                                                        <option value="3">You are in danger of being dis-enrolled..</option>
                                                                    </select>
                                                                    <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" data-select2-open="canned-messages">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                        </span>
                                                                </div>
                                                            </div>--}}

                                                            {{--<div class="form-group">--}}
                                                                {{--<p class="text-center">OR</p>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="form-group text-center">--}}
                                                                {{--<a class="btn btn-sm green-jungle btn-new-msg btn-block"> &nbsp; Create New Message &nbsp; </a>--}}
                                                            {{--</div>--}}
                                                        </div>
                                                        <div class="col-sm-8 border-left">
                                                            @else
                                                                <div class="col-sm-12 border-left">
                                                                    @endif

                                                        <div class="scroller" id="direct-message-scroll" style="height: 280px;">
                                                            <ul class="chats chat_message"></ul>
                                                        </div>
                                                        @if(session('userRole') != 'admin' && session('userRole') != 'facilityadmin')
                                                        <ul class="chats margin-top-10">
                                                            <li class="out">
                                                                <img class="avatar" alt="" src="{{ env('APP_BASE_URL') }}/img/default-user.png" />
                                                                <div class="message no-bg">
                                                                        <div class="form-group">
                                                                            <textarea class="recomend-text form-control todo-taskbody-taskdesc form-control" id="send_message" name="send_message" rows="2"></textarea>
                                                                        </div>

                                                                        <button type="button" id="attachment" class=" btn btn-sm btn-outline dark margin-t-b-10"> <i class="fa fa-paperclip" aria-hidden="true"></i>  Attachment &nbsp; </button>
                                                                        <button type="submit"  name="submit" value="send" class="send btn btn-sm btn-outline dark margin-t-b-10"> &nbsp; Send &nbsp; </button>
                                                                        <button type="submit" name="submit" value="save" class="send btn btn-sm btn-outline dark margin-t-b-10"> &nbsp; Save and Send &nbsp; </button>
                                                                        <span class="form-group" >
                                                                            <input type="file" name="uploadfile" id="uploadfile" class="form-control" style="display: none;" >
                                                                        </span>
                                                                </div>
                                                                <div id="file_upload_show"></div>
                                                            </li>
                                                        </ul>
                                                        @endif
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>

                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
        {{--<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>--}}

@stop


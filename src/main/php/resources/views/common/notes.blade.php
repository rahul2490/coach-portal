<!--BEGIN MODAL Add notes-->
<div class="modal fade in" id="modal_viewnotes"  role="basic" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"> Notes </h4>
            </div>
            {!! Form::open( ['method' => 'GET', 'id' => 'get_coach_notes', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('get-coach-notes')  ] ) !!}
                <div class="modal-body">
                    <input type="hidden" id="patient_id" name="patient_id" value="{{ $patient_id }}">
                    <div class="scroller" style="height: 70vh;">
                        <div class="general-item-list get_coach_notes"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            {!! Form::close() !!}
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
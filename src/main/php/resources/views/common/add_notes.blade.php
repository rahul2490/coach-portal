<!--BEGIN MODAL Add notes-->
<div class="modal fade in" id="modal_addnotes" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Notes</h4>
            </div>
            <div class="modal-body">

                {!! Form::open( ['method' => 'POST', 'id' => 'add_new_coach_notes', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('add-coach-notes')  ] ) !!}
                    <div class="form-body">
                        <div class="form-group">
                            <label for="multiple" class="control-label"> Note Types
                            </label>
                            <select class="form-control" name="notify_type" id="notify_type">
                                <option value="Pre Call" selected="">Pre Call</option>
                                <option value="Post Call">Post Call</option>
                                <option value="Other">Other</option>
                            </select>
                        </div>
                        <input type="hidden" id="patient_id" name="patient_id" value="{{ $patient_id }}">
                        <input type="hidden" name="foodcoach_user_id" value="{{ $patient_info->primary_food_coach_id }}">
                        <div class="form-group">
                            <label for="multiple" class="control-label add_all_call_notes"> Add Pre Call Notes
                            </label>
                            <textarea class="form-control" name="add_coach_notes"></textarea>
                        </div>


                        <div class="form-group">
                            <label for="multiple" class="control-label"> Notify Coaches
                            </label>
                            <div class="input-group select2-bootstrap-append">
                                @if(count($leadcoach_foodcoach))
                                    <select id="ddNotifyCoaches" class="form-control ddNotifyCoaches" name="ddNotifyCoaches[]" multiple>
                                        @foreach($leadcoach_foodcoach as $key => $coach)
                                            @if(!empty($patient_info->primary_food_coach_id) && $patient_info->primary_food_coach_id == $coach['user_id'])
                                                @continue;
                                            @endif
                                            @if(!empty($patient_info->lead_user_id) && $patient_info->lead_user_id == $coach['user_id'])
                                                @continue;
                                            @endif
                                            <option value="{{ $coach['provider_id'] }}" title="{{ $coach['email'] }}">{{  $coach['full_name'] }}</option>
                                        @endforeach
                                    </select>
                                @endif

                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-open="ddNotifyCoaches">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>

                            </div>
                            @if(!empty($patient_info->lead_user_id))
                                @foreach($leadcoach_foodcoach as $key => $coach)
                                    @if($patient_info->lead_user_id == $coach['user_id'])
                                        <div class="help-block">Note: Primary Lifestyle Coach <a class="sbold">{{  $coach['full_name'] }}</a> will be automatically notified.</div>
                                    @endif
                                @endforeach
                            @endif
                        </div>


                        <div class="form-group col-md-12 text-right">
                            <button data-dismiss="modal" aria-hidden="true" type="button" class="btn btn-default"> &nbsp; Cancel &nbsp; </button>
                            <button type="submit" class="btn green-jungle add"> &nbsp; Add &nbsp; </button>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                {!! Form::close() !!}

                <div class="clearfix"></div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- BEGIN PROFILE SIDEBAR -->
<div class="profile-sidebar">
    <button type="button" class="btn btn-outline btn-icon-only green btn-sm pull-right" href="#modal_add_profile_image" data-toggle="modal"><i class="fa fa-pencil"></i></button>
    <!-- PORTLET MAIN -->
    <div class="portlet light profile-sidebar-portlet ">

        <!-- SIDEBAR USERPIC -->
        <div class="profile-userpic">
            <img src="{{ env('PROFILE_IMAGE_BASE_URL'). $patient_info->image_path}}" class="img-responsive" alt=""> </div>
        <!-- END SIDEBAR USERPIC -->
        <!-- SIDEBAR USER TITLE -->
        <div class="profile-usertitle">
            <div class="profile-usertitle-name"> <span class="dashboard_user_full_name">{{ str_limit($patient_info->full_name) }}</span>  <br/>
                <small class="font-grey-salsa"> @if(isset($patient_info->group_display_name)) {{ $patient_info->group_display_name }} @else {{ $patient_info->first_name }} @endif
                    | {{ $patient_info->patient_id }} </small>
                @if($patient_info->mrn !='')
                <br><small class="font-grey-salsa"> MRN: {{ $patient_info->mrn }} </small>
                    @endif
            </div>
            <!-- <div class="profile-usertitle-name"></div> -->
            @if(isset($patient_info->diabetes_type_name))
                <div class="profile-usertitle-name"> <span class="badge badge-danger"> {{ $patient_info->diabetes_type_name }}  </span></div>
            @endif

            @if(isset($patient_info->group_name))
                <div class="profile-usertitle-job"> {{ $patient_info->group_name }} </div>
            @endif
        </div>
        <!-- END SIDEBAR USER TITLE -->
        <!-- SIDEBAR BUTTONS -->
        <div class="text-center margin-btm-10">
            <span class=" btn-icon-only grey-cascade"><i class="fa fa-phone"></i>   {{ $patient_info->phone }}</span><br>
            <span class=" btn-icon-only grey-cascade " ><i class="fa fa-envelope"></i>   {{ $patient_info->email }}</span>
        </div>
        <div class="clearfix"></div>
        <!-- END SIDEBAR BUTTONS -->

    </div>
    <!-- END PORTLET MAIN -->
    <!-- PORTLET group -->
    <div class="portlet light-green-bg">

        <div class="portlet-body">
            <!-- start group info-->
            <div class="col-sm-12  ">
                <div class="col-sm-6 text-center">
                    <a class="btn btn-link font-lg font-green-jungle" style="cursor: text;" title="Current Week"  data-toggle="tooltip"><small> Week </small></br> <span class="sbold font-hg">@if(empty($patient_info->member_completed_week)) N/A @elseif($patient_info->member_completed_week <= 0) 1 @else {{ $patient_info->member_completed_week }} @endif</span></a>
                </div>
                <div class="col-sm-6 text-center">
                    @if(session('userRole') == 'leadcoach')
                        <a class="btn btn-link font-lg font-green-jungle" href="{{$group_link}}" title="Session Detail"  data-toggle="tooltip" ><small> Session </small></br> <span class="sbold font-hg">@if($session!=""){{ $session }} @else {{ "N/A" }} @endif </span></a>
                    @else
                        <a class="btn btn-link font-lg font-green-jungle" style="cursor: text;" title="Session Detail"  data-toggle="tooltip" ><small> Session </small></br> <span class="sbold font-hg">@if($session!=""){{ $session }} @else {{ "N/A" }} @endif </span></a>
                    @endif
                </div>
            </div>
            <div class="clearfix"></div>
            <!--end group info-->

        </div>
    </div>



    <!-- PORTLET MAIN -->
    <div class="portlet light ">
        <!--start my motivation-->
        @if($patient_info->is_public == 1)
            <div class="profile-desc-title font-dark margin-btm-10 "> Motivation </div>
            <div class="">
                <!-- <h4 class="profile-desc-title"></h4> -->
                <span class="profile-desc-text"> {!!  $patient_info->description  !!}  </span>
                <div class="margin-top-20 profile my-motivation">
                    <ul class="list-unstyled profile-nav">
                        <li>
                            <img src="@if(empty($patient_info->image_name)) {{ env('PROFILE_IMAGE_BASE_URL') . 'resources/css/images/default_profile_image.gif' }} @else {{ env('PROFILE_IMAGE_BASE_URL').'motivationImages/'.$patient_info->image_name }} @endif" class="img-responsive pic-bordered" alt="">
                        </li>
                    </ul>
                </div>
            </div>
            <!--end motivation-->
        @endif


        <div class="portlet-body">

            <div class="profile-desc-title font-dark margin-btm-10 "> Profile </div>
            <div class="profile-desc-text">
                <p>Readiness to Change:<strong> Contemplation </strong> </p>
                <p>Preferred Coaching Style:<strong> {{ $patient_info->coaching_style }}	</strong> </p>
                <p>Age:<strong> {{ $patient_info->age }} </strong> </p>
                <p>Race/Ethinicity:<strong> {{ $patient_info->ethnicity_race }} </strong> </p>
                <p>Barriers to success: <strong>Work travel</strong> </p>
                <p>Past Weight Loss efforts: <strong>NutriSystem</strong></p>
                <p>Outcome of Past effort: <strong>Lost weight and kept all of it off</strong></p>
                <p>Dietary restrictions: <strong>N/A</strong></p>
                <p >Key Facts:<strong> N/A </strong> </p>

                <h5 class="sbold">Personality</h5>
                <div class="subtitle">

                    <div class="profile-desc-text">
                        <p>Extraversion:  <strong>5.5 (4.4)</strong></p>
                        <p>Agreeableness: <strong>4.4 (5.2) </strong></p>
                        <p>Conscientiousness: <strong>6.2 (5.4)</strong></p>
                        <p>Neuroticism: <strong>3.3 (4.8)</strong></p>
                        <p>Openness to Experience: <strong>5.8 (5.4)</strong></p>
                    </div>
                </div>

                <div class="clearfix"></div>
                <!--  <p><a class="btn btn-sm btn-link font-dark pull-right"  data-toggle="collapse" data-target="#keyfactsinfo"> More </a></p> -->
                <div class="clearfix"></div>
            </div>

            <div class="profile-desc-title font-dark margin-btm-10 margin-top-20">  Device Info </div>
            <div class="profile-desc-text">

                <p>App: <strong> {{ $patient_info->app_version }}</strong></p>
                <p>Device Version: <strong>{{ $patient_info->device_type }} </strong> </p>
                <p>Last Login Date: <strong>{{ $patient_info->last_login_date }}</strong> </p>
                <p>TimeZone: <strong> {{ $patient_info->timezone }} </strong></p>
                <p>Last Fitbit Sync: <strong> {{ $patient_info->last_fitbit_sync }} </strong> </p>
                <p>Body Scale: <strong>N/A</strong> </p>

            </div>

        </div>

    </div>
    <!-- END PORTLET MAIN -->






    <!--end links-->
</div>
<!-- END  PROFILE SIDEBAR -->
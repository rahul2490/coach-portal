<!--meals logged -->
<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption caption-md">
            <span class="caption-subject font-green-jungle  bold">Meals Logged</span>
            <!-- <span class="caption-helper hide">weekly stats...</span> -->
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided" data-toggle="buttons">

                <label class="active btn btn-transparent btn-no-border blue-oleo btn-outline btn-circle btn-sm daily-switch">
                    <input type="radio" name="options" class="toggle " id="option2">Daily</label>
                <label class="btn btn-transparent btn-no-border blue-oleo btn-outline btn-circle btn-sm weekly-switch">
                    <input type="radio" name="options" class="toggle " id="option2">Weekly</label>
            </div>
        </div>
    </div>
    <div class="portlet-body daily">
        <div  class="row text-right">

            <a class="btn patient_meal_logged_daily left"><i class="fa fa-angle-left font-green"></i></a>
            <a class="btn meal_logged_daily">{{ $date_array['daily_start'] }} - {{ $date_array['daily_end'] }}</a>
            <a class="btn patient_meal_logged_daily right"><i class="fa fa-angle-right font-green"></i></a>

        </div>

        <div id="highchart_4" class="margin-top-20" style="height:175px;"></div>
    </div>

    <div id="portlet-weekly-logedmeal" class="portlet-body weekly">
        <div  class="row text-right">

            <a class="btn weekly_patient_meal_logged left"><i class="fa fa-angle-left font-green"></i></a>
            <a class="btn weekly_meal_logged">{{ $date_array['monthly_start'] }} - {{ $date_array['monthly_end'] }}</a>
            <a class="btn weekly_patient_meal_logged right"><i class="fa fa-angle-right font-green"></i></a>

        </div>

        <div id="highchart_4-w" class="margin-top-20" style="height:175px;"></div>
    </div>
</div>


@if(isset($patient_meal_logged_daily))
    <script type="application/javascript">
        var meal_logged_daily = [];
        try{
            meal_logged_daily = JSON.parse('{!! json_encode($patient_meal_logged_daily['daily_meal_logged_data']) !!}');
        }catch(e){}

        var weekly_meal_logged = [];
        try{
            weekly_meal_logged = JSON.parse('{!! json_encode($patient_meal_logged_weekly['daily_meal_logged_data']) !!}');
        }catch(e){}
    </script>
@endif
@extends('include.admin-layout')

@section('page-title')
    My Dashboard
@stop

@section('body-class')

@stop

@section('content')

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <!-- BEGIN PAGE CONTENT BODY -->
                    <div class="page-content">
                        <div class="container-fluid">
                            <!-- BEGIN PAGE BREADCRUMBS -->
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{route(session('userRole').'.home')}}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Dashboard</span>
                                </li>
                            </ul>
                            <!-- END PAGE BREADCRUMBS -->
                            <!-- BEGIN PAGE CONTENT INNER -->
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-12">

                                        @include('common.dashboard_profile_sidebar')

                                        <!-- BEGIN PROFILE CONTENT -->
                                        <div class="profile-content">
                                            <div class="row">
                                                @include('common.dashboard_header')

                                                <div class="col-md-6">

                                                    <div class="portlet light  mt-element-ribbon padding-0 hide">
                                                        <div class="ribbon ribbon-color-danger uppercase sbold">Milestone Risk</div>
                                                        <!--  <div class="portlet-title tabbable-line">
                                                             <div class="caption ">
                                                                 <i class="fa fa-gear font-green "></i>
                                                                 <span class="caption-subject font-green bold uppercase">Member Profile and Settings</span>
                                                             </div>
                                                         </div> -->
                                                        <div class="portlet-body">
                                                            <div class="ribbon-content">Needs to weigh in twice or log 3 meals this week or next to earn Milestone2</div>
                                                        </div>
                                                    </div>


                                                    <!--section 2 group charts -->
                                                    <div class="portlet light ">
                                                        <div class="portlet-title ">
                                                            <div class="caption ">
                                                                <span class="caption-subject font-green-jungle bold ">Goals</span>
                                                            </div>
                                                            <?php
                                                            if(!empty($patient_goals_info)) {
                                                            ?>
                                                            <div class="actions">
                                                                <a data-dismiss="modal" aria-hidden="true"  class="btn  btn-sm font-grey-gallery sbold " href="#modal_goals" data-toggle="modal">
                                                                    See All Goals </a>
                                                            </div>
                                                            <?php } ?>
                                                        </div>

                                                        <?php $i=1;


                                                            if(!empty($patient_goals_info)) {
                                                        ?>
                                                        @foreach($patient_goals_info as $goals)
                                                            @if($i==1)
                                                        <div class="portlet-body">
                                                            <div class="">
                                                                <table class="table table-light" width="100%" >
                                                                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                                                                    <tr class="">
                                                                        <td class="goals-icon" >
                                                                            <div class=""><h3 class="">  @if($goals->goal_type == "Physical Activity") <i class="iconhs-shoe font-yellow-crusta "></i>   @else  <i class="iconhs-spoon font-green-jungle"></i>  @endif </h3></div>
                                                                        </td>
                                                                        <td class="">
                                                                            <div class="goal-details">
                                                                                <div class="font-md font-dark sbold"> {{$goals->goal_name}} </div>
                                                                                <div class="font-sm font-grey-mint"> {{$goals->reminder_settings}} </div>
                                                                                @if($goals->reminder_time)
                                                                                    <div class=" font-sm font-grey-salsa"><i class="fa fa-clock-o"></i> {{ $goals->reminder_time }} </div>
                                                                                @endif
                                                                                @if($goals->reminder_settings != "")
                                                                                    <div class=" badge badge-green-jungle font-sm font-white uppercase"> Active </div>
                                                                               @else
                                                                                    <div class=" badge font-sm font-white uppercase"> Deactivated </div>
                                                                                @endif
                                                                            </div>
                                                                        </td>
                                                                        <td class="">
                                                                            <div id="chart_div_new_195" class="chart_activity1">
                                                                                <div class="challenge-graph-single margin-top-10">
                                                                                    <span class="badges">
                                                                                        <span class="badge badge-total">{{ count($patient_goals_statics_info_array) }} checkins</span>
                                                                                    </span>
                                                                                    <span class="frequency-graph frequency-graph-weekly">
                                                                                        <div class="grid">
                                                                                            <ul>
                                                                                                <li style="bottom:7px; @if(in_array($date_array['last_week_3']->copy()->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li class="labels_li">{{ $date_array['last_week_3']->format('m/d') }}</li>
                                                                                                <li style="bottom:14px; @if(in_array($date_array['last_week_3']->copy()->addDay(1)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:21px; @if(in_array($date_array['last_week_3']->copy()->addDay(2)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:28px; @if(in_array($date_array['last_week_3']->copy()->addDay(3)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif "></li>
                                                                                                <li style="bottom:35px; @if(in_array($date_array['last_week_3']->copy()->addDay(4)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:42px; @if(in_array($date_array['last_week_3']->copy()->addDay(5)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:49px; @if(in_array($date_array['last_week_3']->copy()->addDay(6)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                            </ul>
                                                                                            <ul>
                                                                                                <li style="bottom:7px; @if(in_array($date_array['last_week_2']->copy()->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li class="labels_li">{{ $date_array['last_week_2']->format('m/d') }}</li>
                                                                                                <li style="bottom:14px; @if(in_array($date_array['last_week_2']->copy()->addDay(1)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:21px; @if(in_array($date_array['last_week_2']->copy()->addDay(2)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:28px; @if(in_array($date_array['last_week_2']->copy()->addDay(3)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:35px; @if(in_array($date_array['last_week_2']->copy()->addDay(4)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:42px; @if(in_array($date_array['last_week_2']->copy()->addDay(5)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:49px; @if(in_array($date_array['last_week_2']->copy()->addDay(6)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                            </ul>
                                                                                            <ul>
                                                                                                <li style="bottom:7px; @if(in_array($date_array['last_week_1']->copy()->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li class="labels_li">{{ $date_array['last_week_1']->format('m/d') }}</li>
                                                                                                <li style="bottom:14px; @if(in_array($date_array['last_week_1']->copy()->addDay(1)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:21px; @if(in_array($date_array['last_week_1']->copy()->addDay(2)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:28px; @if(in_array($date_array['last_week_1']->copy()->addDay(3)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:35px; @if(in_array($date_array['last_week_1']->copy()->addDay(4)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:42px; @if(in_array($date_array['last_week_1']->copy()->addDay(5)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                                <li style="bottom:49px; @if(in_array($date_array['last_week_1']->copy()->addDay(6)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"></li>
                                                                                            </ul>
                                                                                            <ul>
                                                                                                <li style="bottom:7px; @if(in_array($date_array['last_week_0']->copy()->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif"> </li>
                                                                                                <li class="labels_li">{{ $date_array['last_week_0']->format('m/d') }}</li>
                                                                                                <li style="bottom:14px; @if(in_array($date_array['last_week_0']->copy()->addDay(1)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif" title="{{ $date_array['last_week_0']->copy()->addDay(1)->format('Y-m-d') }}" ></li>
                                                                                                <li style="bottom:21px; @if(in_array($date_array['last_week_0']->copy()->addDay(2)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif" title="{{ $date_array['last_week_0']->copy()->addDay(2)->format('Y-m-d') }}"></li>
                                                                                                <li style="bottom:28px; @if(in_array($date_array['last_week_0']->copy()->addDay(3)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif " title="{{ $date_array['last_week_0']->copy()->addDay(3)->format('Y-m-d') }}"></li>
                                                                                                <li style="bottom:35px; @if(in_array($date_array['last_week_0']->copy()->addDay(4)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif" title="{{ $date_array['last_week_0']->copy()->addDay(4)->format('Y-m-d') }}"></li>
                                                                                                <li style="bottom:42px; @if(in_array($date_array['last_week_0']->copy()->addDay(5)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif" title="{{ $date_array['last_week_0']->copy()->addDay(5)->format('Y-m-d') }}"></li>
                                                                                                <li style="bottom:49px; @if(in_array($date_array['last_week_0']->copy()->addDay(6)->format('Y-m-d'), $patient_goals_statics_info_array)){{ 'background-color:#69921f;' }} @endif" title="{{ $date_array['last_week_0']->copy()->addDay(6)->format('Y-m-d') }}"></li>
                                                                                            </ul>
                                                                                            <ul>
                                                                                            </ul>
                                                                                        </div>
                                                                                    </span>
                                                                                </div>
                                                                            </div>

                                                                        </td>

                                                                    </tr>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                            @endif
                                                            <?php $i++; ?>
                                                        @endforeach
                                                        <?php }else{ ?>
                                                                <div class="note note-info">
                                                                    <span class="block">No Goal Found</span>
                                                                </div>
                                                        <?php } ?>
                                                    </div>

                                                    <!--section 3 communication -->
                                                    <div class="portlet light ">
                                                        <div class="portlet-title ">
                                                            <div class="caption ">
                                                                <span class="caption-subject font-green-jungle bold ">Communications</span>
                                                            </div>
                                                            <div class="actions">

                                                            </div>

                                                        </div>

                                                        <div class="portlet-body ">
                                                            <div class=" ">
                                                                <div class="profile-desc-text">
                                                                    <p>Last Message Sent to Member: <strong> {{ $patient_communications_info->last_message_sent_to_patient }} </strong> </p>
                                                                    <p>last Message from member: <strong>  {{ $patient_communications_info->last_message_of_patient }} </strong> </p>
                                                                    <p>Wants Phone Calls: <strong> N/A </strong> </p>
                                                                    <p>Last Phone Call: <strong> N/A </strong> </p>
                                                                    <p>Scheduled Phone Call: <strong> N/A </strong> </p>
                                                                    <p>Time Set To message Coach: <strong> N/A </strong> </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <!--section 4 communication -->
                                                    <div class="portlet light">
                                                        <div class="portlet-title ">
                                                            <div class="caption ">
                                                                <span class="caption-subject font-green-jungle bold ">Conversations</span>
                                                            </div>
                                                        </div>

                                                        <div class="portlet-body">
                                                            <div class="convesation-wrap ">
                                                                <!-- BEGIN: Comments -->
                                                                <div class="general-item-list">
                                                                @if(!empty($patient_conversation_info))
                                                                    @foreach($patient_conversation_info as $conversation)

                                                                            <?php
                                                                            $chat_image = '';
                                                                            ?>

                                                                            @if($conversation['message_image_path'] != '')

                                                                                @if(strstr($conversation['message_image_path'], ".pdf"))

                                                                                <?php     $chat_image = '<span class="body"><img  class="img-attachment" src="'.env("APP_BASE_URL").'/img/pdf.png'.'"></span>'; ?>

                                                                                @else
                                                                                        <?php     $chat_image = '<span class="body"><img  class="img-attachment" src="'.env('PROFILE_IMAGE_BASE_URL') . $conversation['message_image_path'].'"></span>'; ?>
                                                                                @endif
                                                                            @endif
                                                                            <div class="item">
                                                                                <div class="item-head">
                                                                                    <div class="item-details">
                                                                                        <img class="item-pic rounded" src="{{ $conversation['image_path'] }}">
                                                                                        <a href="" class="item-name primary-link">{{ $conversation['full_name'] }}</a>
                                                                                        <span class="item-label"></span>
                                                                                    </div>
                                                                                    <span class="item-status">{{ $conversation['timestamp'] }}</span>
                                                                                </div>
                                                                                <div class="item-body">{!! $conversation['message'] !!} {!! $chat_image !!}</div>
                                                                            </div>
                                                                    @endforeach
                                                                    @else
                                                                        <div class="note note-info">
                                                                            <span class="block">No Message History</span>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="text-right">
                                                                    <a data-id="{{ $patient_info->user_id }}" data-toggle="modal" href="#direct-msg-coach" class="btn btn-sm btn-link sbold">View / Send messages</a>
                                                                </div>
                                                                <!-- END: Comments -->
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!--end conversation -->

                                                    <!--start coach notes -->

                                                    <div class="portlet light ">
                                                        <div class="portlet-title ">
                                                            <div class="caption ">
                                                                <span class="caption-subject font-green-jungle bold ">Coach Notes</span>
                                                            </div>

                                                            <div class="actions">
                                                                @if(count($patient_coach_notes) > 5)
                                                                    <a class="btn btn-sm font-grey-gallery sbold " href="#modal_viewnotes" data-toggle="modal" > More </a>
                                                                @endif
                                                                @if(session('userRole') != 'admin' && session('userRole') != 'facilityadmin' )
                                                                    <a data-dismiss="modal" aria-hidden="true" class="btn green-jungle btn-sm" href="#modal_addnotes" data-toggle="modal">Add Notes</a>
                                                                @endif
                                                            </div>

                                                        </div>

                                                        <div class="portlet-body ">
                                                            <div class="convesation-wrap ">
                                                                <!-- BEGIN: Comments -->
                                                                <div class="general-item-list">
                                                                    <?php $i=1;?>
                                                                    @if(!empty($patient_coach_notes))
                                                                    @foreach($patient_coach_notes as $notes)
                                                                        @if($i<=5)
                                                                            <div class="item">
                                                                                <div class="item-head">
                                                                                    <div class="item-details">
                                                                                        <img class="item-pic rounded" src="{{ env('APP_BASE_URL').'/img/default-user.png' }}">
                                                                                        <a href="javascript:void(0)" class="item-name primary-link">{{ $notes['provider_full_name'] }}</a>
                                                                                        <span class="item-label"></span>
                                                                                    </div>
                                                                                    <span class="item-status">{{ $notes['notes_date'] }}</span>
                                                                                </div>
                                                                                <div class="item-body member-name">{!!$notes['notes']!!} </div>
                                                                            </div>
                                                                        @endif
                                                                        <?php $i++; ?>
                                                                    @endforeach
                                                                    @else
                                                                        <div class="note note-info">
                                                                            <span class="block">No Coach Notes</span>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <!-- END: Comments -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end coach notes -->
                                                </div>


                                                <div class="col-sm-6 profile-rightbar">
                                                    <input type="hidden" id="uuid" name="uuid" value="{{ $uuid }}">
                                                    @include('common.weight_summary_chart')

                                                    @include('common.physical_ activity_chart')

                                                    @include('common.meals_logged_chart')

                                                    @include('common.meal_sources_chart')

                                                    @include('common.engagement_chart')

                                                </div>
                                            </div>
                                        </div>
                                        <!-- END PROFILE CONTENT -->
                                    </div>
                                </div>
                            </div>
                            <!-- END PAGE CONTENT INNER -->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT BODY -->
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->

            @include('common.direct_message')
            @include('common.notes')
            @include('common.add_notes')
            @include('common.keyfacts')
            @include('common.add_keyfacts')
            @include('common.add_session')
            @include('common.member_schedual')
            @include('common.motivation')
            @include('common.user_profile_image')
            @include('common.to_do_notes')
            @include('common.add_to_do_notes')
            @include('common.session')
            @include('common.goals')
@stop


<!--BEGIN MODAL motivation -->
<div class="modal fade in" id="modal_addmotivation"  role="basic" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Motivation</h4>
            </div>
            <div class="modal-body">

                <!--start my motivation-->
                {!! Form::open( ['method' => 'POST', 'id' => 'add_motivation_image_text', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('motivation-image-update') ] ) !!}
                    <input type="hidden" id="patient_id" name="patient_id" value="{{ $patient_id }}">
                    <input type="file" name="uploadpostfile" id="uploadpostfile" class="form-control" style="display:none;" >
                    <input type="hidden" id="file_name" name="file_name">
                    <div class="form-body">
                        <div class=" col-sm-6 col-sm-offset-3 margin-top-20 profile my-motivation">
                            <ul class="list-unstyled profile-nav">
                                <li>
                                    <img src="@if(empty($patient_info->image_name)) {{ env('PROFILE_IMAGE_BASE_URL') . 'resources/css/images/default_profile_image.gif' }} @else {{ env('PROFILE_IMAGE_BASE_URL').'motivationImages/'.$patient_info->image_name }} @endif" class="img-responsive pic-bordered motivation_image" alt="">
                                    <a href="javascript:;" class="profile-edit" id="post_attachment"> edit </a>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="multiple" class="control-label"> Motivation Text
                            </label>
                            <textarea class="form-control" name="post_description" >{{$patient_info->description}}</textarea>
                        </div>

                        <div class="form-group col-md-12 text-right">
                            <button data-dismiss="modal" aria-hidden="true" type="button" class="btn btn-default"> &nbsp; Cancel &nbsp; </button>
                            <button type="submit" class="btn green-jungle"> &nbsp; Add &nbsp; </button>
                        </div>

                    </div>
                {!! Form::close() !!}

                <!--end motivation-->

                <div class="clearfix"></div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
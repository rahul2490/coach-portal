<!-- bootstrap modals start here-->
<!--BEGIN MODAL-->
<div class="modal fade in" id="change_password" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Change Password</h4>
            </div>
            <div class="modal-body">
                <div class="row profile">
                    <div class="col-sm-12">
                        {!! Form::open( ['method' => 'POST', 'id' => 'changepassword', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('change-password') ] ) !!}
                        <div class="form-body">
                            <div class="row">
                                <div class="portlet light ">
                                    <div class="portlet-body form">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Old Password</label>
                                                <input class="form-control" name="old_password" placeholder="Old Password" value="" type="password" autocomplete="false">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">New Password</label>
                                                <input class="form-control" id="new_password" name="new_password" placeholder="New Password" value="" type="password" autocomplete="false">
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Confirm Password</label>
                                                <input class="form-control" name="confirm_password" placeholder="Confirm Password" value="" type="password" autocomplete="false">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="margiv-top-10">
                                            <button type="button" class="btn  btn-default" data-dismiss="modal"> &nbsp; Close &nbsp; </button>
                                            <button type="submit" class="btn green-jungle" id="changepasswordsubmit"> Change &nbsp; </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
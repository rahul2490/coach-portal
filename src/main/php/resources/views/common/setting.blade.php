
@extends('include.admin-layout')

@section('page-title')
   Settings
@stop

@section('body-class')

@stop

@section('content')
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <!-- BEGIN PAGE CONTENT BODY -->
                    <div class="page-content">
                        <div class="container-fluid">
                            <!-- BEGIN PAGE BREADCRUMBS -->
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{route(session('userRole').'.home')}}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="{{env("APP_BASE_URL").session('userRole').'/dashboard/'.$profile_detail->patient_id}}">Dashboard</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Settings</span>
                                </li>
                            </ul>
                            <!-- END PAGE BREADCRUMBS -->
                            <!-- BEGIN PAGE CONTENT INNER -->
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-12">

                                        <!-- BEGIN PROFILE SIDEBAR -->
                                    @include('common.dashboard_profile_sidebar')
                                        <!-- END  PROFILE SIDEBAR -->
                                        <!-- BEGIN PROFILE CONTENT -->
                                        <div class="profile-content">
                                            <div class="row">

                                            @include('common.dashboard_header')
                                                <!--  BEGIN WEIGHT INFO SECTION -->

                                                <!--End Activity Section -->
                                                <div class="col-md-12">

                                                    <div class="portlet light ">
                                                        <div class="portlet-title tabbable-line">
                                                            <div class="caption ">
                                                                <i class="fa fa-gear font-green "></i>
                                                                <span class="caption-subject font-green bold uppercase">Member Profile & Settings</span>
                                                            </div>

                                                        </div>
                                                        <div class="portlet-body">
                                                            <!--SECTION1 PROFILE -->
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">

                                                                    <div class="caption">
                                                                        <i class="fa fa-user"></i>Profile </div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                                                    </div>
                                                                    <!--  <div class="actions">
                                                                      <a href="javascript:;" class="btn font-white btn-sm">
                                                                             <i class="fa fa-pencil"></i> Edit </a>
                                                                     </div> -->

                                                                </div>
                                                                <div class="portlet-body">


                                                                    {!! Form::open( ['method' => 'POST', 'id' => 'form_member_profile', 'onsubmit' => 'return false;','role' => 'form', 'enctype'=>'multipart/form-data','url' => route('update_member_profile_setting') ] ) !!}

                                                                        <input type="hidden" name="patient_id" value="{{ $profile_detail->patient_id }}">
                                                                        <input type="hidden" name="user_id" value="{{ $profile_detail->user_id }}">
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">First Name</label>
                                                                            <input type="text" placeholder="First Name" class="form-control" name="first_name" value="{{$profile_detail->first_name}}"  /> </div>
                                                                        {{--<div class="form-group col-sm-6">
                                                                            <label class="control-label">Middle Name</label>
                                                                            <input type="text" placeholder="Middle Name" class="form-control" name="middle_name" value=""  /> </div>
                                                                         <div class="clearfix"></div> --}}
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Last Name</label>
                                                                            <input type="text" placeholder="Last Name" class="form-control" name="last_name" value="{{$profile_detail->last_name}}" /> </div>
                                                                        {{--<div class="form-group col-sm-6" >
                                                                            <label class="control-label">Nick Name</label>
                                                                            <input type="text" placeholder="Nick Name" class="form-control" name="nick_name" value="{{$profile_detail->nickname}}" /> </div>--}}
                                                                    <div class="clearfix"></div>
                                                                    <div class="form-group col-sm-6">
                                                                            <label class="control-label">Email</label>
                                                                            <input type="text" placeholder="Email" class="form-control" name="email" value="{{$profile_detail->email}}" /> </div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Mobile Number</label>
                                                                            <input type="text" placeholder="+1 646 580 DEMO (6284)" name="phone" class="form-control"  value="{{$profile_detail->phone}}"/> </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="form-group col-sm-6">
                                                                            <label class="control-label ">Date of Birth</label>
                                                                            <div class="input-group date date-picker">
                                                                                <input class="form-control" name="dob" id ="dob" value="{{date("m/d/Y", strtotime($profile_detail->dob))}}" type="text" readonly>
                                                                                <span class="input-group-btn">
                                                                                    <button class="btn default" type="button">
                                                                                        <i class="fa fa-calendar"></i>
                                                                                    </button>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Gender</label>
                                                                            <select class="form-control" name="gender">
                                                                                <option value="Male"  @if($profile_detail->gender=="Male") {{ "selected" }} @endif >Male</option>
                                                                                <option value="Female"  @if($profile_detail->gender=="Female") {{ "selected" }} @endif >Female</option>
                                                                                <option value="Other"  @if($profile_detail->gender=="Other") {{ "selected" }} @endif >Other</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Address1</label>
                                                                            <textarea class="form-control" rows="1" name="address1">{{$profile_detail->address}}</textarea>
                                                                        </div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Address2</label>
                                                                            <textarea class="form-control" rows="1" name="address2">{{$profile_detail->mail_address2}}</textarea>
                                                                        </div>
                                                                         <div class="clearfix"></div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">City</label>
                                                                            <input type="text" placeholder="City " class="form-control" name="city" value="{{$profile_detail->city}}" /> </div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">State</label>
                                                                            {!! Form::select('state', $state, $profile_detail->state,  ['class' =>'form-control']) !!}        </div>
                                                                      <div class="clearfix"></div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Zip</label>
                                                                            <input type="text" placeholder="Zip" name="zip" value="{{ $profile_detail->zip_code}}"class="form-control" /> </div>
                                                                        <!--      <div class="form-group col-sm-6">
                                                                             <label class="control-label">Race</label>
                                                                                 <select class="form-control">
                                                                                         <option value="" selected="" disabled="" class="">(select)</option><option value="American Indian or Allaskan Native">American Indian or Allaskan Native</option><option value="Black/African American">Black/African American</option><option value="Hispanic/Latino">Hispanic/Latino</option><option value="Native Hawaiin or other Pacific Islander">Native Hawaiin or other Pacific Islander</option><option value="White/Caucasian">White/Caucasian</option><option value="Mixed race/mutiracial">Mixed race/mutiracial</option><option value="Do not know">Do not know</option>
                                                                                 </select>
                                                                             </div> -->
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Education</label>
                                                                            <select class="form-control" name="education">
                                                                                <option value="" selected="" disabled="" class="">(select)</option><option value="Elementary school" @if($profile_detail->education=="Elementary school"){{ "selected" }} @endif>Elementary school</option><option value="Some high school" @if($profile_detail->education=="Some high school"){{ "selected" }} @endif>Some high school</option><option value="High school degree"  @if($profile_detail->education=="High school degree"){{ "selected" }} @endif>High school degree</option><option value="Some college"  @if($profile_detail->education=="Some college"){{ "selected" }} @endif>Some college</option><option value="College degree" @if($profile_detail->education=="College degree"){{ "selected" }} @endif>College degree</option><option value="Graduate degree" @if($profile_detail->education=="Graduate degree"){{ "selected" }} @endif>Graduate degree</option><option value="Do not know" @if($profile_detail->education=="Do not know"){{ "selected" }} @endif>Do not know</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Language</label>
                                                                            <select class="form-control" name="language">
                                                                                <option value="" selected="" disabled="" class="">(select)</option><option value="English" @if($profile_detail->language=="English"){{ "selected" }} @endif>English</option><option value="Spanish"  @if($profile_detail->language=="Spanish"){{ "selected" }} @endif>Spanish</option><option value="Do not know"  @if($profile_detail->language=="Do not know"){{ "selected" }} @endif>Do not know</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Dietary Restriction</label>
                                                                            <select class="form-control" id="ddDietaryRestrictions" name="dieatry_restriction[]" multiple>
                                                                                <option value="Dairy free" @if (in_array("Dairy free", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif >Dairy free</option><option value="Egg Allergy"  @if (in_array("Egg Allergy", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Egg Allergy</option><option value="Fish Allergy" @if (in_array("Fish Allergy", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Fish Allergy</option><option value="Gluten free"  @if (in_array("Gluten free", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Gluten free</option><option value="Kosher" @if (in_array("Kosher", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Kosher</option><option value="Lactose free"  @if (in_array("Lactose free", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Lactose free</option><option value="Liquid diet" @if (in_array("Liquid diet", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Liquid diet</option><option value="Low fat" @if (in_array("Low fat", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Low fat</option><option value="Low sodium" @if (in_array("Low sodium", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Low sodium</option><option value="Milk Allergy"  @if (in_array("Milk Allergy", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif >Milk Allergy</option><option value="Muslim" @if (in_array("Muslim", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif >Muslim</option><option value="None" @if (in_array("None", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>None</option><option value="Other"  @if (in_array("Other", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Other</option><option value="Paleo" @if (in_array("Paleo", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Paleo</option><option value="Peanut Allergy"  @if (in_array("Peanut Allergy", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Peanut Allergy</option><option value="Shellfish Allergy" @if (in_array("Shellfish Allergy", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Shellfish Allergy</option><option value="Soy Allergy" @if (in_array("Soy Allergy", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif >Soy Allergy</option><option value="Treenut Allergy" @if (in_array("Treenut Allergy", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Treenut Allergy</option><option value="Vegetarian (Asian)" @if (in_array("Vegetarian (Asian)", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Vegetarian (Asian)</option><option value="Vegetarian (Indian)" @if (in_array("Vegetarian (Indian)", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Vegetarian (Indian)</option><option value="Vegetarian with dairy" @if (in_array("Vegetarian with dairy", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Vegetarian with dairy</option><option value="Vegetarian with fish (&quot;pescatarian&quot;) @if (in_array("Vegetarian with fish (&quot;pescatarian&quot;)", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif">Vegetarian with fish ("pescatarian")</option><option value="Vegetarian/vegan - no animal products" @if (in_array("Vegetarian/vegan - no animal products", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Vegetarian/vegan - no animal products</option><option value="Wheat free" @if (in_array("Wheat free", explode(", ",$profile_detail->dietary_restrictions))) {{ "selected" }}  @endif>Wheat free</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Primary Meal Preparer</label>
                                                                            <select class="form-control" id="ddMealPreparer" name="primary_meal_prepare[]" multiple>
                                                                                <option value="Self" @if (in_array("Self", explode(", ",$profile_detail->meal_preparer))) {{ "selected" }}  @endif>Self</option><option value="Spouse/Partner" @if (in_array("Spouse/Partner", explode(", ",$profile_detail->meal_preparer))) {{ "selected" }}  @endif >Spouse/Partner</option><option value="Adult child" @if (in_array("Adult child", explode(", ",$profile_detail->meal_preparer))) {{ "selected" }}  @endif >Adult child</option><option value="Other caregiver" @if (in_array("Other caregiver", explode(", ",$profile_detail->meal_preparer))) {{ "selected" }}  @endif >Other caregiver</option><option value="Do not know" @if (in_array("Do not know", explode(", ",$profile_detail->meal_preparer))) {{ "selected" }}  @endif >Do not know</option>
                                                                            </select>
                                                                        </div>
                                                                        <!--   <div class="form-group col-sm-6">
                                                                           <label class="control-label">ICF Signed</label><div class="clearfix"></div>
                                                                           <input type="checkbox" class="make-switch" data-on-text="On" data-off-text="Off" data-on-color="default" data-off-color="default">
                                                                          </div> -->
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">MRN</label>
                                                                            <input type="text" placeholder="" class="form-control" name="mrn" value="{{ $profile_detail->mrn }}"/> </div>
                                                                    <div class="clearfix"></div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Facility</label>
                                                                            <select class="form-control" name="facility" @if(session('userRole')!="admin") {{ "disabled" }} @endif >
                                                                                @if(!empty($facility_list))
                                                                                @foreach($facility_list as $facility)
                                                                                    <option value="{{$facility->facility_id}}" @if($facility->facility_id == $profile_detail->facility_id ){{"selected"}} @endif >{{$facility->name}}</option>
                                                                                @endforeach
                                                                                @endif
                                                                            </select>
                                                                        </div>
                                                                        <!--      <div class="form-group col-sm-6">
                                                                             <label class="control-label">Coaching Style</label>
                                                                                 <select class="form-control">
                                                                                        <option value="" selected="" disabled="" class="">(select)</option><option value="Gentle">Gentle</option><option value="Moderate">Moderate</option><option value="Hands on">Hands on</option>
                                                                                 </select>
                                                                             </div> -->
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Group Display Name</label>
                                                                            <input type="text" placeholder="" class="form-control" name="group_display_name"  value="@if($profile_detail->group_display_name!="") {{$profile_detail->group_display_name }}@else{{ $profile_detail->first_name }} @endif"/> </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Group Visibility</label>
                                                                            <div class="clearfix"></div>
                                                                            <input type="checkbox" class="make-switch" @if($profile_detail->group_visibility == "Private"){{"checked"}} @endif name="group_visibility"  value="1" data-on-text="Yes" data-off-text="No" data-on-color="default" data-off-color="default">
                                                                        </div>

                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Activity</label>
                                                                            <div class="input-group">
                                                                                <input class="form-control" placeholder="Steps"  name="activity"  aria-describedby="" type="text" pattern="\d*" value="{{ $profile_detail->activity_steps }}">
                                                                                <span class="input-group-addon" id="steps_per_day">Steps per day</span>
                                                                            </div>   </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class=" col-sm-6">
                                                                            <label class="control-label">Height</label>

                                                                            <div class="row">
                                                                                <div class="col-sm-6 form-group">
                                                                                    <div class="input-group">
                                                                                        <input class="form-control" placeholder="Feet" aria-describedby="sizing-addon1" type="text" pattern="\d*" name="height_feet" value="{{ $profile_detail->height_feet }}">
                                                                                        <span class="input-group-addon" id="feet" >Feet</span>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-sm-6 form-group">
                                                                                    <div class="input-group">
                                                                                        <input class="form-control" placeholder="Inches" aria-describedby="sizing-addon1" type="text" pattern="\d*" name="height_inch" value="{{ $profile_detail->height_inch }}">
                                                                                        <span class="input-group-addon" id="inch">Inches</span>
                                                                                    </div>
                                                                                </div></div>
                                                                        </div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Starting Weight(lbs)</label>
                                                                            <input type="text" placeholder="" name="starting_weight" class="form-control"   value="{{ $profile_detail->starting_weight }}"/> </div>
                                                                        <div class="clearfix"></div>


                                                                        {{--<div class="form-group col-sm-6">
                                                                            <label class="control-label">Goal Steps</label>
                                                                            <input type="text" placeholder="" class="form-control" name="goal_steps"  value="" /> </div>--}}
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Calorie Budget</label>
                                                                            <input type="text" placeholder="" class="form-control" name="calorie_budget" value="{{ $profile_detail->daily_calorie_budget }}" /> </div>


                                                                    @if($user_role =='techsupport')
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Temprory Password</label>
                                                                            <input type="password" placeholder="Temprory Password" class="form-control" name="temp_password"  /> </div>
                                                                        <div class="clearfix"></div>
                                                                    @endif




                                                                        <div class="form-group col-sm-12 margiv-top-10">
                                                                            <button type="submit" class="btn green-jungle"> Update </button>

                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    {!! Form::close() !!}
                                                                </div>
                                                            </div>
                                                            <!-- END PROFILES -->
                                                            <!--SECTION2 REMINDERS -->

                                                            <div class="portlet box green">
                                                                <div class="portlet-title">

                                                                    <div class="caption">
                                                                        <i class="fa fa-bell"></i>Reminders </div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                                                    </div>
                                                                    <!-- <div class="actions">
                                                                     <a href="javascript:;" class="btn font-white btn-sm">
                                                            <i class="fa fa-pencil"></i> Edit </a>
                                                                    </div> -->

                                                                </div>
                                                                <div class="portlet-body">
                                                                    {!! Form::open( ['method' => 'POST', 'id' => 'form_member_reminder', 'onsubmit' => 'return false;','role' => 'form', 'enctype'=>'multipart/form-data','url' => route('update_member_reminder') ] ) !!}

                                                                    <input type="hidden" name="patient_id" value="{{ $profile_detail->patient_id }}">
                                                                    <input type="hidden" name="user_id" value="{{ $profile_detail->user_id }}">
                                                                    <input type="hidden" name="target_id" value="{{ $target_id }}">
                                                                        <h4 class="form-section">Meals</h4>
                                                                        <div class="">
                                                                            <div class="form-group col-sm-12 sampleinput1">
                                                                                <label class="control-label col-md-6"><h5 class="">Remind Me to Log Meals </h5></label>
                                                                                <div class="col-sm-6 text-right">
                                                                                    <input type="checkbox"    name="is_log_meal_time" value="1"  @if($is_meal_time==1){{"checked"}} @endif class="make-switch mainmeals main main1" data-label-icon="fa fa-fullscreen" data-on-text="<i class='fa fa-bell'></i>" data-off-text="<i class='fa fa-bell-slash'></i>"  data-on-color="default" data-off-color="default">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group col-sm-6">
                                                                                <label class="control-label col-md-12">Breakfast</label>
                                                                                <div class="col-md-6">
                                                                                    <div class="input-icon ">
                                                                                        <i class="fa fa-clock-o"></i>
                                                                                        <input type="text" name="breakfast" class="form-control form_datetime" placeholder="07:00 AM"  value="{{ $Breakfast }}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 text-right">
                                                                                    <input type="checkbox" name="is_breakfast_time"   value="1" @if($is_breakfast_time==1) {{ "checked" }} @endif class="make-switch ckmeals ck ck1" data-label-icon="fa fa-fullscreen" data-on-text="<i class='fa fa-bell'></i>" data-off-text="<i class='fa fa-bell-slash'></i>"  data-on-color="default" data-off-color="default">

                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group col-sm-6">
                                                                                <label class="control-label col-md-12">Lunch</label>
                                                                                <div class="col-md-6">
                                                                                    <div class="input-icon ">
                                                                                        <i class="fa fa-clock-o"></i>
                                                                                        <input type="text" name="lunch"  class="form-control form_datetime" placeholder="12:00 PM" value="{{$Lunch}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 text-right">
                                                                                    <input type="checkbox"  name="is_lunch_time"  value="1" @if($is_lunch_time==1) {{ "checked" }} @endif  class="make-switch ckmeals ck ck2" data-label-icon="fa fa-fullscreen" data-on-text="<i class='fa fa-bell'></i>" data-off-text="<i class='fa fa-bell-slash'></i>"  data-on-color="default" data-off-color="default">

                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group col-sm-6">
                                                                                <label class="control-label col-md-12">Snack</label>
                                                                                <div class="col-md-6">
                                                                                    <div class="input-icon ">
                                                                                        <i class="fa fa-clock-o"></i>
                                                                                        <input type="text" name="snack" class="form-control form_datetime" placeholder="03:30 PM" value="{{$Snack}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 text-right">
                                                                                    <input type="checkbox"   name="is_snack_time"  value="1" @if($is_snack_time==1) {{ "checked" }} @endif class="make-switch ckmeals ck ck3" data-label-icon="fa fa-fullscreen" data-on-text="<i class='fa fa-bell'></i>" data-off-text="<i class='fa fa-bell-slash'></i>"  data-on-color="default" data-off-color="default">

                                                                                </div>
                                                                            </div>

                                                                            <div class="form-group col-sm-6">
                                                                                <label class="control-label col-md-12">Dinner</label>
                                                                                <div class="col-md-6">
                                                                                    <div class="input-icon ">
                                                                                        <i class="fa fa-clock-o"></i>
                                                                                        <input type="text" name="dinner" class="form-control form_datetime" placeholder="06:30 PM" value="{{$Dinner}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 text-right">
                                                                                    <input type="checkbox" name="is_dinner_time"  value="1" @if($is_dinner_time==1) {{ "checked" }} @endif  class="make-switch ckmeals ck ck4" data-label-icon="fa fa-fullscreen" data-on-text="<i class='fa fa-bell'></i>" data-off-text="<i class='fa fa-bell-slash'></i>" data-on-color="default" data-off-color="default">

                                                                                </div>
                                                                            </div>

                                                                            <div class="clearfix"></div>

                                                                        </div>
                                                                        <div class="clearfix"></div>


                                                                        <hr> <!-- end meal reminders-->
                                                                        <h4 class="form-section">Weight</h4>
                                                                        <div class="">
                                                                            <div class="form-group col-sm-12">
                                                                                <label class="control-label col-md-12">Weight Reminder</label>
                                                                                <div class="col-md-3">
                                                                                    <div class="input-icon ">
                                                                                        <i class="fa fa-clock-o"></i>
                                                                                        <input type="text" name="weight_reminder" class="form-control form_datetime" placeholder="06:45 P.M." value="{{$weight_time}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-9 text-right">
                                                                                    <input type="checkbox" value="1" name="is_weight_time"  @if($is_weight_time==1) {{ "checked" }} @endif class="make-switch " data-label-icon="fa fa-fullscreen" data-on-text="<i class='fa fa-bell'></i>" data-off-text="<i class='fa fa-bell-slash'></i>"  data-on-color="default" data-off-color="default">

                                                                                </div>
                                                                            </div>
                                                                            <div class="clearfix"></div>
                                                                        </div>

                                                                        <hr><!-- end weight reminders-->

                                                                        <h4 class="form-section">Message Notification</h4>
                                                                        <div class="">
                                                                            <div class="form-group col-sm-12 sampleinput1">
                                                                                <label class="control-label col-md-8"><h5 class="">When Coaches message me, alert me via email  </h5></label>
                                                                                <div class="col-sm-4 text-right">
                                                                                    <input type="checkbox"  name="is_email" value="1"  @if($email==1) {{ "checked" }} @endif  class="make-switch " data-label-icon="fa fa-fullscreen" data-on-text="<i class='fa fa-bell'></i>" data-off-text="<i class='fa fa-bell-slash'></i>"  data-on-color="default" data-off-color="default">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group col-sm-12 sampleinput1">
                                                                                <label class="control-label col-md-8"><h5 class="">When Coaches message me, alert me via App notification  </h5></label>
                                                                                <div class="col-sm-4 text-right">
                                                                                    <input type="checkbox" name="is_app_notification" value="1"  @if($is_in_app_message_notify==1) {{ "checked" }} @endif  class="make-switch " data-label-icon="fa fa-fullscreen" data-on-text="<i class='fa fa-bell'></i>" data-off-text="<i class='fa fa-bell-slash'></i>"  data-on-color="default" data-off-color="default">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group col-sm-12">
                                                                                <label class="control-label col-md-6"><h5 class="">Remind me to contact coach</h5></label>
                                                                                <div class="col-md-6 text-right">
                                                                                    <input type="checkbox" name="is_coach_communication_reminder" value="1"  @if($is_coach_communication_reminder==1) {{ "checked" }} @endif   class="make-switch " data-label-icon="fa fa-fullscreen" data-on-text="<i class='fa fa-bell'></i>" data-off-text="<i class='fa fa-bell-slash'></i>"  data-on-color="default" data-off-color="default">
                                                                                </div>
                                                                                <div class="clearfix"></div>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label"> Select Time</label>
                                                                                    <div class="input-icon ">
                                                                                        <i class="fa fa-clock-o"></i>
                                                                                        <input type="text" name="time" class="form-control form_datetime" placeholder="06:45 P.M." value="{{$time_of_day}}">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <label class="control-label"> Select Frequency</label>
                                                                                    <select class="form-control" name="frequency" id="frequency">
                                                                                        <option value="Once/Week"  @if($type_of_reminder=="Once/Week"){{ "selected" }} @endif > Once/Week  </option>
                                                                                        <option value="Other/Week"  @if($type_of_reminder=="Other/Week"){{ "selected" }} @endif > Every Other Week </option>
                                                                                        <option value="Once/Month"  @if($type_of_reminder=="Once/Month"){{ "selected" }} @endif>  Once/Month  </option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-md-4 @if($type_of_reminder=='Once/Month') {{ 'hide' }} @else {{ '' }} @endif" id="d_week">
                                                                                    <label class="control-label"> Select Day of Week</label>
                                                                                    <select class="form-control" name="week">
                                                                                        <option disabled="" value="">(select)</option>
                                                                                        <option value="Monday" @if($day_of_week=="Monday"){{ "selected" }} @endif >Monday</option>
                                                                                        <option value="Tuesday" @if($day_of_week=="Tuesday"){{ "selected" }} @endif>Tuesday</option>
                                                                                        <option value="Wednesday" @if($day_of_week=="Wednesday"){{ "selected" }} @endif>Wednesday</option>
                                                                                        <option value="Thursday" @if($day_of_week=="Thursday"){{ "selected" }} @endif>Thursday</option>
                                                                                        <option value="Friday" @if($day_of_week=="Friday"){{ "selected" }} @endif>Friday</option>
                                                                                        <option value="Saturday" @if($day_of_week=="Saturday"){{ "selected" }} @endif>Saturday</option>
                                                                                        <option value="Sunday" @if($day_of_week=="Sunday"){{ "selected" }} @endif>Sunday</option>
                                                                                    </select>
                                                                                </div>


                                                                                <div class="col-md-4 @if($type_of_reminder=='Once/Month') {{ '' }} @else {{ 'hide' }} @endif" id="d_month">
                                                                                    <label class="control-label"> Select Date of Month</label>
                                                                                    <select class="form-control" name="month">
                                                                                        <option disabled="" value="" >(select)</option>
                                                                                        <option value="1" @if($date_of_month== 1){{ "selected" }} @endif>1</option>
                                                                                        <option value="2" @if($date_of_month== 2){{ "selected" }} @endif>2</option>
                                                                                        <option value="3" @if($date_of_month== 3){{ "selected" }} @endif>3</option>
                                                                                        <option value="4" @if($date_of_month== 4){{ "selected" }} @endif>4</option>
                                                                                        <option value="5" @if($date_of_month== 5){{ "selected" }} @endif>5</option>
                                                                                        <option value="6" @if($date_of_month== 6){{ "selected" }} @endif>6</option>
                                                                                        <option value="7" @if($date_of_month== 7){{ "selected" }} @endif>7</option>
                                                                                        <option value="8" @if($date_of_month== 8){{ "selected" }} @endif>8</option>
                                                                                        <option value="9" @if($date_of_month== 9){{ "selected" }} @endif>9</option>
                                                                                        <option value="10" @if($date_of_month== 10){{ "selected" }} @endif>10</option>
                                                                                        <option value="11" @if($date_of_month== 11){{ "selected" }} @endif>11</option>
                                                                                        <option value="12" @if($date_of_month== 12){{ "selected" }} @endif>12</option>
                                                                                        <option value="13" @if($date_of_month== 13){{ "selected" }} @endif>13</option>
                                                                                        <option value="14" @if($date_of_month== 14){{ "selected" }} @endif>14</option>
                                                                                        <option value="15" @if($date_of_month== 15){{ "selected" }} @endif>15</option>
                                                                                        <option value="16" @if($date_of_month== 16){{ "selected" }} @endif>16</option>
                                                                                        <option value="17" @if($date_of_month== 17){{ "selected" }} @endif>17</option>
                                                                                        <option value="18" @if($date_of_month== 18){{ "selected" }} @endif>18</option>
                                                                                        <option value="19" @if($date_of_month== 19){{ "selected" }} @endif>19</option>
                                                                                        <option value="20" @if($date_of_month== 20){{ "selected" }} @endif>20</option>
                                                                                        <option value="21" @if($date_of_month== 21){{ "selected" }} @endif>21</option>
                                                                                        <option value="22" @if($date_of_month== 22){{ "selected" }} @endif>22</option>
                                                                                        <option value="23" @if($date_of_month== 23){{ "selected" }} @endif>23</option>
                                                                                        <option value="24" @if($date_of_month== 24){{ "selected" }} @endif>24</option>
                                                                                        <option value="25" @if($date_of_month== 25){{ "selected" }} @endif>25</option>
                                                                                        <option value="26" @if($date_of_month== 26){{ "selected" }} @endif>26</option>
                                                                                        <option value="27" @if($date_of_month== 27){{ "selected" }} @endif>27</option>
                                                                                        <option value="28" @if($date_of_month== 28){{ "selected" }} @endif>28</option>
                                                                                        <option value="29" @if($date_of_month== 29){{ "selected" }} @endif>29</option>
                                                                                        <option value="30" @if($date_of_month== 30){{ "selected" }} @endif>30</option>
                                                                                        <option value="31" @if($date_of_month== 31){{ "selected" }} @endif>31</option>
                                                                                    </select>
                                                                                </div>




                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <!-- end message reminders-->
                                                                        <div class="form-group col-sm-12 margin-top-10">
                                                                            <button type="submit" class="btn green-jungle"> Update </button>

                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    {!! Form::close() !!}
                                                                </div>
                                                            </div>
                                                            <!-- END REMINDERS -->


                                                            <!--diabetes information-->
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-hospital-o"></i>Diabetes Information </div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                                                    </div>
                                                                    <!-- <div class="actions">
                                                                     <a href="javascript:;" class="btn font-white btn-sm">
                                                            <i class="fa fa-pencil"></i> Edit </a>
                                                                    </div> -->

                                                                </div>
                                                                <div class="portlet-body">
                                                                    {!! Form::open( ['method' => 'POST', 'id' => 'form_member_diabetes_info', 'onsubmit' => 'return false;','role' => 'form', 'enctype'=>'multipart/form-data','url' => route('update_member_diabetes_info') ] ) !!}
                                                                    <input type="hidden" name="patient_id" value="{{ $profile_detail->patient_id }}">
                                                                    <input type="hidden" name="user_id" value="{{ $profile_detail->user_id }}">
                                                                    <div class="form-group col-sm-6">
                                                                            <label class="control-label">Diagnosis</label>
                                                                            <select class="form-control" name="diagnosis">
                                                                                <option value="" selected="" disabled="" class="">(select)</option><option value="Type 2"   @if($profile_detail->diagnosis=="Type 2"){{ "selected" }} @endif >Type 2</option><option value="Gestational" @if($profile_detail->diagnosis=="Gestational"){{ "selected" }} @endif>Gestational</option><option value="Type 1" @if($profile_detail->diagnosis=="Type 1"){{ "selected" }} @endif>Type 1</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Year of Diagnosis</label>
                                                                            <select class="form-control" name="year_of_diagnosis">
                                                                                <option value="" selected="" disabled="" class="">(select)</option><option value="Less than 2 years" @if($profile_detail->diagnosis_date=="Less than 2 years"){{ "selected" }} @endif >Less than 2 years</option><option value="2-5 years" @if($profile_detail->diagnosis_date=="2-5 years"){{ "selected" }} @endif >2-5 years</option><option value="6-10 years" @if($profile_detail->diagnosis_date=="6-10 years"){{ "selected" }} @endif >6-10 years</option><option value="More than 10 years" @if($profile_detail->diagnosis_date=="More than 10 years"){{ "selected" }} @endif >More than 10 years</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Time since last Diabetes Education</label>
                                                                            <select class="form-control" name="time_since_last_diabetes_education">
                                                                                <option value="" selected="" disabled="" class="">(select)</option><option value="Less than 2 years" @if($profile_detail->time_since_last_diabetes_edu=="Less than 2 years"){{ "selected" }} @endif>Less than 2 years</option><option value="2-5 years" @if($profile_detail->time_since_last_diabetes_edu=="2-5 years"){{ "selected" }} @endif >2-5 years</option><option value="6-10 years" @if($profile_detail->time_since_last_diabetes_edu=="6-10 years"){{ "selected" }} @endif >6-10 years</option><option value="More than 10 years" @if($profile_detail->time_since_last_diabetes_edu=="More than 10 years"){{ "selected" }} @endif >More than 10 years</option><option value="Never had Diabetes Education" @if($profile_detail->time_since_last_diabetes_edu=="Never had Diabetes Education"){{ "selected" }} @endif >Never had Diabetes Education</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Diabetes Support</label>
                                                                            <div class="input-group">
                                                                            <select class="form-control" multiple="" id="ddDiabetesSupport" name="diabetes_support[]">
                                                                                <option value="Family" @if(in_array("Family", explode(", ", $profile_detail->diabetes_support))){{ "selected" }} @endif>Family</option><option value="Friend(s)" @if(in_array("Friend(s)", explode(", ", $profile_detail->diabetes_support))){{ "selected" }} @endif >Friend(s)</option><option value="Co-worker(s)" @if(in_array("Co-worker(s)", explode(", ", $profile_detail->diabetes_support))){{ "selected" }} @endif >Co-worker(s)</option><option value="Healthcare provider(s)" @if(in_array("Healthcare provider(s)", explode(", ", $profile_detail->diabetes_support))){{ "selected" }} @endif >Healthcare provider(s)</option><option value="Support group" @if(in_array("Support group", explode(", ", $profile_detail->diabetes_support))){{ "selected" }} @endif >Support group</option><option value="No one" @if(in_array("No one", explode(", ", $profile_detail->diabetes_support))){{ "selected" }} @endif >No one</option>
                                                                            </select>
                                                                            </div>
                                                                        </div>
                                                                    <div class="clearfix"></div>
                                                                        <div class="form-group col-sm-6"  data-error-container="#being_treated_depression_group-error">
                                                                            <label class="control-label">Being Treated of depression recently</label>
                                                                            <div class="mt-radio-inline">
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="being_treated_depression"  class="being_treated_depression" value="Yes"   @if($profile_detail->treated_depression=="Yes"){{ "checked" }} @endif > Yes
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="being_treated_depression"  class="being_treated_depression" value="No" @if($profile_detail->treated_depression=="No"){{ "checked" }} @endif> No
                                                                                    <span></span>
                                                                                </label>
                                                                                <label class="mt-radio">
                                                                                    <input type="radio" name="being_treated_depression"  class="being_treated_depression"  value="Do not know" @if($profile_detail->treated_depression=="Do not know"){{ "checked" }} @endif> I don't Know
                                                                                    <span></span>
                                                                                </label>
                                                                            </div>
                                                                            <div id="being_treated_depression_group-error"> </div>
                                                                        </div>
                                                                       <div class="clearfix"></div>
                                                                        <div class="form-group col-sm-12 margin-top-10">
                                                                            <button type="submit" class="btn green-jungle"> Update </button>

                                                                        </div>

                                                                        <div class="clearfix"></div>

                                                                    {!! Form::close() !!}
                                                                </div>
                                                            </div>


                                                            <!-- member questionare -->
                                                            <div class="portlet box green">
                                                                <div class="portlet-title">
                                                                    <div class="caption">
                                                                        <i class="fa fa-question"></i>Member Questionare </div>
                                                                    <div class="tools">
                                                                        <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                                                                    </div>
                                                                    <!--  <div class="actions">
                                                                      <a href="javascript:;" class="btn font-white btn-sm">
                                                             <i class="fa fa-pencil"></i> Edit </a>
                                                                     </div> -->

                                                                </div>
                                                                <div class="portlet-body">
                                                                    <form role="form" action="#">
                                                                        <div class="form-group col-sm-12">
                                                                            <label class="control-label">Is there a specific motivation, goal or event that is causing you to join our program at this particular time? If so, what?</label>
                                                                            <textarea class="form-control" rows="1" placeholder="Motivational Quote ">Lorem ipsum dolor sit amet diam nonummy nibh dolore.</textarea>
                                                                            <!--   <div class="mt-checkbox-list">
                                                                                      <label class="mt-checkbox">
                                                                                          <input type="checkbox"> My Coaches can see My Motivation
                                                                                          <span></span>
                                                                                      </label>
                                                                                  </div> -->
                                                                        </div>

                                                                        <div class="form-group col-sm-12">
                                                                            <label class="control-label">On a scale of 1-10, how READY are you to commit to a lifestyle-changing program? (10 would be the most ready you have ever been - this is a top priority in your life right now; 1 is not ready at all.)o Lifestyle changing program </label>
                                                                            <select class="form-control">
                                                                                <option>1</option>
                                                                                <option>2</option>
                                                                                <option>3</option>
                                                                                <option>4</option>
                                                                                <option>5</option>
                                                                                <option>6</option>
                                                                                <option>7</option>
                                                                                <option>8</option>
                                                                                <option>9</option>
                                                                                <option>10</option>

                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-sm-12">
                                                                            <label class="control-label">Are there any habits, barriers or constraints that you see standing it the way of your success? (check all that apply)</label>
                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <select id="habits" class="form-control" multiple>
                                                                                        <option>Physical activity barriers, e.g., injuries</option>
                                                                                        <option>Lack of time</option>
                                                                                        <option>Access to healthy food</option>
                                                                                        <option>Lack of support from family/partner</option>
                                                                                        <option>Unhealthy lifestyles or eating patterns of family/partner</option>
                                                                                        <option>Other add more </option>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <input type="text" placeholder="State if others" class="form-control" value="" />
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="form-group col-sm-12">
                                                                            <label class="control-label">Have you ever been a part of a program or diet focused on losing weight? [If no, skip to next question.] If yes, What program? (single answer)</label>
                                                                            <select class="form-control">
                                                                                <option value="" selected="" disabled="" class="">(select)</option>
                                                                                <option>Weight Watchers</option>
                                                                                <option>Jenny Craig</option>
                                                                                <option>NutriSystem</option>
                                                                                <option>Atkins</option>
                                                                                <option>Other</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-sm-12">
                                                                            <label class="control-label">What was your experience? (single answer)</label>
                                                                            <select class="form-control">
                                                                                <option value="" selected="" disabled="" class="">(select)</option>
                                                                                <option>Did not lose weight</option>
                                                                                <option>Lost weight but regained all</option>
                                                                                <option>Lost weight but regained some</option>
                                                                                <option>Lost weight and regained none</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-sm-6">
                                                                            <label class="control-label">Preferred coaching style</label>
                                                                            <select class="form-control">
                                                                                <option value="" selected="" disabled="" class="">(select)</option>
                                                                                <option>Cheerleader</option>
                                                                                <option>Drill sergeant</option>
                                                                                <option>Explorer</option>
                                                                                <option>Listener</option>
                                                                                <option>Challenger</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="form-group col-sm-12 margin-top-10">
                                                                            <a href="javascript:;" class="btn green-jungle"> Update </a>  </div>
                                                                        <div class="clearfix"></div>
                                                                    </form>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>








                                                </div>
                                            </div>
                                        </div>
                                        <!-- END PROFILE CONTENT -->
                                    </div>
                                </div>
                            </div>
                            <!-- END PAGE CONTENT INNER -->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT BODY -->
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->

            @include('common.motivation')
            @include('common.user_profile_image')
@stop



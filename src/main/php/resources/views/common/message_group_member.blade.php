<!--modal message Group message -->
<div class="modal fade" id="msg-group-member"  role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Messages Group Members</h4>
            </div>
            <div class="modal-body">
                <div class="modal-form-title">
                    <div class="col-sm-12 caption font-green">
                        <i class="fa fa-users font-green"></i>
                        <span class="caption-subject bold uppercase message_group_member_group_name"></span>
                    </div>
                   {{-- <div class="col-sm-6 form-horizontal pull-right text-right">
                        <strong> 2</strong> at Milestone Risk </br>
                        <strong> 0</strong> Missing Staring Weight
                    </div>--}}
                    <div class="clearfix"></div>
                </div>

                {!! Form::open( ['method' => 'POST', 'id' => 'message_group_member_form', 'onsubmit' => 'return false;','role' => 'form', 'url' => route(session('userRole') . '.message-group-member'), 'save_url' => route(session('userRole') . '.saveGroupMessage')  ] ) !!}
                {!! Form::hidden('group_id', '' ) !!}
                {!! Form::hidden('user_id', session('userId') ) !!}
                    <!-- <div class="form-group pull-right text-right">
                        <a class="btn btn-sm green-jungle btn-new-msg"> &nbsp; Create New Message &nbsp; </a>
                    </div>
                    <div class="clearfix"></div>   -->
                    <div class="form-group">
                        <label for="group-members" class="control-label"> Select Group Members
                        </label>
                        <div class="input-group select2-bootstrap-append">
                            <select id="group-members" class="form-control group-members" name="group_members[]" multiple >

                            </select>
                            <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-open="group-members">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="recom-messages2" class="control-label"> Select Canned Message
                        </label>
                        <div class="input-group select2-bootstrap-append">
                            <select id="recom-messages2" class="form-control recom-messages2" name="canned_message" >
                                <option value="" disabled selected>Select Recommended Message</option>
                                <option value="Let us go back to what was successful for you..">Let us go back to what was successful for you..</option>
                                <option value="Just checking in to see how you’re doing..">Just checking in to see how you’re doing..</option>
                                <option value="You are in danger of being dis-enrolled..">You are in danger of being dis-enrolled..</option>
                            </select>
                            <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-open="recom-messages2">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Message</label>
                        <textarea class="recomend-text form-control" name="message" rows="3"></textarea>
                    </div>
                    <div class="form-group text-right">
                        <button type="button" id="attachment" class=" btn btn-sm btn-outline dark "> <i class="fa fa-paperclip" aria-hidden="true"></i>  Attachment &nbsp; </button>
                        <button type="submit" class="btn btn-sm btn-outline dark" > &nbsp; Send &nbsp; </button>
                        <span class="form-group" >
                            <input type="file" name="uploadfile" id="uploadfile" class="form-control" style="display: none;" >
                          </span>
                        <div id="file_upload_show" class="margin-top-10"></div>
                        <div class="clearfix"></div>
                    </div>

                {!! Form::close() !!}
            </div>

        </div>

    </div>

</div>
<!-- /.modal -->
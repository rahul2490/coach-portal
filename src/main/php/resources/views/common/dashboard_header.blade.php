<div class="col-sm-12 text-right margin-btm-10">
    <a class="btn btn-link" href="{{ url(session('userRole').'/pastmeal/'.$patient_info->patient_id) }}" data-toggle=""> Past Meals <i class="fa fa-cutlery"></i> </a>
    @if(session('userRole')=="leadcoach")
        <a class="btn btn-link" href="{{ route('leadcoach.group-wall',['group_id'=> base64_encode($patient_info->group_id)] ) }}" data-toggle="" @if($patient_info->group_id==""){{ "disabled" }} @endif> <i class="fa fa-users"></i> Group Wall @if(!empty($total_post))<span class="badge badge-danger">  {{$total_post}} </span> @endif</a>
    @elseif(session('userRole')=="foodcoach")
        <a class="btn btn-link" href="{{ route('foodcoach.group-wall',['group_id'=> base64_encode($patient_info->group_id)] ) }}" data-toggle="" @if($patient_info->group_id==""){{ "disabled" }} @endif> Group Wall <i class="fa fa-users"></i> </a>
    @elseif(session('userRole')=="facilityadmin")
        <a class="btn btn-link" href="{{ route('facilityadmin.group-wall',['group_id'=>base64_encode($patient_info->group_id)] ) }}" data-toggle="" @if($patient_info->group_id==""){{ "disabled" }} @endif> Group Wall <i class="fa fa-users"></i> </a>
    @endif
    <a class="btn btn-link" disabled data-toggle=""> Video Call <i class="fa fa-video-camera"></i> </a>
    <a class="btn btn-link" href="{{ url(session('userRole').'/logbook/'.$patient_info->patient_id) }}" data-toggle=""> Log Book <i class="fa fa-file-text-o"></i> </a>
    <a class="btn btn-link" href="{{ url(session('userRole').'/setting/'.$patient_info->patient_id) }}" data-toggle=""> Settings <i class="fa fa-gear"></i> </a>
</div>
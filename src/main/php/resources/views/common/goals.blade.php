<!--BEGIN MODAL Add notes-->
<div class="modal fade in" id="modal_goals"  role="basic" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">All Goals </h4>
            </div>
            <div class="modal-body">
                <div class="clearfix"></div>
                <div class="portlet-body">
                    <div class="scroller" style="height: 70vh;">
                        {!! Form::open( ['method' => 'GET', 'id' => 'get_all_goals_of_patient', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('get-all-goals-of-patient')  ] ) !!}
                            <input type="hidden" id="patient_id" name="patient_id" value="{{ $patient_id }}">
                            <table class="table table-light" width="100%" >
                                <tbody class="remove_all_goals" role="alert" aria-live="polite" aria-relevant="all">
                                </tbody>
                            </table>
                        {!! Form::close() !!}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
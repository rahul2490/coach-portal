<div class="portlet light ">
    <div class="portlet-title">
        <div class="caption caption-md">
            <span class="caption-subject font-green-jungle  bold">Weight Summary</span>
        </div>
        <div class="actions">
            <div class="btn-group btn-group-devided" data-toggle="buttons">
                <label class="active btn btn-transparent btn-no-border blue-oleo btn-outline btn-circle btn-sm daily-switch">
                    <input type="radio" name="options" class="toggle " id="option2">Daily</label>
                <label class="btn btn-transparent btn-no-border blue-oleo btn-outline btn-circle btn-sm weekly-switch">
                    <input type="radio" name="options" class="toggle " id="option2">Weekly</label>
            </div>
        </div>
    </div>
    <div class="portlet-body daily">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                <a class="btn patient_weight_daily left"><i class="fa fa-angle-left font-green"></i></a>
                <a class="btn weight_daily_date">{{ $date_array['daily_start'] }} - {{ $date_array['daily_end'] }}</a>
                <a class="btn patient_weight_daily right"><i class="fa fa-angle-right font-green"></i></a>
            </div>

            <div class="col-md-12 list-separated">
                <div class="clearfix"></div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="font-grey-mint font-sm">Starting Wt.</div>
                    <div class=" font-lg font-red-flamingo"> {{ number_format($patient_info->starting_weight,1) }}
                        <span class="font-xs font-grey-mint">lbs</span>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="font-grey-mint font-sm">Goal Wt.</div>
                    <div class=" font-lg theme-font"> {{ number_format($patient_info->target_weight,1) }}
                        <span class="font-xs font-grey-mint">lbs</span>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="font-grey-mint font-sm">Current Wt.</div>
                    <div class=" font-lg theme-font"> {{ $current_weight }}
                        <span class="font-xs font-grey-mint">lbs</span>
                    </div>
                </div>
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <div class="font-grey-mint font-sm"> % Wt. Loss</div>
                    <div class=" font-lg theme-font"> {{ $weight_lose }}
                        <span class="font-xs font-grey-mint">%</span>
                    </div>
                </div>
            </div>



            <div class="col-sm-12 ">
                <span class="font-dark font-md title-main"> Past 7 days </span>
            </div>
            <div class="col-sm-5 list-separated">
                <div class="col-xs-12 "> <span class="font-md font-green-jungle title"> Logging Frequency</span></div>

                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="font-grey-mint font-sm">Goal</div>
                    <div class=" font-lg font-purple"> 7
                        <span class="font-xs font-grey-mint">days</span>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="font-grey-mint font-sm">Actual</div>
                    <div class=" font-lg font-blue-sharp"> <span class="actual_weight">{{ $patient_weight_daily['actual_weight'] }}</span>
                        <span class="font-xs font-grey-mint">days</span>
                    </div>
                </div>

            </div>

            <div class="col-sm-7 list-separated ">
                <div class="col-xs-12">  <span class="font-md font-green-jungle title"> Weight </span></div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <div class="font-grey-mint font-sm">Average</div>
                    <div class=" font-lg font-red-flamingo"><span class="average_weight">{{ $patient_weight_daily['average_weight'] }}</span>
                        <span class="font-xs font-grey-mint">lbs</span>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <div class="font-grey-mint font-sm">Lowest</div>
                    <div class=" font-lg theme-font"> <span class="lowest_weight">{{ $patient_weight_daily['lowest_weight'] }}</span>
                        <span class="font-xs font-grey-mint">lbs</span>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <div class="font-grey-mint font-sm"> Highest </div>
                    <div class=" font-lg font-purple"> <span class="highest_weight">{{ $patient_weight_daily['highest_weight'] }}</span>
                        <span class="font-xs font-grey-mint">lbs</span>
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>
        </div>
        <div id="weight_summary_daily" class="margin-top-20" style="height:175px;"></div>
    </div>
    <div class="clearfix"></div>


    <div id="portlet-weekly-weight" class="portlet-body weekly">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                <a class="btn weekly_weight_daily left">
                    <i class="fa fa-angle-left font-green"></i>
                </a>
                <a class="btn weekly_weight_daily_date">{{ $date_array['monthly_start'] }} - {{ $date_array['monthly_end'] }}</a>
                <a class="btn weekly_weight_daily right"><i class="fa fa-angle-right font-green"></i>
                </a>
            </div>
            <div class="col-md-12 list-separated">
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <div class="font-grey-mint font-sm">Starting Wt.</div>
                    <div class=" font-lg font-red-flamingo"> {{ number_format($patient_info->starting_weight,1) }}
                        <span class="font-xs font-grey-mint">lbs</span>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <div class="font-grey-mint font-sm">Goal Wt.</div>
                    <div class=" font-lg theme-font"> {{ number_format($patient_info->target_weight,1) }}
                        <span class="font-xs font-grey-mint">lbs</span>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <div class="font-grey-mint font-sm">Current Wt.</div>
                    <div class=" font-lg theme-font"> {{ $current_weight }}
                        <span class="font-xs font-grey-mint">lbs</span>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 ">
                <span class="font-dark font-md title-main"> Past 7 Weeks </span>
            </div>
            <div class="col-sm-5 list-separated">
                <div class="col-xs-12 "> <span class="font-md font-green-jungle title"> Logging Frequency</span></div>

                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="font-grey-mint font-sm">Goal</div>
                    <div class=" font-lg font-purple"> 49
                        <span class="font-xs font-grey-mint">days</span>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="font-grey-mint font-sm">Actual</div>
                    <div class=" font-lg font-blue-sharp"> <span class="weekly_actual_weight">{{ $patient_weight_weekly['weekly_actual_weight'] }}</span>
                        <span class="font-xs font-grey-mint">days</span>
                    </div>
                </div>

            </div>

            <div  class="col-sm-7 list-separated ">
                <div class="col-xs-12">  <span class="font-md font-green-jungle title"> Weight </span></div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <div class="font-grey-mint font-sm">Average</div>
                    <div class=" font-lg font-red-flamingo"> <span class="weekly_average_weight">{{ $patient_weight_weekly['weekly_average_weight'] }}</span>
                        <span class="font-xs font-grey-mint">lbs</span>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <div class="font-grey-mint font-sm">Lowest</div>
                    <div class=" font-lg theme-font"> <span class="weekly_lowest_weight">{{ $patient_weight_weekly['weekly_lowest_weight'] }}</span>
                        <span class="font-xs font-grey-mint">lbs</span>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-6">
                    <div class="font-grey-mint font-sm"> Highest </div>
                    <div class=" font-lg font-purple"> <span class="weekly_highest_weight">{{ $patient_weight_weekly['weekly_highest_weight'] }}</span>
                        <span class="font-xs font-grey-mint">lbs</span>
                    </div>
                </div>

            </div>

            <div class="clearfix"></div>
        </div>
        <div id="weight_summary_weekly" class="margin-top-20" style="height:175px;"></div>
    </div>
</div>

@if(isset($patient_weight_daily))
    <script type="application/javascript">
        var patient_weight_daily = [];
        try{
            patient_weight_daily = JSON.parse('{!! json_encode($patient_weight_daily['daily_weight_data']) !!}');
        }catch(e){}

        var week_day_name = [];
        try{
            week_day_name = JSON.parse('{!! json_encode($patient_weight_daily['week_day_name']) !!}');
        }catch(e){}
    </script>
@endif

@if(isset($patient_weight_weekly))
    <script type="application/javascript">
        var weekly_weight_data = [];
        try{
            weekly_weight_data = JSON.parse('{!! json_encode($patient_weight_weekly['weekly_weight_data']) !!}');
        }catch(e){}

        var weekly_day_name = [];
        try{
            weekly_day_name = JSON.parse('{!! json_encode($patient_weight_weekly['weekly_day_name']) !!}');
        }catch(e){}
    </script>
@endif
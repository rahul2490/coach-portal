<!--BEGIN MODAL Add notes-->
<div class="modal fade in" id="modal_view2donotes" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">To-do Notes</h4>
            </div>
            <div class="modal-body">
                <div class=""> <a data-dismiss="modal" aria-hidden="true" class="btn green-jungle btn-sm pull-right" href="#modal_add2donotes" data-toggle="modal">
                        Add To-do List  </a></div>
                <div class="clearfix"></div>
                <ul class="list-unstyled list-modal">
                    <li class="">
                        <p class="">
                            <strong>Daniel Wong </strong> <small class="font-grey-salsa pull-right lowercase">a few seconds ago</small>
                        </p>
                        <p><span class="font-grey-salsa">Post Call</span> - Lorem ipsum dolor sit amet diam nonummy nibh dolore </p>
                    </li>

                    <li class="">
                        <p class="">
                            <strong>Daniel Wong </strong> <small class="font-grey-salsa pull-right lowercase">a few seconds ago</small>
                        </p>
                        <p><span class="font-grey-salsa">Post Call</span> - Lorem ipsum dolor sit amet diam nonummy nibh dolore </p>
                    </li>

                </ul>

                <div class="clearfix"></div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
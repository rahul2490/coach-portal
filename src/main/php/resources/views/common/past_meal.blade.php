
@extends('include.admin-layout')

@section('page-title')
    Past Meal | Healthslate
@stop

@section('body-class')

@stop

@section('content')
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <!-- BEGIN PAGE CONTENT BODY -->
                    <div class="page-content">
                        <div class="container-fluid">
                            <!-- BEGIN PAGE BREADCRUMBS -->
                            <ul class="page-breadcrumb breadcrumb">
                                <li>
                                    <a href="{{route(session('userRole').'.home')}}">Home</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <a href="{{env("APP_BASE_URL").session('userRole').'/dashboard/'.$patient_info->patient_id}}">Dashboard</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                                <li>
                                    <span>Past Meal</span>
                                </li>
                            </ul>
                            <!-- END PAGE BREADCRUMBS -->
                            <!-- BEGIN PAGE CONTENT INNER -->
                            <div class="page-content-inner">
                                <div class="row">
                                    <div class="col-md-12">

                                        <!-- BEGIN PROFILE SIDEBAR -->
                                    @include('common.dashboard_profile_sidebar')
                                        <!-- END  PROFILE SIDEBAR -->
                                        <!-- BEGIN PROFILE CONTENT -->
                                        <div class="profile-content">
                                            <div class="row">
                                                @include('common.dashboard_header')

                                                <div class="col-md-12">

                                                    <!--section 1 group charts -->
                                                    <div class="portlet light ">
                                                        <div class="portlet-title ">
                                                            <div class="caption ">
                                                                <span class="caption-subject font-green-jungle bold ">Past Meals</span>
                                                            </div>
                                                            <div class="actions">
                                                                <div class="btn-group btn-group-devided" data-toggle="buttons">

                                                                    {{--<label class="active btn btn-transparent btn-no-border blue-oleo btn-outline btn-circle btn-sm">--}}
                                                                        {{--<input type="radio"  name="options" value="1" class="toggle past_week" id="option2">Week</label>--}}
                                                                    {{--<label class="btn btn-transparent btn-no-border blue-oleo btn-outline btn-circle btn-sm">--}}
                                                                        {{--<input type="radio"  name="options" value="2" class="toggle past_month" id="option2">Month</label>--}}
                                                                    <input type="hidden"  name="patient_id" id="patient_id" value="{{$patient_id}}">
                                                                </div>
                                                            </div>



                                                        </div>
                                                        <div class="portlet-body">
                                                            <div style="" class="col-md-4 col-sm-6 margin-btm-20">
                                                                <div id="" class="row "><label class="control-label col-sm-4">Filter by:</label>
                                                                    <div class="col-sm-8">
                                                                        <select id="ddFilterBy" class="form-control input-sm input-small input-inline">
                                                                            <option value="all">All</option>
                                                                            <option value="Breakfast">Breakfast</option>
                                                                            <option value="Lunch">Lunch</option>
                                                                            <option value="Snack">Snack</option>
                                                                            <option value="Dinner">Dinner</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div style="" class="col-md-4 col-sm-5"></div>
                                                            <div class="col-md-4 col-sm-6 text-right week_for_date_change portlet-body-for-date">
                                                                {{--  <a class="btn left date-filter">
                                                                      <i class="fa fa-angle-left font-green"></i>
                                                                  </a>
                                                                  <a class="btn date_range">10/26/2017 - 11/01/2017</a>
                                                                  <a  class="btn right date-filter">
                                                                      <i  class="fa fa-angle-right font-green"></i>
                                                                  </a>--}}

                                                                <a class="btn  week left"><i class="fa fa-angle-left font-green"></i></a>

                                                                <a class="btn week_date_range">{{ $date_array['week_start'] }} - {{ $date_array['week_end'] }}</a>

                                                                <a class="btn week right"><i class="fa fa-angle-right font-green"></i></a>


                                                            </div>

                                                            <div class="past-meal-log">
                                                                <div class="meal-log col-sm-12" id="all-meal-log">
                                                                    @if(!empty($past_meal))
                                                                    @foreach($past_meal as $meal)
                                                                    <div class="col-xs-6 col-sm-6 col-lg-3" id="meal_{{  $meal['log_id'] }}">

                                                                        <div class="btn btn-block btn-default btn-bg" >
                                                                            <img  src="{{ $meal['image_name'] }}" class="responsive" width="auto" />
                                                                        </div>
                                                                        <a class="badge badge-danger delete_meal" data-log-id="{{ $meal['log_id'] }}"> <i class="fa fa-trash"  aria-hidden="true"></i> </a>
                                                                        <div class="overlay" onclick="location.href='{{route('pastmeal/edit_past_meal',['logId' => $meal['log_id']])}}'" >

                                                                            <div class="log-info">
                                                                                <h5> <strong>{{ $meal['calories'] }}</strong> <small> {{  $meal['unit_from_food_master'] }} </small></h5>
                                                                                <!-- <h6> Cal </h6>  -->
                                                                                <h6> {{ $meal['meal_name'] }}  </h6>
                                                                            </div>
                                                                        </div>
                                                                        <p class="text-center font-dark"><span class=" sbold font-md"> {{ $meal['type'] }} </span>  <br/> <span class=" font-sm font-grey-salsa"><i class="fa fa-clock-o"></i>  {{ $meal['log_time'] }} </span></p>


                                                                    </div>
                                                                        @endforeach
                                                                    @else
                                                                        <div class="alert alert-warning margiv-top-10">
                                                                            <center><span>No data found</span></center>
                                                                        </div>

                                                                        @endif

                                                                </div>


                                                                <div class="clearfix"></div>

                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>








                                                </div>
                                            </div>
                                        </div>
                                        <!-- END PROFILE CONTENT -->
                                    </div>
                                </div>
                            </div>
                            <!-- END PAGE CONTENT INNER -->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT BODY -->
                    <!-- END CONTENT BODY -->
                </div>
                <!-- END CONTENT -->

            </div>
            <!-- END CONTAINER -->
@stop

@include('common.motivation')
@include('common.user_profile_image')


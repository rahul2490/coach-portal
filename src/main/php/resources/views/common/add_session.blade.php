<!--BEGIN MODAL Add notes-->
<div class="modal fade in" id="modal_addsession" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add 1:1 Session</h4>
            </div>
            <div class="modal-body">

                <form role="form" class="">
                    <div class="form-body">



                        <div class="form-group">
                            <label for="multiple" class="control-label"> Select Coaches
                            </label>
                            <div class="input-group select2-bootstrap-append">
                                <select id="ddCoachesList" class="form-control ddCoachesList" name="ddCoachesList" >
                                    <option value="53">Sanchez, Louis</option><option value="208">Patidar, Rajkumar</option><option value="228">salvani, AmitC</option><option value="220">Baxter, Stormie</option><option value="210">Plachko, Erin</option><option value="285">Neema, ShubhamC</option><option value="323">Fcoach, SheetalFAC food</option><option value="307">FA, sheetalFAC</option><option value="243">Martin, Cami</option><option value="231">N, sindhu</option><option value="52">M, Oz</option><option value="261">Tea, Jessica</option><option value="294">singh, ashish</option><option value="306">admin, sfacilty</option><option value="227">foodCoach, AmitF</option><option value="272">Mahajan, SheetalC</option><option value="130">V1Test, Dan</option><option value="277">Mahajan, SheetalF</option><option value="260">Allen, Lindsey</option><option value="305">Chouhan, Paresh</option><option value="275">CoachChouhan, CoachParesh</option><option value="214">Zajac, Jamie</option><option value="66">Riley, Pat</option><option value="325">Coach, S_HS_Dpp</option><option value="242">Schrider, Laurie</option><option value="211">Serpan, Tava</option><option value="262">Thompson, Vetina</option><option value="334">M1, test1</option><option value="213">Schmidt, Andrea</option><option value="292">neema, ShubhamF</option><option value="244">Willcox, Lisa</option><option value="215">Saul, Jared</option><option value="63">Moore, Meghann</option><option value="309">FA, sheetalFAC2</option>
                                </select>
                                <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button" data-select2-open="multiple">
                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                </button>
                                                        </span>
                            </div>
                            <!--  <div class="help-block">Note: Primary Food Coach <a class="sbold"> foodCoach, AmitF</a> will be automatically notified.</div> -->
                        </div>


                        <div class="form-group">
                            <label for="multiple" class="control-label"> Select Date and Time
                            </label>
                            <div class="input-group date form_datetime">
                                <input type="text" size="16" readonly class="form-control">
                                <span class="input-group-btn">
                                                                                <button class="btn default date-set" type="button">
                                                                                    <i class="fa fa-calendar"></i>
                                                                                </button>
                                                                            </span>
                            </div>
                        </div>


                        <div class="form-group col-md-12 text-right">
                            <button data-dismiss="modal" aria-hidden="true" type="button" class="btn btn-default"> &nbsp; Cancel &nbsp; </button>
                            <button type="button" class="btn green-jungle"> &nbsp; Add &nbsp; </button>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </form>

                <div class="clearfix"></div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
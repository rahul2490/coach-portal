<!--direct message modal-->

<!--modal Direct Messages coach Memeber -->
<div class="modal fade" id="direct-msg-coach"  role="basic" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Direct Messages</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <form action="#">
                        <div  class="col-sm-12" id="chats">
                            <div class="modal-form-title">
                                <div class="col-sm-6 caption font-green">
                                    <i class="fa fa-user font-green"></i>
                                    <span class="caption-subject bold uppercase">Brain S-1918</span><a class="label label-sm label-primary markread"> Mark as read <i class="fa fa-check"></i></a>
                                </div>
                                <div class="col-sm-6 form-horizontal">
                                    <a class="btn pull-right" data-toggle="tooltip" title="Open in New Window"><i class="fa fa-expand"></i></a>
                                    <div class="">
                                        <label class="col-md-4 control-label">Filter By</label>
                                        <div class="input-group select2-bootstrap-append">
                                            <select id="filter-messages" class="form-control " name="" >
                                                <option value="" selected="selected" disabled="">Select</option>
                                                <option value="1531">Salvani, AmitC</option>
                                                <option value="ALL">ALL</option>
                                            </select>
                                            <span class="input-group-btn">
                                                                <button class="btn btn-default" type="button" data-select2-open="filter-messages">
                                                                    <span class="glyphicon glyphicon-search"></span>
                                                                </button>
                                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-sm-4">

                                <div class="form-group">
                                    <label for="recom-messages" class="control-label"> Other Recommended Message
                                    </label>
                                    <div class="input-group select2-bootstrap-append">
                                        <select id="recom-messages" class="form-control recom-messages" name="recom-messages" >
                                            <option value="">Select Recommended Message</option>
                                            <option value="1">Let us go back to what was successful for you..</option>
                                            <option value="2">Just checking in to see how you’re doing..</option>
                                            <option value="3">You are in danger of being dis-enrolled..</option>
                                        </select>
                                        <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" data-select2-open="recom-messages">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </span>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="saved-messages" class="control-label"> My Saved Message
                                    </label>
                                    <div class="input-group select2-bootstrap-append">
                                        <select id="saved-messages" class="form-control saved-messages" name="saved-messages" >
                                            <option value="" >Select Saved Message</option>
                                            <option value="1">Let us go back to what was successful for you..</option>
                                            <option value="2">Just checking in to see how you’re doing..</option>
                                            <option value="3">You are in danger of being dis-enrolled..</option>
                                        </select>
                                        <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" data-select2-open="saved-messages">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="canned-messages" class="control-label"> Canned Message
                                    </label>
                                    <div class="input-group select2-bootstrap-append">
                                        <select id="canned-messages" class="form-control canned-messages" name="canned-messages" >
                                            <option value="">Select Canned Message</option>
                                            <option value="1">Let us go back to what was successful for you..</option>
                                            <option value="2">Just checking in to see how you’re doing..</option>
                                            <option value="3">You are in danger of being dis-enrolled..</option>
                                        </select>
                                        <span class="input-group-btn">
                                                <button class="btn btn-default" type="button" data-select2-open="canned-messages">
                                                    <span class="glyphicon glyphicon-search"></span>
                                                </button>
                                            </span>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <p class="text-center">OR</p>
                                </div>
                                <div class="form-group text-center">
                                    <a class="btn btn-sm green-jungle btn-new-msg btn-block"> &nbsp; Create New Message &nbsp; </a>
                                </div>
                            </div>
                            <div class="col-sm-8 border-left">

                                <div class="scroller" id="direct-message-scroll" style="height: 280px;">

                                    <ul class="chats">
                                        <!--       <li class="out">
                                                <img class="avatar" alt="" src="../assets/layouts/layout3/img/avatar2.jpg" />
                                                <div class="message no-bg">
                                                    <textarea class="recomend-text form-control todo-taskbody-taskdesc" name="recomend-text-message" rows="4">You are in danger of being dis-enrolled..</textarea>
                                                      <button type="button" class=" btn btn-sm btn-outline dark margin-t-b-10"> <i class="fa fa-paperclip" aria-hidden="true"></i>  Attachment &nbsp; </button>
                                                      <button type="button" class=" btn btn-sm btn-outline dark margin-t-b-10"> &nbsp; Send &nbsp; </button>
                                                      <button type="button" class=" btn btn-sm btn-outline dark margin-t-b-10"> &nbsp; Save and Send &nbsp; </button>
                                                </div>

                                            </li> -->

                                        <li class="out">
                                            <img class="avatar" alt="" src="../assets/layouts/layout3/img/avatar2.jpg" />
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Lisa Wong </a>
                                                <span class="datetime"> at 20:11 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>
                                                <span class="body"><img class="img-attachment" src="../assets/pages/media/pages/logo_azteca.jpg" alt="Screen Shot 2017-07-12 at 10.52.40 AM"></span> </div>
                                        </li>
                                        <li class="in">
                                            <img class="avatar" alt="" src="../assets/layouts/layout3/img/avatar1.jpg" />
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Bob Nilson </a>
                                                <span class="datetime"> at 20:20 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>

                                            </div>
                                        </li>

                                        <li class="out">
                                            <img class="avatar" alt="" src="../assets/layouts/layout3/img/avatar2.jpg" />
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Lisa Wong </a>
                                                <span class="datetime"> at 20:33 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>

                                            </div>
                                        </li>

                                        <li class="in">
                                            <img class="avatar" alt="" src="../assets/layouts/layout3/img/avatar1.jpg" />
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Bob Nilson</a>
                                                <span class="datetime"> at 20:35 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>

                                            </div>
                                        </li>
                                        <li class="out">
                                            <img class="avatar" alt="" src="../assets/layouts/layout3/img/avatar2.jpg" />
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Lisa Wong  </a>
                                                <span class="datetime"> at 20:40 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. sed diam nonummy nibh euismod tincidunt ut laoreet.
                                                                </span>

                                            </div>
                                        </li>

                                        <li class="in">
                                            <img class="avatar" alt="" src="../assets/layouts/layout3/img/avatar1.jpg" />
                                            <div class="message">
                                                <span class="arrow"> </span>
                                                <a href="javascript:;" class="name"> Bob Nilson </a>
                                                <span class="datetime"> at 20:55 </span>
                                                <span class="body"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </span>

                                            </div>
                                        </li>

                                    </ul>
                                </div>
                                <ul class="chats margin-top-10">
                                    <li class="out">
                                        <img class="avatar" alt="" src="../assets/layouts/layout3/img/avatar2.jpg" />
                                        <div class="message no-bg">
                                            <textarea class="recomend-text form-control todo-taskbody-taskdesc" name="recomend-text-message" rows="2">You are in danger of being dis-enrolled..</textarea>
                                            <button type="button" class=" btn btn-sm btn-outline dark margin-t-b-10"> <i class="fa fa-paperclip" aria-hidden="true"></i>  Attachment &nbsp; </button>
                                            <button type="button" class=" btn btn-sm btn-outline dark margin-t-b-10"> &nbsp; Send &nbsp; </button>
                                            <button type="button" class=" btn btn-sm btn-outline dark margin-t-b-10"> &nbsp; Save and Send &nbsp; </button>
                                        </div>

                                    </li>
                                </ul>



                            </div>
                            <div class="clearfix"></div>
                        </div>


                    </form>

                </div>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@section('header-styles')
    @parent
    @if (isset($pre_css))
        @foreach ($pre_css as $c)
            <link href="{{ asset(elixir('css/'.$c.'.css')) }}"  rel="stylesheet" type="text/css">
        @endforeach
    @endif
    <link href="{{ asset(elixir('css/application.css')) }}"  rel="stylesheet" type="text/css">
    @if (isset($css))
        @foreach ($css as $c)
            <link href="{{ asset(elixir('css/'.$c.'.css')) }}"  rel="stylesheet" type="text/css">
        @endforeach
    @endif
@stop

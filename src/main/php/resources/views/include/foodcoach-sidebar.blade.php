@section('foodcoach-navbar')

    <div class="hor-menu  ">
        <ul class="nav navbar-nav">
            <li @if(Route::getCurrentRoute()->getPath() == 'foodcoach/group' ) class="active" @endif>
                <a href="{{route('foodcoach.group')}}"> My Groups
                </a>
            </li>
            <li @if(Route::getCurrentRoute()->getPath() == 'foodcoach/members/{group_id?}') class="active" @endif>
                <a href="{{route('foodcoach.members')}}"> My Members</a>
            </li>
            <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                <a href="javascript:void(0);"> Member Recruitment
                    <span class="arrow"></span>
                </a>
                <ul class="dropdown-menu pull-left">
                    <li aria-haspopup="true" class=" active">
                        <a href="{{ url('foodcoach/addmember')}}" class="nav-link  active">
                            Add Member
                        </a>
                    </li>
                    <li aria-haspopup="true" class=" active">
                        <a href="{{ url('foodcoach/prospectivemember') }}" class="nav-link  active">
                            Prospective Members
                        </a>
                    </li>
                </ul>
            </li>
            <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                <a href="javascript:void(0);"> Help
                    <span class="arrow"></span>
                </a>
                <ul class="dropdown-menu pull-left">
                    <li aria-haspopup="true" class=" active">
                        <a href="{{ url() }}/assets/help/help.pdf" class="nav-link  active" target="_blank">
                            Help Guide
                        </a>
                    </li>
                    <li aria-haspopup="true" class=" active">
                        <a href="{{ url('foodcoach/portaltrainingwebinar') }}" class="nav-link  active">
                            Portal Training Webinar
                        </a>
                    </li>
                    <li aria-haspopup="true" class=" active">
                        <a href="{{ url('foodcoach/apphelpvideoios') }}" class="nav-link  active">
                            App Help Videos (IOS)
                        </a>
                    </li>
                    <li aria-haspopup="true" class=" active">
                        <a href="{{ url('foodcoach/apphelpvideoandroid') }}" class="nav-link  active">
                            App Help Videos (Android)
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>

@stop
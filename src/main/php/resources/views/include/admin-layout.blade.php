@include('include.footer-scripts')
@include('include.header-styles')
@include('include.header')

<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.7
Version: 4.7.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>

    <meta charset="utf-8" />
    <title>@yield('page-title','Home') | {{ config('app.app_name') }} </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="HealthSlate Portal" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />

    @yield('header-styles')
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="{{ env('APP_BASE_URL') }}/favicon.ico" /> </head>
<!-- END HEAD -->

<body class="page-container-bg-solid @yield('body-class')">

<div class="page-wrapper">
    <!-- BEGIN HEADER -->
    @yield('header')
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->

    <div class="page-wrapper-row full-height">
        <div class="page-wrapper-middle">
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE BAR -->
                <!-- Main content -->

                @yield('content')

            <!-- /.content -->
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->

    <div class="page-wrapper-row">
        <div class="page-wrapper-bottom">
            <!-- BEGIN FOOTER -->
            <!-- BEGIN INNER FOOTER -->
            <div class="page-footer">
                <div class="container"> {{date('Y')}} &copy; All rights reserved</div>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
            <!-- END INNER FOOTER -->
            <!-- END FOOTER -->
        </div>
    </div>
</div>

<p>
    <a href="javascript:;" class="btn btn-outline sbold red hide" id="loding_icon"></a>
</p>

@include('common.change-password')

<input type="hidden" id="logout_url" value="{{ route('logout') }}">
@include('auto_logout')


<script>
    var DATE_DIFFERENCE;
    var APP_BASE_URL    = '{{ siteURL() }}';
    var USER_ROLE       = '{{ session('userRole') }}';
    var USER_ID         = '{{ session('userId') }}';
</script>

@yield('footer-scripts')

<!-- END THEME LAYOUT SCRIPTS -->
</body>
</html>
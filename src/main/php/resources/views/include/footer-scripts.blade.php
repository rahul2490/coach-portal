@section('footer-scripts')
    @parent
    <script src="{{ asset(elixir('js/application.js')) }}" ></script>
    @if (isset($js))
        @foreach ($js as $n)
            <script src="{{ asset(elixir('js/'.$n.'.js')) }}" ></script>
        @endforeach
    @endif

    @if(isset($common_js))
        @foreach ($common_js as $n)
            <script src="{{ asset(('js/'.$n.'.js')) }}" ></script>
        @endforeach
    @endif
    @if(isset($highchart_js))
        @foreach ($highchart_js as $n)
            <script src="{{ asset(('global/plugins/highcharts/js/'.$n.'.js')) }}" ></script>
        @endforeach
    @endif
@stop



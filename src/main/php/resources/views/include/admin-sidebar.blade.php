@section('admin-navbar')

    <div class="hor-menu  ">
        <ul class="nav navbar-nav">
            <li @if(Route::getCurrentRoute()->getPath() == 'facilityadmin/facility') class="active" @endif >
                <a href="{{route('admin.facility')}}"> Facility</a>
            </li>
            <li @if(Route::getCurrentRoute()->getPath() == 'admin/coaches') class="active" @endif>
                <a href="{{route('admin.coaches')}}"> Coaches</a>
            </li>
            <li @if(Route::getCurrentRoute()->getPath() == 'admin/members/{group_id?}' ) class="active" @endif>
                <a href="{{route('admin.members')}}"> Members Data</a>
            </li>
            <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                <a href="javascript:void(0);"> Member Recruitment
                    <span class="arrow"></span>
                </a>
                <ul class="dropdown-menu pull-left">
                    <li aria-haspopup="true" class=" active">
                        <a href="{{ url('admin/addmember')}}" class="nav-link  active">
                            Add Member
                        </a>
                    </li>
                    <li aria-haspopup="true" class=" active">
                        <a href="{{ url('admin/prospectivemember') }}" class="nav-link  active">
                            Prospective Members
                        </a>
                    </li>
                    <li aria-haspopup="true" class=" active">
                        <a href="/hs-super-admin/patient/import" class="nav-link  active">
                            Bulk Patient Import
                        </a>
                    </li>
                </ul>
            </li>
            <li aria-haspopup="true" class="menu-dropdown classic-menu-dropdown ">
                <a href="javascript:void(0);"> Help
                    <span class="arrow"></span>
                </a>
                <ul class="dropdown-menu pull-left">
                    <li aria-haspopup="true" class=" active">
                        <a href="{{ url() }}/assets/help/help.pdf" class="nav-link  active" target="_blank">
                            Help Guide
                        </a>
                    </li>
                    <li aria-haspopup="true" class=" active">
                        <a href="{{ url('admin/portaltrainingwebinar') }}" class="nav-link  active">
                            Portal Training Webinar
                        </a>
                    </li>
                    <li aria-haspopup="true" class=" active">
                        <a href="{{ url('admin/apphelpvideoios') }}" class="nav-link  active">
                            App Help Videos (IOS)
                        </a>
                    </li>
                    <li aria-haspopup="true" class=" active">
                        <a href="{{ url('admin/apphelpvideoandroid') }}" class="nav-link  active">
                            App Help Videos (Android)
                        </a>
                    </li>
                </ul>
            </li>

        </ul>
    </div>

@stop

@section('header')

    <div class="page-wrapper-row">
        <div class="page-wrapper-top">
            <!-- BEGIN HEADER -->
            <div class="page-header">
                <!-- BEGIN HEADER TOP -->
                <div class="page-header-top">
                    <div class="container-fluid">
                        <!-- BEGIN LOGO -->
                        <div class="page-logo">
                            <a href="{{route(session('userRole') . '.home')}}">
                                <?php $cobrand_id = Session::get('user')->cobrandId;?>
                                @if($cobrand_id==1)
                                <img src="{{ asset('img/logo-default.png') }}" alt="logo" class="logo-default">
                                    @else
                                    <img src="{{ asset('img/solera-logo-default.png') }}" alt="logo" class="logo-default">
                                @endif
                            </a>
                        </div>
                        <!-- END LOGO -->
                        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                        <a href="javascript:;" class="menu-toggler"></a>
                        <!-- END RESPONSIVE MENU TOGGLER -->
                        <!-- BEGIN TOP NAVIGATION MENU -->
                        <div class="top-menu">
                            <ul class="nav navbar-nav pull-right">
                                <!-- BEGIN NOTIFICATION DROPDOWN -->
                                <!-- DOC: Apply "dropdown-hoverable" class after "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                                <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->

                                <li class="dropdown">
                                    @foreach(@session('facility') as $key => $meal)
                                        @if(@session('active_facility') == $meal->facility_id)
                                            <a href="javascript:void(0)" class="btn green-jungle dropdown-toggle facility_dd" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                <span> {{$meal->name}} </span><i class="fa fa-angle-down"></i>
                                            </a>
                                        @endif
                                    @endforeach

                                    <ul class="dropdown-menu dropdown-menu-default">
                                        @foreach(@session('facility') as $key => $meal)
                                            @if(@session('active_facility') != $meal->facility_id)
                                                <li>
                                                    <a href="javascript:void(0)" class="facilityDropdown" data-facility="{{$meal->facility_id}}" data-action="{{ route(session('userRole') . '.facilityChange') }}"> {{ $meal->name}} </a>
                                                </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </li>

                                <!-- END NOTIFICATION DROPDOWN -->
                                @if(!empty(@session('facility')))
                                <li class="droddown dropdown-separator">
                                    <span class="separator"></span>
                                </li>
                                @endif

                                <!-- BEGIN USER LOGIN DROPDOWN -->
                                <li class="dropdown dropdown-user dropdown-dark">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <img alt="" class="img-circle" src="{{ asset('img/default-user.png') }}">
                                        <span class="username username-hide-mobile">{{ session('user')->firstName }} {{ session('user')->lastName }}</span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-default">
                                        <li>
                                            <a href="{{ route('logout') }}">
                                                <i class="icon-lock"></i> Log Out </a>
                                        </li>
                                        <li>
                                            <a id="change_password_modal" data-target="#change_password" data-toggle="modal">
                                                <i class="icon-key"></i> Change Password </a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- END USER LOGIN DROPDOWN -->
                                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                                <li class="dropdown dropdown-extended quick-sidebar-toggler">
                                    <span class="sr-only"></span>
                                        <i class="icon-logout" data-action="{{ route('logout') }}"></i>
                                </li>
                                <!-- END QUICK SIDEBAR TOGGLER -->
                            </ul>
                        </div>
                        <!-- END TOP NAVIGATION MENU -->
                    </div>
                </div>
                <!-- END HEADER TOP -->
                <!-- BEGIN HEADER MENU -->
                <div class="page-header-menu">
                    <div class="container-fluid">
                        <!-- BEGIN MEGA MENU -->
                        <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                        <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                        @if(session('userRole') == 'admin')
                            @include('include.admin-sidebar')
                            @yield('admin-navbar')
                        @elseif(session('userRole') == 'facilityadmin')
                            @include('include.facilityadmin-sidebar')
                            @yield('facilityadmin-navbar')
                        @elseif(session('userRole') == 'leadcoach')
                            @include('include.leadcoach-sidebar')
                            @yield('leadcoach-navbar')
                        @elseif(session('userRole') == 'foodcoach')
                            @include('include.foodcoach-sidebar')
                            @yield('foodcoach-navbar')
                        @elseif(session('userRole') == 'techsupport')
                            @include('include.techsupport-sidebar')
                            @yield('techsupport-navbar')
                        @else
                            
                        @endif

                        <!-- END MEGA MENU -->
                    </div>
                </div>
                <!-- END HEADER MENU -->
            </div>
            <!-- END HEADER -->
        </div>
    </div>









{{--
    <!-- Main Header -->

    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('layouts/layout/img/logo-invert.png') }}" alt="logo" class="logo-default" /> </a>
                <div class="menu-toggler sidebar-toggler">
                    <span></span>
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                <span></span>
            </a>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- END TODO DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="{{ asset('img/default-user.png') }}" />
                            <span class="username username-hide-on-mobile"> {{ session('user')->firstName }} {{ session('user')->lastName }} </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="{{ route('logout') }}">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
    </div>--}}
@stop
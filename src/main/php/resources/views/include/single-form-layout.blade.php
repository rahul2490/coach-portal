@include('include.header-styles')
@include('include.footer-scripts')

<!DOCTYPE html>
    <html lang="en">
    <head>
        <title>{{{ $page_title or 'Home Page' }}} | {{ config('app.app_name') }} </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="HealthSlate Portal" name="description" />
        <meta content="" name="author" />

        @yield('header-styles')
    </head>
    <body class="page-404-full-page @yield('body-class')">

        @yield('content')

        @yield('footer-scripts')
    </body>
    </html>

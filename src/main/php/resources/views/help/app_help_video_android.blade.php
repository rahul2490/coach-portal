@extends('include.admin-layout')

@section('page-title')
App Help Videos (Android)
@stop

@section('body-class')
app_help_video_android

@stop

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->

        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route(session('userRole') . '.home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>App Help Videos Android </span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-blue-ebonyclay">
                                            <i class="icon-settings font-blue-ebonyclay"></i>
                                            <span class="caption-subject bold uppercase">App Help Videos (Android) </span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">

                                        <!-- app help video android start here-->

                                        <div id="container-fluid" class=""/>

                                        <!-- Page Content -->
                                        <div class="" style="">
                                            <!-- <div class="title"  > <h2 class="clr-green"> Help Videos 3/17 </h2></div>-->
                                            <div class="session-overview-list">
                                                <ul class="video-data-android">

                                                </ul>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div><!-- /.container -->


                                    </div>

                                    <style>
                                        .img-responsive
                                        {
                                            max-width:none !important;
                                        }
                                        .list-group-item{
                                            border:none!important;
                                            margin-bottom: 10px;
                                        }
                                        .list-group-item .grey-item{
                                            border:1px solid #ddd;
                                            padding: 20px;
                                            cursor:pointer;
                                        }
                                    </style>

                                        <!-- app help video android end here-->

                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->




@stop

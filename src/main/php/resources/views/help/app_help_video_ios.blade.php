@extends('include.admin-layout')

@section('page-title')
    App Help Videos (IOS)
@stop

@section('body-class')
    app_help_video_ios
@stop

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->

        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route(session('userRole') . '.home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>App Help Videos (IOS)</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-blue-ebonyclay">
                                            <i class="icon-settings font-blue-ebonyclay"></i>
                                            <span class="caption-subject bold uppercase">App Help Videos (IOS)</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                     <!-- video list code -->
                                        <div class="" style="">
                                        <!-- <div class="title"  > <h2 class="clr-green"> Help Videos 3/17 </h2></div> -->
                                        <div class="session-overview-list">
                                            <ul class="video-data">

                                                {{--<li class="list-group-item" data-videourl="l9whlf8srw">
                                                    <div class="grey-item" href="">
                                                        <div class="icon-div">
                                                            <img class="img-responsive" src="{{url()}}/assets/help/ios_image/1d73f1e83f856fef8f91110bf9006ce3772b624c.jpg" alt="">
                                                        </div>
                                                        <div class="content-div">
                                                            <h2>Intro - iOS  </h2><p class="view-status">  <strong>Video: 2:20 </strong>/ Created On May 9, 2017 at 1:34am</p>
                                                        </div>
                                                        <div class="clearfix">

                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="list-group-item" data-videourl="t59ww12f9q">
                                                    <div class="grey-item" href="">
                                                        <div class="icon-div">
                                                            <img class="img-responsive" src="{{url()}}/assets/help/ios_image/4b0b432c40ca57d2c9794ff5ccd0875abf2f09d6.jpg" alt="">   </div>
                                                        <div class="content-div">
                                                            <h2>Weighing Yourself - iOS  </h2><p class="view-status">  <strong>Video: 0:26 </strong>/ Created On Jun 1, 2017 at 9:30pm</p>
                                                        </div>
                                                        <div class="clearfix">

                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="list-group-item" data-videourl="xjgniv06y6"><div class="grey-item" href="">
                                                        <div class="icon-div"> <img class="img-responsive" src="{{url()}}/assets/help/ios_image/703bcb714f89019f01e6541cd66b12521b07baf2.jpg" alt="">
                                                        </div>
                                                        <div class="content-div">
                                                            <h2>Food Logging - iOS  </h2><p class="view-status">  <strong>Video: 2:13 </strong>/ Created On Jun 2, 2017 at 12:50am</p>
                                                        </div>
                                                        <div class="clearfix">

                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="list-group-item" data-videourl="7n3uftfvbs">
                                                    <div class="grey-item" href="">
                                                        <div class="icon-div"> <img class="img-responsive" src="{{url()}}/assets/help/ios_image/3390af721214e68ff76133e6a9881e1ba7a965d2.jpg" alt="">
                                                        </div>
                                                        <div class="content-div">
                                                            <h2>Installing your Fitbit - iOS  </h2>
                                                            <p class="view-status">  <strong>Video: 1:20 </strong>/ Created On July 18, 2017 at 3:15pm</p>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </li>
                                                <li class="list-group-item" data-videourl="fjccqxpzk8">
                                                    <div class="grey-item" href="">
                                                        <div class="icon-div">
                                                            <img class="img-responsive" src="{{url()}}/assets/help/ios_image/03b364cacc7c63f6e4f0a7725efb2bc0b1ed5d13.jpg" alt="">
                                                        </div>
                                                        <div class="content-div"><h2>Creating My Motivation Image - iOS  </h2><p class="view-status">  <strong>Video: 0:28 </strong>/ Created On July 18, 2017 at 4:15pm</p>
                                                        </div>
                                                        <div class="clearfix">

                                                        </div>
                                                    </div>
                                                </li>--}}
                                            </ul>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div><!-- /.container -->
                                        <style>
                                            .img-responsive
                                            {
                                                max-width:none !important;
                                            }
                                            .list-group-item{
                                                border:none!important;
                                                margin-bottom: 10px;
                                            }
                                            .list-group-item .grey-item{
                                                border:1px solid #ddd;
                                                padding: 20px;
                                                cursor:pointer;
                                            }
                                        </style>



                                     <!--video list code end -->


                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop

@extends('include.admin-layout')

@section('page-title')
    Portal Training Webinar
@stop

@section('body-class')

@stop

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->

        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route(session('userRole') . '.home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Portal Training Webinar</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-blue-ebonyclay">
                                            <i class="icon-settings font-blue-ebonyclay"></i>
                                            <span class="caption-subject bold uppercase">Portal Training Webinar</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                     <!-- video player code start -->
                                        <div class="medium-12 large-12 twelve columns">
                                            <h3 id="hVideoTitle" class="meal-summary-text margin-top-10"></h3>
                                            <div class="video-player">

                                                <div class="video_container col-sm-8 col-sm-offset-2">
                                                    <script src="https://fast.wistia.com/embed/medias/4okuc2gp0l.jsonp" async></script>
                                                    <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>
                                                    <div class="wistia_responsive_padding" >
                                                        <div class="wistia_responsive_wrapper" >
                                                            <div class="wistia_embed wistia_async_w752pxmttf videoFoam=true" style="height:100%;width:100%">&nbsp;</div>
                                                        </div>
                                                    </div>
                                                    <!-- END PAGE CONTENT INNER -->
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <!-- END PAGE CONTENT BODY -->
                                            <!-- END CONTENT BODY -->
                                        </div>



                                     <!-- video player code end -->
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop

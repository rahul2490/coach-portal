@extends('include.admin-layout')

@section('page-title')
    App Help Videos (Android)
@stop

@section('body-class')
    app_help_video_android

@stop

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->

        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route(session('userRole') . '.home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('apphelpvideoandroid')}}">App Help Videos (Android)</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Android</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-blue-ebonyclay">
                                            <i class="icon-settings font-blue-ebonyclay"></i>
                                            <span class="caption-subject bold uppercase">App Help Videos (Android) </span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">

                                       <div id="container-fluid" class=""/>
                                        <!-- Navigation -->
                                        <nav class="navbar navbar-fixed-top" role="navigation">
                                            <!-- Brand and toggle get grouped for better mobile display -->

                                            <div class="navbar-header">

                                                <!-- <a class="navbar-brand " href=""> <img src="https://hstage.healthslate.com/resources/css/images/logowhite.png" alt="" >
                                                </a> -->

                                                <div class="clearfix"></div>
                                            </div>
                                            <!-- /.container -->
                                        </nav>
                                        <!-- Page Content -->
                                        <div class="" style="padding-top:50px;">
                                            <div class="title"  > <h2 class="clr-green">  Food Logging IOS </h2></div>
                                            <div class="session-details-list">
                                                <div class="col-sm-4 col-sm-offset-4">
                                                    <div class="video_container ">
                                                        <script src="https://fast.wistia.com/embed/medias/4okuc2gp0l.jsonp" async></script>
                                                        <script src="https://fast.wistia.com/assets/external/E-v1.js" async></script>
                                                        <div class="wistia_responsive_padding" style="">
                                                            <div class="" style="">
                                                                <div class="wistia_embed videoFoam=true" style="height:100%;width:auto">&nbsp;
                                                                </div></div></div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div><!-- /.container -->


                                    </div>


                                    </div>


                                    <!--end code for video list -->


                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>

                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
    <!-- END PAGE CONTENT BODY -->
    <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->

@stop

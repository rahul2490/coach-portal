@extends('include.admin-layout')

@section('page-title')
    My Groups
@stop

@section('body-class')

@stop

@section('content')

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container-fluid">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{route(session('userRole').'.home')}}">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>My Groups</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="mt-content-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption font-green">
                                                <i class="fa fa-users font-green"></i>
                                                <span class="caption-subject bold uppercase">My Groups</span>
                                            </div>
                                            <div class="actions">
                                                <a href="#post-on-group" data-toggle="modal"  data-backdrop="static" data-keyboard="false"  class="btn green-jungle ">
                                                    <i class="fa fa-plus"></i> Create New Post For Groups </a>
                                            </div>
                                        </div>

                                        <div class="portlet-body">
                                            <div style="" class="col-md-3 col-sm-3 custom-filter-sort">
                                                <div id="" class="dataTables_filter">
                                                    <label>Group Type:
                                                        <select class="form-control input-sm input-small  input-inline" id="filterGroup">
                                                            <option value="all">All Groups</option>
                                                            <option value="inperson">In Person</option>
                                                            <option value="online">Online</option>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div style="" class="col-md-3 col-sm-3 custom-filter-sort">
                                                <div id="" class="dataTables_filter">
                                                    <label>Group Capacity:
                                                        <select class="form-control input-sm input-small  input-inline" id="filterCapacity">
                                                            <option value="all">All</option>
                                                            <option value="notfull">Available (Not Full)</option>
                                                            <option value="full">Full</option>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>

                                            <table class="table table-bordered dt-responsive member-table " width="100%" id="group_list" data-action="{{ route('leadcoach.group-list') }}">
                                                <thead>
                                                    <tr>
                                                        <th ></th>
                                                        <th width="30%">Group Name</th>
                                                        <th width="20%">Coaches</th>
                                                        <th width="5%"  data-orderable='false'>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

        </div>

        <!--modal message Group message -->
        <div class="modal fade" id="post-on-group"  role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">New Post For Groups</h4>
                    </div>
                    <div class="modal-body">
                        <div class="modal-form-title">
                            {{--<div class="col-sm-12 caption font-green">--}}
                                {{--<i class="fa fa-users font-green"></i>--}}
                                {{--<span class="caption-subject bold uppercase message_group_member_group_name"></span>--}}
                            {{--</div>--}}
                            {{-- <div class="col-sm-6 form-horizontal pull-right text-right">
                                 <strong> 2</strong> at Milestone Risk </br>
                                 <strong> 0</strong> Missing Staring Weight
                             </div>--}}
                            <div class="clearfix"></div>
                        </div>

                    {!! Form::open( ['method' => 'POST', 'id' => 'post_group_form', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('leadcoach.group-post')  ] ) !!}
                    <!-- <div class="form-group pull-right text-right">
                        <a class="btn btn-sm green-jungle btn-new-msg"> &nbsp; Create New Message &nbsp; </a>
                    </div>
                    <div class="clearfix"></div>   -->


                        <div class="form-group">

                            <div class="mt-checkbox-inline">
                                <label class="mt-checkbox">
                                    <input class="field" id="all_groups"   name="group_names[]" type="checkbox" value="all">  Select All Groups
                                    <span></span>
                                </label>
                            </div>

                            <label for="group-members" class="control-label"> Select Group
                            </label>
                            <div class="input-group select2-bootstrap-append">
                                <select id="group-names" class="form-control group-names" name="group_names[]" multiple >
                                @foreach($group_list as $list)
                                        <option value="{{$list->group_id}}">{{$list->name}}</option>
                                @endforeach
                                </select>
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" data-select2-open="group-members">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Message</label>
                            <textarea class="recomend-text form-control" id="message" name="message" rows="3"></textarea>
                        </div>
                        <div class="form-group text-right">
                            <button type="button" id="attachment_post" class=" btn btn-sm btn-outline dark "> <i class="fa fa-paperclip" aria-hidden="true"></i>  Attachment &nbsp; </button>
                            <button type="submit" class="btn btn-sm btn-outline dark" > &nbsp; Send &nbsp; </button>
                            <span class="form-group" >
                            <input type="file" name="uploadfile" id="uploadfile_post" class="form-control uploadfile" style="display: none;" >
                          </span>
                            <div id="file_upload_show_post" class="margin-top-10"></div>
                            <div class="clearfix"></div>
                        </div>

                        {!! Form::close() !!}
                    </div>

                </div>

            </div>

        </div>
        <!-- /.modal -->

        @include('common.message_group_member')

@stop

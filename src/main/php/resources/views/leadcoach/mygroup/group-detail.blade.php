@extends('include.admin-layout')

@section('page-title')
    My Groups
@stop

@section('body-class')

@stop

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route(session('userRole').'.home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ route('leadcoach.group') }}"> My Groups</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span> Class</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-green">
                                            <i class="icon-settings font-green"></i>
                                            <span class="caption-subject bold uppercase">{{ $group_detail->name }} | {{ $group_detail->group_id }}</span>
                                        </div>
                                        <div class="actions">

                                            <a class="btn green-jungle" href="{{ route('leadcoach.group-wall', ['group_id' => base64_encode($group_detail->group_id)]) }}" data-toggle="">
                                                <i class="fa fa-users"></i> Group Wall
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row margin-btm-20">
                                            <div class="col-md-3 col-xs-6 margin-top-10">
                                                <div class=""> <i class="fa fa-user"></i> {{ $coach_detail->full_name }} <small class="font-grey-salsa">Lifestyle Coach</small>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-6 margin-top-10">

                                                <div class=""><i class="iconhs-weight" data-toggle="tooltip" title="Weight Loss By Group"></i> @if(!empty($member_group_detail)) {{ sprintf("%4.1f", $member_group_detail[0]['total_weight_loss'] )  }} @else {{ "0" }} @endif lbs
                                                    <span class="ont-grey-mint">@if(!empty($member_group_detail) && $member_group_detail[0]['goal_completion_percentage']!="Group target not set") {{ $member_group_detail[0]['goal_completion_percentage'] }} @else {{ "0" }} @endif % <span class="glyphicon glyphicon-circle-arrow-down font-green-jungle"></span></span>
                                                </div>
                                            </div>
                                        </div>


                                        <table class="table table-bordered dt-responsive table_group-session" width="100%" id="table-my-group-detail" >
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th width="15%">Id/Name</th>
                                                    <th>Weight</th>
                                                    <th>Activity</th>
                                                    <th>Logging</th>
                                                    <th>Session</th>
                                                </tr>
                                            </thead>

                                            <tbody role="alert" aria-live="polite" aria-relevant="all">

                                            @if(!empty($member_group_detail))
                                                @foreach($member_group_detail as $value)


                                                    <tr class="">
                                                        <th></th>
                                                        <td>
                                                            <div class="col-sm-8"><span class="memberid"> {{ $value['userTypeId'] }} </span></div>
                                                            <div class="col-sm-4">
                                                                <img class="user-pic rounded" src="{{ $value['patient_profile_image'] }}" width="45" height="45">
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <a href="{{ url(session('userRole') . '/dashboard') .'/'. $value['userTypeId'] }}" class="member-name sbold">{{ $value['full_name'] }}</a>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <p class="">This session @if(isset($value['thisWeekWeight'])) <span class="pull-right sbold font-dark">{{ sprintf("%4.1f", $value['thisWeekWeight']) }} <small> lbs </small> </span> @endif</p>
                                                            <p>Prior weight @if(isset($value['priorWeight']))<span class="pull-right sbold">{{ sprintf("%4.1f",$value['priorWeight']) }} <small>lbs</small></span>@endif </p>
                                                            <p>Change since prior <span class="pull-right sbold"> @if(isset($value['changeSincePriorWeight']))  @if($value['changeSincePriorWeight']>0)<span class="glyphicon glyphicon-circle-arrow-up font-red-sunglo"></span> @else <span class="glyphicon glyphicon-circle-arrow-down font-green-jungle"></span> @endif  {{ sprintf("%4.1f",abs($value['changeSincePriorWeight'])) }} <small>lbs</small> @endif </span></p>
                                                            <p>Starting weight @if(isset($value['startingWeight']))<span class="pull-right sbold">{{ sprintf("%4.1f", $value['startingWeight']) }} <small>lbs</small></span> @endif</p>
                                                            <p>Change since start  <span class="pull-right sbold">@if(isset($value['changeSinceStartWeight'])) @if($value['changeSinceStartWeight']>0) <span class="glyphicon glyphicon-circle-arrow-up font-red-sunglo "></span> @else <span class="glyphicon glyphicon-circle-arrow-down font-green-jungle"></span> @endif   {{ sprintf("%4.1f",abs($value['changeSinceStartWeight'])) }} <small>lbs</small>  @endif  @if($value['changeSinceStartWeightPercent']) {{ ",".sprintf("%4.1f",abs($value['changeSinceStartWeightPercent'])) }} <small> % </small> @endif </span> </p>
                                                            <p>Goal Achieved @if(isset($value['goalAcheived'])) <span class="pull-right sbold">{{ $value['goalAcheived'] }}  <small>%</small></span>@endif</p>
                                                        </td>
                                                        <td>
                                                            <p class="">This session @if($value['thisWeekActivityMinutes']!="")   <span class="pull-right sbold">{{ $value['thisWeekActivityMinutes'] }} <small>mins</small></span> @endif</p>
                                                            <p>Prior session @if($value['priorWeekActivityMinutes']!="")<span class="pull-right sbold">{{ $value['priorWeekActivityMinutes'] }} <small>mins</small></span> @endif</p>
                                                            <p>Change since prior @if($value['changeSincePriorActivity']!="")<span class="pull-right sbold"> @if($value['changeSincePriorActivity']>0) <span class="glyphicon glyphicon-circle-arrow-up font-red-sunglo"></span> @else <span class="glyphicon glyphicon-circle-arrow-down font-green-jungle"></span> @endif  {{ abs($value['changeSincePriorActivity']) }} <small>mins</small></span> @endif</p>

                                                        </td>
                                                        <td>Since Last Session
                                                            @if($value['logDaysCount']!=""  &&   $value['logsCount']!="" ) <p class="sbold">{{$value['logDaysCount']}} Days - {{ $value['logsCount'] }} logs</p> @endif </td>
                                                        <td>@if($value['programWeek']) <p>Session : {{$value['programWeek']}} </p> @endif
                                                            <p>Videos Watched   <span class="pull-right sbold">@if(isset($value['viewedContentCount']) && isset($value['totalContentCount'])){{$value['viewedContentCount']}}/{{$value['totalContentCount']}} @endif </span></p> </td>

                                                    </tr>
                                                @endforeach
                                            @endif

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
@stop


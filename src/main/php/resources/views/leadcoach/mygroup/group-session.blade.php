@extends('include.admin-layout')

@section('page-title')
    My Groups
@stop

@section('body-class')

@stop

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route(session('userRole').'.home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('leadcoach.group') }}"> My Groups</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span> Class</span>
                    </li>

                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-green">
                                            <i class="icon-settings font-green"></i>
                                            <span class="caption-subject bold uppercase">{{ $group_detail->name  }}|{{ $group_detail->group_id  }}</span>
                                        </div>
                                        <div class="actions">
                                            <a class="btn green-jungle" href="#modal_schedule-session" data-toggle="modal">
                                                <i class="fa fa-clock-o"></i> Schedule A Session
                                            </a>
                                            <a class="btn green-jungle" href="{{route('leadcoach.group-wall', ['group_id' => base64_encode($group_id)]) }}" data-toggle="">
                                                <i class="fa fa-users"></i> Group Wall
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row margin-btm-20">

                                            <div class="col-md-3 col-xs-6 text-center">
                                                <div class=""> <i class="fa fa-user"></i> {{ $coach_detail->full_name }} <small class="font-grey-salsa">Lifestyle Coach</small>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-6">
                                                <div class=""> <i class="fa fa-list-alt" data-toggle="tooltip" title="Session"></i> @if($session_title!=""){{"Session  ".$session_title  }} @else {{ "NA" }} @endif
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-6">
                                                <div class=""><i class="iconhs-weight" data-toggle="tooltip" title="Weight Loss"></i>@if(isset($weight_lose->total_weight_loss)) {{ $weight_lose->total_weight_loss." lbs"  }} @else   {{ "0 lbs"  }} @endif
                                                    <span class="ont-grey-mint">@if($weight_lose->goal_completion_percentage!="" && $weight_lose->goal_completion_percentage!="Group target not set") {{ round($weight_lose->goal_completion_percentage, 0)." %"  }} @else   {{ "0 %"  }} @endif <span class="glyphicon glyphicon-circle-arrow-down font-green-jungle"></span></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3  col-xs-6">
                                                <div class=""><i class="fa fa-calendar" data-toggle="tooltip" title="Session Date"></i>  @if($session_date!=""){{ $session_date }} @else {{ "NA" }} @endif
                                                    <span class=" font-grey-mint"> </span>
                                                </div>
                                            </div>

                                        </div>


                                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="group_schedule_session"  data-action="{{ route('leadcoach.group-session-list') }}" >

                                            <thead>
                                            <input type="hidden" name="group_id" id="group_id" value="{{ $group_detail->group_id  }}">
                                            <tr>
                                                <th></th>
                                                <th width="30%">Session</th>
                                                <th>Session Date</th>
                                                <th>Location</th>
                                                <th>In Person</th>
                                                <th>Make Up</th>
                                                <th>Unlock Content Date</th>
                                                <th>Status</th>
                                                <th data-orderable='false'>Action</th>

                                            </tr>
                                            </thead>
                                            <tbody role="alert" aria-live="polite" aria-relevant="all">

                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
    </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->



        <!-- bootstrap modals start here-->
        <!--BEGIN MODAL-->
        <div class="modal fade in" id="modal_schedule-session" role="basic" aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Schedule A Session</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row profile">
                            <div class="col-sm-12">
                                {!! Form::open( ['method' => 'POST', 'id' => 'schedule_session_form', 'onsubmit' => 'return false;','role' => 'form', 'enctype'=>'multipart/form-data','url' => route('leadcoach.group-session-store') ] ) !!}
                                {!! Form::hidden('group_id',$group_detail->group_id ) !!}
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label for="" class="control-label">Diabetes Type
                                            </label>
                                            <select id="diabetesTypeList" name="curriculum_program_type" class="form-control" ><option diabetestypeid="{{$group_detail->diabetes_type_id}}" selected="selected" value="{{$group_detail->diabetes_type_id}}"> @if($group_detail->diabetes_type_name == "DPP-CALORIES"){{ "Prevent T2 Curriculum" }} @elseif($group_detail->diabetes_type_name == "DPP") {{ "2012 Curriculum" }} @else {{ "TYPE 2" }} @endif </option></select>
                                        </div>

                                        <div class="form-group">
                                            <label for="groupSessionsTopicNameList1" class="control-label"> Session
                                            </label>
                                            <div class="input-group select2-bootstrap-append">
                                                {{--<select id="groupSessionsTopicNameList1" class="form-control groupSessionsTopicNameList1" name="groupSessionsTopicNameList1" >
                                                    <option value="08599569-0135-4e7b-b093-18882bd19a7b" >1. Introduction to The Program</option><option value="3c9f22c3-0928-4cdb-8fe1-25985e6d404b" >2. Get Active to Prevent T2</option><option value="60d2e49a-c6a8-426c-b663-1602b858f084" >3. Track Your Activity</option><option value="defa59c2-b641-4c5c-8b6e-190eb71e36bc" >4. Eat Well To Prevent T2</option><option value="e233ea70-83fa-4374-a1a3-72fa4dc3efaf" >5. Track Food</option><option value="5ac7fe0f-3687-42ac-a193-a990a649f3d4" >6. Get More Active</option><option value="437209ab-53c0-4ec8-a9c6-6fbd9a8f513f" >7. Burn More Calories Than You Take In</option><option value="781ff403-0046-478f-82da-e7169432a726" >8. Shop and Cook to Prevent T2</option><option value="9491c42e-c95f-44e9-b951-9a7afbb6723f" >9. Manage Stress</option><option value="e2b28b5d-7c49-418a-ac38-96da30247a42" >10. Find Time for Fitness</option><option value="a5b1dac6-1b03-4d13-9cb1-69b568553fa2" >11. Cope With Triggers</option><option value="e378d53a-f75a-4b09-9c96-e4cd404b3dac" >12. Keep Heart Healthy</option><option value="1226d4bf-a507-4334-92a5-9d992e0cc5ee" >13. Take Charge of Your Thoughts</option><option value="886ffd8b-5bc0-4d75-985e-d727c38b661e" >14. Get Support</option><option value="25cccd78-bb2a-441d-bed5-97d999c35bfd" >15. Eat Well Away from Home</option><option value="51f3522a-a2b9-43be-a984-e38007e521ac" >16. Stay Motivated to Prevent T2</option><option value="81e0a22e-9428-4800-8ec5-5b737ce92de8" >17. When Weight Loss Stalls</option><option value="c0dce890-7c00-4045-b432-d00028f2d5eb" >18. Take a Fitness Break</option><option value="9e5d08ed-4e8c-48a1-b62c-1bd21c560276" >19. Stay Active to Prevent T2</option><option value="06f5d8d2-593d-4f6f-8aaa-4e2fa8d59b89" >20. Stay Active Away from Home</option><option value="5c519acd-aba6-4925-918d-65b917ca23c4" >21. More About T2</option><option value="43d9de12-ddc9-4291-bd80-d4e4175e3e72" >22. More About Carbs</option><option value="bf2248a0-a257-40c5-825f-69cf74552742" >23. Have Healthy Food you Enjoy</option><option value="40c1b99e-95e2-46fd-8fbe-03921a51db01" >24. Get Enough Sleep</option><option value="0bc3435a-fc77-4ce6-b298-ce49e8069cf5" >25. Get Back on Track</option><option value="b79bd3af-196a-430a-b232-5dfd489674b3" >26. Prevent T2 - for Life!</option>
                                                </select>--}}

                                                {!! Form::select('session', $session_group_list, '' , ['id' => 'groupSessionsTopicNameList1' ,'class' =>'form-control groupSessionsTopicNameList1']) !!}
                                                <span class="input-group-btn">
                                                                                            <button class="btn btn-default" type="button" data-select2-open="groupSessionsTopicNameList1">
                                                                                                <span class="glyphicon glyphicon-search"></span>
                                                                                            </button>
                                                                                    </span>
                                            </div>
                                            <!--  <div class="help-block">Note: Primary Food Coach <a class="sbold"> foodCoach, AmitF</a> will be automatically notified.</div> -->
                                        </div>



                                        <div class="form-group">
                                            <label for="" class="control-label"> Select Date
                                            </label>
                                            <div class="input-group date schedule_form_datetime">
                                                <input type="text" size="16" readonly class="form-control" name="date">
                                                <span class="input-group-btn">
                                                    <button class="btn default date-set" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                              </span>
                                    </div>
                                        </div>


                                        <div class="form-group">
                                            <label for="" class="control-label"> Select Time
                                            </label>

                                            <div class="input-icon">
                                                <i class="fa fa-clock-o"></i>
                                                <input type="text" class="form-control" id="schedule_time" name="time" >
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label"> Location
                                            </label>
                                            <input type="text" size="" class="form-control" name="location" placeholder="Select Location">


                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label"> Select Unlock Content Date
                                            </label>
                                            <div class="input-group date schedule_form_datetime">
                                                <input type="text" size="16" readonly class="form-control" name="unlock_content_date">
                                                <span class="input-group-btn">
                                                                                                            <button class="btn default date-set" type="button">
                                                                                                                <i class="fa fa-calendar"></i>
                                                                                                            </button>
                                                                                                        </span>
                                            </div>
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox">
                                                    <input id="inlineCheckbox21" value="1" type="checkbox" name="create_all_session"> Create All Sessions
                                                    <span></span>
                                                </label>

                                            </div>
                                        </div>








                                        <div class="form-group col-md-12 text-right">
                                            <button data-dismiss="modal" aria-hidden="true" type="button" class="btn btn-default"> &nbsp; Cancel &nbsp; </button>
                                            <button type="submit" class="btn green-jungle"> &nbsp; Add &nbsp; </button>
                                        </div>
                                    </div>
                                    <div class="clearfix"> </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->



        <!--BEGIN MODAL-->
        <div class="modal fade in" id="modal_update-session"  role="basic" aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Update A Session</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row profile">
                            <div class="col-sm-12">
                                   {!! Form::open( ['method' => 'POST', 'id' => 'schedule_session_update_form', 'onsubmit' => 'return false;','role' => 'form', 'enctype'=>'multipart/form-data','url' => route('leadcoach.get-group-session-detail'), 'update_group_session_detail'=> route('leadcoach.update-group-session-detail') ]  ) !!}
                                {!! Form::hidden('group_session_id','' ) !!}
                                  <div class="form-body">

                                        <div class="form-group">
                                            <label for="multiple" class="control-label"> Session
                                            </label>
                                            <input type="text" size="" class="form-control" name="session"  readonly>
                                            <!--  <div class="help-block">Note: Primary Food Coach <a class="sbold"> foodCoach, AmitF</a> will be automatically notified.</div> -->
                                        </div>



                                        <div class="form-group">
                                            <label for="" class="control-label"> Select Date
                                            </label>
                                            <div class="input-group date schedule_update_form_datetime" id="update_select_date" >
                                                <input type="text"  name="date" size="16" readonly class="form-control">
                                                <span class="input-group-btn">
                                                    <button class="btn default date-set" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label"> Select Time
                                            </label>
                                            <div class="input-icon">
                                                <i class="fa fa-clock-o"></i>
                                                <input type="text" class="form-control" id="schedule_update_time" name="time" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label"> Location
                                            </label>
                                            <input type="text" size="" class="form-control" name="location" placeholder="Select Location">


                                        </div>

                                        <div class="form-group">
                                            <label for="" class="control-label"> Select Unlock Content Date
                                            </label>
                                            <div class="input-group date schedule_update_form_datetime" id="update_content_date">
                                                <input type="text" size="16" readonly class="form-control" name="unlock_content_date">
                                                <span class="input-group-btn">
                                                    <button class="btn default date-set" type="button">
                                                        <i class="fa fa-calendar"></i>
                                                    </button>
                                                </span>
                                            </div>

                                        </div>








                                        <div class="form-group col-md-12 text-right">
                                            <button data-dismiss="modal" aria-hidden="true" type="button" class="btn btn-default"> &nbsp; Cancel &nbsp; </button>
                                            <button type="submit" class="btn green-jungle"> &nbsp; Update &nbsp; </button>
                                        </div>
                                    </div>
                                    <div class="clearfix"> </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

@stop


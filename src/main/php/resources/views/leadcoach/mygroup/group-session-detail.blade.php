@extends('include.admin-layout')

@section('page-title')
    My Groups
@stop

@section('body-class')

@stop

@section('content')

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container-fluid">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="{{route(session('userRole').'.home')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ route('leadcoach.group') }}"> My Groups</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{ route('leadcoach.group-session',['group_id'=>$group_detail[0]->group_id]) }}"> Class</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Session Details</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="mt-content-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-green">
                                            <i class="icon-settings font-green"></i>
                                            <span class="caption-subject bold uppercase">{{ $group_detail[0]->group_name }} | {{ $group_detail[0]->group_id }}</span>

                                        </div>
                                        <div class="actions">

                                            <a class="btn green-jungle @if($count_for_unlock_content>0)  @elseif($unlock_content_for_all!=0) @else {{ 'hide'}} @endif" href="#modal_unlock_content_for_all"  data-toggle="modal"  id="unlock_content_for_all_btn" >
                                                <i class="fa fa-unlock"></i> Unlock Content for All
                                            </a>

                                            <a class="btn green-jungle" href="{{route('leadcoach.group-wall', ['group_id' => base64_encode($group_id)]) }}" data-toggle="">
                                                <i class="fa fa-users"></i> Group Wall
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row margin-btm-20">
                                            <input type="hidden" id="session_date" value="{{ date("Y-m-d H:i:s",strtotime($start_date)) }}">
                                            <div class="col-md-12 col-xs-12">
                                                <h4 class=" font-hg theme-font"> {{$topic_title}}
                                                </h4>
                                            </div>
                                            <div class="col-md-3 col-xs-6 margin-top-10">
                                                <div class=""> <i class="fa fa-user"></i> {{ $coach_detail->lead_coach_full_name }}<small class="font-grey-salsa">  Lifestyle Coach</small>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-xs-6 margin-top-10">
                                                <div class=""><i class="iconhs-weight" data-toggle="tooltip" title="Weight Loss By Group"></i> @if(isset($weight_lose->total_weight_loss)) {{ $weight_lose->total_weight_loss." lbs"  }} @else   {{ "0 lbs"  }} @endif
                                                    <span class="ont-grey-mint">@if($weight_lose->goal_completion_percentage!="" && $weight_lose->goal_completion_percentage!="Group target not set") {{  round($weight_lose->goal_completion_percentage, 0)." %"  }} @else   {{ "0 %"  }} @endif <span class="glyphicon glyphicon-circle-arrow-down font-green-jungle"></span></span>
                                                </div>
                                            </div>
                                            <div class="col-md-3  col-xs-6 margin-top-10">
                                                <div class=""><i class="fa fa-calendar" data-toggle="tooltip" title="Session Date"></i>  {{ $start_date }} <small class="font-grey-salsa"> Session date</small>
                                                    <span class=" font-grey-mint"> </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3  col-xs-6 margin-top-10">


                                                <input type ="hidden" id="unlock_content_date" value="{{ date("m/d/Y", strtotime($unlock_content_date)) }}">
                                                <input type ="hidden" id="unlock_content_time" value="{{ date("h:i A", strtotime($unlock_content_date)) }}">
                                                <div class=""><i class="fa fa-unlock" data-toggle="tooltip" title="Unlock Content  Date"></i>  {{ $unlock_content_date }} <small class="font-grey-salsa">Unlock Content date</small>
                                                    <span class=" font-grey-mint"> </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3  col-xs-6 margin-top-10">
                                                <div class=""><i class="fa fa fa-map-marker" data-toggle="tooltip" title="Location"></i> {{ $group_detail[0]->location }}
                                                    <span class=" font-grey-mint"> </span>
                                                </div>
                                            </div>
                                            <div class="col-md-3  col-xs-6 margin-top-10">
                                                <div class=""><i class="fa fa-calendar" data-toggle="tooltip" title=" Prior Session Date"></i> @if($prior_session_date!=""){{ $prior_session_date }} @else {{ "NA" }} @endif   <small class="font-grey-salsa">Prior Session date</small>
                                                    <span class=" font-grey-mint"> </span>
                                                </div>
                                            </div>

                                        </div>


                                        <table class="table table-bordered dt-responsive table_group-session"  data-action="{{ route('leadcoach.group-session-attendance-list') }}" width="100%" id="group_schedule_session_attendance" >
                                            <thead>
                                            <input type="hidden" name="group_session_id" id="group_session_id" value="{{ $group_session_id  }}">
                                            <tr>
                                                <th></th>
                                                <th width="15%">Id/Name</th>
                                                <th>Weight</th>
                                                <th>Activity</th>
                                                <th>Logging</th>
                                                <th>Attendance</th>
                                                <th>Makeup Status</th>
                                                <th  data-orderable='false'>Action </th>

                                            </tr>
                                            </thead>
                                            <tbody role="alert" aria-live="polite" aria-relevant="all">


                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->



    <!-- bootstrap modals start here-->
    <!--BEGIN MODAL-->
    <div class="modal fade in" id="modal_add-edit-attendance"  role="basic" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id ="attendance_title"></h4>
                </div>
                <div class="modal-body">
                    <div class="row profile">
                        <div class="col-sm-12">

                            {!! Form::open( ['id' => 'modal_add_edit_attendance_form', 'onsubmit' => 'return false;','role' => 'form', 'enctype'=>'multipart/form-data','url' => route('leadcoach.get-session-attendance-data'), 'update_session_attendance' => route('leadcoach.update-session-attendance-data') , 'files' => true  ] ) !!}
                            {!! Form::hidden('group_session_id','' ) !!}
                            {{--{!! Form::hidden('member_id', '' ) !!}
                            {!! Form::hidden('patient_id', '' ) !!}
                            {!! Form::hidden('group_id', $group_detail[0]->group_id) !!}--}}
                            {!! Form::hidden('session_attendance_id', '' ) !!}

                                <div class="form-body">
                                    <div class="form-group">
                                        <label for="" class="control-label"> Attendance Type </label>
                                        <div class="mt-radio-inline" data-error-container="#form_2_membership_error">
                                            <label class="mt-radio">
                                                <input type="radio" name="attendance_type" value="InPerson" /> In Person
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="attendance_type" value="MakeUp" /> Make Up
                                                <span></span>
                                            </label>
                                        </div>

                                    </div>

                                    <div class="form-group for_makeup">
                                        <label for="" class="control-label"> Tracking Type </label>
                                        <div class="mt-radio-inline" data-error-container="#form_2_membership_error">
                                            <label class="mt-radio">
                                                <input type="radio" name="tracking_type" value="0" /> Digital Tracker
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="tracking_type" value="1" /> Paper Tracker
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>


                                    <div class="form-group for_makeup">
                                        <label for="" class="control-label"> Days Logged
                                        </label>
                                        <input type="number" size="" class="form-control" name="days_logged" placeholder="">
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="control-label"> Select Unlock Content Date
                                        </label>
                                        <div class="input-group date attendance_content_date">
                                            <input type="text" size="16" name="unlock_content_date" id="attendance_update_content_date" readonly class="form-control" aria-required="true">
                                            <span class="input-group-btn">
                                                <button class="btn default date-set" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="" class="control-label"> Select Time
                                        </label>
                                        <div class="input-icon">
                                            <i class="fa fa-clock-o"></i>
                                            <input type="text" class="form-control" id="attendance_time" name="time" readonly>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="multiple" class="control-label">Add Notes
                                        </label>
                                        <textarea id="notes" name="notes" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group col-md-12 text-right">
                                        <button type="button" class="btn  btn-default" data-dismiss="modal"> &nbsp; Cancel &nbsp; </button>
                                        <button type="submit" class="btn green-jungle"> &nbsp; Submit &nbsp; </button>
                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!--modals end here-->


    <!--BEGIN MODAL-->
    <div class="modal fade in" id="modal_add-edit-weight" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="weight_title" ></h4>
                </div>
                <div class="modal-body">
                    <div class="row profile">
                        <div class="col-sm-12">
                            {!! Form::open( ['id' => 'modal_add_edit_weight_form', 'class'=>'col-sm-12', 'onsubmit' => 'return false;','role' => 'form', 'enctype'=>'multipart/form-data','url' => route('leadcoach.get-session-attendance-weight-data'), 'update_session_attendance_weight' =>route('leadcoach.update-session-attendance-weight-data') , 'files' => true  ] ) !!}
                            {!! Form::hidden('session_attendance_id', '' ) !!}
                            {!! Form::hidden('patient_id', '' ) !!}
                                <div class="form-body">
                                    <div class="form-group ">
                                        <label class="control-label">Weight</label>
                                        <div class="input-group">
                                            <input class="form-control" placeholder="" aria-describedby=""  name="weight" type="text">
                                            <span class="input-group-addon" id="">Lbs</span>
                                        </div>   </div>

                                    <div class="form-group col-md-12 text-right">
                                        <button type="button" class="btn  btn-default" data-dismiss="modal"> &nbsp; Cancel &nbsp; </button>
                                        <button type="submit" class="btn green-jungle"> &nbsp; Submit &nbsp; </button>
                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!--BEGIN MODAL FOR  MARK AS COMPLETED  -->
    <div class="modal fade in" id="modal_mark-as-completed" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Mark As Complete</h4>
                </div>
                <div class="modal-body">
                    <div class="row profile">
                        <div class="col-sm-12">
                            {!! Form::open( ['id' => 'modal_mark_as_completed_form', 'class'=>'col-sm-12', 'onsubmit' => 'return false;','role' => 'form', 'enctype'=>'multipart/form-data','url' => route('leadcoach.get-session-attendance-weight-data'), 'mark_as_complete' =>route('leadcoach.update-mark-as-complete') , 'files' => true  ] ) !!}
                            {!! Form::hidden('session_attendance_id', '' ) !!}
                            {!! Form::hidden('patient_id', '' ) !!}

                            <div class="form-body">
                                <div class="form-group ">
                                    <label class="control-label">Weight</label>
                                    <div class="input-group">
                                        <input class="form-control" placeholder="" aria-describedby=""  name="weight" type="text">
                                        <span class="input-group-addon" id="">Lbs</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="multiple" class="control-label">Add Notes
                                    </label>
                                    <textarea id="notes" name="notes" class="form-control"></textarea>
                                </div>

                                <div class="form-group col-md-12 text-right">
                                    <button type="button" class="btn  btn-default" data-dismiss="modal"> &nbsp; Cancel &nbsp; </button>
                                    <button type="submit" class="btn green-jungle"> &nbsp; Submit &nbsp; </button>
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <!--BEGIN MODAL For Unlock content for all-->
    <div class="modal fade in" id="modal_unlock_content_for_all" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Unlock Content</h4>
                </div>
                <div class="modal-body">
                    <div class="row profile">
                        <div class="col-sm-12">
                            {!! Form::open( ['id' => 'modal_unlock_content_for_all_form', 'class'=>'col-sm-12', 'onsubmit' => 'return false;','role' => 'form', 'enctype'=>'multipart/form-data','url' => route('leadcoach.unlock_content_get_data'), 'update_unlock_content_get_data' =>route('leadcoach.update_unlock_content_data') , 'files' => true  ] ) !!}
                             {!! Form::hidden('group_session_id', $group_session_id ) !!}

                            <div class="form-body">
                                <div class="form-group">
                                    <label for="" class="control-label"> Select Unlock Content Date
                                    </label>
                                    <div class="input-group date unlock_content_date_for_all">
                                        <input type="text" size="16" name="unlock_content_date" id="unlock_content_date_for_all" readonly class="form-control" aria-required="true">
                                        <span class="input-group-btn">
                                                <button class="btn default date-set" type="button">
                                                    <i class="fa fa-calendar"></i>
                                                </button>
                                            </span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="" class="control-label"> Select Time
                                    </label>
                                    <div class="input-icon">
                                        <i class="fa fa-clock-o"></i>
                                        <input type="text" class="form-control" id="unlock_content_time_for_all" name="time" readonly>
                                    </div>
                                </div>

                                <div class="form-group col-md-12 text-right">
                                    <button type="button" class="btn  btn-default" data-dismiss="modal"> &nbsp; Cancel &nbsp; </button>
                                    <button type="submit" class="btn green-jungle"> &nbsp; Submit &nbsp; </button>
                                </div>
                            </div>
                            <div class="clearfix"> </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->





    <!--BEGIN MODAL-->
    <div class="modal fade in" id="modal_add-edit-activity"  role="basic" aria-hidden="true">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="activity_title"></h4>
                </div>
                <div class="modal-body">
                    <div class="row profile">
                        <div class="col-sm-12">

                                {!! Form::open( ['id' => 'modal_add_edit_activity_form', 'class'=>'col-sm-12', 'onsubmit' => 'return false;','role' => 'form', 'enctype'=>'multipart/form-data','url' => route('leadcoach.get-session-attendance-data'), 'update_session_attendance_activity' => route('leadcoach.update-session-attendance-activity-data') , 'files' => true  ] ) !!}
                                {!! Form::hidden('session_attendance_id', '' ) !!}
                                {!! Form::hidden('patient_id', '' ) !!}
                                {!! Form::hidden('session_date', '' ) !!}
                                <div class="form-body">
                                    <div class="form-group ">
                                        <label class="control-label">Activity</label>
                                        <div class="input-group">
                                            <input class="form-control" placeholder=" " aria-describedby="" name="minutes" type="text">
                                            <span class="input-group-addon" id="">Minutes</span>
                                        </div>   </div>

                                    <div class="form-group col-md-12 text-right">
                                        <button type="button" class="btn  btn-default" data-dismiss="modal"> &nbsp; Cancel &nbsp; </button>
                                        <button type="submit" class="btn green-jungle"> &nbsp; Submit &nbsp; </button>
                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!--modals end here-->

@stop

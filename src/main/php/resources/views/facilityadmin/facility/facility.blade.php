@extends('include.admin-layout')

@section('page-title')
    Facility
@stop

@section('body-class')

@stop


@section('content')

    <!-- BEGIN PAGE CONTENT BODY -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- BEGIN PAGE BREADCRUMBS -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{route('facilityadmin.home')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Facility</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMBS -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="page-content-inner">
                <div class="mt-content-body">
                    {!! Form::open( array('url' => URL::route('facilityadmin.facility-store'), 'method' => 'post', 'id' => 'form_facilityedit', 'role' => 'form') ) !!}
                        <div class="row">
                            <div class="portlet light ">

                                <div class="portlet-body form">

                                    <div class="clearfix"></div>

                                    @if(Session::has('success'))
                                        <div class="alert alert-warning margiv-top-10">
                                            <button class="close" data-close="alert"></button>
                                            <span> {!! Session::get('success') !!} </span>
                                        </div>
                                    @endif

                                    @if($errors->has())
                                        @foreach ($errors->all() as $error)
                                            <div class="alert alert-danger margiv-top-10">
                                                <button class="close" data-close="alert"></button>
                                                <span> {{ $error }} </span>
                                            </div>
                                        @endforeach
                                    @endif

                                    <div class="col-md-6">
                                        <h3 class="form-section">Details</h3>
                                        <div class="form-group">
                                            <label class="control-label">Facility Name </label>
                                            <input type="text" placeholder="Facility Name" class="form-control" value="{{$facility->name}}" name="name" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">State</label>
                                            {!! Form::select('state', $state, $facility->state,  ['class' =>'form-control']) !!}
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">City</label>
                                            <input class="form-control" name="city" required value="{{$facility->city}}" type="text">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Zip</label>
                                            <input class="form-control" name="zip"  required="" value="{{$facility->zip}}" type="text">
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Address</label>
                                            <textarea class="form-control" name="address" rows="2" placeholder="Address">{{$facility->address}}</textarea>

                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Time Zone</label>
                                            {!! Form::select('timezone', $timezone, $facility->timezone_offset,  ['class' =>'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <h3 class="form-section">Contact Info</h3>
                                        <div class="form-group">
                                            <label class="control-label">Contact Number </label>
                                            <input class="form-control" name="contact_info" required="" value="{{$facility->contact_info}}" type="text">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Contact Person Name</label>
                                            <input class="form-control" name="contact_person_name"  required="" value="{{$facility->contact_person_name}}" type="text">
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Partner ID </label>
                                            <input id="partner_id" name="partner_id" class="form-control" value="{{$facility->partner_id}}" type="text">
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">CDC Organization Code</label>
                                            <input id="cdc_organization_code" name="cdc_organization_code" class="form-control" value="{{$facility->cdc_organization_code}}" type="text">
                                        </div>

                                        <div class="form-group">
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox">
                                                    {!! Form::checkbox('is_notification_enabled', 1, $facility->is_notification_enabled, ['class' => 'field']) !!}  Notifications enabled
                                                    <span></span>
                                                </label>
                                                <label class="mt-checkbox">
                                                    {!! Form::checkbox('is_skip_consent', 1, $facility->is_skip_consent, ['class' => 'field']) !!} Skip Consent Form
                                                    <span></span>
                                                </label>
                                                <label class="mt-checkbox">
                                                    {!! Form::checkbox('is_chatbot_enabled', 1, $facility->is_chatbot_enabled, ['class' => 'field']) !!} Enable Chatbot
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="margiv-top-10">
                                        <button type="submit" class="btn green-jungle">Submit</button>
                                        {{--<button type="button" class="btn grey-salsa btn-outline">Cancel</button>--}}
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        <!-- END PAGE CONTENT INNER -->
        </div>
    </div>

    <!-- END PAGE CONTENT BODY -->


@stop

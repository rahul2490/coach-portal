@extends('include.admin-layout')

@section('page-title')
    Add Member
@stop

@section('body-class')

@stop


@section('content')

    <!-- BEGIN PAGE CONTENT BODY -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- BEGIN PAGE BREADCRUMBS -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{route(session('userRole') . '.home')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Add Member</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMBS -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="page-content-inner">
                <div class="mt-content-body">
                    {!! Form::open( array('url' => URL::route('addmember-store'), 'method' => 'post', 'id' => 'form_add_member_recruitment', 'role' => 'form') ) !!}
                    <div class="row">
                        <div class="portlet light ">

                            <div class="portlet-body form">

                                @if(Session::has('success'))
                                    <div class="alert alert-warning margiv-top-10">
                                        <button class="close" data-close="alert"></button>
                                        <span> {!! Session::get('success') !!} </span>
                                    </div>
                                @endif

                                @if($errors->has())
                                    @foreach ($errors->all() as $error)
                                        <div class="alert alert-danger margiv-top-10">
                                            <button class="close" data-close="alert"></button>
                                            <span> {{ $error }} </span>
                                        </div>
                                    @endforeach
                                @endif

                                <div class="col-md-6">
                                    <h3 class="form-section">Add Member</h3>
                                    <div class="form-group">
                                        <label class="control-label">First Name </label>
                                        <input type="text" placeholder="First Name" class="form-control" value="{{ old('first_name') }}" name="first_name" />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Last Name </label>
                                        <input type="text" placeholder="Last Name" class="form-control" value="{{ old('last_name') }}" name="last_name" />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">MRN </label>
                                        <input type="text" placeholder="MRN" class="form-control" value="{{ old('mrn') }}" name="mrn" id="mrn" />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Confirm MRN </label>
                                        <input type="text" placeholder="Confirm MRN" class="form-control" value="{{ old('confirm_mrn') }}" name="confirm_mrn" />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email </label>
                                        <input type="email" placeholder="Email" class="form-control" value="{{ old('email') }}" name="email" />
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Address</label>
                                        <textarea class="form-control" name="address" rows="2" placeholder="Address">{{ old('address') }}</textarea>

                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">City</label>
                                        <input class="form-control" name="city"  value="{{ old('city') }}" placeholder="City" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">State</label>
                                        {!! Form::select('state', $state, '',  ['class' =>'form-control']) !!}
                                        {{--<input class="form-control" name="state"  value="" placeholder="state" type="text">--}}
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Zip Code</label>
                                        <input class="form-control" name="zip"  value="{{ old('zip') }}"  placeholder="Zip Code"type="text">
                                    </div>



                                </div>
                                <div class="col-md-6">
                                    <h3 class="form-section"> &nbsp;   </h3>

                                    <div class="form-group">
                                        <label class="control-label">Phone Number </label>
                                        <input class="form-control" name="phone_number" value="{{ old('phone_number') }}" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Date of Birth</label>
                                        <input class="form-control" name="date_of_birth" id ="date_of_birth" value="{{ old('date_of_birth') }}" type="text" readonly>
                                    </div>



                                    <div class="form-group">
                                        <label class="control-label">Gender </label>
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio">
                                                <input type="radio" name="gender" value="Male"  @if(old('gender')=="Male"){{ "checked" }} @endif />Male
                                                <span></span>
                                            </label>
                                            <label class="mt-radio">
                                                <input type="radio" name="gender" value="Female" @if(old('gender')=="Female"){{ "checked" }} @endif/>Female
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Type</label>
                                        {!! Form::select('diabetes_type', $diabetes_type, old('diabetes_type'),  ['class' =>'form-control']) !!}
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Patient Status </label>

                                        <select name="patient_status" class="form-control" >
                                            <option  value=""  @if(old('patient_status')==""){{ "selected" }} @endif >Select Patient Status </option>
                                            <option value="Waiting" @if(old('patient_status')=="Waiting"){{ "selected" }} @endif >Waiting</option>
                                        </select>
                                    </div>
                                    @if(!empty($facility_list))
                                        <div class="form-group">
                                            <label class="control-label">Choose Facility</label>
                                            <select  class="form-control facility_list" name="facility_list" >
                                                <option value="">Select Facility</option>
                                                @foreach($facility_list as $facility)
                                                    <option value="{{$facility->facility_id}}">{{$facility->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    <div class="form-group">
                                        <label class="control-label">Choose Lifestyle Coach</label>
                                        <select  class="form-control" name="life_style_coach" id="lifestyle_coach" >
                                            <option value="">Select Lifestyle Coach</option>
                                            {{--@foreach($lead_coach as $key => $coach)--}}
                                                {{--@foreach($coach as $in_key => $in_coach)--}}
                                                    {{--<option value="{{ $in_key }}" @if(old('life_style_coach')== $in_key){{ "selected" }} @endif>{{  $in_coach." (".$in_key .")" }}</option>--}}
                                                {{--@endforeach--}}
                                            {{--@endforeach--}}

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Choose Primary Food Coach</label>
                                        <select  class="form-control " name="food_coach" id="food_coach"  >
                                            <option value="">Select Food Coach</option>
                                            {{--@foreach($food_coach as $key => $coach)--}}
                                                {{--@foreach($coach as $in_key => $in_coach)--}}
                                                    {{--<option value="{{ $in_key }}" @if(old('food_coach')== $in_key){{ "selected" }} @endif>{{  $in_coach." (".$in_key .")" }}</option>--}}
                                                {{--@endforeach--}}
                                            {{--@endforeach--}}

                                        </select>
                                    </div>


                                    <div class="form-group">
                                        <div class="mt-checkbox-inline">
                                            <label class="mt-checkbox">
                                                {{--{!! Form::checkbox('offline_member', 1,  ['class' => 'field', 'checked'=>'']) !!}  Offline Member--}}
                                                <input type="checkbox" name="offline_member" class="field" value="1" @if(old('offline_member')){{ "checked" }} @endif > Offline Member
                                                <span></span>
                                            </label>

                                        </div>
                                    </div>


                                    {{--<div class="form-group">
                                        <label class="control-label">Select Member's Team</label>
                                        <div class="mt-checkbox-inline">

                                            <label class="mt-checkbox">
                                                {!! Form::checkbox('is_notification_enabled', 'shubhamc neema',  ['class' => 'field']) !!}  ShubhamC neema
                                                <span></span>
                                            </label>
                                           --}}{{-- <label class="mt-checkbox">
                                                {!! Form::checkbox('is_skip_consent', 1, ['class' => 'field']) !!} Skip Consent Form
                                                <span></span>
                                            </label>--}}{{--
                                        </div>
                                    </div>--}}

                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="margiv-top-10">
                                    <button type="submit" class="btn green-jungle">Submit</button>
                                    {{--<button type="button" class="btn grey-salsa btn-outline">Cancel</button>--}}
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>

    <!-- END PAGE CONTENT BODY -->

@stop

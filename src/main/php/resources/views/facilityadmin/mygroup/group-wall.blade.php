@extends('include.admin-layout')

@section('page-title')
    My Groups
@stop

@section('body-class')

@stop

@section('content')

    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container-fluid">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{route(session('userRole').'.home')}}">Home</a>
                            <i class="fa fa-circle"></i>
                            <a href="{{route('facilityadmin.group')}}">My Groups</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Group Feed</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="mt-content-body">
                            <!-- BEGIN TEXT & VIDEO -->
                            <div class="row margin-bottom-40">
                                <div class="col-sm-8">
                                    <div class="portlet light bordered ">

                                        <div class="portlet-body my_group_comp">
                                            <input type="hidden" id="page_url" value="{{'facilityadmin'}}">
                                            <div class="clearfix"></div>
                                            <!-- END TASK COMMENT FORM -->
                                            <!-- TASK COMMENTS -->
                                            <div class="form-group">
                                                <div class="col-md-12 margin-top-20 ">
                                                    <ul class="media-list post-list " id="media">
                                                        @if(count($post_data)==0 )
                                                            <div class="alert alert-warning margiv-top-10">
                                                                <center><span>No data found</span></center>
                                                            </div>
                                                        @endif
                                                        @foreach($post_data as $post)
                                                       <li class="media" id="post_{{ $post['post_id']}}" >
                                                           @if($post['is_auto_post']=='1')
                                                                <a class="pull-left" href="javascript:;">
                                                                    <img class="todo-userpic" src="{{ env('APP_BASE_URL').'/img/HS_auto-post-icon.png' }}" width="27px" height="27px"> </a>
                                                            @else
                                                                <a class="pull-left" href="javascript:;">
                                                                    <img class="todo-userpic" src="{{ $post['post_user_image']}}" width="27px" height="27px"> </a>
                                                           @endif


                                                            <div class="media-body todo-comment">

                                                                <div class="actions pull-right">
                                                                    <a post_id="{{ $post['post_id']}}" class="btn font-grey-salsa btn-sm delete-group-post" href="javascript:;">
                                                                        <i class="fa fa-trash"></i> Delete
                                                                    </a>
                                                                </div>
                                                                <p class="todo-comment-head">
                                                                    @if($post['is_auto_post']=='1')
                                                                        <span class="todo-comment-username">HealthSlate</span> &nbsp;
                                                                        @else
                                                                        <span class="todo-comment-username">{{$post['full_name']}}</span> &nbsp;
                                                                        @endif

                                                                    <span class="todo-comment-date">{{$post['timestamp']}}</span>
                                                                </p>

                                                                @if($post['post_type']=="Html")
                                                                <p class="todo-text-color"> {!!$post['description']!!}</p>
                                                                @endif
                                                                @if($post['post_type']=="Text")
                                                                    <p class="todo-text-color"> {!!$post['description']!!}</p>
                                                                    @if(($post['media_type']=="Image")||($post['media_type'] =='image'))
                                                                        <img  width="100%" src="{{$post['media_url']}}">
                                                                    @endif
                                                                    @endif
                                                                @if($post['post_type']=="Foodlog")
                                                                    <p class="todo-text-color">{!!$post['description']!!}</p>
                                                                    <p class="todo-text-color"><span>{{$post['meal_type']}}</span> | <span>{{$post['log_calories']}} Cal</span></p>
                                                                    <img  src="{{$post['food_image']}}">
                                                                @endif

                                                                <!-- Nested media object -->
                                                                <div class="media-comment_{{$post['post_id']}}"  class="desc img-responsive" id="media-comment_{{$post['post_id']}}">
                                                                    <!-- Nested media object -->
                                                                    @if(!empty($post['post_comments']) )
                                                                        @foreach($post['post_comments'] as $comments)

                                                                            <div class="media" id="post_comment_{{ $comments['post_comment_id']}}">

                                                                                <a class="pull-left" href="javascript:;">

                                                                                    <img class="todo-userpic" src="{{ $comments['comment_user_image']}}" width="27px" height="27px"> </a>

                                                                                <a post_comment_id="{{ $comments['post_comment_id']}}"  post_id="{{$post['post_id']}}" class="btn font-grey-salsa btn-sm delete-post-comment pull-right" href="javascript:;">
                                                                                    <i class="fa fa-trash"></i> Delete
                                                                                </a>

                                                                                <div class="media-body">


                                                                                    <p class="todo-comment-head"><span class="todo-comment-username">{{$comments['commented_user_name']}}</span> &nbsp
                                                                                        <span class="todo-comment-date">{{$comments['timestamp']}}</span></p>

                                                                                    <p class="todo-text-color">{!! $comments['comment'] !!}</p>

                                                                                </div>
                                                                            </div>

                                                                        @endforeach

                                                                    @endif
                                                                </div>
                                                                <div class="actions pull-right margin-btm-10 margin-t-m-10">

                                                                    <small> {{$post['total_like']}} </small> <a class="btn btn-circle btn-icon-only btn-default _like">
                                                                        <i class="fa fa-thumbs-up"></i>
                                                                    </a>
                                                                    <small id="total_comment_{{$post['post_id']}}"> {{$post['total_comment']}} </small> <a class="btn btn-circle btn-icon-only btn-default _comment">
                                                                        <i class="fa fa-commenting"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                                <input type="hidden" id="row_no" value="25">
                                                                <input type="hidden" id="group_id" value="{{$post['group_id']}}">
                                                                <input type="hidden" id="post_url" value="<?php echo env('APP_BASE_URL')?>facilityadmin/group-wall_scroll">
                                                                <input type="hidden" id="post_delete_url" value="<?php echo env('APP_BASE_URL')?>facilityadmin/post-delete">
                                                            </div>
                                                        </li>

                                                          @endforeach


                                                       </ul>


                                                </div>


                                            </div>
                                            <!-- END TASK COMMENTS -->





                                            <div class="clearfix"></div>
                                        </div>
                                    </div>




                                </div>

                                <div class="col-sm-4 col-xs-12 ">
                                    <!-- group feed -->
                                    <div class="portlet light inbox bordered ">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-group font-dark"></i>
                                                <span class="caption-subject font-dark bold uppercase">Group Progress</span>
                                            </div>


                                        </div>
                                        <div class="portlet-body ">
                                            <div class="mt-element-list">
                                                <div class="mt-list-container list-simple">
                                                    <ul>
                                                        <li class="mt-list-item">
                                                            <div class="list-icon-container font-yellow-lemon">
                                                                <i class="iconhs-weight"></i>
                                                            </div>
                                                           @if(!empty($weight_lose->total_weight_loss))
                                                            <div class="pull-right font-grey-salsa">{{$weight_lose->total_weight_loss}} lbs lost</div>
                                                               @else
                                                                <div class="pull-right font-grey-salsa">0.0 lbs lost</div>
                                                            @endif

                                                            <div class="list-item-content">
                                                                Group weight lost
                                                            </div>
                                                        </li>
                                                        <li class="mt-list-item">
                                                            <div class="list-icon-container font-purple ">
                                                                <i class="iconhs-shoe"></i>
                                                            </div>
                                                            <div class="pull-right font-grey-salsa"> {{$group_steps_average }} average steps/day </div>
                                                            <div class="list-item-content">
                                                                Group steps
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                    <!-- most steps section -->
                                    <div class="portlet light inbox bordered ">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa fa-trophy"></i>
                                                <span class="caption-subject font-dark bold uppercase">Most Steps/day Since Monday</span>

                                            </div>


                                        </div>
                                        <div class="portlet-body ">

                                            <div class="mt-actions margin-btm-20">
                                                <?php $i=1; ?>
                                                @if(count($step_patient )>0)
                                                @foreach($step_patient as $patient)
                                                    @if($patient->is_step_count_challenge_accepted==1)
                                                <div class="mt-action">
                                                    <div class="mt-action-img">
                                                        @if(!empty($patient->patient_image))
                                                            <img width="35" class="contact-pic"  src="{{env('PROFILE_IMAGE_BASE_URL').$patient->patient_image}}" />
                                                            @else
                                                            <img width="35" class="contact-pic"  src="{{env('APP_BASE_URL').'/img/default-user.png'}}" />
                                                            @endif
                                                    </div>
                                                    <div class="mt-action-body">
                                                        <div class="mt-action-row">
                                                            <div class="mt-action-info ">
                                                                <div class="mt-action-details ">
                                                                    <span class="mt-action-author">{{$patient->patient_name}}</span>
                                                                    <?php  $day=0;
                                                                    if($day_name=='Mon'){
                                                                        $day=1;
                                                                    }
                                                                    if($day_name=='Tue'){
                                                                        $day=2;
                                                                    }
                                                                    if($day_name=='Wed'){
                                                                        $day=.3;
                                                                    }
                                                                    if($day_name=='Thu'){
                                                                        $day=4;
                                                                    }
                                                                    if($day_name=='Fri'){
                                                                        $day=5;
                                                                    }
                                                                    if($day_name=='Sat'){
                                                                        $day=6;
                                                                    }
                                                                    if($day_name=='Sun'){
                                                                        $day=7;
                                                                    }?>
                                                                    <p class="mt-action-desc">{{round($patient->total_steps/$day)}} Steps/day</p>
                                                                </div>
                                                            </div>

                                                            <div class="">
                                                                <span href="javascript:;" class="btn btn-circle yellow-lemon pull-right">{{$i}}</span>
                                                                   {{--<span href="javascript:;" class="btn btn-circle purple pull-right">{{$i}}</span>--}}
                                                                   {{--<span href="javascript:;" class="btn btn-circle yellow-gold pull-right">{{$i}}</span>--}}

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                                <?php $i++;?>
                                                     @endif

                                                @endforeach
                                                    @else
                                                    <div class="mt-action">
                                                        <div class="mt-action-img">
                                                        </div>
                                                        <div class="mt-action-body">
                                                            <div class="mt-action-row">
                                                                <div class="mt-action-info ">
                                                                    <div class="mt-action-details ">
                                                                        <span class="mt-action-author">No Data available</span>
                                                                    </div>
                                                                </div>
                                                                <div class="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif

                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>


                                    <!-- group memebers -->

                                    <div class="portlet light inbox bordered ">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-group font-dark"></i>
                                                <span class="caption-subject font-dark bold uppercase">Group Members</span>
                                            </div>


                                        </div>
                                        <div class="portlet-body ">
                                            <div class="">

                                                <ul class="inbox-contacts">
                                                    @if(!empty($group_member))
                                                        @foreach($group_member as $value)
                                                            <li>
                                                                <a>
                                                                    <img class="contact-pic" src="{{ env('APP_BASE_URL').'/img/default-user.png' }}">
                                                                    <span class="contact-name">{{ $value->full_name }}</span>
                                                                @if($value->user_type=="Coach")
                                                                    @if($value->leadcoach_id==$value->user_id)
                                                                    <small class="text-right font-grey-salsa"> Lifestyle Coach </small>
                                                                        @else
                                                                            <small class="text-right font-grey-salsa"> Coach </small>
                                                                        @endif
                                                                 @endif
                                                                    @if($value->user_type=="Food Coach")
                                                                        @if($value->foodcoach_id==$value->user_id)
                                                                        <small class="text-right font-grey-salsa"> Primary Food Coach </small>
                                                                        @else
                                                                            <small class="text-right font-grey-salsa"> Coach </small>
                                                                        @endif
                                                                    @endif

                                                                        {{--<small class="text-right font-grey-salsa">{{ ($value->user_type == 'Coach') ? 'Lifestyle Coach' : (($value->user_type == 'PATIENT') ? $value->patient_id : $value->user_type) }}</small>--}}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    @endif
                                                </ul>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <!-- END TEXT & VIDEO -->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

    </div>
    <!-- END CONTAINER -->

@stop



@extends('include.admin-layout')

@section('page-title')
    My Groups
@stop

@section('body-class')

@stop

@section('content')

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container-fluid">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{route('facilityadmin.home')}}">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>My Groups</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="mt-content-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption font-green">
                                                <i class="fa fa-users font-green"></i>
                                                <span class="caption-subject bold uppercase">My Groups</span>
                                            </div>
                                            <div class="actions">
                                                <a href="#create-new-group" data-toggle="modal"  data-backdrop="static" data-keyboard="false"  class="btn green-jungle ">
                                                    <i class="fa fa-plus"></i> Create New Group </a>
                                            </div>
                                        </div>

                                        <div class="portlet-body">
                                            <div style="" class="col-md-3 col-sm-3 custom-filter-sort">
                                                <div id="" class="dataTables_filter">
                                                    <label>Group Type:
                                                        <select class="form-control input-sm input-small  input-inline" id="filterGroup">
                                                            <option value="all">All Groups</option>
                                                            <option value="inperson">In Person</option>
                                                            <option value="online">Online</option>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div style="" class="col-md-3 col-sm-3 custom-filter-sort">
                                                <div id="" class="dataTables_filter">
                                                    <label>Group Capacity:
                                                        <select class="form-control input-sm input-small input-inline" id="filterCapacity">
                                                            <option value="all">All</option>
                                                            <option value="notfull">Available (Not Full)</option>
                                                            <option value="full">Full</option>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>

                                            {{--<table class="table table-bordered dt-responsive member-table" width="100%" id="group_list" data-action="{{ route('facilityadmin.group-list') }}">
                                                <thead>
                                                <tr>
                                                    <th ></th>
                                                    <th width="30%">Group Name</th>
                                                    <th width="18%">Status</th>
                                                    <th width="15%"  data-orderable='false'>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>--}}


                                            <table class="table table-bordered dt-responsive member-table " width="100%" id="group_list" data-action="{{ route('facilityadmin.group-list') }}">
                                                <thead>
                                                    <tr>
                                                        <th ></th>
                                                        <th width="30%">Group Name</th>
                                                        <th width="20%">Coaches</th>
                                                        <th width="5%"  data-orderable='false'>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>


                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->


        <!-- /.modal -->
        <!--modal Direct Messages coach Memeber -->
        <div class="modal fade" id="create-new-group"  role="basic" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Create Group </h4>
                    </div>
                    <div class="modal-body">

                        {!! Form::open( ['method' => 'POST', 'id' => 'create_new_group', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('facilityadmin.group-create') ] ) !!}
                            <div class="form-group">
                                <label class="control-label">Group Name</label>
                                <input type="text" placeholder="Group Name" class="form-control" value="" name="name" />
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label class="control-label">Diabetes Type </label>
                                    {!! Form::select('diabetesTypeId', $diabetes_type, '',  ['class' =>'form-control', 'id' => 'diabetesTypeList']) !!}
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="control-label">Capacity</label>
                                    <input autocomplete="off" min="0" name="capacity" placeholder="" class="form-control" type="number">
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label>Select Group Type</label>
                                    <div class="mt-radio-inline padding-0" data-error-container="#group_type-error">
                                        <label class="mt-radio"> In Person
                                            <input value="1" name="group_type" type="radio" class="group_type">
                                            <span></span>
                                        </label>
                                        <label class="mt-radio"> Online
                                            <input value="0" name="group_type" type="radio" class="group_type">
                                            <span></span>
                                        </label>
                                    </div>
                                    <div id="group_type-error"> </div>
                                </div>
                                <div class="form-group col-sm-6">
                                    <label class="control-label">Preferred Language</label>
                                    <select id="preferredLanguage" name="preferredLanguage" class="form-control" >
                                        <option diabetestypeid="1" value="English">English</option>
                                        <option diabetestypeid="2" value="Spanish">Spanish</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group selectlifecoach">
                                <label for="select-life-coach" class="control-label"> Select Lifestyle Coach
                                </label>
                                <div class="input-group select2-bootstrap-append">
                                    <select id="selectlifecoach" class="form-control select-life-coach" name="selectLifeCoach" >
                                        <option value="">Select Lifestyle Coach</option>
                                        @foreach($lead_coach as $key => $coach)
                                            @foreach($coach as $in_coach)
                                                <option title="{{ $in_coach->email }}" value="{{ $in_coach->user_id }}">{{  $in_coach->full_name }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-select2-open="selectlifecoach">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                     </span>
                                </div>
                            </div>
                          {{--  <div class="form-group selectfoodcoach">
                                <label for="select-food-coach" class="control-label"> Select Food Coach
                                </label>
                                <div class="input-group select2-bootstrap-append">
                                    <select id="selectfoodcoach" class="form-control select-food-coach" name="selectFoodCoach" >
                                        <option value="">Select Food Coach</option>
                                        @foreach($food_coach as $key => $coach)
                                            @foreach($coach as $in_key => $in_coach)
                                                <option title="{{ $in_coach->email }}" value="{{ $in_coach->user_id }}">{{  $in_coach->full_name }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" data-select2-open="selectfoodcoach">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                    </span>
                                </div>
                            </div>--}}
                            <div class="form-group">
                                <label for="auto-post" class="control-label"> Auto Post Time
                                </label>
                                <div class="input-icon">
                                    <i class="fa fa-clock-o"></i>
                                    <input type="text" class="form-control timepicker timepicker-default" name="autoPostTime" readonly>
                                </div>
                                <div class="mt-checkbox-inline ">
                                    <label class="mt-checkbox">
                                        <input type="checkbox" class="autoPostEnable" value="1" name="autoPostEnable" aria-describedby="service-error"> Enable Auto Post
                                        <span></span>
                                    </label>
                                </div>


                            </div>
                            <div class="form-group text-right">
                                <!--   <button type="button" class="btn  btn-default" class="close" data-dismiss="modal" aria-hidden="true"> &nbsp; Cancel &nbsp; </button>  -->
                                <button type="submit" class="btn green-jungle" > &nbsp; Create &nbsp; </button>
                            </div>

                        {!! Form::close() !!}

                    </div>

                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal" id="edit_group_detail">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Group Details</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open( ['method' => 'POST', 'id' => 'edit_mygroup_detail', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('facilityadmin.group-detail') , 'create-group' => route('facilityadmin.group-update') ] ) !!}
                        {!! Form::hidden('id', '' ) !!}
                            <div class="form-group">
                                <label class="control-label">Group Name</label>
                                <input type="text" placeholder="Group Name" class="form-control" value="" name="name" />
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label class="control-label">Diabetes Type </label>
                                    {!! Form::select('diabetesTypeId', $diabetes_type, '',  ['class' =>'form-control', 'id' => 'diabetesTypeList']) !!}
                                </div>

                                <div class="form-group col-sm-6">
                                    <label class="control-label">Capacity</label>
                                    <input autocomplete="off" min="0" name="capacity" placeholder="" class="form-control" type="number">
                                </div>
                            </div>


                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label class="control-label">Preferred Language</label>
                                    <select id="preferredLanguage" name="preferredLanguage" class="form-control" >
                                        <option diabetestypeid="1" value="English">English</option>
                                        <option diabetestypeid="2" value="Spanish">Spanish</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group selectlifecoach">
                                <label for="select-life-coach" class="control-label"> Select Lifestyle Coach
                                </label>
                                <div class="input-group select2-bootstrap-append">
                                    <select id="selectlifecoach" class="form-control select-life-coach" name="selectLifeCoach" >
                                        <option value="">Select Lifestyle Coach</option>
                                        @foreach($lead_coach as $key => $coach)
                                            @foreach($coach as $in_key => $in_coach)
                                                <option title="{{ $in_coach->email }}" value="{{ $in_coach->user_id }}">{{  $in_coach->full_name }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                            <button class="btn btn-default" type="button" data-select2-open="select-life-coach">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                    </span>
                                </div>
                            </div>
                            {{--<div class="form-group selectfoodcoach">
                                <label for="select-food-coach" class="control-label"> Select Food Coach
                                </label>
                                <div class="input-group select2-bootstrap-append">
                                    <select id="selectfoodcoach" class="form-control select-food-coach" name="selectFoodCoach" >
                                        <option value="">Select Food Coach</option>
                                        @foreach($food_coach as $key => $coach)
                                            @foreach($coach as $in_key => $in_coach)
                                                <option title="{{ $in_coach->email }}" value="{{ $in_coach->user_id }}">{{  $in_coach->full_name }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" data-select2-open="select-food-coach">
                                            <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                     </span>
                                </div>
                            </div>--}}
                            <div class="form-group">
                                <label for="auto-post" class="control-label"> Auto Post Time
                                </label>
                                <div class="input-icon">
                                    <i class="fa fa-clock-o"></i>
                                    <input type="text" class="form-control timepicker timepicker-default" name="autoPostTime" readonly>
                                </div>
                                <div class="mt-checkbox-inline ">
                                    <label class="mt-checkbox">
                                        <input type="checkbox" class="autoPostEnable" value="1" name="autoPostEnable" aria-describedby="service-error"> Enable Auto Post
                                        <span></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <button type="button" class="btn  btn-default" class="close" data-dismiss="modal" aria-hidden="true"> &nbsp; Cancel &nbsp; </button>
                                <button type="submit" class="btn green-jungle" > &nbsp; Save &nbsp; </button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <div class="modal" id="remove_group">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Delete Group</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open( ['method' => 'POST', 'id' => 'delete-group', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('facilityadmin.group-delete') ] ) !!}
                        {!! Form::hidden('id', '' ) !!}
                            Are you sure you want to delete this group?
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" id="remove_group_from_facility">Delete</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <div class="modal" id="remove_group_coach">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Remove Coach</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open( ['method' => 'POST', 'id' => 'remove-coach-from-group', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('facilityadmin.remove-group-coach') ] ) !!}
                            {!! Form::hidden('uid', '' ) !!}
                            {!! Form::hidden('gid', '' ) !!}
                            Are you sure you want to remove this Coach from that Group?
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" id="remove_coach_from_group">Remove</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->



        <!-- bootstrap modals start here-->
        <!--BEGIN MODAL-->
        <div class="modal fade in" id="assign_coach_to_group" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Assign Coach</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row profile">
                            <div class="col-sm-12">
                                {!! Form::open( ['method' => 'POST', 'id' => 'assign_coach_to_groups', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('facilityadmin.assign-coach-to-group'), 'assign_coach_data' => route('facilityadmin.get-group-assign-coach-data') ] ) !!}
                                <div class="form-body">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <div class="mt-radio-list" data-error-container="#form_2_membership_error">
                                                <label class="mt-radio">
                                                    <input type="radio" class="membership" name="membership" value="1"> Food Coach
                                                    {!! Form::hidden('group_id', '' ) !!}
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div id="form_2_membership_error"> </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="mt-radio-list" data-error-container="#form_2_membership_error">
                                                <label class="mt-radio">
                                                    <input type="radio" class="membership" name="membership" value="2"> Lifestyle Coach
                                                    <span></span>
                                                </label>
                                            </div>
                                            <div id="form_2_membership_error"> </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"> </div>

                                    <div class="form-group select_multi_coach_food" style="display: none;">
                                        <label for="multiple" class="control-label"> Select Coach </label>
                                        <div class="input-group select2-bootstrap-append">
                                            <select id="food_coach" class="form-control select2" name="select_multi_food" multiple>
                                                <optgroup label="Food Coach">
                                                </optgroup>
                                            </select>
                                            <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" data-select2-open="multiple">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group select_multi_coach_life" style="display: none;">
                                        <label for="multiple" class="control-label"> Select Coach </label>
                                        <div class="input-group select2-bootstrap-append">
                                            <select id="lead_coach" class="form-control select2" name="select_multi_life" multiple>
                                                <optgroup label="Lifestyle Coach">
                                                </optgroup>
                                            </select>
                                            <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" data-select2-open="multiple">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-12 text-right">
                                        <button type="button" class="btn  btn-default" data-dismiss="modal"> &nbsp; Cancel &nbsp; </button>
                                        <button type="submit" class="btn green-jungle" id="assign_coach_for_facility"> &nbsp; Submit &nbsp; </button>
                                    </div>

                                </div>
                                <div class="clearfix"> </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

@stop

@extends('include.admin-layout')

@section('page-title')
    Coach
@stop

@section('body-class')

@stop


@section('content')

    <!-- BEGIN PAGE CONTENT BODY -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- BEGIN PAGE BREADCRUMBS -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="{{route('facilityadmin.home')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>COACH</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMBS -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="page-content-inner">
                <div class="mt-content-body">
                    @if(!$add_coach)
                        {!! Form::open( array('url' => URL::route('facilityadmin.coach-edit-store'), 'method' => 'post', 'id' => 'coach_edit_form', 'role' => 'form') ) !!}
                    @else
                        {!! Form::open( array('url' => URL::route('facilityadmin.coach-add-store'), 'method' => 'post', 'id' => 'coach_edit_form', 'role' => 'form') ) !!}
                    @endif

                        <div class="row">
                            <div class="portlet light ">

                                <div class="portlet-body form">

                                    <div class="alert alert-danger margiv-top-10" style="display: none">
                                        <button class="close" data-close="alert"></button>
                                        <span></span>
                                    </div>

                                    <div class="alert alert-warning margiv-top-10" style="display: none">
                                        <button class="close" data-close="alert"></button>
                                        <span></span>
                                    </div>

                                    <div class="col-md-6">
                                        <h3 class="form-section">Personal Details</h3>

                                        <div class="form-group">
                                            <label class="control-label">First Name</label>
                                            @if(!$add_coach)
                                                <input type="hidden" value="{{ base64_encode($coach->provider_id) }}" name="coach_id" />
                                                <input type="hidden" value="{{ base64_encode($coach->user_id) }}" name="user_id" />
                                            @endif
                                            <input type="text" placeholder="First Name" class="form-control" value="{{ @$coach->first_name }}" name="first_name" />
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Last Name</label>
                                            <input class="form-control" placeholder="Last Name" name="last_name" value="{{ @$coach->last_name }}" type="text">
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Type</label>
                                            @if($add_coach)
                                                {!! Form::select('coach_type', $coach_type, '',  ['class' =>'form-control']) !!}
                                            @else
                                                {!! Form::select('coach_type', $coach_type, $coach->user_type,  ['class' =>'form-control', 'disabled', 'selected']) !!}
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Acuity Link</label>
                                            <input class="form-control" name="acuity_link" value="{{ @$coach->acuity_link }}" type="text">
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <h3 class="form-section">Contact Info</h3>
                                        <div class="form-group">
                                            <label class="control-label">Email</label>
                                            @if($add_coach)
                                                <input class="form-control" name="email" value="" type="text">
                                            @else
                                                <input class="form-control" name="email" value="{{ $coach->email }}" type="text">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Phone</label>
                                            <input class="form-control" name="phone" value="{{ @$coach->phone }}" type="text">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">City</label>
                                            @if($add_coach)
                                                <input class="form-control" name="city" value="" type="text">
                                            @else
                                                <input class="form-control" name="city" value="{{ $coach->city }}" type="text">
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">State</label>
                                            @if($add_coach)
                                                {!! Form::select('state', $state, '',  ['class' =>'form-control']) !!}
                                            @else
                                                {!! Form::select('state', $state, $coach->state,  ['class' =>'form-control']) !!}
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox">
                                                    {!! Form::checkbox('is_email_enabled', 1, @$coach->is_email_enabled, ['class' => 'field']) !!}  Email Enabled
                                                    <span></span>
                                                </label>
                                                <label class="mt-checkbox">
                                                    {!! Form::checkbox('is_sms_enabled', 1, @$coach->is_sms_enabled, ['class' => 'field']) !!} SMS Enabled
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="margiv-top-10 col-md-6">
                                        @if(!$add_coach)
                                            <button type="submit" class="btn green-jungle">Update</button>
                                        @else
                                            <button type="submit" class="btn green-jungle">Save</button>
                                        @endif
                                        <a href="javascript:history.back()"><button type="button" class="btn grey-salsa btn-outline">Cancel</button></a>
                                    </div>
                                    @if(!$add_coach)
                                        <div class="margiv-top-10 col-md-6">
                                            <button type="button" class="btn green-jungle" id ="coach_reset_password_btn" data-id="{{ $coach->user_id }}">Resend Email/Pin Code</button>
                                        </div>
                                    @endif
                                </div>
                                <div class="clearfix"></div>

                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        <!-- END PAGE CONTENT INNER -->
        </div>
    </div>

    <!-- END PAGE CONTENT BODY -->

@stop

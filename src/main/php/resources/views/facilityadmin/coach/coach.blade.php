@extends('include.admin-layout')

@section('page-title')
    Coaches
@stop

@section('body-class')

@stop


@section('content')

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container-fluid">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{route('facilityadmin.home')}}">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Coaches </span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="mt-content-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">

                                            <div class="caption font-blue-ebonyclay">
                                                <i class="icon-settings font-blue-ebonyclay"></i>
                                                <span class="caption-subject bold uppercase">Coaches</span>
                                            </div>

                                            <div class="actions">
                                                <a class="btn green-jungle" id="modal_assign_coach" data-target="#modal_assigncoach" data-backdrop="static" data-keyboard="false" data-toggle="modal">
                                                    <i class="fa fa-tasks"></i> Assign Coach
                                                </a>
                                                <a class="btn green-jungle" href="{{ route('facilityadmin.coach-edit', ['coach_id' => 'add']) }}">
                                                    <i class="fa fa-plus"></i> Add Coach
                                                </a>
                                            </div>

                                        </div>
                                        <div class="portlet-body">
                                            @if(Session::has('message'))

                                                <div class="alert alert-warning margiv-top-10">
                                                    <button class="close" data-close="alert"></button>
                                                    <span> {!! Session::get('message') !!} </span>
                                                </div>

                                            @endif

                                            <table id="coach_list" data-action="{{ route('facilityadmin.coachesList') }}" class="table table-striped table-bordered table-hover dt-responsive" aria-describedby="table_info" >
                                                <div class="alert alert-warning alert-dismissable" style="display:none">
                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                    <span>Coach removed Successfully.</span>
                                                </div>
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 1px;" aria-label="Coach ID: activate to sort column ascending"></th>
                                                        <th class="sorting_desc" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 40px;" aria-label="Coach ID: activate to sort column ascending">Coach ID</th>
                                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 87px;" aria-label="Name: activate to sort column ascending">Name</th>
                                                        <th class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 70px;" aria-label="Email: activate to sort column ascending">Email</th>
                                                        <th class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 100px;" aria-label="Phone: activate to sort column ascending">Phone</th>
                                                        <th class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 50px;" aria-label="Type: activate to sort column ascending">Type</th>
                                                        <th class="sorting_disabled" role="columnheader" rowspan="1" colspan="1" style="width: 20px;" aria-label="SMS Enabled?">SMS/Email Alert</th>
                                                        <th class="sorting_disabled" role="columnheader" tabindex="0" aria-controls="table" rowspan="1" colspan="1" style="width: 80px;" aria-label="Action: activate to sort column ascending">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                                    <?php /*
                                                    <?php $div_flag = false ?>
                                                    @foreach($coaches as $key => $coach)
                                                        <tr role="row" class="{{ $div_flag ? 'odd' : 'even' }}" data-row_id="{{ $coach->provider_id }}">
                                                            <td></td>
                                                            <td>{{ $coach->provider_id }}</td>
                                                            <td>{{ $coach->first_name . ' ' . $coach->last_name }}</td>
                                                            <td><span class="word-break">{{ $coach->email}}</span></td>
                                                            <td><span class="phone-number">{{ $coach->phone }}</span></td>
                                                            <td>{{ $coach->user_type }}</td>
                                                            <td>{{ ($coach->is_email_enabled == 1) ? 'Yes' : 'No' }}</td>
                                                            <td>{{ ($coach->is_sms_enabled == 1) ? 'Yes' : 'No'}}</td>
                                                            <td><a data-toggle="modal" data-id="{{ $coach->provider_id }}" data-target="#remove_coach" class="btn dark btn-sm btn-outline sbold uppercase">Remove <i class="fa fa-trash"></i></a></td>
                                                        </tr>
                                                        <?php $div_flag = !$div_flag  ?>
                                                    @endforeach */ ?>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

        <!-- bootstrap modals start here-->
        <!--BEGIN MODAL-->
        <div class="modal fade in" id="modal_assigncoach" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog ">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Assign Coach</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row profile">
                            <div class="col-sm-12">
                                {!! Form::open( ['method' => 'POST', 'id' => 'assign_coach_to_facility', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('facilityadmin.assign-coach'), 'assign_coach_data' => route('facilityadmin.get-assign-coach-data') ] ) !!}
                                    <div class="form-body">
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <div class="mt-radio-list" data-error-container="#form_2_membership_error">
                                                    <label class="mt-radio">
                                                        <input type="radio" class="membership" name="membership" value="1"> Food Coach
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div id="form_2_membership_error"> </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mt-radio-list" data-error-container="#form_2_membership_error">
                                                    <label class="mt-radio">
                                                        <input type="radio" class="membership" name="membership" value="2"> Lifestyle Coach
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div id="form_2_membership_error"> </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"> </div>

                                        <div class="form-group select_multi_coach_food" style="display: none;">
                                            <label for="multiple" class="control-label"> Select Coach </label>
                                            <div class="input-group select2-bootstrap-append">
                                                <select id="food_coach" class="form-control select2" name="select_multi_food" multiple>

                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" data-select2-open="food_coach">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group select_multi_coach_life" style="display: none;">
                                            <label for="multiple" class="control-label"> Select Coach </label>
                                            <div class="input-group select2-bootstrap-append">
                                                <select id="lead_coach" class="form-control select2" name="select_multi_life" multiple>

                                                </select>
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default" type="button" data-select2-open="lead_coach">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="form-group col-md-12 text-right">
                                            <button type="button" class="btn  btn-default" data-dismiss="modal"> &nbsp; Cancel &nbsp; </button>
                                            <button type="submit" class="btn green-jungle" id="assign_coach_for_facility"> &nbsp; Submit &nbsp; </button>
                                        </div>

                                    </div>
                                    <div class="clearfix"> </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->


        <div class="modal" id="remove_coach">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Remove Coach</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open( ['method' => 'POST', 'id' => 'delete-msg', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('facilityadmin.delete-coach') ] ) !!}
                        {!! Form::hidden('id', '' ) !!}
                        Are you sure you want to remove the Coach from this facility?
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-danger" id="remove_coach_from_facility">Remove</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

@stop

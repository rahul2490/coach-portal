@extends('include.admin-layout')

@section('page-title')
    My Groups
@stop

@section('body-class')

@stop

@section('content')

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container-fluid">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{route(session('userRole').'.home')}}">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>My Groups</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="mt-content-body">
                            <div class="row">

                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption font-green">
                                                <i class="fa fa-users font-green"></i>
                                                <span class="caption-subject bold uppercase">My Groups</span>
                                            </div>
                                        </div>

                                        <div class="portlet-body">
                                            <div style="" class="col-md-3 col-sm-3 custom-filter-sort">
                                                <div id="" class="dataTables_filter">
                                                    <label>Group Type:
                                                        <select class="form-control input-sm input-small  input-inline" id="filterGroup">
                                                            <option value="all">All Groups</option>
                                                            <option value="inperson">In Person</option>
                                                            <option value="online">Online</option>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div style="" class="col-md-3 col-sm-3 custom-filter-sort">
                                                <div id="" class="dataTables_filter">
                                                    <label>Group Capacity:
                                                        <select class="form-control input-sm input-small  input-inline" id="filterCapacity">
                                                            <option value="all">All</option>
                                                            <option value="notfull">Available (Not Full)</option>
                                                            <option value="full">Full</option>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>

                                            <table class="table table-bordered dt-responsive member-table " width="100%" id="group_list" data-action="{{ route('foodcoach.group-list') }}">
                                                <thead>
                                                    <tr>
                                                        <th ></th>
                                                        <th width="30%">Group Name</th>
                                                        <th width="20%">Coaches</th>
                                                        <th width="5%"  data-orderable='false'>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

        </div>

        @include('common.message_group_member')

@stop

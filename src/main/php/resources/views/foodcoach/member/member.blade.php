@extends('include.admin-layout')

@section('page-title')
    Members Data
@stop

@section('body-class')

@stop

@section('content')

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->

            <!-- BEGIN PAGE CONTENT BODY -->
            <div class="page-content">
                <div class="container-fluid">
                    <!-- BEGIN PAGE BREADCRUMBS -->
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <a href="{{route(session('userRole').'.home')}}">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Members Data</span>
                        </li>
                    </ul>
                    <!-- END PAGE BREADCRUMBS -->
                    <!-- BEGIN PAGE CONTENT INNER -->
                    <div class="page-content-inner">
                        <div class="mt-content-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption font-blue-ebonyclay">
                                                <i class="icon-settings font-blue-ebonyclay"></i>
                                                <span class="caption-subject bold uppercase">Members Data</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            {{--<div style="" class="col-md-3 col-sm-3 custom-filter-sort">
                                                <div id="" class="row dataTables_filter"><label class="control-label col-sm-4">Filter:</label>
                                                    <div class="col-sm-8">
                                                        <select id="membersFilter_" class="form-control input-sm input-small input-inline sort-messages" >
                                                            <option value="all">All</option>
                                                            --}}{{--<option value="member_without_group">Members Without Groups</option>--}}{{--
                                                            --}}{{--<option value="recentMessages" selected="selected">Recent Messages</option>
                                                            <option value="recentCoachNotes">Recent Coach Notes</option>
                                                            <option value="myNewMembers">My New Members</option>
                                                            <option value="inactivityWatchList">Inactivity Watch List</option>
                                                            <option value="oneToOneSessions">Scheduled 1:1 sessions</option>
                                                            <option value="threePercentWeightLoss">3% weight loss</option>
                                                            <option value="sevenPercentWeightLoss">7% weight loss</option>--}}{{--

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>--}}
                                            <div style="" class="col-md-4 col-sm-4 custom-filter-sort">
                                                <div id="" class="row dataTables_filter"><label class="control-label col-sm-2">Filter:</label>
                                                    <div class="col-sm-8">
                                                        <select id="member_filter" class="form-control input-sm input-small input-inline sort-messages" >
                                                            <option value="all">All Members</option>
                                                            <option value="hidden_members">Hidden Members</option>
                                                            <option value="in_person_member">In Person Members</option>
                                                            <option value="online_member">Online Members</option>
                                                            <option value="member_without_group">Members Without Groups</option>
                                                            @if(!empty($member_group_id))
                                                                <option value="member_group" selected>Member By Group Name</option>
                                                            @else
                                                                <option value="member_group" >Member By Group Name</option>
                                                            @endif
                                                            @if(empty($member_group_id))
                                                                <option value="my_members" selected>My Members</option>
                                                            @else
                                                                <option value="my_members">My Members</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            @if($member_group_id != '')
                                                <div class="col-md-4 col-sm-4 custom-filter-sort group_filter">
                                            @else
                                                <div style="display: none" class="col-md-4 col-sm-4 custom-filter-sort group_filter">
                                            @endif
                                                <div class="row dataTables_filter"><label class="control-label col-sm-2">Group:</label>
                                                    <div class="col-sm-8">
                                                        {!! Form::select('group', $facility_group_list, $member_group_id ,  ['id' => 'group_filter' ,'class' =>'form-control input-sm input-small input-inline sort-messages']) !!}
                                                    </div>
                                                </div>
                                            </div>


                                                        <div style="display: none" class="col-md-4 col-sm-4 custom-filter-sort another_filter">

                                                            <div class="row dataTables_filter"><label class="control-label col-sm-2">Members Without Groups:</label>
                                                                <div class="col-sm-8">
                                                                    {{--{!! Form::select('group', $facility_group_list, $member_group_id ,  ['id' => 'another_filter' ,'class' =>'form-control input-sm input-small input-inline sort-messages']) !!}--}}
                                                                    <select id="another_filter" class="form-control input-sm input-small input-inline another_filter">
                                                                        <option value="">Select Type</option>
                                                                        <option value="all">All</option>
                                                                        <option value="offline_members">Offline</option>
                                                                        <option value="ennrolled_members">Enrolled</option>
                                                                    </select>

                                                                </div>
                                                            </div>
                                                        </div>



                                            <table class="table table-bordered dt-responsive member-table member-list" style="width:100%;margin-bottom:70px!important;" id="member_list" data-action="{{ route('foodcoach.membersList') }}">
                                                <thead>
                                                <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                <tr>
                                                    <th ></th>
                                                    <th width="10%">Status</th>
                                                    <th width="10%">Status (Engagement Score)</th>
                                                    <th width="20%" data-orderable='false'>Recommended Message</th>
                                                    <th width="3%">Last Sent</th>
                                                    <th width="3%">Unread </th>
                                                    <th width="3%">Enrollment Date</th>
                                                    <th width="5%"  data-orderable='false'>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>


                                        </div>
                                    </div>
                                    <!-- END EXAMPLE TABLE PORTLET-->
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- END PAGE CONTENT INNER -->
                </div>
            </div>
            <!-- END PAGE CONTENT BODY -->
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->



        @include('common.direct_message')




        <div class="modal" id="member_detail">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Member Details</h4>
                    </div>
                    <div class="modal-body">
                        {!! Form::open( ['method' => 'GET', 'id' => 'patient_detail', 'onsubmit' => 'return false;','role' => 'form', 'url' => route('facilityadmin.member-detail') ] ) !!}
                        {!! Form::hidden('id', '' ) !!}
                        <ul class="member_data">
                        </ul>
                        {!! Form::close() !!}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Cancel</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


@stop


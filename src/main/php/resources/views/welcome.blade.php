@extends('include.admin-layout')

@section('page-title')
    Home
@stop

@section('body-class')

@stop


@section('content')

    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE CONTENT BODY -->

    <!-- BEGIN PAGE CONTENT BODY -->
    <div class="page-content">
        <div class="container-fluid">
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="page-content-inner">
                <div class="mt-content-body">
                    <form role="form" action="#" id="form_facilityedit" action="#">
                        <div class="row">
                            <div class="portlet light ">
                                <!-- <div class="portlet-title tabbable-line">
                                    <div class="caption caption-md">
                                        <i class="icon-globe font-blue-ebonyclay"></i>
                                        <span class="caption-subject font-blue-ebonyclay bold uppercase">Welcome!</span>
                                    </div>
                                </div> -->
                                <div class="portlet-body form">
                                    <div class="col-md-12">
                                        <h2 class="meal-summary-text">Welcome to the Coaching and Program Coordinator Site.</h2>
                                        <h4>Some of the areas you will work with, includes:</h4>
                                        <ul class="teal-bullets">
                                            @if(session('userRole') == 'admin')
                                                <li><label>Add/Edit Facilities</label></li>
                                            @endif

                                            @if(session('userRole') == 'admin' || session('userRole') == 'facilityadmin')
                                                <li><label>Add/Edit Coaches</label></li>
                                                <li><label>Assign Lifestyle Coaches to Members</label></li>
                                            @endif
                                                <li><label>Facility Members List</label></li>
                                            <li><label>Invite Members to join  @if(get_cobrand_id() == 1) HealthSlate @else SoleraONE @endif Program</label></li>
                                        </ul>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>



                            </div>
                        </div>

                </div>
                </form>
            </div>
        </div>
        <!-- END PAGE CONTENT INNER -->
    </div>
    </div>
    <!-- END PAGE CONTENT BODY -->

    <!-- bootstrap modals notify password expire start here-->
    <!--BEGIN MODAL-->
    <div class="modal fade" id="modal_password_expiration" data-backdrop="static">
        <div class="modal-dialog modal-small">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Your password is about to expire.</h4>
                </div>
                <div class="modal-body">
                   @if((60 - $date_diff)== 1)
                        <p><i class="fa fa-warning font-red"></i> Your password will expire today. </p>
                    @elseif((60 - $date_diff)== 2)
                        <p><i class="fa fa-warning font-red"></i> Your password will expire tommorow. </p>
                    @else
                    <p>
                        <i class="fa fa-warning font-red"></i> Your password will expire in  {{ 60 - $date_diff }}
                        <span id="idle-timeout-counter"></span> days.</p>
                    @endif
                     <p> To change your password click  <a id="click_change_passowrd">here </a> </p>
                </div>
                <div class="modal-footer">
                    <button  type="button" class="btn green btn-outline sbold uppercase" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<!-- bootstrap modals for notify password expire end here-->

    {{-- it's for getting date difference of registration/password modify date and today date   --}}
    <script>

        DATE_DIFFERENCE = '{{ $date_diff }}';

    </script>

@stop

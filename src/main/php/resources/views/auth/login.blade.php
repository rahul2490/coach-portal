@extends('include.single-form-layout')

@section('body-class') login @stop

@section('content')

    <!-- BEGIN LOGO -->
    <div class="logo-wrapper">
        <div class="logo">
            <a href="javascript:void(0)" class="btn">
                @if($cobrand_id==1)
                <img src="{{ asset('img/logo-big.png') }}" alt="" class="img-responsive" /> </a>
            @else
                <img src="{{ asset('img/SoleraONE.png') }}" alt="" class="img-responsive" /> </a>
            @endif
        </div>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        <!-- BEGIN LOGIN FORM -->
        {!! Form::open( array('url' => URL::route('login-handler'), 'method' => 'post', 'class' => 'login-form', 'role' => 'form') ) !!}
            <h3 class="form-title font-green-jungle">Sign In</h3>

            <div class="form-group">
                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                <label class="control-label visible-ie8 visible-ie9">Email address</label>
                {!! Form::text('username', old('username'), array('placeholder' => 'Email address', 'class' => 'form-control form-control-solid placeholder-no-fix', 'autofocus')) !!}
            </div>
            <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                {!! Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control form-control-solid placeholder-no-fix')) !!}
            </div>
            <div class="form-actions no-border">
                <button type="submit" class="btn green-jungle uppercase">SIGN IN</button>
                <div>
                    <a href="javascript:void(0);" style="float: left;" id="forget-password" class="forget-password font-green-jungle">Forgot Password?</a>
                </div>
            </div>

            <div class="alert alert-danger validation_form_ajax display-hide margin-top-10">
                <button class="close" data-close="alert"></button>
                <span> Enter any email and password. </span>
            </div>

            @if(Session::has('errorMsg'))
            <div class="alert alert-danger validation_form_server margin-top-10">
                <button class="close" data-close="alert"></button>
                <span> {!! Session::get('errorMsg') !!} </span>
            </div>
            @endif

        {!! Form::close() !!}
        <!-- END LOGIN FORM -->

        <!-- BEGIN FORGOT PASSWORD FORM -->
        {!! Form::open( array('url' => URL::route('forgot-password'), 'method' => 'post', 'id' => 'forgot_password', 'role' => 'form', 'class' => 'forget-form') ) !!}
            <h3 class="font-green-jungle">Forget Password ?</h3>
            <p> Enter your e-mail address below to reset your password. </p>

            <div class="form-group">
                <input class="form-control placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email" />
            </div>

            <div class="form-actions">
                <button type="button" id="back-btn" class="btn green-jungle btn-outline">Back</button>
                <button type="submit" class="btn green-jungle uppercase pull-right">Submit</button>
            </div>
        {!! Form::close() !!}
        <!-- END FORGOT PASSWORD FORM -->

    </div>

    <div class="copyright">
        <div class="page-footer-inner"> {{date('Y')}} &copy; All rights reserved</div>
    </div>

    <script>
        var DATE_DIFFERENCE;

    </script>


@stop
@extends('include.single-form-layout')

@section('body-class') login @stop

@section('content')

    <!-- BEGIN LOGO -->
    <div class="logo-wrapper">
        <div class="logo">
            <a href="javascript:void(0)" class="btn">
                @if($cobrand_id==1)
                    <img src="{{ asset('img/logo-big.png') }}" alt="" class="img-responsive" /> </a>
            @else
                <img src="{{ asset('img/SoleraONE.png') }}" alt="" class="img-responsive" /> </a>
            @endif
        </div>
    </div>
    <!-- END LOGO -->
    <!-- BEGIN LOGIN -->
    <div class="content">
        @if(isset($token_exipred))

            <div class="form-group">
               <h3 class="font-green-jungle"> Your link has been expired </h3>
            </div>

        @else
            <!-- BEGIN LOGIN FORM -->
        {!! Form::open( array('url' => URL::route('resetpasswordstore'), 'method' => 'post', 'id' => 'reset_password', 'class' => 'login-form', 'role' => 'form') ) !!}
        <h3 class="form-title font-green-jungle">Reset Password</h3>
        {!! Form::hidden('reset_password_token',  $token , array('id' => 'reset_password_token')) !!}

        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            {!! Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control form-control-solid placeholder-no-fix','id'=>'password')) !!}
        </div>

        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Repeat Password</label>
            {!! Form::password('repeat_password', array('placeholder' => 'Repeat Password', 'class' => 'form-control form-control-solid placeholder-no-fix','id'=>'repeat_password')) !!}
        </div>

        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Pin Code</label>
            {!! Form::text('pin_code', old('pin_code'), array('placeholder' => 'Pin Code', 'class' => 'form-control form-control-solid placeholder-no-fix', 'autofocus' ,'id'=>'pin_code')) !!}
        </div>
        <div class="form-actions no-border">
            <center><button type="submit" class="btn green-jungle uppercase">RESET MY PASSWORD</button></center>
            {{--<label class="rememberme check mt-checkbox mt-checkbox-outline">
                <input type="checkbox" name="remember" value="1" />Remember
                <span></span>
            </label>--}}
            {{--<div class="">
                <a href="javascript:void(0);" style="float: left;" id="forget-password" class="forget-password font-green-jungle">Forgot Password?</a>
            </div>--}}
        </div>

         {{--<div class="alert alert-danger display-hide margin-top-10">
            <button class="close" data-close="alert"></button>
            <span> Enter any email and password. </span>
        </div>--}}

        @if(Session::has('errorMsg'))
            <div class="alert alert-danger margin-top-10">
                <button class="close" data-close="alert"></button>
                <span> {!! Session::get('errorMsg') !!} </span>
            </div>
    @endif

    {!! Form::close() !!}
    <!-- END LOGIN FORM -->

            @endif

    </div>

    <div class="copyright">
        <div class="page-footer-inner"> {{date('Y')}} &copy; All rights reserved</div>
    </div>

    <script>
        var DATE_DIFFERENCE;

    </script>


@stop
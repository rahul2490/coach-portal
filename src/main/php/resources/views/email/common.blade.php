<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>{{ $subject or '' }}</title>
    <style>
        * {
            font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
        }
    </style>
</head>
<body style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;height: 100%;-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important;margin:0;">
    Hello {!! $name or '' !!},<br/>
    <p>
        {!! $content or '' !!}
    </p>
    <p>
        <small>{{ $footer_message or '' }}</small>
    </p>
    @if(!isset($hide_hs_footer) || !$hide_hs_footer)
    <p>
        Sincerely,<br/>
        The @if(get_cobrand_id() == 1) HealthSlate @else SoleraONE @endif Team
    </p>
    @endif
</body>
</html>

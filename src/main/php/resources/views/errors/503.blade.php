@extends('include.single-form-layout')

@section('body-class')
skin-blue
@stop

@section('content')
    <div class="row">
        <div class="col-md-12 page-404">
            <div class="number font-red"> {{ $code or '' }} </div>
            <div class="details">
                <h3>
                    @if(isset($code) && $code == 404)
                        Oops! Page not found.
                    @else
                        Whoops, looks like something went wrong.
                    @endif
                </h3>
                <p> We could not find the page you were looking for </p>


            </div>
        </div>
    </div>

@stop

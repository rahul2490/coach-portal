<?php

$base_url = env( 'PROFILE_IMAGE_BASE_URL');
if($base_url == 'https://hstage.healthslate.com/' || $base_url == 'https://hstage.healthslate.com' || $base_url == 'http://hstage.healthslate.com/' ||
$base_url == 'hstage.healthslate.com')
{
    //Staging Pem file
    return [
        'gcm' => [
            'priority'  => 'normal',
            'dry_run'   => false,
            'apiKey'    => 'AIzaSyCbbZFkq8LMhwEilD2pIAaiWOFkPZ0-Bco',
        ],
        'fcm' => [
            'priority'  => 'normal',
            'dry_run'   => false,
            'apiKey'    => 'AIzaSyCbbZFkq8LMhwEilD2pIAaiWOFkPZ0-Bco',
        ],
        'apn' => [
            'certificate'   => config_path() . '/iosCertificates/WenderCastPush.pem',
            'passPhrase'    => '123Pat!3Nt123',
            'passFile'      => config_path() . '/iosCertificates/WenderCastPush.pem',
            'dry_run'       => true
        ]
    ];
}
else
{
    //Production Pem file
    return [
        'gcm' => [
            'priority'  => 'normal',
            'dry_run'   => false,
            'apiKey'    => 'AIzaSyCbbZFkq8LMhwEilD2pIAaiWOFkPZ0-Bco',
        ],
        'fcm' => [
            'priority'  => 'normal',
            'dry_run'   => false,
            'apiKey'    => 'AIzaSyCbbZFkq8LMhwEilD2pIAaiWOFkPZ0-Bco',
        ],
        'apn' => [
            'certificate'   => config_path() . '/iosCertificates/PatientApp-Prod-ReleaseCert.pem',
            'passPhrase'    => '123Pat!3Nt123',
            'passFile'      => config_path() . '/iosCertificates/PatientApp-Prod-ReleaseCert.pem',
            'dry_run'       => true
        ]
    ];
}


<?php


$api_base_url = env('HS_SERVICE_BASE_URL', 'http://localhost:8080/');

$public_base_url = env('HS_SERVICE_PUBLIC_BASE_URL', 'https://healthslate.com/');

$URL_PREFIX = rtrim($api_base_url, '/') . '/services/v2/';

$V3_URL_PREFIX = rtrim($api_base_url, '/') . '/services/v3/';

$curriculum_api_base_url = env( 'HS_CURRICULUM_SERVICE_BASE_URL', 'https://localhost:3000/' );
$curriculum_v2_api_base_url = rtrim( env( 'HS_CURRICULUM_SERVICE_V2_BASE_URL', 'https://ckh1xro9qf.execute-api.us-east-1.amazonaws.com/' ), '/' ) . '/';
$CURRICULUM_URL_PREFIX = rtrim( $curriculum_api_base_url, '/' ) . '/api/';
$engagement_url = 'https://asmfxi0ml1.execute-api.us-east-1.amazonaws.com/prod/patientState';

return array(
    'guzzle_config' => [
        'base_uri' => $URL_PREFIX,
        'http_errors' => false,
        'decode_content' => true,
        'verify' => true,
        'cookies' => false,
        'connect_timeout' => 15,
        'headers' => [
            'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) HealthSlate-Meal-Portal/1.0',
            'Accept' => 'application/json',
        //'auth' => ['username', 'password']
        ]
    ],
    /**
     * =========================================================
     * =========================================================
     *
     * Health Slate Service API URLs, Should not be modified
     */
    /**
     * Oauth Login URL
     */
    'POST_get_auth_token' => 'auth/getAuthTokenAdmin',
    /**
     * validate user token
     */
    'GET_validate_token' => 'auth/validateToken',
    /**
     * get refresh token
     */
    'GET_get_refresh_token' => 'auth/getRefreshToken',
    /**
     * get user info & user roles by access token
     */
    'GET_user_info_by_token' => 'user/getUserRole',
    /**
     * @deprecated in favour of `POST_store_wmr_message`
     */
    'POST_send_meal_recommendation_email' => 'log/sendMealRecommendationEmail',
    /**
     * store wmr message
     *
     */
    'POST_store_wmr_message' => $V3_URL_PREFIX . 'weeklyMealReview/create',
    'POST_bulk_store_wmr_messages' => $V3_URL_PREFIX . 'weeklyMealReview/bulk/create',
    /**
     * send wmr messages by ids
     */
    'POST_send_wmr_message' => $V3_URL_PREFIX . 'weeklyMealReview/sendWMRMessages',
    /**
     *
     */
    'DELETE_delete_wmr_message' => $V3_URL_PREFIX . 'weeklyMealReview/delete',
    /**
     * redirect handler URL for WMR Message
     */
    'redirect_to_screen' => rtrim($public_base_url, '/') . '/services/v3/redirect/meal/',
    /**
     * redirect url for member mesage log screen
     */
    'member_message_log_screen_url' => rtrim($public_base_url, '/') . '/app/educator/coachPatientMessages.action',
    /**
     * redirect url for member dashboard screen
     */
    'member_dashboard_url' => rtrim($public_base_url, '/') . '/app/educator/dashboard.action',
    /**
     * search food on Fatsecret
     */
    'GET_fatSecret_search_food_name' => $V3_URL_PREFIX . 'fatSecret/getFoods',
    /**
     * Get food detail from Fatsecret
     */
    'GET_fatSecret_get_food_detail' => $V3_URL_PREFIX . 'fatSecret/getFoodDetail',
    /**
     * Create suggested Meal
     */
    'POST_create_suggested_meal' => 'meal/saveSuggestedMealLog',

    'GET_curriculum_users_report_by_id'       =>  $CURRICULUM_URL_PREFIX.'users/',

    'GET_engagement_users_report_by_id'        => $engagement_url,

    'GET_dpp_curriculum_users_report_by_uuid'       =>  rtrim( $curriculum_api_base_url, '/' ) .'/curriculum/report/',

    'GET_dpp_curriculum_users_sections_by_uuid'       =>  rtrim( $curriculum_api_base_url, '/' ) .'/curriculum/user/',
    /**
     * Batch request API for getting list of completed sections
     */
    'POST_curriculum_users_sections_completed_by_uuids'       =>  rtrim( $curriculum_api_base_url, '/' ) .'/curriculum/report/sections/',
    /**
     * curriculum v2 API URL
     */
    'POST_curriculum_v2_request_url'       => $curriculum_v2_api_base_url . 'prod/curriculum',
);

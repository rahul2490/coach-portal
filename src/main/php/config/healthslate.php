<?php
/**
 * User: rpatidar
 * Date: 31/08/2017
 */

return array(


	/**
	 * =========================================================
	 * =========================================================
	 *
	 * Health Slate Custom Configurations
	 */

	'profile_image_base_url' => rtrim(env('PROFILE_IMAGE_BASE_URL', 'https://healthslate.com/'),'/'),

	'log_image_base_url' => rtrim(env('LOG_IMAGE_BASE_URL', 'https://healthslate.com/'),'/'),

    'log_image_save_url'=> '/usr/share/tomcat7/webapps/ROOT/',

    'log_post_image_save_url'=> '/usr/share/tomcat7/webapps/patientapp-groups/',

    'motivation_images_save_url'=> '/usr/share/tomcat7/webapps/ROOT/motivationImages/',

    'user_profile_images_save_url'=> '/usr/share/tomcat7/webapps/ROOT/profileImages/',

    'IOS_PDF_ICON_FOR_NOTIFICATION' => '\xf0\x9f\x93\x84',

    'IOS_IMAGE_ICON_FOR_NOTIFICATION' => '\xf0\x9f\x93\xb7',

	/**
	 * allowed tags in chat message
	 */
	'allowed_html_tags'     =>  '<p><i><br><img><a><b>',
	/**
	 * min. number of chat messages to show by default
	 * int
	 */
	'last_chat_msg_to_show'     =>  100,
	/**
	 * min. number of chat users to show by default
	 * int
	 */
	'last_chat_user_to_show'  => 50,

	/**
	 * Default Date Time format
	 */
	'default_date_time_format'  =>  'm/d/Y h:i A',
	/**
	 * Default Date format
	 */
	'default_date_format'  =>  'm/d/Y',
	/**
	 * Default Date format for mysql queries
	 */
	'mysql_default_date_format'  =>  '%m/%d/%Y',
	/**
	 *  Minimum Table rows per page
	 *  For Laravel Paginator
	 */
	'table_row_per_page'          => 50,
	/**
	 * Minimum Table rows per page for DataTable
	 */
	'list_datatable_row_per_page' => 50,
	/**
	 * moment js date time formats
	 */
	'default_js_date_time_format'   => 'MM/DD/YYYY hh:mm A',
	/**
	 * moment js date format
	 */
	'default_js_date_format'   => 'MM/DD/YYYY',
	/**
	 *  Super admin Login ID and Password
	 */
	'hs_super_admin_login' => [
		'username' => env( 'HS_SUPER_ADMIN_ID', '' ),
		'password' => env( 'HS_SUPER_ADMIN_PASSWORD', '' )
	],
	/**
	 * WMR Scheduler Execution Day (int)
	 * 1 - Sunday, 2 - Monday, 3 - Tuesday and So on.
	 * Default is Friday i.e. 6
	 */
	'hs_WMR_sending_day'  => intval( env( 'HS_WMR_SEND_DAY', 6 ) ),
	/**
	 * WMR Scheduler Sending Time (int)
	 * It is in 24 hour format where
	 * First two digits are for hours 00 to 23
	 * Last two digits are for minutes 00, 15, 30, 45
	 * Note: Minutes should be in `multiple of 15` as cron runs on interval of 15 minutes per hour.
	 * Default is 5PM i.e. 1700
	 */
	'hs_WMR_sending_time' => intval( env( 'HS_WMR_SEND_TIME_HHMM', 1700 ) ),


    'hs_constants' => [
        'GENDER_MALE'              => 'Male',
        'GENDER_FEMALE'            => 'Female',
        /**
         * DIABETES_TYPE_IDs from diabetes_type table.
         */
        'TYPE_2_DIABETES_TYPE_ID'  => 1,
        'DPP_FAT_DIABETES_TYPE_ID' => 2,
        'DPP_CAL_DIABETES_TYPE_ID' => 3,
    ],
);
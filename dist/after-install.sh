#! /bin/sh

PROJECT_HOME='<%= prefix %>'

# Detecting web server user, might need to add  grep -v `whoami`
USER=$(cut -d: -f 1 /etc/passwd | egrep '(www-data|apache|apache2|nginx)' | head -n 1)

# Refresh / Generate cache files.
php $PROJECT_HOME/artisan route:cache

# Change file permissions
chown $USER -R $PROJECT_HOME
chmod 775 -R $PROJECT_HOME/storage/
chmod 775 -R $PROJECT_HOME/bootstrap/cache/

if [ "$(ls $PROJECT_HOME/storage/framework/views)" ]; then
	echo "Removing old views cache."
	rm -r $PROJECT_HOME/storage/framework/views/*
fi

echo "<%= name %> <%= version %>-<%= iteration %> successfully installed at $PROJECT_HOME."
